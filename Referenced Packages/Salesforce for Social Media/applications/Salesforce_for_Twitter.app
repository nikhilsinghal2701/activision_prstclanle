<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Salesforce_for_Twitter</defaultLandingTab>
    <label>Salesforce for Social Media</label>
    <tab>Salesforce_for_Twitter</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Case</tab>
    <tab>Twitter_Conversation__c</tab>
    <tab>Twitter_Account__c</tab>
    <tab>Twitter_Search__c</tab>
    <tab>Fcbk_Page__c</tab>
    <tab>Fcbk_User__c</tab>
    <tab>Non_Keywords__c</tab>
    <tab>Create_Gamer</tab>
    <tab>Report_on_Email_Templates</tab>
    <tab>Master_Links__c</tab>
    <tab>Internal_CS_Service_Request__c</tab>
</CustomApplication>
