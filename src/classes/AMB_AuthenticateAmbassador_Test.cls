@isTest

private class AMB_AuthenticateAmbassador_Test {
    static testMethod void testAuthenticateAmbassador(){
        test.startTest();
        
        /*class -START *********/           
        Account acc = new Account (
        Name = 'newAcc1'
        );  
        insert acc;
        Contact con = new Contact (
        AccountId = acc.id,
        LastName = 'portalTestUser',
        isambassador__c=true
        );
        insert con;
        
        Profile p = [select Id,name from Profile where name='ATVI OHV Cust Port User' limit 1];
        
        User newUser = new User(
        profileId = p.id,
        username = 'newUser@yahoo.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = con.id
        );
        insert newUser;
        /******** Create Customer portal user without having any role in test class - END *********/ 
        
        System.runAs(newUser){
        
        AuthenticateAmbassador obj = new AuthenticateAmbassador();
        obj.fnRediarect();
        }
        
        con.isambassador__c=false;
        update con;
        
        System.runAs(newUser){
        
        AuthenticateAmbassador obj = new AuthenticateAmbassador();
        obj.fnRediarect();
        }
        
        
        
        test.stopTest();
    }
}