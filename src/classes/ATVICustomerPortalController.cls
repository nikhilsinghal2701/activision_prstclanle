/**
* @author           A. PAL
* @date             03-MAR-2012
* @description      Controller for rendering VF content on the ATVI customer portal pages 
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* D. Mazumdar               05-MAR-2012             Fixed the issue with multi-lingual country specific 
*                                                   drop down functionality
* A. Pal                    06-APR-2012             Modified getIsloggedIn() method
* Mohith S                  07-APR-2012             Modified to add getProduct() Method to display my games in authenticated page
* A. Pal          21-APR-2012        Made following performance tuning changes:
*                          1. Added method doProductListLoad()
*                          2. Removed SOQL on Public Product from constructor to doProductListLoad() method 
* A. Pal          21-APR-2012        Beautified the code             
*---------------------------------------------------------------------------------------------------
* Review Log  :
*---------------------------------------------------------------------------------------------------
* Reviewer                      Review Date            Review Comments
* --------                      ------------          ---------------
* D. Mazumdar(Deloitte Senior Consultant)      08-MAR-2012            Passed
* Benji (Salesforce)                06-APR-2012            Suggested performance improvements
* A. Pal(Deloitte Senior Consultant)        19-APR-2012            Final Inspection before go-live
* Achin Suman (Deloitte Consultant)          25-APR-2012            EQA
*---------------------------------------------------------------------------------------------------
*/

public with sharing class ATVICustomerPortalController{

    public ATVICustomerPortalController(ApexPages.StandardController controller) {
    }

    /* Variable Declarations */
    public string username {get{System.debug('username being returned:'+username);return username;}set;}
    public Id sfContactId {get;set;}
    public String userCountry;
    public List<Asset_Custom__c> mygames;
    public boolean Isviewall{get;set;}
    public Boolean isClanLead {get;set;}
    public String userLastName {get;set;}
    public ChatNews__c cNews {get; set;}
    /*Used to set the language of the Apex page acccording to the user's choice*/    
    public String userLanguage;
    public list<alert__c> alerts {get;set;}
    public boolean show {get;set;}
    public string brand{get;set;}
    public Set<string> gametitles = new Set<String>();

    
    public boolean isMobile{get;set;}
    
    public Set<String> getgameTitles(){
        return gametitles;
    } 
    public Set<String> setgameTitles(){
        return gametitles ;
    }
    /*Used to set the language of the Apex page acccording to the user's choice*/    
    public String lang {get;set;}/*
    public void getUserLanguage1()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            system.debug('tempCookie not null');
            lang = tempCookie.getValue();            
        }else{
            String param = hasParam();
            if(param != null){
                system.debug('fgw lang == '+param);
                lang=param;
            }else{
                system.debug('tempCookie is null');
                lang='en_USDefault';
            }
        }
    }*/
    public string hasParam(){
        map<String, String> params = ApexPages.currentPage().getParameters();
        for(String key : params.keySet()){
            if(key.equalsIgnoreCase('uil') || key.equalsIgnoreCase('ulang')){
                return String.escapeSingleQuotes(params.get(key));
            }
        }
        return null;
    }
    
    /*Checks cookies if it should show. Expires in 24 hours. Can be edited.
    Cookie is set in js in page/component*/
    public void getShowAlert(){
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('showAlert');
        if(tempCookie != null){
            show=false;           
        }else{
            show=true;
        }
    }

    /*Constructor.  Gets Language, and checks if should be shown*/
    public String getUserLanguage()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            system.debug('tempCookie not null');
            userLanguage = tempCookie.getValue();            
        }else{
            String paramLang = hasParam();
            if(paramLang != null){
                system.debug('fgw paramLang = '+paramLang);
                userLanguage = paramLang;
            }else{
                system.debug('tempCookie is null');
                userLanguage='en_US';
            }
        }
        lang = userLanguage;
        return userLanguage;
    }
    
    public void setUserLanguage(String userLanguage)
    {
        this.userLanguage = userLanguage;
    }
    
    public String displayLanguage;
    public boolean isloggedIn;
    public string selectedLang;
    public String getSelectedLang()
    {
        System.debug('@@@@@@@@@@@@@@@@@'+selectedLang);
        return selectedLang;
        
    }
    public void setSelectedLang(String selectedLang)
    {       
        this.selectedLang = selectedLang;               
    }
    
    User thisUser       = new User();
    String userLocale   = UserInfo.getLocale();
    public String branding;
    public String brandParam;
    public String langParam;    
    transient Cookie brandCookie;
    transient Cookie langCookie;    
    public boolean isUS;    
    public string[] productName {set;get;}
    public ATVICustomerPortalController cspCon{get{return this;} set;}
    
    /*
        Default Constructor
    */
    public ATVICustomerPortalController(){  
        isMobile = Phone_Utils.getIsMobile();
        /*Fixes IE Styling Issues QT 11/13/12 7:02PM*/
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=Edge');     
        /*Code to verify whether the current user is a logged-in user*/
        User thisUser=[select id
                            , FirstName
                            , Country
                            , Contact.Id
                            , Contact.Name
                            , Contact.Email
                        from User 
                        where Id=:UserInfo.getUserId()];
                        
        if(thisUser != null){
            //  if Contact ID is not null, then the user is a customer portal user
            if(thisUser.Contact.Id != null){
                sfContactId = thisUser.Contact.Id;
                System.debug('Contact record found for user : ' + thisUser.Id);             
            }else{
                System.debug('No Contact record found for user : ' + thisUser.Id);
                sfContactId = null;
            }
        
        }
         // Default the user language to US English
        if( (userLanguage == null) || (userLanguage =='') ){
            //  Set the default user langauage to the logged in user's locale if user is logged in
            //  Else set English (en_US) as the default locate
            userLanguage    =   getUserCountry();
            
        }
        
       
        brandParam = ApexPages.currentPage().getParameters().get('clickedOn');
        if(brandParam != null) {
            brandCookie = new Cookie('clickedOn', brandParam, null, -1, false);
            ApexPages.currentPage().setCookies(new Cookie[]{brandCookie});                       
        }
        setupCNews(); 
        
        show=true;
        getUserLanguage();
        getShowAlert();
        brand = ApexPages.currentPage().getParameters().get('clickedOn');
        system.debug('+++++++Alerts1++++++++++'+alerts);
        if (brand==null || brand==''){
            alerts=[select id,name,English__c,Spanish__c,German__c,Dutch__c,Portuguese__c,Italian__c,French__c,Swedish__c,
                    TheLink__c,
                    TheLink_Dutch__c,
                    TheLink_French__c,
                    TheLink_German__c,
                    TheLink_Italian__c,
                    TheLink_Portuguese__c,
                    TheLink_Spanish__c,
                    TheLink_Swedish__c,
                    category__c
                    from Alert__c where isActive__c=true
                    Order by CreatedDate Desc
                    ];
                    system.debug('+++++++Alerts1++++++++++'+alerts);
                    

        }
        else{
            alerts=[select id,name,English__c,Spanish__c,German__c,Dutch__c,Portuguese__c,Italian__c,French__c,Swedish__c,
                    TheLink__c,
                    TheLink_Dutch__c,
                    TheLink_French__c,
                    TheLink_German__c,
                    TheLink_Italian__c,
                    TheLink_Portuguese__c,
                    TheLink_Spanish__c,
                    TheLink_Swedish__c,
                    Category__c
                    from Alert__c where isActive__c=true AND Category__c INCLUDES(:brand) Order by CreatedDate Desc];
                    system.debug('+++++++Alerts++++++++++'+alerts);
        }
        If(alerts.size() > 0 || alerts != null){
            for(Alert__c alertObj:alerts){
                    gameTitles.add(alertObj.Category__c);
                    If(gameTitles != null){
                    
                    }
            }
        }
        
        /*If there's no active alerts, Do not display*/
        if (alerts.size()==0)
        {
                show=false;
        }    
    }
    
    /*Used to pre-load the Game titles list for the Find a Game predictive search*/
    /*Created by A. PAL on 04.21.2012*/
    public PageReference doProductListLoad(){
      system.debug('Inside doProductListLoad');
      List<Public_Product__c> productList = new List<Public_Product__c>();
      productList = [ SELECT  Name, Image__c FROM Public_Product__c];
      
      productName = new String[0];
      for (Public_Product__c productObject: productList) {
         productName.add(productObject.Name);
      }          
          
        system.debug(productName.size()+' product titles added in productName');
        return null;        
   }
        
     /*Used in the Authenticated Homepage to display the games added to the profile of the logged-in user*/ 
   public List<Asset_Custom__c> getproduct(){
    
      Isviewall=false;
 
      system.debug('sfContactId inside getproduct'+sfContactId );
      List<Asset_Custom__c> myAssets=[Select Id 
                    ,Gamer__c
                    ,CreatedDate
                    ,Platform__c
                    ,My_Product__r.Image__c
                    ,My_Product__r.Name
                    ,My_Product__r.Type__c
                    ,My_Product__r.Data_Category__c
                    ,My_Product__r.Platform__c 
                   from Asset_Custom__c 
                  where Gamer__c=:sfContactId 
                  and My_Product__r.Type__c=:'Software' 
                  order by CreatedDate DESC];
      if(myAssets.size()>5){
        Isviewall=true;
        mygames=new List<Asset_Custom__c>();
        integer i;
        for(i=5;i<myAssets.size();i++){
          mygames.add(myAssets[i]);
        }      
      }    
      
      system.debug('Total number of games added to the profile of ['+UserInfo.getUserId()+']='+myAssets.size());        
      return myAssets;    
  }
      
    /*Returns the games to show in the 'Show All My Games' pop-up*/
    public List<Asset_Custom__c> getMypopgames(){
      return mygames;
    }
    
    public String eliteStatus{get;set;}  
    public String gamerName{get;set;}
    public String gamerEmail{get;set;}    
      
    /**
    *   @name           doUserNameLoad
    *   @description    used to show welcome message to logged in user with the proper firstname and lasname or gamer's tag name
    **/    
    public PageReference doUserNameLoad(){
        String userId   = UserInfo.getUserId();
        system.debug('User Id = ' + userId);            //  debugging statement
        if(sfContactId != null){
                List<Contact> gamers=[select id
                                           , name
                                           , salutation
                                           , lastname      
                                           , firstname                    
                                           , email
                                           , Elite_status__c
                                           , MailingCountry 
                                           , Gamertag__c
                                           , Country__c
                                       from Contact 
                                       where id=:sfContactId limit 1];
                
                if(gamers.size() > 0){
                    Contact gamer = gamers.get(0);
                    gamerName=AtviUtility.checkNull(gamer.Name);
                    gamerEmail=AtviUtility.checkNull(gamer.Email);  
                    String mailingCountry=AtviUtility.checkNull(gamer.MailingCountry);                     
                    if(gamer.Elite_Status__c != null){
                        username = AtviUtility.checkNull(gamer.Gamertag__c);
                        eliteStatus=AtviUtility.checkNull(gamer.Elite_Status__c);
                        if(username=='') username = AtviUtility.checkNull(gamer.firstname);
                    }else if(mailingCountry.contains('US') || mailingCountry.contains('States')){
                        username= AtviUtility.checkNull(gamer.firstname);
                        if(username=='') username = AtviUtility.checkNull(gamer.lastname);
                    }else{
                        username = AtviUtility.checkNull(gamer.salutation) + ' ' + AtviUtility.checkNull(gamer.lastname);
                    }
                    ATVI_GetClanLeaderInfo clanLeader =new ATVI_GetClanLeaderInfo();
                    clanLeader.vContactid = gamer.id;
                    clanLeader.isClanleader();
                    System.debug('is Clan Leader ...' + clanLeader.isClnLdr);
                    if(clanLeader!=null) {            
                        gamer.isClan_Leader__c = clanLeader.isClnLdr;
                    }
                    isClanLead = gamer.isClan_Leader__c;
                    userLastName = gamer.lastname;
                }                
                system.debug('Username is : ' + username);  //  debugging statement
        }else{
            system.debug('sfContactId is null');            //  debugging statement
            username = 'Gamer!';
        }
                      
        return null;
    }        
    
    /*
        @name           setUsername
        @description    sets the name of the logged in customer portal user
    */
    private void setUsername(){
        String userlocale   = UserInfo.getLocale();
        if(userlocale.contains('US')){
            username    = 'Hi ' + UserInfo.getFirstName();
        }else{
            username    = 'Hello ' + UserInfo.getLastName();
        }
        
        system.debug('Locale is: ' + userlocale + '. Username is :' + username);
    }
    
    /**
    *   @name           getUserCountry 
    *   @description    Used to determine the country selected and show the corresponding flag
    **/ 
    private String getUserCountry(){
        
        string strCountry   = 'en_US';        
                
        if(thisUser != null){
            if(sfContactId != null){
                system.debug('User is a customer portal user hence this page is being viewed in the customer portal');                                  
                strCountry  = UserInfo.getLocale();
                system.debug('User Locale is ' + strCountry);
                //  Check that User's locale should be from the list of 8 supported ones, otherwise set to english
                if( !strCountry.contains('en_US') && !strCountry.contains('en_GB') && !strCountry.contains('de_DE') && !strCountry.contains('fr_FR') && !strCountry.contains('it_IT') && !strCountry.contains('es_ES') && !strCountry.contains('en_AU') && !strCountry.contains('nl_NL') && !strCountry.contains('fr_BE') && !strCountry.contains('fr_LU') && !strCountry.contains('sv_SE'))
                    strCountry  = 'en_US'; 
                //country   = userLocale.substring(userLocale.indexOf('_')+1);                 
            }
        }
        
        system.debug('Detected country : ' + strCountry);     
        return strCountry;                   
    }
    
    /**
    *   @name           refreshLanguage 
    *   @description    gets called when user clicks on a particluar flag and the page refreshes with the new language
    **/    
    public PageReference refreshLanguage(){               
        if(selectedLang != null && selectedLang != ''){
                userLanguage    = selectedLang;
        }else{
                userLanguage    = 'en_us';
        }
        
        langCookie = new Cookie('uLang', userLanguage, null, -1, false);
        ApexPages.currentPage().setCookies(new Cookie[]{langCookie});
        userLanguage = langCookie.getValue();
        return null;
    }
    
    /**
    *   @name           getIsloggedIn 
    *   @description    returns true if the current user is a customer portal user. Means he is logged into customer portal
    **/ 
    public boolean getIsloggedIn(){
        boolean isLoggedIn  = false;
       
        system.debug('UserInfo.getUserType()='+UserInfo.getUserType());
        if(UserInfo.getUserType()=='CSPLitePortal') isLoggedIn=true;
        
        return isLoggedIn;
    }
       
    /*
        @name           getDisplayLanguage
        @description    Gets the language of the logged in user. If public site, displays the selected language
    */
    public String getDisplayLanguage(){
        String lang = 'English';
        system.debug('$$ User Lang set for display = ' + userLanguage);     //  debugging statement
        if(userLanguage != null && userLanguage != ''){
                
                lang    = AtviUtility.SearchLabelfromKey(userLanguage, true);
                if( (lang == null) || (lang == '') ) 
                    lang    = AtviUtility.SearchLabelfromKey(userLanguage.substring(0,userLanguage.indexOf('_')), true);
                system.debug('$$ Display Language = ' + lang);              //  debugging statement
        }
        
        //  if no lanugage is selected, then english is set as the default language
        if( (lang == null) || (lang == '') ) 
            lang    = 'English';
        
        return lang;
    } 
         
    /*
     * Start of code for the predictive search components 
     * added by sumankrishnasaha
     * 01-Mar-12
     */
    Boolean
        is_cod_mw3              = false,
        is_cod_blackops         = false,
        is_goldeneye            = false,
        is_transformers         = false,
        is_spiderman            = false,
        is_xmen                 = false,
        is_prototype            = false;
    String
        selected_cod_mw3        = '',
        selected_cod_blackops   = '',
        selected_goldeneye      = '',
        selected_transformers   = '',
        selected_spiderman      = '',
        selected_xmen           = '',
        selected_prototype      = '';
        
    private void makeFalse() {
        is_cod_mw3 = false;
        is_cod_blackops = false;
        is_goldeneye = false;
        is_transformers = false;
        is_spiderman = false;
        is_xmen = false;
        is_prototype = false;
        
        selected_cod_mw3 = '';
        selected_cod_blackops = '';
        selected_goldeneye = '';
        selected_transformers = '';
        selected_spiderman = '';
        selected_xmen = '';
        selected_prototype = '';
    }
    
    public PageReference cod_mw3() {
        /*
         * make all other games false
         */
        makeFalse();
        /*
         * make cod_mw3 true
         */
        is_cod_mw3 = true;
        selected_cod_mw3 = 'selected';
        return null;
    }
    
    public Boolean getIs_cod_mw3 () {
        return is_cod_mw3;
    }
    
    public String getSelected_cod_mw3 () {
        return selected_cod_mw3 ;
    }
    
    public PageReference cod_blackops() {
        /*
         * make all other games false
         */
        makeFalse();
        /*
         * make cod_blackopstrue
         */
        is_cod_blackops = true;
        selected_cod_blackops = 'selected';
        return null;
    }
    
    public Boolean getIs_cod_blackops () {
        return is_cod_blackops;
    }
    
    public String getSelected_cod_blackops () {
        return selected_cod_blackops ;
    }
    
    public PageReference goldeneye() {
        /*
         * make all other games false
         */
        makeFalse();
        /*
         * make goldeneye true
         */
        is_goldeneye = true;
        selected_goldeneye = 'selected';
        return null;
    }
    
    public Boolean getIs_goldeneye () {
        return is_goldeneye;
    }
    
    public String getSelected_goldeneye () {
        return selected_goldeneye;
    }
    
    public PageReference transformers() {
        /*
         * make all other games false
         */
        makeFalse();
        /*
         * make cod_mw3 true
         */
        is_transformers = true;
        selected_transformers = 'selected';
        return null;
    }
    public Boolean getIs_transformers () {
        return is_transformers;
    }
    public String getSelected_transformers () {
        return selected_transformers ;
    }
    
    public PageReference spiderman() {
        /*
         * make all other games false
         */
        makeFalse();
        /*
         * make spiderman true
         */
        is_spiderman = true;
        selected_spiderman = 'selected';
        return null;
    }
    public Boolean getIs_spiderman () {
        return is_spiderman;
    }
    public String getSelected_spiderman () {
        return selected_spiderman ;
    }
    
    public PageReference xmen() {
        /*
         * make all other games false
         */
        makeFalse();
        /*
         * make xmen true
         */
        is_xmen = true;
        selected_xmen = 'selected';
        return null;
    }
    public Boolean getIs_xmen() {
        return is_xmen;
    }
    public String getSelected_xmen () {
        return selected_xmen ;
    }
    
    public PageReference prototype() {
        /*
         * make all other games false
         */
        makeFalse();
        /*
         * make prototype true
         */
        is_prototype = true;
        selected_prototype = 'selected';
        return null;
    }
    public Boolean getIs_prototype () {
        return is_prototype;
    }
    public String getSelected_prototype() {
        return selected_prototype;
    }
    /*End of code for the predictive search components*/    
    
    /*getBranding - determines which game the gamer clicked and accordingly determines which bg image is to be shown on webpages
    * 06.Mar.2012 A. PAL    
    */
    public String getBranding(){
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('clickedOn');
        if(tempCookie != null)
            branding = tempCookie.getValue();
        system.debug('Branding being returned:'+branding);
        return branding;                
    }
    
    /**language code used to construct the Banning Policy article url displayed on the home page
    ** created 03.29.2012 - abpal
    **/
    public String bannedFaqLanguage{
        set;
        get{
            string bannedLang;
            if(userLanguage=='en_US'||userLanguage=='en_GB')
                bannedLang='en_US';
            else if (userLanguage=='de_DE')
                bannedLang='de';
            else if (userLanguage=='fr_FR')
                bannedLang='fr';
            else if (userLanguage=='es_ES')
                bannedLang='es';
            else if (userLanguage=='it_IT')
                bannedLang='it';
            else if (userLanguage=='es_ES')
                bannedLang='es';
            else if (userLanguage=='nl_NL')
                bannedLang='nl_NL';
            else if (userLanguage=='sv_SE')
                bannedLang='se';  
            else if (userLanguage=='pt_BR')
                bannedLang='pt_BR';             
                
            system.debug('bannedFaqLanguage being returned:'+bannedLang);
            return bannedLang;
        }       
    }
    
    //a the latest active chatNews object is selected and set here
    public void setupCNews(){
        list<ChatNews__c> cnL = [Select c.news__c,c.Dutch_News__c,c.French_News__c,c.German_News__c,c.Italian_News__c,c.Portuguese_News__c,c.Spanish_News__c,c.Swedish_News__c, c.CreatedDate, c.Active__c From ChatNews__c c where active__c = true order by createddate limit 1];
        if(cnL != null && cnL.size()>0){
            cNews= cnL[0];
        }
        else 
         cNews= new ChatNews__c();  
   }  
    
}