public class ATVIHomepageModules {
	public ATVIHomepageModules(ATVICustomerPortalController cont) {
		
	}
	public boolean isBlankOrNull(String s){return s == null || s == ''; }
	
	public Component.Apex.OutputPanel getHomepageModules() {
        Component.Apex.OutputPanel dynOutPanel= new Component.Apex.OutputPanel();
        dynOutPanel.styleClass = 'homepage-module-container row';
        dynOutPanel.layout = 'block';
        Integer i = 1;

		for(Homepage_Module__c module : [SELECT HTML__c, Custom_Module_Style_Class__c 
										 FROM Homepage_Module__c 
										 WHERE IsActive__c = TRUE 
										 ORDER BY Location__c ASC]) {
			Component.Apex.OutputPanel childPanel = new Component.Apex.OutputPanel();
			childPanel.layout = 'block';
			childPanel.styleClass = 'homepage-module col-sm-6 col-md-4';
			if(! isBlankOrNull(module.Custom_Module_Style_Class__c)){
				childPanel.styleClass += ' ' + module.Custom_Module_Style_Class__c;
			}
   			Component.Apex.OutputText dynOutputText = new Component.Apex.OutputText();
			dynOutputText.escape = false;
           	if(module.HTML__c.containsIgnoreCase('{!')){
           		dynOutputText.expressions.value = module.HTML__c;
           	}else{
           		dynOutputText.value = module.HTML__c;
			}
			childPanel.childComponents.add(dynOutputText);
           	dynOutPanel.childComponents.add(childPanel);
           	
           	if(Math.Mod(i, 2) == 0){
           		Component.Apex.OutputPanel everyTwo = new Component.Apex.OutputPanel();
		        everyTwo.layout = 'block'; 
		        everyTwo.styleClass = 'cf hidden-xs hidden-md hidden-lg';
           		dynOutPanel.childComponents.add(everyTwo);
           	}
           	if(Math.Mod(i, 3) == 0){
           		Component.Apex.OutputPanel everyThree = new Component.Apex.OutputPanel();
		        everyThree.layout = 'block'; 
		        everyThree.styleClass = 'cf hidden-xs hidden-sm';
           		dynOutPanel.childComponents.add(everyThree);
           	}
           	i++;
    	}
        
        return dynOutPanel;
    }
}