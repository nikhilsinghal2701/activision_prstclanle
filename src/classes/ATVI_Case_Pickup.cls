/*
Modifies by sandeep:04/30/2014:lines 12,26,31-36 :removed page reference 
:to fix the "assign cases2" button on case list view

*/

public with sharing class ATVI_Case_Pickup{
    ApexPages.standardSetController pSsetCont; //private standard set controller 
    public ATVI_Case_Pickup(ApexPages.standardSetController sSetCont){
        pSsetCont = sSetCont;
    }
    public void pickupCases(){
        Case[] casesToPickup = new Case[]{};
        Case[] casesInSet = (Case[]) pSsetCont.getRecords();
        Case[] casesInSetWithFields = [SELECT Id, OwnerId, LastModifiedDate, Status FROM Case 
                                       WHERE Id IN :casesInSet AND 
                                           OwnerId != :UserInfo.getUserId() AND
                                           Status IN ('New','Open','Updated') 
                                       ORDER BY LastModifiedDate ASC];
        for(Case c : casesInSetWIthFields){
            String caseOwnerId = c.OwnerId;
            if(! caseOwnerId.startsWith('005')) //not owned by another user
            {
                c.OwnerId = UserInfo.getUserId();
                casesToPickup.add(c);
            }
            if(casesToPickup.size() == 3) break;
        }
        update casesToPickup;
    }
}