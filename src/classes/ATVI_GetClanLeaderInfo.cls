public class ATVI_GetClanLeaderInfo {
    public id vContactid;
    public boolean isProd;
    public string ghProdEndpoint = 'http://ghosts-teams-webzone.prod.demonware.net/ghosts/shared/teams/';
    public string ghDevEndpoint ='http://ghosts-shared-dev-webzone.demonware.net/ghosts/shared/teams/';
    public string ghEndpoint;    
    public string awDevEndpoint = 'https://aw-dev-api.dev.beachheadstudio.com';
    public string awProdEndpoint = 'https://prod-api.prod.beachheadstudio.com';
    public string awEndpoint;
    public string platform;
    public string ucdid;
    public string Clanid;
    public string awPcClanEndpoint ;
    public string awXbClanEndpoint ;
    public string awXb1ClanEndpoint;
    public string awPs3ClanEndpoint;
    public string awPs4ClanEndpoint;
    public boolean isClnLdr = false;
    public list<Contact> gamer ;
    DwProxy.service serv = new DwProxy.service();
    public membershipResponse clanMembers ;
    public clanidResponse clanResObj;
    public class clanidResponse{
        public teamcls team;
    }
    public class teamcls{
        public string teamid;
    }
    public class membershipResponse{
        public clanMember[] members ;
        public integer count;
    }
    public void isleader(){
        
        system.debug('clanMembers.count>>>'+clanMembers.count);
        
        if(clanMembers.count>1 && isClnLdr == false)
            for(integer i=0;i<clanMembers.count;i++){
                if(ucdid==clanMembers.members[i].userid.split('-')[1] && clanMembers.members[i].membershipType==2) isClnLdr=true;                     
            }
    }
    public class clanMember{
        public string userName ;
        public string userId ;
        public integer membershipType ;
        //public string platform ;
        //public string network ;
        //public string image ;
        //public long kills ;
        //public long deaths ;
        //public long wins ;
        //public long losses ;
    }
    public ATVI_GetClanLeaderInfo(){
        isProd = UserInfo.getOrganizationId() == '00DU0000000HMgwMAG';
        isProd=true;
        awEndpoint = isProd ? awProdEndpoint : awDevEndpoint;
        ghEndpoint = isProd ? ghProdEndpoint : ghDevEndpoint;
        awPcClanEndpoint = isProd ?'https://aw-pc-titleservices.prod.demonware.net/aw/pc/teams/':'https://aw-pc-titleservices.dev.demonware.net/aw/pc/teams/'; 
        awXbClanEndpoint = isProd ?'https://aw-xbox-titleservices.prod.demonware.net/aw/xbox/teams/':'https://aw-xbox-titleservices.dev.demonware.net/aw/xbox/teams/';
        awXb1ClanEndpoint = isProd ?'https://aw-xb1-titleservices.prod.demonware.net/aw/xb1/teams/':'https://aw-xb1-titleservices.dev.demonware.net/aw/xb1/teams/';
        awPs3ClanEndpoint = isProd ?'https://aw-ps3-titleservices.prod.demonware.net/aw/ps3/teams/':'https://aw-ps3-titleservices.dev.demonware.net/aw/ps3/teams/';
        awPs4ClanEndpoint = isProd ?'https://aw-ps4-titleservices.prod.demonware.net/aw/ps4/teams/':'https://aw-ps4-titleservices.dev.demonware.net/aw/ps4/teams/';
    }
    public void isClanleader(){
        DwProxy.service svc = new DwProxy.service();
        String sesssIon_token ='',query='';
        string response;
        boolean flgExecOnce=true;
        for(Multiplayer_Account__c mpAcc : [SELECT Platform__c,Contact__r.clan_id__c,Contact__r.account.name,Contact__r.ucdid__c FROM Multiplayer_Account__c WHERE 
                                            Contact__c =:vContactid])
        {
            ucdid = mpAcc.Contact__r.ucdid__c==null?mpAcc.Contact__r.account.name:mpAcc.Contact__r.ucdid__c;
            try{
                if(flgExecOnce){
                    serv.timeout_x = 120000;
                    query =ghEndpoint +mpAcc.Contact__r.clan_id__c+'/members';
                    //system.debug('query ?????'+query);
                    response = serv.passEncryptedGetData(query);
                    
                    clanMembers = (membershipResponse) JSON.deserialize(response, membershipResponse.class);
                    system.debug('response >>>>'+clanMembers); 
                    flgExecOnce=false;
                    isleader();
                    system.debug('isClnLdr 1>>'+ucdid+'>>'+isClnLdr); 
                }
            }
            catch(exception e){
                system.debug('Error in Clan isleader for Ghosts >>>' +e); 
            }
            
            // once a clan leader always a clan leader
            if(isClnLdr == true) break;
            
            try{
                if(!test.isrunningtest())sesssIon_token = svc.generateAWsessionId(Label.AWclanUcdid);
                string qSuffix = '@headers-exist@@usecsnetworkcreds#true@bh-network#'+mpAcc.platform__c.toLowerCase();
                serv.timeout_x = 120000;
                query =awEndpoint +'/aw/clans/player/'+mpAcc.Contact__r.ucdid__c+'?session_token='+sesssIon_token+qSuffix;
                //system.debug('query ?????'+query);
                response = serv.passEncryptedGetData(query);
                //system.debug('response >>>>'+response); 
                clanResObj=(clanidResponse) JSON.deserialize(response, clanidResponse.class);
                system.debug('response >>>>'+clanResObj); 
            }
            catch(exception e){
                system.debug('Error in Clan isleader for aw >>>' +e);
            }                 
            //string awClanEndpoint = (mpAcc.platform__c.toLowerCase()=='ps4'?awPs4ClanEndpoint :(mpAcc.platform__c.toLowerCase()=='ps3'? awPs3ClanEndpoint:(mpAcc.platform__c.toLowerCase()=='pc'? awPcClanEndpoint:(mpAcc.platform__c.toLowerCase()=='xbl'? awXb1ClanEndpoint: awXbClanEndpoint))));               
            if(clanResObj!=null&&clanResObj.team!=null&&clanResObj.team.teamid!=null){
                if(mpAcc.platform__c.toLowerCase()=='psn'){
                    awClanQuery(awPs4ClanEndpoint);
                    if(isClnLdr == true) break;
                    awClanQuery(awPs3ClanEndpoint);
                    if(isClnLdr == true) break;
                }
                else if(mpAcc.platform__c.toLowerCase()=='xbl'){  
                    awClanQuery(awXbClanEndpoint);
                    if(isClnLdr == true) break;
                    awClanQuery(awXbClanEndpoint);
                    if(isClnLdr == true) break;
                }
                else if(mpAcc.platform__c.toLowerCase()=='steam'){
                    awClanQuery(awPcClanEndpoint);
                    if(isClnLdr == true) break;
                }
            }           
        }      
    }
     
    public void awClanQuery(String awClanEndpoint){
        string qSuffix = '@headers-exist@@usecsnetworkcreds#true';
        serv.timeout_x = 120000;
        String query =awClanEndpoint+clanResObj.team.teamid+'/members'+qSuffix ;
        system.debug('query ?????'+query);
        String response = serv.passEncryptedGetData(query);
        system.debug('response1--->'+response);
        clanMembers = (membershipResponse) JSON.deserialize(response, membershipResponse.class);
        system.debug('awcresponse >>>>'+clanMembers); 
        isleader();
        system.debug('isClnLdr 2>>'+ucdid+'>>'+isClnLdr); 
        
    }
    
    
}