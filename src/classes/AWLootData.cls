public class AWLootData {
	public String EnglishName {get;set;}
	public String ItemGroup {get;set;}
	public Boolean HasStats {get;set;}
	public Boolean IsExpired {get;set;}
	public String ItemSource {get;set;}
	public String Rarity {get;set;}
	public WeaponStats WeaponStats{get;set;}
	public Class WeaponStats{
		public Integer Accuracy {get;set;}
		public Integer AccuracyDelta {get;set;}
		public Integer Ammo {get;set;}
		public Integer AmmoDelta {get;set;}
		public Integer Damage {get;set;}
		public Integer DamageDelta {get;set;}
		public Integer FireRate {get;set;}
		public Integer FireRateDelta {get;set;}
		public Integer Handling {get;set;}
		public Integer HandlingDelta {get;set;}
		public Integer Magazine {get;set;}
		public Integer MagazineDelta {get;set;}
		public Integer Range {get;set;}
		public Integer RangeDelta {get;set;}		
	}
	public Integer EventId {get;set;}
}