public with sharing class Alert {
    
    public list<alert__c> alerts {get;set;}
    public boolean show {get;set;}
    public string brand{get;set;}
    
    /*Used to set the language of the Apex page acccording to the user's choice*/    
    public String lang {get;set;}
    public void getUserLanguage()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            system.debug('tempCookie not null');
            lang = tempCookie.getValue();            
        }else{
            system.debug('tempCookie is null');
            lang='en_USDefault';
        }
    }
    
    /*Checks cookies if it should show. Expires in 24 hours. Can be edited.
	Cookie is set in js in page/component*/
    public void getShowAlert(){
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('showAlert');
        if(tempCookie != null){
            show=false;           
        }else{
            show=true;
        }
    }

    /*Constructor.  Gets Language, and checks if should be shown*/
    public Alert(){
        show=true;
        getUserLanguage();
        getShowAlert();
        brand = ApexPages.currentPage().getParameters().get('clickedOn');
        
        if (brand==null || brand==''){
	        alerts=[select id,name,English__c,Spanish__c,German__c,Dutch__c,Portuguese__c,Italian__c,French__c,Swedish__c,
	                TheLink__c,
	                TheLink_Dutch__c,
	                TheLink_French__c,
	                TheLink_German__c,
	                TheLink_Italian__c,
	                TheLink_Portuguese__c,
	                TheLink_Spanish__c,
	                TheLink_Swedish__c
	                from Alert__c where isActive__c=true
	                Order by CreatedDate Desc
	                ];
        }
        else{
        	alerts=[select id,name,English__c,Spanish__c,German__c,Dutch__c,Portuguese__c,Italian__c,French__c,Swedish__c,
	                TheLink__c,
	                TheLink_Dutch__c,
	                TheLink_French__c,
	                TheLink_German__c,
	                TheLink_Italian__c,
	                TheLink_Portuguese__c,
	                TheLink_Spanish__c,
	                TheLink_Swedish__c,
	                Category__c
	                from Alert__c where isActive__c=true AND Category__c INCLUDES(:brand) Order by CreatedDate Desc];
        }
        
        /*If there's no active alerts, Do not display*/
        if (alerts.size()==0)
        {
                show=false;
        }
    }
}