public class AmbassadorRedirectController {

    public PageReference redirect() {
        //chck if user is ambassador
        User u = [SELECT ContactId, Contact.IsAmbassador__c FROM User WHERE Id = :UserInfo.getUserId()];
        if (u.Contact.IsAmbassador__c) {
            //move to settings - 'https://test.salesforce.com/services/auth/sso/00D19000000DXM0EAO/AmbassadorAuthProvider'
            return new PageReference(System.Label.AmbassadorOrgSSOURL);
        } else {
            return Page.AMB_Registration;
        }
    }
}