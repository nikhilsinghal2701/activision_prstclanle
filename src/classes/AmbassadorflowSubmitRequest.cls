global class AmbassadorflowSubmitRequest implements Process.Plugin { 

// The main method to be implemented. The Flow calls this at runtime.
global Process.PluginResult invoke(Process.PluginRequest request) { 
        // Get the subject of the Chatter post from the flow
        id AmbassadorId = (id) request.inputParameters.get('AmbassadorId');
        
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
    req1.setComments('Submitting request for approval.');
    req1.setObjectId(AmbassadorId);
    Approval.ProcessResult ApprovalResult = Approval.process(req1);

        // return to Flow
        Map<String,Object> result = new Map<String,Object>(); 
        return new Process.PluginResult(result); 
    } 

    // Returns the describe information for the interface
    global Process.PluginDescribeResult describe() { 
        Process.PluginDescribeResult result = new Process.PluginDescribeResult(); 
        result.Name = 'Submit For Approval';
        result.Tag = 'Ambassador';
        result.inputParameters = new 
           List<Process.PluginDescribeResult.InputParameter>{ 
          new Process.PluginDescribeResult.InputParameter('AmbassadorId', Process.PluginDescribeResult.ParameterType.STRING, true) 
            }; 
        result.outputParameters = new 
           List<Process.PluginDescribeResult.OutputParameter>{ }; 
        return result; 
    }
}