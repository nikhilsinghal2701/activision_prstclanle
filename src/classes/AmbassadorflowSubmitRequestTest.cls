@isTest
                        
private class AmbassadorflowSubmitRequestTest {

    static testmethod void flowTests() {
      
        AmbassadorflowSubmitRequest plugin = new AmbassadorflowSubmitRequest();
        Map<String,Object> inputParams = new Map<String,Object>();

       Ambassador_Application__c AmbObj = new Ambassador_Application__c();
       AmbObj.email__c = 'test@test.com';
       insert AmbObj ;
        InputParams.put('AmbassadorId',AmbObj.id);

        Process.PluginRequest request = new Process.PluginRequest(inputParams);           
        plugin.describe();
        plugin.invoke(request);
    } 
}