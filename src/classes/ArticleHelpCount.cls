public class ArticleHelpCount{
    public ArticleHelpCount(){} //standard constructor
    public ArticleHelpCount(ApexPages.StandardController cont){
        theArtId = cont.getId();
    }
    public String theArtId;
    public void setTheArtId(String aid){theArtId = aid;}
    public String getTheArtId(){return theArtId;}

    public Integer ArticleHelpCount; 
    public void setArticleHelpCount(Integer i){ArticleHelpCount = i;}
    public Integer getArticleHelpCount(){
        ArticleHelpCount = 0;
        try{
            if(theArtId != null){
                Id aId = (Id) theArtId;
                system.debug('aid == '+aId);
                for(PKB_Article_Solve_Tracking__c count : [SELECT Article_Solve_Count__c FROM PKB_Article_Solve_Tracking__c WHERE Name = :aId LIMIT 1]){
                    ArticleHelpCount = integer.valueOf(count.Article_Solve_Count__c);
                }
            }
        }catch(Exception e){
        }
        return ArticleHelpCount;
    }
}