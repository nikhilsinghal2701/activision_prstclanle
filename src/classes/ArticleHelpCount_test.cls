@IsTest
public class ArticleHelpCount_test{
    public static testMethod void test1(){
        ArticleHelpCount tstCls = new ArticleHelpCount();
        user u = [SELECT Id FROM User LIMIT 1]; //just need any random id
        system.assertEquals(null,tstCls.getTheArtId());
        system.assertEquals(0,tstCls.getArticleHelpCount());
        tstCls.setTheArtId(u.Id);
        system.assertEquals(u.Id,tstCls.getTheArtId());
        insert new PKB_Article_Solve_Tracking__c(Name = u.Id, Article_Solve_Count__c = 13);
        system.assertEquals(13, tstCls.getArticleHelpCount());
        tstCls.setArticleHelpCount(333);
        system.assertEquals(333, tstCls.ArticleHelpCount);
        tstCls = new ArticleHelpCount(new ApexPages.standardController(new Account()));
    }
}