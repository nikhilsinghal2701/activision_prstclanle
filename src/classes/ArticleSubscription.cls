public class ArticleSubscription {
    public String KAVid{get;set;}
    public Boolean isSubscribed{get;set;}
    public list<article_subscription__c> artsub{get;set;}
    public FAQ__kav fk{get;set;} 
    public user thisUser{get;set;}
    public Boolean showconfirm{get;set;}
    
    public ArticleSubscription(ApexPages.StandardController std){
        showconfirm=false;   
        fk = (faq__kav)std.getRecord();
        thisUser = [SELECT Id, Contact.Id from User 
                    WHERE Id =: UserInfo.getUserId()];
    
        artsub = [select id from article_subscription__c 
                  where contact__c=:thisUser.contact.id
                  and Kav_id__c=:fk.id];
        if(artsub.size()>0){
            isSubscribed=true;
        }
        
    }
    
    public ArticleSubscription(){
       
    }
    public pageReference showConfirm(){
        showconfirm=true;
        return null;
    }
    
    public pageReference subscribe(){
        Article_Subscription__c sub = new Article_Subscription__c();
        sub.KAV_ID__C = fk.id;
        sub.contact__c = thisUser.contact.id;
        insert sub;
        isSubscribed = true;
        showconfirm = false;
        return null;
    }
    
    public pageReference unsubscribe(){
        delete artsub[0];
        isSubscribed=false;
        return null;
    }
}