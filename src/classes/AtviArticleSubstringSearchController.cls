/*******************************************************************************
 * Name         -   AtviArticleSubstringSearchController
/**
* @author           Suman
* @date             08-FEB-2012
* @description      This controller returns a list of Knowledge Articles.
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Suman                     08-FEB-2012             Created
* ---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                     Review Comments
* --------                                          ------------                    --------------- 
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                     Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
public without sharing class AtviArticleSubstringSearchController {
    //Constructor for pkb_Controller class extension
    public AtviArticleSubstringSearchController(pkb_Controller controller) {
        List<KnowledgeArticleVersion> knowledgeArticleList = new List<KnowledgeArticleVersion>();
        //Query the knowlegdearticlelist and return the knowledgearticletitle
        knowledgeArticleList = [SELECT Title from KnowledgeArticleVersion 
                                WHERE PublishStatus = 'online' AND 
                                Language = 'en_US' limit 100];
        System.debug('knowledgeArticleList.size() = ' + knowledgeArticleList.size());
        knowledgeArticleTitle = new String[0];
        for(KnowledgeArticleVersion KnowledgeArticleVersionObject : knowledgeArticleList) {
          knowledgeArticleTitle.add(String.escapeSingleQuotes(KnowledgeArticleVersionObject.Title));
        }
    }
    //Constructor for standardcontroller extension
    public AtviArticleSubstringSearchController(ApexPages.StandardController controller) {
        List<KnowledgeArticleVersion> knowledgeArticleList = new List<KnowledgeArticleVersion>();
        //Query the knowlegdearticlelist and return the knowledgearticletitle
        knowledgeArticleList = [SELECT Title from KnowledgeArticleVersion 
                                WHERE PublishStatus = 'online' AND 
                                Language = 'en_US' limit 100];
        System.debug('knowledgeArticleList.size() = ' + knowledgeArticleList.size());
        knowledgeArticleTitle = new String[0];
        for (KnowledgeArticleVersion KnowledgeArticleVersionObject : knowledgeArticleList) {
          knowledgeArticleTitle.add(String.escapeSingleQuotes(KnowledgeArticleVersionObject.Title));
        }
        setupFilteredLanguageResults();
    }
    
    public string[] knowledgeArticleTitle {set;get;}
    //No argument constructor
    public AtviArticleSubstringSearchController() {
        List<KnowledgeArticleVersion> knowledgeArticleList = [SELECT Title from KnowledgeArticleVersion 
                                                                WHERE PublishStatus = 'online' AND 
                                                                Language = 'en_US' limit 100];
        knowledgeArticleTitle = new String[0];
        for (KnowledgeArticleVersion kav : knowledgeArticleList) {
          knowledgeArticleTitle.add(String.escapeSingleQuotes(kav.Title));
        }
    }

    public string lang {get; set;}
    public String[] getLocalizedArticleTitles(){
        try{
            knowledgeArticleTitle = new String[]{};
            for(KnowledgeArticleVersion kav : database.query('SELECT Title from KnowledgeArticleVersion WHERE '+
                                                  'PublishStatus = \'online\' AND '+
                                                  'Language = \''+convertLangFromIsoToSFDC(lang)+'\' LIMIT 100'))
            {
                knowledgeArticleTitle.add(String.escapeSingleQuotes(kav.Title));
            }
            system.debug('local knowledgeArticleTitle.size() == '+knowledgeArticleTitle.size());
            return knowledgeArticleTitle;
        }catch(Exception e){
            system.debug(e);
            return new string[]{};
        }
    }

    public String[] localizedFilteredArticleTitles {get; set;}
    public void setupFilteredLanguageResults(){
        localizedFilteredArticleTitles = new string[]{};
        try{
            //find the category to filter the query by
            //setup data
            String selectedCategory;
            String cookieVal;
            Cookie aCookie = ApexPages.currentPage().getCookies().get('clickedOn');
            if(aCookie != null){
                cookieVal = aCookie.getValue();
            }
            String gtitle = ApexPages.currentPage().getParameters().get('clickedOn');
                
            //translate 
            if(cookieVal=='MW3'){
               selectedCategory = 'Call_of_Duty_Modern_Warfare_3';  
            }else if (cookieVal == 'Skylanders_Giants'){
               selectedCategory = 'Skylanders_Giants';
            }else if (cookieVal == 'Black_Ops'){
              selectedCategory = 'Call_of_Duty_Black_Ops';  
            }else if (cookieVal == 'Black_Ops_II'){
               selectedCategory = 'Call_of_Duty_Black_Ops_II'; 
            }else if (cookieVal == 'Transformers'){
               selectedCategory = 'Transformers_Dark_of_the_Moon';
            }else if (cookieVal == 'Cabela_s_Dangerous_Hunts_2013'){
               selectedCategory = 'Cabela_s_Dangerous_Hunts_2013';
            }else if (cookieVal == '007'){
               selectedCategory = 'James_Bond_Goldeneye_007_Reloaded';
            }else if (cookieVal == 'Cabelas'){
               selectedCategory = 'Cabela_s_Big_Game_Hunter_2012';
            }else if (cookieVal == 'Skylanders_Spyro_s_Adventure'){
               selectedCategory = 'Skylanders_Spyro_s_Adventure';
            }else{ //no translation
                if(gTitle == null && cookieVal == null){
                    selectedCategory = 'All__c';
                }else{
                    if(gtitle == null) gtitle = 'All';
                    boolean existingDataCategory = false;
                    list<Schema.DataCategoryGroupSObjectTypePair> pairs = new list<Schema.DataCategoryGroupSObjectTypePair>();
            
                    Schema.DataCategoryGroupSObjectTypePair thisPair = new Schema.DataCategoryGroupSObjectTypePair();
                    thisPair.sObject = 'KnowledgeArticleVersion';
                    thisPair.dataCategoryGroupName = 'Game_Title';
                    pairs.add(thisPair);        
                    
                    for(Schema.DescribeDataCategoryGroupStructureResult result : Schema.describeDataCategoryGroupStructures(pairs,false)){
                        for(Schema.DataCategory self : result.getTopCategories()){
                            for(Schema.DataCategory child : self.getChildCategories()){
                                if(child.getName().containsIgnoreCase(cookieVal)){ 
                                    selectedCategory = cookieVal;
                                    break;
                                }
                                
                                if(child.getName().containsIgnoreCase(gTitle)){
                                    selectedCategory = gTitle;
                                    break;
                                }
                            }
                            if(selectedCategory != null) break;
                        }
                        if(selectedCategory != null) break;
                    }
                }
            }

            String artQry = 'SELECT title FROM faq__kav WHERE publishstatus = \'Online\' and language = \'';
            artQry += convertLangFromIsoToSFDC(pkb_Controller.selectedLanguage)+'\' WITH DATA CATEGORY Game_Title__c AT ';
            artQry += '('+selectedCategory+'__c)';
            artQry += ' LIMIT 100';
            system.debug('artQry == '+artQry);

            for(FAQ__kav kav : database.query(artQry)){
              localizedFilteredArticleTitles.add(String.escapeSingleQuotes(kav.Title));
            }
        }catch(Exception e){
            system.debug('ERROR == '+e);
        }
    }
    
    public static string convertLangFromIsoToSFDC(String pUlang){
        if(pUlang == null || pUlang == '') return 'en_US';
        if(pUlang.toLowerCase().contains('de')) return 'de';
        if(pUlang.toLowerCase().contains('fr')) return 'fr';
        if(pUlang.toLowerCase().contains('it')) return 'it';
        if(pUlang.toLowerCase().contains('es')) return 'es';
        return 'en_US';
    }

}