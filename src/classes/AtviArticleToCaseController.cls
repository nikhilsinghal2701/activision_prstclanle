/**
* @author           Mohith
* @date             04-APR-2012
* @description      This controller fetches the Article details.
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Mohith                                        04-APR-2012                             Created
* A. Pal                                        08-APR-2012                             Added post authentication component rendering.
                                                                                                        Added method to insert article rating given by 
                                                                                                        an authenticated gamer into the VOTE table.
* ---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                                                                      Review Date                                             Review Comments
* --------                                                                                      ------------                                    --------------- 
* A. Pal(Deloitte Senior Consultant)                            19-APR-2012                                             Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
public with sharing class AtviArticleToCaseController {
    
    public AtviArticleToCaseController(ApexPages.StandardController sc) {}
    public static integer len{get;set;}
    public static PageReference saveArticleTitle() {
        String userSessionId = UserInfo.getSessionId();
        String userLanguage = UserInfo.getLanguage();
        System.debug('User language='+userLanguage);
        String articleTitleId = ApexPages.currentPage().getParameters().get('Id');
        String articleTitle='';
        
        list<KnowledgeArticleVersion> kaList = [SELECT Id, Title, Language FROM 
                                                                                        KnowledgeArticleVersion WHERE 
                                                                                        KnowledgeArticleId =: articleTitleId 
                                                                                        AND publishstatus = 'online'];
        

        if(kaList.size()>0) {
            for(KnowledgeArticleVersion ka : kaList) {
                if(ka.Language == userLanguage) {      
                    articleTitle = ka.Title;
                }           
            }
        } 
        
        System.debug('articleTitle = '+articleTitle);
        Cookie aCookie = ApexPages.currentPage().getCookies().get(userSessionId);
        String articlesVisited;
        
        if(aCookie != null) {
            articlesVisited = aCookie.getValue();
            if(articlesVisited != null) {
                if(!articlesVisited.contains(articleTitle)) {
                    articlesVisited += ','+articleTitle;  
                }
            }else {
                articlesVisited = articleTitle;
            }   
            System.debug('articlesVisited = '+articlesVisited); 
        }else {
            articlesVisited = articleTitle;
        }
        
        aCookie = new Cookie(userSessionId,articlesVisited,null,-1,false);        
        ApexPages.currentPage().setCookies(new Cookie[]{aCookie});        
        return null;
    }    
    
    public boolean isPortalUser{
        get{
                if(UserInfo.getUserType().equalsIgnoreCase('CSPLitePortal')) return true;
                else return false;
        }
        set;
    }
    
    public String kaIdforRating{get;set;}
    public String voteType{get;set;}
    public PageReference rateAnArticle(){
        String userId = UserInfo.getUserId();
        system.debug('inside rateAnArticle');
        
        List<Vote> votesByThisUser = [SELECT Id, ParentId, Type, CreatedById 
                                                                  FROM Vote WHERE CreatedById =: userId AND 
                                                                  ParentId =: kaIdforRating];
        
        if(votesByThisUser.size()>0){
                try{
                        delete votesByThisUser;
                        system.debug('All votes by the user ['+userId+'] has been deleted');
                }catch(DMLException dmle){
                        system.debug(dmle.getMessage());
                }
        }else{
                        system.debug('No votes by the user ['+userId+']');
        }
                
                try {
                        Vote newVote = new Vote();
                        newVote.ParentId = kaIdforRating;
                        newVote.Type = voteType;
                        insert newVote;
                        system.debug('Article '+kaIdforRating+' has been rated '+voteType+' by the user '+userId);
                }catch(DMLException dmle) {
                        system.debug(dmle.getMessage());
                }               
                return null;            
    }    
}