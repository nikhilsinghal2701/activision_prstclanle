/**
* @author           Mohith
* @date             03-MAR-2012
* @description      This controller is used to create a case whenever user submits a case (web to case).
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Mohith					03-MAR-2012				Created
* A. Pal					01-MAY-2012		        Set Case Origin to "Web" in casecreate() method
* ---------------------------------------------------------------------------------------------------
* Review Log	:
*---------------------------------------------------------------------------------------------------
* Reviewer											Review Date						Review Comments
* --------											------------					--------------- 
* A. Pal(Deloitte Senior Consultant)				19-APR-2012						Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
public with sharing class AtviArticletoCaseForm { 
   public String EmailAddress {get; set;}
   public String Gamertag {get; set{System.debug('@@@@@@'+Gamertag);}}
   public String description;
   public String userLanguage;
   public String getdescription() {
   	return description;
   }
   public void setdescription(String s) {
   	description = s;
   }
   public String subject;
   public String getsubject() {
   	return subject;
   }
   public void setsubject(String s) {
   	subject = s;
   }
   public final static String GeneralSupportCaseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('General Support').getRecordTypeId();
   private User newuser;
 
   public AtviArticletoCaseForm(ApexPages.StandardController controller) {}
    //Method to set the user language from the language cookie 
    public String getUserLanguage()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
        	system.debug('tempCookie not null');
            userLanguage = tempCookie.getValue();            
        }else{
        	system.debug('tempCookie is null');
        	userLanguage = 'en_US';
        }
        return userLanguage;
    }
    
    public void setUserLanguage(String userLanguage)
    {
        this.userLanguage = userLanguage;
    }
 
 //Method to create case from a user 
 public PageReference casecreate()
 { 
    System.debug('@@@@@@'+subject);
    system.debug('@@@@'+description);//debug statement
    String userId=UserInfo.getUserId();
    newuser = [SELECT Id, FirstName, Contact.Id, Contact.Name, Contact.Email, 
    		   Contact.Phone FROM User WHERE Id =: UserInfo.getUserId()];                      
    System.debug('@@@@@@@'+userId);//Debug Statement    
    Id oContact = newuser.ContactId;
    System.debug(oContact);//Debug Statement 
    //Create a general support case
    if (oContact != null) {
        case oCase = new case();
        oCase.Origin='Web';//Added by A. Pal on 01-may-2012
        oCase.Description = description;
        oCase.Subject = subject;
        oCase.RecordTypeId = GeneralSupportCaseRecTypeId;
        system.debug('######'+oCase.Description);//Debug statements
        oCase.ContactId = oContact;
        oCase.W2C_Origin_Language__c=userLanguage;
        try {
        	insert oCase;
        }catch(Exception dEx) {}        
    }
	 PageReference message=System.Page.AtviCaseCreationConfirmation;
	 return message;
 }
}