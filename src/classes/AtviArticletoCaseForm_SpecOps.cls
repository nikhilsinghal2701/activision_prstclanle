public with sharing class AtviArticletoCaseForm_SpecOps {
    public String EmailAddress {get; set;}
    public String Gamertag {get; set;}
    public String description {get; set;}
    public String userLanguage;
    public String subject{get; set;}
    public String Rank{get; set;}
    public case invoiceStatement {get;set;}
    
    public String getdescription() {
    return description;
    }
    public void setdescription(String s) {
    description = s;
    }
    public String getsubject() {
    return subject;
    }
    public void setsubject(String s) {
    subject = s;
    }
    
    public final static String GeneralSupportCaseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('SpecOps Request').getRecordTypeId();
    private User newuser;
   
    public AtviArticletoCaseForm_SpecOps(ApexPages.StandardController controller) {
    invoiceStatement = new case();        
    }
    
    //Method to set the user language from the language cookie 
    public String getUserLanguage()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            system.debug('tempCookie not null');
            userLanguage = tempCookie.getValue();            
        }else{
            system.debug('tempCookie is null');
            userLanguage = 'en_US';
        }   
        return userLanguage;
    }
    
    public void setUserLanguage(String userLanguage)
    {
        this.userLanguage = userLanguage;
    }
 
     //Method to create case from a user 
     public PageReference casecreate()
     {  
        String userId=UserInfo.getUserId();
        newuser = [SELECT Id, FirstName, Contact.id, Contact.Name, Contact.Email, 
                   Contact.Phone FROM User WHERE Id =: UserInfo.getUserId()];                      
        Id oContact = newuser.ContactId;
        System.debug(oContact);//Debug Statement 
               
        //Create a general support case
        if (oContact != null) {
            case oCase = new case();
            oCase.Origin='Web';//Added by A. Pal on 01-may-2012
            oCase.Description = description;
            oCase.Gamertag__c = Gamertag;
            oCase.Spec_Ops_Rank__c = Rank;
            oCase.subject = 'SpecOps Rank Request';
            oCase.RecordTypeId = GeneralSupportCaseRecTypeId;
            oCase.ContactId = oContact;
                        
            if(Gamertag.length() == 0){
                Apexpages.message msg = new apexpages.message(ApexPages.Severity.WARNING, 'You must enter a Gamertag.');
                apexpages.addmessage(msg);
                return null;
            }
            if(Rank.length() == 0){
                Apexpages.message Rankmsg = new apexpages.message(ApexPages.Severity.WARNING, 'You must enter a Spec Ops Rank.');
                apexpages.addmessage(Rankmsg);
                return null;
            }
            
           
            try 
            {
                insert oCase;
            }
            catch(Exception dEx) { return null; } 
        }
         PageReference message=System.Page.AtviCaseCreationConfirmation_SpecOps;
         return message;
     } 
}