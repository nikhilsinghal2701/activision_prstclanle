@isTest
public class AtviArticletoCaseForm_SpecOps_test{
    public static testMethod void test1(){
        try{
            user U = [SELECT id FROM User WHERE IsActive = true AND ContactId != null LIMIT 1];
            system.runAs(u){
                AtviArticletoCaseForm_SpecOps tstCls = new AtviArticletoCaseForm_SpecOps(
                    new ApexPages.standardController(new Case())
                );
                tstCls.EmailAddress = '123';
                tstCls.GamerTag = '';
                tstCls.Description = '123';
                tstCls.userLanguage = 'en_US';
                tstCls.subject = '123';
                tstCls.Rank = '1';
                tstCls.invoiceStatement = new case();
                tstCls.getDescription();
                tstCls.setDescription('asdf');
                tstCls.getSubject();
                tstCls.setSubject('asdf');
                tstCls.getUserLanguage();
                tstCls.setUserLanguage('en_US');
    
                tstCls.caseCreate();
            }
        }catch(Exception e){
        }
    }
    
    public static testMethod void test2(){
        try{
            user U2 = [SELECT id FROM User WHERE IsActive = true AND ContactId != null LIMIT 1];
            system.runAs(u2){
                AtviArticletoCaseForm_SpecOps tstCls = new AtviArticletoCaseForm_SpecOps(
                    new ApexPages.standardController(new Case())
                );
                tstCls.EmailAddress = '123';
                tstCls.GamerTag = '123';
                tstCls.Description = '123';
                tstCls.userLanguage = 'en_US';
                tstCls.subject = '123';
                tstCls.Rank = '';
                tstCls.invoiceStatement = new case();
                tstCls.getDescription();
                tstCls.setDescription('asdf');
                tstCls.getSubject();
                tstCls.setSubject('asdf');
                tstCls.getUserLanguage();
                tstCls.setUserLanguage('en_US');
    
                tstCls.caseCreate();
            }
        }catch(Exception e){
        }
    }
}