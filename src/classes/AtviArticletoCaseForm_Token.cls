public with sharing class AtviArticletoCaseForm_Token {
    public String EmailAddress {get; set;}
    public String Gamertag {get; set;}
    public String description {get; set;}
    public String userLanguage;
    public String subject{get; set;}
    public String SENID{get; set;}
    public String Token{get; set;}
    public String error {get;set;}
    public List<SelectOption> statusOptions {get;set;}
    public case invoiceStatement {get;set;}
    
    public String getdescription() {
    return description;
    }
    public void setdescription(String s) {
    description = s;
    }
    public String getsubject() {
    return subject;
    }
    public void setsubject(String s) {
    subject = s;
    }
    
    public final static String GeneralSupportCaseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Request').getRecordTypeId();
    private User newuser;
   
    public AtviArticletoCaseForm_Token(ApexPages.StandardController controller) {
    invoiceStatement = new case();        
        statusOptions = new List<SelectOption>();

        // Use DescribeFieldResult object to retrieve status field.
        Schema.DescribeFieldResult statusFieldDescription = case.Error_Reason__c.getDescribe();

        // For each picklist value, create a new select option
        for (Schema.Picklistentry picklistEntry:statusFieldDescription.getPicklistValues()){

            statusOptions.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));

            // obtain and assign default value
            if (picklistEntry.defaultValue){
                invoiceStatement.Error_Reason__c = pickListEntry.getValue();
            }  
        }     
    }
    
    //Method to set the user language from the language cookie 
    public String getUserLanguage()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            system.debug('tempCookie not null');
            userLanguage = tempCookie.getValue();            
        }else{
            system.debug('tempCookie is null');
            userLanguage = 'en_US';
        }   
        return userLanguage;
    }
    
    public void setUserLanguage(String userLanguage)
    {
        this.userLanguage = userLanguage;
    }
 
     //Method to create case from a user 
     public PageReference casecreate()
     {  
        String userId=UserInfo.getUserId();
        newuser = [SELECT Id, FirstName, Contact.id, Contact.Name, Contact.Email, 
                   Contact.Phone FROM User WHERE Id =: UserInfo.getUserId()];                      
        Id oContact = newuser.ContactId;
        System.debug(oContact);//Debug Statement 
        
        List<Case> userOpenCases = [Select id From Case c WHERE Subject = 'Token Request' AND Status = 'Open' AND c.contact.id = :newuser.ContactId];
        if (userOpenCases.size() > 0){
           apexPages.AddMessage(new apexPages.Message(apexPages.Severity.FATAL, 'You cannot submit more than one request.'));
           return null;
        }
        
        //String specialChars = '~|`|!|@|#|$|%|^|&|*|(|)|_|-|+|=|{|[|}|]|:|;|"|\'|<|,|>|.|?|/|\\';
        list<string> specialChars = new list<string>{'-','&', '*'};
            for (Integer i = 0; i < specialChars.size(); i++){
                if (Token.Contains(SpecialChars[i]))
                {
                    apexPages.AddMessage(new apexPages.Message(apexPages.Severity.FATAL, 'You Cannot use special characters '));
                    return null;
                }
            }
        
      
        
        //Create a general support case
        if (oContact != null) {
            case oCase = new case();
            oCase.Origin='Web';//Added by A. Pal on 01-may-2012
            oCase.Description = description;
            oCase.Gamertag__c = Gamertag;
            oCase.SEN_ID__c = SENID;
            oCase.Token_Code__c = Token;
            oCase.Error_Reason__c = invoiceStatement.Error_Reason__c;
            oCase.subject = 'Token Request';
            oCase.RecordTypeId = GeneralSupportCaseRecTypeId;
            system.debug('######'+oCase.Description);//Debug statements
            oCase.ContactId = oContact;
            oCase.Product_Effected__c = 'a0UU0000001bA0b';
                        
            if(Gamertag.length() == 0 && SENID.length() == 0){
                Apexpages.message msg = new apexpages.message(ApexPages.Severity.WARNING, 'You must enter at least one Gamertag or SEN ID');
                apexpages.addmessage(msg);
                return null;
            }
           
            try 
            {
                insert oCase;
            }
            catch(Exception dEx) { return null; } 
        }
         PageReference message=System.Page.AtviCaseCreationConfirmation_Token;
         return message;
     } 
}