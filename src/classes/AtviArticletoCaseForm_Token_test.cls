@isTest
public class AtviArticletoCaseForm_Token_test{
    public static testMethod void test1(){
        try{
            user U = [SELECT id FROM User WHERE IsActive = true AND ContactId != null LIMIT 1];
            system.runAs(u){
                AtviArticletoCaseForm_Token tstCls = new AtviArticletoCaseForm_Token(
                    new ApexPages.standardController(new Case())
                );
                tstCls.EmailAddress = '123';
                tstCls.GamerTag = '123';
                tstCls.Description = '123';
                tstCls.userLanguage = 'en_US';
                tstCls.subject = 'Token Request';
                tstCls.Token = 'Token*';
                tstCls.invoiceStatement = new case();
                tstCls.getDescription();
                tstCls.setDescription('asdf');
                tstCls.getSubject();
                tstCls.setSubject('asdf');
                tstCls.getUserLanguage();
                tstCls.setUserLanguage('en_US');
    
                tstCls.caseCreate();
            }
        }catch(Exception e){
        }
     }
     
     public static testMethod void test2(){
     	
     	try{
            user U = [SELECT id FROM User WHERE IsActive = true AND ContactId != null LIMIT 1];
            system.runAs(u){
                AtviArticletoCaseForm_Token tstCls = new AtviArticletoCaseForm_Token(
                    new ApexPages.standardController(new Case())
                );
                tstCls.EmailAddress = '123';
                tstCls.GamerTag = '123';
                tstCls.Description = '123';
                tstCls.userLanguage = 'en_US';
                tstCls.subject = 'Token Request';
                tstCls.Token = 'Token';
                tstCls.invoiceStatement = new case();
                tstCls.getDescription();
                tstCls.setDescription('asdf');
                system.Debug('Get Subject');
                system.Debug(tstCls.getSubject());
                tstCls.setSubject('Token Request');
                tstCls.setUserLanguage('en_US');
                system.Debug('Get User Language');
                system.Debug(tstCls.getUserLanguage());
                
    
                tstCls.caseCreate();
            }
        }catch(Exception e){
        }
     }
     
     public static testMethod void test3(){
        try{
            user U = [SELECT id FROM User WHERE IsActive = true AND ContactId != null LIMIT 1];
            system.runAs(u){
                AtviArticletoCaseForm_Token tstCls = new AtviArticletoCaseForm_Token(
                    new ApexPages.standardController(new Case())
                );
                tstCls.EmailAddress = '123';
                tstCls.GamerTag = '';
                tstCls.SENID = '';
                tstCls.Description = '123';
                tstCls.userLanguage = 'en_US';
                tstCls.subject = 'Token Request';
                tstCls.Token = 'Token';
                tstCls.invoiceStatement = new case();
                tstCls.getDescription();
                tstCls.setDescription('asdf');
                tstCls.getSubject();
                tstCls.setSubject('asdf');
                tstCls.getUserLanguage();
                tstCls.setUserLanguage('en_US');
    
                tstCls.caseCreate();
            }
        }catch(Exception e){
        }
     }
     
     
}