/**
* @author           Abhishek
* @date             05-MAR-2012
* @description      This controller fetches the details of cases for displaying in My Returns and Case details screens.
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* A. Pal                    05-MAR-2012             Created
* A. Pal                    06-MAR-2012             Added new property nonRmaCaseDetails and its getter
* A. Pal                    18-MAR-2012             Added new method updateCaseFlag()
* A. Pal                    09-APR-2012              Renamed loadnonRmaCaseDetails() to loadCaseDetails()
* Mohith                  09-APR-2012              Added properties to collapse the view once no comments or cases found
* A. Pal                    03-MAY-2012             Changed the SOQL in loadCaseDetails() method to fetch FirstName and 
*                                                                  LastName of LastModifiedBy for CaseComments and Attachments
* G. Warb                 12-NOV-2012             Added "ContactId = :contactIdOfUser" to case (ln 217) & chat (320) queries
* G. Warb                 12-NOV-2012             Added authorizedCaseIds to prevent
* G. Warb                 26-NOV-2012             Modified "theCase.W2C_Origin_Language__c=userlanguage;" to
*                                                                    "if(theCase.W2C_Origin_Language__c == null || theCase.W2C_Origin_Language__c == '') theCase.W2C_Origin_Language__c = userlanguage;"
*                                                                    Because gamers are changing their country flags to gain access to off-hours support and their cases are being routed to the wrong queues 
* ---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                     Review Comments
* --------                                          ------------                    --------------- 
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                     Final Inspection before go-live
* Achin Suman                                       25-APR-2012                     EQA
*---------------------------------------------------------------------------------------------------
*/
public class AtviCaseDetailsController { //sharing enforced through query filters, cannot use "with sharing" here because it removes access to cases    
    static string contactIdOfUser;
    set<Id> authorizedCaseIds = new set<Id>();
    public List<CaseDetails> rmaCaseDetails;
    public String commentByGamer{get;set;}
    public String userlanguage; 
    public Static boolean Iscommentssection{get;set;}
    public Static boolean Isattachmentsection{get;set;} 
    public Attachment newAttachment {
        get {
            if (newAttachment == null) newAttachment = new Attachment();
            return newAttachment;
        }
        set;
    }
    
    public AtviCaseDetailsController(ApexPages.StandardController controller){
        
        getContactIdOfUser();
    }
    //Method to set the user language from the language cookie 
    public String getUserLanguage()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            system.debug('tempCookie not null');
            userLanguage = tempCookie.getValue();            
        }else{
            system.debug('tempCookie is null');
            userLanguage = 'en_US';
        }
        return userLanguage;
    }
    
    public void setUserLanguage(String userLanguage)
    {
        this.userLanguage = userLanguage;
    }
  
    //Determines which Contact record is associated with the User record of the current user
    private static void getContactIdOfUser(){
        string userId=Userinfo.getUserId();    
                
        List<User> userRec=[select id, Contact.Id from user where id=:userId limit 1];        
        
        if(userRec.size()>0){        
            User u=userRec.get(0);        
            if(u.Contact!=null){            
                contactIdOfUser=u.Contact.Id;            
                system.debug('Contact Id for the user ['+UserInfo.getUserName()+'] is:'+contactIdOfUser);        
            }else{             
                system.debug('No contact associated with this user');               
            }    
        }
    }
    
    /*CaseDetails-Inner Class which holds data for a case fetched from different objects
    * abpal - 05-Mar-12
    */
    public class CaseDetails{
        public String rmaReturncode{get;set;}
        public String caseSubject{get;set;} 
        public string caseStatus{get;set;}
        public string caseNumber{get;set;}
        public list<Case_Comments_Log__c> caseCommentsLogList=new List<Case_Comments_Log__c>();
        public list<Case_Comments_Log__c> getCaseCommentsLogList(){
            return caseCommentsLogList;
        } 
        public void setCaseCommentsLogList(list<Case_Comments_Log__c> caseCommsLogList){
            caseCommentsLogList=caseCommsLogList; 
        }
        public list<CaseHistory> caseHistoryList=new list<CaseHistory>();       
        public list<CaseHistory> getCaseHistoryList(){
            return caseHistoryList;
        } 
        public void setCaseHistoryList(list<CaseHistory> caseHistList){
            caseHistoryList=caseHistList; 
        }
        public List<Attachment> attachmentList=new List<Attachment>();
        public list<Attachment> getAttachmentList(){
            return attachmentList;
        } 
        public void setAttachmentList(list<Attachment> attList){
            attachmentList=attList; 
        }       
    }
    
    /*getRmaCaseDetails-returns a list of CaseDetail objects containing RMA cases only
    * abpal - 05-Mar-12
    */
    public List<CaseDetails> getRmaCaseDetails(){
        List<Id> caseIds=new List<Id>();
        List<CaseDetails> caseDetailsList=new List<CaseDetails>();
        List<Case> caseList;
        Isattachmentsection=false;
        Iscommentssection=false;
        try{
            caseList =  [SELECT id
                              , CaseNumber
                              , Order_Number__c
                              , Subject
                              , CreatedDate
                              , Status
                              , Priority
                              , OwnerId 
                              , LastModifiedDate
                              , Return_Code__c
                              , (select id
                                  , OldValue
                                  , NewValue
                                  , Field
                                  , CreatedDate 
                                from Histories
                                where Field='Return_Code__c')                                                     
                              , (select id
                                  , Comment_Body__c
                                  , Comment_By__c
                                  , Status_When_Commented__c
                                  , Comment_DateTime__c 
                                from Case_Comments_Logs__r)                                                    
                           FROM   Case 
                          WHERE   ContactId=:contactIdOfUser 
                          AND     Type='RMA'
                       ORDER BY   CreatedDate DESC];
            
            system.debug(caseList.size()+' cases found for user ['+UserInfo.getUserId()+']');
        }catch(Exception e){
            system.debug(e.getMessage());
        }
        system.debug(caseList.size()+' cases up for iteration');
        if(caseList.size()>0){
            
            for(Case caseObj:caseList){
                authorizedCaseIds.add(caseObj.Id); //used by action methods to ensure that only the contact on the case can add content
                CaseDetails mycaseDetails=new CaseDetails();
                mycaseDetails.rmaReturncode=AtviUtility.checkNull(caseObj.Return_Code__c);
                mycaseDetails.caseStatus=AtviUtility.checkNull(caseObj.Status);
                mycaseDetails.caseSubject=AtviUtility.checkNull(caseObj.Subject);  
                mycaseDetails.caseNumber=AtviUtility.checkNull(caseObj.Order_Number__c);           
                if(caseObj.Histories.size()>0){
                    mycaseDetails.setCaseHistoryList(caseObj.Histories);
                    system.debug(caseObj.Histories.size()+' case histories found for case:'+caseObj.Id);
                }else{
                    system.debug('No case history for case:'+caseObj.Id);
                }
                if(caseObj.Case_Comments_Logs__r.size()>0){
                    Iscommentssection=true;
                    mycaseDetails.setCaseCommentsLogList(caseObj.Case_Comments_Logs__r);
                    system.debug(caseObj.Case_Comments_Logs__r.size()+' case comments logs found for case:'+caseObj.Id);
                }else{
                    system.debug('No case comments log for case:'+caseObj.Id);
                }
                
                caseIds.add(caseObj.Id);    
                caseDetailsList.add(mycaseDetails);         
            }
            
        }else{
            system.debug('caseList is empty');
        }
        
        system.debug('caseDetailsList size:'+caseDetailsList.size());
        return caseDetailsList;        
    }
    
    public String caseNumber{get;set;}
    public String trackingNumber{get;set;}
    public String returnCode{get;set;}
    public String caseStatus{get;set;}
    public String caseSubject{get;set;}
    public String caseDesc{get;set;}
    public String caseGameTitle{get;set;}
    public DateTime LastUpdated{get;set;}
    public String caseGamePlatform{get;set;}
    public Case CaseObj{get;set;}
    public List<CaseComment> caseCommentsLogs{get;set;}
    public List<CaseHistory> caseHistories{get;set;}
    public List<Attachment> caseAttachments{get;set;}
    public list<CaseChatTranscript__c> caseChats{get;set;}
    public string showChats{get; set;}
    
    /*loadNonRmaCaseDetails-loads the details related to a selected case for display in the Case Details screen
    * abpal - 06-Mar-12
    */
   public PageReference loadCaseDetails(){
        String caseId=ApexPages.currentPage().getParameters().get('id');    
        system.debug('caseId in loadNonRmaCaseDetails:'+caseId);    
        List<Case> caseList2;
        Isattachmentsection=false;
        Iscommentssection=false;
        caseCommentsLogs=new List<CaseComment>();
        caseHistories=new List<CaseHistory>();
        caseAttachments=new List<Attachment>();
        List<Attachment> attachments;
        try{
            caseList2 =  [SELECT id
                              , RecordType.Name
                              , CaseNumber
                              , Order_Number__c
                              , Subject
                              , Description
                              , CreatedDate
                              , Status
                              , Priority
                              , OwnerId 
                              , LastModifiedDate
                              , Type
                              , Product_Effected__r.Name
                              , Product_Effected__r.Platform__c
                              , Return_Code__c
                              , Tracking_Number__c
                              , Problem_description__c
                              , Case_Not_Editable_by_Gamer__c
                              , (select id
                                  , OldValue
                                  , NewValue
                                  , Field
                                  , CreatedDate 
                                from Histories
                                where Field='Return_Code__c') 
                              , (select id
                                  , LastModifiedDate
                                  , CommentBody
                                  , LastModifiedBy.FirstName /*Modified by A. Pal on 03.May.2012*/
                                  , LastModifiedBy.LastName /*Modified by A. Pal on 03.May.2012*/
                                from CaseComments
                                order by CreatedDate desc)                                                     
                           FROM   Case 
                          WHERE ContactId=:contactIdOfUser AND  Id=:caseId];
            
            system.debug(caseList2.size()+' cases found with case id ['+caseId+']');
            
            attachments=[select id
                            , ContentType
                            , Name
                            , LastModifiedDate
                            , LastModifiedBy.FirstName /*Modified by A. Pal on 03.May.2012*/
                            , LastModifiedBy.LastName /*Modified by A. Pal on 03.May.2012*/
                        from Attachment
                        where parentId = :caseId
                        order by LastModifiedDate desc];
                                            
            system.debug(attachments.size()+' attachments found for case ['+caseId+']');
            
        }catch(Exception e){
            system.debug(e.getMessage());
        }
        
        if(caseList2.size()==1){            
                caseObj=caseList2.get(0);
                authorizedCaseIds.add(caseObj.Id); //used by action methods to ensure that only the contact on the case can add content
                caseNumber=AtviUtility.checkNull(caseObj.CaseNumber);
                caseGameTitle=AtviUtility.checkNull(caseObj.Product_Effected__r.Name);
                caseGamePlatform=AtviUtility.checkNull(caseObj.Product_Effected__r.Platform__c);
                LastUpdated = caseObj.LastModifiedDate;
                if(caseObj.RecordType!=null && caseObj.RecordType.Name.contains('RMA')){
                    //caseNumber=AtviUtility.checkNull(caseObj.Order_Number__c);
                    trackingNumber=AtviUtility.checkNull(caseObj.Tracking_Number__c);
                    returnCode=AtviUtility.checkNull(caseObj.Return_Code__c);
                    if(caseObj.Product_Effected__r!=null){
                        caseGameTitle=AtviUtility.checkNull(caseObj.Product_Effected__r.Name);
                    }
                    caseDesc=AtviUtility.checkNull(caseObj.Problem_description__c);
                }else{
                    caseNumber=AtviUtility.checkNull(caseObj.CaseNumber);
                    caseDesc= AtviUtility.checkNull(caseObj.Description); 
                }
                caseStatus=AtviUtility.checkNull(caseObj.Status);
                caseSubject=AtviUtility.checkNull(caseObj.Subject);                 
                if(caseObj.Histories.size()>0){
                    caseHistories.addAll(caseObj.Histories);
                    system.debug(caseObj.Histories.size()+' case histories found for case:'+caseObj.Id);
                }else{
                    system.debug('No case history for case:'+caseObj.Id);
                }
                
                if(caseObj.CaseComments.size()>0){                    
                    caseCommentsLogs.addAll(caseObj.CaseComments);
                    Iscommentssection=true;
                    system.debug(caseObj.CaseComments.size()+' case comments logs found for case:'+caseObj.Id);
                }else{
                    system.debug('No case comments log for case:'+caseObj.Id);
                }
                
                if(attachments.size()>0){                    
                    caseAttachments.addAll(attachments);
                    Isattachmentsection=true;
                    system.debug(attachments.size()+' attachments set for case ['+caseObj.Id+']');
                }else{
                    system.debug('No attachment found for case:'+caseObj.Id);
                }                       
            
        }else if(caseList2.size()>1){
            system.debug('caseList has more than 1 object with Id:'+caseId);
        }else{
            system.debug('caseList is empty');
        }
        showChats = 'none';
        if(caseId !=null){
            caseChats = [Select c.transcript__c, c.CreatedDate, c.Case__c, c.Agent_Name__c,
                                    Duration_sec__c
                                    From CaseChatTranscript__c c
                                    where Case__c = :caseId AND
                                    Case__r.ContactId = :contactIdOfUser //G. Warburton, 12-Nov-2012 security tightening
                                    order by CreatedDate];
            if(caseChats!=null && caseChats.size()>0) showChats='block';
        }
        return null;
    }
    
    /*saveCaseComment-inserts into SFDC the comment written by Gamer in Case Details screen
    * abpal - 07-Mar-12
    */
    public PageReference saveCaseComment(){
        Iscommentssection=false;
        String caseId=ApexPages.currentPage().getParameters().get('id');  
        system.debug('caseId in insertCaseComment:'+caseId);
        
        CaseComment newComment=new CaseComment();
        newComment.CommentBody=AtviUtility.checkNull(commentByGamer);
        newComment.ParentId=AtviUtility.checkNull(caseId);
        try{
            Iscommentssection=true;
            if(authorizedCaseIds.contains(caseId)){ //added so only the contact on the case can make updates
                insert newComment;
                system.debug('New gamer comment added from portal');
            }
        }catch(Exception e){
            system.debug(e.getMessage());
        }
        commentByGamer=null;
        PageReference thisPage=ApexPages.currentPage();
        thisPage.getParameters().put('id',caseId);     
        thisPage.setRedirect(true);   
        updateCaseFlag(caseId);
        return thisPage;
    }
    
    /*upload-inserts into SFDC the attachment uploaded by Gamer in Case Details screen
    * abpal - 07-Mar-12
    */
    public PageReference upload() {
        Isattachmentsection=false;
        String caseId=ApexPages.currentPage().getParameters().get('id'); 
        system.debug('caseId in upload:'+caseId);
        newAttachment.parentid = AtviUtility.checkNull(caseId);
        try{
                if(authorizedCaseIds.contains(caseId)){ //added so only the contact on the case can make updates
                insert newAttachment;
                system.debug('New attachment uploaded');
                }
            Isattachmentsection=true; 
        }catch(Exception e){
            system.debug(e.getMessage());               
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error in file upload:'+e.getMessage()));
        }
        newAttachment.Body=null;
        PageReference thisPage=ApexPages.currentPage();
        thisPage.getParameters().put('id',caseId);
        thisPage.setRedirect(true);        
        updateCaseFlag(caseId);
        return thisPage;
     }
     
     /*reopenCase - reopens a closed case in SFDC 
     * abpal - 07-Mar-12
     */
      public PageReference reopenCase(){
        String caseId=ApexPages.currentPage().getParameters().get('id'); 
        system.debug('caseId in reopenCase:'+caseId);
        List<Case> closedCases=[select id, status, isClosed, Closed_By__c from Case where Id=:caseId and status='Closed'];
        Case closedCase;
        if(closedCases.size()==1){
            closedCase=closedCases.get(0);
            if(closedCase.Closed_By__c==null){
                system.debug('Invalid data in Closed By field');
                return null;
            }
            closedCase.Status='Open';
            closedCase.Closed_By__c=null;
            try{
                if(authorizedCaseIds.contains(caseId)){ //added so only the contact on the case can make updates
                        update closedCase;
                }
                system.debug('case ['+caseId+']reopened');
            }catch(Exception e){
                system.debug(e.getMessage());
            }           
        }else if(closedCases.size()>1){
            system.debug('closedCases has more than 1 object with Id:'+caseId);
        }else{
            system.debug('closedCases is empty');
        }
        
        PageReference thisPage=ApexPages.currentPage();
        thisPage.getParameters().put('id',caseId);
        return thisPage;
     }
     /*updateCaseFlag-method to set the has_gamer_commented__c field to true in case 
     * a new comment or attachment is uploaded from portal
     * abpal - 18.03.12
     */
     private void updateCaseFlag(string caseId){
        List<Case> theCases=[select id, has_gamer_commented__c, W2C_Origin_Language__c from Case where Id=:caseId];
         Case theCase = new Case();
         if(theCases!=null && theCases.size() > 0)
         {
           theCase = theCases[0];
         }
         else
         {
           system.debug('cases returned 0');
         }
         
         if(theCase!=null && authorizedCaseIds.contains(caseId) ){
             System.debug('Case found with Id='+caseId);
             theCase.has_gamer_commented__c=true;
             if(theCase.W2C_Origin_Language__c == null || theCase.W2C_Origin_Language__c == '') theCase.W2C_Origin_Language__c = userlanguage;
             try{
                 update theCase;
             }catch(Exception e){
                 system.debug(e.getMessage());
             }
         }
     }
}