/**
* @author           Abhishek Pal
* @date             07-MAY-2012
* @description      This class is called from the Case triggers
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* A. Pal                    08-MAY-2012             Created, When a product is selected while creating or updating an RMA case.
*                                                                  This method references the Public_Product__r.Return_Required__c field
*                                                                  and prepopulates that value in the Case.Return_Required__c field.
*---------------------------------------------------------------------------------------------------
* Review Log  :          
*---------------------------------------------------------------------------------------------------
* Reviewer                  Review Date            Review Comments
* --------                  ------------          --------------- 
* Karthik(Deloitte Consultant)        08-MAY-2012            Final Inspection before go-live
* G. Warb (ATVI SF Sr. Dev)             28-NOV-2012            Security review, we are adding "with sharing" everywhere as possible.
*                                                                                        this class is being called from a trigger so user context is already being applied, so adding
*                                                                                        with sharing will not change behavior 
*---------------------------------------------------------------------------------------------------
*/
public with sharing class AtviCaseTriggerClass {
	public static void checkReturnRequired(List<Case> cases){
		system.debug('Inside AtviCaseTriggerClass.checkReturnRequired method');	
		map<Id, Public_Product__c> productsOnDisc=loadProductsFromDisc();
		List<RecordType> recTypes=[select Id from RecordType where name = 'RMA' and sObjectType='Case'];
		String rmaRecTypeId;
		if(recTypes.size()>0) rmaRecTypeId=recTypes.get(0).Id;
		system.debug('rmaRecTypeId='+rmaRecTypeId);
		
		system.debug(cases.size()+' Cases to be processed');		
		for(Case c:cases){
			String recordTypeId=c.RecordTypeId;
			system.debug('recordTypeId='+recordTypeId);
			system.debug('c.Product_Effected__c='+c.Product_Effected__c);
			if(recordTypeId==rmaRecTypeId && c.Product_Effected__c!=null){				
				if(productsOnDisc.get(c.Product_Effected__c)!=null){
					c.Return_Required__c=productsOnDisc.get(c.Product_Effected__c).Return_Required__c;
				}
			}
			system.debug('c.Return_Required__c='+c.Return_Required__c);
		}	
		system.debug('returning from AtviCaseTriggerClass.checkReturnRequired method');			
	} 
	
	public static boolean isLoadProductsFromDiscCalled=false;
	public static map<Id, Public_Product__c> loadProductsFromDisc(){
		//if(isLoadProductsFromDiscCalled) return null;
		
		Map<Id, Public_Product__c> mapProdIdToRetReq=new Map<Id, Public_Product__c>();
		
		List<Public_Product__c> products=[select Id, return_required__c from public_product__c];
		system.debug(products.size()+' products on disc');
		if(products.size()>0){
			for(Public_Product__c p:products){
				mapProdIdToRetReq.put(p.Id, p);
			}
		}
		system.debug(mapProdIdToRetReq.size()+' product maps returned');
		isLoadProductsFromDiscCalled=true;
		return mapProdIdToRetReq;		
	}
	
	public static testMethod void testCheckReturnRequired(){
		
		Public_Product__c testProduct=new Public_Product__c();
		testProduct.Name='Test Game';
		testProduct.Return_Required__c=true;
		testProduct.RMA_Available__c = true;
		insert testProduct;		
		
		List<RecordType> recTypes=[select Id from RecordType where name = 'RMA' and sObjectType='Case'];
		RecordType rmaRecType;
		if(recTypes.size()>0) rmaRecType=recTypes.get(0);
		else rmaRecType=new RecordType();
		
		Case testCase=new Case();
		testCase.RecordTypeId=rmaRecType.Id;
		testCase.Subject='Test Case';
		testCase.Problem_Description__c='Test Description';
		testCase.Product_Effected__c=testProduct.Id;
		
		Test.startTest();
			insert testCase;
		Test.stopTest();
	}
}