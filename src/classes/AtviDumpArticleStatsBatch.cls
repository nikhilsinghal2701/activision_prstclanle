/**
* @author           A. PAL
* @date             30-MAR-2012
* @description      Batch class to collect article vote and view stats and dump them in the Article_Statistics__c object 
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* A. Pal                    30-MAR-2012             Created
* A. Pal					22-APR-2012				Changed the main query and associated processing logic to enable fetching of lesser rows
*---------------------------------------------------------------------------------------------------
* Review Log	:
*---------------------------------------------------------------------------------------------------
* Reviewer											Review Date						Review Comments
* --------											------------					---------------
* A. Pal(Deloitte Senior Consultant)				19-APR-2012						Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
global class AtviDumpArticleStatsBatch implements Schedulable, Database.Batchable<sObject>, Database.Stateful{
        
      public string query;
      public List<Article_Statistics__c> aStatsList=new List<Article_Statistics__c>();      
    
      global Database.QueryLocator start(Database.BatchableContext BC) {
             
        String channelFlag='Pkb';
        boolean deleted=false;
        if(query==''||query==null){
	        query='Select id, ParentId, NormalizedScore from KnowledgeArticleViewStat where isDeleted=:deleted and Channel=:channelFlag order by NormalizedScore desc limit 250';	        
        }
        system.debug('query='+query);                                               
        return Database.getQueryLocator(query);
      }
    
      global void execute(Database.BatchableContext BC, 
                          List<sObject> batch) {
        // Access initialState here  
        system.debug('Inside execute method');  				          
        Map<String, Double> viewScoreMap=new Map<String, Double>();
        Map<String, Double> voteScoreMap=new Map<String, Double>();
        
        for(sObject s:batch){
        	KnowledgeArticleViewStat kaVS=(KnowledgeArticleViewStat)s;
        	viewScoreMap.put(kaVS.ParentId, kaVS.NormalizedScore); 
        }
        
        system.debug('Number of KnowledgeArticleViewStat records fetched='+viewScoreMap.size());
        
        List<KnowledgeArticleVoteStat> voteStatList=[Select id, ParentId, NormalizedScore from KnowledgeArticleVoteStat where parentId in :viewScoreMap.keySet()];
        
        system.debug('Number of KnowledgeArticleVoteStat records fetched='+voteStatList.size());
        
        for(KnowledgeArticleVoteStat vote:voteStatList){
        	voteScoreMap.put(vote.ParentId, vote.NormalizedScore); 
        }
            
        system.debug('New record being created');
        
        for(String kaId:viewScoreMap.keySet()){
        	Article_Statistics__c aStats=new Article_Statistics__c();        
	        aStats.Knowledge_Article_Id__c=kaId;
	        aStats.Article_Vote_Score__c=voteScoreMap.get(kaId);
	        aStats.Article_View_Score__c=viewScoreMap.get(kaId);
	        aStats.Time_of_Update__c=System.now();
	        
	        aStatsList.add(aStats);  //make this stateful 
        }
       
        try{
            upsert aStatsList Knowledge_Article_Id__c;
            system.debug(aStatsList.size()+' records inserted into Article_Statistics__c');
        }catch(DMLException e){
            system.debug(e.getMessage());           
        }   
        
      }
    	
      /*Added call to bubble sorter method - A, PAL on 04.22.2012*/
      global void finish(Database.BatchableContext BC) {
      	
      	//List<Article_Statistics__c> aStatsList250=getTop250(aStatsList);      	
                      
        system.debug('DumpArticleStatsBatch finished'); 
            
      } 
      
         
      global void execute(SchedulableContext sc){
        system.debug('Initiating batch run...');
        AtviDumpArticleStatsBatch dumper=new AtviDumpArticleStatsBatch();
        database.executeBatch(dumper,100);
        system.debug('Batch run complete');
      }
      
      /*Added by A. PAL on 04.22.2012*/
      /*Gets the most popular 250 articles from the list of all articles statistics records 
      /*created in this batch throufh bubble sorting*/
      private List<Article_Statistics__c> getTop250(List<Article_Statistics__c> aStatsList){
      	system.debug('Inside getTop250 method');
      	List<Article_Statistics__c> aListSorted=new List<Article_Statistics__c>();   
      	List<Article_Statistics__c> topArticles=new List<Article_Statistics__c>();      	
		system.debug('Total number of articles passed='+aStatsList.size());
		for(integer i=0;i<aStatsList.size();i++){
			if(i==0) {
		        system.debug('aStatsList.get(0)='+aStatsList.get(0));
				aListSorted.add(aStatsList.get(0));
			}else{
                Integer aListSortedSize=aListSorted.size();
                system.debug('aListSortedSize='+aListSorted.size());
                Article_Statistics__c currListItem=aStatsList.get(i);
                system.debug('View Score in currListItem='+currListItem.Article_View_Score__c);
                system.debug('aListSorted.get('+(aListSortedSize-1)+')='+aListSorted.get(aListSortedSize-1));
                Integer posForNewEntry=0;
	            for(integer i2=0;i2<aListSortedSize;i2++){
	                if(currListItem.Article_View_Score__c<aListSorted.get(i2).Article_View_Score__c){
						posForNewEntry=i2;
						break;
					}else{
						posForNewEntry=aListSortedSize;
					}
	            }
            system.debug('posForNewEntry='+posForNewEntry);
           if(posForNewEntry<aListSortedSize){
                aListSorted.add(posForNewEntry,currListItem);
			}else{
				aListSorted.add(currListItem);
			}
            system.debug('currListItem='+currListItem.Id+' added at pos ='+posForNewEntry+' in aListSorted');
            system.debug('aListSorted.get('+posForNewEntry+').Article_View_Score__c='+aListSorted.get(posForNewEntry).Article_View_Score__c);
			}	
			system.debug('aListSorted='+aListSorted);
		}
		system.debug('aListSorted.size()='+aListSorted.size());
		List<Integer> numListTop3=new List<Integer>();
		
		Integer ub=aListSorted.size();
		Integer lb=ub-250;
		if(ub<250) lb=0;
		
		for(Integer m=(ub-1);m>=lb;m--){
			topArticles.add(aListSorted.get(m));			
		}
		for(Integer show=0;show<topArticles.size();show++){
			system.debug('Article View Stats='+topArticles.get(show).Article_View_Score__c);
		}
      	
      	return topArticles;      	
      }
}