/**
* @author           Abhishek
* @date             16-FEB-2012
* @description      This set controller fetches all the cases where the current user is the Contact 
                    mentioned in the case.
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* A. PAL                    16-FEB-2012             Created
* Mohith                    04-MAR-2012             Added Select Option to filter Cases according to status.
* Mohith                    31-MAR-2012             Modified constructor to set the value Case Status as My Cases first time page loads
* Mohith                    05-APR-2012             Modified to add pagination 
* A. PAL                    09-APR-2012             Renamed BindData() to BindCaseData(). 
                                                    Added 3 new methods: fetchCases(),doBindStdCaseData(),doBindRMACaseData()
* Griffin Warburton         09-MAY-2014             Added Product_L1__c & Platform__c to the case query 
* ---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                     Review Comments
* --------                                          ------------                    --------------- 
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                     Final Inspection before go-live
* Achin Suman (Deloitte Consultant)                 25-APR-2012                     EQA
*---------------------------------------------------------------------------------------------------
*/
public class AtviMyCasesController {
    
    public List<Case> caseList = new List<Case>();
    public List<Stat_Corruption_Report__c> scrList = new list<Stat_Corruption_Report__c>();
    public String rmaFlag;
    public Boolean showSubmissions {get;set;}
    static string contactIdOfUser;
    private List<Case> pagecases;
    private Integer pageNumber;
    private Integer pageSize;
    private Integer totalPageNumber; 
    
    static string recordidofRMA = [SELECT r.SobjectType, r.Name, r.Id 
                                   FROM RecordType r 
                                   WHERE r.SobjectType = 'Case' AND 
                                   r.Name = 'RMA' limit 1].Id;    
    
    public Integer getPageNumber() {
        return pageNumber;
    }
    
    public List<Case> getMyCases() {
        return pagecases;
    }
    
    public List<Stat_Corruption_Report__c> getMySCReports() {
        return scrList;
    }
    
    public Integer getPageSize() {
        return pageSize;
    }

    public Boolean getPreviousButtonEnabled() {
        return !(pageNumber > 1);
    }
    
    public Boolean getNextButtonDisabled() {
        if (caseList == null) return true;
        else return ((pageNumber * pageSize) >= caseList.size());
    }
    
    public Integer getTotalPageNumber() {
        if (totalPageNumber == 1 && caseList.size()>0) {
           totalPageNumber = caseList.size() / pageSize;
           System.debug('#####'+totalPageNumber);
           Integer mod = caseList.size() - (totalPageNumber * pageSize);
           if (mod > 0)
            totalPageNumber++;
        }
        return totalPageNumber;
   }
   //Method to fetch RMA and General cases
   private List<Case> fetchCases(boolean isRMACase) {
        List<Case> cases=new List<Case>();
        system.debug('Fetching RMA cases?'+isRMACase);
        if(isRMACase) {
            cases = [SELECT Id, CaseNumber, Subject, CreatedDate, Status,
                     Callback_Request__c, 
                     Priority, OwnerId, LastModifiedDate, Return_Code__c, Description,
                     Product_Effected__r.Name,
                     Product_Effected__r.Platform__c, 
                     Product_Effected__r.Medallia_Product_3__c, RecordTypeId, 
                     Product_L1__c, Platform__c,
                     Order_Number__c FROM Case 
                     WHERE  ContactId =: contactIdOfUser AND 
                     RecordTypeId =: recordidofRMA 
                     ORDER BY LastModifiedDate DESC];
                     
            scrList = [SELECT Id, Gamertag__c, Issue__c, Status__c
                       FROM Stat_Corruption_Report__c
                       WHERE OwnerID=: Userinfo.getUserId()];
            
        }else {
            cases = [SELECT Id, CaseNumber, Subject, CreatedDate, Status, 
                     Callback_Request__c, 
                     Priority, OwnerId, LastModifiedDate, Return_Code__c, Description,
                     Product_Effected__r.Name,
                     Product_Effected__r.Platform__c, 
                     Product_Effected__r.Medallia_Product_3__c, RecordTypeId, 
                     Product_L1__c, Platform__c,
                     Order_Number__c FROM Case 
                     WHERE ContactId =: contactIdOfUser AND 
                     RecordTypeId !=: recordidofRMA AND
                     Case_Not_Visible_by_Gamer__c =: false 
                     ORDER BY LastModifiedDate DESC];
                     
             scrList = [SELECT Id, Gamertag__c, Issue__c, Status__c, CreatedDate
                       FROM Stat_Corruption_Report__c
                       WHERE OwnerID=: Userinfo.getUserId()
                       ORDER BY LastModifiedDate DESC limit 5];
                       
               if(scrList.size()>0){
                   showSubmissions = true;
               }
               else{
                   showSubmissions = false;
               }
        }
        system.debug('Cases fetched'+cases.size());
        return cases;
   }
   //Custom pagination code
   private void BindCaseData(Integer newPageIndex,Boolean isRMACase) {               
       if(caseList.size() == 0) caseList = fetchCases(isRMACase);                                                
       system.debug(caseList.size()+' cases returned of type RMA='+isRMACase);                      
       pagecases = new List<case>();
       Transient Integer counter = 0;
       Transient Integer min = 0;
       Transient Integer max = 0;
         
       if (newPageIndex > pageNumber) {
           min = pageNumber * pageSize;
           system.debug('*****'+min);
           max = newPageIndex * pageSize;
           system.debug('*****'+max);
       }else {
            max = newPageIndex * pageSize;
            min = max - pageSize;
       }
      
       for(Case c : caseList) {
            counter++;
            if (counter > min && counter <= max)
                pagecases.add(c);
       }

      pageNumber = newPageIndex;
      system.debug('@@@@'+newPageIndex);
      if (pagecases == null || pagecases.size() <= 0)
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Data not available for this view.')); 
     }
     
    public AtviMyCasesController(ApexPages.StandardSetController controller) {
        getContactIdOfUser(); 
        showSubmissions = false;
        pageNumber = 0;
        totalPageNumber = 1;
        pageSize = 10;
        rmaFlag = ApexPages.currentPage().getParameters().get('rma');
        if(rmaFlag == '1') {
            BindCaseData(1,true);
        }else {
            BindCaseData(1,false);
        }                          
    }
    
    public PageReference nextBtnClick() {
        if(rmaFlag == '1') {
            BindCaseData(pageNumber + 1,true);
        }else {
            BindCaseData(pageNumber + 1,false);
        }
        return null;
    }

    public PageReference previousBtnClick() {
        if(rmaFlag == '1') {
            BindCaseData(pageNumber - 1,true);
        }else {
            BindCaseData(pageNumber - 1,false);
        }
        return null;
     }
    
    public string getUserID(){
        return Userinfo.getUserId();
    }     
    //Determines which Contact record is associated with the User record of the current user
    private static void getContactIdOfUser() {
        string userId = Userinfo.getUserId();    
                
        List<User> userRec = [SELECT Id, Contact.Id FROM User 
                              WHERE Id =: userId limit 1];  
        if(userRec.size()>0) {        
            User u=userRec.get(0);        
            if(u.Contact!=null) {            
                contactIdOfUser=u.Contact.Id;            
                system.debug('Contact Id for the user ['+UserInfo.getUserName()+'] is:'+contactIdOfUser);        
            }else {             
                system.debug('No contact associated with this user');               
            }    
        }
    }
}