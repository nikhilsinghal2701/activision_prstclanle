/**
* @author           Mohith
* @date             03-MAR-2012
* @description      This controller is used to create a case whenever user submits a case (web to case).
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Mohith          03-MAR-2012        Created
* A. Pal          01-MAY-2012            Set Case Origin to "Web" in casecreate() method
* ---------------------------------------------------------------------------------------------------
* Review Log  :
*---------------------------------------------------------------------------------------------------
* Reviewer                      Review Date            Review Comments
* --------                      ------------          --------------- 
* A. Pal(Deloitte Senior Consultant)        19-APR-2012            Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
public with sharing class AtviRMAContactForm { 
   
   public boolean phonereq{get;set;}
   public boolean descreq{get;set;}
   public string prodid = Apexpages.currentpage().getParameters().get('prod');
   
   public public_product__c prod = [select id,name from public_product__c where id=:prodid];
   public string getprod(){
       return prod.name;
   }
   public case descstate {get;set;}
   public case desccountry {get;set;}
   public List<SelectOption> statusOptions {get;set;}
   public List<SelectOption> statusOptions2 {get;set;}
   public String Gamertag {get; set{System.debug('@@@@@@'+Gamertag);}}
   public String description;
   public String street{get;set;}
   public String street2{get;set;}
   public String city{get;set;}
   public String State{get;set;}
   public String zip{get;set;}
   public String country{get;set;}
   public String phone{get;set;}
   //public String EmailAddress {get; set;}
   //public String email;
   public String item{get;set;}
   public String userLanguage;
   public String getdescription() {
     return description;
   }
   public void setdescription(String s) {
     description = s;
   }
   public String subject;
   public String getsubject() {
     return subject;
   }
   public void setsubject(String s) {
     subject = s;
   }
   
   
   public final static String GeneralSupportCaseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CallBack').getRecordTypeId();
   private User newuser;
 
   public AtviRMAContactForm(ApexPages.StandardController controller) {
       /*system.debug(prodid);
       prod = [select id,name from public_product__c where id=:prodid];*/
       phonereq=false;
       descreq=false;
       statusOptions = new List<SelectOption>();
       statusOptions2 = new List<SelectOption>();

        // Use DescribeFieldResult object to retrieve status field.
        Schema.DescribeFieldResult statusFieldDescription = case.State__c.getDescribe();
        Schema.DescribeFieldResult statusFieldDescription2 = case.Country__c.getDescribe();


        // For each picklist value, create a new select option
        for (Schema.Picklistentry picklistEntry:statusFieldDescription.getPicklistValues()){

            statusOptions.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
            //system.debug(pickListEntry.getValue());
            //system.debug(pickListEntry.getLabel());

            
        } 
        
        
        for (Schema.Picklistentry picklistEntry : statusFieldDescription2.getPicklistValues()){
            
            statusOptions2.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
           
        }
          
   
   }
    //Method to set the user language from the language cookie 
    public String getUserLanguage()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
          system.debug('tempCookie not null');
            userLanguage = tempCookie.getValue();            
        }else{
          system.debug('tempCookie is null');
          userLanguage = 'en_US';
        }
        return userLanguage;
    }
    
    public void setUserLanguage(String userLanguage)
    {
        this.userLanguage = userLanguage;
    }
 
 //Method to create case from a user 
 public PageReference casecreate()
 { 
     if(phone==''||description==''){
         if(phone==''){
             phonereq=true;
         }
         else{
             phonereq=false;
         }
         if(description==''){
             descreq=true;
         }
         else{
             descreq=false;
         }
         return null;
         
     }
    System.debug('@@@@@@'+subject);
    system.debug('@@@@'+description);//debug statement
    String userId=UserInfo.getUserId();
    newuser = [SELECT Id, FirstName, Contact.Id, Contact.Name, Contact.Email, 
           Contact.Phone FROM User WHERE Id =: UserInfo.getUserId()];                      
    System.debug('@@@@@@@'+userId);//Debug Statement    
    Id oContact = newuser.ContactId;
    System.debug(oContact);//Debug Statement 
    //Create a general support case
    if (oContact != null) {
        case oCase = new case();
        oCase.Origin='Web';//Added by A. Pal on 01-may-2012
        oCase.Description = description;
        oCase.Subject = subject;
        oCase.Address_1__c = street;
        oCase.Address_2__c = street2;
        oCase.City__c = city;
        oCase.State__c = state;
        oCase.Country__c = country;
        oCase.phone__c = phone;
        oCase.product_effected__c = prodid;
        oCase.subject = 'RMA Call Back';
        oCase.RecordTypeId = GeneralSupportCaseRecTypeId;
        system.debug('######'+oCase.Description);//Debug statements
        oCase.ContactId = oContact;
        oCase.W2C_Origin_Language__c=userLanguage;
        oCase.W2C_Route_To_Custom_Queue__c=true;
        try {
          insert oCase;
        }catch(Exception dEx) {}        
    }
   PageReference message=System.Page.AtviCaseCallBackConfirmation;
   return message;
 }
}