/**
* @author           Abhishek
* @date             17-FEB-2012
* @description      This controller fetches the mailing address for the contact associated with a 
					case so that the RMA Ship-To address fields could be pre-populated in the New 
					Case form.
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* A. Pal					17-FEB-2012				Created
* ---------------------------------------------------------------------------------------------------
* Review Log	:
*---------------------------------------------------------------------------------------------------
* Reviewer											Review Date						Review Comments
* --------											------------					--------------- 
* A. Pal(Deloitte Senior Consultant)				19-APR-2012						Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
public class AtviRmaAddressFillController {
	
	public static String RmaShipTo {get;set;}
	public static String RmaAddress_1 {get;set;}
	public static String RmaAddress_2 {get;set;}
	public static String RmaCity {get;set;}
	public static String RmaState {get;set;}
	public static String RmaCountry {get;set;}
	public static String RmaZip {get;set;}
	public static String RmaPhone {get;set;}
	public static String RmaEmail {get;set;} 
	public static string beforeQString;
	public static string targetURL {get;set;}
	
	public AtviRmaAddressFillController(ApexPages.StandardController scon) {}
	
	public static PageReference populateRmaVariables(){
		//Getting the query string parameters from the current page URL
		String contactId = ApexPages.currentPage().getParameters().get('def_contact_id');
		String acctId = ApexPages.currentPage().getParameters().get('def_account_id');
		String recordType = ApexPages.currentPage().getParameters().get('RecordType');
		String retURL = ApexPages.currentPage().getParameters().get('retURL');
		
		if(contactId != null && contactId != '') {
			system.debug('Contact Id associated for this case: '+contactId);
			List<Contact> contacts = [SELECT Id, Name, MailingStreet, 
									  MailingCity, MailingState, MailingCountry, 
									  MailingPostalCode, Phone, Email 
									  FROM Contact WHERE Id =: contactId 
									  limit 1];
			
			if(contacts.size()>0) {
				Contact contact = contacts.get(0);
				RmaShipTo = contact.Name;
				system.debug('RmaShipTo='+RmaShipTo);
				RmaAddress_1 = contact.MailingStreet;
				system.debug('RmaAddress_1='+RmaAddress_1);
				RmaCity = contact.MailingCity;
				system.debug('RmaCity='+RmaCity);
				RmaState = contact.MailingState;
				system.debug('RmaState='+RmaState);
				RmaCountry = contact.MailingCountry;
				system.debug('RmaCountry='+RmaCountry);
				RmaZip = contact.MailingPostalCode;
				system.debug('RmaZip='+RmaZip);
				RmaPhone = contact.Phone;
				system.debug('RmaPhone='+RmaPhone);
				RmaEmail = contact.Email;				
				system.debug('RmaEmail='+RmaEmail);
				targetURL = 'retURL='+retURL+'&def_contact_id='+contactId+'&def_account_id='+acctId+'&RecordType='+recordType+'&ent=Case';
			}else {
				RmaShipTo = '';
				RmaAddress_1 = '';
				RmaCity = '';
				RmaState = '';
				RmaCountry = '';
				RmaZip = '';
				RmaPhone = '';
				RmaEmail = '';	
				
				targetURL = 'retURL=%2F500%2Fo&RecordType='+recordType+'&ent=Case';		
			}			
		}else {
			RmaShipTo = '';
			RmaAddress_1 = '';
			RmaCity = '';
			RmaState = '';
			RmaCountry = '';
			RmaZip = '';
			RmaPhone = '';
			RmaEmail = '';
			system.debug('Contact Id not associated for this case');
			targetURL = 'retURL=%2F500%2Fo&RecordType='+recordType+'&ent=Case';
		}
		system.debug('targetURL='+targetURL);
		return null;
	}
	
	public static String getBeforeQString() {
		
		String currentURL = ApexPages.currentPage().getUrl();		
		String currentQString = currentURL.substring(currentURL.indexOf('?')+1);
		system.debug('Original query string='+currentQString);		
		return currentQString;
	}		
}