/**
* @author           Suman
* @date             23-FEB-2012
* @description      This class provides reusable methods.
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Suman						23-FEB-2012				Created
*													Copied kavNames method from pkb_Controller
* Suman						24-FEB-2012				Added method - SearchLabelfromKey to decode 
													language or country key	into its expected labels. 
													Added method - Searchkeyfromlabel to decode 
													language label into its expected key
* A. Pal					19-MAR-2012				Added new method checkNull(String s)
* ---------------------------------------------------------------------------------------------------
* Review Log	:
*---------------------------------------------------------------------------------------------------
* Reviewer											Review Date						Review Comments
* --------											------------					--------------- 
* A. Pal(Deloitte Senior Consultant)				19-APR-2012						Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
public with sharing class AtviUtility {

  /* ***** KAV EXTENSION, used in VF pages when need to derive article type name from whole object name ***** */
  private final static String KAV_EXTENSION = '__kav';
  /**
   * DYNAMIC RETRIEVAL OF ARTICLE TYPES VIA DESCRIBES AND OBJECT ID PREFIXES
   */
  public static Set<String> kavNames { 
    set;
    get {
        if (kavNames == null) {
            kavNames = new Set<String>();
        Map<String,Schema.SOBjectType> gd = Schema.getGlobalDescribe();

        for (String s : gd.keySet()) {
          if (s.contains(KAV_EXTENSION)) {
            kavNames.add(s);
          }
        }
        }
      return kavNames;
    }
  }
  
  //Method to decode language or country key into its expected labels
  public static String SearchLabelfromKey(String searchvar, Boolean getLanguage){
    Schema.DescribeFieldResult F;
    if(getLanguage)
        F = User.LanguageLocaleKey.getDescribe(); 
    else{
        F = User.LocaleSidKey.getDescribe(); 
    }       
    integer i;        
    List<Schema.PicklistEntry> P = F.getPicklistValues();        
    Map<string,string> mapofKeyandLanguages=new  Map<string,string>();        
    for (i=0 ;i<P.size();i++)        {         
        mapofKeyandLanguages.put(P[i].getValue(),P[i].getLabel());        
    }                 
    
    String getLabel=mapofKeyandLanguages.get(searchvar);        
    return getLabel;             
  }
  
   //Method to decode language label into its expected key               
   public static String Searchkeyfromlabel(String searchvar){        
    Schema.DescribeFieldResult F = User.LanguageLocaleKey.getDescribe();        
    integer i;        
    List<Schema.PicklistEntry> P = F.getPicklistValues();        
    Map<string,string> mapofKeyandLanguages=new  Map<string,string>();        
    for (i=0 ;i<P.size();i++){         
        mapofKeyandLanguages.put(P[i].getLabel(),P[i].getValue());        
    }         
    String getkey=mapofKeyandLanguages.get(searchvar);          
    return getkey;             
   }  
   
   //checkNull method - replaces null string with vacant quotes to avoid NullpointerException
   public static String checkNull(String s){
   		return s==null?'':s;	
   }  
}