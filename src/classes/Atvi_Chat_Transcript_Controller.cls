public class Atvi_Chat_Transcript_Controller {
 
 public CaseChatTranscript__c chat{get; set;}
 
 
 public Atvi_Chat_Transcript_Controller(){
    string cId = Apexpages.currentPage().getParameters().get('cId');
    User curUser = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId()];
    if(cId !=null){
        list<CaseChatTranscript__c> cList = [Select c.transcript__c, c.CreatedDate//, c.Case__c 
                                                                        From CaseChatTranscript__c c
                                                                        where id=:cId
                                                                        AND Case__r.ContactId = :curUser.ContactId]; //this clause added by FGW for security
        if(cList.size()>0) chat=cList[0];
    }
 }
 
 private static testmethod void test1(){
    CaseChatTranscript__c chat = new CaseChatTranscript__c();
    chat.transcript__c = 'blah';
    insert chat;
    
    Apexpages.currentPage().getParameters().put('cId', chat.id);
    Atvi_Chat_Transcript_Controller c = new Atvi_Chat_Transcript_Controller();
 }
}