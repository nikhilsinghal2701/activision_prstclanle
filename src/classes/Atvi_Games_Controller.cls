public with sharing class Atvi_Games_Controller
{
    public String selectedPlatform           { get; set; }
    public List<String> platforms = new List<String>{'Choose your platform', 'Xbox 360', 'NDS', 'Wii', 'PS3', 'PC/Mac', '3DS'};
    public List<Public_Product__c> popularGames { get; set; }
    public Boolean renderPopularGames = false;
    public Boolean getRenderPopularGames()
    {
    	System.debug('Render Popular Games...' + renderPopularGames);
    	return renderPopularGames;
    }
    
    public Boolean renderGamesSearch = false;
    public Boolean getRenderGamesSearch()
    {
    	System.debug('Render Games Search...' + renderGamesSearch);
    	return renderGamesSearch;
    }
    
    public String addProdId { get; set; }
    public String delProdId { get; set; }
    public List<Asset_Custom__c> listMyGames { get; set; }
    public Boolean renderMyGames = false;
    public Boolean getRenderMyGames()
    {
    	System.debug('Render My Games...' + renderMyGames);
    	return renderMyGames;
    }
    
    Map<Id, Asset_Custom__c> mapAssetsById = new Map<Id, Asset_Custom__c>();
    
    /**
     * sumankrishnasaha
     * 04-Mar-12
     */
    public string[] popularProductName {
        set;
        get;
    }
    boolean showingLiveSupportPanel = false;
    public boolean getShowingLiveSupportPanel() {
        return showingLiveSupportPanel;
    }
    boolean showingChatPanel = false;
    public boolean getShowingChatPanel() {
        return showingChatPanel;
    }
    public string gameName {
        set;
        get;
    }
    public string gamePlatform {
        set;
        get;
    }
    
    List<Public_Product__c> publicProductList;
    public List<Public_Product__c> getpublicProductList() {
        return publicProductList;
    }
    List<Public_Product__c> publicProductListFull;
    public List<Public_Product__c> publicProductListFull() {
        return publicProductListFull;
    }
    public Id popularProductId {
        get;
        set;
    }
    
    public Atvi_Games_Controller(ATVICustomerPortalController conExtn)
    {
        listMyGames                     = new List<Asset_Custom__c>();
        map<id, Integer> productTitleMap= new map<id, Integer>();
        map<id, Public_Product__c> productObjMap=new map<id, Public_Product__c>();
        List<id> popularProdsSorted     = new List<id>();
        
        /**
         * Abhishek Pal
         * 04-Mar-12
         */
        /*List<Public_Product__c> productList =     [Select id
                                                    ,   name
                                                    ,   (select id from Asset_Custom__r)
                                                 from public_product__c 
                                                 where Type__c = 'Software'];                                                            
        if(productList.size() > 0) {
            system.debug(productList.size() + ' products found');   // debugging statement
            for(Public_Product__c ppObj:productList){
                productTitleMap.put(ppObj.Id,ppObj.Asset_Custom__r.size());
                productObjMap.put(ppObj.Id,ppObj);
            }
        } else {
            System.debug('No products found');                      // debugging statement
        }
        
        for(Id prodId:productTitleMap.keyset()){
            if(popularProdsSorted.size()>0){
                Integer productRating       = productTitleMap.get(prodId);
                Id worstProductSorted       = popularProdsSorted.get(popularProdsSorted.size()-1);
                Integer worstRatingSorted   = productTitleMap.get(worstProductSorted);
                System.debug('Least popular product : ' + worstProductSorted + ' rated['+worstRatingSorted+']');
                if(productRating<=worstRatingSorted){
                    popularProdsSorted.add(prodId);
                    System.debug('1. Product rating = ' + productRating + ' is less than worst rating ' +  worstRatingSorted);
                }else{
                    Integer indexCounter=0;
                    List<Id> popularProdsSortedTemp=new List<Id>();
                    System.debug('2. Product rating = ' + productRating + ' is NOT less than worst rating ' +  worstRatingSorted);
                    for(Id sortedObjId : popularProdsSorted){
                        if(productRating>productTitleMap.get(sortedObjId)){
                            popularProdsSortedTemp.add(prodId);
                    System.debug('3. $$$');
                        }else{
                            popularProdsSortedTemp.add(sortedObjId);
                    System.debug('4. $$$');
                        }
                        indexCounter++;
                    }
                    popularProdsSorted=popularProdsSortedTemp;
                }
            }else{
                    System.debug('5. $$$');
                popularProdsSorted.add(prodId);
            }
        }
        popularGames=new List<Public_Product__c>();
        for(integer i=0; i<6; i++){
             Id ppObjId = popularProdsSorted.get(i);
             System.debug('Next Object: '+ppObjId);             
             popularGames.add(productObjMap.get(ppObjId));       
        }*/
        
        /**
         * sumankrishnasaha
         * 05-Mar-12
         */
        publicProductList = [
                SELECT Name, Sold_Count__c, Image__c, Platform__c, Chat_Available__c
                FROM Public_Product__c
                WHERE Type__c = 'Software'
                ORDER BY Sold_Count__c DESC LIMIT 6];
        publicProductListFull = [
                SELECT Name, Sold_Count__c, Image__c, Platform__c, Chat_Available__c
                FROM Public_Product__c
                WHERE Type__c = 'Software'];
    }
    
    public List<SelectOption> getPlatformValues()
    {
        List<SelectOption> items = new List<SelectOption>();
        for(String val : platforms)
            items.add(new SelectOption(val, val));
        return items;
    }
    
    public PageReference add_Platform()
    {
        renderPopularGames = true;   
        renderGamesSearch = true;           
        renderMyGames = true;
        popularGames = new List<Public_Product__c>();
        if(selectedPlatform != null && selectedPlatform != 'Choose your platform')
        {
            popularGames = [SELECT Name, Sold_Count__c, Image__c FROM Public_Product__c
            	WHERE Type__c = 'Software' and Platform__c includes (:selectedPlatform) ORDER BY Sold_Count__c DESC LIMIT 4];            
        }
        System.debug('Render Popular Games...' + renderPopularGames);
        System.debug('Render Games Search...' + renderGamesSearch);
        System.debug('Render My Games...' + renderMyGames);
        return null;
    }
    
    public PageReference add_Games()
    {
        System.debug('Product Id...' + addProdId);
        Public_Product__c p = [Select Id, Image__c from Public_Product__c where Id =: addProdId];
        Asset_Custom__c myGame = new Asset_Custom__c(My_Product__c = p.Id, Image__c = p.Image__c, Gamer__c = '003J00000049wni');
        insert myGame;
        mapAssetsById.put(myGame.Id, myGame);
        listMyGames.add(myGame);
        
        return null;
    }    
    
    public PageReference remove_Games()
    {
    	Asset_Custom__c toDelGame = [Select Id from Asset_Custom__c where Id =: delProdId and Gamer__c = '003J00000049wni'];
    	delete toDelGame;
    	mapAssetsById.remove(delProdId);
    	listMyGames.add(mapAssetsById.values());
    	return null;    	
    }
    
    /**
     * sumankrishnasaha
     * 04-Mar-12
     */
    public PageReference showLiveSupportPanels() {
        showingChatPanel = false;
        for (Public_Product__c myPublicProduct : publicProductList) {
            if (myPublicProduct.Id == popularProductId) {
                showingChatPanel = myPublicProduct.Chat_Available__c;
                break;
            }
        }
        showingLiveSupportPanel = true;
        return null;
    }
    
    public PageReference showLiveSupportPanels2() {
    	System.debug('starting showLiveSupportPanels2');
        System.debug('product name = ' + gameName);
        System.debug('product platform = ' + gamePlatform);
        showingChatPanel = false;
        showingLiveSupportPanel = false;
        for (Public_Product__c myPublicProduct : publicProductListFull) {
            if (myPublicProduct.Name == gameName && myPublicProduct.Platform__c.contains(gamePlatform)) {
            	System.debug('found product, chat availability = ' + myPublicProduct.Chat_Available__c);
                showingChatPanel = myPublicProduct.Chat_Available__c;
                showingLiveSupportPanel = true;
                break;
            }
        }
        System.debug('ending showLiveSupportPanels2');
        return null;
    }
    
    /*@RemoteAction
    public static PageReference showLiveSupportPanels1(String myId, String myName) {
        System.debug('public static PageReference showLiveSupportPanels1');
        popularGameName = myName;
        popularProductId= myId;
        showingChatPanel = false;
        for (Public_Product__c myPublicProduct : publicProductList) {
            if (myId == popularProductId) {
                showingChatPanel = myPublicProduct.Chat_Available__c;
            }
        }
        showingLiveSupportPanel = true;
        return null;
    }*/
}