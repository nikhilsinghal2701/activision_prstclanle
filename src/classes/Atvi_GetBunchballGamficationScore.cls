public class Atvi_GetBunchballGamficationScore {

    public id vContactid;
    public boolean isProd;
    public string vUserNameJive;
    public String vPasswordJive;
    public userResponse theuserResponse {get;set;}
    public sessionKeyResponse thesessionKeyResponse {get;set;}
    public sessionKeyResponse thepoints {get;set;}
    
   public Atvi_GetBunchballGamficationScore(){
        isProd = UserInfo.getOrganizationId() == '00DU0000000HMgwMAG';
        isprod=true;
        string vRec='JIVE_UAT';
        if(isProd==true) vRec='JIVE_PROD';
       
        try{
            Integration_Credential__c vObj = [select Username__c , password__c
                                              from Integration_Credential__c where Application_Name__c=:vRec];
            vUserNameJive = vObj.Username__c;
            vPasswordJive = vObj.Password__c;
            system.debug(vUserNameJive+':'+vPasswordJive);
        }
        catch(exception e){}  
    }
    public void getScore(){
        string vPoints;
        string vEmail='';
        try{
            vEmail=[select email from contact where id =:vContactid][0].email;    
        }
        catch(exception e){}
        if(vEmail!=null)
            getUserid(vEmail);
        if(theuserResponse!=null&&theuserResponse.id!=null){
            getBunchballSession();
        }
        if(thesessionKeyResponse!=null&&thesessionKeyResponse.nitro !=null&&thesessionKeyResponse.nitro.login !=null&&thesessionKeyResponse.nitro.login.sessionkey!=null){
            
            HTTP h = new HTTP();
            HTTPRequest r = new HTTPRequest();
            r.setEndpoint('https://solutions.nitro.bunchball.net/nitro/json?userid='+theuserResponse.id+'&method=user.getPointsBalance&sessionKey='+thesessionKeyResponse.nitro.login.sessionkey+'&apiVersion=4.4');
            r.setMethod('GET');
            HTTPResponse resp = h.send(r);
            //system.debug('>>>>>'+resp);
            System.debug(resp.getBody());
            thepoints=(sessionKeyResponse) JSON.deserialize(resp.getBody(), sessionKeyResponse.class);
            System.debug(thepoints);
            //contact vConObj = new Contact(id=vContactid);
           // vConObj.Gamification_Score__c=decimal.valueof(thepoints.nitro.balance.lifetimebalance);
           // update vConObj;
        } 
    }
    public  void getUserid(String vEmail){
        
        HTTP h = new HTTP();
        HTTPRequest r = new HTTPRequest();
        if(isprod) r.setEndpoint('https://community.activision.com/api/core/v3/people/email/'+vEmail);
        else r.setEndpoint('https://community.uat.activision.com/api/core/v3/people/email/'+vEmail);//jivesoftware.com@localhost
        system.debug(vUserNameJive+':'+vPasswordJive);
        Blob headerValue = Blob.valueOf(vUserNameJive+':'+vPasswordJive);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        r.setHeader('Authorization', authorizationHeader);
        r.setMethod('GET');
        HTTPResponse resp = h.send(r);
        system.debug('>>>>>'+resp);
        System.debug(resp.getBody());
        theuserResponse=(userResponse) JSON.deserialize(resp.getBody().split(';')[1], userResponse.class);
        system.debug('>>>>>'+theuserResponse);
    }
    public void getBunchballSession(){
        HTTP h = new HTTP();
        HTTPRequest r = new HTTPRequest();
        string vBody;
        if(isprod) r.setEndpoint('https://community.activision.com/community/points/leaderboard-old');
        else r.setEndpoint('https://community.uat.activision.com/community/points/leaderboard-old');
        r.setMethod('GET');
        HTTPResponse resp = h.send(r);
        vBody=resp.getBody().split('jive.nitro.widget.individualLeaders.Main\\(')[1].split('\\);')[0].replaceall('\\"','');
        system.debug('>>>>>'+vBody);
        String vApikey = vBody.split(',')[0].split(':')[1];
        String vuserid = vBody.split(',')[5].split(':')[1];
        system.debug(vuserid+'>>>>>'+vApikey);
        
        h = new HTTP();
        r = new HTTPRequest();
        r.setEndpoint('https://solutions.nitro.bunchball.net/nitro/json?method=user.login&apiKey='+vApikey.trim()+'&userId='+vuserid.trim());
        r.setMethod('GET');
        resp = h.send(r);
        //system.debug('>>>>>'+resp);
        System.debug(resp.getBody());
        thesessionKeyResponse=(sessionKeyResponse) JSON.deserialize(resp.getBody(), sessionKeyResponse.class);
        system.debug('>>>>>'+thesessionKeyResponse.nitro.login.sessionkey);
    }
    public class userResponse{
        string id{get;set;}
    }
    public class sessionKeyResponse{
        public sessionKeyResponsechild1 Nitro{get;set;}
    }
    public class sessionKeyResponsechild1{
        public sessionKeyResponsechild2 login{get;set;}
        public sessionKeyResponsechild3 Balance{get;set;}
    }
    public class sessionKeyResponsechild2{
        string sessionKey{get;set;}
    }
    public class sessionKeyResponsechild3{
        public string lifetimeBalance{get;set;}
    }
}