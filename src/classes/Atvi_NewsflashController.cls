/*******************************************************************************
 * Name         -   Atvi_NewsflashController
 * Description  -   This class is the controller for Atvi_Newsflash with bellow functions :-
 *                  SearchAlerts: Remote Action to Query Newsflash
 *                  NewsFlashDelivered: Remote Action to Create Newsflash delivered records 
 * Modification Log :
 * -----------------------------------------------------------------------------
 * Developer            Date        Description
 * -----------------------------------------------------------------------------
 * Sandeep             7/10/2014     Created
 *---------------------------------------------------------------------------------------------------
* Review Log  :
*---------------------------------------------------------------------------------------------------
* Reviewer                      Review Date            Review Comments
* --------                      ------------          --------------- 
* 
********************************************************************************************************/

global with sharing class Atvi_NewsflashController {
    
    public static List<Newsflash__c> todaysNewsflashList {get;set;}
    public Newsflash__c fromDate{get;set;}
    public Newsflash__c toDate{get;set;}
    public Atvi_NewsflashController(){
        try{
            todaysNewsflashList = [select id,Name, Subject__c, Content__c, Requested_Date_Time__c FROM Newsflash__c WHERE Requested_Date_Time__c >:datetime.valueof(''+date.today()) and Requested_Date_Time__c < :system.now() and Approval_Status__c='Approved' ];
        }
        catch(exception e){system.debug('Error in constructor'+e);}
    }
     public void reQuery(){
        try{
            todaysNewsflashList = [select id,Name, Subject__c, Content__c, Requested_Date_Time__c FROM Newsflash__c WHERE Requested_Date_Time__c >:datetime.valueof(''+date.today()) and Requested_Date_Time__c < :system.now() and Approval_Status__c='Approved' ];
        }
        catch(exception e){system.debug('Error in reQuery'+e);}
    }
     @RemoteAction
    global static string SearchAlerts( List<Newsflash__c> Tl) {
        String newsflasIds='';
        List<Newsflash__c> newsflashList =new List<Newsflash__c>();
        try{        
            newsflashList = [SELECT id,Name, Subject__c, Content__c, Requested_Date_Time__c 
                       FROM Newsflash__c WHERE Requested_Date_Time__c > :(system.now()).addMinutes(-1) 
                       and Requested_Date_Time__c < :system.now() and Approval_Status__c='Approved' ];
            if(newsflashList.size()>0)
                for(Newsflash__c nfObj:newsflashList){
                    newsflasIds+=nfObj.id+',';
                }
        }
         catch(exception e){system.debug('Error in SearchAlerts'+e);}
        return newsflasIds;
    }
    @RemoteAction
    global static void NewsFlashDelivered(id nfId ) {
        Newsflash_Delivered__c dObj = new Newsflash_Delivered__c();
        try{
            dObj.Agent__c=userinfo.getUserId();
            dObj.Delivered_Time__c=datetime.now();
            dObj.Newsflash__c=nfId ;
            dObj.Unique_id__c=userinfo.getUserId()+'_'+nfId+'_'+datetime.now().format('dd-MMM-yyyy hh:mm');
            upsert dObj;
        }
         catch(exception e){system.debug('Error in NewsFlashDelivered'+e);}
    }
}