public with sharing class Atvi_NewsflashViewed {
    public id newsflashId;
    public id userid ;
    public string param{get;set;}
    
    public Atvi_NewsflashViewed(ApexPages.StandardController controller) {
        try{
             newsflashId=ApexPages.currentPage().getParameters().get('id');
             userid=UserInfo.getUserId();
            }
          catch(exception e){system.debug('Error in creating NF viewed constructor '+e);}      
    }
    public void CreateNewsflashViewedRec(){
        Newsflash_Viewed__c nfvObj;
        try{
            if(newsflashId!=null){
                list<Newsflash_Viewed__c>nfvlist=[select id,View_Count__c from Newsflash_Viewed__c where agent__c=:userid and newsflash__c=:newsflashId];
                if(nfvlist.size()>0){
                    nfvObj = nfvlist[0];
                    nfvObj.View_Count__c=nfvObj.View_Count__c+1;
                    update nfvObj ;
                }   
                else{
                    nfvObj = new Newsflash_Viewed__c();
                    if(param=='1'){
                        List<newsflash_delivered__c> nfList=[select id from newsflash_delivered__c where agent__c=:userid  and newsflash__c=:newsflashId];
                        system.debug('>>>>'+nfList);
                        if(nfList.size()>0)nfvObj.Newsflash_Delivered__c=nfList[0].id;
                    } 
                    nfvObj.Viewed_Time__c=datetime.now();
                    nfvObj.Newsflash__c=newsflashId;
                    nfvObj.agent__c=userid ;
                    nfvObj.View_Count__c=1;
                    insert nfvObj ;
                }
            }
          }
          catch(exception e){system.debug('Error in creating NF viewed records '+e);}
    }
}