/**
* @author           Abhishek
* @date             12-APR-2012
* @description      This controller is used in the Atvi_Get_live_support component to supply the
                    gamer information to the component.
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* A. Pal                    12-APR-2012             Created
* Q. Tran                   6-Jun-2012              Added EN code diversification
* ---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                     Review Comments
* --------                                          ------------                    --------------- 
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                     Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
public class Atvi_chat_controller {
        
    public String gamerName {get;set;}
    public String gamerEmail {get;set;}
    public String gamerUserName {get;set;}
    public String gamerEliteStatus {get;set;}
    public String gamerId {get;set;}
    public String UserLang {get;set;}
    public String eliteStatus{get;set;}
    public boolean isWeekendUK {get;set;}
    public boolean isWeekendAU {get;set;}
    
    public Atvi_chat_controller() {
        getUserLang();
        getWeekend();
        String contactId;   
        //Querying the logged-in gamer information  
        User thisUser = [SELECT Id, FirstName, Country, Contact.Id, 
                         Contact.Name, Contact.Email from User 
                         WHERE Id =: UserInfo.getUserId()];
                        
        if(thisUser != null) {
            //  if Contact ID is not null, then the user is a customer portal user
            if(thisUser.Contact.Id != null) {
                contactId = thisUser.Contact.Id;
                System.debug('Contact record found for user : ' + thisUser.Id);             
            }else {
                System.debug('No Contact record found for user : ' + thisUser.Id);
                contactId = null;
            }           
        }        
       
        if(contactId != null) {
                List<Contact> gamers = [SELECT Id, Name, Salutation, LastName, 
                                        FirstName, Email, Elite_status__c, 
                                        MailingCountry, Gamertag__c, Country__c
                                        FROM Contact 
                                        WHERE Id =: contactId limit 1];
                
                if(gamers.size() > 0) {
                    Contact gamer = gamers.get(0);
                    //Logic to greet the gamer in the component
                    gamerName = AtviUtility.checkNull(gamer.Name);
                    gamerEmail = AtviUtility.checkNull(gamer.Email);  
                    String mailingCountry = AtviUtility.checkNull(gamer.MailingCountry); 
                    //If the gamer is of Elite status, greet by name in the Gamertag                    
                    if(gamer.Elite_Status__c != null) {
                        gamerUserName = AtviUtility.checkNull(gamer.Gamertag__c);
                        gamerEliteStatus = AtviUtility.checkNull(gamer.Elite_Status__c);
                    }
                    //If Gamer country is US, greet by FirstName
                    else if(mailingCountry.contains('US') || mailingCountry.contains('States')) {
                        gamerUserName = AtviUtility.checkNull(gamer.firstname);
                    }
                    //If any other country, greet by Mr./Mrs. LastName
                    else {
                        gamerUserName = AtviUtility.checkNull(gamer.salutation) + ' ' + AtviUtility.checkNull(gamer.lastname);
                    }
                }                
                system.debug('Username is : ' + gamerUserName);
        }else {
            system.debug('sfContactId is null');
            gamerUserName = 'Gamer!';
        }
    }
    //6-23-12 QT This pulls the raw language cookie untruncated to diversify en language codes
    public void getUserLang()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            system.debug('tempCookie not null');
            userLang = tempCookie.getValue();            
        }else{
            system.debug('tempCookie is null');
            userLang = 'en_US';
        }           
    }
    public void getWeekend()
    {
        if(datetime.now().format('E','ACT') == 'Sat' || datetime.now().format('E','ACT') == 'Sun')
        {
            isWeekendAU = true;
        }
        else
        {
            isWeekendAU = false;
        }
        
        if(datetime.now().format('E','GMT') == 'Sat' || datetime.now().format('E','GMT') == 'Sun')
        {
            isWeekendUK = true;
        }
        else
        {
            isWeekendUK = false;
        }
    }
}