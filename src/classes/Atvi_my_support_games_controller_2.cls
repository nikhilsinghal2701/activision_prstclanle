/**
* @author           Suman
* @date             12-APR-2012
* @description      This is controller is used in the Atvi_my_support_games page to add/remove
                                        games for the authenticated gamer.
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Suman                                         12-APR-2012                             Created
* A. Pal                    13-APR-2012             Added debug and try catch statements 
*---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                                                                      Review Date                                             Review Comments
* --------                                                                                      ------------                                    --------------- 
* A. Pal(Deloitte Senior Consultant)                            19-APR-2012                                             Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
public with sharing class Atvi_my_support_games_controller_2 {

    public PageReference Saved() {
        return null;
    }
    public boolean check{get;set;}
    
    public void extractGamepanel(){
    check = true;
    }
    
    public Id sfContactId;
    List<Public_Product__c> productList;
    List<String> picklistValues;
    public Set<Id> myGamesId;
    public List<Asset_Custom__c> tempList;
    public List<Asset_Custom__c> myclicks;
    public List<String> selectedplatforms;
    public boolean displayPopup{get;set;}
    public Map<String, List<Asset_Custom__c>> mapGamesByPlatform;
    public Map<String, List<Asset_Custom__c>> getMapGamesByPlatform()
    {
      return mapGamesByPlatform;
    }
      public String[] productNames;
      public String[] getProductNames() {
            return productNames;
      }
      public String productName {
            get;
            set;
      }
      public String productPlatform {
            get;
            set;
      }
      
              /*Used to set the language of the Apex page acccording to the user's choice*/    
            public String lang {get;set;}
            public void getUserLanguage()
            {
                Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
                if(tempCookie != null){
                    system.debug('tempCookie not null');
                    lang = tempCookie.getValue();            
                }else{
                    system.debug('tempCookie is null');
                    lang='en_USDefault';
                }
            }
      
      public Atvi_my_support_games_controller_2() { 
                check = false;
                getUserLanguage();           
        //Logic to retrieve the contact id of the logged-in gamer    
        User thisUser = [SELECT Id, FirstName, Country, Contact.Id, 
                                         Contact.Name, Contact.Email FROM User 
                         WHERE Id =: UserInfo.getUserId()];
                        
        if(thisUser != null) {
            //if Contact ID is not null, then the user is a customer portal user
            if(thisUser.Contact.Id != null) {
                sfContactId = thisUser.Contact.Id;
                System.debug('Contact record found for user : ' + sfContactId);             
            }else {
                System.debug('No Contact record found for user : ' + thisUser.Id);
                sfContactId = null;
            }
        }
        List<Asset_Custom__c> myGamesIdAsset = [SELECT My_Product__c FROM Asset_Custom__c WHERE Gamer__c = :sfContactId];
        myGamesId = new Set<Id>();
        for (Asset_Custom__c asset : myGamesIdAsset) {
                myGamesId.add(asset.My_Product__c);
        System.debug('All assets: ' + asset.My_Product__c);
        }
        //Retrieving the Platform picklist values using describe calls
        picklistValues = new List<String>();
        for(Schema.PicklistEntry p : Public_Product__c.Platform__c.getDescribe().getPicklistValues()) {
              picklistValues.add(p.getValue());
        }
        //Method to display games owned by the logged-in gamer in a map  
        populateMyGamesmap();
        
        //Querying the public product table for predictive search
        //except the games owned by the logged-in gamer
        productList = new List<Public_Product__c>();
              productList = [SELECT Name, Data_Category__c, Platform__c 
                                         FROM Public_Product__c 
                                         WHERE Type__c = 'Software' AND Id NOT in :myGamesId];
              System.debug('productList = ' + productList);
              productNames = new String[0];
              for (Public_Product__c productObject: productList) {         
                    productNames.add(String.escapeSingleQuotes(productObject.Name + ', ' + productObject.Platform__c));
              }      
      }      
      
    //Getter method for Platform picklist values
        public List<String> getPlatformValues() {
    List<String> items = new List<String>();
    for(String val : picklistValues) {
        items.add(val);
    }
    return items;
    }
    
    //Method to populate games owned by the logged-in gamer in a map
    private void populateMyGamesmap(){
        mapGamesByPlatform = new Map<String, List<Asset_Custom__c>>();
        List<Asset_Custom__c> assets = [SELECT Id, My_Product__c, 
                                                                        My_Product__r.Data_Category__c, 
                                                                        My_Product__r.Name, 
                                                                        My_Product__r.Platform__c,
                                                                        My_Product__r.Redesign_Image_Value__c,
                                                                        My_Product__r.Redesign_Image__c,
                                                                        My_Product__r.Image__c, Platform__c
                                                                        FROM Asset_Custom__c 
                                                                        WHERE Gamer__c = :sfContactId 
                                                                        AND My_Product__r.Type__c = 'Software' 
                                                                        ORDER BY CreatedDate DESC];
           
                        if(assets.size()>0) {
                                system.debug(assets.size()+' games added to the gamer ['+sfContactId+']');
                                List<Asset_Custom__c> tempList;
                                //Add the games in a map with Platform as key
                                for(Asset_Custom__c a:assets) {
                                        if(!mapGamesByPlatform.containsKey(a.Platform__c)) {
                        tempList = new List<Asset_Custom__c>();
                        tempList.add(a);
                        mapGamesByPlatform.put(a.Platform__c, tempList);
                    }else {
                        tempList = mapGamesByPlatform.get(a.Platform__c);
                        tempList.add(a);
                        mapGamesByPlatform.put(a.Platform__c, tempList);
                    }    
                                }       
                                system.debug('Size of mapGamesByPlatform='+mapGamesByPlatform.size());                  
                        }else {
                                system.debug('No game added to the gamer ['+sfContactId+']');
                        }
        
    }
    //method for PopUp Display and Hide
    public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup(){
        displayPopup = true;
    }   
    public pagereference supportCases(){
    pagereference p = new pagereference('/apex/AtviMyCasesPage');
    p.setredirect(true);
    return p;
    }

    //Method to add more games for the logged-in gamer
    public PageReference addToMyGames() {
      System.debug('To add:' + productName + productPlatform);
      List<Public_Product__c> productList = [SELECT Id, Data_Category__c 
                                                                                 FROM Public_Product__c WHERE 
                                                                                 Name =: productName AND 
                                                                                 Platform__c includes (:productPlatform)];
      List<Asset_Custom__c> myNewGames = new List<Asset_Custom__c>();
      for (Public_Product__c myId : productList) {
            System.debug('Asset Custom pair:' + myId.Id + ' ' + sfContactId);
            myNewGames.add(new Asset_Custom__c(My_Product__c = myId.Id, Gamer__c = sfContactId, Platform__c=productPlatform));
      }
      
      try {
        insert myNewGames;
        system.debug(myNewGames.size()+' rows inserted in Asset_Custom__c');
      }catch(dmlException dmle) {
        system.debug(dmle.getMessage());
      }      
      populateMyGamesmap();
      
      displayPopUp = false;
      pagereference p = new Pagereference('/apex/atvi_my_support_games');
      p.setredirect(true);
      return p;
      
    }
    
    //Method to remove games for the logged-in gamer
    public PageReference removeFromMyGames() {
      System.debug('To remove:' + productName + productPlatform);
      List<Asset_Custom__c> assetList = [SELECT Id, Platform__c,  
                                                                         My_Product__r.Name FROM Asset_Custom__c 
                                                                         WHERE Gamer__c =: sfContactId AND 
                                                                         Platform__c =: productPlatform AND  
                                                                         My_Product__r.Name =: productName];
      
      try {
        delete assetList;
        system.debug(assetList.size()+' rows deleted from Asset_Custom__c');
      }catch(dmlException dmle) {
        system.debug(dmle.getMessage());
      }     
      populateMyGamesmap();      
      return null;
    }
    
      
}