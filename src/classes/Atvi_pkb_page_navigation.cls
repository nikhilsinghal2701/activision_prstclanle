public without sharing class Atvi_pkb_page_navigation { //NO DML only calculates number of pages for this article type

	public string pLang {get; set;}
	public integer pPageSize {get; set;}
	public string pCategories {get; set;}
	public integer pCurrentPageNumber {get; set;}
	string[] results;
	public string[] getPageNumbers(){
		results = new string[]{};
		try{
			pCategories = pCategories.trim();
			pCategories = pCategories.replaceAll(' ','_');
			if(pCategories.contains(':')) pCategories = pCategories.split(':')[1];
			if(! pCategories.containsIgnoreCase('__c')) pCategories += '__c'; //assuming only 1 category 

			String artCountQry = 'SELECT count() FROM KnowledgeArticleVersion WHERE PublishStatus=\'Online\' AND Language = :pLang'; 
			artCountQry += ' WITH DATA CATEGORY Game_Title__c AT ('+pCategories+',All__c)';
			system.debug('artCountQry == '+artCountQry);

			Integer numOfArts = database.countQuery(artCountQry);
			Integer numberOfPages = Integer.valueOf(
				math.floor(decimal.valueOf(numOfArts) / decimal.valueOf(pPageSize))
			);
			Integer startNumber;
			Integer maxPages = 5;
			if((pCurrentPageNumber - 2) <= 1){ //at the beginning of the list
			    startNumber = 1;
			}else if((pCurrentPageNumber + 2) >= numberOfPages){ //at end of list
			    startNumber = (numberOfPages + 1) - maxPages;
			}else{ //in middle, start from -2
			    startNumber = pCurrentPageNumber - 2;
			}
			if(numberOfPages < maxPages) maxPages = numberOfPages;

			do{
			    results.add(String.valueOf(startNumber++));
			}while(results.size() < maxPages);
		}catch(Exception e){
			system.debug('ERROR == '+e);

		}
		system.debug('KB Article Page number list == '+results);
		return results;
	}

	
}