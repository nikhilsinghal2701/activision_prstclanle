/**
* @author           Suman
* @date             02-MAR-2012
* @description      This controller is used to provide the public products to be used as predictive
                    search in the Atvi_public_product_search component.
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Suman                     02-MAR-2012             Created
* F.Warb                    10-OCT-2013             Added hardware / software groupings
* F.Warb                    02-DEC-2013             Query only executed once specific values are chosen by user
*---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                     Review Comments
* --------                                          ------------                    --------------- 
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                     Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
public with sharing class Atvi_product_controller {
    
    public string[] specificResults {get; set;}
    public string param1 {get; set;}
    public string param2 {get; set;}
    public void assignParam1(){} //not necessary to do anything here, the VF passes the value into param1 
    public pageReference doQuery(){
        specificResults = new string[]{};

        //Querying the public products with condition Available for Self Service
        //as True. Only these public products are available for Warranty and Returns process.
        String query = 'SELECT Name, Platform__c, Type__c, SubType__c FROM Public_Product__c WHERE (Available_for_Self_Service__c = True AND ';
        query += 'Type__c = \''+param1+'\' AND ';
        if(param1 == 'software'){
            query += 'Platform__c INCLUDES (\'';
            query += string.escapeSingleQuotes(param2)+'\'))';
        }else{
            //skylander figure //main //other
            query += 'SubType__c = \'';
            if(param2 == 'skylander figure') query += 'skylander figure\')';
            else if(param2 == 'main') query += 'main\' AND Name LIKE \'%portal\')';
            else query += '\') OR (SubType__c = \'main\' AND (NOT Name LIKE \'%portal\'))';
        }
        
        system.debug('query == '+query);
        for (Public_Product__c prod : database.query(query)) {
            specificResults.add(String.escapeSingleQuotes(prod.Name) + ', '+ prod.Platform__c);            
        }
        system.debug('specificResults.size == '+specificResults.size());
        return null;
    }
}