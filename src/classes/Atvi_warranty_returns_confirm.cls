/**
* @author           A. PAL
* @date             02-MAY-2012
* @description      This controller is used to fetch the required data to display in the Atvi_rma_confirmation page
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* A. PAL                    02-MAY-2012             Created
* G. WAR                   13-NOV-2012            Added current user filter onto query
* ---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                     Review Comments
* --------                                          ------------                    --------------- 
* Karthik(Deloitte Consultant)                      03-MAY-2012                     Final Inspection
*---------------------------------------------------------------------------------------------------
*/
public with sharing class Atvi_warranty_returns_confirm {
    
    public Case rmaCase{get; set;}
    
    public Atvi_warranty_returns_confirm(){
        User curUser = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId()];
        String caseId=ApexPages.currentPage().getParameters().get('caseId');        
        
        if(caseId!=''){
            system.debug('Case number sent in request parameter:'+caseId);
            List<Case> rmaCases=[select id
                                    , RecordTypeId
                                    , Product_Effected__r.Image__c
                                    , Product_Effected__r.Name
                                    , Product_Effected__r.Platform__c 
                                    , ContactId
                                    , Origin
                                    , Attention__c
                                    , Address_1__c
                                    , Address_2__c
                                    , City__c
                                    , State__c
                                    , Zipcode__c
                                    , Country__c
                                    , Problem_Description__c
                                    , Subject
                                    , CaseNumber
                                    , Phone__c
                                    , Ship_To__c
                                from Case
                                where id=:caseId AND
                                    ContactId = :curUser.ContactId
                                ];
                                
            if(rmaCases.size()==1){
                rmaCase=rmaCases.get(0);
            }else{
                system.debug('rmaCases.size()='+rmaCases.size());
            }
        }else{
            system.debug('No case number sent in request parameter');
        }
    }
    
    public String userLang;
    //Returns the language selected from the language cookie
    public String getUserLang() {
        String tempLang;
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null)
            tempLang = tempCookie.getValue();            
        else tempLang = 'en_US';
        
        if(tempLang.contains('en'))
            userLang = 'en_US';
        else if(tempLang.contains('de'))
            userLang = 'de';
        else if(tempLang.contains('fr'))
            userLang = 'fr';
        else if(tempLang.contains('it'))
            userLang = 'it';
        else if(tempLang.contains('es'))
            userLang = 'es';
        else if(tempLang.contains('sv'))
            userLang = 'sv';
        else if(tempLang.contains('nl'))
            userLang = 'nl_NL';
        else if(tempLang.contains('pt'))
            userLang = 'pt_BR';         
        
        return userLang;
    }
    
    /*public static testmethod void testAtvi_warranty_returns_confirm(){
    
        CheckAddError.addError = false;        
        Contact tester=new Contact();
        tester.FirstName='FName';
        tester.LastName='LName';
        tester.Languages__c='TEST';
        insert tester;
        
        Public_Product__c testProduct=new Public_Product__c();
        testProduct.Name='Test Game';
        testProduct.Platform__c='Test Platform';
        insert testProduct;
        
        Case testCase=new Case();
        testCase.Product_Effected__c=testProduct.Id;
        testCase.ContactId=tester.Id;
        testCase.Origin='Web';
        testCase.Attention__c='Tester';
        testCase.Address_1__c='123 Street';
        testCase.City__c='test city';
        testCase.State__c='XX';
        testCase.Zipcode__c='1234';
        testCase.Country__c='Test';
        testCase.Problem_Description__c='Dummy Problem';
        testCase.Subject='test Subject';
        insert testCase;        
        
        Test.startTest();
            Atvi_warranty_returns_confirm testConfirmFail=new Atvi_warranty_returns_confirm();
            system.debug('Language selected='+testConfirmFail.getUserLang());
            
            ApexPages.currentPage().getParameters().put('caseId',testCase.Id);
            Atvi_warranty_returns_confirm testConfirm=new Atvi_warranty_returns_confirm();
           // Avoid dropping cookies in test classes. But in case the test coverage is not good...
            //ApexPages.currentPage().getCookies().put(new Cookie('uLang','de_DE'));
            //system.debug('Language selected='+testConfirmFail.getUserLang());
                  
        Test.stopTest();
    }*/

}