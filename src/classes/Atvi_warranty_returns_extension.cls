/**
* @author           Karthik
* @date             08-APR-2012
* @description      This controller is used to fetch the required data for the Warranty and Returns process.
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Karthik                   08-APR-2012             Created
* A. Pal                    02-MAY-2012             Modified submitReview() method to redirect to Atvi_rma_confirmation page
* ---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                     Review Comments
* --------                                          ------------                    --------------- 
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                     Final Inspection before go-live
* Achin Suman                                       25-APR-2012                     EQA
*---------------------------------------------------------------------------------------------------
*/
public with sharing class Atvi_warranty_returns_extension {  
    public final static String rmaCaseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.RMA_Case_Record_Type).getRecordTypeId();
    public Contact sfContact {get; set;}
    public string modelNo {get; set;}
    public integer digits{get;set;}
    
    public Boolean isAustralia{set;}
    public Boolean getisAustralia() {
        String tempLang;
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null)
            tempLang = tempCookie.getValue();
            if (tempLang=='en_AU')
            {
                return true;
            }
            else{
                return false;
            }         
    }
    
    public Boolean isViewAll;
    public Boolean getIsViewAll() {
        return isViewAll;
    }
    public pageReference goToShowAll(){
        pageReference pr = Page.Atvi_Warranty_returns;
        pr.setRedirect(true);
        pr.getParameters().put('showAll','1');
        return pr;
    }
    
    public List<Asset_Custom__c> topSixGames;
    //Retrieve top 12 games owned by the gamer
    public List<Asset_Custom__c> getTopSixGames() {
        List<Asset_Custom__c> tempListGames = [Select My_Product__c, 
                            My_Product__r.Name, My_Product__r.Platform__c, 
                            My_Product__r.Image__c from Asset_Custom__c 
                            where Gamer__c =: sfContact.Id and 
                            My_Product__r.Available_for_Self_Service__c = True 
                            order by CreatedDate Desc];
        isViewAll = tempListGames.size() > 12;
        /*if(tempListGames.size() > 13) {
            isViewAll = true;
            remGames = new List<Asset_Custom__c>();
            for(Integer i=7; i<tempListGames.size(); i++)
                remGames.add(tempListGames[i]);         
        }*/      
        return tempListGames;
    }   
    
    public List<Asset_Custom__c> remGames;
    public List<Asset_Custom__c> getRemGames() {
        return remGames;
    }
    
    public Boolean renderHome = true;
    public Boolean getRenderHome() {
        return renderHome;
    }
    
    public Boolean renderProdInfo = false;
    public Boolean getRenderProdInfo() {
        return renderProdInfo;
    }
    
    public Boolean renderMultiProd = false;
    public Boolean getRenderMultiProd() {
        return renderMultiProd;
    }
    
    public Boolean renderModelCheck = false;
    public Boolean getrenderModelCheck(){
        return renderModelCheck;
    }
    
    public Boolean renderProdError = false;
    public Boolean getrenderProdError(){
        return renderProdError;
    }
    
    public Boolean renderProdError2 = false;
    public Boolean getrenderProdError2(){
        return renderProdError2;
    }
    
    public Id prodId {get; set;}
    public Boolean renderConInfo = false;
    public Boolean getRenderConInfo() {
        return renderConInfo;
    }
    
    public Boolean renderReview = false;
    public Boolean getRenderReview() {
        return renderReview;
    }
    
    public Boolean renderConfirmation = false;
    public Boolean getRenderConfirmation() {
        return renderConfirmation;
    }
    
    public Public_Product__c selPubProd;
    public Public_Product__c getSelPubProd() {
        return selPubProd;
    }
    
    public list<public_product__c> selectedPublicProduct{get;set;}
    
    public String rmaCaseNumber {get; set;}
    public String probDesc {get; set;}
    public String gameName {get; set;}
    public String gamePlatform {get; set;}
    
    public String userLang;
    //Returns the language selected from the language cookie
    public String getUserLang() {
        String tempLang;
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null)
            tempLang = tempCookie.getValue();            
        else{ tempLang = 'en_US';}
        
        //Added AU as special case Non supported and keeps original en_US labeling
        if(tempLang.contains('en'))
            userLang = 'en_US';    
        else if(tempLang.contains('de'))
            userLang = 'de';
        else if(tempLang.contains('fr'))
            userLang = 'fr';
        else if(tempLang.contains('it'))
            userLang = 'it';
        else if(tempLang.contains('es'))
            userLang = 'es';
        else if(tempLang.contains('sv'))
            userLang = 'sv';
        else if(tempLang.contains('nl'))
            userLang = 'nl_NL';
        else if(tempLang.contains('pt'))
            userLang = 'pt_BR';         
        
        return userLang;
    }
    
    public Atvi_warranty_returns_extension() 
    {
        User thisUser = [Select Contact.Id from User 
                         where Id =: UserInfo.getUserId()];
        
        sfContact = [Select Id, Name, FirstName, LastName, Phone,  Address_Line_1__c,
                     Address_Line_2__c, City__c, State__c, Zipcode__c, Country__c, Email, RMA_Country__c, RMA_State__c
                     from Contact where Id =: thisUser.Contact.Id];     
    }
    //Method to fetch information about the selected public product
    public PageReference selectProd() {
       
        selPubProd = [Select Name, Image__c, Platform__c, Model__c, check_Model__c from Public_Product__c 
                      where Id =: prodId];  
        if(selPubProd.check_Model__c){
            renderModelCheck = true;
        }
        renderHome = false;
        renderProdInfo = true;
            if(selPubProd.Model__c != null){
            digits = string.valueof(selPubProd.Model__c).length();
            }
        return null;
    }
    //Method to fetch the public product selected from the predictive search
    public PageReference selectGameController() {
        system.debug('gamePlatform====='+gamePlatform+'******gameName****'+gameName);
        selPubProd = [Select Name, Image__c, Platform__c, Model__c, check_Model__c, rare__c from Public_Product__c 
                      where Name =: gameName and Platform__c =: gamePlatform];
        if(selPubProd.check_Model__c){
            renderModelCheck = true;
        }
        renderHome = false;
        renderProdInfo = true;
        if(selPubProd.Model__c != null){
            digits = string.valueof(selPubProd.Model__c).length();
        } 
        system.debug('renderModelCheck====='+renderModelCheck+'******renderHome****'+renderHome+'&&&&&&&&renderProdInfo '+renderProdInfo);             
        return null;
    }
    public string isSuccess{get;set;}
    //Render Product Information section
    public pagereference submitProdInfo() {
        system.debug('Inside Submit ProdInfo');
        isSuccess = 'ModelChk';
        boolean check;    
        renderModelCheck = true;//added
        if(renderModelCheck){
            system.debug('Inside If Statement ...');

            if(modelNo!='' && modelNo!=null){
                system.debug('Found matching model number...');
                /*if(ModelNo.length()>8){
                    renderProdError = true;
                    return null;
                }
                check=pattern.matches('[0-9]+',modelNo); */
                selectedpublicproduct = [Select id,Name, Image__c, Platform__c, Model__c, check_Model__c,rare__c from Public_Product__c 
                      where Model_Number__c=:modelNo.toLowerCase() and Available_for_Self_Service__c=true];
                if(selectedpublicproduct.size()<1){
                    renderProdError=true;
                    isSuccess = 'ModelChk';
                    return null;
                }
                else if(selectedpublicproduct.size()==1){
                    if(selectedpublicproduct[0].rare__c == true){
                        return new pageReference('/articles/'+getuserlang()+'/FAQ/Returning-Special-Edition-Skylanders');
                        //return page.rare;
                    }
                    if(selectedpublicproduct[0].check_Model__c){
                        list<case> entrycheck = [select id from case where createddate=Last_N_Days:30 and recordtypeid=:rmaCaseRecTypeId and product_effected__r.check_model__c=true];
            
                        if(entrycheck.size()>=1){
                            renderProdError2 = true;
                            return null;
                        }
                    }
                    system.debug('selectedpublicProduct size: 1');
                    selpubprod = selectedpublicproduct[0];
                    system.debug('selpubprod:'+selpubprod);
                    renderProdInfo = true;
                    renderhome = false;
                    isSuccess = 'oid';
                    return null;
                }
                else if(selectedpublicproduct.size()>1){
                    //Only Skylanders
                    list<case> entrycheck = [select id from case where createddate=Last_N_Days:30 and recordtypeid=:rmaCaseRecTypeId and product_effected__r.check_model__c=true];
            
                    if(entrycheck.size()>=1){
                        renderProdError2 = true;
                        return null;
                    }
                    renderMultiProd = true;
                    renderhome=false;
                    isSuccess = 'oid';
                    return null;
                }
            }
            else{
                renderProdError = true;
                return null;
            }
            
            /*if(check){
                if(integer.valueof(modelNo)==selPubProd.Model__c){
                    renderProdInfo = false;
                    renderConInfo = true;
                }
                else{
                    renderProdError=true;
                }
            }
            
            else{
                renderProdError=true;
            }*/
            
        }
        else{
            renderProdInfo = false;
            renderConInfo = true;
        }
        
        return null;
    }
    
    public pagereference ProdInfoContinue(){
        renderprodinfo = false;
        renderconinfo = true;
        return null;
    }
    
    public string param3 {get;set;}
    //Select Multi Option
    public PageReference selectMulti(){
        system.debug('I am inside selectMulti');
        selPubProd = [Select Name, Image__c, Platform__c, Model__c, check_Model__c, rare__c from Public_Product__c 
                      where id=:param3];
        system.debug('selectMulti:'+selPubProd);
        renderConInfo = true;
        renderProdInfo = false;
        renderMultiProd = false;
        return null;
    }
    //Render Contact Information section
    public PageReference submitConInfo() {
        renderConInfo = false;
        renderReview = true;
        return null;
    }
    
    public String shipTo{get;set;}
    public String ccTo{get;set;}
    //Method to create RMA case
    public PageReference submitReview() {       
        renderReview = false;
        renderConfirmation = true;
        PageReference pRef=null;
        Case rmaCase = new Case();
        rmaCase.RecordTypeId = rmaCaseRecTypeId;
        rmaCase.Product_Effected__c = selPubProd.Id;
        rmaCase.ContactId = sfContact.Id;
        rmaCase.Origin = Label.Case_Origin_RMA;
        rmaCase.Attention__c = ccTo;
        rmaCase.Ship_To__c=sfContact.FirstName+ ' '+sfContact.LastName;
        rmaCase.Phone__c=sfContact.Phone;
        rmaCase.Email__c=sfContact.Email;
        rmaCase.Address_1__c = sfContact.Address_Line_1__c;
        rmaCase.Address_2__c = sfContact.Address_Line_2__c;
        rmaCase.City__c = sfContact.City__c;
        rmaCase.State__c = sfContact.RMA_State__c;
        rmaCase.Zipcode__c = sfContact.Zipcode__c;
        rmaCase.Country__c = sfContact.RMA_Country__c;
        rmaCase.Problem_Description__c = probDesc;
        rmaCase.Subject = selPubProd.Name;
        rmaCase.RMA_Lang__c = getUserLang();
        try{
            insert rmaCase;
            rmaCaseNumber = [Select CaseNumber from Case where Id =: rmaCase.Id].CaseNumber;        
            pRef=new PageReference('/apex/Atvi_rma_confirmation?caseId='+rmaCase.id);   
            system.debug('New RMA case created:'+rmaCaseNumber);
        }catch(dmlexception dmle){
            system.debug(dmle.getMessage());
        }       
        return pRef;
    }
    //Render Product Information section
    public PageReference prevProdInfo() {
        renderProdInfo = true;
        renderConInfo = false;
        renderHome = false;     
        return null;
    }
    //Render Contact Information section
    public PageReference prevConInfo() {
        renderConInfo = true;
        renderReview = false;
        renderProdInfo = false;     
        return null;
    }
    
    public string[] specificResults {get; set;}
    public string param1 {get; set;}
    public string param2 {get; set;}
    public void assignParam1(){} //not necessary to do anything here, the VF passes the value into param1 
    public pageReference doQuery(){
        specificResults = new string[]{};

        //Querying the public products with condition Available for Self Service
        //as True. Only these public products are available for Warranty and Returns process.
        String query = 'SELECT Name, Platform__c, Type__c, SubType__c, model__c FROM Public_Product__c WHERE (Available_for_Self_Service__c = True AND ';
        query += 'Type__c = \''+param1+'\' AND ';
        if(param1 == 'software'){
            query += 'Platform__c INCLUDES (\'';
            query += string.escapeSingleQuotes(param2)+'\'))';
        }else{
            //skylander figure //main //other
            query += 'SubType__c = \'';
            if(param2 == 'skylander figure') query += 'skylander figure\')';
            else if(param2 == 'main') query += 'main\' AND Name LIKE \'%portal\')';
            else if(param2 == 'trap') query += 'Trap\')';
            else query += '\') OR (SubType__c = \'main\' AND (NOT Name LIKE \'%portal\'))';
        }
        
        system.debug('query == '+query);
        for (Public_Product__c prod : database.query(query)) {
            specificResults.add(String.escapeSingleQuotes(prod.Name) + ', '+ prod.Platform__c); 
            //specificResults.add(string.valueOf(prod.Model__c));            
        }
        system.debug('specificResults.size == '+specificResults.size());
        return null;
    }
    
    /*public pagereference refreshPage(){
        if(selectedpublicproduct.size()==1){
            if(selectedpublicproduct[0].rare__c == true){
                return page.rare;
            }
        }
        return null;
    }*/
}