public with sharing class Atvi_welcome_message_controller {
  
    public String username{get;set;}
    public String eliteStatus{get;set;}
     
     public Atvi_welcome_message_controller() {
       String userId = UserInfo.getUserId();
          system.debug('User Id = ' + userId);            
          
          if(Userinfo.getUserType().equalsIgnoreCase('CSPLitePortal')) {
           
                  List<User> users = [SELECT Id, Contact.Name, 
                            Contact.Salutation, Contact.LastName, 
                            Contact.FirstName, Contact.Email, 
                            Contact.Elite_Status__c, 
                            Contact.MailingCountry, 
                            Contact.Country__c, Contact.Gamertag__c
                                      FROM User WHERE Id =: userId limit 1];
                  
                  if(users.size()>0) {
                      User u = users.get(0);
                      //Logic to greet the gamer in the component        
                      if(u.Contact != null) { 
                        Contact c = u.Contact; 
                        String mailingCountry = AtviUtility.checkNull(c.MailingCountry); 
                        //If the gamer is of Elite status, greet by name in the Gamertag                               
                        if(c.Elite_Status__c != null) {
                            username = AtviUtility.checkNull(c.Gamertag__c);
                            if(username == '') username = AtviUtility.checkNull(c.firstname);
                            eliteStatus = AtviUtility.checkNull(c.Elite_Status__c);
                        }
                        //If Gamer country is US, greet by FirstName
                        else if(mailingCountry.contains('US') || mailingCountry.contains('States')) {
                            username = AtviUtility.checkNull(c.firstname);
                            if(username == '') username = AtviUtility.checkNull(c.lastname);
                        }
                        //If any other country, greet by Mr./Mrs. LastName
                        else {
                            username = AtviUtility.checkNull(c.salutation) + ' '+AtviUtility.checkNull(c.firstname) + ' ' + AtviUtility.checkNull(c.lastname);
                        }
                      }else {
                        system.debug('No contact record found for the user['+userid+']');
                      }
                  }                
                  system.debug('Username is : ' + username); 
                  system.debug('Elite status is : ' + eliteStatus);                            
          }
  }        
}