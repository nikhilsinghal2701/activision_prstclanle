public with sharing class AuthenticateAmbassador {
    public User thisUser;
    public AuthenticateAmbassador (){
    }
    public pagereference fnRediarect(){
        pagereference p=null;   
        try{
        thisUser=[select id,Contact.isAmbassador__c,Contact.UCDID__c,Username
                        from User 
                        where Id=:UserInfo.getUserId()];
        
        if(thisUser!=null && thisUser.contact!=null && thisUser.contact.isAmbassador__c!=false){        
            if(ApexPages.currentPage().getHeaders().get('Host')!=null && ApexPages.currentPage().getHeaders().get('Host').contains('force.com')) 
                {
                    
                        
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint('https://ambuat-ambassador.cs15.force.com/services/apexrest/Ambassador/'+thisUser.contact.UCDID__c);
                    req.setMethod('GET');
                    Http http = new Http();
                    HTTPResponse res = http.send(req);
                    if(res.getBody().contains('success'))
                       p=new pagereference(System.Label.AmbassadorOrgSSOURL);
                    else{
                        p = Page.AMB_Slots_Full;
                        return p;
                        //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Currently No Slots Available , Please try again later !');
                        //ApexPages.addMessage(myMsg);
                        }
                }     
            else {
            
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint('https://ambassador.secure.force.com/services/apexrest/Ambassador/'+thisUser.contact.UCDID__c);
                    req.setMethod('GET');
                    Http http = new Http();
                    HTTPResponse res = http.send(req);
                    if(res.getBody().contains('success'))
                      p=new pagereference(System.Label.AmbassadorOrgSSOURL);
                    else{
                        p = Page.AMB_Slots_Full;
                        return p;
                        //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Currently No Slots Available , Please try again later !');
                        //ApexPages.addMessage(myMsg);
                        }
                 }
        }
        else p=new pagereference('/AMB_Registration');
        }
        catch(exception e){system.debug('>>>> error in AuthenticateAmbassador >>>>'+e);}
        return p;
    }
}