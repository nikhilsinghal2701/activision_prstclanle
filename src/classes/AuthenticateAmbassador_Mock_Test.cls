@isTest
public class AuthenticateAmbassador_Mock_Test implements HttpCalloutMock {
  
    protected String body;
  

    public AuthenticateAmbassador_Mock_Test(String body) {     
        this.body = body;
     
    }

    public HTTPResponse respond(HTTPRequest req) {        
        HttpResponse res = new HttpResponse();        
        res.setBody(body);
        res.setStatusCode(200);
        return res;
    }

}