@isTest
private class AuthenticateAmbassador_Test {
	
	@isTest static void test_method_one() {

		Contact testcontact = UnitTestHelper.generateTestContact();
		testcontact.isAmbassador__c = true;
		testcontact.UCDID__c = '12345';
		testcontact.Birthdate = Date.today().addYears(-40);
		insert testcontact;
		User testportaluser = UnitTestHelper.getCustomerPortalUser('12345',testcontact.Id);
		insert testportaluser;

		PageReference currentpage = Page.ambassador;
		Test.setCurrentPage(currentpage);

		


        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new AuthenticateAmbassador_Mock_Test('success'));
		system.runAs(testportaluser){
			AuthenticateAmbassador aa = new AuthenticateAmbassador();
			PageReference p1 = aa.fnRediarect();
			system.assertEquals(System.Label.AmbassadorOrgSSOURL,p1.getUrl());
		}

		Test.setMock(HttpCalloutMock.class, new AuthenticateAmbassador_Mock_Test('fail'));
		system.runAs(testportaluser){
			AuthenticateAmbassador aa2 = new AuthenticateAmbassador();
			aa2.fnRediarect();
		}
        

		Test.stopTest();


	}

	@isTest static void test_method_two() {

		Contact testcontact = UnitTestHelper.generateTestContact();
		testcontact.isAmbassador__c = true;
		testcontact.UCDID__c = '12345';
		testcontact.Birthdate = Date.today().addYears(-40);
		insert testcontact;
		User testportaluser = UnitTestHelper.getCustomerPortalUser('12345',testcontact.Id);
		insert testportaluser;

		PageReference currentpage = new PageReference('https://csuat-activisionsupport.cs11.force.com/ambassador');
		currentpage.getHeaders().put('Host','csuat-activisionsupport.cs11.force.com');
		

		


        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new AuthenticateAmbassador_Mock_Test('success'));
		system.runAs(testportaluser){
			Test.setCurrentPage(currentpage);
			system.debug('Current page host is: '+ApexPages.currentPage().getHeaders());
			AuthenticateAmbassador aa = new AuthenticateAmbassador();			
			PageReference p1 = aa.fnRediarect();
			system.assertEquals(System.Label.AmbassadorOrgSSOURL,p1.getUrl());
		}

		Test.setMock(HttpCalloutMock.class, new AuthenticateAmbassador_Mock_Test('fail'));
		system.runAs(testportaluser){
			AuthenticateAmbassador aa2 = new AuthenticateAmbassador();
			aa2.fnRediarect();
		}

		Test.setMock(HttpCalloutMock.class, new AuthenticateAmbassador_Mock_Test('fail'));
		AuthenticateAmbassador aa3 = new AuthenticateAmbassador();
		PageReference p2 = aa3.fnRediarect();
		system.assertEquals('/AMB_Registration',p2.getUrl());

		
        

		Test.stopTest();


	}
	
	
	
}