public with sharing class AutoCompleteController2 {

    public boolean showDelButn { get; set; }
 
    public List<String> Emailstr { get; set; }
    public List<EmailTemplate> EmailList{get;set;}
    public List<Wrapper> lstWarp { get; set; }

    public class Wrapper{
       public boolean check{get; set;}
       public String str {get; set;}
       public Wrapper( boolean b, String s ){
        check =b;
        str = s;
        }     
     }
    
    
    public AutoCompleteController2(){
        showDelButn = true;
        EmailStr = new List<String>();
        EmailList = new List<EmailTemplate>();
        EmailList = [select id,name,subject,timesused,lastUsedDate from EmailTemplate Limit 999 ];
        for(EmailTemplate Estr:Emaillist){
            EmailStr.add(Estr.name);
        }
    }
    
    // Instance fields
    public String searchTerm {get; set;}
    public String selectedMovie {get; set;}
    
    // JS Remoting action called when searching for a movie name
    @RemoteAction
    public static List<EmailTemplate> searchMovie(String searchTerm) {
        System.debug('Movie Name is: '+searchTerm );
        List<EmailTemplate> movies = Database.query('Select Id, Name from EmailTemplate where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\' ' );
        return movies;
    }

    public Set<String> setStr; 
    public PageReference add() {   
          
        if(lstWarp == null && setStr == null){
          lstWarp = new List<Wrapper>();
          setStr = new Set<String>();
         }
        
        if(setStr.isEmpty() && searchTerm !=null && searchTerm !=''){
            setStr.add(String.escapeSingleQuotes(searchTerm));
           
         }else if(!setStr.isEmpty() && searchTerm !=null && searchTerm !=''){
            Boolean msg1 = setStr.add(String.escapeSingleQuotes(searchTerm));
            if(msg1 == false){
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Add another Email Template')); 
               System.debug('..........msg......................');  
            }              
          }else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Select Email Template'));
          }
            
        if(!setStr.isEmpty()){
          lstWarp.clear();
          for(String s: setStr){
            lstWarp.add(new Wrapper(false, s));
          }     
         }
 
        searchTerm ='';
        return null;
    }
    
    public void remove() {
      try{
        Set<String> strgsForStore = new Set<String>();       
        for(Wrapper w:lstWarp){
            if(w.check == false){
                strgsForStore.add(w.str);
            }
        } 
        lstWarp.clear();
        for(String s: strgsForStore){
           lstWarp.add(new Wrapper(false, s));
        }
        setStr.clear();
        setStr.addAll(strgsForStore);
        }catch(Exception e){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Add Email Template'));
        }
        
        finally{
           showDelButn = true;
           System.debug('...........finally.................');
        }
    }  
    
    public void deleteButton() {

        for(Wrapper w:lstWarp){
          if(w.check == true){
             showDelButn = false;
             System.debug('...........deleteButton....true.............');
          } else{
             showDelButn = true;
             System.debug('...........deleteButton....false.............');
          }
        }
    }
    
    public PageReference Export() {
        return page.autocomplete2;
    }
   
    
    public pagereference RunReport() {
        system.debug('#################'+searchTerm+'******');
        if(lstWarp != null || lstWarp.size()>0){      
           List<String> forEmailTempts = new List<String>();
           for(Wrapper w:lstWarp){
              forEmailTempts.add(w.str);
           }      
           EmailList = [select id,name,subject,timesused,lastUsedDate from EmailTemplate where name in :forEmailTempts];       
        }
        else{
        EmailList = [select id,name,subject,timesused,lastUsedDate from EmailTemplate];
        //system.debug('#################'+datee+'******'+Todatee+'+++++++++++++++'+EmailList[0].LastUsedDate+'=============='+EmailList[0].name);
        }
        system.debug('#################******+++++++++++++++'+EmailList);
        
        return page.autocomplete7;
    }


}