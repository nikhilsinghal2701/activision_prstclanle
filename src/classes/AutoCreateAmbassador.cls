global class AutoCreateAmbassador implements Schedulable{   
    
    public static void setSchedule(){
        System.schedule('AutoCreateAmbassador', '0 0 * * * ?', new AutoCreateAmbassador());
    }
    global void execute(SchedulableContext SC){    
        AutoCreate();
    }
    @future(callout=true)
    public static void AutoCreate(){
        list<ambassador_application__c> lapp = new list<ambassador_application__c>();
        string communityname;
        list<contact> ContactAmbUpdate = new list<contact>();
        boolean isProd = UserInfo.getOrganizationId() == '00DU0000000HMgwMAG';
        //datetime dt = datetime.now().addHours(-1);
        list<ambassador_application__c> aal = [select id, First_Name__c, Last_Name__c, email__c,contact__r.id, contact__r.UCDID__c from ambassador_application__c where awaitingtransfer__c=true];
        system.debug(aal);
        RequestNewAmbassador rna = new RequestNewAmbassador();
        if(aal.size()>0){
            for(ambassador_application__c a:aal)
            {
                try{
                    Contact ContactObj = [select id,UCDID__c from contact where id=:a.contact__r.id];
                    
                    system.debug('Contact in For:'+ContactObj.id);
                    //if(theContact.UCDID__c == null || theContact.UCDID__c == '') return new pageReference('/'+theContact.Id+'?nooverride=1');
                    DWSingleIdentity siSvc = new DWSingleIdentity(new ApexPages.standardController(ContactObj));
                    //String accessToken = siSvc.getSFDCClientTokenSynchronously();
                    String accessToken;
                    //**************************************
                    DwProxy_utils dwUtils = new DwProxy_utils();
                    String sessionId = dwUtils.getSessionId('Demonware Promotion Code Id Sync');
                    DwProxy.service svc = new DwProxy.service();
                    svc.timeout_x = 120000;
                    String response;
                    String reqEndpoint = 'https://dev.umbrella.demonware.net/v1.0/tokens/clientcredentials/?client=sfdc';
                    String reqBody = '{"clientSecret":"8a33a363a543a7837e28a09f124073b3"}';
                    
                    if(isProd) reqEndpoint = 'https://prod.umbrella.demonware.net/v1.0/tokens/clientcredentials/?client=sfdc';
                    if(isProd) reqBody = '{"clientSecret":"34ce89d59a8615d267967fc53550fe89aa7dff99d6b8ff54"}';
                    
                    try{
                        response = svc.passEncryptedPostData(reqEndpoint, reqBody, 'application/json', sessionId);
                        SI_UmbrellaTokenResponse resp = (SI_UmbrellaTokenResponse)JSON.deserialize(response, SI_UmbrellaTokenResponse.class);
                        accessToken = resp.accessToken;
                    }catch(Exception e){
                        system.debug('ERROR == '+e);
                    }
                    
                    
                    //**************************************
                    
                    
                    String getUrl = 'https://';
                    if(isProd) getUrl += 'prod';
                    else getUrl += 'dev';
                    getUrl += '.umbrella.demonware.net/v1.0/users/uno/'+ContactObj.UCDID__c+'/?client=sfdc';
                    getUrl += '@headers-exist@@Authorization#Bearer ';
                    getUrl += accessToken;
                    
                    
                    
                    //DwProxy.service svc = new DwProxy.service();
                    svc = new DwProxy.service();
                    if(test.isRunningTest()){
                        response = '{"umbrellaID":20674150,"accounts":[{"username":"JustLock","secondaryAccountID":151603078058342325,"accountID":8802003543715574524,"authorized":false,"provider":"psn"},{"username":"Justlock","accountID":12854756,"authorized":true,"provider":"uno"}]}';
                    }
                    else{
                        response = svc.passEncryptedGetData(getUrl,sessionId); 
                    }
                    system.debug(response);
                    SI_UmbrellaResponse linkedAccountResponse = (SI_UmbrellaResponse) JSON.deserialize(response, SI_UmbrellaResponse.class);
                    if(linkedAccountResponse != null && linkedAccountResponse.accounts != null){ //linked accounts present
                        for(SI_Account linkedAccount : linkedAccountResponse.accounts){
                            if(linkedAccount.provider == 'uno'){
                                communityname = linkedAccount.username;
                            }
                        }
                    }
                    
                    rna.requestambassador('',communityname,EncodingUtil.urlEncode(a.email__c, 'UTF-8'),string.valueof(a.contact__r.UCDID__c));
                    a.awaitingtransfer__c = false;
                    lapp.add(a);
                    ContactObj.isambassador__c = true;
                    ContactAmbUpdate.add(ContactObj);
                    //ContactAmbUpdate.isambassador__c = true;
                }catch(Exception e){
                    system.debug('LAS CALLOUT ERROR == '+e);
                }         
            }
            if(lapp.size()>0){
                update lapp;
                update ContactAmbUpdate;
            }
        }
    }
    
    
    public class SI_UmbrellaTokenResponse{
        public long expires {get; set;}
        public string accessToken {get; set;}
    }
    public class SI_UmbrellaResponse{
        public SI_Account[] accounts {get; set;}
        public String umbrellaID {get; set;}
    }
    public class SI_Account{
        public string accountId {get; set;}
        public string secondaryAccountID {get; set;}
        public boolean authorized {get; set;}
        public string provider {get; set;}
        public string username {get; set;}
    }
    
}