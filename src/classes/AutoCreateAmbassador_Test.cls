@isTest
public class AutoCreateAmbassador_Test {
    static testmethod void test(){
        contact testContact = new Contact(firstname='test',lastname='test',email='test@email.com',ucdid__c = '333');
        insert testContact;
        
        Ambassador_Application__c testapp = new Ambassador_Application__c(
        first_name__c = 'first',
            last_name__c = 'last',
            contact__c = testContact.id,
            uno_id__c = '333',
            status__c = 'Approved',
            email__c = 'test@email.com',
            awaitingtransfer__c = true
        );
        
        insert testapp;
        for(CronTrigger trig : [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name LIKE 'AutoCreateAmbassador']){
            System.abortJob(trig.Id);
        }
        AutoCreateAmbassador.setSchedule();
        AutoCreateAmbassador.autocreate();
    }
}