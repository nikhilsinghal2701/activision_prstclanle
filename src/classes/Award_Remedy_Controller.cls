public class Award_Remedy_Controller
{
	public string ENVIRONMENT = 'PROD';
	
    public Remedy__c currentRemedy { get; set; }
    public List<Remedy__c> previousRemedies { get; set; }
    public boolean Result { get; set; }
    public Profile currentUserProfile { get; set; }
    public string currentUserProfileName { get; set; } 

	public string errorMessage { get; set; }
	public boolean errorOccured { get; set; }

    public Award_Remedy_Controller(ApexPages.StandardController controller)
    {
    	 errorOccured = false;
    	
         currentRemedy = (Remedy__c)controller.getRecord();
         //Soql required fields:
         currentRemedy = [SELECT id, Award__c, Multiplayer_Account__r.DWID__c, DXP_Interval__c, remedyAwarded__c, Multiplayer_Account__r.Platform__c, Multiplayer_Account__r.Gamertag__c, Multiplayer_Account__r.Is_Current__c FROM Remedy__c WHERE id = :currentRemedy.id LIMIT 1];
         
         //Retrieve Name of current user's profile:
    	currentUserProfile = [Select Id, Name From Profile WHERE id = :userinfo.getProfileId()];
    	currentUserProfileName = currentUserProfile.Name; 
    	
    	//Return if MPAccount is not current:
    	if(currentRemedy.Multiplayer_Account__r.Is_Current__c == False)
    	{
    		errorOccured = true;
    		errorMessage = 'Cannot Award Remedy to Non-Current Multiplayer Account';
    		return;
    	}
    	
    	//Return if remedy has already been awarded.
    	if(currentRemedy.remedyAwarded__c)
    	{
    		errorOccured = true;
    		errorMessage = 'Remedy has already been awarded';
    		return;
    	}
    	
    	//Return if DXP Interval is not set:
    	if(currentRemedy.DXP_Interval__c == '' || currentRemedy.Dxp_Interval__c == null)
    	{
    		errorOccured = true;
    		errorMessage = 'You must supply a valid Double XP Interval';
    		return;
    	}
    	
    	//Retrieve Remedies within the last week:
    	DateTime todayDate = datetime.now();
    	DateTime lastWeek = todayDate.addDays(-7);  
    	
    	previousRemedies = [SELECT Id, remedyAwarded__c, Multiplayer_Account__r.DWID__c, LastModifiedDate FROM Remedy__c WHERE Multiplayer_Account__r.DWID__c = :currentRemedy.Multiplayer_Account__r.DWID__c AND LastModifiedDate > :lastWeek AND remedyAwarded__c = True];
    	if(previousRemedies.size() > 0)
    	{
    		if(currentUserProfileName != 'System Administrator' && currentUserProfileName != 'CS - Supervisor ATVI' && currentUserProfileName != 'CS - User Manager')
    		{
	    		//User has recieved a remedy within a week, supervisor approval is required:
	    		errorOccured = true;
	    		errorMessage = currentRemedy.Multiplayer_Account__r.Gamertag__c + ' has received a remedy within 7 days. Please contact a manager if an override is necessary.';
	    		return;
    		}
    	}
    }
    
    public pagereference Award()
    {
        //Set up Service:
        LASService.BasicHttpBinding_ILinkedAccounts service = new LASService.BasicHttpBinding_ILinkedAccounts();
        
        //Translate DXP Interval:
        integer DXP_Minutes = 0;
        if(currentRemedy.DXP_Interval__c == '1 Hour')
        {
            DXP_Minutes = 60;
        }
        else if(currentRemedy.DXP_Interval__c == '2 Hours')
        {
            DXP_Minutes = 120;
        } 
        
        //Translate platform
        string Platform = '';
        if(currentRemedy.Multiplayer_Account__r.Platform__c == 'XBL')
        {
        	Platform = 'xbox';
        }
        else if(currentRemedy.Multiplayer_Account__r.Platform__c == 'PSN')
        {
        	Platform = 'ps3';	
        }
        else
        {
        	errorOccured = true;
			errorMessage = 'Invalid Platform';
			return null;
        }
        
        if (currentRemedy.Award__c == 'Double XP')
        {
            try
            {
            	if(ENVIRONMENT == 'PROD')
            	{
            		//If System is running test, return testResponse:
            		if(System.Test.isRunningTest())
            		{
            			if (currentRemedy.Multiplayer_Account__r.DWID__c == '123')
            			{
            				Result = True;
            			}
            			else
            			{
            				Result = False;
            			}
            		}
            		//Otherwise, award user:
            		else
            		{
                		Result = service.AwardDoubleXP_PROD(currentRemedy.Multiplayer_Account__r.DWID__c, DXP_Minutes, Platform);
            		}
            	}
            	else if(ENVIRONMENT == 'DEV')
            	{
            		if(System.Test.isRunningTest())
            		{
            			if (currentRemedy.Multiplayer_Account__r.DWID__c == 'xyz')
            			{
            				Result = True;
            			}
            			else
            			{
            				Result = False;
            			}
            		}
            		else
            		{
            			Result = service.AwardDoubleXP_DEV(currentRemedy.Multiplayer_Account__r.DWID__c, DXP_Minutes, Platform);
            		}
            	}
            	
                if(Result == True)
                {
	                currentRemedy.remedyAwarded__c = True;
	    			update currentRemedy;
	    			
	    			errorOccured = true;
	    			errorMessage = 'Remedy has been awarded';
	    			return null;
                }
                else
                {
                	errorOccured = true;
	    			errorMessage = 'Error awarding Remedy, please contact your supervisor for assistance.';
	    			return null;
                }
            }
            catch(Exception ex)
            {
            	errorOccured = true;
    			errorMessage = 'Error Awarding Remedy:' + ex;
    			return null;
            }
        }
		else
		{
	    	errorOccured = true;
			errorMessage = 'Invalid Remedy Type';
	    	return null;
		}
    }
}