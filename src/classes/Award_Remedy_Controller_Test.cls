@isTest
private class Award_Remedy_Controller_Test {

    public static testMethod void setupTest()
	{
		//Create Contact:
		Contact testContact = new Contact();
		testContact.FirstName = 'testFirstName';
		testContact.LastName = 'testLastName';
		testContact.Email = 'testEmail@testEmail.com';
		testContact.Birthdate = date.newinstance(1960, 2, 17);
		testContact.UCDID__c = '123';
		insert testContact;
		
		//Current Account With Non-Awarded Remedy
		Multiplayer_Account__c testMPAccount = new Multiplayer_Account__c();
		testMPAccount.Contact__c = testContact.id;
		testMPAccount.Platform__c = 'XBL';
		testMPAccount.Gamertag__c = 'testGamertag';
		testMPAccount.DWID__c = '123';
		testMPAccount.Is_Current__c = True;
		insert testMPAccount;
		
		Remedy__c testRemedy = new Remedy__c();
		testRemedy.Game_Title__c = 'MW3';
		testRemedy.Award__c = 'Double XP';
		testRemedy.DXP_Interval__c = '2 Hours';
		testRemedy.Comments__c = 'TestComment';
		testRemedy.Multiplayer_Account__c = testMPAccount.id;
		testRemedy.remedyAwarded__c = False;
		insert testRemedy;
		
		//Run Code with all ducks in order:
		ApexPages.StandardController stdController = new ApexPages.StandardController(testRemedy);
		Award_Remedy_Controller remController = new Award_Remedy_Controller(stdController);
		PageReference pr1 = remController.Award();
		
		//Run Code where MPAccount is not current:
		Multiplayer_Account__c testMPAccount2 = new Multiplayer_Account__c();
		testMPAccount2.Contact__c = testContact.id;
		testMPAccount2.Platform__c = 'PSN';
		testMPAccount2.Gamertag__c = 'testGamertag2';
		testMPAccount2.DWID__c = '456';
		testMPAccount2.Is_Current__c = False;
		insert testMPAccount2;
		
		Remedy__c testRemedy2 = new Remedy__c();
		testRemedy2.Game_Title__c = 'MW3';
		testRemedy2.Award__c = 'Double XP';
		testRemedy2.DXP_Interval__c = '1 Hour';
		testRemedy2.Comments__c = 'TestComment';
		testRemedy2.Multiplayer_Account__c = testMPAccount2.id;
		testRemedy2.remedyAwarded__c = False;
		insert testRemedy2;
		
		ApexPages.StandardController stdController2 = new ApexPages.StandardController(testRemedy2);
		Award_Remedy_Controller remController2 = new Award_Remedy_Controller(stdController2);
		PageReference pr2 = remController2.Award();
		
		//Set Remedy to Awarded:
		Multiplayer_Account__c testMPAccount3 = new Multiplayer_Account__c();
		testMPAccount3.Contact__c = testContact.id;
		testMPAccount3.Platform__c = 'XBL';
		testMPAccount3.Gamertag__c = 'testGamertag3';
		testMPAccount3.DWID__c = '789';
		testMPAccount3.Is_Current__c = True;
		insert testMPAccount3;
		
		Remedy__c testRemedy3 = new Remedy__c();
		testRemedy3.Game_Title__c = 'MW3';
		testRemedy3.Award__c = 'Double XP';
		testRemedy3.DXP_Interval__c = '1 Hour';
		testRemedy3.Comments__c = 'TestComment';
		testRemedy3.Multiplayer_Account__c = testMPAccount3.id;
		testRemedy3.remedyAwarded__c = True;
		insert testRemedy3;
		
		ApexPages.StandardController stdController3 = new ApexPages.StandardController(testRemedy3);
		Award_Remedy_Controller remController3 = new Award_Remedy_Controller(stdController3);
		PageReference pr3 = remController3.Award();
		
		//Set Invalid DXP Interval:
		Multiplayer_Account__c testMPAccount4 = new Multiplayer_Account__c(); 
		testMPAccount4.Contact__c = testContact.id;
		testMPAccount4.Platform__c = 'XBL';
		testMPAccount4.Gamertag__c = 'testGamertag3';
		testMPAccount4.DWID__c = '000';
		testMPAccount4.Is_Current__c = True;
		insert testMPAccount4;
		
		Remedy__c testRemedy4 = new Remedy__c();
		testRemedy4.Game_Title__c = 'MW3';
		testRemedy4.Award__c = 'Double XP';
		testRemedy4.DXP_Interval__c = '';
		testRemedy4.Comments__c = 'TestComment';
		testRemedy4.Multiplayer_Account__c = testMPAccount4.id;
		testRemedy4.remedyAwarded__c = False;
		insert testRemedy4;
		
		ApexPages.StandardController stdController4 = new ApexPages.StandardController(testRemedy4);
		Award_Remedy_Controller remController4 = new Award_Remedy_Controller(stdController4);
		PageReference pr4 = remController4.Award();
		
		//Run With Same Gamertag to hit weekly limit:
		Multiplayer_Account__c testMPAccount5 = new Multiplayer_Account__c(); 
		testMPAccount5.Contact__c = testContact.id;
		testMPAccount5.Platform__c = 'XBL';
		testMPAccount5.Gamertag__c = 'testGamertag3';
		testMPAccount5.DWID__c = '135';
		testMPAccount5.Is_Current__c = True;
		insert testMPAccount5;
				
		Remedy__c testRemedy5a = new Remedy__c();
		testRemedy5a.Game_Title__c = 'MW3';
		testRemedy5a.Award__c = 'Double XP';
		testRemedy5a.DXP_Interval__c = '1 Hour';
		testRemedy5a.Comments__c = 'TestComment';
		testRemedy5a.Multiplayer_Account__c = testMPAccount5.id;
		testRemedy5a.remedyAwarded__c = True;
		insert testRemedy5a;
		
		Remedy__c testRemedy5b = new Remedy__c();
		testRemedy5b.Game_Title__c = 'MW3';
		testRemedy5b.Award__c = 'Double XP';
		testRemedy5b.DXP_Interval__c = '1 Hour';
		testRemedy5b.Comments__c = 'TestComment';
		testRemedy5b.Multiplayer_Account__c = testMPAccount5.id;
		testRemedy5b.remedyAwarded__c = False;
		insert testRemedy5b;
		
		//Run as Agent:
		Profile p = [SELECT Id FROM Profile WHERE Name LIKE '%Agent%' LIMIT 1];
		User testUser = new User(Alias = 'remUser', Email='remedyUser@testorg.com', EmailEncodingKey='UTF-8', LastName='Remedy', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='remedyTest@testorg.com');
        
        System.runAs(testUser){
			ApexPages.StandardController stdController5 = new ApexPages.StandardController(testRemedy5b);
			Award_Remedy_Controller remController5 = new Award_Remedy_Controller(stdController5);
			PageReference pr5 = remController5.Award();
        }
		
		//Catch Invalid Platform:
		Multiplayer_Account__c testMPAccount6 = new Multiplayer_Account__c(); 
		testMPAccount6.Contact__c = testContact.id;
		testMPAccount6.Platform__c = 'XBOOX';
		testMPAccount6.Gamertag__c = 'testGamertag3';
		testMPAccount6.DWID__c = 'abc';
		testMPAccount6.Is_Current__c = True;
		insert testMPAccount6;
		
		Remedy__c testRemedy6 = new Remedy__c();
		testRemedy6.Game_Title__c = 'MW3';
		testRemedy6.Award__c = 'Double XP';
		testRemedy6.DXP_Interval__c = '1 Hour';
		testRemedy6.Comments__c = 'TestComment';
		testRemedy6.Multiplayer_Account__c = testMPAccount6.id;
		testRemedy6.remedyAwarded__c = False;
		insert testRemedy6;
		
		ApexPages.StandardController stdController6 = new ApexPages.StandardController(testRemedy6);
		Award_Remedy_Controller remController6 = new Award_Remedy_Controller(stdController6);
		PageReference pr6 = remController6.Award();
		
		//Test DEV:
		Multiplayer_Account__c testMPAccount7 = new Multiplayer_Account__c(); 
		testMPAccount7.Contact__c = testContact.id;
		testMPAccount7.Platform__c = 'XBL';
		testMPAccount7.Gamertag__c = 'testGamertag3';
		testMPAccount7.DWID__c = 'xyz';
		testMPAccount7.Is_Current__c = True;
		insert testMPAccount7;
		
		Remedy__c testRemedy7 = new Remedy__c();
		testRemedy7.Game_Title__c = 'MW3';
		testRemedy7.Award__c = 'Double XP';
		testRemedy7.DXP_Interval__c = '1 Hour';
		testRemedy7.Comments__c = 'TestComment';
		testRemedy7.Multiplayer_Account__c = testMPAccount7.id;
		testRemedy7.remedyAwarded__c = False;
		insert testRemedy7;
		
		ApexPages.StandardController stdController7 = new ApexPages.StandardController(testRemedy7);
		Award_Remedy_Controller remController7 = new Award_Remedy_Controller(stdController7);
		remController7.ENVIRONMENT = 'DEV';
		PageReference pr7 = remController7.Award();
		
		//Test Dev False:
		Multiplayer_Account__c testMPAccount8 = new Multiplayer_Account__c(); 
		testMPAccount8.Contact__c = testContact.id;
		testMPAccount8.Platform__c = 'XBL';
		testMPAccount8.Gamertag__c = 'testGamertag3';
		testMPAccount8.DWID__c = 'jon';
		testMPAccount8.Is_Current__c = True;
		insert testMPAccount8;
		
		Remedy__c testRemedy8 = new Remedy__c();
		testRemedy8.Game_Title__c = 'MW3';
		testRemedy8.Award__c = 'Double XP';
		testRemedy8.DXP_Interval__c = '1 Hour';
		testRemedy8.Comments__c = 'TestComment';
		testRemedy8.Multiplayer_Account__c = testMPAccount8.id;
		testRemedy8.remedyAwarded__c = False;
		insert testRemedy8;
		
		ApexPages.StandardController stdController8 = new ApexPages.StandardController(testRemedy8);
		Award_Remedy_Controller remController8 = new Award_Remedy_Controller(stdController8);
		remController8.ENVIRONMENT = 'DEV';
		PageReference pr8 = remController8.Award();
		
		//Catch Invalid Award:
		Multiplayer_Account__c testMPAccount9 = new Multiplayer_Account__c(); 
		testMPAccount9.Contact__c = testContact.id;
		testMPAccount9.Platform__c = 'XBL';
		testMPAccount9.Gamertag__c = 'testGamertag3';
		testMPAccount9.DWID__c = 'alb';
		testMPAccount9.Is_Current__c = True;
		insert testMPAccount9;
		
		Remedy__c testRemedy9 = new Remedy__c();
		testRemedy9.Game_Title__c = 'MW3';
		testRemedy9.Award__c = 'PRESTIGE TOKEN';
		testRemedy9.DXP_Interval__c = '1 Hour';
		testRemedy9.Comments__c = 'TestComment';
		testRemedy9.Multiplayer_Account__c = testMPAccount9.id;
		testRemedy9.remedyAwarded__c = False;
		insert testRemedy9;
		
		ApexPages.StandardController stdController9 = new ApexPages.StandardController(testRemedy9);
		Award_Remedy_Controller remController9 = new Award_Remedy_Controller(stdController9);
		PageReference pr9 = remController9.Award();
	}
}