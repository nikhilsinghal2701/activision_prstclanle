global class BO2RankResetSchedulable implements Schedulable {

// This test runs a scheduled job at midnight Sept. 3rd. 2022
    
    //public static String CRON_EXP = '0 0 0 3 9 ? 2022';
    
    global void execute(SchedulableContext ctx) {
        //CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
        //FROM CronTrigger WHERE Id = :ctx.getTriggerId()];
        pullUnreadRecords();
        }
        @future (callout=true)
        static void pullUnreadRecords(){
        LASService.BasicHttpBinding_ILinkedAccounts service = new LASService.BasicHttpBinding_ILinkedAccounts();   
        CSServiceLib.ArrayOfStatRestoreRequest var = service.GetUpdatedStatRequests(2000); 
        map<Integer, String> guid2status = new map<Integer, String>();
        
        if(var.StatRestoreRequest != null){
	        system.debug('This is Var'+var);
	        for(CSServiceLib.StatRestoreRequest restoreReq : var.StatRestoreRequest){
	            system.debug('I am inside For Loop');
	            guid2status.put(restoreReq.m_guid, restoreReq.m_status);
	        }
	        
	        list<integer> keys = new list<integer>();
	        keys.addall(guid2status.keyset());
	        system.debug('Records Returned:' + keys.size());
	
	        Stat_Corruption_Report__c[] sfdcRecords2update = new Stat_Corruption_report__c[]{};
	        
	        system.debug(guid2status);
	        
	        For(Stat_Corruption_Report__c rep : [SELECT Id, Status__c, LAS_GUID__c FROM Stat_Corruption_Report__c WHERE LAS_GUID__c IN :keys]){//might have to convert set to list here
	                Rep.Status__c = guid2status.get(integer.Valueof(rep.LAS_GUID__c));
	                sfdcRecords2update.add(rep);
	        }
	        Update sfdcRecords2update;
        }
    }   
    
    /*@isTest 
    public static void unitTests() {
        Stat_Corruption_Report__c scr = new Stat_Corruption_Report__c(Status__c = 'OPEN',LAS_GUID__c = 12345, multiplayer_account__c = 'a0kZ00000013QJq');
        insert scr;
        
    String CRON_EXP = '0 0 0 16 11 ? 2022';
    Test.startTest();
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_16());
        tstService.GetUpdatedStatRequests(2000); 
        String jobId = System.schedule('TestBO2StatReset',  CRON_EXP, new BO2RankResetSchedulable());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];    
        System.assertEquals(CRON_EXP, ct.CronExpression);
    Test.stopTest();
        
    }*/
}