public with sharing class C2C_ChatSurvey{
    public C2C_ChatSurvey__c surv {get; set;}
    public C2C_ChatSurvey(){
        surv = new C2C_ChatSurvey__c();
    }
    
    public pageReference pSave(){
        try{
            upsert surv;
            return Page.C2C_ThankYou;
        }catch(Exception e){
            system.debug(e);
            return null;
        }
    }

}