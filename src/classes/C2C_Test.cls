@isTest
public class C2C_Test{
     
     static testMethod void testC2C_ChatSurvey(){
          C2C_ChatSurvey tstCls = new C2C_ChatSurvey();
          system.assertEquals(tstCls.surv, new C2C_ChatSurvey__c());
          tstCls.pSave();
          system.assert(tstCls.surv.Id != null);
     }

     static testMethod void testC2C_Chat2SurveyLink_ChatThenSurvey(){
          user gamer = [SELECT Id FROM User WHERE IsActive = TRUE AND ContactId != null LIMIT 1];
          user agent = [SELECT Id FROM User WHERE Profile.Name LIKE '%agent%' AND IsActive = TRUE LIMIT 1];
          C2C_ChatSurvey__c surv = new C2C_ChatSurvey__c();
          LiveChatTranscript chat = new LiveChatTranscript();
          system.runAs(agent){
               LiveChatVisitor lcv = new LiveChatVisitor();
               insert lcv;
               chat.LiveChatVisitorId = lcv.Id;
               chat.ChatInitiatedBy__c = gamer.Id;
               insert chat;
          }
          test.startTest(); //make the asynchronous actions synchronous
          system.runAs(gamer){
               surv.Experience_rating__c = 10;
               surv.Issue_resolved__c = 'test';
               surv.Next_step__c = 'test';
               insert surv;
          }
          test.stopTest();
          C2C_ChatSurvey__c tstSurv = [SELECT Id, Linked_to_transcript__c FROM C2C_ChatSurvey__c WHERE Id = :surv.Id];
          system.assert(tstSurv.Linked_to_Transcript__c);
          LiveChatTranscript tstChat = [SELECT Id, Chat_Survey__c FROM LiveChatTranscript WHERE Id = :chat.Id];
          system.assertEquals(tstChat.Chat_Survey__c, tstSurv.Id);

     }

static testMethod void testC2C_Chat2SurveyLink_SurveyThenChat(){
          user gamer = [SELECT Id FROM User WHERE IsActive = TRUE AND ContactId != null LIMIT 1];
          user agent = [SELECT Id FROM User WHERE Profile.Name LIKE '%agent%' AND IsActive = TRUE LIMIT 1];
          C2C_ChatSurvey__c surv = new C2C_ChatSurvey__c();
          LiveChatTranscript chat = new LiveChatTranscript();

          test.startTest(); //make the asynchronous actions synchronous
          system.runAs(gamer){
               surv.Experience_rating__c = 10;
               surv.Issue_resolved__c = 'test';
               surv.Next_step__c = 'test';
               insert surv;
          }
          test.stopTest();
          
          system.runAs(agent){
               LiveChatVisitor lcv = new LiveChatVisitor();
               insert lcv;
               chat.LiveChatVisitorId = lcv.Id;
               chat.ChatInitiatedBy__c = gamer.Id;
               insert chat;
          }
          C2C_ChatSurvey__c tstSurv = [SELECT Id, Linked_to_transcript__c FROM C2C_ChatSurvey__c WHERE Id = :surv.Id];
          system.assert(tstSurv.Linked_to_Transcript__c);
          LiveChatTranscript tstChat = [SELECT Id, Chat_Survey__c FROM LiveChatTranscript WHERE Id = :chat.Id];
          system.assertEquals(tstChat.Chat_Survey__c, tstSurv.Id);

     }

}