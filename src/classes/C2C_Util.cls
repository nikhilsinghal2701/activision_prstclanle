public class C2C_Util{
    @future
    public static void linkChatsToSurveys(map<Id, Id> creatorId2surveyId){
        Id[] createdByIds = new Id[]{};
        Id[] linkedToTranscript = new Id[]{};
        createdByIds.addAll(creatorId2surveyId.keySet());
        sObject[] objs2update = new sObject[]{};
        for(LiveChatTranscript chat : [SELECT Id, ChatInitiatedBy__c FROM LiveChatTranscript WHERE Chat_Survey__c = null AND ChatInitiatedBy__c IN :createdByIds AND CreatedDate = TODAY ORDER BY CreatedDate DESC]){
            if(creatorId2surveyId.containsKey(chat.ChatInitiatedBy__c)){
                chat.Chat_Survey__c = creatorId2surveyId.get(chat.ChatInitiatedBy__c);
                linkedToTranscript.add(chat.Chat_Survey__c);
                creatorId2surveyId.remove(chat.ChatInitiatedBy__c);
                objs2update.add(chat);
            }
        }
        if(! objs2update.isEmpty()){
            for(C2C_ChatSurvey__c surv : [SELECT Id, Linked_To_Transcript__c FROM C2C_ChatSurvey__c WHERE Id IN :linkedToTranscript]){
                surv.Linked_To_Transcript__c = true;
                objs2update.add(surv);
            }
            try{update objs2update;
            }catch(exception e){}
        }
    }
}