global class CRR_BatchJobMonitor implements Schedulable, Database.Batchable<sObject>, Database.AllowsCallouts{
    global static string setSchedule(){
        try{
            System.schedule('CRR_BatchJobMonitor-3', '0 3 * * * ?', new CRR_BatchJobMonitor());
            System.schedule('CRR_BatchJobMonitor-18', '0 18 * * * ?', new CRR_BatchJobMonitor());
            System.schedule('CRR_BatchJobMonitor-33', '0 33 * * * ?', new CRR_BatchJobMonitor());
            System.schedule('CRR_BatchJobMonitor-48', '0 48 * * * ?', new CRR_BatchJobMonitor());
        }catch(Exception e){
            return e.getMessage();
        }
        return 'success';
    }
//Batch methods
    global Database.QueryLocator start(Database.BatchableContext BC){
        return database.getQueryLocator('SELECT Id, BatchId__c, JobId__c FROM CRR_Batch_Log__c WHERE IsComplete__c = false'); 
    }
    global void execute(Database.BatchableContext bc, sObject[] scope){
        Attachment[] atts = new Attachment[]{};
        //login
        DwProxy_utils utils = new DwProxy_utils();
        //get a session id
        String sessId = utils.getSessionId('Demonware Promotion Code Id Sync');
        if(sessId == null) sessId = '';
        String endpoint = 'https://cs9';
        if(utils.isProd) endpoint = 'https://na12';
        endpoint += '.salesforce.com/services/async/28.0/job/';
        utils = null;
         
        //query and handle results   
        for(Integer i = 0; i < 10; i++){
            if(i >= scope.size()) break;
            sObject obj = scope[i];
            endpoint += obj.get('JobId__c') + '/batch/' + obj.get('BatchId__c');
            
            httpRequest req = new httpRequest();
            req.setMethod('GET');
            req.setHeader('X-SFDC-Session',sessId);
            req.setTimeout(120000);
            req.setEndpoint(endPoint);
            http h = new http();
            httpResponse resp = h.send(req);
            system.debug('resp == '+resp);
            system.debug('resp.getBody() == '+resp.getBody());
            String body = resp.getBody();
            //body == batchstate
            if(body.contains('<state>Completed') ||
                body.contains('<state>Failed') ||
                body.contains('<state>Not Processed'))
            {
                obj.put('IsComplete__c',true);
                endpoint += '/result';
                req.setEndpoint(endPoint);
                h = new http();
                resp = h.send(req);
                body = resp.getBody();
                resp = null; //reduce heap space
                h = null; //reduce heap space
                req = null; //reduce heap space
                String failures = '', strFirstLine = '';
                Integer successIndex = 0;
                Boolean firstLine = true, successIndexFound = false, foundFailures = false;
                system.debug('body == '+body);
                do{
                    String line = '';
                    if(body.indexOf('\n') > 0){
                        if(body.length() > body.indexOf('\n')){
                            line = body.subString(0, body.indexOf('\n')+1);//get the line
                            body = body.subString(body.indexOf('\n')+1, body.length());//remove the line from body (reduce heap)
                        }else{
                            line = body;
                            body = '';
                        }
                    }else{
                        line = body;
                        body = '';
                    }
                    if(firstLine){
                        firstLine = false;
                        failures = line; //add the header line to the failure 
                        for(String headerName : line.split('","')){
                            system.debug('headerName == '+headerName);
                            if(! successIndexFound && headerName.toLowerCase() != 'success'){
                                successIndex++;
                            }else{
                                successIndexFound = true;
                            }
                        }
                    }else if(successIndexFound){
                        String[] cells = line.split('","');
                        if(cells.size() > successIndex && cells[successIndex].toLowerCase() == 'false'){
                            foundFailures = true;
                            failures += line;
                        }
                    }
                }while(body != '');
                
                if(foundFailures){
                    for(CRR_Code_Type__c typ : [SELECT Id FROM CRR_Code_Type__c WHERE Bulk_Job_Id__c = :(String) obj.get('JobId__c')]){                    
                        atts.add(new Attachment(body = blob.valueOf(failures), ParentId = typ.Id, Name = 'Failures.csv'));
                    }
                }
            }
            
        }
        
        //record results in a location accessible to users
        database.insert(atts, false); //attachmentsToInsert, allOrNone
        update scope; //save the status of batches 
    }
    global void finish(Database.BatchableContext bc){}
    
    
    
//Schedulable methods
    global void execute(SchedulableContext sc){
        if([SELECT count() FROM CRR_Code_Type__c WHERE Bulk_Job_Id__c != null AND Loading_Complete__c = false LIMIT 1] > 0){    
            doMonitoring();
        }
        if([SELECT count() FROM CRR_Batch_Log__c WHERE IsComplete__c = false] > 0){
            database.executeBatch(new CRR_BatchJobMonitor(), 1);
        }
    }
    
    @future(callout=true)
    public static void doMonitoring(){
        //check if anything needs monitoring
        CRR_Code_Type__c[] unfinishedJobs = [SELECT Bulk_Job_Id__c FROM CRR_Code_Type__c WHERE Bulk_Job_Id__c != null AND Loading_Complete__c = false LIMIT 10];
        CRR_Code_Type__c[] nowFinished = new CRR_Code_Type__c[]{};
        if(unfinishedJobs.size() > 0){
            DwProxy_utils utils = new DwProxy_utils();
            //get a session id
            String sessId = utils.getSessionId('Demonware Promotion Code Id Sync');
            if(sessId == null) sessId = '';
            String endpoint = 'https://cs9';
            if(utils.isProd) endpoint = 'https://na12';
            endpoint += '.salesforce.com/services/async/28.0/job/';
            for(CRR_Code_Type__c unfinished : unfinishedJobs){
                httpRequest req = new httpRequest();
                req.setMethod('GET');
                req.setHeader('X-SFDC-Session',sessId);
                req.setTimeout(120000);
                req.setEndpoint(endPoint+unfinished.Bulk_Job_Id__c);
                http h = new http();
                httpResponse resp = h.send(req);
                system.debug('resp == '+resp);
                system.debug('resp.getBody() == '+resp.getBody());
                jobInfo job = new jobInfo(resp.getBody());
                if(job.isComplete()){
                    unfinished.Loading_Complete__c = true;
                    unfinished.Number_of_Codes__c = job.numberRecordsProcessed - job.numberRecordsFailed;
                    nowFinished.add(unfinished);
                } 
            }
        }
        update nowFinished;        
    }
    
    public class jobInfo{
        public boolean isComplete(){
            if(this.state == 'Closed' && this.numberBatchesTotal != null && this.numberBatchesCompleted != null){
                return this.numberBatchesTotal == this.numberBatchesCompleted;
            }
            return false;
        }
        public jobInfo(String xmlString){
            try{
                XmlStreamReader reader = new XmlStreamReader(xmlString);
                while(reader.hasNext()){
                    String val = '', locName = '';
                    while(reader.getEventType() == XmlTag.START_ELEMENT){
                        locName = reader.getLocalName();
                        reader.next();
                    }
                    if(reader.getEventType() == XmlTag.CHARACTERS){
                        val = reader.getText();
                        if(locName == 'id') this.id = val;
                        else if(locName == 'operation') this.operation = val;
                        else if(locName == 'object') this.pObject = val;
                        else if(locName == 'createdById') this.createdById = val;
                        //else if(locName == 'createdDate') this.createdDate = ??;
                        //else if(locName == 'systemModStamp') this.systemModStamp = ??;
                        else if(locName == 'state') this.state = val;
                        else if(locName == 'concurrencyMode') this.concurrencyMode = val;
                        else if(locName == 'contentType') this.contentType = val;
                        else if(locName == 'numberBatchesQueued') this.numberBatchesQueued = Integer.valueOf(val);
                        else if(locName == 'numberBatchesInProgress') this.numberBatchesInProgress = Integer.valueOf(val);
                        else if(locName == 'numberBatchesCompleted') this.numberBatchesCompleted = Integer.valueOf(val);
                        else if(locName == 'numberBatchesFailed') this.numberBatchesFailed = Integer.valueOf(val);
                        else if(locName == 'numberBatchesTotal') this.numberBatchesTotal = Integer.valueOf(val);
                        else if(locName == 'numberRecordsProcessed') this.numberRecordsProcessed = Integer.valueOf(val);
                        else if(locName == 'numberRetries') this.numberRetries = Integer.valueOf(val);
                        else if(locName == 'apiVersion') this.apiVersion = Double.valueOf(val);
                        else if(locName == 'numberRecordsFailed') this.numberRecordsFailed = Integer.valueOf(val);
                        else if(locName == 'totalProcessingTime') this.totalProcessingTime = Integer.valueOf(val);
                        else if(locName == 'apiActiveProcessingTime') this.apiActiveProcessingTime = Integer.valueOf(val);
                        else if(locName == 'apexProcessingTime') this.apexProcessingTime = Integer.valueOf(val);
                    }
                    reader.next();
                }
            }catch(System.XMLException xmlE){
                system.debug('xmlE == '+xmlE);
            }catch(Exception e){
                system.debug('e == '+e);
            }
        }
        public Id id;
        public String operation;
        public String pObject;
        public Id createdById;
        public DateTime createdDate;
        public DateTime systemModstamp;
        public String state;
        public String concurrencyMode;
        public String contentType;
        public Integer numberBatchesQueued;
        public Integer numberBatchesInProgress;
        public Integer numberBatchesCompleted;
        public Integer numberBatchesFailed;
        public Integer numberBatchesTotal;
        public Integer numberRecordsProcessed;
        public Integer numberRetries;
        public Double apiVersion;
        public Integer numberRecordsFailed;
        public Integer totalProcessingTime;
        public Integer apiActiveProcessingTime;
        public Integer apexProcessingTime;
    
    /*
    <?xml version="1.0" encoding="UTF-8"?>
    <jobInfo xmlns="http://www.force.com/2009/06/asyncapi/dataload">
 <id>750K00000002fStIAI</id>
 <operation>insert</operation>
 <object>CRR_Code__c</object>
 <createdById>005K0000001oYITIA2</createdById>
 <createdDate>2014-01-17T18:46:32.000Z</createdDate>
 <systemModstamp>2014-01-17T18:46:49.000Z</systemModstamp>
 <state>Closed</state>
 <concurrencyMode>Parallel</concurrencyMode>
 <contentType>CSV</contentType>
 <numberBatchesQueued>0</numberBatchesQueued>
 <numberBatchesInProgress>0</numberBatchesInProgress>
 <numberBatchesCompleted>20</numberBatchesCompleted>
 <numberBatchesFailed>0</numberBatchesFailed>
 <numberBatchesTotal>20</numberBatchesTotal>
 <numberRecordsProcessed>199999</numberRecordsProcessed>
 <numberRetries>0</numberRetries>
 <apiVersion>28.0</apiVersion>
 <numberRecordsFailed>89999</numberRecordsFailed>
 <totalProcessingTime>1071264</totalProcessingTime>
 <apiActiveProcessingTime>886766</apiActiveProcessingTime>
 <apexProcessingTime>0</apexProcessingTime>
</jobInfo>
*/
    }
    

}