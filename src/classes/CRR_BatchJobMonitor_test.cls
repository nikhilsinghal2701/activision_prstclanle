@isTest
public class CRR_BatchJobMonitor_test{
    public static testMethod void test_theEasyStuff(){
        Id anyId;
        //delete the jobs if they exist, test context data doesn't exclude scheduled jobs
        for(CronTrigger trig : [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name LIKE 'CRR_BatchJobMonitor-%']){
            System.abortJob(trig.Id);
            anyId = trig.Id;
        }
        String result = CRR_BatchJobMonitor.setSchedule(); //success
        system.assertEquals('success',result);
        
        result = CRR_BatchJobMonitor.setSchedule(); //failure because jobs with those names already exist
        system.assertEquals('The Apex job named "CRR_BatchJobMonitor-3" is already scheduled for execution.', result);   

        String response = '<?xml version="1.0" encoding="UTF-8"?>';
        response += '<jobInfo xmlns="http://www.force.com/2009/06/asyncapi/dataload">';
        response += '<id>750K00000002fStIAI</id>';
        response += '<operation>insert</operation>';
        response += '<object>CRR_Code__c</object>';
        response += '<createdById>005K0000001oYITIA2</createdById>';
        response += '<createdDate>2014-01-17T18:46:32.000Z</createdDate>';
        response += '<systemModstamp>2014-01-17T18:46:49.000Z</systemModstamp>';
        response += '<state>Closed</state>';
        response += '<concurrencyMode>Parallel</concurrencyMode>';
        response += '<contentType>CSV</contentType>';
        response += '<numberBatchesQueued>0</numberBatchesQueued>';
        response += '<numberBatchesInProgress>0</numberBatchesInProgress>';
        response += '<numberBatchesCompleted>20</numberBatchesCompleted>';
        response += '<numberBatchesFailed>0</numberBatchesFailed>';
        response += '<numberBatchesTotal>20</numberBatchesTotal>';
        response += '<numberRecordsProcessed>199999</numberRecordsProcessed>';
        response += '<numberRetries>0</numberRetries>';
        response += '<apiVersion>28.0</apiVersion>';
        response += '<numberRecordsFailed>89999</numberRecordsFailed>';
        response += '<totalProcessingTime>1071264</totalProcessingTime>';
        response += '<apiActiveProcessingTime>886766</apiActiveProcessingTime>';
        response += '<apexProcessingTime>0</apexProcessingTime>';
        response += '</jobInfo>';
        
        CRR_BatchJobMonitor.jobInfo tstCls = new CRR_BatchJobMonitor.jobInfo(response);
        system.assert(tstCls.isComplete());
    }
    public static testMethod void test_batch(){
        try{
            CRR_BatchJobMonitor tstCls = new CRR_BatchJobMonitor();
            insert new CRR_Batch_Log__c(BatchId__c = 'asdf', JobId__c = 'asdf', IsComplete__c = false);
            test.setMock(HttpCalloutMock.class, new CRR_HttpCalloutMock_Test());
            test.StartTest();
                database.executeBatch(new CRR_BatchJobMonitor(), 1);
            test.stopTest();
        }catch(Exception e){
            system.debug('ERROR == '+e);
        }
    }
    public static testMethod void test_future(){
        try{
            test.setMock(HttpCalloutMock.class, new CRR_HttpCalloutMock_Test());
            insert new CRR_Code_Type__c(Bulk_Job_Id__c = '123', Loading_Complete__c = false);
            CRR_BatchJobMonitor.doMonitoring();
        }catch(Exception e){
            system.debug('ERROR == '+e);
        }
    }
    
}