public class CRR_BulkDeleteExtension{
    sObject rec;
    public CRR_BulkDeleteExtension(ApexPages.standardController cont){
        try{cont.addFields(new String[]{'deleting__c'});
        }catch(Exception e){}
        this.rec = cont.getRecord();
    }
    public pageReference bulkDeleteCRR(){
        if(!CRR_Status.isRunning){
            if([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5){
                database.executeBatch(new CRR_CodeDeletion((Id) this.rec.get('id'), 'CRR_Code_Type__c'));
                this.rec.put('deleting__c',true);
                update this.rec;
                return new pageReference('/'+(Id) this.rec.get('id'));
            }else{
                addError('No available batch processing capacity available.  Please try again later.');
            }
        }else{
            addError('Code Redemption Repository activity in process; please wait until it completes and try again.  Check the Status page for details.');
        }        
        return null;
    }

    public void addError(String msg){
        try{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, msg));
        }catch(Exception e){
            system.debug(e);
        }
    }
}