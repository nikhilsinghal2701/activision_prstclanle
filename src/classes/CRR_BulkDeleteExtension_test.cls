@isTest
public class CRR_BulkDeleteExtension_test{

    public static testMethod void test_theEasyStuff(){
        //this one trips the try/catch
        CRR_BulkDeleteExtension tstCls = new CRR_BulkDeleteExtension(new ApexPages.standardController(new Account()));
        
        //this one has the field
        CRR_Code_Type__c typ = new CRR_Code_Type__c();
        insert typ;
        tstCls = new CRR_BulkDeleteExtension(new ApexPages.standardController(typ));
        
        tstCls.bulkDeleteCRR();
        
        tstCls.addError('fail'); //trips the try/catch because there's no VF page context
        test.setCurrentPage(Page.CRR_SystemStatus);
        tstCls.addError('fail'); 
    }
}