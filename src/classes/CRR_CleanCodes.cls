global without sharing class CRR_CleanCodes implements Database.Batchable<sObject>{

    global Database.queryLocator start(Database.BatchableContext ctxt){
        return database.getQueryLocator('SELECT Id FROM CRR_Code__c WHERE Code_Distribution_Request_Id__c = null AND isDistributed__c = true'); 
    }
    public static void execute(Database.BatchableContext ctxt, sObject[] scope){
        for(sObject code : scope){
            code.put('isDistributed__c',false);
        }
        
        database.update(scope, false);
    }
    global void finish(Database.batchableContext ctxt){
    }
}