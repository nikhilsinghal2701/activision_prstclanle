global with sharing class CRR_CodeDeletion implements Database.Batchable<sObject>, Database.Stateful{
    global final Id typeId;
    global final String parentTableApiName;
    global final String gQuery; //global query
    global CRR_CodeDeletion(String pQuery){
        gQuery = pQuery;
    }
    global CRR_CodeDeletion(Id parentIdOfCodesToDelete, String pParentApiName){
        typeId = parentIdOfCodesToDelete;
        parentTableApiName = pParentApiName;
    }
    global Database.QueryLocator start(Database.BatchableContext ctxt){
        string query = 'SELECT Id FROM CRR_Code__c WHERE Code_Type__c = \''+typeId+'\'';
        if(gQuery != null) query = gQuery;
        return Database.getQueryLocator(query);
    }
    global void execute(Database.batchableContext BC, sObject[] scope){
        database.delete(scope, false);
    }
    global void finish(Database.BatchableContext BC){
        delete [SELECT Id FROM CRR_Code_Type__c WHERE Id = :typeId];
    }
}