@isTest
public class CRR_CodeDeletion_test{
    public static testMethod void test1(){
        CRR_Code_Type__c typ = new CRR_Code_Type__c();
        insert typ;
        insert new CRR_Code__c(Code__c = '123',First_Party__c = 'NOA', Code_Type__c = typ.Id);
        CRR_CodeDeletion tstCls = new CRR_CodeDeletion('query');
        tstCls = new CRR_CodeDeletion(typ.Id,'test');
        database.executeBatch(tstCls);
        
    }
}