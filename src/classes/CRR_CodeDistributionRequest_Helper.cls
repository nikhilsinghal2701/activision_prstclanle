public with sharing class CRR_CodeDistributionRequest_Helper {
    
    private final CRR_Code_Distribution_Request__c codedistrequest ; 
                                        
    public string gameselection{get;set;}
    public string platformselection{get;set;}
    public string codetypeselection{get;set;}
    public string promocodeselection{get;set;}
    public string contenttypeselection{get;set;}
    public string contentnameselection{get;set;}
    public decimal availcodes{get;set;}
    public pageReference doNothing(){return null;}
    
    list<CRR_Code_Type__c> promocodes = [Select Id, Name, (Select Game_Title__c From Content__r Order By CreatedDate DESC LIMIT 1), Code_Product__r.Id,Code_Product__r.Name, Platform__c From CRR_Code_Type__c Where Loading_Complete__c = True];
    
    
    public CRR_CodeDistributionRequest_Helper(ApexPages.StandardController stdController){
        this.codedistrequest = (CRR_Code_Distribution_Request__c)stdController.getRecord();
    }
    
    
    
    public List<SelectOption> getGames() {
        List<SelectOption> games = new List<SelectOption>();
        games.add(new SelectOption('None', 'None'));
        Set<String> noDupes = new Set<String>();
        for (CRR_Code_Type__c gameslist : promocodes){
            system.debug('Content__r is '+gameslist.Content__r);
            if(promocodes != NULL){
               if(gameslist.Content__r.isEmpty() == False){
                ContentVersion cv = gameslist.Content__r; 
                if(!(noDupes.contains(cv.Game_Title__c)) && cv.Game_Title__c != null) {
                    games.add(new SelectOption(cv.Game_Title__c,cv.Game_Title__c));
                    noDupes.add(cv.Game_Title__c);
                    }
                }
            
            }
        }
        
        return games;
    }
    
    public List<SelectOption> getPlatforms(){
        string query = 'Select Id, Name, Code_Product__r.Id,Code_Product__r.Name, (Select Game_Title__c, Platform__c From Content__r ';
            
        if(gameselection == 'None' || gameselection == null){
            query+='Where Game_Title__c =null Order by CreatedDate DESC LIMIT 1) From CRR_Code_Type__c Where Loading_Complete__c = True';
        } else query+='Where Game_Title__c = '+'\''+String.escapeSingleQuotes(gameselection)+'\' Order by CreatedDate DESC LIMIT 1)'+' From CRR_Code_Type__c Where Loading_Complete__c = True';
        system.debug('Platforms query is '+query);
    	
        CRR_Code_Type__c[] promoplat = Database.query(query);
        system.debug(gameselection);
        List<SelectOption> platforms = new List<SelectOption>();
        platforms.add(new SelectOption('None', 'None'));
        Set<String> noDupes = new Set<String>();
        for (CRR_Code_Type__c platformlist : promoplat){
            if(promoplat != NULL){
               if(platformlist.Content__r.isEmpty() == False){
                ContentVersion cv = platformlist.Content__r;
                if(!(noDupes.contains(cv.Platform__c)) && cv.Platform__c != null) {
                    platforms.add(new SelectOption(cv.Platform__c,cv.Platform__c));
                    noDupes.add(cv.Platform__c);
                    }
                }
            }
            
        }
        
        return platforms;
    }

    public List<SelectOption> getCodeType() {
        string query = 'Select Id, Name, Code_Product__r.Id,Code_Product__r.Name, (Select Game_Title__c, Platform__c, Code_Type__c From Content__r ';
            
        if(gameselection == 'None' || gameselection == null){
            query+='Where Game_Title__c=null';
        } else query+='Where Game_Title__c = '+'\''+String.escapeSingleQuotes(gameselection)+'\'';

        if(platformselection == 'None' || platformselection == null){
            query+=' And Platform__c=null Order by CreatedDate DESC LIMIT 1) From CRR_Code_Type__c Where Loading_Complete__c = True';
        } else query+=' And Platform__c= '+'\''+platformselection+'\' Order by CreatedDate DESC LIMIT 1) '+' From CRR_Code_Type__c Where Loading_Complete__c = True';
        system.debug('CodeType Query is '+query);
        CRR_Code_Type__c[] promotype = Database.query(query);
        system.debug(gameselection+' '+platformselection);
        List<SelectOption> codeTypes = new List<SelectOption>();
        codeTypes.add(new SelectOption('None', 'None'));
        Set<String> noDupes = new Set<String>();
        for (CRR_Code_Type__c codes : promotype){
            if(promotype != NULL){
               if(codes.Content__r.isEmpty() == False){
                ContentVersion cv = codes.Content__r; 
                if(!(noDupes.contains(cv.Code_type__c)) && cv.Code_type__c != null){
                    codeTypes.add(new SelectOption(cv.Code_type__c, cv.Code_type__c));
                    noDupes.add(cv.Code_type__c);
                    }
                }
            }   
        }
        return codeTypes;
    }
    
    public List<SelectOption> getContentType() {
        string query = 'Select Id, Name, Code_Product__r.Id,Code_Product__r.Name, (Select Game_Title__c, Platform__c, Code_type__c, Content_Type__c From Content__r ';
            
        if(gameselection == 'None' || gameselection == null){
           query+='Where Game_Title__c=null';
        } else query+='Where Game_Title__c = '+'\''+String.escapeSingleQuotes(gameselection)+'\'';

        if(platformselection == 'None' || platformselection == null){
            query+=' And Platform__c=null';
        } else query+=' And Platform__c= '+'\''+platformselection+'\'';

        if(codetypeselection == 'None' || codetypeselection == null){
            query+=' And Code_type__c=null Order by CreatedDate DESC LIMIT 1) From CRR_Code_Type__c Where Loading_Complete__c = True';
        } else query+=' And Code_Type__c='+'\''+codetypeselection+'\' Order by CreatedDate DESC LIMIT 1)'+' From CRR_Code_Type__c Where Loading_Complete__c = True';
        system.debug('Content Type Query is '+query);
        CRR_Code_Type__c[] contenttype = Database.query(query); 
    	
        List<SelectOption> contenttypes = new List<SelectOption>();
        contenttypes.add(new SelectOption('None', 'None'));
        Set<String> noDupes = new Set<String>();
        for (CRR_Code_Type__c types : contenttype){
            if(contenttype != NULL){
               if(types.Content__r.isEmpty() == False){
                ContentVersion cv = types.Content__r; 
                if(!(noDupes.contains(cv.Content_Type__c)) && cv.Content_Type__c != null){
                    contenttypes.add(new SelectOption(cv.Content_Type__c, cv.Content_Type__c));
                    noDupes.add(cv.Content_Type__c);
                    }
                }
            }   
        }
        return contenttypes;
    }
    
    public List<SelectOption> getContentName() {
        string query = 'Select Id, Name, Code_Product__r.Id,Code_Product__r.Name, (Select Game_Title__c, Platform__c, Code_type__c, Content_Type__c, Content_Name__c From Content__r ';
            
        if(gameselection == 'None' || gameselection == null){
            query+='Where Game_Title__c=null';
        } else query+='Where Game_Title__c = '+'\''+String.escapeSingleQuotes(gameselection)+'\'';

        if(platformselection == 'None' || platformselection == null){
            query+=' And Platform__c=null';
        } else query+=' And Platform__c= '+'\''+platformselection+'\'';

        if(codetypeselection == 'None' || codetypeselection == null){
            query+=' And Code_type__c=null';
        } else query+=' And Code_Type__c='+'\''+codetypeselection+'\'';
        
        if (contenttypeselection == 'None' || contenttypeselection == null){
            query+=' And Content_Type__c=null Order by CreatedDate DESC LIMIT 1) From CRR_Code_Type__c Where Loading_Complete__c = True';
        } else query+=' And Content_Type__c = '+'\''+contenttypeselection+'\' Order by CreatedDate DESC LIMIT 1)'+' From CRR_Code_Type__c Where Loading_Complete__c = True';
        system.debug('Content Name Query is '+query);
        CRR_Code_Type__c[] contentname = Database.query(query);
        List<SelectOption> contentnames = new List<SelectOption>();
        contentnames.add(new SelectOption('None', 'None'));
        Set<String> noDupes = new Set<String>();
        for (CRR_Code_Type__c names : contentname){
            if(contentname != NULL){
               if(names.Content__r.isEmpty() == False){
                ContentVersion cv = names.Content__r; 
                if(!(noDupes.contains(cv.Content_Name__c)) && cv.Content_Name__c != null){
                    contentnames.add(new SelectOption(cv.Content_Name__c, cv.Content_Name__c));
                    noDupes.add(cv.Content_Name__c);
                    }
                }
            }   
        }
        return contentnames;
    }
    
    public List<SelectOption> getPromoCodes(){
        string query = 'Select Id, Name, Promotion_name__c, Filename__c, (Select Title, Platform__c, Game_Title__c, Code_Type__c, Content_Type__c, Content_Name__c, First_party__c From Content__r ';
            
        if (gameselection == 'None' || gameselection == null){
            query+='Where Game_Title__c=null';
        } else query+='Where Game_Title__c = '+'\''+String.escapeSingleQuotes(gameselection)+'\'';

        if (platformselection == 'None' || platformselection == null){
            query+=' And Platform__c=null';
        } else query+=' And Platform__c= '+'\''+platformselection+'\'';

        if (codetypeselection == 'None' || codetypeselection == null){
            query+=' And Code_type__c=null';
        } else query+=' And Code_Type__c='+'\''+codetypeselection+'\'';

        if (contenttypeselection == 'None' || contenttypeselection == null){
            query+=' And Content_Type__c=null';
        } else query+=' And Content_Type__c = '+'\''+contenttypeselection+'\'';

        if (contentnameselection == 'None' || contentnameselection == null){
            query+=' And Content_Name__c=null Order by CreatedDate DESC LIMIT 1) From CRR_Code_Type__c';
        } else query+=' And Content_Name__c= '+'\''+contentnameselection+'\' Order by CreatedDate DESC LIMIT 1)'+'From CRR_Code_Type__c';

        system.debug('Promocode query is '+query);
        CRR_Code_Type__c[] availablecodes = Database.query(query); 
        system.debug(gameselection+' '+platformselection+' '+codetypeselection);
        system.debug(availablecodes);
        List<SelectOption> codelist = new List<SelectOption>();
        codelist.add(new SelectOption('None', 'None'));
        for (CRR_Code_Type__c tempcode : availablecodes){
            if(availablecodes != NULL){
               if(tempcode.Content__r.isEmpty() == False){
                ContentVersion cv = tempcode.Content__r;
                codelist.add(new SelectOption(tempcode.Id, cv.Title+' '+cv.First_party__c));
              }
            }
           } 
           
        system.debug(codelist);
        return codelist;
    }

    public Decimal getAvailableCodes(){
        If(promocodeselection != Null && promocodeselection != 'None'){
        CRR_Code_Type__c code = [Select Id, Number_of_Codes__c From CRR_Code_Type__c Where Id=:promocodeselection];
        availcodes = code.Number_of_codes__c;
        return availcodes;
       }

       return null; 
    }
    
    public PageReference Save(){
        if(promocodeselection == Null || promocodeselection == 'None'){
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'You must select a promotion code before saving');
            ApexPages.addMessage(errormsg);
            return null;
        } else if(this.codedistrequest.Number_of_codes_to_deliver__c > availcodes){
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'You cannot request more codes than are available');
            ApexPages.addMessage(errormsg);
            return null;
        } else {
    	this.codedistrequest.Code_Type__c = promocodeselection;
    	insert this.codedistrequest;
    	return null;
      }
    } 
    
    

}