public without sharing class CRR_CodeDownload{//the where clause on row "12" achieves the necessary security

    public codeDistributionWrapper[] myDistributions {get; set;}
    
    public CRR_CodeDownload(){
        if(! ApexPages.currentPage().getParameters().containsKey('id')){
            set<Id> gIds = new set<Id>();
            for(CRR_Code_Recipient_Group_Membership__c crrGroup : [SELECT Code_Distribution_Recipient_Group__c FROM CRR_Code_Recipient_Group_Membership__c 
                                                                WHERE Code_Distribution_Recipient__c = :UserInfo.getUserId()])
            {
                gIds.add(crrGroup.Code_Distribution_Recipient_Group__c);
            }
            CRR_Code_Distribution_Request__c[] dists = [SELECT Id, Name, Number_of_Codes_to_Deliver__c, Code_Type__r.Name, Code_Type__r.Description__c,
                                                        (SELECT Id FROM Code_Distribution_Downloads__r) FROM CRR_Code_Distribution_Request__c 
                                                        WHERE Code_Distribution_Recipient_Group__c IN :gIds OR 
                                                        Code_Distribution_Recipient__r.Recipient__c = :UserInfo.getUserId()
                                                        ORDER BY CreatedDate ASC
                                                        ];
            this.myDistributions = new codeDistributionWrapper[]{};
            /*
            for(CRR_Code_Distribution_Request__c distribution : [SELECT Id, Name, Number_of_Codes_to_Deliver__c, Code_Type__r.Name, Code_Type__r.Description__c,
                                                                        (SELECT Id FROM Code_Distribution_Downloads__r)
                                                                 FROM CRR_Code_Distribution_Request__c 
                                                                 WHERE Code_Distribution_Recipient__r.Recipient__c = :UserInfo.getUserId()
                                                                 ORDER BY CreatedDate ASC])
                                                                 */
            for(CRR_Code_Distribution_Request__c distribution : dists)
            {
                myDistributions.add(new codeDistributionWrapper(distribution));
            }
        }
    }

    public class codeDistributionWrapper{
        public codeDistributionWrapper(CRR_Code_Distribution_Request__c toWrap){
            this.dist = toWrap;
            this.downloaded = ! toWrap.Code_Distribution_Downloads__r.IsEmpty();
        }
        public CRR_Code_Distribution_Request__c dist {get; set;}
        public boolean downloaded {get; set;}
    }
    public Id downloadId {get; set;}
    public pagereference logDownload(){
        if(this.downloadId != null){
            CRR_CodeDownload.doLog(this.downloadId);
            for(codeDistributionWrapper wrap : myDistributions ){
                if(wrap.dist.Id == downloadId){
                    wrap.downloaded = true;
                    break;
                }
            }
        }
        return null;
    }
    @future
    public static void doLog(Id downloadedDeliveryId){
        insert new CRR_Code_Distribution_Download__c(Code_Distribution_Request__c = downloadedDeliveryId);
    }

}