@isTest 
public class CRR_CodeDownload_test{
    public static testMethod void test1(){
        CRR_Code_Distribution_Recipient__c recip = new CRR_Code_Distribution_Recipient__c();
        recip.Recipient__c = [SELECT id FROM User WHERE ContactId = null AND 
                              IsActive = TRUE AND Email LIKE '%@activision.com' AND
                              Profile.Name = 'System Administrator'
                              LIMIT 1].Id;
        insert recip;
        
        CRR_Code_Type__c typ = new CRR_Code_Type__c();
        typ.Number_of_codes__c = 1;
        typ.Loading_complete__c = true;
        insert typ;
        
        CRR_Code__c cod = new CRR_Code__c(Code__c = '123', First_Party__c = 'NOA', Code_Type__c = typ.Id);
        insert cod;
        
        CRR_Code_Distribution_Request__c req = new CRR_Code_Distribution_Request__c();
        req.Code_Distribution_Recipient__c = recip.Id;
        req.Code_Type__c = typ.Id;
        req.Number_of_Codes_to_Deliver__c = 1;
        insert req;
        
        CRR_CodeDownload tstCls = new CRR_CodeDownload();
        CRR_CodeDownload.codeDistributionWrapper tstWrap = new CRR_CodeDownload.codeDistributionWrapper(req);
        tstCls.downloadId = req.Id;
        tstCls.logDownload();
    }
}