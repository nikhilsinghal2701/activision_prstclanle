public with sharing class CRR_CodeReturn{
    //CONSTRUCTORS
    public CRR_CodeReturn(){ //called via code
    }
    public CRR_CodeReturn(ApexPages.standardController cont){ //called via VF
        this.pCodeTypeId = cont.getId();
        if(cont.getId() != null){
            this.specifiedCodeType = [SELECT Name FROM CRR_Code_Type__c WHERE Id = :cont.getId()].Name;
        }
    }
    
    //data stores
    //max size of file uploadable
    static final integer FILE_LIMIT = 3899854; //max processable size thru batch return class, 130,000 29 character alpha-numeric codes
    
    //The return record that will be created
    CRR_Code_Return__c pReturn;
    public CRR_Code_Return__c theReturn {
        get{
            if(this.pReturn == null) this.pReturn = new CRR_Code_Return__c();
            return this.pReturn;
        }
    }
    
    //the file that contains the codes to return
    transient Blob pFile;
    public Blob theFile {
        get{return this.pFile;}
        set{this.pFile = value;}
    }
    
    //the id of the promotion code (code type) being returned
    String pCodeTypeId;
    public String theCodeTypeId {
        get{return this.pCodeTypeId;}
        set{this.pCodeTypeId = value;}
    }
    
    //name of the file
    String pFileName;
    public String theFileName {
        get{return this.pFileName;}
        set{this.pFileName = value;}
    }

    //code type being returned is specified
    public String specifiedCodeType {get; set;}

    //list of available code types for return
    selectOption[] fpOptions;
    public selectOption[] getCodeTypes(){
        if(fpOptions == null){ //fpOptions is null, we haven't built it yet
            map<Id, String> fpId2fpName = new map<Id, String>();
            for(CRR_Code_Distribution_Request__c typ : [SELECT Code_Type__c, Code_Type__r.Name FROM CRR_Code_Distribution_Request__c WHERE Recipient_Lookup__c = :UserInfo.getUserId() ORDER BY Name]){
                fpId2fpName.put(typ.Code_Type__c, typ.Code_Type__r.Name);
            }
            
            fpOptions = new selectOption[]{new selectOption('','--None--')};
            for(Id fpId : fpId2fpName.keySet()){
                fpOptions.add(new selectOption(fpId, fpId2fpName.get(fpId)));
            }
        }
        return fpOptions;
    }
    
    public pageReference submitFile(){
        SavePoint sp = database.setSavePoint();
        try{
            //check that they have specified the set of codes being returned
            if(this.pCodeTypeId == null || this.pCodeTypeId == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Must specify the set of codes being returned'));
                return null;
            }
            //check that a file has been provided 
            if(this.pFile == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Must provide the list of codes to return'));
                return null;
            }
            
            //check that the file is less than 3.5MB
            if(this.pFile.size() > FILE_LIMIT){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'File too large, the limit is 3.5MB'));
                return null;
            }
            
            //insert the Code_Return__c record for its id
            this.theReturn.Code_Type__c = this.pCodeTypeId;
            
            insert this.theReturn; 
            //attach the file to the return
            Attachment att = new Attachment(
                Name = this.pFileName,
                Body = this.pFile,
                ParentId = this.theReturn.Id
            );
            insert att;
            att = null;
            
            String delimiter = '', strFileBody = this.pFile.toString();
            this.pFile = null;
            if(strFileBody.contains('\r\n')){
                delimiter = 'n';
            }else if(strFileBody.contains('\n\r')){
                delimiter = 'r';
            }else if(strFileBody.contains('\r')){
                delimiter = 'r';
            }else if(strFileBody.contains('\n')){
                delimiter = 'n';
            }
            this.theReturn.Delimiter__c = delimiter;
            update this.theReturn;

            if(! CRR_Status.isRunning){
                database.executeBatch(
                    new CRR_CodeReturnBatch(
                        strFileBody, 
                        [SELECT Id, Code_Type__c, Delimiter__c, First_Party__c FROM CRR_Code_Return__c WHERE Id = :this.theReturn.Id]
                    ), 5000); //1/2 the DML limit (upsert, then upsert again for each error)
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, 'Processing initiated'));
            }else{
                this.theReturn.Queued__c = true;
                update this.theReturn;
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Processing queued'));
            }
            pReturn = new CRR_Code_Return__c();
            return null;
        }catch(Exception e){
            system.debug(e);
            Apexpages.addMessages(e);
            database.rollback(sp);
        }
        return null;
    }
    
    

}