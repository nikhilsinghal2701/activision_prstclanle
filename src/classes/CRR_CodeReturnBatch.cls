global with sharing class CRR_CodeReturnBatch implements Database.batchable<String>{
    global final String codes;
    global final String foundDelimiter;
    global final String alterDelimiter;
    global final CRR_Code_Return__c theReturn;
    global final String theFirstparty;
    global CRR_CodeReturnBatch(String pCodes, CRR_Code_Return__c pTheReturn){
        this.codes = pCodes;
        this.foundDelimiter = pTheReturn.Delimiter__c == 'n' ? '\n' : '\r';
        this.alterDelimiter = pTheReturn.Delimiter__c == 'n' ? '\r' : '\n';
        this.theReturn = pTheReturn;
        this.theFirstParty = pTheReturn.First_Party__c;
        this.theReturn.Queued__c = false;
        update this.theReturn;
    }
    global iterable<String> start(Database.BatchableContext ctxt){
        return new CRR_StringIteratorIMPL(this.codes, this.foundDelimiter);
    }
    
    global void execute(Database.BatchableContext ctxt, String[] lstCodes){
        String[] properlySplitCodes = new String[]{};
        for(String strCode : lstCodes){
            if(strCode.contains(this.alterDelimiter)){
                //alternate delimiter found, replace all with the expected one
                strCode = strCode.replaceAll(this.alterDelimiter, this.foundDelimiter);
                do{ //replace all duplicates
                    strCode = strCode.replaceAll(this.foundDelimiter+this.foundDelimiter,this.foundDelimiter);
                }while(strCode.contains(this.foundDelimiter+this.foundDelimiter));
                properlySplitCodes.addAll(strCode.split(this.foundDelimiter));
            }else{         
                properlySplitCodes.add(strCode);
            }
        }
        CRR_Code__c[] retCodes = new CRR_Code__c[]{};
        String codeTypeId = '';
        for(String strCode : properlySplitCodes){
            strCode = strCode.trim();
            system.debug(LoggingLevel.ERROR, 'fgw strCode+\':\'+this.theFirstParty == '+strCode+':'+this.theFirstParty);
            if(strCode != ''){
                retCodes.add(new CRR_Code__c(
                    Duplicate_Block__c = strCode+':'+this.theFirstParty,
                    Code_Return__c = this.theReturn.Id,
                    Code__c = strCode,
                    isDistributed__c = false,
                    Code_Distribution_Request_Id__c = ''
                ));
            }
        }
        set<String> errMsgs = new set<String>();
        Integer totalCodesReturned = 0;
        for(database.upsertResult ur : database.upsert(retCodes, CRR_Code__c.Duplicate_Block__c, false)){ //upsert
            for(Database.Error err : ur.errors){
                errMsgs.add(err.message);
            }
            if(ur.isSuccess() && (ur.errors == null || ur.errors.isEmpty())){
                totalCodesReturned++;
            }
        }
        CRR_Code_Type__c parent = [SELECT Id, Number_of_Codes__c, Number_of_codes_distributed__c FROM CRR_Code_Type__c WHERE Id = :this.theReturn.Code_Type__c];
        parent.Number_of_Codes_Distributed__c -= totalCodesReturned;
        parent.Number_of_Codes__c += totalCodesReturned;
        update parent;
        if(! errMsgs.isEmpty()){
            CRR_Code_Repository_Error__c crrErr = new CRR_Code_Repository_Error__c(
                Object_Id__c = this.theReturn.Id,
                Error__c = ''
            );
            for(String errMsg : errMsgs){
                crrErr.Error__c += errMsg + '|';
            }
            if(crrErr.Error__c.Length() > 32768) crrErr.Error__c = crrErr.Error__c.subString(0,32768);
            insert crrErr;
        }
    }
    global void finish(Database.BatchableContext ctxt){
        theReturn.Return_Errors__c = [SELECT count() FROM CRR_Code_Repository_Error__c WHERE Object_Id__c = :theReturn.Id] > 0;
        theReturn.Return_Complete__c = !theReturn.Return_Errors__c;
        update theReturn;
    }
    

}