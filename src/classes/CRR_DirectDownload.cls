public with sharing class CRR_DirectDownload{
    /*
    This class allows users to download their codes
    //Check to see if they are allowed access to these codes
    //if so, log and execute the download
    //if not, download empty file
    //
    //Download process
    //Check to see if there are more than 1/2M codes
    //If less or equal, download
    //If greater then direct them to download the pre-prepared file from the content library
    */
    public transient list<CRR_Code__c[]> nestedCodeGroup {get; set;}
    ApexPages.standardController controller;
    CRR_Code_Distribution_Request__c theReq;
    boolean userAllowedToAccessCodes = false;
    
    public CRR_DirectDownload(ApexPages.standardController cont){
        cont.addFields(new string[]{
            'Number_of_codes_to_deliver__c',
            'Code_Distribution_Recipient_Group__c',
            'Code_Distribution_Recipient__r.Recipient__c',
            'Prepared_File_Id__c',
            'Code_Type__r.Platform__c',
            'Code_Type__r.SKU1__c',
            'Code_Type__r.Name'
        });
        theReq = (CRR_Code_Distribution_Request__c) cont.getRecord();
        this.controller = cont;
        this.nestedCodeGroup = new list<CRR_Code__c[]>();

        //check direct distribution field
        if(UserInfo.getUserId() == theReq.Code_Distribution_Recipient__r.Recipient__c){
            this.userAllowedToAccessCodes = true;
        }
        if(! this.userAllowedToAccessCodes){ //not found in direct distribution, check group distribution
            this.userAllowedToAccessCodes = [SELECT count() //tell us how many
                                        FROM CRR_Code_Recipient_Group_Membership__c WHERE //groups there are that
                                        Id = :theReq.Code_Distribution_Recipient_Group__c //are the group on this distribution
                                        AND Code_Distribution_Recipient__r.Recipient__c = :UserInfo.getUserId()] //and the current user is a member
                                        > 0; //if this number is greater than zero then assign true to the var, otherwise false
        }
        system.debug('this.userAllowedToAccessCodes == '+this.userAllowedToAccessCodes);
        if(this.userAllowedToAccessCodes){
            if(isBlankOrNull(theReq.Prepared_File_Id__c)){
                Id cId = (Id) cont.getId();
                system.debug('cId == '+cId);
                for(CRR_Code__c[] codes : [SELECT Code__c, Game_Title__c, Platform__c, Ordered_By__c, Language__c, Type__c,
                                           Distribution_Approved_By__c, Distribution_Date__c FROM CRR_Code__c 
                                           WHERE Code_Distribution_Request_Id__c = :cId LIMIT 250000])
                {
                    this.nestedCodeGroup.add(codes);
                }
            }
        }
    }
    public string cleanS(String s){
        if(s == null) return '';
        return s;
    }
    public string getFilename(){
        return 'Vouchers' //filename
                +'-'+EncodingUtil.urlEncode((String.ValueOf(Integer.valueOf(this.theReq.Number_of_Codes_To_Deliver__c))),'UTF-8')
                +'-'+EncodingUtil.urlEncode(cleanS(this.theReq.Code_Type__r.SKU1__c),'UTF-8')
                +'-'+EncodingUtil.urlEncode(cleanS(this.theReq.Code_Type__r.Platform__c),'UTF-8')
                +'-'+EncodingUtil.urlEncode(this.theReq.Code_Type__r.Name,'UTF-8')
                +'.csv';
    }
    public pageReference doLogDownload(){
        pageReference pr;
        if(this.userAllowedToAccessCodes){
            logDownload(controller.getId());
            if(! isBlankOrNull(this.theReq.Prepared_File_Id__c)){
                pr = new PageReference(this.theReq.Prepared_File_Id__c+'&n='+getFilename());
            }
        }
        return pr;
    }
    public boolean isBlankOrNull(String s){ return s == '' || s == null; }
 
    @future
    public static void logDownload(Id distId){
        insert new CRR_Code_Distribution_Download__c(Code_Distribution_Request__c = distId);
    }
}