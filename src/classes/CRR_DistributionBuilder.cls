global class CRR_DistributionBuilder implements Database.batchable<CRR_Code_Distribution_Request__c>, Database.Stateful, Database.AllowsCallouts{
    static final integer CODES_PER_EXECUTE = Limits.getLimitDMLRows() - 1;
    global final CRR_Code_Distribution_Request__c[] distReqs; //this is the inflated list that will be processed
    global final CRR_Code_Distribution_Request__c[] onComplete; //this is the "flat" list that will be updated if there is an error
    global boolean error = false; //this is for transaction control
    
    global CRR_DistributionBuilder (set<Id> pDistReqIds){
        //this batch is kicked off by the insert event of a CRR_Code_Distribution_Request__c object
        //if you pass before insert records they won't have an id
        //if you pass after insert records they are read only
        //solve this by passing the ids set from the after trigger and querying editable versions
        CRR_Code_Distribution_Request__c[] pDistReqs = [SELECT Id, Code_Type__c, Number_of_Codes_to_Deliver__c, 
                                                            CreatedDate, Distribution_Approved_By__c 
                                                        FROM CRR_Code_Distribution_Request__c 
                                                        WHERE Id IN :pDistReqIds];
         
        //mark these as in process
        for(CRR_Code_Distribution_Request__c inProcReq : pDistReqs){
            inProcReq.Processing__c = true;
        }
        update pDistReqs;

        //we could be distributing a massive amount of codes (2.2M is the largest documented single code type)
        //the execute method below cannot write a 2.2M line file, or update 2.2M records, max records per DML is 10K
        //so add 1 record per 10k to distribute and iterate over that inflated list
        
        //this is the inflation list holder
        CRR_Code_Distribution_Request__c[] iterableDistRequests = new CRR_Code_Distribution_Request__c[]{};
        //build the inflated list
        for(CRR_Code_Distribution_Request__c distReq : pDistReqs){ //for each distribution request
            //store the original value so it can be reverted when we add this record to the flat list for error handling
            Decimal i = distReq.Number_of_Codes_to_Deliver__c;
            do{
                //set this instance of the record to the number of codes the distribution needs to have in its attachment
                CRR_Code_Distribution_Request__c disti = new CRR_Code_Distribution_Request__c(
                    Code_Type__c = distReq.Code_Type__c,
                    Id = distReq.Id,
                    Distribution_Approved_By__c = distReq.Distribution_Approved_By__c,
                    Distribution_Request_Date__c = distReq.CreatedDate
                );
                disti.Number_of_Codes_to_deliver__c = i > CODES_PER_EXECUTE ? CODES_PER_EXECUTE : i;
                if(disti.Number_of_Codes_to_deliver__c < 0) disti.Number_of_Codes_to_deliver__c = i + CODES_PER_EXECUTE;
                iterableDistRequests.add(disti); //add the sobject to the list so it will be processed once
                i -= CODES_PER_EXECUTE;
            }while(i > 0);
        } 
        this.onComplete = pDistReqs; //store the flat list of distribution requests (probably only be 1 ever, JIC though)
        this.distReqs = iterableDistRequests; //store the inflated list
    }
    global iterable<CRR_Code_Distribution_Request__c> start(Database.BatchableContext ctxt){
        return this.distReqs;
    }
    global void execute(Database.BatchableContext ctxt, CRR_Code_Distribution_Request__c[] scopes){
        try{
            CRR_Code_Distribution_Request__c dist = scopes[0];//for DML's sake set this to only 1 at a time please
            
            if(! this.error){
                //get the codes
                String qryString = 'SELECT Id, Code__c FROM CRR_Code__c WHERE Code_Type__c = \''+dist.Code_Type__c+'\'';
                //only pull codes that haven't been distributed or redeemed
                qryString += ' AND Code_Distribution_Request_Id__c = \'\' AND Redeemed_By__c = null ';
                //add limit
                qryString += ' LIMIT '+string.valueOf(dist.Number_of_Codes_to_deliver__c);
                //lock the rows so they aren't updated while we're working with them
                qryString +=' FOR UPDATE';
                CRR_Code__c[] codes = database.query(qryString);
                
                //prepare the distribution attachment
                for(CRR_Code__c code : codes){
                    code.Code_Distribution_Request_Id__c = dist.Id;
                    code.Code_Return__c = null;
                    code.Distribution_Approved_By__c = dist.Distribution_Approved_By__c;
                    code.Distribution_Date__c = dist.Distribution_Request_Date__c;
                }
                
                //update the codes
                Integer totalDistributed = 0;
                Database.SaveResult[] srs = database.update(codes);
                for(Database.SaveResult sr : srs){
                    if(sr.isSuccess()) totalDistributed++;
                }
                CRR_Code_Type__c parent = [SELECT Id, Number_of_Codes__c, Number_of_Codes_Distributed__c FROM CRR_Code_Type__c 
                                            WHERE Id = :dist.Code_Type__c];
                parent.Number_of_Codes__c -= totalDistributed;
                parent.Number_of_Codes_Distributed__c += totalDistributed;
                update parent;
            }
        }catch(Exception e){
            system.debug('fgw E == '+e);
            this.error = true;
        }        
    }

    global void finish(Database.BatchableContext ctxt){
        set<Id> reqs = new set<Id>();
        for(CRR_Code_Distribution_Request__c com : this.onComplete){
            reqs.add(com.Id);
        }
        CRR_Code_Distribution_Request__c[] reqsToUpdate = new CRR_Code_Distribution_Request__c[]{};
        set<Id> prepFiles = new set<Id>();

        for(CRR_Code_Distribution_Request__c req : [SELECT Id, Number_of_Codes_to_Deliver__c //,Number_of_Codes_Prepared__c
                                                    FROM CRR_Code_Distribution_Request__c 
                                                    WHERE Id IN :reqs])
        {
            if(req.Number_of_Codes_to_Deliver__c < 50000000){
                if(req.Number_of_Codes_to_Deliver__c >= 250000){
                    //greater than the amount of codes that can be delivered via download on demand
                    //but less than the number that can be processed in a single batch (50M)
                    //so if there were no errors add to the list of distribution Ids to send to the 
                    //usav1sfdmweb01.activision.com server so it can download and prepare the file
                    if(this.error == false) prepFiles.add(req.Id);
                }
                req.Processing_Error__c = this.error;
                req.Distribution_prepared__c = !this.error;
                req.Processing__c = false;
            }else{
                //TODO: build functionality to allow distributions greater than 50M
                //TO THIS BY: 
                //A - create the Distribution_Request__c.Number_of_Codes_Prepared__c field 
                //B - Maintain this field in the number of codes mainenance segment in the execute loop
                //C - Check here to see if all needed codes have been distributed
                //C.i - if not, kick off another batch
                //C.ii - If so: if(this.error == false) prepFiles.add(req.Id);
            }
            reqsToUpdate.add(req);
        }
        update reqsToUpdate;
        set<Id> nextSet = new set<Id>();
        for(CRR_Code_Distribution_Request__c nextDist : [SELECT Id FROM CRR_Code_Distribution_Request__c 
                                                          WHERE Processing__c = false AND
                                                          Distribution_prepared__c = false AND
                                                          Processing_Error__c = false LIMIT 1])
        {
            nextSet.add(nextDist.id);
        }
        Boolean isProd = CRR_Utils.getIsProd();
        if(prepFiles.size() > 0){
            DwProxy.service svc = new DwProxy.service();
            for(Id distReqId : prepFiles){
                if(LIMITS.getCallouts() < LIMITS.getLimitCallouts()){
                    try{
                        svc.codeRepoPrepDownload(isProd, distReqId);  //this will timeout as it waits for bulk query results
                    }catch(Exception e){}
                }
            }   
        }
        
        if(! nextSet.isEmpty()){
            database.executeBatch(new CRR_DistributionBuilder(nextSet), 1);
        }

    }
}