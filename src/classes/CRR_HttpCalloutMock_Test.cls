@isTest
global class CRR_HttpCalloutMock_Test implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        string response = '';
        HttpResponse res = new HttpResponse();
        if(req.getEndpoint().containsIgnoreCase('/services/async/28.0/job/')){
            if(req.getEndpoint().containsIgnoreCase('/result')){
                //response = 'line 1 of file body, something like';
                //response = 'code__c, status, message'; crlf
                //response = '123,FAILED,Duplicate found';
            }else{
                //CRR_BatchJobMointor.doMonitoring(), return jobInfo XML
                response = '<?xml version="1.0" encoding="UTF-8"?>';
                response += '<jobInfo xmlns="http://www.force.com/2009/06/asyncapi/dataload">';
                response += '<id>750K00000002fStIAI</id>';
                response += '<operation>insert</operation>';
                response += '<object>CRR_Code__c</object>';
                response += '<createdById>005K0000001oYITIA2</createdById>';
                response += '<createdDate>2014-01-17T18:46:32.000Z</createdDate>';
                response += '<systemModstamp>2014-01-17T18:46:49.000Z</systemModstamp>';
                response += '<state>Closed</state>';
                response += '<concurrencyMode>Parallel</concurrencyMode>';
                response += '<contentType>CSV</contentType>';
                response += '<numberBatchesQueued>0</numberBatchesQueued>';
                response += '<numberBatchesInProgress>0</numberBatchesInProgress>';
                response += '<numberBatchesCompleted>20</numberBatchesCompleted>';
                response += '<numberBatchesFailed>0</numberBatchesFailed>';
                response += '<numberBatchesTotal>20</numberBatchesTotal>';
                response += '<numberRecordsProcessed>199999</numberRecordsProcessed>';
                response += '<numberRetries>0</numberRetries>';
                response += '<apiVersion>28.0</apiVersion>';
                response += '<numberRecordsFailed>89999</numberRecordsFailed>';
                response += '<totalProcessingTime>1071264</totalProcessingTime>';
                response += '<apiActiveProcessingTime>886766</apiActiveProcessingTime>';
                response += '<apexProcessingTime>0</apexProcessingTime>';
                response += '</jobInfo>';
            }
            /*String xmlString = '<response>';
            xmlString += '<id>'+anyId+'</id>';
            xmlString += '<operation>someOperation</operation>';
            xmlString += '<object>someObject</object>';
            xmlString += '<createdById>'+anyId+'</createdById>';
            xmlString += '<state>Closed</state>';
            xmlString += '<concurrencyMode>someConcurrencyMode</concurrencyMode>';
            xmlString += '<contentType>someContentType</contentType>';
            xmlString += '<numberBatchesQueued>1</numberBatchesQueued>';
            xmlString += '<numberBatchesInProgress>2</numberBatchesInProgress>';
            xmlString += '<numberBatchesCompleted>5</numberBatchesCompleted>';
            xmlString += '<numberBatchesFailed>4</numberBatchesFailed>';
            xmlString += '<numberBatchesTotal>5</numberBatchesTotal>';
            xmlString += '<numberRecordsProcessed>6</numberRecordsProcessed>';
            xmlString += '<numberRetries>7</numberRetries>';
            xmlString += '<apiVersion>30.0</apiVersion>';
            xmlString += '<numberRecordsFailed>0</numberRecordsFailed>';
            xmlString += '<totalProcessingTime>300</totalProcessingTime>';
            xmlString += '<apiActiveProcessingTime>300</apiActiveProcessingTime>';
            xmlString += '<apexProcessingTime>300</apexProcessingTime>';
            xmlString += '</response>';*/
            
            res.setHeader('Content-Type', 'application/xml');
            res.setBody(response);
            res.setStatusCode(200);
            return res;
        }
        return null;
    }
}