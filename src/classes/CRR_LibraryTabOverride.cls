public class CRR_LibraryTabOverride{

    public CRR_LibraryTabOverride(ApexPages.StandardController controller) {}
    public CRR_LibraryTabOverride(){}

    public pageReference goToCodeLibrary(){
        try{
            Id codeRepoId = [SELECT Id FROM ContentWorkspace WHERE Name LIKE 'Code Repo%' LIMIT 1].Id;
            pageReference pr = new pagereference('/'+codeRepoId);
            pr.setRedirect(true);
            return pr;
        }catch(Exception e){
        }
        return null;
    }
    
}