global class CRR_Monitor implements Schedulable{
    //v0.1 User>>SFDC Library>>Middleware server>>BODS>>SFDC Stage>>SFDC Target = very long processing time, maybe 10h for 1M records
    //v0.2 User>>SFDC Library>>Middleware server>>SFDC Target via Bulk Upload = less processing time, 5h for 1M records 
        //(trigger on CRR_Code__c.object slow upload)

    global void execute(SchedulableContext SC){
        doOffCycleMonitoring(false);
    }
    public static Integer getDistributionsPending(){
        return [SELECT count() FROM CRR_Code_Distribution_Request__c WHERE Processing__c = false AND Distribution_prepared__c = false AND
                                                              Processing_Error__c = false];
    }
    public static void startDistributionProcessing(){
        set<Id> nextSet = new set<Id>();
        for(CRR_Code_Distribution_Request__c nextDist : [SELECT Id FROM CRR_Code_Distribution_Request__c 
                                                          WHERE Processing__c = false AND
                                                          Distribution_prepared__c = false AND
                                                          Processing_Error__c = false LIMIT 2])
        {
            nextSet.add(nextDist.id);
        }
        database.executeBatch(new CRR_DistributionBuilder(nextSet), 1);
    }

    public static boolean getCodesToClean(){
        return [SELECT count() FROM CRR_Code__c 
                            WHERE Code_Distribution_Request_Id__c = '' AND isDistributed__c = true LIMIT 1] > 0;
    }
    public static void startCleaningCodes(){
        database.executebatch(new CRR_CleanCodes());
    }

    public static integer getReturnsPending(){
        return [SELECT count() FROM CRR_Code_Return__c WHERE Queued__c = true];
    }
    public static void startReturnsProcessing(){
        CRR_Code_Return__c toProcess = [SELECT Id, Delimiter__c, First_Party__c, Code_Type__c FROM CRR_Code_Return__c 
                                                WHERE Queued__c = true LIMIT 1];
        if([SELECT count() FROM Attachment WHERE ParentId = :toProcess.Id] > 0){
            Attachment[] atts = [SELECT Body FROM Attachment WHERE ParentId = :toProcess.Id 
                                    ORDER BY CreatedDate ASC LIMIT 1];
            if(! atts.IsEmpty())
                database.executebatch(new CRR_CodeReturnBatch(atts[0].Body.toString(), toProcess));
        }else{
            toProcess.Queued__c = false;
            update toProcess;
        }
    }

    //public static Id doBODSmonitoring(Boolean limitToOneBatch){
      //  return [select id from user limit 1].id;
    //}

    public static void doOffCycleMonitoring(Boolean limitToOneBatch){
        //these actions could have tasks that were deferred while other tasks were running:
        //CRR_CleanCodes (after a distribution is deleted need to maintain the isDistributed__c checkbox)
        //CRR_DistributionBuilder (If a distribution was deferred because the system was being cleaned or codes were being returned)
        //CRR_CodeReturn (if a distribution was being executed or codes were being cleaned)
        if(!CRR_Status.isRunning){
            String nextProcess = getWhichProcessIsNext();
            if(nextProcess == 'distribute'){
                startDistributionProcessing();
            }else if(nextProcess == 'clean'){
                startCleaningCodes();
            }else if(nextProcess == 'return'){
                startReturnsProcessing();
            }
        }
    }
    public static string getWhichProcessIsNext(){
        string nextProcess = 'unknown';
        //check for pending distributions
        Integer foundDistributions = getDistributionsPending(); 
        
        //check for codes to clean
        boolean foundCleans = getCodesToClean();
        
        //check for returns pending processing
        Integer foundReturns = getReturnsPending();

        //start the relevant process
        if(foundDistributions == 0 && foundCleans == false && foundReturns == 0){//no work needed
            nextProcess = 'no pending jobs';
        }else if(foundDistributions > 0 && foundCleans == false && foundReturns == 0){//only need to do distributions, so let's do it
            nextProcess = 'distribute';
        }else if(foundDistributions == 0 && foundCleans == true && foundReturns == 0){//only need to do cleaning, so let's do it
            nextProcess = 'clean';
        }else if(foundDistributions == 0 && foundCleans == false && foundReturns > 0){//only need to return, so let's do it
            nextProcess = 'return';
        }else{//we need to do more than one thing... which one take priority?
            if(foundDistributions > 0 && foundCleans == true && foundReturns > 0){//need to do all 3!
                nextProcess = 'clean';
            }else if(foundDistributions == 0 && foundCleans == true && foundReturns > 0){//clean & return
                //cleaning occurs when the code isn't distributed but is marked as such, which is an artifact of the return process
                //so returning again could exacerbate the issue.  Do a clean before a return
                nextProcess = 'clean';
            }else if(foundDistributions > 0 && foundCleans == false && foundReturns > 0){//distribute and return
                //distribution could depend upon some of the codes in the return, return before distribute
                nextProcess = 'return';
            }else if(foundDistributions == 0 && foundCleans == true && foundReturns > 0){//distribute and clean
                //distribution could depend upon some of the codes needing to be cleaned, so clean before distribute
                nextProcess = 'clean';
            }
        }
        return nextProcess;

    }
    //schedule is at 8, 23, 37 & 52 to minimize risk of it conflicting with the file download process
    public static void setSchedule(){
        ApexCodeSetting__c[] newJobs = new ApexCodeSetting__c[]{};
        for(String s : new String[]{'8','23','37','52'}){
            ApexCodeSetting__c existingJob = ApexCodeSetting__c.getValues('CRR_Monitor '+s);
            if(existingJob != null){
                existingJob.Description__c = system.schedule('CRR_Monitor '+s, '0 '+s+' * * * ?' , new CRR_Monitor());
                newJobs.add(existingJob);
            }else{
                newJobs.add(new ApexCodeSetting__c(
                    Name = 'CRR_Monitor '+s,
                    Description__c = system.schedule('CRR_Monitor '+s, '0 '+s+' * * * ?' , new CRR_Monitor())
                ));
            }
        }
        database.upsert(newJobs, false);
        for(AsyncApexJob job : [SELECT Id FROM AsyncApexJob WHERE Status = 'Queued' AND ApexClass.Name = 'CRR_Monitor']){
            System.abortJob(job.Id);
        }
    }
    public static void removeSchedule(){
        String[] jobIds = new String[]{};
        for(String s : new String[]{'8','23','37','52'}){
            ApexCodeSetting__c monitor = ApexCodeSetting__c.getValues('CRR_Monitor '+s);
            if(monitor != null){
                system.abortJob(monitor.Description__c);
            }
        }
    }


}