public class CRR_Status{
    public static boolean isRunning = [SELECT count() 
                                       FROM AsyncApexJob 
                                       WHERE ApexClass.Name LIKE 'CRR%' AND Status IN ('Queued','Processing','Preparing') 
                                       AND ApexClass.Name != 'CRR_BatchJobMonitor'
                                       LIMIT 1] > 0;
    AsyncApexJob[] pCurrentlyRunningJobs;
    public AsyncApexJob[] getCurrentlyRunningJobs(){
        if(pCurrentlyRunningJobs == null) pCurrentlyRunningJobs = database.query(getAsyncJobQueryString('Processing'));
        return pCurrentlyRunningJobs;
    }

    AsyncApexJob[] pQueuedJobs;
    public AsyncApexJob[] getQueuedJobs(){
        if(pQueuedJobs == null) pQueuedJobs = database.query(getAsyncJobQueryString('Queued'));
        return pQueuedJobs;
    }

    AsyncApexJob[] pPreparingJobs;
    public AsyncApexJob[] getPreparingJobs(){
        if(pPreparingJobs == null) pPreparingJobs = database.query(getAsyncJobQueryString('Preparing'));
        return pPreparingJobs;
    }

    AsyncApexJob[] pCompletedJobs;
    public AsyncApexJob[] getCompletedJobs(){
        if(pCompletedJobs == null) pCompletedJobs = database.query(getAsyncJobQueryString('Completed'));
        return pCompletedJobs;
    }
    public static string getAsyncJobQueryString(String statusType){
        String query = 'SELECT CreatedBy.Name, JobType, ApexClass.Name, CreatedDate, CompletedDate, Status ';
        query += ', TotalJobItems, JobItemsProcessed, NumberOfErrors ';
        query += 'FROM AsyncApexJob WHERE ApexClass.Name LIKE \'CRR%\' AND Status = \''+statusType+'\'';
        query += ' AND ApexClass.Name != \'CRR_BatchJobMonitor\' ORDER BY CreatedDate DESC LIMIT 100';
        return query;
    }
    
    ContentVersion[] pDocumentsToProcess;
    public ContentVersion[] getDocumentsToProcess(){
        if(pDocumentsToProcess == null)pDocumentsToProcess = database.query(getDocumentQueryString(false));
        return pDocumentsToProcess;
    }
    ContentVersion[] pDocumentsProcessed;
    public ContentVersion[] getDocumentsProcessed(){
        if(pDocumentsProcessed == null) pDocumentsProcessed = database.query(getDocumentQueryString(true));
        return pDocumentsProcessed;
    }
    public static string getDocumentQueryString(Boolean returnProcessedDocuments){
        String query = 'SELECT Id FROM ContentVersion WHERE CreatedBy.Name = \'123\' LIMIT 1000';
        try{
            Id repoRecTypId = Schema.SObjectType.ContentVersion.getRecordTypeInfosByName().get('Code repository codes').getRecordTypeId();
            query = 'SELECT Id,Activation_Date__c,CreatedById,Billable__c,Title,Expiration_Date__c,First_party__c, First_Party_Group__c,';
            query += 'First_party_reference_number__c,Game__c, FileType,Language_Choices__c,Platform__c, Promotion_Code__c,Code_Type__c,';
            query += 'Code_Product__r.Name, CreatedDate FROM ContentVersion WHERE RecordTypeId = \''+repoRecTypId+'\' AND ';
            query += 'Retrieved_for_Processing__c = '+returnProcessedDocuments +' AND isLatest = true ORDER BY CreatedDate DESC LIMIT 1000';
        }catch(Exception e){//no such record type
            try{
                ApexPages.addMessages(e);
            }catch(Exception ex){}//not on a VF page
            system.debug(e);
        }
        return query;
    }
    public boolean getCanStart(){ 
        return (isRunning == false && getNextTaskType() != 'no pending jobs');
    }
    public pageReference startJob(){
        CRR_Monitor.doOffCycleMonitoring(false);
        return null;
    }
    public void currentStatus(){
        ApexPages.Message status = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Idle');
        if(isRunning){
            status = new ApexPages.Message(ApexPages.Severity.WARNING, 'Processing...');
        }
        ApexPages.addMessage(status);
    }
    public string getNextTaskType(){
        return CRR_Monitor.getWhichProcessIsNext();
    }
    
    public String getLastIntegrationCheckIn(){
        String result = '';
        try{
            ApexCodeSetting__c lastCheckIn = ApexCodeSetting__c.getInstance('Code Repository Check In');
            Double d = lastCheckIn.Number__c;
            DateTime dt = DateTime.newInstance(d.longValue());
            result = dt.format();
        }catch(Exception e){
            result = e.getMessage();
        }
        return result;
    }


}