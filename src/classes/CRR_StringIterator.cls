global class CRR_StringIterator implements Iterator<String>{ 

    String codes {get; set;} 
    String delimiter {get; set;}

    public CRR_StringIterator(String pCodes, String pDelimiter){ 
        this.codes = pCodes;
        this.delimiter = pDelimiter;
    }   

    global boolean hasNext(){ 
        return this.codes.indexOf(this.delimiter) >= 0 || this.codes != '' ;
    }    

    global String next(){ 
        String nextVal;
        if(this.codes.contains(this.delimiter)){       
            nextVal = this.codes.subStringBefore(delimiter);
            this.codes = this.codes.subStringAfter(this.delimiter);
        }else{
            nextVal = this.codes;
            this.codes = '';
        }
        return nextVal;
    } 
}