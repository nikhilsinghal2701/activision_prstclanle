global class CRR_StringIteratorIMPL implements iterable<String>{
    global final string codes;
    global final string delimiter;
    global CRR_StringIteratorIMPL(String pCodes, String pDelimiter){
        this.codes = pCodes;
        this.delimiter = pDelimiter;
    }
    global Iterator<String> Iterator(){
       return new CRR_StringIterator(this.codes, this.delimiter);
    }
}