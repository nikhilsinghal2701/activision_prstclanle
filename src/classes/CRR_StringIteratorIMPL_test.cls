@isTest
public class CRR_StringIteratorIMPL_test{
    public static testMethod void test1(){
        CRR_StringIteratorIMPL tstCls = new CRR_StringIteratorIMPL('123,456,789', ',');
        Iterator<string> strng = tstCls.Iterator();
        String next = strng.next();
        system.assert(strng.hasNext());
        system.assertEquals('123',next);
    }
}