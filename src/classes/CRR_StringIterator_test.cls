@isTest
public class CRR_StringIterator_test{
    public static testMethod void test1(){
        CRR_StringIterator tstCls = new CRR_StringIterator('123,456,789', ',');
        system.assert(tstCls.hasNext());
        String next = tstCls.next();
        system.assertEquals(next,'123');
    }
}