@isTest
public class CRR_Trigger_Test{
    public static testMethod void testCRR_PopulateDupeBlock(){
        CRR_Code_Distribution_Recipient_Group__c grp = new CRR_Code_Distribution_Recipient_Group__c();
        insert grp;
        CRR_Code_Distribution_Recipient__c recip = new CRR_Code_Distribution_Recipient__c();
        recip.Recipient__c = [SELECT id FROM User WHERE ContactId = null AND 
                              IsActive = TRUE AND Email LIKE '%@activision.com' AND
                              Profile.Name = 'System Administrator'
                              LIMIT 1].Id;
        insert recip;
        
        CRR_Code_Recipient_Group_Membership__c rec = new CRR_Code_Recipient_Group_Membership__c(
            Code_Distribution_Recipient_Group__c = grp.Id,
            Code_Distribution_Recipient__c = recip.Id
        );
        
        insert rec;
        CRR_Code_Recipient_Group_Membership__c rec2 = [SELECT Dupe_Block__c FROM CRR_Code_Recipient_Group_Membership__c WHERE Id = :rec.Id];
        system.assertEquals(grp.Id+':'+recip.Id, rec2.Dupe_Block__c);
        
    }
}