@isTest
private class CSAT_Answer_Trigger_Test {
	
	@isTest static void test_method_one() {

		Contact gamer = UnitTestHelper.generateTestContact();
        insert gamer;

        Case c = UnitTestHelper.generateTestCase();
        c.ContactID = gamer.Id;
        c.Comments__c = 'Test';
        insert c;

		CSAT_Survey_Response__c csr = new CSAT_Survey_Response__c(Survey_Name__c='Test Name',Case__c=c.Id,Gamer__c=gamer.Id);
		insert csr;

		CSAT_Survey_Response_Answer__c csra1 = new CSAT_Survey_Response_Answer__c(Answer_Number__c=1,CSAT_Survey_Response__c=csr.Id);
		insert csra1;

		CSAT_Survey_Response_Answer__c csra2 = new CSAT_Survey_Response_Answer__c(Answer_Date_Time__c=Datetime.Now(),CSAT_Survey_Response__c=csr.Id);
		insert csra2;

		CSAT_Survey_Response_Answer__c csra3 = new CSAT_Survey_Response_Answer__c(CSAT_Survey_Response__c=csr.Id);
		insert csra3;

		CSAT_Survey_Response_Answer__c test1 = [Select Id,Answer_Number__c,Answer_Text__c From CSAT_Survey_Response_Answer__c Where Id=:csra1.Id];
		CSAT_Survey_Response_Answer__c test2 = [Select Id,Answer_Date_Time__c,Answer_Text__c From CSAT_Survey_Response_Answer__c Where Id=:csra2.Id];
		CSAT_Survey_Response_Answer__c test3 = [Select Id,Answer_Boolean__c,Answer_Text__c From CSAT_Survey_Response_Answer__c Where Id=:csra3.Id];

		System.assertEquals(test1.Answer_Text__c,String.valueOf(test1.Answer_Number__c));
		System.assertEquals(test2.Answer_Text__c,String.valueOf(test2.Answer_Date_Time__c));
		System.assertEquals(test3.Answer_Text__c,String.valueOf(test3.Answer_Boolean__c));



	}
	
	
	
}