global class CSAT_Survey_Batch implements Schedulable, Database.Batchable<id>, Database.AllowsCallouts {
    
    //Do all the data prep work here, in the scheduled class
    //scheduled class constructor
    public CSAT_Survey_Batch(){}
    global void execute(SchedulableContext sc) {
        prepSurveyData();
    }
    //ATVI scheduled class business rule schedule
    public static void setSchedule(){
        System.schedule('CSAT_Survey_Batch-13', '0 13 * * * ?', new CSAT_Survey_Batch());
    }
    //scheduled class data prep
    public static boolean prepSurveyData(){
        map<Id, sObject> caseIdToRecordToSurvey = new map<Id, sObject>();
        Contact[] contactsWithCasesClosedToday;
        Decimal percentOfContactsToSurvey;
        Integer numberOfDaysBeforceReSurvey;
        Integer maxTwitterInvitationsPerRun;
        
        try{
            percentOfContactsToSurvey = ApexCodeSetting__c.getValues('Twitter_Daily_Survey_Percentage').Number__c;
            numberOfDaysBeforceReSurvey = (-1 * (Integer) ApexCodeSetting__c.getValues('Twitter_Survey_Quarantine_Days').Number__c);
            ApexCodeSetting__c maxPerRun = ApexCodeSetting__c.getValues('Twitter_Max_Per_Run');
            if(maxPerRun.IsActive__c == true) maxTwitterInvitationsPerRun = (Integer) maxPerRun.Number__c;
        }catch(Exception e){
            return false; //if the custom settings don't exist, or we can't use them, do not send surveys
        }
        
        Id[] contactIds = new Id[]{}; //for the case query
        map<Id, Case> contactId2Case = new map<Id, Case>();
        map<Id, Case> casesNotSurvey = new map<Id, Case>();
        datetime lastHour = datetime.now().addHours(-1);
        /*for(Case c : [SELECT ContactId FROM Case WHERE ClosedDate >: lastHour AND ContactId != '' AND 
                      Status != 'Auto-Closed' AND RecordType.Name like '%social%' ])*/
        for(Case c : [SELECT ContactId FROM Case WHERE ClosedDate >: lastHour AND ContactId != '' AND 
                      (Status = 'Auto-Closed' or Status = 'Closed') AND RecordType.Name like '%social%' AND
                     last_cs_social_response__c != ''])
        {
            contactId2Case.put(c.ContactId, c);
            contactIds.add(c.ContactId);
        }
        
        if(contactIds.IsEmpty())
            return false; //no one to survey
        
        casesNotSurvey = contactId2Case.clone();
        
        contactsWithCasesClosedToday = [SELECT Email, (SELECT ParentId, MediaProvider, Provider, R6Service__MediaProvider__c 
                                                       FROM Personas where Allegiance_Opt_Out__c = false) 
                                        FROM Contact 
                                        WHERE Id IN :contactIds AND 
                                        (Contact.Last_Social_Survey__c = null OR 
                                         Contact.Last_Social_Survey__c < :system.today().addDays(numberOfDaysBeforceReSurvey))
                                       ];
        Integer numberOfContactsToSurvey = (Integer) Math.ceil(contactsWithCasesClosedToday.size() * (percentOfContactsToSurvey / 100));
        Integer recipientsFound = 0;
        Integer twitterRecipientsFound = 0;
        String[] contactId2CaseId = new String[]{};
        
        for(Contact c : contactsWithCasesClosedToday){
            if(c.Email != null && c.Email != ''){
                caseIdToRecordToSurvey.put(contactId2Case.get(c.Id).Id, c);
                contactId2CaseId.add((string)c.Id + '-' + (string) contactId2Case.get(c.Id).Id);
                casesNotSurvey.remove(c.id);
                recipientsFound++;
            }else if(twitterRecipientsFound < maxTwitterInvitationsPerRun){
                for(SocialPersona s : c.Personas){
                    if(s.R6Service__MediaProvider__c == 'Twitter' || s.MediaProvider == 'Twitter' || s.Provider == 'Twitter'){
                        caseIdToRecordToSurvey.put(contactId2Case.get(c.Id).Id, s);
                        contactId2CaseId.add((string)c.Id + '-' + (string) contactId2Case.get(c.Id).Id);
                        casesNotSurvey.remove(c.id);
                        recipientsFound++;
                        twitterRecipientsFound++;
                        break; //out of the socialPersona loop
                    }
                }
            }
            if(recipientsFound == numberOfContactsToSurvey) {
                //Find remaining cases and marked Medallia Survey not required field = TRUE
                list<case> casesToUpdate = new list<case>() ;
                for (case ca: casesNotSurvey.values()){
                    ca.Medallia_Survey_Not_Required__c = true;
                    casesToUpdate.add(ca);
                }
                update casesToUpdate; 
                break;
            }
        }
        recordSurveyEvent(contactId2CaseId);
        Database.executeBatch(new CSAT_Survey_Batch(caseIdToRecordToSurvey), 10);
        return true;
    }
    @future
    public static void recordSurveyEvent(String[] contactsBeingSurveyed){
        map<Id, Id> contactId2caseId = new map<Id, Id>();
        for(String idhid : contactsBeingSurveyed){
            String[] ids = idhid.split('-');
            contactId2caseId.put(ids[0], ids[1]);
        }
        Contact[] contactsToUpdate = [SELECT Id, Last_Social_Survey__c, Social_Survey_Case_Id__c FROM Contact 
                                      WHERE Id = :contactId2caseId.keyset()];
        for(Contact c : contactsToUpdate){
            c.Last_Social_Survey__c = system.today();
            c.Social_Survey_Case_Id__c = contactId2caseId.get(c.Id);
        }
        update contactsToUpdate;
    }
    
    //do all of the processing here in the batch class
    map<Id, sObject> caseIdToRecordToSurvey;
    //batch class constructor
    public CSAT_Survey_Batch(map<Id, sObject> pCaseIdToRecordToSurvey) {
        caseIdToRecordToSurvey = pCaseIdToRecordToSurvey;
    }
    
    global Id[] start(Database.BatchableContext BC) {
        Id[] idList = new Id[]{};
        idList.addAll(caseIdToRecordToSurvey.keySet());
        return idList;
    }
    
    global void execute(Database.BatchableContext BC, Id[] scope) {
        Contact[] contactsToUpdate = new contact[]{};
        String[] caseId_hyphen_personaId = new String[]{};
        Integer max10enforcement = 0;
        for(Id key : scope){
            sObject obj = caseIdToRecordToSurvey.get(key);
            if(obj.getSobjectType() == Contact.getSobjectType()){
                //don't do anything here, Tibco will pick the case up overnight
            }else{
                caseId_hyphen_personaId.add(key+'-'+(string)obj.get('id'));
            }
            max10enforcement++;
            if(max10enforcement == 10) break;
        }
        
        if(caseId_hyphen_personaId.size() > 0) tweetSurveyLink(caseId_hyphen_personaId);
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
    //@future(callout=true)
    public static void tweetSurveyLink(String[] caseId_hyphen_personaId){
        //prep the data
        String[] personaIds = new String[]{};
        String[] caseIds = new String[]{};

        for(String toSplit : caseId_hyphen_personaId){
            String[] splitVal = toSplit.split('-');
            caseIds.add(splitVal[0]);
            personaIds.add(splitVal[1]);
        }
        
        map<Id, SocialPersona> sps = new map<Id, SocialPersona>([SELECT Id, Name FROM SocialPersona WHERE Id IN :personaIds]);
        //Get Base URL for shortener
        string baseURL = [select base_url__c from LinkForce_Settings__c limit 1].base_URL__c;
        baseURL = baseURL.replaceAll('http://','');

        list<short_URL__c> shortList = new list<short_URL__c>();
        map<SocialPersona,Short_URL__c> spToShort = new map<SocialPersona,Short_URL__c>();
        //do the work
        for(Integer i = 0; i < personaIds.size(); i++){
            SocialPersona sp = sps.get(personaIds[i]);
            //Soql the case to pull Product,createddate
            case c = [SELECT Id, CaseNumber, Product_L1__c, CreatedDate, First_Activision_Update_By__r.FirstName, LastModifiedBy.Name,
                      Issue_Type__c, Issue_Sub_Type__c, L3__c, LastModifiedBy.FirstName
                      FROM Case WHERE id=:caseIds[i]];
            //Create Short URL
            short_URL__c sh = new short_URL__c();
            //Build Personalized Survey URL
            String sUrl = '';
            sURL = 'https://activision.allegiancetech.com/cgi-bin/qwebcorporate.dll?idx=DHSMQQ'; //74 characters
            sURL += '&incident_id='+c.CaseNumber; //21 chars
            sURL += '&title=' + c.Product_L1__c; //c.Product_L1__c max 255 characters
            sURL += '&transaction_date=' + encodingutil.urlencode(string.valueof(c.createddate.format()),'UTF-8'); //createdDate = 23 chars
            sURL += '&incident_creation_channel=Twitter';
            sURL += '&psid='+c.id; //18 chars
            sUrl += '&agent='; //User.FirstName is 40 chars max
            if(c.First_Activision_Update_By__c == null)
                sURL += c.First_Activision_Update_By__r.FirstName;
            else
                sURL += c.LastModifiedBy.FirstName;
            sURL += '&issue_type='+c.Issue_Type__c; //All custom picklist fields are max 255 chars, so 255 here
            sURL += '&issue_subtype='+c.Issue_Sub_Type__c; //255 here
            sURL += '&issue_cause='+c.L3__c; //255 here
            //current "max" URL (18-Apr-2014) based on format above is 1277 chars
            
            if(sURL.length() <= 255)
                sh.URL__c = sURL;
            else{
                sh.URL__c = sURL.subString(0, 254); //url field is required can not leave it blank
                sh.Long_URL__c = sURL; //long URL is 2500 chars, so double the amount needed (room for growth if necessary)
            }
            
            //Custom URL uses case ID for bounce removal
            sh.custom_url__c = 's'+c.id;
            sh.Social_Name__c = sp.id;
            
            spToShort.put(sp,sh);

            system.debug('sh == '+sh);
        }
        
        Twitter twit = new Twitter();
        map<short_URL__c,string> shortToPost = new map<short_URL__c,string>();
        for(SocialPersona sp:spToShort.keySet()){
            String personalizedSurveyLink = baseURL + spToShort.get(sp).custom_url__c;
//            String tweetMessage = 'Hello DM'+sp.Name+' we are interested in your support experience! '+personalizedSurveyLink;
//            shortToPost.put(spToShort.get(sp),Twit.sendMessage(tweetMessage));
            String tweetDirectMessage = 'Hello '+sp.Name+' we are interested in your support experience! '+personalizedSurveyLink;
            system.debug('tweetDirectMessage == '+tweetDirectMessage);
            shortToPost.put(
                spToShort.get(sp),
                //Twit.sendMessage(tweetMessage)
                Twit.sendDirectMessage(tweetDirectMessage, sp.Name)
            );
        }
        
        list<short_url__c> shToUpdate = new list<short_url__c>();
        for(short_url__c shurl:shortToPost.keySet()){
            shurl.Twitter_Post_ID__c = shortToPost.get(shurl);
            shToUpdate.add(shurl);
        }
        insert shToUpdate;
        
    }
    
}