@isTest
public class CSAT_Survey_Batch_Test{
    public static testmethod void CSAT_Survey_Batch_Test() {
        
        apexcodesetting__c acs = new apexcodesetting__c();
        acs.name = 'Twitter_Daily_Survey_Percentage';
        acs.description__c = 'test';
        acs.Number__c = 1;
        insert acs;

        apexcodesetting__c acs1 = new apexcodesetting__c();
        acs1.name = 'Twitter_Survey_Quarantine_Days';
        acs1.description__c = 'test';
        acs1.Number__c = 1;
        insert acs1;
        
        Account testAccount= new Account();
        testAccount.Name = 'Test account';
        testAccount.Type__c = 'Test User';
        insert testAccount;
        
        RecordType testrecordtype = [select id from recordtype where name like '%social%' limit 1];
        
        
        User testUser = [SELECT Subscriptions__c, Street, State, Id,
                PostalCode, Phone, MobilePhone, LastName,
                Gender__c, FirstName, Email, Country, ContactId,
                IsPortalEnabled, City, Birthdate__c, LanguageLocaleKey
             FROM User LIMIT 1];

    
        Contact testContact = new Contact();
        testContact.LastName = 'Test User';
        testContact.Languages__c = 'English';
        testContact.MailingCountry = 'US';
        testContact.AccountId = testAccount.id;
        insert testContact;
        
        SocialPersona sp = new SocialPersona();
        sp.ParentId = testContact.id;
        sp.Name = 'test';
        sp.Provider = 'Twitter';
        insert sp;
        
        Case testTokenCase = UnitTestHelper.generateTestCase();

        testTokenCase.AccountId = testAccount.Id;
        testTokenCase.ContactId = testContact.Id;
        
        testTokenCase.Description = 'This is a Description';
        testTokenCase.Gamertag__c = 'Gamertag';
        testTokenCase.SEN_ID__c = 'SEN ID';
        testTokenCase.Token_Code__c = '1234567890123';
        testTokenCase.Error_Reason__c = 'This is an Error Reason';
        testTokenCase.Subject = 'This is Subject';
        testTokenCase.RecordTypeId = testrecordtype.id;
        testTokenCase.W2C_Origin_Language__c = 'en_US';
        
        testTokenCase.Type = 'RMA';   
        testTokenCase.Status = 'Closed';
        testTokenCase.Closed_By__c = testUser.id;
        insert testTokenCase;
        
        PageReference notfound = new PageReference( urlHomeController.DEFAULT_URL );
        PageReference redirect = new PageReference('http://www.salesforce.com');
        
        linkforce_settings__c ls = new linkforce_settings__c();
        ls.base_url__c = 'http://csuat-activisionsupport.cs9.force.com/lf/';
        ls.Default_URL__c = 'http://rewards.activision.com';
        insert ls;
        
        Short_URL__c shurl = new Short_URL__c();
        shurl.url__c = redirect.getURL();
        shurl.Custom_URL__c = 'test';
        shurl.Long_URL__c = 'activision.allegiancetech.com/cgi-bin/qwebcorporate.dll?idx=DHSMQQ';
        insert shurl;
        
        integration_credential__c ic = new integration_credential__c();
        ic.Application_Name__c = 'Test';
        ic.access_token__c = '123';
        ic.refresh_token__c = '123';
        ic.api_key__c = '123';
        ic.api_secret__c = '123';
        ic.Twitter_Enabled__c = true;
        insert ic;
        
        test.startTest();
            try{
                //FGW: Quang, I updated this because calling setSchedule() when the job is 
                //FGW: already scheduled will cause the test to fail.  So...
                //remove the job if it exists
                string jobName = 'CSAT_Survey_Batch-13';
                for(CronTrigger ct : [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name = :jobName])
                    system.abortJob(ct.Id);
                
                //Then add the new one
                CSAT_Survey_Batch.setSchedule();
                
                //Now assert behavior
                boolean jobExists = false;
                for(CronTrigger ct : [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name = :jobName])
                    jobExists = true;
                
                system.assert(jobExists);
            }catch(Exception e){
            }
        
        test.stopTest();
        
    }    
}