@isTest
private class CSAT_Survey_Response_Trigger_Test {
	
	@isTest static void test_method_one() {
		Id queue = [Select Id From Group Where Name = 'CSAT Survey Response Queue' And Type = 'Queue' Limit 1].Id;
		User u = UnitTestHelper.generateTestUser();
		u.ProfileId=[select Id from Profile where Name = 'System Administrator' Limit 1].Id;
		insert u;
		Contact c = UnitTestHelper.generateTestContact();
		insert c;
		Case ca = UnitTestHelper.generateTestCase();
		ca.Comments__c = 'Test';
		ca.ContactId = c.Id;
		insert ca;
		Case cawithnumber = [Select Id, CaseNumber From Case Where Id =:ca.Id];
		string casenumber = cawithnumber.CaseNumber;
		string casenumbernozero = casenumber.subString(1);
		CSAT_Survey_Response__c csr = new CSAT_Survey_Response__c(Case_Number__c = casenumbernozero, Status__c = 'Open', Reason__c ='Product', Contacted__c = True);
		insert csr;
		CSAT_Survey_Response__c testcsr = [Select Id, Case__c, Gamer__c, OwnerId From CSAT_Survey_Response__c Where Id = :csr.Id];

		system.assertEquals(ca.Id,testcsr.Case__c);
		system.assertEquals(c.Id,testcsr.Gamer__c);
		system.assertEquals(queue,testcsr.OwnerId);
		
	}
	
	
	
}