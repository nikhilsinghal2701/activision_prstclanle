global class CS_CodeDistribution implements Database.Batchable<sObject>, Schedulable {
	
    //SCHEDULEABLE
    global void execute(SchedulableContext SC){
        database.executeBatch(new CS_CodeDistribution());
    }

    global static void setSchedule(){
        system.schedule('CS_CodeDistribution-04', '0 4 * * * ?', new CS_CodeDistribution());
        system.schedule('CS_CodeDistribution-14', '0 14 * * * ?', new CS_CodeDistribution());
        system.schedule('CS_CodeDistribution-24', '0 24 * * * ?', new CS_CodeDistribution());
        system.schedule('CS_CodeDistribution-34', '0 34 * * * ?', new CS_CodeDistribution());
        system.schedule('CS_CodeDistribution-44', '0 44 * * * ?', new CS_CodeDistribution());
        if(! Test.isRunningTest()) system.schedule('CS_CodeDistribution-54', '0 54 * * * ?', new CS_CodeDistribution());
    }

    //BATCH
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Platform__c, Replacement_Code__c, First_Party_Account_Id__c';
        query += ' , Code_Type__c, Unique_Key__c, Game_Title__c, CreatedById, Case__c';
        query += ' FROM Customer_Code_Request__c';
        query += ' WHERE Resolution__c = \'Issue Code\' AND';
        query += ' Replacement_Code__c = null AND';
        query += ' Duplicate__c = false';
        query += ' ORDER BY CreatedDate ASC';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        //filter out duplicates
        map<String, Customer_Code_Request__c> key2Issue = new map<String, Customer_Code_Request__c>();
        Customer_Code_Request__c[] dupes = new Customer_Code_Request__c[]{};

        //duplicate check 1, in case we have duplciates in scope
        //also preps our data for out of scope duplicate check 2
        for(Customer_Code_Request__c issue : (Customer_Code_Request__c[]) scope){
            if(key2Issue.containsKey(issue.Unique_Key__c)){ 
                issue.Duplicate__c = true;
                issue.Resolution__c = 'Duplicate';
                issue.Status__c = 'Closed';
                dupes.add(issue);
                continue;
            }
            key2Issue.put(issue.Unique_Key__c, issue);
        }

        //duplicate check 2, more likely event that the duplicate exists "out of scope" 
        for(Customer_Code_Request__c dupeIssue : [SELECT Unique_Key__c FROM Customer_Code_Request__c 
                                                  WHERE Unique_Key__c IN :key2Issue.keySet() 
                                                    AND Replacement_Code__c != null])
        {
            Customer_Code_Request__c dupe = key2Issue.get(dupeIssue.Unique_Key__c);
            dupe.Duplicate__c = true;
            dupe.Resolution__c = 'Duplicate';
            dupe.Status__c = 'Closed';
            dupes.add(dupe);
            key2Issue.remove(dupeIssue.Unique_Key__c);
        }
        update dupes;

        //do distribution
        Customer_Code_Request__c[] toUpdate = new Customer_Code_Request__c[]{};
        map<String, Customer_Code_Request__c[]> issueKey2issues = new map<String, Customer_Code_Request__c[]>();
        for(Customer_Code_Request__c rec : key2Issue.values()){
            String issueKey = rec.Game_Title__c.toLowerCase() + rec.Code_Type__c.toLowerCase() + rec.Platform__c.toLowerCase();

            Customer_Code_Request__c[] innerLoopList = new Customer_Code_Request__c[]{};
            if(issueKey2issues.containsKey(issueKey)) innerLoopList = issueKey2Issues.get(issueKey);
            innerLoopList.add(rec);
            issueKey2issues.put(issueKey, innerLoopList);
        
        }
        for(String key : issueKey2issues.keySet()){
            Customer_Code_Request__c[] results = doAssignment(issueKey2issues.get(key),key);
            for(Customer_Code_Request__c result : results){
            	system.debug(LoggingLevel.ERROR, result);
            }
            toUpdate.addAll(results);
        }
        update toUpdate;
    }

    public Customer_Code_Request__c[] doAssignment(Customer_Code_Request__c[] recs, String issueKey){
        Customer_Code_Request__c[] results = new Customer_Code_Request__c[]{};
        system.debug(LoggingLevel.ERROR, 'issueKey == '+issueKey);
        if(recs.size() > 0){
            set<String> contactIds = new set<String>();
            for(Customer_Code_Request__c iss : recs){
                contactIds.add(iss.CreatedById);
            }
            
            map<Id, User> userId2User = new map<Id, User>([SELECT Id, ContactId FROM User WHERE Id IN :contactIds]);
            
            Integer foundCodeCount = 0, loopCount = 0;
            String codeQuery = 'SELECT Code__c FROM CRR_Code__c WHERE CS_Code_Name__c = \'' + String.escapeSingleQuotes(issueKey) + '\'';
            codeQuery += ' AND Redeemed_by__c = null LIMIT '+recs.size();
            system.debug('codeQuery == '+codeQuery);
            CRR_Code__c[] codes = new CRR_Code__c[]{};
            CRR_Code__c[] availCodes = (CRR_Code__c[]) database.query(codeQuery);
			foundCodeCount = availCodes.size();
                
            while(loopCount < recs.size() && loopCount < foundCodeCount){
                if(userId2User.containsKey(recs[loopCount].CreatedById)){
                    recs[loopCount].Replacement_Code__c = availCodes[loopCount].Code__c;
                    availCodes[loopCount].Redeemed_by__c = userId2User.get(recs[loopCount].CreatedById).ContactId;
                    codes.add(availCodes[loopCount]);
                }
                loopCount++;
            }
            
            update codes;
           
        	Case[] toClose = new Case[]{};
            CaseComment[] toComment = new CaseComment[]{};
        	for(Customer_Code_Request__c rec : recs){
                if(rec.Replacement_Code__c == null || rec.Replacement_Code__c == ''){
                    rec.Resolution__c = 'No Codes Available';
                }else{
                    toComment.add(new CaseComment(ParentId = rec.Case__c, CommentBody = rec.Replacement_Code__c));
                }
                rec.status__c = 'Closed';
                results.add(rec);
                toClose.add(new Case(Id = rec.Case__c, Status = 'Closed'));
            }
            try{
            	update toClose;
        	}catch(Exception e){}

        }
        return results;
    }
    
    global void finish(Database.BatchableContext BC) {}
 
}