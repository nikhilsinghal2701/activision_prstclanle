public class CS_Form_Controller {

  public List<CS_Form_Field__c> CSformFields {get;set;}
  public CS_Form__c CSform {get;set;}
  public Parent_CS_Form__c CSParentForm {get;set;}
  public list<CS_Form__c> CSformList {get;set;}
  public List<SelectOption> selectOptions {get;set;}
  public map<String, SelectOption[]> fieldName2SelectOptions {get; set;}
  public string userLanguage { get; set; }
  public boolean showSubmissionMsg { get; set; }
  public boolean exceededMaxFileSize { get; set; }
  public boolean FieldRequired {get; set;}
  public boolean MultiPlayerAccountPresent {get; set;}
  public string RequiredFieldName {get; set;}
  public boolean invalidFileType {get; set;}
  public boolean NoFormWithSelectedLang {get; set;}
  public string submittedCaseNumber { get; set; }
  public Contact ContactObj = new Contact();
  public selectOption[] MultiPlayerAccounts { get; set; }
  public map<Id, Multiplayer_Account__c> mpaId2mpa = new map<Id, Multiplayer_Account__c>();
  public Case CaseObj = new Case();
  public Group g;
  public string dispositionProduct {get; set;}
  public string dispositionProductOther {get; set;}
  public string dispositionSubProduct {get; set;}
  public string dispositionPlatform {get; set;}
  public transient string subjectvalue;
  public transient string descriptionValue;
  public transient string ISPValue;
  public transient string gamertagValue;
  public transient string senIDValue;
  public transient string commentValue;

  //Form Uploaded Document
  public Attachment uploadedFile { get ; set; }
  public transient integer uploadedFileSize { get; set; }
  public transient string uploadedFileName { get; set; }
  public transient blob uploadedFileBody { get; set; }
  public string uploadedFileContentType { get; set; }

  public CS_Form_Controller(ApexPages.StandardController controller) {
      CSParentForm = (Parent_CS_Form__c)controller.getRecord();
      
      Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
      if(tempCookie != null){
          userLanguage = tempCookie.getValue();            
      }else{
        userLanguage = 'en_US';
      }
      fieldName2SelectOptions = new map<String, SelectOption[]>{''=>new SelectOption[]{}, null=>new SelectOption[]{}};
  }
    
  public void fieldDetails(){
      CSformList = [Select id, Create_Case__c, Is_Active__c, Name__c, Case_Status__c, Case_Not_Editable_by_Gamer__c, Case_Queue_Name__c, 
                    Submission_Message__c, Case_Not_Visible_by_Gamer__c, 
                    Max_Input_File_Size__c, File_Size_Exception_Message__c, Product_L1__c, Product_L2__c, Sub_Product_DLC__c, Platform__c, 
                    Issue_Type__c, Issue_Sub_Type__c, L3__c,
                    W2C_Origin_Language__c, Medallia_Survey_Not_Required__c, Notify_Gamer_on_Form_Submission__c, Product_Effected__c, 
                    Parent_CS_Form__c, Form_Language__c, Create_Code_Request_Record__c, Code_Reqeust_Record_Status__c
                    FROM CS_Form__c WHERE Parent_CS_Form__c =:CSParentForm.id and Form_Language__c =:userLanguage Limit 1];
      
      if (CSformList.size() > 0){ //use the native language form if possible
          CSform  = CSformList[0];
      }else{ //default to en_US
        CSform  = [Select id, Create_Case__c, Is_Active__c, Name__c, Case_Status__c, Case_Not_Editable_by_Gamer__c, Case_Queue_Name__c, 
                   Submission_Message__c, Case_Not_Visible_by_Gamer__c, 
                   Max_Input_File_Size__c, File_Size_Exception_Message__c, Product_L1__c, Product_L2__c, Sub_Product_DLC__c, Platform__c, 
                   Issue_Type__c, Issue_Sub_Type__c, L3__c,
                   W2C_Origin_Language__c, Medallia_Survey_Not_Required__c, Notify_Gamer_on_Form_Submission__c, Product_Effected__c, 
                   Parent_CS_Form__c, Form_Language__c, Create_Code_Request_Record__c, Code_Reqeust_Record_Status__c
                   FROM CS_Form__c WHERE Parent_CS_Form__c =:CSParentForm.id and Form_Language__c =:'en_US' limit 1];        
        
        NoFormWithSelectedLang = true;
      }
        
      CSformFields = [SELECT Checkbox__c, Field_Type__c, Rich_Text_Area__c, Text__c, Text_Area__c, Text_Area_Long__c, PicklistValues__c, 
                        Label__c, Is_Required__c, Mapping_to_Case__c, GTText__c, Save_To_Field__c, Tool_Tip__c, Customer_Code_Request_Field_Mapping__c
                        FROM CS_Form_Field__c WHERE CS_Form__c =: CSform.id Order by Sorting_Order__c];
        
        if (CSform.Case_Queue_Name__c != null && CSform.Case_Queue_Name__c != ''){
            g = [Select id from Group where Name =: CSform.Case_Queue_Name__c LIMIT 1];
        }
        
        for (CS_Form_Field__c csfFld: CSformFields ){
            if(csfFld.PicklistValues__c != null && csfFld.PicklistValues__c != ''){
                List<SelectOption> tmpSelectOptions = new List<SelectOption>();
                List<String> picklistVals = csfFld.PicklistValues__c.split(',');
                for(String pVal : picklistVals){
                    tmpSelectOptions.add(new SelectOption(pVal, pVal));
                }
                fieldName2SelectOptions.put(csfFld.Label__c, tmpSelectOptions);
            }
        }

        showSubmissionMsg = false;
        Id LoggedInUserId = UserInfo.getUserId();
        User LoggedInUser = [Select id, ContactId From User Where Id =: LoggedInUserId limit 1]; 
        if (LoggedInUser.ContactId != null)
            ContactObj = [Select Id From Contact Where Id =: LoggedInUser.ContactId ];
        
        if (CSform.Product_L1__c != null)
            dispositionProduct = CSform.Product_L1__c;         
        if (CSform.Product_L2__c != null)
            dispositionProductOther = CSform.Product_L2__c;  
        if (CSform.Sub_Product_DLC__c != null)
            dispositionSubProduct = CSform.Sub_Product_DLC__c; 
        if (CSform.Platform__c != null)
            dispositionPlatform = CSform.Platform__c;
            
        if(ContactObj != null){
          try{
              DWSingleIdentity siSvc = new DWSingleIdentity(new ApexPages.standardController(ContactObj));
              siSvc.updateLinkedAccounts();
            }catch(Exception e){
              system.debug('LAS CALLOUT ERROR == '+e);
            }
            this.mpaId2mpa = new map<Id, Multiplayer_Account__c>([SELECT Id, Platform__c, Gamertag__c, DWID__c FROM Multiplayer_Account__c 
                                                                  WHERE Contact__c = :ContactObj.id AND
                                                                  Platform__c IN ('XBL','PSN') AND
                                                                  Is_Current__c = TRUE AND DWID__c != ''
                                                                  ]);
            if (this.mpaId2mpa != null && this.mpaId2mpa.size()>0){
                MultiPlayerAccounts = new selectOption[]{};
                for(Id mpaId : this.mpaId2mpa.keySet()){
                    MultiPlayerAccounts.add(new selectOption(
                      this.mpaId2mpa.get(mpaId).DWID__c, 
                      this.mpaId2mpa.get(mpaId).Gamertag__c+' ('+this.mpaId2mpa.get(mpaId).Platform__c+')'));
                }
                MultiPlayerAccountPresent = true;
            }
        }                   
    }

    public static boolean isBlankOrNull(String s){ return s == '' || s == null; }
    
    public PageReference submitForm(){
        RecordType generalSupportType = [SELECT id FROM RecordType WHERE Name = 'General Support' LIMIT 1];
        CaseObj.RecordTypeId = generalSupportType.id;
        if (ContactObj != null)
            CaseObj.ContactId = ContactObj.id;
        CaseObj.Origin = 'Web Form';
        CaseObj.Web_Form_Name__c = CSform.Name__c;
        CaseObj.Comment_Not_Required__c = true;
        CaseObj.subject = '';
        CaseObj.Gamertag__c = '';
        CaseObj.W2C_Origin_Language__c = userLanguage;
        CaseObj.W2C_Route_To_Custom_Queue__c = true;
        String ccrCodeType='',ccrFirstPartyAccount='',ccrGameTitle='',ccrPlatform='';
    
        for(CS_Form_Field__c csfFld : CSformFields ){
            //////// Field required validations//////////////////
            if(csfFld.Is_Required__c == true){
                if (csfFld.Field_Type__c == 'Text' && (csfFld.Text__c == null || csfFld.Text__c == '' )){
                    RequiredFieldName = csfFld.Label__c;
                    return null;
                }
                if (csfFld.Field_Type__c == 'TextAreaLong' && (csfFld.Text_Area_Long__c == null || csfFld.Text_Area_Long__c == '' )){
                    RequiredFieldName = csfFld.Label__c;
                    return null;
                }  
                if (csfFld.Field_Type__c == 'Checkbox' && csfFld.Checkbox__c == false){
                    RequiredFieldName = csfFld.Label__c;
                    return null;
                }   
                if (csfFld.Field_Type__c == 'Picklist' && (csfFld.Text__c == null || csfFld.Text__c == '' )){
                    RequiredFieldName = csfFld.Label__c;
                    return null;
                }                                            
            }

            if(! isBlankOrNull(csfFld.Customer_Code_Request_Field_Mapping__c)){
              //these fields are too prone to typos, so should always be picklists
              if(csfFld.Customer_Code_Request_Field_Mapping__c == 'Code Type'){
                ccrCodeType=csfFld.Text__c; 
              }else if(csfFld.Customer_Code_Request_Field_Mapping__c == 'First Party Account'){
                ccrFirstPartyAccount=csfFld.GTText__c; //always needs to be GT Picklist
              }else if(csfFld.Customer_Code_Request_Field_Mapping__c == 'Game Title'){
                ccrGameTitle=csfFld.Text__c; 
              }else if(csfFld.Customer_Code_Request_Field_Mapping__c == 'Platform'){
                ccrPlatform=csfFld.Text__c;
              }
            }


            if(! isBlankOrNull(csfFld.Save_To_Field__c)){
                if((csfFld.Field_Type__c == 'Text' || csfFld.Field_Type__c == 'Picklist') && ! isBlankOrNull(csfFld.Text__c)){
                    ContactObj.put(csfFld.Save_To_Field__c.split('\\.')[1], csfFld.Text__c);
                }else if (csfFld.Field_Type__c == 'TextAreaLong' && ! isBlankOrNull(csfFld.Text_Area_Long__c)){
                  ContactObj.put(csfFld.Save_To_Field__c.split('\\.')[1], csfFld.Text_Area_Long__c);
                }else if (csfFld.Field_Type__c == 'Checkbox'){
                  ContactObj.put(csfFld.Save_To_Field__c.split('\\.')[1], csfFld.Checkbox__c);
                }
            }

            ///// Mapping for Description Field of Case Object ////////  
            if(csfFld.Mapping_to_Case__c!= null && csfFld.Mapping_to_Case__c.contains('Description')){
              Integer descriptionFieldLength = Schema.SObjectType.Case.fields.Description.getLength();
              if (csfFld.Text_Area_Long__c != null && csfFld.Text_Area_Long__c != ''){
                  if (descriptionValue != null && descriptionValue != ''){
                      descriptionValue += ', ' + csfFld.Text_Area_Long__c;
                  }else{
                      descriptionValue = csfFld.Text_Area_Long__c;
                  }
              }
              if (descriptionValue != null && descriptionValue.length()> descriptionFieldLength ){
                  CaseObj.description =  descriptionValue.substring(0,descriptionFieldLength);
              }else{
                  CaseObj.description =  descriptionValue;
              }
            }              
            
            ///// Mapping for Subject Field of Case Object ////////
            if(csfFld.Mapping_to_Case__c!= null && csfFld.Mapping_to_Case__c.contains('Subject')){
              Integer subjectFieldLength = Schema.SObjectType.Case.fields.Subject.getLength();
              if (csfFld.Text__c != null && csfFld.Text__c != ''){
                  if (subjectvalue != null && subjectvalue!= ''){
                      subjectvalue += ', ' + csfFld.Text__c;
                  }else{
                      subjectvalue = csfFld.Text__c;
                  }
              }
              if (subjectvalue != null && subjectvalue.length()> subjectFieldLength  ){
                  CaseObj.subject =  subjectvalue.substring(0,subjectFieldLength);
              }else{
                  CaseObj.subject =  subjectvalue;
              }
            }             
            
            ///// Mapping for ISP Field of Case Object ////////
            if(csfFld.Mapping_to_Case__c!= null && csfFld.Mapping_to_Case__c.contains('ISP')){
              Integer ISPFieldLength = Schema.SObjectType.Case.fields.ISP__c.getLength();
              if (csfFld.Text__c != null && csfFld.Text__c != ''){
                  if (ISPValue != null && ISPValue != ''){
                      ISPValue += ', ' + csfFld.Text__c;
                  }else{
                      ISPValue = csfFld.Text__c;
                  }
              }
              if (ISPValue != null && ISPValue.length()> ISPFieldLength ){
                  CaseObj.ISP__c =  ISPValue.substring(0,ISPFieldLength);
              }else{
                  CaseObj.ISP__c =  ISPValue;
              }
            }             
            
            ///// Mapping for Gamertag Field of Case Object ////////
            if(csfFld.Mapping_to_Case__c!= null && csfFld.Mapping_to_Case__c.contains('Gamertag')){
              Integer gamertagFieldLength = Schema.SObjectType.Case.fields.Gamertag__c.getLength();
              if (csfFld.Text__c != null && csfFld.Text__c != ''){
                  if (gamertagValue != null && gamertagValue != ''){
                      gamertagValue += ', ' + csfFld.Text__c;
                  }else{
                      gamertagValue = csfFld.Text__c;
                  }
              }
              if (gamertagValue != null && gamertagValue.length()> gamertagFieldLength ){
                  CaseObj.Gamertag__c =  gamertagValue.substring(0,gamertagFieldLength);
              }else{
                  CaseObj.Gamertag__c =  gamertagValue;
              }
            }              
            
            ///// Mapping for SEN ID Field of Case Object ////////      
            if(csfFld.Mapping_to_Case__c!= null && csfFld.Mapping_to_Case__c.contains('SEN ID')){
              Integer senIdFieldLength = Schema.SObjectType.Case.fields.SEN_ID__c.getLength();
              if (csfFld.Text__c != null && csfFld.Text__c != ''){
                  if (senIDValue != null && senIDValue != ''){
                      senIDValue += ', ' + csfFld.Text__c;
                  }else{
                      senIDValue = csfFld.Text__c;
                  }
              }
              if (senIDValue != null && senIDValue.length()> senIdFieldLength ){
                  CaseObj.SEN_ID__c =  senIDValue.substring(0,senIdFieldLength );
              }else{
                  CaseObj.SEN_ID__c =  senIDValue;
              }
            }             
            
            ///// Mapping for Case Comment ////////
            if(csfFld.Mapping_to_Case__c!= null && csfFld.Mapping_to_Case__c.contains('Case Comment')){
              Integer caseCommentLength = Schema.SObjectType.CaseComment.fields.CommentBody.getLength();
              if(commentValue == null) commentValue = '';
              if ( csfFld.Field_Type__c == 'Picklist' && csfFld.Text__c != null && csfFld.Text__c != ''){
                  if (commentValue!= null && commentValue!= ''){
                      commentValue += '\n' + csfFld.Label__c + ': '+ csfFld.Text__c;
                  }else{
                      commentValue += csfFld.Label__c + ': '+ csfFld.Text__c;
                  }
              }
              try{
                if (csfFld.Field_Type__c == 'GT Picklist' && csfFld.GTText__c != null && csfFld.GTText__c != ''){
                  if (commentValue!= null && commentValue!= ''){
                        commentValue += '\n' + csfFld.Label__c + ': '+ csfFld.GTText__c;
                  }else{
                        commentValue += csfFld.Label__c + ': '+ csfFld.GTText__c;
                  } 
                }
              }catch(Exception e){
                system.debug(e);
              }
              if ( csfFld.Field_Type__c == 'Checkbox'){
                  if (commentValue!= null && commentValue!= ''){
                      commentValue += '\n' + csfFld.Label__c + ': '+ csfFld.Checkbox__c;
                  }else{
                      commentValue += csfFld.Label__c + ': '+ csfFld.Checkbox__c;
                  }                 
              
              }
              if (commentValue != null && commentValue.length()> caseCommentLength ){
                  commentValue = commentValue.substring(0,caseCommentLength);
              }
            }        
        }

        if (CSform.Case_Status__c != null){
            CaseObj.Status = CSform.Case_Status__c;    
        }else{
            CaseObj.Status = 'Open';
        }
        if (CSform.Case_Not_Editable_by_Gamer__c == true){
            CaseObj.Case_Not_Editable_by_Gamer__c = true;    
        }
        if (CSform.Case_Not_Visible_by_Gamer__c == true){
            CaseObj.Case_Not_Visible_by_Gamer__c = true;    
        } 
        
        if (CSform.Medallia_Survey_Not_Required__c == true)
            CaseObj.Medallia_Survey_Not_Required__c = true;  
        if (CSform.Notify_Gamer_on_Form_Submission__c == true)
            CaseObj.Notify_Gamer_on_Form_Submission__c = true;                             
        
        /////////// Disposition related fields population  ///////////////////////   
        if (CSform.Product_L1__c != null)
            CaseObj.Product_L1__c = CSform.Product_L1__c;
            
        if (CSform.Product_L2__c != null)
            CaseObj.Product_L2__c = CSform.Product_L2__c;  
            
        if (CSform.Sub_Product_DLC__c != null)
            CaseObj.Sub_Product_DLC__c = CSform.Sub_Product_DLC__c; 
            
        if (CSform.Platform__c != null)
            CaseObj.Platform__c = CSform.Platform__c;   
            
        if (CSform.Issue_Type__c != null)
            CaseObj.Issue_Type__c = CSform.Issue_Type__c;   
            
        if (CSform.Issue_Sub_Type__c != null)
            CaseObj.Issue_Sub_Type__c = CSform.Issue_Sub_Type__c; 
            
        if (CSform.L3__c != null)
            CaseObj.L3__c = CSform.L3__c; 
            
        if(CaseObj.Product_L1__c=='Other') CaseObj.Product_L1__c = CaseObj.Product_L2__c;
          CaseObj.Type = CaseObj.Issue_Type__c;                                                                              
        
        
        ////////// Assigning Public Product //////////////////////////
        if (CSform.Product_Effected__c != null)
            CaseObj.Product_Effected__c = CSform.Product_Effected__c; 
            
        if(CaseObj.Product_Effected__c == null){
            for(Public_Product__c prod : [SELECT Id FROM Public_Product__c WHERE Medallia_Product_1__c  = :CaseObj.Platform__c AND Medallia_Product_2__c = :CaseObj.Product_L1__c LIMIT 1]){
                CaseObj.Product_Effected__c = prod.Id;
            }
        }                 
        
        system.debug('File Size$$$$$$$$$$$$$$'+uploadedFileBody);
        if (uploadedFileSize != null && CSform.Max_Input_File_Size__c != null && uploadedFileSize > (CSform.Max_Input_File_Size__c * 1024 * 1024)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,CSform.File_Size_Exception_Message__c));
            exceededMaxFileSize = true;
            uploadedFileName = null;
            uploadedFileBody = null;
            return null;
        }
        if(uploadedFileBody != null && uploadedFileContentType != 'image/jpeg' && uploadedFileContentType != 'image/png'){
            invalidFileType = true;
            uploadedFileName = null;
            uploadedFileBody = null;
            return null;
        }
        
        if(ccrCodeType.containsIgnoreCase('atlas') && uploadedFileBody == null){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'File required for Atlas editions.'));
          return null;
        }
        
        insert CaseObj;
        
        if (commentValue != null && commentValue != ''){
            CaseComment caseComm = new CaseComment();
            caseComm.CommentBody = commentValue;
            caseComm.IsPublished = true;
            caseComm.ParentId = CaseObj.id;
            insert caseComm;    
        }        
        if (uploadedFileName != null && uploadedFileBody != null){
            Attachment tmpAttach = new Attachment();
            tmpAttach.ParentId = CaseObj.id;
            tmpAttach.Name = uploadedFileName;
            tmpAttach.Body = uploadedFileBody;
            insert tmpAttach;
        }
        if(CSform.Create_Code_Request_Record__c && !ccrCodeType.containsIgnoreCase('atlas')){
          Customer_Code_Request__c theRequest = new Customer_Code_Request__c();
          theRequest.Code_Type__c = ccrCodeType;
          theRequest.First_Party_Account_Id__c = ccrFirstPartyAccount;
          theRequest.Game_Title__c = ccrGameTitle;
          theRequest.Platform__c = ccrPlatform;
          theRequest.Resolution__c = CSform.Code_Reqeust_Record_Status__c;
          theRequest.Language__c = userLanguage;
          theRequest.Case__c = CaseObj.Id;
          insert theRequest;
        }

        system.debug('***********************'+CaseObj.id);
        CaseObj = [Select id, CaseNumber FROM Case Where id =:CaseObj.id];
        submittedCaseNumber = CaseObj.CaseNumber;
        showSubmissionMsg = true;
        if (g != null)
            CaseObj.OwnerId = g.id;
        update CaseObj;        
        return null;
    }

    public boolean getRequiredLinkedAccountPresent(){
      if(CSform.Create_Code_Request_Record__c) return MultiPlayerAccountPresent;
      return true;
    }

}