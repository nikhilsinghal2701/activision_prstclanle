@isTest(SeeAllData=true)
private class CS_Form_Controller_Test{
    static testMethod void testOne(){

        //Create Contact:
        Contact testContact = new Contact();
        testContact.FirstName = 'con First Name';
        testContact.LastName = 'con Last Name';
        testContact.Email = 'conEmail@test.com';
        testContact.Birthdate = date.newinstance(1990, 2, 2);
        testContact.UCDID__c = 'abcdef';
        insert testContact;
        
        //Create User:
        Profile p = [SELECT Id FROM Profile WHERE Name='ATVI OHV Cust Port User']; 
          User u = new User(Alias = 'standt', Email='conEmail@test.com', 
              EmailEncodingKey='UTF-8', LastName='con Last Name', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
          u.ContactId = testContact.id; 
          
        //QueueSobject thisQueue = [Select q.Queue.id, q.Queue.Name, q.SobjectType, q.Id From QueueSobject q where q.SobjectType ='Case' limit 1];
        //string thisQueueName = thisQueue.Queue.Name;             

        Parent_CS_Form__c parentCSform = new Parent_CS_Form__c();
        parentCSform.Name__c = 'Test Parent Form';
        insert parentCSform;
        
        CS_Form__c CSform = new CS_Form__c();
        CSform.Name__c = 'Test CS Form';
        CSform.Form_Language__c = 'en_US';
        CSform.Case_Status__c = 'Resolved';
        CSform.Case_Not_Editable_by_Gamer__c = true;
        CSform.Case_Not_Visible_by_Gamer__c = true;
        CSform.Medallia_Survey_Not_Required__c = true;
        CSform.Notify_Gamer_on_Form_Submission__c = true;
        CSform.Product_L1__c = 'Other';
        CSform.Product_L2__c = 'Call of Duty';
        CSform.Sub_Product_DLC__c = 'Core Game';
        CSform.Platform__c = 'Playstation';
        CSform.Issue_Type__c = 'Administration';
        CSform.Issue_Sub_Type__c = 'Feedback';
        CSform.L3__c = 'General Positive Feedback';
        CSform.Case_Queue_Name__c = 'Web Form Queue';
        CSform.Parent_CS_Form__c = parentCSform.id;
        insert CSform;
        
        CS_Form_Field__c csFormField = new CS_Form_Field__c();
        csFormField.Label__c = 'New Text Field';
        csFormField.Field_Type__c = 'Text';
        csFormField.Is_Required__c = true;
        csFormField.Mapping_to_Case__c = 'Subject;Gamertag;ISP;SEN ID';
        csFormField.Text__c = 'Test Value';
        csFormField.Sorting_Order__c = 1;
        csFormField.CS_Form__c = CSform.id;
        insert csFormField;   
        
        CS_Form_Field__c csFormField2 = new CS_Form_Field__c();
        csFormField2.Label__c = 'New Picklist Field';
        csFormField2.Field_Type__c = 'Picklist';
        csFormField2.Is_Required__c = true;
        csFormField2.Mapping_to_Case__c = 'Case Comment';
        csFormField2.Sorting_Order__c = 2;
        csFormField2.PicklistValues__c = 'value one, value two';
        csFormField2.Text__c = 'value one';
        csFormField2.CS_Form__c = CSform.id;
        insert csFormField2; 
        
        CS_Form_Field__c csFormField3 = new CS_Form_Field__c();
        csFormField3.Label__c = 'New Text Area Long Field';
        csFormField3.Field_Type__c = 'TextAreaLong';
        csFormField3.Is_Required__c = true;
        csFormField3.Mapping_to_Case__c = 'Description';
        csFormField3.Text_Area_Long__c = 'Test Description';
        csFormField3.Sorting_Order__c = 3;
        csFormField3.CS_Form__c = CSform.id;
        insert csFormField3; 
        
        CS_Form_Field__c csFormField4 = new CS_Form_Field__c();
        csFormField4.Label__c = 'New Checkbox Field';
        csFormField4.Field_Type__c = 'Checkbox';
        csFormField4.Is_Required__c = true;
        csFormField4.Mapping_to_Case__c = 'Case Comment';
        csFormField4.Sorting_Order__c = 4;
        csFormField4.Checkbox__c = true;
        csFormField4.CS_Form__c = CSform.id;
        insert csFormField4;                                   
        
     
        //system.runAs(u){                            
            ApexPages.currentPage().getParameters().put('Id', parentCSform.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(parentCSform);
            CS_Form_Controller csFormController = new CS_Form_Controller(sc);
                
            csFormController.userLanguage = 'en_US';
            csFormController.uploadedFileName = 'Test Attachment';
            csFormController.uploadedFileBody = Blob.valueOf('Test Attachment Body');
            csFormController.uploadedFileContentType = 'image/jpeg';        
            
    
            csFormController.fieldDetails();
            csFormController.submitForm();
        //}

        
    }

}