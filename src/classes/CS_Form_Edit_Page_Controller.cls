public class CS_Form_Edit_Page_Controller{

    public CS_Form__c csFormObj{get;set;}
    public selectOption[] l3options{get;set;}
    public boolean GrayOut{get;set;}
    public boolean ppReq{get;set;}
    
    public CS_Form_Edit_Page_Controller(ApexPages.StandardController stdController){
        csFormObj = (CS_Form__c)stdController.getRecord();
        GrayOut = true;
        
        if (csFormObj.id != null)
            csFormObj = [Select id, Case_Not_Editable_by_Gamer__c, Case_Queue_Name__c, Case_Status__c, Create_Case__c, Is_Active__c, Name__c, 
                         Submission_Message__c, Case_Not_Visible_by_Gamer__c, Max_Input_File_Size__c, File_Size_Exception_Message__c,
                         Product_L1__c, Product_L2__c, Sub_Product_DLC__c, Platform__c, Issue_Type__c, Issue_Sub_Type__c, L3__c, W2C_Origin_Language__c,
                         Medallia_Survey_Not_Required__c, Notify_Gamer_on_Form_Submission__c, Product_Effected__c, Parent_CS_Form__c, Form_Language__c,
                         Create_Code_Request_Record__c, Code_Reqeust_Record_Status__c
                         FROM CS_Form__c WHERE id=:csFormObj.id];        

        if(csFormObj.id != null){
            getL3s(); //load the picklist with the values
            if(csFormObj.L3__c != null && csFormObj.L3__c != ''){ //an l3 has already been chosen, let's make it shown as selected
                l3Options.remove(0); //remove none from the list
                for(integer i = 0; i < l3Options.size(); i++){ //remove the duplicate entry
                    if(l3Options[i].getValue() == csFormObj.L3__c){
                        l3Options.remove(i);
                        break;
                    }
                }
                if(l3Options.size() > 0){
                    l3Options.add(0, new selectOption(csFormObj.L3__c, csFormObj.L3__c)); //add existing l3 as selected
                }else{
                    l3Options.add(new selectOption(csFormObj.L3__c, csFormObj.L3__c)); //add existing l3 as selected
                }
            }
        }else setupL3();
    }
    
    public List<SelectOption> getQueues() {
        List<SelectOption> options = new List<SelectOption>(); 
        for (QueueSobject  q : [Select Queue.id, Queue.Name, SobjectType, Id From QueueSobject where SobjectType =:'Case' ORDER BY Queue.Name]){
            options.add(new SelectOption(q.Queue.Name, q.Queue.Name)); 
        }
        return options;
    } 
        
    public pageReference setupL3(){
        system.debug('l3Options == '+l3Options);
        l3Options = new selectOption[]{new selectOption('','--None--')};
        system.debug('l3Options == '+l3Options);
        ppReq = false;
        return null;
    }

    public pageReference getL3s(){
        GrayOut = false;
        system.debug('l3Options == '+l3Options);
        setupL3();
        system.debug('l3Options == '+l3Options);
        csFormObj.Issue_Type__c = csFormObj.Issue_Type__c == null ? '' : csFormObj.Issue_Type__c;
        csFormObj.Issue_Sub_Type__c = csFormObj.Issue_Sub_Type__c== null ? '' : csFormObj.Issue_Sub_Type__c;
        String dispositionQuery = 'SELECT L3__c FROM Disposition__c WHERE L1__c = \''+String.escapeSingleQuotes(csFormObj.Issue_Type__c);
        dispositionQuery += '\' AND L2__c = \''+String.escapeSingleQuotes(csFormObj.Issue_Sub_Type__c)+'\' AND ';
        if(csFormObj.Product_L1__c != null && (csFormObj.Product_L1__c.containsIgnoreCase('elite') || csFormObj.Product_L1__c.containsIgnoreCase('app')))
            dispositionQuery += 'ELITE__c';
        else
            dispositionQuery += 'GAME__c';
        dispositionQuery += ' = TRUE ORDER BY L3__c';
        for(Disposition__c l3 : database.query(dispositionQuery )) l3Options.add(new selectOption(l3.L3__c, l3.L3__c));
        
        l3Options.add(new selectOption('Needs L3','Needs L3'));
        ppReq = new set<String>{'Toy','Portal','Disc','Peripheral','Special Edition 3rd Party'}.contains(csFormObj.Issue_Sub_Type__c);
        return null;
    }
    
    public PageReference saveRecord() {
        if(csFormObj.Product_Effected__c == null){
            for(Public_Product__c prod : [SELECT Id FROM Public_Product__c WHERE Medallia_Product_1__c  = :csFormObj.Platform__c AND Medallia_Product_2__c = :csFormObj.Product_L1__c LIMIT 1]){
                csFormObj.Product_Effected__c = prod.Id;
            }
        }
        
        if(ppReq){
            if(csFormObj.Product_Effected__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Must specify Product Affected'));
                return null;
            }
        }

        upsert csFormObj;
        PageReference pgref = new PageReference('/'+csFormObj.id);
        pgref.setRedirect(true);
        return pgref;
    }

}