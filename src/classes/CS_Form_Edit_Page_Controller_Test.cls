@isTest
private class CS_Form_Edit_Page_Controller_Test{
    CS_Form__c csFormObj;
    static testMethod void csMethod(){
  
    PageReference pageRef = Page.CS_Form_Edit_Page;
    Test.setCurrentPage(pageRef);
       
    selectOption[] l3Options = new selectOption[]{new selectOption('','Emblem/Title')};
    
        Parent_CS_Form__c parentCSform = new Parent_CS_Form__c();
        parentCSform.Name__c = 'Test Parent Form';
        insert parentCSform;    
        
        CS_Form__c c = new CS_Form__c();
        c.Name__c = 'Test Form';
        C.L3__c = 'Emblem/Title';
        c.Parent_CS_Form__c = parentCSform.id;
        insert c;
                   
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        
        CS_Form_Edit_Page_Controller Con = new CS_Form_Edit_Page_Controller(sc);
                  
            con.getL3s();
            con.setupL3();
            con.getQueues();
            con.saveRecord();
        
    }
 }