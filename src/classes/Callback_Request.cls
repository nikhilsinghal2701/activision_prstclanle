public class Callback_Request{

    public dateTime t3FirstCallAt {get; set;}
    public dateTime t3LastCallAt {get; set;}
    public date callbackDay {get; set;}
    public dateTime selectedtime {get;set;}
    public string pChosenTimeZone = 'America/Los_Angeles';
    public string userLanguage {get;set;}
    
    set<DateTime> alreadyBookedTimes = new set<DateTime>();
    public Callback_Request(ATVICustomerPortalController acpc){
        this(); //call the original constructor, 
    }
    public Callback_Request(){
        try{
            callbackDay = system.today();
            DateTime testDay = DateTime.newInstance(callbackDay, Time.newInstance(1,1,1,1));
            set<String> excludedDays = new set<String>{'Sun','Sat'};
            if(excludedDays.contains(testDay.format('E'))){
                do{
                    callbackDay = callbackDay.addDays(1);
                    testDay = DateTime.newInstance(callbackDay, Time.newInstance(1,1,1,1));
                }while(excludedDays.contains(testDay.format('E')));
            }
            getCallbackSettingForThisDay(callbackDay);

            Date nextWeek = callbackDay.addDays(7);
            
            for(CallbacK_Request__c req : [SELECT Requested_Callback_Time__c FROM Callback_Request__c 
                        WHERE  Requested_Callback_Time__c >= :callbackDay AND 
                               Requested_Callback_Time__c <= :nextWeek])
            {
                alreadyBookedTimes.add(req.Requested_Callback_Time__c);
            } //added limit to ensure max around 120 at launch
           
            //For Contact Us Flow Stuff
            userLanguage = 'en_US';
            //Get User Language:
            Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
            if(tempCookie != null){
                userLanguage = tempCookie.getValue();            
            }
            
            if(ApexPages.CurrentPage() != null){
                if(ApexPages.CurrentPage().getParameters().containsKey('z')){
                    String urlTimeZone = ApexPages.CurrentPage().getParameters().get('z');
                    //(GMT+13:00) Phoenix Inseln Zeit (Pacific/Enderbury)
                    //split 1 results in [0] == "(GMT+13:00) Phoenix Inseln Zeit" [1] == Pacific/Enderbury)
                    //split 2 results in [0] == Pacific/Enderbury
                    pChosenTimeZone = urlTimeZone.split(' \\(')[1].split('\\)')[0];
                }
                
            }
        }catch(Exception e){
            system.debug('ERROR == '+e);
        }
    }

    set<String> excludedDays = new set<String>{'Sun','Sat'};
    Callback_Setting__c currentCallbackSettings;
    public void getCallbackSettingForThisDay(Date thisDay){
        boolean callbackSettingFound = false;
        for(Callback_Setting__c setting : [SELECT Start_of_Day__c, Before_call_padding__c, Minutes_per_call__c,
                                               After_call_padding__c, End_of_Day__c FROM Callback_Setting__c WHERE 
                                               Activation_Date__c <= :thisDay ORDER BY Activation_Date__c DESC LIMIT 1])
        {
            currentCallbackSettings = setting;
            callbackSettingFound = true;
        }
        if(! callbackSettingFound){
            currentCallbackSettings = new Callback_Setting__c(
                Start_of_Day__c = DateTime.newInstance(thisDay, Time.newInstance(10, 0, 0, 0)),
                Before_call_padding__c = 5,
                Minutes_per_Call__c = 20,
                After_call_padding__c = 5,
                End_of_Day__c = DateTime.newInstance(thisDay, Time.newInstance(18, 0, 0, 0))
            );
        }
        t3FirstCallAt = DateTime.newInstance(thisDay, currentCallbackSettings.Start_of_Day__c.time()); //DateTime.newInstance(callbackDay, Time.newInstance(10, 0, 0, 0));
        t3LastCallAt = DateTime.newInstance(thisDay, currentCallbackSettings.End_of_Day__c.time()); //DateTime.newInstance(callbackDay, Time.newInstance(18, 0, 0, 0));
    }
    
    public string getChosenTimeZone(){
        return pChosenTimeZone;
    }
    
    public List<CallBackSlot[]> getNext4Days(){
        List<CallBackSlot[]> listOfCallBackDaySlots = new List<CallBackSlot[]>();
        listOfCallBackDaySlots.add(getDay());
        nextDay();
        listOfCallBackDaySlots.add(getDay());
        nextDay();
        listOfCallBackDaySlots.add(getDay());
        nextDay();
        listOfCallBackDaySlots.add(getDay());
        return listOfCallBackDaySlots;
    }

    public void nextDay(){
        Integer lDaysToGoForward = 0;
        DateTime dayToCheck;
        do{
            lDaysToGoForward++;
            dayToCheck = DateTime.newInstance(callbackDay.addDays(lDaysToGoForward), Time.newInstance(1, 0, 0, 0));
        }while(excludedDays.contains(dayToCheck.format('E')));
        
        callbackDay = callbackDay.addDays(lDaysToGoForward);
        getCallbackSettingForThisDay(callbackDay);
    }

    public CallBackSlot[] getDay(){
        CallbackSlot[] slots = new CallbackSlot[]{};
        getCallbackSettingForThisDay(t3FirstCallAt.date());
        DateTime loopTime = t3FirstCallAt;
        CallbackSlot justForDateHeader = new CallbackSlot(loopTime, pChosenTimeZone);
        slots.add(justForDateHeader); //customer cannot select this one, it's just for displaying the day being selected
        do{
            if(slots.size() > 1){ //only add padding for beginning of the calls after the first slot
                loopTime = loopTime.addMinutes(Integer.valueOf(currentCallbackSettings.Before_call_padding__c));
            }
            if(loopTime > system.now() && ! alreadyBookedTimes.contains(loopTime)){
                slots.add(new CallbackSlot(loopTime, pChosenTimeZone));
            }
            loopTime = loopTime.addMinutes(Integer.valueOf(currentCallbackSettings.Minutes_per_Call__c)); //add the time for the call
            loopTime = loopTime.addMinutes(Integer.valueOf(currentCallbackSettings.After_call_padding__c)); //add the time for after the call
        }while(loopTime < t3LastCallAt);
        system.debug('Callback Slots are '+slots);
        return slots;
    }

    public class callbackSlot{
        public callbackSlot(DateTime pCallbackTime, String cChosenTimeZone){
            this.callbackTime = pCallbackTime;
            this.csnTimeZone = cChosenTimeZone;
        }
        public Callback_Request__c[] requests {get;set;}
        public string csnTimeZone {get; set;}
        public string csnLocale {get; set;}
        public string getTimeInLocalZone(){
            return callbackTime.format(
                'h:mm aaa', //Fri, 4 Jul 12:08 //note, this will not translate Fri to Freitag 
                //'EEE, d MMM hh:mm aaa', //Fri, 4 Jul 12:08 //note, this will not translate Fri to Freitag 
                csnTimeZone);
        }
        public string getEpoch(){
            return String.valueOf(callbackTime.getTime());
        }
        public datetime callbackTime {get; set;}
    }
}