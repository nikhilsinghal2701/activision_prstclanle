@isTest
private class Callback_Request_Test {
    
    @isTest static void test_method_one() {
        Contact testcontact = UnitTestHelper.generateTestContact();
        insert testcontact;

        User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
        insert testportaluser;

        Callback_Request__c cr1 = new Callback_Request__c(Contact__c = testcontact.Id, Phone_Number__c = '3101234567', Requested_Callback_Time__c = system.now().addDays(1));
        insert cr1;

        Callback_Setting__c cs1 = new Callback_Setting__c(Activation_Date__c = system.today(), 
                                                          Start_of_Day__c = DateTime.newInstance(Date.today(), 
                                                          Time.newInstance(6, 0, 0, 0)), 
                                                          End_of_Day__c = DateTime.newInstance(Date.today(), 
                                                          Time.newInstance(15, 0, 0, 0)), 
                                                          Minutes_per_Call__c = 20, 
                                                          Before_Call_Padding__c = 0, 
                                                          After_Call_Padding__c = 0);
        insert cs1;

        PageReference currentpage = Page.Callback_Request;
        currentpage.getParameters().put('z','(GMT-07:00) Pacific Daylight Time (America/Tijuana)');
        Cookie langcookie = new Cookie('uLang','en_US',null,-1,false);
        currentpage.setCookies(new Cookie[]{langcookie});
        Test.setCurrentPage(currentpage);

        ATVICustomerPortalController acpc = new ATVICustomerPortalController();

        Callback_Request cr = new Callback_Request(acpc);

        Integer hour = System.now().hour();
        Integer minute = System.now().minute();

       /* System.runAs(testportaluser){
            system.assertEquals(cr.getChosenTimeZone(),'America/Tijuana');
            //system.debug('Slots = '+cr.getNext4Days());
            List<Callback_Request.CallBackSlot[]> slots = cr.getNext4Days();
            system.assertEquals(slots.size(),4);
            integer loopcount = 0;
            for(Callback_Request.CallBackSlot[] slot : slots){
                if(loopcount > 0 || hour < 6){
                    System.assertEquals(slot.size(),28);
                } else if(hour >= 15){
                    System.assertEquals(slot.size(),1);
                } else {
                    Integer t = (hour - 6) * 3;
                    Integer m = 0;
                    If(minute <= 20){
                        m++;
                    } else if(minute <=40){
                        m = m + 2;
                    } else { m = m +3; }
                    
                    System.assertEquals(slot.size(),28 - (t + m));


                }
                loopcount++;
                 
                for(Callback_Request.CallBackSlot s: slot){
                    System.debug('Slot Time is '+s.getTimeInLocalZone());
                    System.debug('Get Epoch is '+s.getEpoch());
                }
            }
            


        } */


    }
    
    @isTest static void test_method_two() {
        Contact testcontact = UnitTestHelper.generateTestContact();
        insert testcontact;

        User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
        insert testportaluser;

        Callback_Request__c cr1 = new Callback_Request__c(Contact__c = testcontact.Id, Phone_Number__c = '3101234567', Requested_Callback_Time__c = system.now().addDays(1));
        insert cr1;

        
        PageReference currentpage = Page.Callback_Request;
        currentpage.getParameters().put('z','(GMT-07:00) Pacific Daylight Time (America/Tijuana)');
        Cookie langcookie = new Cookie('uLang','en_US',null,-1,false);
        currentpage.setCookies(new Cookie[]{langcookie});
        Test.setCurrentPage(currentpage);

        ATVICustomerPortalController acpc = new ATVICustomerPortalController();

        Callback_Request cr = new Callback_Request(acpc);

            

    }
    
}