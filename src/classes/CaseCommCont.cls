public class CaseCommCont {
    Public List<CaseHistory> HistList{get;set;}
    transient Public List<CaseArticle> CaseArticleList{get;set;}
    transient Public List<KnowledgeArticleVersion> KAVList{get;set;} 
    transient Public List<Case_Comment__c> CommList{get;set;}
    Public Id caseId{get;set;}
    transient Public SET<ID> CAIdSet{get;set;}
    public String  LinkObj{get;set;}
    public List<wrapperClass> wrap{get;set;}
    transient List<CaseHistory> HistListDummy{get;set;}
    public boolean Bool{get;set;}
    transient public Map<Id,List<KnowledgeArticleVersion>> ArticleMap{get;set;}
    
    transient public list<Case_Comment__c> getrelatedcomments { get; set; }
    transient public Set<ID> IdSet { get; set; }
    transient public set<id> s;
    transient public list<casecomment> casecommentlist {get;set;}
    public Id ptid{get;set;}
    public CaseComment sts{get;set;}
    public String  BaseLink{get;set;}
    public String  LinkObj2{get;set;}
    public List<CommentClass> lstWarp{get;set;}
    public CommentClass Redirect{get;set;}  
    public boolean Checkprof{get;set;}
    public CommentClass CaseC{get;set;}
    
    public boolean SocialUser{get;set;}
    public List<ArticleClass> ArticleWrapper{get;set;}
    public Class ArticleClass{
        public KnowledgeArticleVersion KAV{get;set;}
        public CaseArticle CArt{get;set;}
        public ArticleClass(KnowledgeArticleVersion KA,CaseArticle CAOb){
            KAV = KA;
            CArt = CAOb;
        }
        public Pagereference Detach(){
            CaseArticle CAObj = new CaseArticle(Id=CArt.Id,CaseId=CArt.CaseId);
            Case Cs = new Case(id=CAObj.CaseId);
            Pagereference p = new pagereference('/'+CAObj.CaseId);
            system.debug('++++CASE ARTICLE++++'+Cs+'--------'+CAObj);
            delete CAObj;
            
            p.Setredirect(true);
            return p;

        }
    }
    public Class CommentClass{
        Public CaseComment CsCom {get;set;}
        Public String link {get;set;}
        public boolean bool{get;set;}
        public CommentClass(String l,CaseComment cc){
            Link = l;
            CsCom = cc;
            
        }
        public Pagereference MakePublic(){
            CaseComment IdOb = CsCom;
            IdOb.Ispublished = true;
            update CsCom;
            return null;
        }
        
        public Pagereference MakePrivate(){
            CaseComment IdOb = CsCom;
            IdOb.Ispublished = false;
            update CsCom;
            return null;
        }
        public pagereference Redirect1(){
            Pagereference p = new pagereference(Link);
            p.Setredirect(false);
            return p;
        }
        public Pagereference Del(){
            CaseComment IdOb = CsCom;
            bool = true;
            delete CsCom;
            Pagereference p = new pagereference('/'+IdOb.parentId);
            p.Setredirect(true);
            return p;

        }
        
    }
    
    public void retrieveCaseComment(){
        lstWarp = new List<CommentClass>();
        CAIdSet = new SET<ID>();
        ArticleMap = new Map<Id,List<KnowledgeArticleVersion>>();
        ArticleWrapper = new LIST<ArticleClass>();
        CaseArticleList = new LIST<CaseArticle>();
        KAVList = new LIST<KnowledgeArticleVersion>();
        getrelatedcomments  = new LIST<Case_Comment__c>();
        casecommentlist = new LIST<CaseComment>();
        idSet = new SET<ID>();
        /******************************/
        wrap = new List<wrapperclass>();
        HistList = new List<CaseHistory>();
        HistListDummy = new List<CaseHistory>();
        /******************************/
        Profile prof = [select id from Profile where name =:'CS - User Manager'];
        Profile prof1 ;
        Profile prof5 ;
        //Profile prof2 = [select id from Profile where name =:'System Administrator - Defer Sharing'];
        //Profile prof3 = [select id from Profile where name =:'System Administrator - No Apex/VF'];
        Profile prof4 ;
            if(userinfo.getprofileId() == prof.id )
            Checkprof = true;
            Else
            { 
                prof1 = [select id from Profile where name =:'System Administrator'];
                If(userinfo.getprofileId() == prof1.id )
                Checkprof = true;
                Else
                { 
                    prof4 = [select id from Profile where name =:'CS - User Manager'];
                    If(userinfo.getprofileId() == prof4.id )
                    Checkprof = true;
                    Else{
                        prof5 = [select id from Profile where name =:'CS - Social Agent']; 
                        If(userinfo.getprofileId() == prof5.id ){
                        Checkprof = true;
                        SocialUser = true;
                        }
                        Else
                        Checkprof = false;
                    }
                }
            }
         system.debug('++++++++++++++PROFILE++++++++++++++'+checkprof);
         ptid = ApexPages.currentPage().getParameters().get('id');
         getrelatedcomments = [select Case_Comment_Id__c from Case_Comment__c where Case__c=:ptid];
         for(Case_Comment__c id:getrelatedcomments ){
             IdSet.add(id.Case_Comment_Id__c);
         }
         system.debug('*******ID******'+ptid);
         casecommentlist =[Select id,CommentBody,IsPublished,ParentId,createdById,createdDate from CaseComment where ParentId=:ptid AND id NOT in:Idset ORDER By CreatedDate DESC ];
         for(CaseComment c: casecommentlist ){
             LinkObj2 = '/'+c.id+'/e?parent_id='+c.parentId+'&retURL=%2F'+c.parentId ;
             lstWarp.add(new CommentClass(LinkObj2, c));
         }
       CaseArticleList = [SELECT id,KnowledgeArticleId,CaseId from CaseArticle where caseID =:ptid];
       for(CaseArticle CA:CaseArticleList){
           CAIdSet.add(CA.KnowledgeArticleId);
       }
       KAVList =[SELECT id,Summary,title,createdById,Language,VersionNumber,createddate,lastmodifiedbyid,lastmodifieddate,KnowledgeArticleId from KnowledgeArticleVersion where PublishStatus =:'Online' AND Language=:'en_US' AND KnowledgeArticleId in :CAIdSet];
       /*for(KnowledgeArticleVersion KAVObj:KAVList){
            transient list<KnowledgeArticleVersion> KAList = new list<KnowledgeArticleVersion>();
            if(ArticleMap.containsKey(Obj.Associated_General_Support_Case__c))
                RMAList = RmaCaseMap.get(obj.Associated_General_Support_Case__c);
            RMAList.add(obj);
            RmaCaseMap.put(obj.Associated_General_Support_Case__c,RMAList);
       }*/
       for(KnowledgeArticleVersion k:KAVList){
               CaseArticle []C = [Select id,CaseId,knowledgeArticleId from CaseArticle where knowledgeArticleId=:k.knowledgeArticleId AND CaseId=:ptid];
               if(c.size() > 0)
               ArticleWrapper.add(new ArticleClass(k,c[0]));
       }
       system.debug('******KAV**********'+CaseArticleList+'++++++++++++++++++'+CAIdSet+'---------'+KAVList );
       retrieveCustomCaseComment(); 
    }
    
    public void retrieveCustomCaseComment(){
        Bool = CommentUtility.CommentAdded;
        CaseId=apexpages.currentpage().getparameters().get('Id');
        HistListDummy = [Select id,Field,NewValue,OldValue,CreatedBy.Name,createddate from CaseHistory where caseid=:CaseId ORDER BY createddate ASC];
        if(HistListDummy.size() >0){
            for(CaseHistory HLD:HistListDummy){
                if(HLD.Newvalue != null){
                    if(!(string.valueOf(HLD.Newvalue).startsWith('005')|| string.valueOf(HLD.Newvalue).startsWith('003')|| string.valueOf(HLD.Newvalue).startsWith('00G')|| string.valueOf(HLD.Newvalue).startsWith('a0U')|| string.valueOf(HLD.Newvalue).startsWith('500')))
                    HistList.add(HLD);
                }
                else
                    HistList.add(HLD);
            }
            
        }
        
        system.debug('+++++++++++++++++++'+HistList);
        CommList = [select id,comments__c,Make_Public__c,origin__c,createdby.name,Createddate,case__c from Case_comment__c where Case__c =:CaseId Order By createddate DESC];
        for(Case_Comment__c c: CommList){
             LinkObj = '/apex/case_comment_page?caseid='+c.Case__c+'&CcmId='+c.id ;
             Wrap.add(new wrapperclass(LinkObj, c));
        }
    
    }
    public pagereference refreshpage(){
    CommList = [select id,comments__c,Make_Public__c,origin__c,createdby.name,Createddate,lastModifiedBy.name,LastModifieddate,case__c from Case_comment__c where Case__c =:CaseId Order By createddate DESC];
        for(Case_Comment__c c: CommList){
             LinkObj = '/apex/case_comment_page2?caseid='+c.Case__c+'&CcmId='+c.id ;
             Wrap.add(new wrapperclass(LinkObj, c));
        }
    return null;
    }
    public Class wrapperClass{
        Public Case_Comment__c CsCom {get;set;}
        Public String link {get;set;}
        public boolean bool{get;set;}
        public wrapperClass(String l,Case_Comment__c cc){
            Link = l;
            CsCom = cc;
            //Redirect1();
            
        }
         public Pagereference MakePublic(){
            Case_Comment__c IdOb = CsCom;
            IdOb.make_Public__c= true;
            update CsCom;
            return null;
        }
        
        public Pagereference MakePrivate(){
            Case_Comment__c IdOb = CsCom;
            IdOb.make_Public__c= false;
            update CsCom;
            return null;
        }
        public pagereference Redirect1(){
            Pagereference p = new pagereference(Link);
            p.Setredirect(true);
            return p;
        }
        public pagereference Redirect2(){
            Pagereference p = new pagereference('/apex/Case_Comment_Page2?caseId={!CsCom.Case__c}');
            p.Setredirect(true);
            return p;
        }
        public Pagereference Del(){
            system.debug('+++++++++++++IN DEL----1--++++++++++++'+csCom);
            Case_Comment__c IdOb = CsCom;
            bool = true;
            system.debug('+++++++++++++IN DE----2---L++++++++++++'+csCom);
            delete CsCom;
            system.debug('+++++++++++++IN DEL---3---++++++++++++'+csCom);
            Pagereference p = new pagereference('/'+IdOb.Case__c);
            p.Setredirect(true);
            return p;
            
            //.turn null;
        }
        
    }
    
    public Boolean bounce{get;set;}
    public Case theCase{get;set;}
    
    public CaseCommCont(ApexPages.StandardController controller) {
        Bool = CommentUtility.CommentAdded;
        /*********************************/
        lstWarp = new List<CommentClass>();
        getrelatedcomments  = new LIST<Case_Comment__c>();
        casecommentlist = new LIST<CaseComment>();
        idSet = new SET<ID>();
        /******************************/
        wrap = new List<wrapperclass>();
        HistList = new List<CaseHistory>();
        HistListDummy = new List<CaseHistory>();
        retrieveCaseComment();
        
        theCase = (case)controller.getRecord();
        if(theCase.recordtypeid == [select id from recordtype where name='Social Media' limit 1].id){
            bounce = true;
        }
        else{
            bounce = false;
            }
    }
    
    public pageReference SocialBounce(){
        if(bounce){
            return new PageReference('/'+thecase.id+'?nooverride=1');
        }
        return null;
    }
}