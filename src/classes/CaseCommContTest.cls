@isTest
public class CaseCommContTest{
    public static testMethod void casecommethod(){  
        try{
            PageReference pageRef = Page.CasePageV3;
            Test.setCurrentPage(pageRef);
            Case cc = new Case(Comment_not_Required__c = true);
            contact con = new contact(lastname = 'test');
            insert con;
            cc.contactid = con.id;
            cc.Type      = 'Registration';
            cc.Sub_Type__c = 'Wrong Email';
            cc.status = 'Open';
            cc.Origin = 'web';
            Public_Product__c pp = new  Public_Product__c(name = 'testgame');
            insert pp;
            cc.Product_Effected__c= pp.id;    
            cc.Priority = 'Medium';
            cc.subject = 'Test';
            cc.Description = 'Troubleshooting';
            insert cc;
        
            ApexPages.StandardController sc = new ApexPages.standardController(cc);
            CaseCommCont controller = new CaseCommCont(sc);
        }catch(Exception e){
        }
    }
    public static testMethod void test1(){
        try{
            CaseCommCont.ArticleClass innerCls = new CaseCommCont.ArticleClass(
                new KnowledgeArticleVersion(), new CaseArticle()
            );
            innerCls.Detach();
        }catch(exception e){
        }
        try{
            CaseCommCont.CommentClass innerCls2 = new CaseCommCont.CommentClass('i',new CaseComment());
        }catch(Exception e){
        }
    }
}