public with sharing class CaseCommentController {
    Public List<Case_Comment__c> caseCommentList{get;set;} //= new List<Case_Comment__c>();
    Public List<Case_Comment__c> caseCommList{get;set;}
    public string CaseId{get;set;}
    public string CcmId{get;set;}
    Public Case CaseVar{get;set;}
    Public Case CaseOb{get;set;} 
    public Case_comment__c caseComment{get;set;}
    public boolean PublicComm{get;set;}
    public String Comm{get;set;}
    public string Origin{get;set;}
    
     //------------- Article Search : Start -----------------------------
    public List<ArticleClass> ArticleWrapper{get;set;}
    transient Public SET<ID> CAIdSet{get;set;}
    public Id ptid{get;set;}
    transient Public List<KnowledgeArticleVersion> KAVList{get;set;} 
    transient Public List<CaseArticle> CaseArticleList{get;set;}
    public Class ArticleClass{
        public KnowledgeArticleVersion KAV{get;set;}
        public CaseArticle CArt{get;set;}
        public ArticleClass(KnowledgeArticleVersion KA,CaseArticle CAOb){
            KAV = KA;
            CArt = CAOb;
        }
        public Pagereference Detach(){
            CaseArticle CAObj = new CaseArticle(Id=CArt.Id,CaseId=CArt.CaseId);
            Case Cs = new Case(id=CAObj.CaseId);
            Pagereference p = new pagereference('/apex/Case_Comment_Page?caseId='+CAObj.CaseId);
            system.debug('++++CASE ARTICLE++++'+Cs+'--------'+CAObj);
            delete CAObj;
            
            p.Setredirect(false);
            return p;
        }
    }
    public void retrieveKnowledgeArticles(){
        
        CAIdSet = new SET<ID>();
        ArticleWrapper = new LIST<ArticleClass>();
        CaseArticleList = new LIST<CaseArticle>();
        KAVList = new LIST<KnowledgeArticleVersion>();
            ptid = ApexPages.currentPage().getParameters().get('caseId');
       CaseArticleList = [SELECT id,KnowledgeArticleId,CaseId from CaseArticle where caseID =:ptid];
       for(CaseArticle CA:CaseArticleList){
           CAIdSet.add(CA.KnowledgeArticleId);
       }
       KAVList =[SELECT id,Summary,title,createdById,Language,VersionNumber,createddate,lastmodifiedbyid,lastmodifieddate,KnowledgeArticleId from KnowledgeArticleVersion where PublishStatus =:'Online' AND Language=:'en_US' AND KnowledgeArticleId in :CAIdSet];

       for(KnowledgeArticleVersion k:KAVList){
               CaseArticle []C = [Select id,CaseId,knowledgeArticleId from CaseArticle where knowledgeArticleId=:k.knowledgeArticleId AND CaseId=:ptid];
               if(c.size() > 0)
               ArticleWrapper.add(new ArticleClass(k,c[0]));
       }
    }
    //------------- Article Search : End -------------------------------
    //ApexPages.StandardController controller;
    public CaseCommentController(ApexPages.StandardController controller) {
        system.debug('++++----1----++++++'+CaseId);
        CaseId= ApexPages.currentPage().getParameters().get('Caseid') ;
        CcmId= ApexPages.currentPage().getParameters().get('Ccmid') ;
        if(ccmId != null){
            caseCommList = new LIST<Case_Comment__c>();
            caseCommList = [Select id,Origin__c,comments__c,case__c from Case_Comment__c where id=:CcmId];
            caseComment = new Case_comment__c(id=CcmId,comments__c=caseCommList[0].comments__c,Origin__c=caseCommList[0].Origin__c ,case__c = caseCommList[0].case__c );
        }
        Else{
            system.debug('++++++++++'+CaseId);
            CaseComment = new Case_comment__c();
            system.debug('++++LANGUAGE++++++'+CaseId);
            CaseOb =[Select W2C_Origin_Language__c from case where id=:CaseId];
            CaseComment.Case__c = CaseId;
            if(caseOb.W2C_Origin_Language__c != null){
            if(caseOb.W2C_Origin_Language__c.contains('en'))
            CaseComment.Comments__c = 'Issue Summary/Symptoms :'+'\n'+'\n'+'Troubleshooting attempted before contact:'+'\n'+'\n'+'Troubleshooting attempted during contact:'+'\n'+'\n'+'End Result:';
            Else if(caseOb.W2C_Origin_Language__c.contains('es'))
            CaseComment.Comments__c = 'Resumen de incidencias/indicios :'+'\n'+'\n'+'Búsqueda de errores realizada antes del contacto :'+'\n'+'\n'+'Búsqueda de errores realizada durante el contacto :'+'\n'+'\n'+'Resultado final :';
            Else if(caseOb.W2C_Origin_Language__c.contains('nl'))
            CaseComment.Comments__c = 'Probleemoverzicht/Symptomen :'+'\n'+'\n'+'Probleem proberen op te lossen voordat contact werd opgenomen :'+'\n'+'\n'+'Probleem proberen op te lossen tijdens het contact :'+'\n'+'\n'+'Eindresultaat :';
            Else if(caseOb.W2C_Origin_Language__c.contains('pt'))
            CaseComment.Comments__c = 'Sumário dos Problemas/Sintomas :'+'\n'+'\n'+'Resolução de problemas tentada antes do contacto :'+'\n'+'\n'+'Resolução de problemas tentada durante o contacto :'+'\n'+'\n'+'Resultado Final :';
            Else if(caseOb.W2C_Origin_Language__c.contains('sv'))
            CaseComment.Comments__c = 'Sammanfattning av ärendet/symtom :'+'\n'+'\n'+'Försök till felsökning innan kontakt :'+'\n'+'\n'+'Försök till felsökning under pågående kontakt :'+'\n'+'\n'+'Slutresultat :';
            Else if(caseOb.W2C_Origin_Language__c.contains('it'))
            CaseComment.Comments__c = 'Riepilogo/Sintomi del problema :'+'\n'+'\n'+'Tentativo di risolvere il problema prima del contatto :'+'\n'+'\n'+'Tentativo di risolvere il problema durante il contatto :'+'\n'+'\n'+'Risultato finale :';
            Else if(caseOb.W2C_Origin_Language__c.contains('de'))
            CaseComment.Comments__c = 'Problem-Zusammenfassung/Symptome :'+'\n'+'\n'+'Problemlösung vor Kontaktaufnahme durchgeführt :'+'\n'+'\n'+'Problemlösung während Kontaktaufnahme durchgeführt :'+'\n'+'\n'+'Ergebnis :';
            Else if(caseOb.W2C_Origin_Language__c.contains('fr'))
            CaseComment.Comments__c = 'Résumé des problèmes/Symptômes :'+'\n'+'\n'+'Tentatives avant de prendre contact :'+'\n'+'\n'+'Tentatives pendant le contact :'+'\n'+'\n'+'Résultat :';
            Else
            CaseComment.Comments__c = 'Issue Summary/Symptoms :'+'\n'+'\n'+'Troubleshooting attempted before contact:'+'\n'+'\n'+'Troubleshooting attempted during contact:'+'\n'+'\n'+'End Result:';
            }
            else
            CaseComment.Comments__c = 'Issue Summary/Symptoms :'+'\n'+'\n'+'Troubleshooting attempted before contact:'+'\n'+'\n'+'Troubleshooting attempted during contact:'+'\n'+'\n'+'End Result:';
        }
    }
    public void CaseComment(){ 
        
        If(CaseId != null && CaseId !=''){
        CaseVar =[select id,subject,description, ownerid,status,escalated__c from Case where id=:CaseId];
        caseCommentList =[select id,Make_Public__c,Comments__c,Origin__c,CreatedById,CreatedDate from Case_Comment__c where Case__c =:CaseId order by createdDate DESC];
        retrieveKnowledgeArticles();
        }
    }
    public Pagereference Cancel(){
    pagereference p = new pagereference('/'+CaseId);
    p.setredirect(true);
    return p;
    }
    
    public Pagereference SaveCaseComment(){
        system.debug(CaseComment+'+++*'+PublicComm+'*+++*'+origin+'*++++++*'+Comm+'*++++++**++++'+CaseId);        
        try{
        If(ccmId != null)
        Update CaseComment ;
        Else
        Insert CaseComment ;
        
        update CaseVar;
        CommentUtility.CommentAdded =true;
        }
        catch(exception ex)
        {
            system.debug('Exception===='+ex);
        }
        pagereference p = new Pagereference('/apex/coment_saved?caseId='+CaseId+'&isdtp=vw');
        p.setRedirect(true);
        return p;
        //return null;
    } 

}