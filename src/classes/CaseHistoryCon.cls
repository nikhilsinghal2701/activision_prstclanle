public with sharing class CaseHistoryCon {
      
     public boolean fullComments { get; private set; } 
     public boolean hidePrivate  { get; private set; }

     public CaseHistoryCon() {
         fullComments           = false;
         hidePrivate            = false;
         truncatedCommentLength = 100;
     }
      
     /* Action method for toggling the fullComments property */
     public void toggleComments() { fullcomments = !fullcomments; }
  
     /* Action method for toggling the visibility control for private related objects.*/
     public void togglePrivate()  { hidePrivate  = !hidePrivate;  }   
      
     /* Action method for navigating the user back to the case page. */
     public PageReference backToCase() {
         return new ApexPages.StandardController(c).view();
     }
  
     /* Method for retrieving the case object and its related items. */
     public Case getcase() {
      
         if(cid == null) return new Case();
         return [SELECT casenumber, subject, contact.name, contact.email,
                        (SELECT CreatedBy.Name, CreatedDate, CommentBody,IsPublished          FROM CaseComments ORDER BY CreatedDate      DESC),
                        (SELECT Owner.Name, ActivityDateTime, Subject, IsVisibleInSelfService FROM Events       WHERE ActivityDateTime <= :System.Now()   ORDER BY ActivityDateTime DESC),
                        (SELECT Owner.Name, LastModifiedDate, Subject, IsVisibleInSelfService, Who.Name FROM Tasks        WHERE ActivityDate     <= :System.Today() 
                                                                                                                 AND IsClosed = true                     ORDER BY LastModifiedDate DESC)
                 FROM case
                 WHERE id = :cid];
     }
      

     public History[] getHistories() {
         History[] histories = new history[]{};
         for (CaseComment comment:c.casecomments) {
             if (!hidePrivate || comment.ispublished) {
                 addHistory(histories, new history(comment.createdDate, comment.ispublished, comment.createdby.name, 'Comment Added', '' , truncateValue(comment.commentbody), comment.id));
             }
         }
        /*
         for (ActivityHistory e:c.ActivityHistories) {
             if (!hidePrivate || e.isvisibleinselfservice) {
                 addHistory(histories, new history(e.activitydate,  e.isvisibleinselfservice, e.owner.name,e.whatid, '' , e.subject, e.id));
             }
         }
         */
         for (Event e:c.events) { 
            if (!hidePrivate || e.isvisibleinselfservice) {
                addHistory(histories, new history(e.activitydatetime,  e.isvisibleinselfservice, e.owner.name,'Event Completed', '' , e.subject, e.id)); 
            }
        }
                   
         for (Task t:c.tasks) {
             if (!hidePrivate || t.isvisibleinselfservice) {
                 addHistory(histories, new history(t.lastmodifieddate,  t.isvisibleinselfservice, t.owner.name,'Task Completed',  t.who.Name , t.subject, t.id));
             }
         }
         /* 
         for (CaseHistory ch:c.histories) {
             addHistory(histories, new history(ch.createdDate, true, ch.createdby.name, ch.field + ' Change', String.valueOf(ch.oldvalue), String.valueOf(ch.newvalue)));
         }
  
         for (Attachment a:c.attachments) {
             addHistory(histories, new history(a.createdDate,  true, a.createdby.name, 'Attachment Added', '' , a.name));
         }
         */ 
         return histories;
     }
      

     private void addHistory(History[] histories, History newHistory) {
         Integer position = histories.size();
         for (Integer i = 0; i < histories.size(); i++) {
             if (newHistory.historydt > histories[i].historydt) {
                 position = i;
                 break;
             }
         }
          
         if (position == histories.size()) {
             histories.add(newHistory);
         } else {
             histories.add(position, newHistory);
         }
     }
      

     private String truncateValue(String s) {
         if (!fullComments && s.length() > truncatedCommentLength) {
             s = s.substring(0,truncatedCommentLength) + '...';
         }
          
         return s;
     }
      

     private Id cid {
         get {
             if(ApexPages.currentPage().getparameters().get('cid') != null) {
                 cid = ApexPages.currentPage().getparameters().get('cid');
             }
             return cid;
         }
         set {
             if(value != null) cid = value;
         }
     }
      

     private Case c {
         get { return getCase(); }
         set;
     }
      
     private Integer truncatedCommentLength { get; set; }  
 }