@IsTest private class CaseHistoryConTests {

    public static testmethod void basicTest() {
    
        CaseHistoryConTests testclass = new CaseHistoryConTests();
        Case c = testclass.setupTestCase();
        CaseHistoryCon controller = testclass.setupController(c);
           
        Test.startTest();
        List<History> histories = controller.getHistories();   
        Test.stopTest();
    }

    public static testmethod void actionsTest() {
        CaseHistoryConTests testclass = new CaseHistoryConTests();
        Case c = testclass.setupTestCase();
        PageReference p = Page.caseHistory;
        p.getParameters().put('cid',c.id);
        Test.setCurrentPage(p);
        CaseHistoryCon controller = new CaseHistoryCon();
        controller.toggleComments();       
        controller.togglePrivate();

        Test.startTest();
        List<History> histories = controller.getHistories();         
        Test.stopTest();

        PageReference result = controller.backToCase();   
    }

    public static testmethod void testHistoryFormatting() {

        Datetime dt = System.now();
        id newid;
        History h = new History(dt, false, 'actor','history type','from','to', newid );
    }
    private Case setupTestCase() {

        String status = [Select masterlabel from casestatus where isclosed = false limit 1].masterlabel;
        Case testCase = new Case(Status = status, Comments__c = 'test Comment');
        Database.insert(testCase);

        List<CaseComment> casecomments = new List<CaseComment>();
        caseComments.add(new CaseComment(ParentId = testCase.id, CommentBody = 'Comment Body', ispublished = false));
        caseComments.add(new CaseComment(ParentId = testCase.id, ispublished = true,
                                         CommentBody = 'Comment Body that is very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very, very long to cause the truncation to occur.'));
                
        Database.insert(casecomments);

        String closedStatus = [select masterlabel from TaskStatus where isclosed = true][0].masterlabel;

        Task t = new Task(whatId = testCase.id, Status = closedStatus, ActivityDate = System.Today().addDays(-1));
        Database.insert(t);

        Event e = new Event(whatId = testCase.id, DurationInMinutes = 60, ActivityDateTime = System.now().addDays(-1));
        Database.insert(e);

        Attachment a = new Attachment(ParentId = testCase.id, Name = 'Attachment', Body = Blob.valueOf('Attachment Body'));
        Database.insert(a);
            
        return testCase;
    }

    private CaseHistoryCon setupController(Case c) {

        CaseHistoryCon controller = new CaseHistoryCon();     
        PageReference p = Page.caseHistory;
        p.getParameters().put('cid',c.id);
        Test.setCurrentPage(p);      
        return controller;
    }
}