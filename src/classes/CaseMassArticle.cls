public with sharing class CaseMassArticle{
    Case_Mass_Article__c storage = new Case_Mass_Article__c(Case_Ids__c='');
    Case_Mass_Article__c recentEntry = new Case_Mass_Article__c();
    ApexPages.Standardsetcontroller controller;
    
    public string searchText {get;set;}
    public string articleID {get;set;}
    public case[] cases {get;set;}
    //public integer selectedCount {set;}
    public string selectedCount {get;set;}
    public boolean showUndo {get;set;}
    public boolean showSummary {get;set;}
    public boolean showRemove {get;set;}
    public boolean noneSelected {get;set;}
    public string articlename {get;set;}
    
    public CaseMassArticle(){
        showUndo = false;
        showSummary = false;
        showRemove = false;
        selectedCount = ApexPages.currentPage().getParameters().get('selectCount');
        if (integer.valueof(selectedCount)<1 || selectedCount==Null || selectedCount==''){
            noneSelected = true;
        }
        else{
            recentEntry = [select id,case_ids__c from case_mass_article__c where ownerid=:UserInfo.getUserId() order by createddate desc limit 1];
            system.debug(recentEntry.case_ids__c);
        }
    }
    
    public CaseMassArticle(ApexPages.StandardSetController cont){
        controller = cont;
        //recentEntry = [select id,case_ids__c from case_mass_article__c where ownerid=:UserInfo.getUserId() order by createddate desc limit 1];
        system.debug(recentEntry.case_ids__c);
    }

    public pageReference goToMassArticlePage(){
        storage.Case_Ids__c='';
        Integer caseAttachLimit = getCaseAttachLimit();
  
        for(Integer i=0; i < caseAttachLimit && i < controller.getSelected().size();i++){
            storage.Case_Ids__c += controller.getSelected()[i].ID+';';
            system.debug(controller.getSelected()[i].id);
            system.debug(caseAttachLimit);
        }
        upsert storage;
        
        //selectedCount = controller.getSelected().size();

        pagereference temp = page.caseMassAttach;
        temp.getParameters().put('selectCount', string.valueof(controller.getSelected().size()));//pass value
        return temp;
    }
    
    public integer getCaseAttachLimit(){
        Integer result = 1;
        if(ApexCodeSetting__c.getInstance('CaseMassCommentLimit') != null){
            result = Integer.valueOf(ApexCodeSetting__c.getInstance('CaseMassCommentLimit').Number__c);
            if(result > 1724) result = 1724; //long text fields can only hold 1724 delimited ids
        }
        return result;
    }
    
    public pageReference submitSearchText(){
        system.debug('articleid:'+articleID);
        return Null;
    }
    public pageReference debug(){
    system.debug('articleid:'+articleID);
        return Null;
    }
    
    public pageReference attachArticles(){
        system.debug('recentEntry:' + recentEntry.Case_Ids__c.split(';'));
        cases = [SELECT Id, Status, RecordTypeId FROM Case WHERE Id IN :recentEntry.Case_Ids__c.split(';')];
        
        system.debug('articleid' + articleid);
        for (case iter:cases){
            system.debug('iter:' + iter.id);
            casearticle temp = new casearticle();
            temp.caseid = iter.id;
            temp.knowledgearticleid = articleid;
            insert temp;
        }
        showUndo = true;
        showSummary = true;
        showRemove = false;
        return Null;
    }
    
    public void undo(){
        casearticle newest = new casearticle();
        list<casearticle> removecases = new list<casearticle>();
        
        newest = [select id,createddate from casearticle where createdbyid=:UserInfo.getUserId() order by createddate desc limit 1];
        removecases = [select id from casearticle where createddate=:newest.createddate];
        delete removecases;
        showRemove = true;
        showUndo = false;
        showSummary = false;

    }
    
    public pageReference cancel(){
        return new pageReference('/500/o');
    }
}