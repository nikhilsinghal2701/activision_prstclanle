public with sharing class CaseMassComment {
    ApexPages.Standardsetcontroller controller;
    boolean onVFpage = false;
    set<String> closedStatuses = new set<String>{'Resolved','Unresolved','Closed'};
    Case_Mass_Comment__c storage = new Case_Mass_Comment__c(Case_Ids__c = '');

    public case[] cases {get; private set;}
    public CaseComment baseComment {get; set;}
    public string chosenStatus {get; set;}
    public boolean changeStatus{get; set;}
    public selectOption[] getAllPosslStatusOptions(){
        String[] unSortedOpts = new String[]{};
        for(Schema.PicklistEntry ple : Case.Status.getDescribe().getPicklistValues()){
            unSortedOpts.add(ple.getValue());
        }
        unSortedOpts.sort();
        selectOption[] opts = new selectOption[]{};
        for(String opt : unSortedOpts){
            opts.add(new selectOption(opt, opt));
        }
        return opts;
    }

    void init(){
        baseComment = new CaseComment(CommentBody = '');
        cases = new Case[]{};
        if(ApexPages.currentPage() != null && ApexPages.currentPage().getUrl() != null && ApexPages.currentPage().getURL().contains('apex/')){
            onVFpage = true;
        }
        for(Case_Mass_Comment__c preExistingStorage : [SELECT Id, Case_Ids__c FROM Case_Mass_Comment__c WHERE OwnerId = :UserInfo.getUserId()]){
            storage = preExistingStorage;
        }
    }
    
    //Used by the standard list view button
    public CaseMassComment(ApexPages.StandardSetController cont){
        init();
        controller = cont; 
    }
    public pageReference goToMassCommentPage(){
        storage.Case_Ids__c = '';
        Integer caseCommentLimit = getCaseCommentLimit();
        for(Integer i = 0; i < caseCommentLimit && i < controller.getSelected().size(); i++){
            storage.Case_Ids__c += controller.getSelected()[i].Id+';';
        }
        upsert storage;
        return Page.CaseMassComment;
    }
    
    //Used by the CaseMassComment.page
    public CaseMassComment(){
        init();
        if(storage.Case_Ids__c != null && storage.Case_Ids__c != ''){
            cases = [SELECT Id, Status, RecordTypeId FROM Case WHERE Id IN :storage.Case_Ids__c.split(';')];
        }
        if((cases == null || cases.size() == 0) && onVFpage){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.FATAL, 'No cases chosen.'));
        }
    }
    public pageReference addMassComment(){
        if(baseComment.CommentBody == ''){
            if(onVFpage){
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.FATAL, 'Comment Body is required'));
            }
            return null;
        }
        try{
            CaseComment[] commentsToInsert = new CaseComment[]{};
            Case[] casesToUpdate = new Case[]{};
            for(Case cas : cases){
                CaseComment clonedCC = baseComment.clone();
                clonedCC.ParentId = cas.Id;
                commentsToInsert.add(clonedCC);
                if(changeStatus){
                    if(chosenStatus == null || chosenStatus == ''){
                        if(onVFpage){
                            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.FATAL, 'A status other than \'--None--\' is required'));
                            return null;
                        }
                    }else{
                        cas.Status = chosenStatus;
                        if(closedStatuses.contains(chosenStatus)){
                            cas.Closed_By__c = UserInfo.getUserId();
                        }
                        casesToUpdate.add(cas);
                    }
                }
            }
            insert commentsToInsert;
            if(casesToUpdate.size() > 0) update casesToUpdate;
            storage.Case_Ids__c = '';
            update storage;
            return returnToTab();
        }catch(Exception e){
            if(onVFpage) ApexPages.addMessages(e);
            system.debug(e);
            return null;
        }
    }
    public pageReference doNothing(){return null;} //used for rerendering on the VF page
    public pageReference returnToTab(){ //upon successful completion of the add comment buttonm, or the cancel button
        return new pageReference('/500/o');
    }
    public integer getCaseCommentLimit(){
        Integer result = 1;
        if(ApexCodeSetting__c.getInstance('CaseMassCommentLimit') != null){
            result = Integer.valueOf(ApexCodeSetting__c.getInstance('CaseMassCommentLimit').Number__c);
            if(result > 1724) result = 1724; //long text fields can only hold 1724 delimited ids
        }
        return result;
    }

}