public class CaseTriggerHandler {

   


/*****************************************************************************************************************/
    public static void CaseCommentRequired(Case[] newCases, Map<ID, Case> newCaseMap, Map<ID, Case> oldCaseMap){

    map<String, Schema.RecordTypeInfo> cRecordTypes = Schema.SObjectType.Case.getRecordTypeInfosByName();
    Id genSupportId = cRecordTypes.get('General Support').getRecordTypeId();
    
    list<profile> profiles = [select id from profile where Name =:'ATVI OHV Cust Port User'];
    // list<RecordType> RecordTypes = [select id from RecordType where Name =: 'General Support' and sObjectType =: 'Case'];
    Set<id> CaseIdSet = new Set<id>();

    if(userinfo.getProfileId() == null 
        || profiles == null || profiles.size() <= 0 || userinfo.getProfileId() == profiles[0].id
        || genSupportId == null || checkAddError.addError == false
        )
        return;

   /* if(userinfo.getProfileId() == null 
        || profiles == null || profiles.size() <= 0 || userinfo.getProfileId() == profiles[0].id
        || RecordTypes == null || RecordTypes.size() <= 0
        || checkAddError.addError == false
        )
        return; */

    for (Case c: newCases) {
        if(Trigger.isInsert || c.OwnerId == oldCaseMap.get(c.Id).OwnerId)
            CaseIdSet.add(c.id);
    }

    List<CaseComment> CaseComments = new List<CaseComment>([select id, parentId from casecomment where parentid in:CaseIdSet]);
    Map<id,list<CaseComment>> CaseMap = new Map<id,list<CaseComment>>();
    for (CaseComment cc:CaseComments ){
        list<CaseComment> CommentList = new list<CaseComment>();
        if(CaseMap.containsKey(cc.parentId))
            CommentList = CaseMap.get(cc.parentId);
        CommentList.add(cc);
        CaseMap.put(cc.parentId,CommentList);
    }

    for (Id caseId : CaseIdSet) {
        Case cases = newCaseMap.get(caseId);
        
        List<CaseComment> ccList = CaseMap.get(cases.id);
        if(ccList == null || ccList.isEmpty()) {
            if (cases.RecordTypeId == genSupportId && cases.Comment_Not_Required__c == false )
                cases.Comments__c.addError('Please add a comment to be able to save the record');
        }
    }
    }  
/*****************************************************************************************************************/


public static void insertCustomComment(Case[] newCases){
    //if (commentCreated == true){
    //    return;
    //}
    //commentCreated = true;
    List<CaseComment> ccList = new List<CaseComment>();
    caseComment cc = new caseComment();
    List<Case> caseList = new List<Case>();
    for (Case Cases: newCases){
            if(cases.Comments__c != null && cases.Comments__c != ''){

                cc = new caseComment(ParentId=cases.id, CommentBody=cases.Comments__c  );
                
                ccList.add(cc);
                
                Case caseObj = new case (id = cases.id, Comments__c = '');
                caseList.add(caseObj);
                
                //break;
            }
    }
    insert ccList;
    //update caseList;

    }  
/*****************************************************************************************************************/
public static boolean updatedCase = false;

    public static void updateGeneralSupportCaseStatus(Case[] newCases){

    map<String, Schema.RecordTypeInfo> cRecordTypes = Schema.SObjectType.Case.getRecordTypeInfosByName();
    Id RMASupportId = cRecordTypes.get('RMA').getRecordTypeId();
    
    if(updatedCase == true)
       return;
    
    updatedCase = true;
    Set<Id> GSCaseIdSet = new Set<Id>();
    // list<RecordType> RcdTypes = [select id from RecordType where Name =: 'RMA' and sObjectType =: 'Case'];
    List<Case> CaseToUpdate = new List<Case>();
    
    List<Case> GSCaseList = new List<Case>();
    List<case> NewRMACaseList = new List<Case>();
    
    For(Case caseobj : newCases){
        If(caseobj.RecordTypeId == RMASupportId){

            If(caseobj.Associated_General_Support_Case__c != null ){
                GSCaseIdSet.add(caseobj.Associated_General_Support_Case__c) ;
                //GSCaseList.add(caseobj);
            }
        }
    }
    
    if(GSCaseIdSet == null || GSCaseIdSet.size() <=0)
        return;
        
    //if(GSCaseIdSet !=null && (!GSCaseIdSet.isEmpty())){
    GSCaseList = [Select id,status,RMA_Repaired_Unit_Delivered__c from case where id in:GSCaseIdSet];

    NewRMACaseList = [Select id,Associated_General_Support_Case__c from Case where Associated_General_Support_Case__c in: GSCaseIdSet and Return_Code__c != 'REPAIRED UNIT DELIVERED'];
    //}
    Map<id,list<Case>> RmaCaseMap = new Map<id,list<Case>>();
    for(case Obj:NewRMACaseList){
        list<Case> RMAList = new list<Case>();
        if(RmaCaseMap.containsKey(Obj.Associated_General_Support_Case__c))
            RMAList = RmaCaseMap.get(obj.Associated_General_Support_Case__c);
        
        RMAList.add(obj);
        RmaCaseMap.put(obj.Associated_General_Support_Case__c,RMAList);
    }
    
    For (Case GSCase: GSCaseList ){
        List<case> CaseUpdateList = new List<case>();
        CaseUpdateList = RmaCaseMap.get(GScase.id);
        
        If((CaseUpdateList == null || CaseUpdateList.size() == 0)
            && GScase.RMA_Repaired_Unit_Delivered__c != True ){
            GScase.RMA_Repaired_Unit_Delivered__c = True;
            CaseToUpdate.add(GSCase); 
        }
    }
    
    update CaseToUpdate;
    
} 
    
/*****************************************************************************************************************/


    public static void CaseOwnerCopy(Case[] newCases){
    map<String, Schema.RecordTypeInfo> cRecordTypes = Schema.SObjectType.Case.getRecordTypeInfosByName();
    Id genSupportId = cRecordTypes.get('General Support').getRecordTypeId();
    
    // list<RecordType> RcdTypeList = [select id from RecordType where Name =: 'General Support' and sObjectType =: 'Case'];
    For (Case CaseObj: newCases){
        string ownId = CaseObj.OwnerId;
        if(CaseObj.RecordTypeId == genSupportId ){
            if (ownId.substring(0,3) == '005'){
                CaseObj.Owner_Copy__c = CaseObj.OwnerId;
            }
            else{
                CaseObj.Owner_Copy__c = null;    
            }
        }   
    }
    } 
    
/*****************************************************************************************************************/    
 public static void CheckGScaseStatus(Case[] newCases, Map<id, case> caseMapNew){   

    Set<Id> NewCaseIdSet = new Set<Id>();
    /*no SOQL "fixed" version*/
    map<String, Schema.RecordTypeInfo> caseRecordTypes = Schema.SObjectType.Case.getRecordTypeInfosByName();
    Id RcdTypeList = caseRecordTypes.get('General Support').getRecordTypeId(); 
    Id RcdTypes = caseRecordTypes.get('RMA').getRecordTypeId(); 
    
    for(Case caseObj: newCases){
        If(CaseObj.RecordTypeId== RcdTypeList)
        NewCaseIdSet.add(caseObj.Id);
    }
    
    List<Case> NewRmaClosedCaseList = [select Id,Status,Associated_General_Support_Case__c from Case where RecordTypeId=:RcdTypes  and Associated_General_Support_Case__r.id in:NewCaseIdSet  and Return_Code__c != 'REPAIRED UNIT DELIVERED'];
    Map<id,list<Case>> CaseMap = new Map<id,list<Case>>();
    for (Case cc:NewRmaClosedCaseList ){
        list<Case> CaseList = new list<Case>();
        if(CaseMap.containsKey(cc.Associated_General_Support_Case__c))
            CaseList = CaseMap.get(cc.Associated_General_Support_Case__c);
        CaseList.add(cc);
        CaseMap.put(cc.Associated_General_Support_Case__c,CaseList);
    }
    
    For(Id caseObj: NewCaseIdSet){
    Case cases = caseMapNew.get(caseObj);
    List<Case> NewRmaCaseList = CaseMap.get(cases.id);
    If(NewRmaCaseList != null && (!NewRmaCaseList.isEmpty())&& NewRmaClosedCaseList != null && (!NewRmaClosedCaseList.isEmpty())){
        If(NewRmaClosedCaseList.size() != null ){
              //If(Cases.Status=='Resolved' || Cases.Status=='Unresolved'){
              If(Cases.isClosed == true){
                  cases.Status.addError('The Case cannot be Closed until all the RMAs under it have status REPAIRED UNIT DELIVERED');
        } 
      }  
    }
    }
}   
    
/*****************************************************************************************************************/      
       
public static void CommentReqdforParentGSCase(Case[] newCases){

    map<String, Schema.RecordTypeInfo> cRecordTypes = Schema.SObjectType.Case.getRecordTypeInfosByName();
    Id genSupportId = cRecordTypes.get('General Support').getRecordTypeId();
    Id RMASupportId = cRecordTypes.get('RMA').getRecordTypeId();



    Set<Id> GSCaseIdSet = new Set<Id>();
    Set<Id> NewGSIdSet = new Set<Id>();
   // list<RecordType> RcdTypes = [select id from RecordType where Name =: 'RMA' and sObjectType =: 'Case'];
   // list<RecordType> GSRcdTypes = [select id from RecordType where Name =: 'General Support' and sObjectType =: 'Case'];
    List<Case> RMACases = new List<Case>();

    
    For(Case caseobj:newCases){
        If(caseobj.RecordTypeId == RMASupportId){
            If(caseobj.Associated_General_Support_Case__c != null){
                GSCaseIdSet.add(caseobj.Associated_General_Support_Case__c) ;
            }         
        }
    }
    List<Case> GSCaseList = new List<Case>();
    List<CaseComment> CaseComments = new List<CaseComment>();
    If(GSCaseIdSet != null && (!GSCaseIdSet.isEmpty())){
    GSCaseList = [Select id from case where id in:GSCaseIdSet and RecordTypeId =:genSupportId];
    
    CaseComments = new List<CaseComment>([select id, parentId from casecomment where parentid in:GSCaseIdSet]);
    }
    
    If(GSCaseList != null && (!GSCaseList.isEmpty())){
        for(Case gsCase: GSCaseList ){
            NewGSIdSet.add(gsCase.id);  
        }
    }


    Map<id,list<CaseComment>> CaseMap = new Map<id,list<CaseComment>>();

    for (CaseComment cc:CaseComments ){
        list<CaseComment> CommentList = new list<CaseComment>();
        if(CaseMap.containsKey(cc.parentId))
            CommentList = CaseMap.get(cc.parentId);
        CommentList.add(cc);
        CaseMap.put(cc.parentId,CommentList);
    }
    
    For(Case c:newCases){
        If(c.RecordTypeId == RMASupportId && c.Associated_General_Support_Case__c != null){

            if (!NewGSIdSet.contains(c.Associated_General_Support_Case__c)){
                c.Associated_General_Support_Case__c.addError('Associated General Support Case field will have cases of General Support Type only');
            }

            Else{
                List<CaseComment> ccList = CaseMap.get(c.Associated_General_Support_Case__c);
                if(ccList == null || ccList.isEmpty()) {
                    //if (cases.RecordTypeId == RecordTypes[0].id && cases.Comment_Not_Required__c == false )
                        c.Associated_General_Support_Case__c.addError('Please add Case Comment to the Parent General Support Case before Submitting RMA');
                }
            }                    
        }
    }
  
   
}
}