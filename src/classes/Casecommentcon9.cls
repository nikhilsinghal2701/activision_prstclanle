public with sharing class Casecommentcon9 {
    transient public list<Case_Comment__c> getrelatedcomments { get; set; }
    transient public Set<ID> IdSet { get; set; }
    transient public  set<id> s;
    transient public list<casecomment> casecommentlist {get;set;}
    public Id ptid{get;set;}
    public CaseComment sts{get;set;}
    public String  BaseLink{get;set;}
    public String  LinkObj{get;set;}
    public List<CommentClass> lstWarp{get;set;}
    public CommentClass Redirect{get;set;}  
    public boolean Checkprof{get;set;}
    
    //public boolean checkprofile(){
    
    
    //return Checkprof;
    //}  
    public CommentClass CaseC{get;set;}
    public Class CommentClass{
        Public CaseComment CsCom {get;set;}
        Public String link {get;set;}
        public boolean bool{get;set;}
        public CommentClass(String l,CaseComment cc){
            Link = l;
            CsCom = cc;
            //Redirect1();
            
        }
        public Pagereference MakePublic(){
            CaseComment IdOb = CsCom;
            IdOb.Ispublished = true;
            update CsCom;
            return null;
        }
        
        public Pagereference MakePrivate(){
            CaseComment IdOb = CsCom;
            IdOb.Ispublished = false;
            update CsCom;
            return null;
        }
        public pagereference Redirect1(){
            Pagereference p = new pagereference(Link);
            p.Setredirect(false);
            return p;
        }
        public Pagereference Del(){
            system.debug('+++++++++++++IN DEL----1--++++++++++++'+csCom);
            CaseComment IdOb = CsCom;
            bool = true;
            system.debug('+++++++++++++IN DE----2---L++++++++++++'+csCom);
            delete CsCom;
            system.debug('+++++++++++++IN DEL---3---++++++++++++'+csCom);
            Pagereference p = new pagereference('/'+IdOb.parentId);
            system.debug('+++++++++++++IN DEL---2----++++++++++++'+p);
            p.Setredirect(true);
            return p;
            
            //.turn null;
        }
        
    }

   // public Casecommentcon9(){
        
    //}
    
    
        public Casecommentcon9(ApexPages.StandardController controller) {
        Profile prof = [select id from Profile where name =:'CS - User Manager'];
        Profile prof1 = [select id from Profile where name =:'System Administrator'];
        //Profile prof2 = [select id from Profile where name =:'System Administrator - Defer Sharing'];
        //Profile prof3 = [select id from Profile where name =:'System Administrator - No Apex/VF'];
        //Profile prof4 = [select id from Profile where name =:'CS - User Manager'];
            if(userinfo.getprofileId() == prof.id )
            Checkprof = true;
            Else 
            If(userinfo.getprofileId() == prof1.id )
            Checkprof = true;
            /*Else 
            If(userinfo.getprofileId() == prof2.id )
            Checkprof = true;
            Else 
            If(userinfo.getprofileId() == prof3.id )
            Checkprof = true;*/
            Else
            Checkprof = false;
         system.debug('++++++++++++++PROFILE++++++++++++++'+checkprof);
         ptid = ApexPages.currentPage().getParameters().get('id');
         //sts = ptId.indexOf('/');
         //BaseId = ptid.substring(sts+1,sts+16);
         //checkprof = false;
         lstWarp = new List<CommentClass>();
         getrelatedcomments  = new LIST<Case_Comment__c>();
         casecommentlist = new LIST<CaseComment>();
         idSet = new SET<ID>();
         getrelatedcomments = [select Case_Comment_Id__c from Case_Comment__c ];
         for(Case_Comment__c id:getrelatedcomments ){
             IdSet.add(id.Case_Comment_Id__c);
         }
         system.debug('*******ID******'+ptid);
         casecommentlist =[Select id,CommentBody,IsPublished,ParentId,createdById,createdDate from CaseComment where ParentId=:ptid AND id NOT in:Idset ORDER By CreatedDate DESC ];
         for(CaseComment c: casecommentlist ){
             LinkObj = '/'+c.id+'/e?parent_id='+c.parentId+'&retURL=%2F'+c.parentId ;
             lstWarp.add(new CommentClass(LinkObj, c));
         }
       
    } 
}