@isTest
Private class ChangeCaseOriginToChat_testClass{
    static Account acc = new Account();    
    static Contact con = new Contact();
    static LiveChatTranscript Ct = new LiveChatTranscript();    
    static Case cases = new Case();
    static Public_Product__c PublicProduct = new Public_Product__c();
    static User TestUserNew12 = new User();
    static RecordType[] RT = [select id from RecordType where Name =: 'General Support'];   
    static Profile[] profiles = [Select name, id From Profile where Name =: 'CS - Agent'];
    static testmethod void Test1(){
        
        //CheckAddError.AddError = false;
       
        PublicProduct.Name = 'Call of Duty';
        Insert PublicProduct;
  
        cases.RecordTypeId = RT[0].id;
        cases.Type = 'Hardware';
        cases.Sub_Type__c = 'Other';
        cases.Status = 'New';
        cases.origin = 'Phone Call';
        cases.Product_Effected__c = PublicProduct.id;
        cases.subject = 'Test';
        cases.Comment_Not_Required__c = True;
        try{
            insert cases;
        }
        catch(dmlException e){
        }        
        //CheckAddError.AddError = true;        
        LiveChatVisitor lcv = new LiveChatVisitor();
        try{
            insert lcv;
        }
        catch(dmlException e){
        }
        
        ct.LiveChatVisitorId = lcv.id ;
        ct.CaseId = cases.id ;
        ct.status = 'completed';
        try{
            insert ct;
        }
        catch(dmlException e){
        }           
    }
    
}