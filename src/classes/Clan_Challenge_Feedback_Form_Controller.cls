/////////////////////////////////////////////////////////////////////////////
// @Name             Clan_Challenge_Feedback_Form_Controller
// @author           Jon Albaugh
// @date             24-JUL-2012
// @description      Controller for: Clan_Challenge_Feedback_Form_Conf
/////////////////////////////////////////////////////////////////////////////


public class Clan_Challenge_Feedback_Form_Controller{
    
    public String userLanguage;

    public string clanName { get; set; }
    public string gamertag { get; set; }
    public string platform { get; set; }
    public string feedback { get; set; }
	
	public List<SelectOption> platforms { get; set; }

    public Clan_Challenge_Feedback_Form_Controller(){
    	platforms = new List<SelectOption>();
    	platforms.add(new SelectOption('XBOX', 'XBOX'));
    	platforms.add(new SelectOption('PS3', 'PS3'));
    }
    
    //Method to create a Feedback Object
    public pagereference createNewFeedback(){
    
        Clan_Challenge_Feedback__c newFeedback = new Clan_Challenge_Feedback__c();
        
        newFeedback.Clan_Name__c = clanName;
        newFeedback.Gamertag__c = gamertag;
        newFeedback.Platform__c = platform;
        newFeedback.Feedback__c = feedback;
        
        
        try{
            insert newFeedback ;
        }
        catch(Exception dEx){ 
            return null; 
        } 
        
        PageReference confirmationPage =System.Page.Clan_Challenge_Feedback_Form_Conf;
        return confirmationPage;
    }
    
    //Method to set the user language from the language cookie 
    public String getUserLanguage()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            system.debug('tempCookie not null');
            userLanguage = tempCookie.getValue();            
        }else{
            system.debug('tempCookie is null');
            userLanguage = 'en_US';
        }   
        return userLanguage;
    }
}