/////////////////////////////////////////////////////////////////////////////
// @Name             Clan_Emblem_Request_Form_Controller
// @author           Jon Albaugh
// @date             31-JUL-2012
// @description      Controller for: Clan_Emblem_Request_Form
/////////////////////////////////////////////////////////////////////////////


public class Clan_Emblem_Request_Form_Controller{
    
    public String userLanguage;
    public String ExceptionMessage{ get; set; }

    public List<SelectOption> platforms { get; set; }
    
    public Clan_Level_Emblem_Request__c request { get; set; }
    public boolean MissingEmblemErrorRendered { get; set; }

    public Clan_Emblem_Request_Form_Controller(ApexPages.StandardController controller){
        request = (Clan_Level_Emblem_Request__c)controller.getRecord();
                
        MissingEmblemErrorRendered = False;
        
        platforms = new List<SelectOption>();
        platforms.add(new SelectOption('XBOX', 'XBOX'));
        platforms.add(new SelectOption('PS3', 'PS3'));
    }
       
    //Method to create a Feedback Object
    public pagereference createNewRequest(){
       
       //Defaults:
       MissingEmblemErrorRendered = False;
   
       //Clan Name verification:
       if(request.Clan_Name__c == null){
           request.Clan_Name__c.AddError('Please Enter Your Clan Name');
           return null;
       }
       
       //Clan ID Verification:
       if (request.Clan_ID__c == null){
           request.Clan_ID__c.AddError('Please Enter Your Clan ID');
           return null;
       }
       
       try{
           Integer i = Integer.valueOf(request.Clan_ID__c);
       }
       catch(Exception e){
           request.Clan_ID__c.AddError('Invalid Clan ID');
           return null;
       }
       
       //Clan Level Verification
       if (request.Clan_Level__c == null){
           request.Clan_Level__c.AddError('Please Enter Your Clan Level');
           return null;
        }
       
       Integer ClanLevel = 0;
       try{
           ClanLevel = Integer.ValueOf(request.Clan_Level__c);
           if (ClanLevel > 50){
               request.Clan_Level__c.AddError('Invalid Clan Level');
               return null;
           }
           
       }
       catch(Exception e){
           request.Clan_Level__c.AddError('Invalid Clan Level');
           return null;
       }
            
       if (request.Missing_Emblems__c == ''){
           MissingEmblemErrorRendered = True;
           return null;
       }
       
       try{
           insert request;
           PageReference confirmationPage =System.Page.Clan_Emblem_Request_Form_Conf;
           return confirmationPage;
       }
       
       catch(Exception ex){
           ExceptionMessage = 'Oops! Something went wrong. Please Refesh the page and try again.';
           return null;
       }
    }
    
    //Method to set the user language from the language cookie 
    public String getUserLanguage()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            system.debug('tempCookie not null');
            userLanguage = tempCookie.getValue();            
        }else{
            system.debug('tempCookie is null');
            userLanguage = 'en_US';
        }   
        return userLanguage;
    }
}