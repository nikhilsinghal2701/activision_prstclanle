/////////////////////////////////////////////////////////////////////////////
// @Name             Clan_Emblem_Request_Form_Controller_Test
// @author           Jon Albaugh
// @date             31-JUL-2012
// @description      Test For (Controller for: Clan_Emblem_Request_Form)
/////////////////////////////////////////////////////////////////////////////

@istest
public with sharing class Clan_Emblem_Request_Form_Controller_Test{

    public static testMethod void setupTest(){
        
        Clan_Level_Emblem_Request__c Cler = new Clan_Level_Emblem_Request__c();
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(Cler);
        Clan_Emblem_Request_Form_Controller controller = new Clan_Emblem_Request_Form_Controller(stdController);
        
        Controller.userLanguage = controller.getUserLanguage();
        
        //Catch null clan name:
        System.AssertEquals(null, controller.createNewRequest());
        
        //Provide Valid Clan Name:
        Cler.Clan_Name__c = 'Test Clan Name';
        
        //Catch null Clan ID:
        System.AssertEquals(null, controller.createNewRequest());
        
        //Catch Non Numeric Clan ID:
        Cler.Clan_Id__c = 'abc123';
        System.AssertEquals(null, controller.createNewRequest());
        
        //Provide Valid Clan ID:
        Cler.Clan_Id__c = '123';
        
        //Catch null Clan Level:
        System.AssertEquals(null, controller.createNewRequest());
        
        //Catch Non Numeric Clan Level:
        Cler.Clan_Level__c = 'abc123';
        System.AssertEquals(null, controller.createNewRequest());
        
        //Catch Invalid Clan Level:
        Cler.Clan_Level__c = '99';
        System.AssertEquals(null, controller.createNewRequest());
        
        //Provide Valid Clan Level:
        Cler.Clan_Level__c = '12';
        
        //Catch Blank Emblems:
        Cler.Missing_Emblems__c = '';
        System.AssertEquals(null, controller.createNewRequest());
        
        //Provide Valid Missing Emblems:
        Cler.Missing_Emblems__c = 'Headshot Emblem';
        
        //Provide Valid Platform:
        Cler.Platform__c = 'XBOX';
        
        //Create Successfull Request
        PageReference pgRef = Controller.createNewRequest();

             
        //set Exception Message:
        controller.ExceptionMessage = 'Exception Message';
    }
}