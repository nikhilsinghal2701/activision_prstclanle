/////////////////////////////////////////////////////////////////////////////////////////////////
// @Name             Clan_Operation_Award_Form_Controller
// @author           Jon Albaugh
// @date             31-JUL-2012
// @description      Controller for: Clan_Emblem_Request_Form
/////////////////////////////////////////////////////////////////////////////////////////////////


public class Clan_Operation_Award_Form_Controller{
    
    public String userLanguage;
    public String ExceptionMessage{ get; set; }

    public List<SelectOption> platforms { get; set; }
    public List<SelectOption> entitlements { get; set; }
    
    public Clan_Operation_Award_Request__c request { get; set; }
    public boolean MissingGamertagsErrorRendered{ get; set; }
	public boolean LongGamertagsErrorRendered{ get; set; }

    public Clan_Operation_Award_Form_Controller(ApexPages.StandardController controller){
        userLanguage = getUserLanguage();
        
        request = (Clan_Operation_Award_Request__c)controller.getRecord();
                
        MissingGamertagsErrorRendered = False;
        
        platforms = new List<SelectOption>();
        platforms.add(new SelectOption('XBOX', 'XBOX'));
        platforms.add(new SelectOption('PS3', 'PS3'));
        
        entitlements = new List<SelectOption>();
        entitlements.add(new SelectOption('XP', 'XP'));
        //Language Hack:
        //English
        if (userLanguage == 'en_US'){
            entitlements.add(new SelectOption('Badge', 'Badge'));
            entitlements.add(new SelectOption('Both', 'Both'));
        }
        //UK English
        else if (userLanguage == 'en_GB'){
            entitlements.add(new SelectOption('Badge', 'Badge'));
            entitlements.add(new SelectOption('Both', 'Both'));
        }
        //German
        else if (userLanguage == 'de_DE'){
            entitlements.add(new SelectOption('Badge', 'Abzeichen'));
            entitlements.add(new SelectOption('Both', 'Beides'));
        }
        //French
        else if (userLanguage == 'fr_FR'){
            entitlements.add(new SelectOption('Badge', 'Badge'));
            entitlements.add(new SelectOption('Both', 'Les deux'));
        }
        //Italian
        else if (userLanguage == 'it_IT'){
            entitlements.add(new SelectOption('Badge', 'Badge'));
            entitlements.add(new SelectOption('Both', 'Entrambi'));
        }
        //Spanish
        else if (userLanguage == 'es_ES'){
            entitlements.add(new SelectOption('Badge', 'Chapa'));
            entitlements.add(new SelectOption('Both', 'Ambos'));
        }
        //Australian English
        else if (userLanguage == 'en_AU'){
            entitlements.add(new SelectOption('Badge', 'Badge'));
            entitlements.add(new SelectOption('Both', 'Both'));
        }
        //Dutch
        else if (userLanguage == 'nl_NL'){
            entitlements.add(new SelectOption('Badge', 'Badge'));
            entitlements.add(new SelectOption('Both', 'Allebei'));
        }
        //FRBE
        else if (userLanguage == 'fr_BE'){
            entitlements.add(new SelectOption('Badge', 'Badge'));
            entitlements.add(new SelectOption('Both', 'Les deux'));
        }
        //FRLU
        else if (userLanguage == 'fr_LU'){
            entitlements.add(new SelectOption('Badge', 'Badge'));
            entitlements.add(new SelectOption('Both', 'Les deux'));
        }
        //Sweedish
        else if (userLanguage == 'sv_SE'){
            entitlements.add(new SelectOption('Badge', 'Badge'));
            entitlements.add(new SelectOption('Both', 'Båda'));
        }
        //
        else if (userLanguage == 'en_NO'){
            entitlements.add(new SelectOption('Badge', 'Badge'));
            entitlements.add(new SelectOption('Both', 'Both'));
        }
        //
        else if (userLanguage == 'en_FI'){
            entitlements.add(new SelectOption('Badge', 'Badge'));
            entitlements.add(new SelectOption('Both', 'Båda'));
        }
        //
        else if (userLanguage == 'pt_BR'){
            entitlements.add(new SelectOption('Badge', 'Crachá'));
            entitlements.add(new SelectOption('Both', 'Ambos'));
        }
        else{
            entitlements.add(new SelectOption('Badge', 'Badge'));
            entitlements.add(new SelectOption('Both', 'Both'));
        }
    }
       
    //Method to create a Feedback Object
    public pagereference createNewRequest(){
       
       //Defaults:
       MissingGamertagsErrorRendered = False;
   
       //Clan Name verification:
       if(request.Clan_Name__c == null){
           request.Clan_Name__c.AddError('Please Enter Your Clan Name');
           return null;
       }
       
       //Clan ID null Verification:
       if (request.Clan_ID__c == null){
           request.Clan_ID__c.AddError('Please Enter Your Clan ID');
           return null;
       }
       
       //Clan ID Numeric Validation:
       try{
           Integer i = Integer.ValueOf(request.Clan_ID__c);
       }
       catch(Exception Ex){
           request.Clan_ID__c.AddError('Invalid Clan ID');
           return null;
       }
       
       //Operation ID null Verification:
       if (request.Clan_Operation_ID__c == null){
           request.Clan_Operation_ID__c.AddError('Please Enter The Operation ID');
           return null;
       }
       
       //Operation ID Numeric Validation:
       try{
           Integer i = Integer.ValueOf(request.Clan_Operation_ID__c);
       }
       catch(Exception Ex){
           request.Clan_Operation_ID__c.AddError('Invalid Operation ID');
           return null;
       }

       //Clan Gamertag Validation:
       if (request.Clan_Gamertags__c == ''){
           MissingGamertagsErrorRendered = True;
           return null;
       }
       if (request.Clan_Gamertags__c.length() > 255){
           LongGamertagsErrorRendered = True;
           return null;
       }
       
       try{
           insert request;
           PageReference confirmationPage =System.Page.Clan_Operation_Award_Form_Conf;
           return confirmationPage;
       }
       
       catch(Exception ex){
           ExceptionMessage = 'Oops! Something went wrong. Please Refesh the page and try again.';
           return null;
       }
    }
    
    //Method to set the user language from the language cookie 
    public String getUserLanguage()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            system.debug('tempCookie not null');
            userLanguage = tempCookie.getValue();            
        }else{
            system.debug('tempCookie is null');
            userLanguage = 'en_US';
        }   
        return userLanguage;
    }
}