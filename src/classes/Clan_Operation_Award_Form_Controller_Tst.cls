/////////////////////////////////////////////////////////////////////////////
// @Name             Clan_Operation_Award_Form_Controller_Test
// @author           Jon Albaugh
// @date             31-JUL-2012
// @description      Test For (Controller for: Clan_Operation_Award_Form_Controller)
/////////////////////////////////////////////////////////////////////////////

@istest
public with sharing class Clan_Operation_Award_Form_Controller_Tst{

    public static testMethod void setupTest(){
        
        Clan_Operation_Award_Request__c Coar = new Clan_Operation_Award_Request__c();
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(Coar);
        Clan_Operation_Award_Form_Controller controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        
        //Language Testing:
        Cookie lang = new Cookie('uLang','en_US',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','EN_GB',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','de_DE',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','fr_FR',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','it_IT',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','es_ES',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','en_AU',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','nl_NL',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','fr_BE',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','fr_LU',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','sv_SE',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','en_NO',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','en_FI',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','pt_BR',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        lang = new Cookie('uLang','test',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{lang});
        controller = new Clan_Operation_Award_Form_Controller(stdController);
        
        //Catch null clan name:
        System.AssertEquals(null, controller.createNewRequest());
        
        //Provide Valid Clan Name:
        Coar.Clan_Name__c = 'Test Clan Name';
        
        //Catch null Clan ID:
        System.AssertEquals(null, controller.createNewRequest());
        
        //Provide Invalid Clan ID:
        Coar.Clan_Id__c = 'abc123';
        
        //Catch Invalid Clan ID:
        System.AssertEquals(null, controller.createNewRequest());
        
        //Provide Valid Clan ID:
        Coar.Clan_Id__c = '123';
        
        //Catch null Operation ID:
        System.AssertEquals(null, controller.createNewRequest());
        
        //Catch Invalid Operation ID:
        Coar.Clan_Operation_Id__c = 'qwer4567';
        System.AssertEquals(null, controller.createNewRequest());
        
        //Provide Valid Operation ID:
        Coar.Clan_Operation_Id__c = '12345';
        
        //Catch Blank Gamertags:
        Coar.Clan_Gamertags__c = '';
        System.AssertEquals(null, controller.createNewRequest());
        
        //Provide Valid Clan Gamertags:
        Coar.Clan_Gamertags__c = 'FEXTWOLF';
        
        //Set Platform & Missing Entitlement
        Coar.Platform__c = 'PS3';
        Coar.Missing_Entitlement__c = 'Both';

        //Assert Proper Return:
        PageReference confirmationPage = controller.createNewRequest();
        
        //set Exception Message:
        controller.ExceptionMessage = 'Exception Message';
    }
}