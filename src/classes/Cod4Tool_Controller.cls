public with sharing class Cod4Tool_Controller {

        public string token { get; set; }
        public string decrypted { get; set; }
        public boolean decryptedSet { get; set; }
        public string Gamertag {get; set;}
    
    public Cod4Tool_Controller(){
        
        decryptedSet = false;
        MAP<string, string> params = ApexPages.currentPage().getParameters();
        for (string paramname : params.keySet() ){
            if(paramname == 'stoken'){
                token = params.get(paramname);
                
                LASService.BasicHttpBinding_ILinkedAccounts service = new LASService.BasicHttpBinding_ILinkedAccounts();
                decrypted = service.DecryptToken( token );     
                decryptedSet = true;
            }
        }       
    }

    public void SaveToken(){
        system.debug( decrypted );       
        if(decrypted != Null && decrypted !=''){
            Cookie tempCookie = ApexPages.currentPage().getCookies().get('gt');
            if(tempCookie != null){
                system.debug('tempCookie not null');
                Gamertag = tempCookie.getValue();            
            }else{
                system.debug('tempCookie is null');
                Gamertag ='Not Found';
            }
            cod4reset__c obj = new cod4reset__c();
            obj.Gamertag__c = Gamertag;
            obj.token__c = decrypted;
                      
            insert obj;
        } 
    }
}