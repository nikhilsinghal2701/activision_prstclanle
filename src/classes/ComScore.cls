public without sharing class ComScore{ //so we can get the UCDID from the user table instead of the occasionally empty contact

    public String getUcdId(){
        String result = '';
        User u = [SELECT ContactId, Contact.UCDID__c, FederationIdentifier FROM User WHERE Id = :UserInfo.getUserId()];
        if(u.FederationIdentifier != null){
            result = EncodingUtil.urlEncode(u.FederationIdentifier, 'UTF-8');
        }else if (u.ContactId != null && u.Contact.UCDID__c != null){
            result = EncodingUtil.urlEncode(u.Contact.UCDID__c, 'UTF-8');
        }
        return result;
    }
    
    public String getBrand(){
        String result = 'not specified';
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('clickedOn');
        if(tempCookie != null) result = tempCookie.getValue();
        if(result == null || result == '') result = 'not specified';
        return EncodingUtil.urlEncode(result, 'UTF-8');
    }
    public String getULang(){
        String result = 'en_US';
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null) result = tempCookie.getValue();
        else if(ApexPages.currentPage().getParameters().containsKey('lang')){
            result = ApexPages.currentPage().getParameters().get('lang');
        }
        return EncodingUtil.urlEncode(result, 'UTF-8');
    }
    
    public String getPageName(){
        String[] urlBlocks = ApexPages.currentPage().getUrl().split('/');
        return EncodingUtil.urlEncode(urlBlocks[urlBlocks.size()-1].split('\\?')[0].toLowerCase(), 'UTF-8');
    }
    public String getArticleTitleFromURL(String pUrl){
        String result = '', strUrl = pUrl;
        if(pUrl == null || pUrl == '') strUrl = URL.getCurrentRequestUrl().toExternalForm();
        try{if(strUrl != null && strUrl.containsIgnoreCase('/articles/')){
                result = strUrl.split('/articles/')[1].split('/')[2].split('\\?')[0];
            }
        }catch(Exception e){system.debug(e);}
        return result;
    }
    public String getArticleTitleFromURL(){
        return getArticleTitleFromURL('');
    }
    public String getArticleL1(){
        return getArticleL1('','');
    }
    public String getArticleL1(String lang, String artTitle){
        String result = 'not specified', uLang = lang, articleUrlName = artTitle;
        if(uLang == '' || uLang == null) uLang = filterLang(getULang());
        if(articleUrlName == '' || articleUrlName == null) articleUrlName = getArticleTitleFromURL();
        try{if(articleUrlName != ''){
                for(FAQ__kav kav : [SELECT WebToCase_Type_L1__c FROM FAQ__kav WHERE PublishStatus = 'online' AND 
                                    Language = :uLang AND URLName = :articleUrlName LIMIT 1])
                {
                    result = kav.WebToCase_Type_L1__c;
                }
            }
        }catch(Exception e){system.debug(e);}
        return result;
    }
    public String getArticleL2(){
        return getArticleL2('','');
    }
    public String getArticleL2(String lang, String artTitle){
        String result = 'not specified', uLang = lang, articleUrlName = artTitle;
        if(uLang == '' || uLang == null) uLang = filterLang(getULang());
        if(articleUrlName == '' || articleUrlName == null) articleUrlName = getArticleTitleFromURL();
        try{if(articleUrlName != ''){
                for(FAQ__kav kav : [SELECT WebToCase_SubType_L2__c FROM FAQ__kav WHERE PublishStatus = 'online' AND 
                                    Language = :uLang AND URLName = :articleUrlName LIMIT 1])
                {
                    result = kav.WebToCase_SubType_L2__c;
                }
            }
        }catch(Exception e){system.debug(e);}
        return result;
    }
    public String filterLang(String s){
        String val = 'en_US';
        if(s == null || s == '') val = 'en_US';
        else if(s.containsIgnoreCase('nl')) val = 'nl_NL';
        else if(s.containsIgnoreCase('fr')) val = 'fr';
        else if(s.containsIgnoreCase('de')) val = 'de';
        else if(s.containsIgnoreCase('it')) val = 'it';
        else if(s.containsIgnoreCase('pt')) val = 'pt_BR';
        else if(s.containsIgnoreCase('es')) val = 'es';
        else if(s.containsIgnoreCase('sv')) val = 'sv';
        return val;
    }

}