@isTest
public class ComScore_test{

    private testMethod static void testGetUcdId(){
    	ComScore tstCls = new ComScore();
        system.assertEquals(tstCls.getUcdId(), '');
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.runAs ( portalAccountOwner1 ) {
            //Create account
            Account portalAccount1 = new Account(
                Name = 'TestAccount',
                OwnerId = portalAccountOwner1.Id
            );
            Database.insert(portalAccount1);
            
            //Create contact
            Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'McTesty',
                AccountId = portalAccount1.Id,
                Email = System.now().millisecond() + 'test@test.com'
            );
            Contact contact2 = new Contact(
                FirstName = 'Test',
                Lastname = 'McTesty',
                AccountId = portalAccount1.Id,
                UCDID__c = '1234',
                Birthdate = system.today(),
                Email = System.now().millisecond() + 'test2@test.com'
            );
            Database.insert(new Contact[]{contact1, contact2});
            
            //Create user
            Profile portalProfile = [SELECT Id FROM Profile WHERE UserType = 'CspLitePortal' LIMIT 1];
            User[] users = new User[]{
                new User(
                    Username = System.now().millisecond() + 'test12345@test.com',
                    ContactId = contact1.Id,
                    ProfileId = portalProfile.Id,
                    Alias = 'test123',
                    Email = 'test12345@test.com',
                    EmailEncodingKey = 'UTF-8',
                    LastName = 'McTesty',
                    CommunityNickname = 'test12345',
                    TimeZoneSidKey = 'America/Los_Angeles',
                    LocaleSidKey = 'en_US',
                    LanguageLocaleKey = 'en_US'
                ),
                new User(
                    Username = System.now().millisecond() + 'test67890@test.com',
                    ContactId = contact2.Id,
                    ProfileId = portalProfile.Id,
                    Alias = 'test678',
                    Email = 'test12345@test.com',
                    EmailEncodingKey = 'UTF-8',
                    LastName = 'McTesty',
                    CommunityNickname = 'test678',
                    TimeZoneSidKey = 'America/Los_Angeles',
                    LocaleSidKey = 'en_US',
                    LanguageLocaleKey = 'en_US'
                )
            };
            Database.insert(users);
        }
    
        User u = [SELECT ContactId, Contact.UCDID__c FROM User WHERE ContactId != null AND isActive = true AND Contact.UCDID__c = null LIMIT 1];
        system.runAs(u){
            system.assertEquals(tstCls.getUcdId(), '');
        }
        u = [SELECT ContactId, Contact.UCDID__c FROM User WHERE ContactId != null AND isActive = true AND Contact.UCDID__c != null LIMIT 1];
        system.runAs(u){
            system.assertEquals(tstCls.getUcdId(), u.Contact.UCDID__c);
        }
    }
    
    private testMethod static void testGetBrand(){
        ComScore tstCls = new ComScore();
        system.assertEquals('not+specified',tstCls.getBrand());
        ApexPages.currentPage().setCookies(new Cookie[]{new Cookie('clickedOn','test',null,-1,false)});
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('clickedOn');
        system.assertEquals('test',tstCls.getBrand());
    }
    
    private testMethod static void testGetULang(){
        ComScore tstCls = new ComScore();
        system.assertEquals('en_US',tstCls.getUlang());
        ApexPages.currentPage().getParameters().put('lang','sv_SV');
        system.assertEquals('sv_SV',tstCls.getUlang());
        ApexPages.currentPage().getParameters().remove('lang');
        ApexPages.currentPage().setCookies(new Cookie[]{new Cookie('uLang','nl_NL',null,-1,false)});
        system.assertEquals('nl_NL',tstCls.getUlang());
    }
    
    private testMethod static void testGetPageName(){
        ComScore tstCls = new ComScore();
        test.setCurrentPage(Page.Atvi_supporthome_unauthenticated);
        system.assertEquals(EncodingUtil.urlEncode('Atvi_supporthome_unauthenticated'.toLowerCase(), 'UTF-8'), tstCls.getPageName());
    }

    private testMethod static void testGetArticleTitleFromURL(){
        ComScore tstCls = new ComScore();
        tstCls.getArticleTitleFromUrl(); //test production method
        tstCls.getArticleTitleFromUrl('/articles/'); //trip the try catch
        system.assertEquals('',tstCls.getArticleTitleFromURL(''));
        system.assertEquals('',tstCls.getArticleTitleFromURL('http://support.activision.com/atvi_supporthome_unauthenticated?clickedon=BOII'));
        system.assertEquals('name-of-article',tstCls.getArticleTitleFromURL('http://support.activision.com/articles/en_US/FAQ/name-of-article?q=search+query'));
    }

    private testMethod static void testGetArticleL1(){
        ComScore tstCls = new ComScore();
        tstCls.getArticleL1();
        String title = 'test-article-'+string.valueOf(Crypto.getRandomInteger());
        FAQ__kav art = new FAQ__kav(Title = title, URLName = title,WebToCase_Type_L1__c = 'l1');
        insert art;
        art = [SELECT Id, WebToCase_Type_L1__c, KnowledgeArticleId FROM FAQ__kav WHERE Id = :art.Id];
        KbManagement.PublishingService.publishArticle(art.KnowledgeArticleId, true);
        system.assertEquals(art.WebToCase_Type_L1__c, tstCls.getArticleL1('en_US',title));
    }

    private testMethod static void testGetArticleL2(){
        ComScore tstCls = new ComScore();
        tstCls.getArticleL2();
        String title = 'test-article-'+string.valueOf(Crypto.getRandomInteger());
        FAQ__kav art = new FAQ__kav(Title = title,URLName = title,WebToCase_SubType_L2__c = 'l2');
        insert art;
        art = [SELECT Id, WebToCase_SubType_L2__c, KnowledgeArticleId FROM FAQ__kav WHERE Id = :art.Id];
        KbManagement.PublishingService.publishArticle(art.KnowledgeArticleId, true);
        system.assertEquals(art.WebToCase_SubType_L2__c, tstCls.getArticleL2('en_US',title));
    }
    
	private testMethod static void testFilterLang(){
        ComScore tstCls = new ComScore();
        system.assertEquals(tstCls.filterLang(null), 'en_US');
        system.assertEquals(tstCls.filterLang(''), 'en_US');
        system.assertEquals(tstCls.filterLang('nl'), 'nl_NL');
        system.assertEquals(tstCls.filterLang('nl_NL'), 'nl_NL');
        system.assertEquals(tstCls.filterLang('fr_FR'), 'fr');
        system.assertEquals(tstCls.filterLang('fr'), 'fr');
        system.assertEquals(tstCls.filterLang('de_DE'), 'de');
        system.assertEquals(tstCls.filterLang('it_IT'), 'it');
        system.assertEquals(tstCls.filterLang('pt'), 'pt_BR');
		system.assertEquals(tstCls.filterLang('pt_BR'), 'pt_BR');
		system.assertEquals(tstCls.filterLang('es_ES'), 'es');
		system.assertEquals(tstCls.filterLang('sv_SV'), 'sv');
    }
    
}