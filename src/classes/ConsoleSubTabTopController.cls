public class ConsoleSubTabTopController{
    public string currentURL{get;set;}
    public string caseid{get;set;}
    public list<case> theCase{get;set;}
    public string result{get;set;}
    public list<SocialPersona> thePersona{get;set;}
    public string error{get;set;}
    public list<SocialPost> thePost{get;set;}
    public string strPost{get;set;}
    
  
    public ConsoleSubTabTopController(){
        error='';
        currentURL = ApexPages.currentPage().getUrl();
        caseid = ApexPages.currentPage().getParameters().get('id');
        theCase = [select id, contactid, R6Service__MediaIcon__c, R6Service__PersonaAvatar__c from Case where id=:caseid];
        thePost = [select id, content from SocialPost where parentid=:caseid order by createddate desc];
        
        if(theCase.size()>0){
            result = 'Found the case';
    	}
        else{
            result = 'No data found';
        }
        
        thePersona = [select id, 
                      name,  
                      R6Service__AvatarIconResource__c
                      from SocialPersona where Parentid=:theCase[0].contactid];
        if(thePersona.size()==0){
        	error += 'Persona Not Found';
        }
      
        if(thePost.size()>0){
        	strPost = thePost[0].content;
        }
        else{
        	error += 'Post Not Found';
        }
    }
}