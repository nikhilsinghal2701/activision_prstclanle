public class ConsoleTabLeftController{
    public string caseid{get;set;}
    public case contactid{get;set;}
    public list<multiplayer_account__c> mpa{get;set;}
    public list<asset_custom__c> assets{get;set;}
    public string error{get;set;}

    public ConsoleTabLeftController(){
        error = '';
        caseid = ApexPages.currentPage().getParameters().get('id');
        contactid = [select id, contactid from case where id=:caseid limit 1];
        mpa = [select id, Platform__c, Gamertag__c, is_current__c from multiplayer_account__c where contact__c=:contactid.id];
        //assets = [select id,
        if(mpa.size()==0){
        	error = 'No Multiplayer Data Found';
        }
    }
}