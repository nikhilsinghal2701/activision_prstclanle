public class ConsoleTabTopController{
    public string caseid{get;set;}
    public list<caseComment> comment{get;set;}
    public string strComment{get;set;}
    public string inputComment{get;set;}
    
    public ConsoleTabTopController(){
        caseid = ApexPages.currentPage().getParameters().get('id');
		comment = [select commentbody from casecomment where parentid=:caseid order by createddate desc];
        if(comment.size()>0){
        	strComment = comment[0].commentbody;
        }
        else{
            strComment = 'No comments have been created';
        }        
    }
    
    public pagereference submit(){
        if(inputComment!=''){
            casecomment cc = new casecomment();
            cc.parentid = caseid;
            cc.commentbody = inputComment;
            insert cc;
        }
        else{
        }
        return null;
    }
}