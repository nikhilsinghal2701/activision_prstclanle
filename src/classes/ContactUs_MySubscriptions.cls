public without sharing class ContactUs_MySubscriptions {
    
    public subscription[] mySubscriptions {get; set;}

    public ContactUs_MySubscriptions() {
        system.debug('hehehheheeheheheheehehehehe');
        this.mySubscriptions = new subscription[]{};
        User theUser = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
        for(Article_Subscription__c sub : [SELECT KAV_id__c FROM Article_Subscription__c WHERE Contact__c = :theUser.ContactId]){
            system.debug('found one!');
            this.mySubscriptions.add(new subscription(sub));
        }
    }
    public class subscription{
        public Article_Subscription__c theSubRec {get; set;}
        public FAQ__kav theSubscribedArtVersion {get; set;}
        public FAQ__kav theCurrentArtVersion {get; set;}
        public boolean hasBeenUpdatedSinceSubscription {get; set;}
        public subscription(Article_Subscription__c pTheSubRec){
            system.debug('Sub is....'+pTheSubRec);
            this.theSubRec = pTheSubRec;
            this.theSubscribedArtVersion = [SELECT KnowledgeArticleId, Title, URLName, Answer__c, Language, VersionNumber
                                            FROM FAQ__kav WHERE Id = :pTheSubRec.KAV_id__c];
            this.theCurrentArtVersion = [SELECT KnowledgeArticleId, Title, URLName, Answer__c, Language, VersionNumber, LastPublishedDate
                                         FROM FAQ__kav 
                                         WHERE IsLatestVersion = true AND 
                                               KnowledgeArticleId = :this.theSubscribedArtVersion.KnowledgeArticleId AND
                                               Language = :this.theSubscribedArtVersion.Language AND
                                               PublishStatus = 'online'];

            this.hasBeenUpdatedSinceSubscription = this.theSubscribedArtVersion.Id != this.theCurrentArtVersion.Id;
        }
    }

}