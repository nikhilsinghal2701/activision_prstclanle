@isTest
private class ContactUs_MySubscriptions_Test {
    
     static testMethod void test_method_one() {
        Contact testcontact = UnitTestHelper.generateTestContact();
        insert testcontact;

        User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
        testportaluser.ProfileId = [select Id, name from Profile where Name = 'ATVI OHV Cust Port User' limit 1].Id;
        insert testportaluser;

        FAQ__Kav testfaq = new FAQ__Kav(Title = 'Test', Summary = 'Test', URLName = 'abc123', Language = 'en_US', Answer__c = 'Test');
        insert testfaq;

        testfaq = [Select KnowledgeArticleId From FAQ__Kav Where Id = :testfaq.Id];
        KBManagement.PublishingService.publishArticle(testfaq.KnowledgeArticleId,true);

        Article_Subscription__c asub = new Article_Subscription__c(Name = 'Test', Article_Name__c = 'Test Article', Contact__c = testcontact.Id, Game_Title__c = 'Test Title', KAV_Id__c = testfaq.Id, URL__c = 'test url');
        insert asub;
        
        ContactUs_MySubscriptions cms = new ContactUs_MySubscriptions();

        ContactUs_MySubscriptions.subscription innercls = new ContactUs_MySubscriptions.subscription(asub);
        system.assertEquals(innercls.theSubRec.Id,asub.Id);
        system.assertEquals(innercls.theSubscribedArtVersion.Id,testfaq.Id);
        system.assertEquals(innercls.hasBeenUpdatedSinceSubscription,false);


    }
    
        
}