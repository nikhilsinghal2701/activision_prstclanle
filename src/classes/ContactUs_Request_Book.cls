public class ContactUs_Request_Book {
	public Case currentCase {get; set;}
	public Boolean canBook {get; set;}
	public Boolean booked {get; set;}
	public boolean callAlreadyScheduled {get; set;}
	public Callback_Request__c theCallback {get; set;}
	Contact theGamer;
	public Contact getTheGamer(){
		if(theGamer == null){
			theGamer = new Contact();
			if(UserInfo.getUserType() == 'CSPLitePortal'){
				theGamer = [SELECT Contact.Id, Contact.FirstName, Contact.LastName, Contact.Email, Contact.Phone 
							FROM User WHERE Id = :UserInfo.getUserId()].Contact;
			}
		}
		return theGamer;
	}
	public string getSelectedTime(){
		try{
            String urlTimeZone = ApexPages.CurrentPage().getParameters().get('z');
            system.debug('urlTimeZone == '+urlTimeZone);
            //Data format #1 (GMT+13:00) Phoenix Inseln Zeit (Pacific/Enderbury) OR
            //Data format #2 (GMT+13:00)%20Phoenix%20Inseln%20Zeit%20(Pacific/Enderbury)
            String splitOn = ' \\('; //Format 1
			if(urlTimeZone.contains('%20'))
			    splitOn = '\\%20\\('; //Format 2
            //split 1 results in [0] == "(GMT+13:00) Phoenix Inseln Zeit" [1] == Pacific/Enderbury)
            //split 2 results in [0] == Pacific/Enderbury
            urlTimeZone = urlTimeZone.split(splitOn)[1].split('\\)')[0];
            
			String strGmtEpochSelectedTime = ApexPages.CurrentPage().getParameters().get('t');
			Long lGmtSelectedTime = Long.valueOf(strGmtEpochSelectedTime.trim());
			DateTime selectedTime = DateTime.newInstance(lGmtSelectedTime);
			return selectedTime.format(
                'EEE, d MMM h:mm aaa', //Fri, 4 Jul 12:08 //note, this will not translate Fri to Freitag 
                //'EEE, d MMM hh:mm aaa', //Fri, 4 Jul 12:08 //note, this will not translate Fri to Freitag 
                urlTimeZone);
		}catch(Exception e){
			return e.getMessage();
		}
	}
	public void bookSlot(){
		booked = false;
		//try{
			String strGmtEpochSelectedTime = ApexPages.CurrentPage().getParameters().get('t');
			Long lGmtSelectedTime = Long.valueOf(strGmtEpochSelectedTime.trim());
			DateTime selectedTime = DateTime.newInstance(lGmtSelectedTime);
			
			String phone = ApexPages.CurrentPage().getParameters().get('p');
			getTheGamer();
			
			try{
				theGamer.FirstName = ApexPages.CurrentPage().getParameters().get('f');
				theGamer.LastName = ApexPages.CurrentPage().getParameters().get('l');
				theGamer.Email = ApexPages.CurrentPage().getParameters().get('e');
				theGamer.Phone = phone;
				update theGamer;
			}catch(Exception ex){
				system.debug('Gamer Update ERROR == '+ex);
			}


			canBook = ([SELECT count() FROM Callback_Request__c WHERE Requested_Callback_Time__c = :selectedtime] == 0);

	        if(canBook){
	        	canBook = [SELECT count() FROM Callback_Request__c WHERE Requested_Callback_Time__c > :system.now() AND Contact__c = :theGamer.Id] == 0;
	        	if(canBook == false){
	        		callAlreadyScheduled = true;
        		}else{
	    			if(Apexpages.CurrentPage().getparameters().containskey('w2cs')){
						theCallback = new Callback_Request__c(
			            	Requested_Callback_Time__c = selectedtime,
			            	Contact__c = theGamer.Id,
			            	Phone_Number__c = phone
			        	);
			        	insert theCallback;

		                webtocase_subtype__c subIssue = [SELECT description__c, subtypemap__c, WebToCase_Type__c 
		            									  FROM webtocase_subtype__c 
		            									  WHERE id = :apexpages.currentpage().getparameters().get('w2cs')];
		                webtocase_type__c mainIssue = [SELECT description__c, typemap__c, WebToCase_GameTitle__c 
		                								FROM webtocase_type__c where id=:subIssue.WebToCase_Type__c];
		                webtocase_gametitle__c game = [SELECT title__c, public_product__c, WebToCase_Platform__c 
		                								FROM webtocase_gametitle__c where id=:mainissue.WebToCase_GameTitle__c];
		                webtocase_platform__c platform = [SELECT name__c from webtocase_platform__c where id=:game.WebToCase_Platform__c];
			        	currentCase = new Case(
				        	RecordTypeId = [SELECT id FROM RecordType WHERE Name = 'General Support' LIMIT 1].id,
				        	ContactId = theGamer.Id,
				        	Status = 'Open',
				        	Origin = 'Customer Created Callback Ticket',
				        	Comment_not_required__c = true,
				        	Case_Not_Editable_by_Gamer__c = true,
				        	Medallia_Survey_Not_Required__c = true
			        	);
				        currentCase.Sub_Type__c = subIssue.SubTypeMap__c; //Derive Sub Type
				        currentCase.Type = mainIssue.typemap__c; //Derive Type
				        currentCase.Product_Effected__c = game.Public_Product__c;//get public product id:selectedTitle.Public_Product__r.id
				        currentCase.W2C_Origin_Language__c = 'en_US';//userLanguage; Get Language
				        currentCase.W2C_Type__c = mainIssue.description__c;
				        currentCase.W2C_SubType__c = subIssue.description__c;
				        currentCase.subject =  game.Title__c + ' (' + platform.name__c + ')' + ' : ' + mainIssue.description__c + ' (' + subIssue.description__c + ')';
				        currentCase.Description = ApexPages.CurrentPage().getParameters().get('d');
				        currentCase = Contact_Us_Controller.addDispositionsToCase(
				            currentCase, 
				            platform.Name__c,
				            game.Title__c,
				            mainIssue.typemap__c,
				            subIssue.SubTypeMap__c
				        );
				        currentCase.Callback_Request__c = theCallback.Id;
				        currentCase.OwnerId = [SELECT Id FROM Group WHERE DeveloperName = 'Tier_3_Callback_Queue'].Id;
			            insert currentCase;
			        	booked = true;
			        }
        		}
    		}
		//}catch(Exception e){
		//	system.debug('ERROR == '+e);
		//}
    }

}