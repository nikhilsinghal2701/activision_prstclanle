@isTest
private class ContactUs_Request_Book_Test {
	
	@isTest static void test_method_1() {
		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		DateTime epochtime = system.now();

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US', Contact_Options_Order__c = 'chat;twitter;facebook;forum;phone;email;form;ambassadorchat;scheduleacall', Chat_Weekday_Start__c = '00:00GMT', Chat_Weekday_End__c = '23:00GMT', Chat_Weekend_Start__c = '00:00GMT', Chat_Weekend_End__c = '23:00GMT', ForumEnabled__c = true, FacebookEnabled__c = true, AmbassadorChatEnabled__c = true, ScheduleACallEnabled__c = true, TwitterEnabled__c = true, phoneEnabled__c = true);
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(WebToCase__c = w2c1.Id, Name__c = 'Test Platform', isActive__c = true,disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Phone__c = '50-Ultimate Premium', Level_Required_To_Access_AmbassadorChat__c = '50-Ultimate Premium');
		insert w2cp1;

		WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Test Title', disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Form__c = '50-Ultimate Premium', Level_Required_To_Schedule_A_Call__c = '50-Ultimate Premium');
		insert w2cg1;

		WebToCase_Type__c w2ct1 = new WebToCase_Type__c(TypeMap__c = 'Test Type', WebToCase_GameTitle__c = w2cg1.Id, Description__c = 'Test Type', isActive__c = true, disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Forum__c = '50-Ultimate Premium', Level_Required_To_Access_Twitter__c = '50-Ultimate Premium');
		insert w2ct1;

		WebToCase_SubType__c w2cst1 = new WebToCase_SubType__c(SubTypeMap__c = 'Test SubType', Description__c = 'Test SubType', WebToCase_Type__c = w2ct1.Id, isActive__c = true, disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Facebook__c = '50-Ultimate Premium');
		insert w2cst1;

		WebToCase_Form__c w2cf1 = new WebToCase_Form__c(Name__c = 'Test Form', WebToCase_SubType__c = w2cst1.Id, isActive__c = true, Max_Open_Submissions_per_Gamer__c = 1);
		insert w2cf1;

		PageReference currentpage = Page.Contact_Us_2014;
		currentpage.getParameters().put('z','(GMT-07:00) Pacific Daylight Time (America/Tijuana)');
		currentpage.getParameters().put('t',String.valueOf(epochtime.getTime()));
		currentpage.getParameters().put('p','1234567890');
		currentpage.getParameters().put('f','Test');
		currentpage.getParameters().put('l','Person');
		currentpage.getParameters().put('e','abc@atvi.com');
		currentpage.getParameters().put('w2cs',w2cst1.Id);
		Test.setCurrentPage(currentpage);

		Id cid;

		system.runAs(testportaluser){
			ContactUs_Request_Book crb = new ContactUs_Request_Book();
			system.assertEquals(crb.getTheGamer().Id,testcontact.Id);
			crb.getSelectedTime();
			crb.bookslot();
			cid = crb.currentCase.Id;
			
		}

		system.assertEquals([Select Count() from Callback_Request__c Where Contact__c = :testcontact.Id],1);
		system.assertEquals([Select Id, ContactId From Case Where ContactId = :testcontact.id].Id,cid);
	}

	@isTest static void test_method_2() {
		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		Callback_Request__c crc = new Callback_Request__c(Contact__c = testcontact.Id, Phone_Number__c = '12345677890', Requested_Callback_Time__c = system.now().addDays(1));
		insert crc;

		DateTime epochtime = system.now();

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US', Contact_Options_Order__c = 'chat;twitter;facebook;forum;phone;email;form;ambassadorchat;scheduleacall', Chat_Weekday_Start__c = '00:00GMT', Chat_Weekday_End__c = '23:00GMT', Chat_Weekend_Start__c = '00:00GMT', Chat_Weekend_End__c = '23:00GMT', ForumEnabled__c = true, FacebookEnabled__c = true, AmbassadorChatEnabled__c = true, ScheduleACallEnabled__c = true, TwitterEnabled__c = true, phoneEnabled__c = true);
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(WebToCase__c = w2c1.Id, Name__c = 'Test Platform', isActive__c = true,disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Phone__c = '50-Ultimate Premium', Level_Required_To_Access_AmbassadorChat__c = '50-Ultimate Premium');
		insert w2cp1;

		WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Test Title', disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Form__c = '50-Ultimate Premium', Level_Required_To_Schedule_A_Call__c = '50-Ultimate Premium');
		insert w2cg1;

		WebToCase_Type__c w2ct1 = new WebToCase_Type__c(TypeMap__c = 'Test Type', WebToCase_GameTitle__c = w2cg1.Id, Description__c = 'Test Type', isActive__c = true, disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Forum__c = '50-Ultimate Premium', Level_Required_To_Access_Twitter__c = '50-Ultimate Premium');
		insert w2ct1;

		WebToCase_SubType__c w2cst1 = new WebToCase_SubType__c(SubTypeMap__c = 'Test SubType', Description__c = 'Test SubType', WebToCase_Type__c = w2ct1.Id, isActive__c = true, disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Facebook__c = '50-Ultimate Premium');
		insert w2cst1;

		WebToCase_Form__c w2cf1 = new WebToCase_Form__c(Name__c = 'Test Form', WebToCase_SubType__c = w2cst1.Id, isActive__c = true, Max_Open_Submissions_per_Gamer__c = 1);
		insert w2cf1;

		PageReference currentpage = Page.Contact_Us_2014;
		currentpage.getParameters().put('z','(GMT-07:00) Pacific Daylight Time (America/Tijuana)');
		currentpage.getParameters().put('t',String.valueOf(epochtime.getTime()));
		currentpage.getParameters().put('p','1234567890');
		currentpage.getParameters().put('f','Test');
		currentpage.getParameters().put('l','Person');
		currentpage.getParameters().put('e','abc@atvi.com');
		currentpage.getParameters().put('w2cs',w2cst1.Id);
		Test.setCurrentPage(currentpage);


		system.runAs(testportaluser){
			ContactUs_Request_Book crb = new ContactUs_Request_Book();
			system.assertEquals(crb.getTheGamer().Id,testcontact.Id);
			crb.getSelectedTime();
			crb.bookslot();

		}


		
	}
	
		
}