public class Contact_Us_2014 {
    //private variables
    //private final ATVICustomerPortalController custPortCont;
    public final Contact theGamer {get; set;}
    public string gc {get; set;}
    public Id directGameId {get;set;}
    public string gctitle {get;set;}
    private Contact_Us_Controller cu;
    private string getGamerQuery(){
        return 'SELECT ContactId, Contact.FirstName, Contact.LastName, Contact.Name, '+
            'Contact.Email, '+
            'Contact.Platform_Xbox360__c, Contact.Platform_Xbox1__c, Contact.Platform_PS3__c, '+
            'Contact.Platform_PS4__c, Contact.Platform_WiiU__c, Contact.Platform_PC__c, '+
            'Contact.Clan_ID__c, Contact.COD_App_User__c, Contact.COD_Forums_User__c, Contact.DWID__c,'+
            'Contact.MW3_Deaths__c, Contact.MW3_DLC_1__c, Contact.MW3_DLC_2__c, Contact.MW3_DLC_3__c, '+
            'Contact.MW3_DLC_4__c, Contact.MW3_Duration__c, Contact.MW3_DWID__c, Contact.MW3_K_D_Ratio__c, '+
            'Contact.MW3_Kills__c, Contact.MW3_Last_Played_Date__c, Contact.MW3_Levels__c, Contact.MW3_mDLC__c, '+
            'Contact.MW3_Prestige__c, Contact.MW3_Season_Pass__c,'+

            'Contact.BO2_Deaths__c, Contact.BO2_DLC_1__c, Contact.BO2_DLC_2__c, Contact.BO2_DLC_3__c, '+
            'Contact.BO2_DLC_4__c, Contact.BO2_Duration__c, Contact.BO2_DWID__c, Contact.BO2_K_D_Ratio__c, '+
            'Contact.BO2_Kills__c, Contact.BO2_Last_Played_Date__c, Contact.BO2_Levels__c, '+
            'Contact.BO2_mDLC__c, Contact.BO2_Prestige__c, Contact.BO2_Season_Pass__c,'+

            'Contact.BO_Deaths__c, Contact.BO_DLC_1__c, Contact.BO_DLC_2__c, Contact.BO_DLC_3__c, '+
            'Contact.BO_DLC_4__c, Contact.BO_Duration__c, Contact.BO_DWID__c, Contact.BO_K_D_Ratio__c, '+
            'Contact.BO_Kills__c, Contact.BO_Last_Played_Date__c, Contact.BO_Levels__c, '+
            'Contact.BO_mDLC__c, Contact.BO_Prestige__c, Contact.BO_Season_Pass__c,'+

            'Contact.Ghosts_Deaths__c, Contact.Ghosts_DLC_1__c, Contact.Ghosts_DLC_2__c, '+
            'Contact.Ghosts_DLC_3__c, Contact.Ghosts_DLC_4__c, Contact.Ghosts_Duration__c, '+
            'Contact.Ghosts_DWID__c, Contact.Ghosts_K_D_Ratio__c, Contact.Ghosts_Kills__c, '+
            'Contact.Ghosts_Last_Played_Date__c, Contact.Ghosts_Levels__c, Contact.Ghosts_mDLC__c, '+
            'Contact.Ghosts_Prestige__c, Contact.Ghosts_Season_Pass__c,'+

            'Contact.AW_Deaths__c, Contact.AW_DLC_1__c, Contact.AW_DLC_2__c, Contact.AW_DLC_3__c, '+
            'Contact.AW_DLC_4__c, Contact.AW_Duration__c, Contact.AW_DWID__c, Contact.AW_K_D_Ratio__c, '+
            'Contact.AW_Kills__c, Contact.AW_Last_Played_Date__c, Contact.AW_Levels__c, '+
            'Contact.AW_mDLC__c, Contact.AW_Prestige__c, Contact.AW_Season_Pass__c '+
            'FROM User WHERE Id = \''+UserInfo.getUserId()+'\'';
    }
    
    //contructors
    public Contact_Us_2014(){
        //custPortCont = new ATVICustomerPortalController(); //language detection
        if(theGamer == null && UserInfo.getUserType()=='CSPLitePortal'){
            User u = database.query(getGamerQuery());
            theGamer = u.Contact;
        }else theGamer = new Contact();
        initW2C();
    }
    public Contact_Us_2014(ApexPages.StandardController cont){
        //custPortCont = new ATVICustomerPortalController(); //language detection
        if(theGamer == null && UserInfo.getUserType()=='CSPLitePortal'){
            User u = database.query(getGamerQuery());
            theGamer = u.Contact;
        }else theGamer = new Contact();
        initW2C();
    }
    public Contact_Us_2014(ATVICustomerPortalController pCustPortCont) {
        //custPortCont = pCustPortCont;
        if(theGamer == null && UserInfo.getUserType()=='CSPLitePortal'){
            User u = database.query(getGamerQuery());
            theGamer = u.Contact;
        }else theGamer = new Contact();
        initW2C();
    }
    
    public void initW2C(){
        getUserLanguage();
        map<String, String> pageParams = ApexPages.CurrentPage().getParameters();
        if(pageParams.containsKey('gc')){
            gc = pageParams.get('gc');
            Cookie app = new Cookie('app','1',null,-1,false);
            Cookie appgame = new Cookie('gc',gc,null,-1,false);
            ApexPages.CurrentPage().setCookies(new Cookie[]{app,appgame});            
            this.getGcTitle(gc);
        }
        if(pageParams.containsKey('w2c')){
            page1InitActions();
        }else if(pageParams.containsKey('w2cp')){
            page2InitActions();
        }else if(pageParams.containsKey('w2cg')){
            page3InitActions();
        }else if(pageParams.containsKey('w2ct')){
            page3InitActions();
        }else if(pageParams.containsKey('w2cs')){
            page4InitActions();
        }else{
            page0InitActions();
        }
    }
    String userLanguage = '';
    public String getUserLanguage(){
        if(userLanguage != '') return userLanguage;
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null && tempCookie.getValue() != ''){
            userLanguage = tempCookie.getValue();            
        }else{
            String paramLang = null;
            map<String, String> params = ApexPages.currentPage().getParameters();
            for(String key : params.keySet()){
                if(key.equalsIgnoreCase('uil') || key.equalsIgnoreCase('ulang')){
                    paramLang = String.escapeSingleQuotes(params.get(key));
                }
            }
            if(paramLang != null){
                userLanguage = paramLang;
            }else{
                userLanguage='en_US';
            }
        }
        return userLanguage;
    }

    public WebToCase__c w2c {get; set;}

    public Id selectedPlatformId {get; set;}
    public WebToCase_Platform__c selectedPlatform {get; set;}

    public Id selectedGameTitleId {get; set;}
    public WebToCase_GameTitle__c selectedGameTitle {get; set;}

    public Id selectedIssueTypeId {get; set;}
    public WebToCase_Type__c selectedIssueType {get; set;}

    public Id selectedIssueSubTypeId {get; set;}
    public WebToCase_SubType__c selectedSubType {get; set;}

    public WebToCase_SubType__c[] topIssueTypes {
        get{
            if(selectedGameTitleId == null && ApexPages.CurrentPage().getParameters().containsKey('w2cg')){
                selectedGameTitleId = ApexPages.CurrentPage().getParameters().get('w2cg');
            }
            if(selectedGameTitleId != null){
                topIssueTypes = [SELECT Description__c FROM WebToCase_SubType__c WHERE WebToCase_Type__r.WebToCase_GameTitle__c = :selectedGameTitleId 
                                 AND IsActive__c = TRUE AND Exclude_From_Top_Issues__c = false AND Click_throughs__c > 0
                                 ORDER BY Click_throughs__c DESC NULLS LAST LIMIT 5];
            }
            if(topIssueTypes == null) topIssueTypes = new WebToCase_SubType__c[]{};
            return topIssueTypes;
        } 
        set;
    }
    public WebToCase_Type__c[] otherIssueTypes {
        get{
            if(selectedGameTitleId == null && ApexPages.CurrentPage().getParameters().containsKey('w2cg')){
                selectedGameTitleId = ApexPages.CurrentPage().getParameters().get('w2cg');
            }
            if(selectedGameTitleId != null){
                otherIssueTypes = [SELECT Description__c, WebToCase_GameTitle__c, WebToCase_GameTitle__r.Title__c, 
                                     WebToCase_GameTitle__r.WebToCase_Platform__r.Name__c,
                                    (SELECT Description__c, WebToCase_Type__c
                                    FROM WebToCase_SubTypes__r WHERE IsActive__c = TRUE 
                                    ORDER BY Sort_Position__c ASC NULLS FIRST, CreatedDate DESC) 
                                   FROM WebToCase_Type__c WHERE WebToCase_GameTitle__c = :selectedGameTitleId 
                                    AND isActive__c = TRUE ORDER BY Sort_Position__c ASC NULLS FIRST, CreatedDate DESC];
            }
            if(otherIssueTypes == null) otherIssueTypes = new WebToCase_Type__c[]{};
            else{
                selectedGameTitle = otherIssueTypes[0].WebToCase_GameTitle__r;
                selectedPlatform = otherIssueTypes[0].WebToCase_GameTitle__r.WebToCase_Platform__r;
            }
            return otherIssueTypes;
        }
        set;
    }

    public Integer destinationPageNumber {get; set;}
    public pageReference navigate(){
        PageReference pr = Page.Contact_Us;
        //pr.setRedirect(false); //this would be "better" because it does not reinstantiate the conrollers, but it doesn't change the page
        pr.setRedirect(true);
        pr.getParameters().put('p',string.valueOf(destinationPageNumber));
        return pr;
    }

    public boolean getIsLoggedIn(){
        return UserInfo.getUserType()=='CSPLitePortal';
    }

    //page 0 methods & properties
    public void page0InitActions() {
        ATVI_GetClanLeaderInfo clanLeader =new ATVI_GetClanLeaderInfo();
        clanLeader.vContactid = theGamer.id;
        clanLeader.isClanleader();
        System.debug('is Clan Leader ...' + clanLeader.isClnLdr);
        if(clanLeader!=null) {            
            theGamer.isClan_Leader__c = clanLeader.isClnLdr;
        }
        //determine last played game and its Associated WebToCase_Type__r list
        //only tracking 4 games mw3, bo2, ghosts & aw
        if(theGamer.AW_Last_Played_Date__c != null ||
            theGamer.Ghosts_Last_Played_Date__c != null ||
            theGamer.BO2_Last_Played_Date__c != null ||
            theGamer.MW3_Last_Played_Date__c != null ||
            theGamer.BO_Last_Played_Date__c != null
            ) //Do we have a record of the gamer playing a title?
        {
            //yes, so, determine the platform they play on
            String[] foundPlatforms = new string[]{};
            if(theGamer.Platform_Xbox1__c)
                foundPlatforms.add('Xbox_One');
            if(theGamer.Platform_Xbox360__c)
                foundPlatforms.add('Xbox_360');
            if(theGamer.Platform_PS4__c)
                foundPlatforms.add('PS4');
            if(theGamer.Platform_PS3__c)
                foundPlatforms.add('PS3');
            if(theGamer.Platform_WiiU__c)
                foundPlatforms.add('WiiU');
            if(theGamer.Platform_PC__c)
                foundPlatforms.add('Windows');

            if(foundPlatforms.size() == 1){
                for(WebToCase_Platform__c plat : [SELECT Image__c FROM WebToCase_Platform__c 
                                                  WHERE WebToCase__r.Language__c = :userLanguage AND
                                                        IsActive__c = true AND Image__c != null ORDER BY Image__c])
                {
                    if(plat.Image__c.equalsIgnoreCase(foundPlatforms[0])){
                        this.selectedPlatformId = plat.Id;
                        break;
                    }
                }
            }

            if(this.selectedPlatformId != null){
                DateTime epoc = DateTime.newInstance(0L);
                if(theGamer.AW_Last_Played_Date__c == null)
                    theGamer.AW_Last_Played_Date__c = epoc;

                if(theGamer.Ghosts_Last_Played_Date__c == null)
                    theGamer.Ghosts_Last_Played_Date__c = epoc;

                if(theGamer.BO2_Last_Played_Date__c == null)
                    theGamer.BO2_Last_Played_Date__c = epoc;
                
                if(theGamer.MW3_Last_Played_Date__c == null)
                    theGamer.MW3_Last_Played_Date__c = epoc;

                if(theGamer.BO_Last_Played_Date__c == null)
                    theGamer.BO_Last_Played_Date__c = epoc;

            
                mostRecentlyPlayedGame = '';
                if(isFirstDateTimeTheMostRecent(theGamer.AW_Last_Played_Date__c,new DateTime[]{
                            theGamer.Ghosts_Last_Played_Date__c,
                            theGamer.BO2_Last_Played_Date__c,
                            theGamer.MW3_Last_Played_Date__c,
                            theGamer.BO_Last_Played_Date__c}))
                {
                    mostRecentlyPlayedGame = 'Call of Duty: Advanced Warfare';
                }else if(isFirstDateTimeTheMostRecent(theGamer.Ghosts_Last_Played_Date__c,new DateTime[]{
                            theGamer.AW_Last_Played_Date__c,
                            theGamer.BO2_Last_Played_Date__c,
                            theGamer.MW3_Last_Played_Date__c,
                            theGamer.BO_Last_Played_Date__c}))
                {
                    mostRecentlyPlayedGame = 'Call of Duty: Ghosts';
                }else if(isFirstDateTimeTheMostRecent(theGamer.BO2_Last_Played_Date__c,new DateTime[]{
                            theGamer.AW_Last_Played_Date__c,
                            theGamer.Ghosts_Last_Played_Date__c,
                            theGamer.MW3_Last_Played_Date__c,
                            theGamer.BO_Last_Played_Date__c}))
                {
                    mostRecentlyPlayedGame = 'Call of Duty: Black Ops II';
                }else if(isFirstDateTimeTheMostRecent(theGamer.MW3_Last_Played_Date__c,new DateTime[]{
                            theGamer.AW_Last_Played_Date__c,
                            theGamer.Ghosts_Last_Played_Date__c,
                            theGamer.BO2_Last_Played_Date__c,
                            theGamer.BO_Last_Played_Date__c}))
                {
                    mostRecentlyPlayedGame = 'Call of Duty: MW3';
                }else if(isFirstDateTimeTheMostRecent(theGamer.BO_Last_Played_Date__c,new DateTime[]{
                            theGamer.AW_Last_Played_Date__c,
                            theGamer.Ghosts_Last_Played_Date__c,
                            theGamer.BO2_Last_Played_Date__c,
                            theGamer.MW3_Last_Played_Date__c}))
                {
                    mostRecentlyPlayedGame = 'Call of Duty: Black Ops';
                }
                for(WebToCase_GameTitle__c gt : [SELECT Id FROM WebToCase_GameTitle__c WHERE Title__c = :mostRecentlyPlayedGame AND 
                                                 Language__c = :getUserLanguage() AND IsActive__c = TRUE AND 
                                                 WebToCase_Platform__c = :this.selectedPlatformId])
                {
                    this.selectedGameTitleId = gt.Id;
                }
            }
        }
    }

    public static boolean isFirstDateTimeTheMostRecent(DateTime theDateTimeToTest, DateTime[] theDateTimesToTest){
        boolean testResult = false; //default to true
        DateTime epoch = DateTime.newInstance(0L);
        if(theDateTimesToTest != null && //we've not been passed a null list
            theDateTimesToTest.size() > 1 && //we have more than one thing to test
            theDateTimeToTest > epoch //theDateTimeToTest is not null
        ){
            for(Integer i = 1; i < theDateTimesToTest.size(); i++){
                testResult = theDateTimeToTest > theDateTimesToTest[i];
                if(testResult == false)
                    break;
            }
        }
        return testResult;
    }
    
    //page 1 methods & properties
    public void page1InitActions() { //choose platform
        if(gc != NULL && gc != ''){
            Cookie lang = new Cookie('uLang',ApexPages.CurrentPage().getParameters().get('uil'),null,-1,false);
            ApexPages.CurrentPage().setCookies(new Cookie[]{lang});
            this.gcPage1InitActions();
        }else{

            for(WebToCase__c pw2c : [SELECT Id, Language__c, (SELECT Name, Name__c, Image__c FROM WebToCase_Platforms__r 
                                 WHERE isActive__c = TRUE ORDER BY Sort_Order__c ASC Nulls LAST) 
                        FROM WebToCase__c WHERE Language__c = :getUserLanguage() OR Language__c = 'en_US' 
                        ORDER BY Language__c LIMIT 2])
        {
            if(this.w2c == null){
                this.w2c = pw2c; //1st loop, always take the value
            }else if(this.w2c.Language__c == 'en_US'){
                this.w2c = pw2c; //2nd loop, if 1st was en_US, then this is the correct language it_IT / de_DE 
            }
        }
    }

    }

    public void gcPage1InitActions(){
        WebToCase_GameTitle__c[] wcg = [Select Id, ref_Platform__c From WebToCase_GameTitle__c Where Title__c =:gctitle];
        Set<string> platforms = new Set<string>();
        for(WebToCase_GameTitle__c wg : wcg){
            platforms.add(wg.ref_Platform__c);
        }
        for(WebToCase__c pw2c : [SELECT Id, Language__c, (SELECT Name, Name__c, Image__c FROM WebToCase_Platforms__r 
                                 WHERE isActive__c = TRUE And Name__c IN :platforms ORDER BY Sort_Order__c ASC Nulls LAST) 
                        FROM WebToCase__c WHERE Language__c = :getUserLanguage() OR Language__c = 'en_US' 
                        ORDER BY Language__c LIMIT 2])
        {
            if(this.w2c == null){
                this.w2c = pw2c; //1st loop, always take the value
            }else if(this.w2c.Language__c == 'en_US'){
                this.w2c = pw2c; //2nd loop, if 1st was en_US, then this is the correct language it_IT / de_DE 
            }
        }       

        
        
    }

    //page2 methods & properties
    public void page2InitActions() { //choose title
        //dostuff
        if(ApexPages.CurrentPage().getParameters().containsKey('w2cp'))
            selectedPlatformId = ApexPages.CurrentPage().getParameters().get('w2cp');
        selectedPlatform = [SELECT Image__c, Name__c, 
                            (SELECT Title__c, Image__c FROM WebToCase_GameTitles__r 
                             WHERE IsActive__c = true ORDER BY Priority__c ASC NULLS LAST) 
                            FROM WebToCase_Platform__c WHERE Id = :selectedPlatformId AND IsActive__c = true];

        if(gc != NULL && gc != ''){
            selectedGameTitleId = [Select Id From WebToCase_GameTitle__c Where WebToCase_Platform__c = :selectedPlatformId And Title__c = :gctitle].Id;
        }
    }
    //page 3 methods & properties
    public static boolean isBlankOrNull(String s){return s == '' || s == null;}
    public String getPlatformId(){
        string gameId = '';
        if(ApexPages.CurrentPage().getParameters().containsKey('w2cg')){
            gameId = ApexPages.CurrentPage().getParameters().get('w2cg');
        }else if(! isBlankOrNull(this.selectedGameTitleId)){
            gameId = this.selectedGameTitleId;
        }
        for(WebToCase_GameTitle__c game : [SELECT WebToCase_Platform__c FROM WebToCase_GameTitle__c WHERE Id = :gameId]){
            return game.WebToCase_Platform__c;
        }
        return '';
    }
    public void page3InitActions() {
        
    }
    public String mostRecentlyPlayedGame {get; set;}

    
    //page 4 methods & properties
    public KnowledgeArticleVersion kbArticle {get; set;}
    public Integer theArticleHelpCount {get; set;}
    string chatButtonId = '573U0000000GmaY'; //default COD chat
    public String getChatButtonId(){
        return chatButtonId;
    }
    public void page4InitActions() {
        Id subTypeId;
        if(ApexPages.CurrentPage().getParameters().containsKey('w2cs')){
            subTypeId = ApexPages.CurrentPage().getParameters().get('w2cs');
        }
        selectedSubType = [SELECT Description__c, Article_Id__c, WebToCase_Type__r.Id, WebToCase_Type__r.Description__c, 
                           WebToCase_Type__r.WebToCase_GameTitle__c FROM WebToCase_SubType__c WHERE Id = :subTypeId ];
        selectedIssueType = selectedSubType.WebToCase_Type__r;
        selectedGameTitle = [SELECT Image__c, Title__c, WebToCase_Platform__r.Image__c, WebToCase_Platform__r.Name__c,
                              WebToCase_Platform__r.ref_Language__c
                             FROM WebToCase_GameTitle__c WHERE Id = :selectedIssueType.WebToCase_GameTitle__c];
        selectedPlatform = selectedGameTitle.WebToCase_Platform__r;

        /*this was in case someone changed their language on step 4 they didn't continue to see the original language contact options*/
        /*deactivated because the system defaults to english, so we don't know that there will always be a language specific contanct channel*/
        //if(userLanguage != selectedPlatform.ref_Language__c){
          //  pageReference pr = Page.Contact_Us;
            //pr.getParameters().put('p','0');
            //pr.setRedirect(true);
            //return pr;
            //return new pageReference(Page.Contact_Us.getUrl());
        //}

        string articlelang = '';
        if(userLanguage.contains('en')) articlelang = 'en_US';
        else if(userLanguage.contains('fr')) articlelang = 'fr';
        else if(userLanguage.contains('it')) articlelang = 'it';
        else if(userLanguage.contains('de')) articlelang = 'de';
        else if(userLanguage.contains('pt')) articlelang = 'pt_BR';
        else if(userLanguage.contains('sv')) articlelang = 'sv';
        else if(userLanguage.contains('nl')) articlelang = 'nl_NL';
        else if(userLanguage.contains('es')) articlelang = 'es';
        else articleLang = 'en_US';

        if(selectedSubType.Article_Id__c != ''){
            theArticleHelpCount = 0;
            for(KnowledgeArticleVersion kb : [SELECT Title, Summary, KnowledgeArticleId, UrlName, Language FROM KnowledgeArticleVersion 
                                              WHERE Language = :articlelang AND publishStatus = 'online' AND 
                                              knowledgeArticleId = :selectedSubType.Article_Id__c LIMIT 1])
            {
                kbArticle = kb;
                ArticleHelpCount cls = new ArticleHelpCount();
                cls.theArtId = kb.Id;
                theArticleHelpCount = cls.getArticleHelpCount();
            }
        }
        //return null;
        /*
        Contact_Us_2014_User_Issue_Channel chatButtonCalc = new Contact_Us_2014_User_Issue_Channel(
            subTypeId,
            UserInfo.getUserId()
        );
        chatButtonId = chatButtonCalc.chatButtonId;
        */
    }

    public void getGcTitle(String pTitle){
        if(pTitle == 'AW'){
            gctitle = 'Call of Duty: Advanced Warfare';

        }

    }
    
}