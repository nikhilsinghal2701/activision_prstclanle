public without sharing class Contact_Us_2014_CallbackCancel {

	private final Id mysObjectId;

    public Contact_Us_2014_CallbackCancel(ApexPages.StandardController controller) {
        this();
    }
    public Contact_Us_2014_CallbackCancel(){
        try{
            this.mysObjectId = ApexPages.CurrentPage().getParameters().get('cid');
        }catch(Exception e){

        }
    }

    public void deleteCallback() {
        try{
            User u = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
            for(Case c : [SELECT Id, Callback_Request__c FROM Case WHERE Id = :mysObjectId AND ContactId = :u.ContactId]){
                c.Status = 'Cancelled by Customer';
                update c;
                delete [SELECT Id FROM Callback_Request__c WHERE Id = :c.Callback_Request__c];
            }
        }catch(Exception e){
            system.debug('Error == '+e);
        }
    }
}