@isTest
private class Contact_Us_2014_CallbackCancel_Test {
	
	@isTest static void test_method_1() {

		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		Callback_Request__c cr = new Callback_Request__c(Contact__c = testcontact.Id, Phone_Number__c = '1234567890', Requested_Callback_Time__c = system.now());
		insert cr;

		Case testcase = UnitTestHelper.generateTestCase();
		testcase.ContactId = testcontact.Id;
		testcase.Callback_Request__c = cr.Id;
		testcase.Comment_Not_Required__c = true;
		insert testcase;

		PageReference currentpage = Page.CallbackCancel;
		currentpage.getParameters().put('cid',testcase.Id);
		Test.setCurrentPage(currentpage);

		system.runAs(testportaluser){
			ApexPages.StandardController scon = new ApexPages.StandardController(cr);
			Contact_Us_2014_CallbackCancel cucc = new Contact_Us_2014_CallbackCancel(scon);
			cucc.deleteCallback();
			system.assertEquals([Select Count() From Callback_Request__c Where Id = :cr.Id],0);
		

			currentpage.getParameters().put('cid','000');
			ApexPages.StandardController scon2 = new ApexPages.StandardController(cr);
			Contact_Us_2014_CallbackCancel cucc2 = new Contact_Us_2014_CallbackCancel(scon2);
			
		}
		
	}
	
	
	
}