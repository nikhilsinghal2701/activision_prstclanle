public without sharing class Contact_Us_2014_Find_Chat_Case {
	public String getCaseId(){
		for(Case c : [SELECT Id FROM Case WHERE CreatedDate = TODAY AND CreatedById = :UserInfo.getUserId() ORDER BY CreatedDate DESC LIMIT 1]){
			return c.Id;
		}
		return '';
	}
}