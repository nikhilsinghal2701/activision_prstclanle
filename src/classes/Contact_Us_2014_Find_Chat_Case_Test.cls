@isTest
private class Contact_Us_2014_Find_Chat_Case_Test {
	
	@isTest static void test_method_one() {
		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		Contact testcontact2 = UnitTestHelper.generateTestContact();
		testcontact2.email = 'def@atvi.com';
		insert testcontact2;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc', testcontact.Id);
		insert testportaluser;

		User testportaluser2 = UnitTestHelper.getCustomerPortalUser('def', testcontact2.Id);
		insert testportaluser2;

		Case testcase = UnitTestHelper.generateTestCase();

		        
        system.runAs(testportaluser){
        	insert testcase;
        	Contact_Us_2014_Find_Chat_Case fcc = new Contact_Us_2014_Find_Chat_Case();
        	system.assertEquals(fcc.getCaseId(),testcase.Id);
        }

        system.runAs(testportaluser2){
        	Contact_Us_2014_Find_Chat_Case fcc2 = new Contact_Us_2014_Find_Chat_Case();
        	system.assertEquals(fcc2.getCaseId(),'');
        }


	}
	
	
	
}