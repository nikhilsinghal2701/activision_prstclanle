public with sharing class Contact_Us_2014_Form {
	WebToCase__c w2c = new WebToCase__c();
	public WebToCase__c getw2c(){
		return w2c;
	}
	public void setw2c(WebToCase__c pw2c){
		w2c = pw2c;
	}

	/* FAQ__kav[] relevantArticles = new FAQ__kav[]{};
	public FAQ__kav[] getRelevantArticles(){
		return relevantArticles;
	} */

	WebToCase_Form__c pForm = new WebToCase_Form__c(submitButtonEnabled__c = false, isActive__c = false);
	public WebToCase_Form__c getTheForm(){
		return pForm;
	}
	public void setTheForm(WebToCase_Form__c pTheForm){
		pForm = pTheForm;
	}
	WebToCase_FormField__c[] userInputFields = new WebToCase_FormField__c[]{};
	public WebToCase_FormField__c[] getUserInputFields(){
		return userInputFields;
	}
	//WebToCase_FormField__c[] dependentPicklists = new WebToCase_FormField__c[]{};
	//public WebToCase_FormField__c[] getDependentPicklists(){
	//	return dependentPicklists;
	//}
	public map<String, boolean> hasDependentPicklist {get; set;}
	public map<Id, SelectOption[]> fieldId2selectOption {get; set;}
	public map<String, String[]> fieldId_fieldVal2dependentSelectOption {get; set;}
	public map<String, String> fieldId_fieldVal2dependentLabel {get; set;}
	
	Id theGamerId = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()].ContactId;
	
	public string currentPicklistSelection {get; set;}
	public pageReference doNothing(){ 
		system.debug('currentPicklistSelection == '+currentPicklistSelection); 
		return null; 
	}
	public Contact_Us_2014_Form() {
		fieldId2selectOption = new map<Id, SelectOption[]>{null=>new selectOption[]{}};
		//fieldId_fieldVal2dependentSelectOption = new map<String, SelectOption[]>{null=>new selectOption[]{}};
		fieldId_fieldVal2dependentSelectOption = new map<String, String[]>{null=>new String[]{}};
		hasDependentPicklist = new map<String, boolean>{null=>false};
		fieldId_fieldVal2dependentLabel = new map<String, String>{null=>''};
		currentPicklistSelection = '';
		if(ApexPages.CurrentPage() != null && ApexPages.CurrentPage().getParameters().containsKey('w2cId'))
			w2c = [SELECT formAlertEnabled__c, formAlertMessage__c FROM WebToCase__c WHERE Id = :ApexPages.CurrentPage().getParameters().get('w2cId')];
		
		if(ApexPages.CurrentPage() != null && ApexPages.CurrentPage().getParameters().containsKey('w2cf')){
			for(WebToCase_Form__c form : [SELECT IsActive__c, Name__c, Queue_Id__c, ref_GameTitle__c, ref_Language__c, ref_Platform__c,
										  ref_SubType__c, ref_Type__c, RouteToCustomQueue__c, submitButtonEnabled__c, WebToCase_SubType__c,
										  	(SELECT FieldType__c, Checkbox__c, Text__c, TextArea__c, TextAreaLong__c, RichText__c, 
                                      		  		Label__c, PicklistValues__c, DependentPicklistValues__c, PicklistSlot__c, DateText__c, 
                                          		  	isRequired__c, Name, GTText__c, Prepopulate_From_and_Save_To__c, Dependent_Label__c,
                                          		  	Enable_Data_Update__c
                                          	FROM WebToCase_FormFields__r ORDER BY Name ASC) 
					 					  FROM WebToCase_Form__c WHERE IsActive__c = TRUE AND Id = :ApexPages.CurrentPage().getParameters().get('w2cf')])
			{
				pForm = form;
				/* //FGW: BRD stated "Show relevant articles above the form submit button" during UAT though relevance was defined as:
				// "Show the specific article we've added to the WebToCase SubType"  This article is displayed on page 4 of contact us
				// So, by the time the customer has arrived here they will have already been exposed to the article link
				//search for relevant articles
				String artLang = pForm.ref_Language__c;
				if(artLang.contains('en')) artLang = 'en_US';
			    else if(artLang.contains('fr')) artLang = 'fr';
			    else if(artLang.contains('it')) artLang = 'it';
			    else if(artLang.contains('de')) artLang = 'de';
			    else if(artLang.contains('pt')) artLang = 'pt_BR';
			    else if(artLang.contains('sv')) artLang = 'sv';
			    else if(artLang.contains('nl')) artLang = 'nl_NL';
			    else if(artLang.contains('es')) artLang = 'es';
			    string keywords = '*'+String.escapeSingleQuotes(pForm.ref_gameTitle__c)+'*'+String.escapeSingleQuotes(pForm.ref_Platform__c)+'*'+
			    				  String.escapeSingleQuotes(pForm.ref_SubType__c)+'*'+String.escapeSingleQuotes(pForm.ref_Type__c)+'*';
				string query = 'FIND \'' + keywords + '\'' + ' IN ALL FIELDs Returning ' +
	        				   'FAQ__kav(ID, Title, UrlName, Summary, Language WHERE PublishStatus = \'Online\' AND Language = \''+artLang+'\' '+
						       'LIMIT ' + 2 + ')';
			    try{
			    	system.debug('article search query == '+query);
	    			relevantArticles = (List<FAQ__kav>) search.query(query)[0];
	    		}catch(exception e){system.debug('ERROR == '+e);}

	    		*/
	    		//ensure that if we are prepopulating then we have the necessary data loaded
	    		String prepopulateDataQuery = '';
	    		Contact prepopulateContact;
	    		for(WebToCase_FormField__c field : pForm.WebToCase_FormFields__r){
	    			if(! isBlankOrNull(field.Prepopulate_From_and_Save_To__c)){
	    				prepopulateDataQuery += field.Prepopulate_From_and_Save_To__c + ',';
	    			}
	    		}
	    		if(prepopulateDataQuery != ''){
	    			try{
		    			User u = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
		    			if(u.ContactId != null){
		    				prepopulateContact = database.query('SELECT '+prepopulateDataQuery+'Id FROM Contact Contact WHERE Id = \''+u.ContactId+'\'');
		    			}
	    			}catch(Exception e){
	    				system.debug('ERROR == '+e);
	    			}
	    		}

	    		//do any prep work on fields
	    		for(WebToCase_FormField__c field : pForm.WebToCase_FormFields__r){
	    			hasDependentPicklist.put(field.Id, false);
					fieldId_fieldVal2dependentSelectOption.put(field.Id+'-', new String[]{}); //for when the page loads
					fieldId_fieldVal2dependentLabel.put(field.Id, '');


	    			if(! isBlankOrNull(field.PicklistValues__c)){
	    				selectOption[] picklistOptions = new selectOption[]{new selectOption('','')};
	    				for(String val : field.PicklistValues__c.split(',')){
	    					picklistOptions.add(new selectOption(val.trim(), val.trim()));
	    				}
	    				fieldId2selectOption.put(field.Id, picklistOptions);
	    			}
	    			//prepopulate
	    			if(prepopulateContact != null && field.Prepopulate_From_and_Save_To__c != ''){
	    				try{
		    				if(field.FieldType__c == 'Text' || field.FieldType__c == 'Picklist')
					          	field.Text__c = (String) prepopulateContact.get(field.Prepopulate_From_and_Save_To__c.split('\\.')[1]);
					        else if(field.FieldType__c == 'TextArea')
				          		field.TextArea__c = (String) prepopulateContact.get(field.Prepopulate_From_and_Save_To__c.split('\\.')[1]);
					        else if(field.FieldType__c == 'TextAreaLong')
			            		field.TextAreaLong__c = (String) prepopulateContact.get(field.Prepopulate_From_and_Save_To__c.split('\\.')[1]);
	            		}catch(Exception e){
	            			system.debug('Prepopulate error == '+e);
	            		}
	    			}
	    			userInputFields.add(field);
	    			//data format is "value_from_first_picklist_1:sub_value_1,sub_value_2,sub_value3;value_from_first_picklist_2:sub_value_1,sub_value_2;"
	    			if(! isBlankOrNull(field.DependentPicklistValues__c)){
	    				if(field.DependentPicklistValues__c.contains(';') && field.DependentPicklistValues__c.contains(':')){
		    				for(String chunk : field.DependentPicklistValues__c.split(';')){
		    					String[] picklistOptions = new String[]{};
		    					String[] currentChunk = chunk.split(':');
		    					for(String val : currentChunk[1].split(',')){
		    						//picklistOptions.add(new selectOption(val.trim(), val.trim()));
		    						picklistOptions.add(val);
		    					}
		    					fieldId_fieldVal2dependentSelectOption.put(field.Id+'-'+currentChunk[0], picklistOptions);
		    					//fieldId_fieldVal2dependentSelectOption.put(field.Id+'-', new selectOption[]{}); //for when the page loads
		    					fieldId_fieldVal2dependentLabel.put(field.Id, field.Dependent_Label__c);
		    					//dependentPicklists.put(field.Id, new WebToCase_FormField__c(RichText__c = field.Id + '-' + currentChunk[0], FieldType__c = 'Picklist'));
		    				}
		    				hasDependentPicklist.put(field.Id, true);
		    				field.DependentPicklistValues__c = '';
	    				}
	    			}
	    		}

	    		//web-to-case display prepped
			}
		}
		for(String key : fieldId_fieldVal2dependentSelectOption.keySet())
			system.debug('key == '+key+'; value == '+fieldId_fieldVal2dependentSelectOption.get(key));
	}

	Case currentCase;
	public Case getCurrentCase(){
		return currentCase;
	}
	
	WebToCase_Platform__c w2c_p;
	WebToCase_GameTitle__c w2c_g;
	WebToCase_Type__c w2c_t;
	WebToCase_SubType__c w2c_st;
	public pageReference submitForm(){
		try{
			if(w2c_p == null || w2c_g == null || w2c_t == null || w2c_st == null){
				w2c_st = [SELECT WebToCase_Type__c, SubTypeMap__c, Description__c, Article_Id__c FROM WebToCase_SubType__c WHERE Id = :pForm.WebToCase_SubType__c];
				w2c_t = [SELECT WebToCase_GameTitle__c, Description__c, TypeMap__c FROM WebToCase_Type__c WHERE Id = :w2c_st.WebToCase_Type__c];
				w2c_g = [SELECT WebToCase_Platform__c, Public_Product__c, Title__c FROM WebToCase_GameTitle__c WHERE Id = :w2c_t.WebToCase_GameTitle__c];
				w2c_p = [SELECT WebToCase__c, Name__c FROM WebToCase_Platform__c WHERE Id = :w2c_g.WebToCase_Platform__c];
			}
		
			currentCase = new Case();
		    currentCase.RecordTypeId = [SELECT id FROM RecordType WHERE Name = 'General Support' LIMIT 1].id;
		    currentCase.ContactId = theGamerId;
		    currentCase.Type = w2c_t.TypeMap__c;
		    currentCase.Sub_Type__c = w2c_st.SubTypeMap__c;
		    currentCase.Status = 'Open'; 
		    currentCase.Product_Effected__c = w2c_g.Public_Product__c;
		    currentCase.Origin = 'Web To Case';
		    currentCase.W2C_Origin_Language__c = pForm.ref_Language__c;
		    currentCase.W2C_Type__c = w2c_t.Description__c;
		    currentCase.W2C_SubType__c = w2c_st.Description__c;
		    currentCase.subject =  w2c_g.Title__c + ' (' + w2c_p.Name__c + ')' + ' : ' + w2c_t.Description__c + ' (' + w2c_st.Description__c + ')';    
		    currentCase.WebToCase_Form__c = pForm.Id;
		    currentCase = Contact_Us_Controller.addDispositionsToCase(
		        currentCase, 
		        w2c_p.Name__c,
		        w2c_g.Title__c,
		        w2c_t.typeMap__c,
		        w2c_st.subTypeMap__c
	        );
		    system.debug('CASE SETUP');
		    
			system.debug('CUSTOM FORM ENABLED SETUP');
    	  	
    	  	Contact saveToContact = new Contact();
	      	for(WebToCase_FormField__c fField : userInputFields){
	        	system.debug('FIELD TYPE' + fField.FieldType__c);
	        	system.debug('FIELD REQ' + fField.isRequired__c);
		        if(fField.FieldType__c == 'Checkbox'){
		          	currentCase.description += fField.Label__c + ': ' + fField.Checkbox__c + '\n';
		        }
		        else if(fField.FieldType__c == 'Text'){
	          		currentCase.description += fField.Label__c + ': ' + fField.Text__c.remove(':').normalizeSpace() + '\n';
		          	if(fField.Enable_Data_Update__c){
	        			saveToContact.put(fField.Prepopulate_From_and_Save_To__c.split('\\.')[1], fField.Text__c.remove(':').normalizeSpace());
    				}
		        }
		        else if(fField.FieldType__c == 'TextArea'){
		          	currentCase.description += fField.Label__c + ': ' + fField.TextArea__c.remove(':') + '\n';
		          	if(fField.Enable_Data_Update__c){
	        			saveToContact.put(fField.Prepopulate_From_and_Save_To__c.split('\\.')[1], fField.TextArea__c.remove(':').normalizeSpace());
    				}
		        }
		        else if(fField.FieldType__c == 'TextAreaLong'){
					if(! isBlankOrNull(fField.TextAreaLong__c)){
		            	currentCase.description += fField.Label__c + ': ' + fField.TextAreaLong__c.remove(':') + '\n';  //WHY U SO NULLL
		            	if(fField.Enable_Data_Update__c){
		        			saveToContact.put(fField.Prepopulate_From_and_Save_To__c.split('\\.')[1], fField.TextAreaLong__c.remove(':').normalizeSpace());
	    				}
		          	}
		        }
		        else if(fField.FieldType__c == 'Picklist'){
	          		currentCase.description += fField.Label__c + ': ' + fField.Text__c.remove(':') + '\n';
	          		if(fField.Enable_Data_Update__c){
        				saveToContact.put(fField.Prepopulate_From_and_Save_To__c.split('\\.')[1], fField.Text__c.remove(':').normalizeSpace());
    				}
		          	if(! isBlankOrNull(fField.DependentPicklistValues__c) ){
		          		currentCase.description += fField.Dependent_Label__c + ': ' + fField.DependentPicklistValues__c.remove(':') + '\n';
		          	}
		        }
		        else if(fField.FieldType__c == 'DateText'){
		          	currentCase.description += fField.Label__c + ': ' + fField.DateText__c.remove(':') + '\n';
		        }
		        else if(fField.FieldType__c == 'GT Picklist'){
		          	currentCase.description += fField.Label__c + ': ' + fField.GTText__c.remove(':') + '\n';
		        }
	      	}
		    insert currentCase;    
		    
		    //Create Article:
	      	if(! isBlankOrNull(w2c_st.Article_Id__c)){
	        	CaseArticle article = new CaseArticle();
	        	article.KnowledgeArticleId = w2c_st.Article_Id__c;
	        	article.CaseId = currentCase.id;
	        	insert article;
	        	system.debug('Article Created: ' + w2c_st.Article_Id__c);
	      	}
		    
		    //Get Case Number:
		    for(Case c : [SELECT CaseNumber FROM Case WHERE id = :currentCase.id]){
		    	currentCase = c;
		    }
		    
		    system.debug('CUSTOM FORM QUEUE: ' + pForm.Queue_ID__c);
		    if(pForm.Queue_ID__c != null){
		        currentCase.OwnerId = pForm.Queue_ID__c;
		        currentCase.W2C_Route_To_Custom_Queue__c = true;
		    }else{ //Hardcode Queues:
		      	if(pForm.ref_Language__c.contains('en') && w2c_g.Title__c.contains('Call of Duty'))
		    	    currentCase.OwnerID = '00GU0000000gYws';
	    	  	else if((pForm.ref_Language__c == 'en_US') && (w2c_g.Title__c.contains('Skylander')))
			        currentCase.OwnerID = '00GU0000000gYwx';
	    	  	else if(pForm.ref_Language__c == 'en_US')
			        currentCase.OwnerID = '00GU0000000gYwv';
		      	else if(pForm.ref_Language__c == 'nl_NL')
			        currentCase.OwnerID = '00GU0000000gYwk';
		      	else if(pForm.ref_Language__c == 'en_GB')
			        currentCase.OwnerID = '00GU0000000gYwl';
		      	else if(pForm.ref_Language__c == 'en_AU')
			        currentCase.OwnerID = '00GU0000000gYwl';
		      	else if(pForm.ref_Language__c == 'en_NO')
			        currentCase.OwnerID = '00GU0000000gYwl';
		      	else if(pForm.ref_Language__c == 'en_FI')
			        currentCase.OwnerID = '00GU0000000gYwl';
		      	else if(pForm.ref_Language__c == 'fr_FR')
			        currentCase.OwnerID = '00GU0000000gYwm';
		      	else if(pForm.ref_Language__c == 'de_DE')
			        currentCase.OwnerID = '00GU0000000gYwn';
		      	else if(pForm.ref_Language__c == 'it_IT')
			        currentCase.OwnerID = '00GU0000000gYwo';
		      	else if(pForm.ref_Language__c == 'pt_BR')
			        currentCase.OwnerID = '00GU0000000gYwp';
		      	else if(pForm.ref_Language__c == 'es_ES')
			        currentCase.OwnerID = '00GU0000000gYwq';
		      	else if(pForm.ref_Language__c == 'sv_SE')
		        	currentCase.OwnerID = '00GU0000000gYwr';
		      	else
		        	currentCase.OwnerID = '00GU0000000gYwz';
		      	currentCase.W2C_Route_To_Custom_Queue__c = false;
		    }
		    update currentCase;  
		}catch(Exception e){
			system.debug('ERROR == '+e);
		}
		return null;
	}
	public static boolean isBlankOrNull(String s){ return s == '' || s == null; }
}