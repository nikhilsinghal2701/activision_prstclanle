public with sharing class Contact_Us_2014_Language {

	String userLanguage = 'en_US';
    public String getUserLanguage(){
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            userLanguage = tempCookie.getValue();            
        }else{
            String paramLang = null;
            map<String, String> params = ApexPages.currentPage().getParameters();
            for(String key : params.keySet()){
                if(key.equalsIgnoreCase('uil') || key.equalsIgnoreCase('ulang')){
                    paramLang = String.escapeSingleQuotes(params.get(key));
                }
            }
            if(paramLang != null){
                userLanguage = paramLang;
            }else{
                userLanguage='en_US';
            }
        }
        return userLanguage;
    }
    public boolean getIsLoggedIn(){
    	return UserInfo.getUserType()=='CSPLitePortal';
    }
}