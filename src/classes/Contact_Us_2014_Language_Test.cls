@isTest
private class Contact_Us_2014_Language_Test {
	
	@isTest static void test_method_1() {
		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc', testcontact.Id);
		insert testportaluser;

		PageReference currentpage = Page.Contact_Us_2014;
		Cookie lang = new Cookie('uLang','en_GB',null,-1,false);
		currentpage.setCookies(new Cookie[]{lang});
		Test.setCurrentPage(currentpage);

		system.runAs(testportaluser){
			Contact_Us_2014_Language cul = new Contact_Us_2014_Language();
			system.assertEquals(cul.getUserLanguage(),'en_GB');
			system.assertEquals(cul.getIsLoggedIn(),true);

		}
	}

	@isTest static void test_method_2() {
		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc', testcontact.Id);
		insert testportaluser;

		PageReference currentpage = Page.Contact_Us_2014;
		currentpage.getParameters().put('uLang','en_AU');
		Test.setCurrentPage(currentpage);

		system.runAs(testportaluser){
			Contact_Us_2014_Language cul = new Contact_Us_2014_Language();
			system.assertEquals(cul.getUserLanguage(),'en_AU');
			system.assertEquals(cul.getIsLoggedIn(),true);

		}
	}

	@isTest static void test_method_3() {
		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc', testcontact.Id);
		insert testportaluser;

		PageReference currentpage = Page.Contact_Us_2014;
		Test.setCurrentPage(currentpage);

		system.runAs(testportaluser){
			Contact_Us_2014_Language cul = new Contact_Us_2014_Language();
			system.assertEquals(cul.getUserLanguage(),'en_US');
			system.assertEquals(cul.getIsLoggedIn(),true);

		}
	}
	
	
	
}