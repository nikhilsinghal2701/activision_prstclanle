public with sharing class Contact_Us_2014_Phone {
	public Case currentCase {get; set;}
	public Contact_Us_2014_Phone() {
		
	}
	public void createCase(){
		User u = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
		currentCase = new Case();
        currentCase.RecordTypeId = [SELECT id FROM RecordType WHERE Name = 'General Support' LIMIT 1].Id;
        currentCase.ContactId = u.contactid;
        currentCase.Type = ApexPages.currentPage().getParameters().get('i2');
        currentCase.Sub_Type__c = ApexPages.currentPage().getParameters().get('s');
        currentCase.Status = 'Open'; 
        currentCase.Product_Effected__c = ApexPages.currentPage().getParameters().get('pid');
        currentCase.W2C_Origin_Language__c = ApexPages.currentPage().getParameters().get('l');
        currentCase.W2C_Type__c = ApexPages.currentPage().getParameters().get('i');
        currentCase.W2C_SubType__c = ApexPages.currentPage().getParameters().get('s');
        currentCase.subject =  ApexPages.currentPage().getParameters().get('g') + 
        	' (' + ApexPages.currentPage().getParameters().get('p') + ')' + ' : ' + 
        	ApexPages.currentPage().getParameters().get('i') + ' (' + ApexPages.currentPage().getParameters().get('s') + ')';

        currentCase.description = ApexPages.currentPage().getParameters().get('m');
        currentCase.Origin = 'Customer Created Phone Ticket';
        currentCase.Comment_not_required__c = true;
        currentCase.Case_Not_Editable_by_Gamer__c = true;
        currentCase.Medallia_Survey_Not_Required__c = true;
        currentCase = Contact_Us_Controller.addDispositionsToCase(
            currentCase, 
            ApexPages.currentPage().getParameters().get('p'),
            ApexPages.currentPage().getParameters().get('g'),
            ApexPages.currentPage().getParameters().get('i2'),
            ApexPages.currentPage().getParameters().get('s2')
        );
        insert currentCase;
        currentCase = [select caseNumber from case where id=:currentCase.id limit 1];
        
	}
}