@isTest
private class Contact_Us_2014_Phone_Test {
	
	@isTest static void test_method_1() {
		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		Case testcase = UnitTestHelper.generateTestCase();
		testcase.Comment_Not_Required__c = true;
		insert testcase;

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		Case tempcase;
		
		PageReference currentpage = Page.Contact_Us_2014;
		currentpage.getParameters().put('i2','Test Type');
		currentpage.getParameters().put('s','Test SubType');
		currentpage.getParameters().put('pid',testproduct.Id);
		currentpage.getParameters().put('l','en_US');
		currentpage.getParameters().put('i','Test Type');
		currentpage.getParameters().put('s','Test SubType');
		currentpage.getParameters().put('g','Test Game');
		currentpage.getParameters().put('p','Test Platform');
		currentpage.getParameters().put('m','Test Description');
		currentpage.getParameters().put('s2','Test SubType 2');
		Test.setCurrentPage(currentpage);

		system.runAs(testportaluser){
			Contact_Us_2014_Phone cup = new Contact_Us_2014_Phone();
			cup.createCase();
			tempcase = cup.currentCase;

		}

        Case ccase = [Select Id,Type,Sub_Type__c,Product_Effected__r.Id,W2C_Origin_Language__c,W2C_Type__c,W2C_SubType__c,Subject,Description From Case Where ContactId = :testcontact.Id];
        system.assertEquals(tempcase.Id,ccase.Id);
        system.assertEquals(ccase.Type,'Test Type');
        system.assertEquals(ccase.Sub_Type__c,'Test SubType');
        system.assertEquals(ccase.Product_Effected__r.Id,testproduct.Id);
        system.assertEquals(ccase.W2C_Origin_Language__c,'en_US');
        system.assertEquals(ccase.W2C_Type__c,'Test Type');
        system.assertEquals(ccase.W2C_SubType__c,'Test SubType');
        system.assertEquals(ccase.Subject,'Test Game (Test Platform) : Test Type (Test SubType)');
        system.assertEquals(ccase.Description,'Test Description');


	}
	
	
	
}