@isTest
private class Contact_Us_2014_Test {

	@isTest static void test_method_1A() {
		
		Contact testcontact = UnitTestHelper.generateTestContact();
		testcontact.Ghosts_Last_Played_Date__c = system.today();
		testcontact.Platform_Xbox1__c = true;
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US');
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(Image__c = 'Xbox_One', WebToCase__c = w2c1.Id, Name__c = 'Test', isActive__c = true);
		insert w2cp1;

		WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Call of Duty: Ghosts');
		insert w2cg1;

		PageReference currentpage = Page.Contact_Us_2014;
		Cookie langcookie = new Cookie('uLang','en_US',null,-1,false);
		currentpage.setCookies(new Cookie[]{langcookie});
		Test.setCurrentPage(currentpage);
		

		System.runAs(testportaluser){
		Contact_Us_2014 cu1 = new Contact_Us_2014();
		cu1.destinationPageNumber = 2;
		PageReference p1 = cu1.navigate();
		system.assertEquals(p1.getParameters().get('p'),'2');
		system.assertEquals(cu1.getIsLoggedIn(),true);

		}

		


	}

	 @isTest static void test_method_1B() {
		
		Contact testcontact = UnitTestHelper.generateTestContact();
		testcontact.Ghosts_Last_Played_Date__c = system.today();
		testcontact.BO2_Last_Played_Date__c = system.today();
		testcontact.MW3_Last_Played_Date__c = system.today();
		testcontact.BO_Last_Played_Date__c = system.today();
		testcontact.Platform_Xbox1__c = true;
		testcontact.Platform_Xbox360__c = true;
		testcontact.Platform_PS4__c = true;
		testcontact.Platform_PS3__c = true;
		testcontact.Platform_WiiU__c = true;
		testcontact.Platform_PC__c = true;
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		PageReference currentpage = Page.Contact_Us_2014;
		currentpage.getParameters().put('uLang','en_US');		
		Test.setCurrentPage(currentpage);

		System.runAs(testportaluser){
		Contact_Us_2014 cu1 = new Contact_Us_2014();
		}

		


	} 

	 @isTest static void test_method_1C() {
		
		Contact testcontact = UnitTestHelper.generateTestContact();
		testcontact.AW_Last_Played_Date__c = system.today();
		testcontact.Platform_Xbox1__c = true;
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US');
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(Image__c = 'Xbox_One', WebToCase__c = w2c1.Id, Name__c = 'Test', isActive__c = true);
		insert w2cp1;

		WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Call of Duty: Advanced Warfare');
		insert w2cg1;

		PageReference currentpage = Page.Contact_Us_2014;
		Cookie langcookie = new Cookie('uLang','en_US',null,-1,false);
		currentpage.setCookies(new Cookie[]{langcookie});
		Test.setCurrentPage(currentpage);

		System.runAs(testportaluser){
		Contact_Us_2014 cu1 = new Contact_Us_2014();
		}

		


	}

	@isTest static void test_method_1D() {
		
		Contact testcontact = UnitTestHelper.generateTestContact();
		testcontact.BO2_Last_Played_Date__c = system.today();
		testcontact.Platform_Xbox1__c = true;
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US');
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(Image__c = 'Xbox_One', WebToCase__c = w2c1.Id, Name__c = 'Test', isActive__c = true);
		insert w2cp1;

		WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Call of Duty: Black Ops II');
		insert w2cg1;

		PageReference currentpage = Page.Contact_Us_2014;
		Cookie langcookie = new Cookie('uLang','en_US',null,-1,false);
		currentpage.setCookies(new Cookie[]{langcookie});
		Test.setCurrentPage(currentpage);

		System.runAs(testportaluser){
		Contact_Us_2014 cu1 = new Contact_Us_2014();
		}

		


	}

	@isTest static void test_method_1E() {
		
		Contact testcontact = UnitTestHelper.generateTestContact();
		testcontact.MW3_Last_Played_Date__c = system.today();
		testcontact.Platform_Xbox1__c = true;
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US');
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(Image__c = 'Xbox_One', WebToCase__c = w2c1.Id, Name__c = 'Test', isActive__c = true);
		insert w2cp1;

		WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Call of Duty: MW3');
		insert w2cg1;

		PageReference currentpage = Page.Contact_Us_2014;
		Cookie langcookie = new Cookie('uLang','en_US',null,-1,false);
		currentpage.setCookies(new Cookie[]{langcookie});
		Test.setCurrentPage(currentpage);

		System.runAs(testportaluser){
		Contact_Us_2014 cu1 = new Contact_Us_2014();
		}

		


	}

	@isTest static void test_method_1F() {
		
		Contact testcontact = UnitTestHelper.generateTestContact();
		testcontact.BO_Last_Played_Date__c = system.today();
		testcontact.Platform_Xbox1__c = true;
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US');
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(Image__c = 'Xbox_One', WebToCase__c = w2c1.Id, Name__c = 'Test', isActive__c = true);
		insert w2cp1;

		WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Call of Duty: Black Ops');
		insert w2cg1;

		PageReference currentpage = Page.Contact_Us_2014;
		Cookie langcookie = new Cookie('uLang','en_US',null,-1,false);
		currentpage.setCookies(new Cookie[]{langcookie});
		Test.setCurrentPage(currentpage);

		System.runAs(testportaluser){
		Contact_Us_2014 cu1 = new Contact_Us_2014();
		}

		


	}

	
	
	@isTest static void test_method_2() {
		
		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'it_IT');
		insert w2c1;

		WebToCase__c w2c2 = new WebToCase__c(Language__c = 'en_US');
		insert w2c2;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(WebToCase__c = w2c1.Id, Name__c = 'Test');
		insert w2cp1;

		WebToCase_Platform__c w2cp2 = new WebToCase_Platform__c(WebToCase__c = w2c2.Id, Name__c = 'Test');
		insert w2cp2;

		PageReference currentpage = Page.Contact_Us_2014;
		currentpage.getParameters().put('w2c','1');
		currentpage.getParameters().put('uLang','it_IT');
		Test.setCurrentPage(currentpage);

		System.runAs(testportaluser){
		Contact_Us_2014 cu1 = new Contact_Us_2014();
		}

		


	}

	@isTest static void test_method_3() {
		
		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US');
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(WebToCase__c = w2c1.Id, Name__c = 'Test Platform', isActive__c = true);
		insert w2cp1;

		WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Test Title');
		insert w2cg1;


		PageReference currentpage = Page.Contact_Us_2014;
		currentpage.getParameters().put('w2cp',w2cp1.Id);
		Test.setCurrentPage(currentpage);

		ATVICustomerPortalController acon = new ATVICustomerPortalController();
		Contact_Us_2014 cu2 = new Contact_Us_2014(acon);

		System.runAs(testportaluser){
		Contact_Us_2014 cu1 = new Contact_Us_2014(acon);
		cu1.selectedGameTitleId = w2cg1.Id;
		system.assertEquals(cu1.getPlatformId(),w2cp1.Id);
		}

		


	}

	@isTest static void test_method_4() {
		
		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US');
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(WebToCase__c = w2c1.Id, Name__c = 'Test Platform', isActive__c = true);
		insert w2cp1;

		WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Test Title');
		insert w2cg1;

		WebToCase_Type__c w2ct1 = new WebToCase_Type__c(WebToCase_GameTitle__c = w2cg1.Id, Description__c = 'Test Type', isActive__c = true);
		insert w2ct1;

		WebToCase_SubType__c w2cst1 = new WebToCase_SubType__c(Description__c = 'Test SubType', WebToCase_Type__c = w2ct1.Id, isActive__c = true, Exclude_From_Top_Issues__c = false, Click_throughs__c = 5);
		insert w2cst1;


		PageReference currentpage = Page.Contact_Us_2014;
		currentpage.getParameters().put('w2cg',w2cg1.Id);
		Test.setCurrentPage(currentpage);

		ApexPages.StandardController scon = new ApexPages.StandardController(testcontact);
		Contact_Us_2014 cu2 = new Contact_Us_2014(scon);
        
        System.runAs(testportaluser){  
		Contact_Us_2014 cu1 = new Contact_Us_2014(scon);
        system.assertEquals(cu1.topIssueTypes.size(),1);
        system.assertEquals(cu1.topIssueTypes[0].Description__c,'Test SubType');
        system.assertEquals(cu1.otherIssueTypes.size(),1);
        system.assertEquals(cu1.otherIssueTypes[0].Description__c,'Test Type');
        system.assertEquals(cu1.getPlatformId(),w2cp1.Id);
		}

		


	}

	@isTest static void test_method_5() {
		
		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US');
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(WebToCase__c = w2c1.Id, Name__c = 'Test');
		insert w2cp1;

		PageReference currentpage = Page.Contact_Us_2014;
		currentpage.getParameters().put('w2ct','1');
		Test.setCurrentPage(currentpage);

		
		Contact_Us_2014 cu1 = new Contact_Us_2014();
		system.assertEquals(cu1.getPlatformId(),'');
		

		


	}

	@isTest static void test_method_6() {
		
		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		FAQ__kav article = new FAQ__kav(
            Title = 'Test',
            Summary = 'Test',
            URLName = 'abc123test',
            Language = 'en_US');
        insert article;
        article = [Select KnowledgeArticleID From FAQ__kav Where ID = :article.Id];
        KbManagement.PublishingService.publishArticle(article.KnowledgeArticleID,true);

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US');
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(WebToCase__c = w2c1.Id, Name__c = 'Test Platform', isActive__c = true);
		insert w2cp1;

		WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Test Title');
		insert w2cg1;

		WebToCase_Type__c w2ct1 = new WebToCase_Type__c(WebToCase_GameTitle__c = w2cg1.Id, Description__c = 'Test Type');
		insert w2ct1;

		WebToCase_SubType__c w2cst1 = new WebToCase_SubType__c(WebToCase_Type__c = w2ct1.Id, Article_Id__c = article.KnowledgeArticleID);
		insert w2cst1;



		PageReference currentpage = Page.Contact_Us_2014;
		currentpage.getParameters().put('w2cs', w2cst1.Id);
		currentpage.getParameters().put('uLang', 'al_AL');		
		Test.setCurrentPage(currentpage);

		
		Contact_Us_2014 cu1 = new Contact_Us_2014();
		system.assertEquals(cu1.getChatButtonId(),'573U0000000GmaY');
		

		


	}

	@isTest static void test_method_7() {
		
		Contact testcontact = UnitTestHelper.generateTestContact();
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc',testcontact.Id);
		insert testportaluser;

		FAQ__kav article = new FAQ__kav(
            Title = 'Test',
            Summary = 'Test',
            URLName = 'abc123test',
            Language = 'en_US');
        insert article;
        article = [Select KnowledgeArticleID From FAQ__kav Where ID = :article.Id];
        KbManagement.PublishingService.publishArticle(article.KnowledgeArticleID,true);

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US');
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(WebToCase__c = w2c1.Id, Name__c = 'Test Platform', isActive__c = true);
		insert w2cp1;

		WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Call of Duty: Advanced Warfare');
		insert w2cg1;

		WebToCase_Type__c w2ct1 = new WebToCase_Type__c(WebToCase_GameTitle__c = w2cg1.Id, Description__c = 'Test Type');
		insert w2ct1;

		WebToCase_SubType__c w2cst1 = new WebToCase_SubType__c(WebToCase_Type__c = w2ct1.Id, Article_Id__c = article.KnowledgeArticleID);
		insert w2cst1;



		PageReference currentpage = Page.Contact_Us_2014;
		currentpage.getParameters().put('w2c', w2c1.Id);
		currentpage.getParameters().put('uLang', 'en_US');
		currentpage.getParameters().put('gc','AW');		
		Test.setCurrentPage(currentpage);

		
		Contact_Us_2014 cu1 = new Contact_Us_2014();
		
		


	}
	
	
	
}