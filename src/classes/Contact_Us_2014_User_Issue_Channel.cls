public class Contact_Us_2014_User_Issue_Channel {
    //Whether or not the channel is enabled for this user or user and issue
    public boolean allowPhoneCall {get; set;}
    public boolean skipPhoneForm {get; set;}
    public String phoneMessaging {get; set;}
    
    public boolean allowForm {get; set;}
    public String formId {get; set;}
    public boolean formOverRequestLimit {get; set;}
    public boolean allowForums {get; set;}
    public String forumUrl {get; set;}
    public boolean allowFacebook {get; set;}
    public boolean allowChat {get; set;}
    public boolean chatWindowOpen {get; set;}
    public boolean allowAmbassadorChat {get; set;}
    public boolean allowScheduleACall {get; set;}
    public boolean allowTwitter {get; set;}
    public String twitterHashTag {get; set;}

    public String chatButtonId {get; set;}
    public string[] contactOptionsOrder {get; set;}
    Decimal supportLevelNumber = 30;
    
    contact pTheGamer = new Contact();
   @RemoteAction
   public static boolean customerSegmentation()
    {   
        decimal prevScore;
        boolean prevLeaderVal,flgChanged;
        try{
            contact thecontact = [select contact.id,contact.Gamification_Score__c,contact.isClan_Leader__c from User WHERE Id = :UserInfo.getUserId()].Contact;
            
            prevScore=thecontact.Gamification_Score__c;
            prevLeaderVal=thecontact.isClan_Leader__c;
            
            
            Atvi_GetBunchballGamficationScore cls = new Atvi_GetBunchballGamficationScore();
            cls.vcontactid=thecontact.id;
            cls.getScore(); 

            ATVI_GetClanLeaderInfo cls1 = new ATVI_GetClanLeaderInfo();
            if(prevLeaderVal==false){
                cls1.vcontactid=thecontact.Id;
                cls1.isClanleader();
                if(cls1!=null)                      
                    thecontact.isClan_Leader__c=cls1.isClnLdr;
            }
            
           
            
            //if(thecontact.Gamification_Score__c!=cls.getScore()||thecontact.isClan_Leader__c !=
            
            
            if(cls!=null&&cls.thepoints!=null&&cls.thepoints.nitro!=null&&cls.thepoints.nitro.balance!=null&&cls.thepoints.nitro.balance.lifetimebalance!=null)                      
            thecontact.Gamification_Score__c=decimal.valueof(cls.thepoints.nitro.balance.lifetimebalance);                      
            
            
            
            system.debug('cs>>>'+thecontact);
            
            update thecontact;
            
            if(prevScore!=thecontact.Gamification_Score__c || prevLeaderVal!=thecontact.isClan_Leader__c)
                flgChanged=true;
            else
                flgChanged=false;
            
        }
        catch(exception e){system.debug('error in customer segmentation >>>'+e);flgChanged=false;}
        
        return flgChanged;
    }
    

    public Contact getGamer(){
        try{
            pTheGamer = [SELECT id,Contact.Id, Contact.Name, Contact.Email FROM User WHERE Id = :UserInfo.getUserId()].Contact;
        }catch(Exception e){}
        return pTheGamer;
    }
    
    public Contact getTheGamer(){
        try{
            if(pTheGamer.id == null) getGamer();
        }catch(Exception e){
            getGamer();
        }
        return pTheGamer;
    }

    public boolean getIsLoggedIn(){
        System.debug('User Type Is '+UserInfo.getUserType());
        return UserInfo.getUserType()=='CSPLitePortal';
    }

    public Contact_Us_2014_User_Issue_Channel(Id webToCaseSubTypeId, Id gamersUserId){ //for other controllers
        init(webToCaseSubTypeId, gamersUserId);
        User u = [SELECT ContactId, Contact.Support_Level_Number__c FROM User WHERE Id = :UserInfo.getUserId()];
        if(u.ContactId != null) supportLevelNumber = u.Contact.Support_Level_Number__c; 
        calcOptions(webToCaseSubTypeId, u);
    }
    public Contact_Us_2014_User_Issue_Channel() { //for VF page
        String w2cs = ApexPages.CurrentPage().getParameters().get('w2cs');
        init(w2cs, UserInfo.getUserId());
        User u = [SELECT ContactId, Contact.Support_Level_Number__c FROM User WHERE Id = :UserInfo.getUserId()];
        if(u.ContactId != null) supportLevelNumber = u.Contact.Support_Level_Number__c; 
        calcOptions(w2cs, u);
    }
    public Contact_Us_2014_User_Issue_Channel(ApexPages.StandardController cont){
        //init(ApexPages.CurrentPage().getParameters().get('w2cs'), UserInfo.getUserId());
        String w2cs = ApexPages.CurrentPage().getParameters().get('w2cs');
        init(w2cs, UserInfo.getUserId());
        User u = [SELECT ContactId, Contact.Support_Level_Number__c FROM User WHERE Id = :UserInfo.getUserId()];
        if(u.ContactId != null) supportLevelNumber = u.Contact.Support_Level_Number__c; 
        calcOptions(w2cs, u);
    }
    public Contact_Us_2014_User_Issue_Channel(Contact_Us_2014 cu2014){ 
        try{
            //Business decided they wanted one vertical column instead of 2 horizontal ones
            //so design firm put everything on page 4 instead of having them on two partial pages loaded through ajax
            if(ApexPages.CurrentPage().getParameters().containsKey('w2cs')){
                String w2cs = ApexPages.CurrentPage().getParameters().get('w2cs');
                init(w2cs, UserInfo.getUserId());
                User u = [SELECT ContactId, Contact.Support_Level_Number__c FROM User WHERE Id = :UserInfo.getUserId()];
                if(u.ContactId != null) supportLevelNumber = u.Contact.Support_Level_Number__c; 
                calcOptions(w2cs, u);
            }
        }catch(Exception e){
            system.debug(LoggingLevel.ERROR, e);
        }
    }

    void init(Id pWebToCaseSubTypeId, Id pGamersUserId){
        w2c_st = [SELECT Contact_Options_Order__c, DisableEmail__c, Description__c, 
                        DisableChat__c, Chat_Button_Id__c, Premium_Chat_Button_Id__c, 
                        DisablePhone__c, disablePhoneForm__c,
                        DisableTwitter__c, TwitterHashTag__c, 
                        DisableForum__c, ForumUrl__c, subTypeMap__c, 
                        DisableScheduleACall__c, DisableFacebook__c, DisableAmbassadorChat__c,

                        customForm__c,
                        
                        Phone_Messaging__c, WebToCase_Type__r.Phone_Messaging__c,

                        Level_Required_To_Access_Phone__c, Level_Required_To_Access_Chat__c, Level_Required_To_Access_Twitter__c,
                        Level_Required_To_Access_Form__c, Level_Required_To_Access_Forum__c, Level_Required_To_Access_Facebook__c,
                        Level_Required_To_Access_AmbassadorChat__c, Level_Required_To_Schedule_A_Call__c, Level_Required_For_Premium_Chat__c,

                        WebToCase_Type__r.Contact_Options_Order__c, WebToCase_Type__r.DisableEmail__c, 
                        WebToCase_Type__r.DisableChat__c, WebToCase_Type__r.Chat_Button_Id__c, WebToCase_Type__r.Premium_Chat_Button_Id__c, 
                        WebToCase_Type__r.DisablePhone__c, WebToCase_Type__r.disablePhoneForm__c,
                        WebToCase_Type__r.DisableTwitter__c, WebToCase_Type__r.TwitterHashTag__c, 
                        WebToCase_Type__r.DisableForum__c, WebToCase_Type__r.ForumUrl__c, 
                        WebToCase_Type__r.DisableScheduleACall__c, WebToCase_Type__r.DisableFacebook__c, 
                        WebToCase_Type__r.DisableAmbassadorChat__c,

                        WebToCase_Type__r.Level_Required_To_Access_Phone__c, WebToCase_Type__r.Description__c,
                        WebToCase_Type__r.Level_Required_To_Access_Chat__c, WebToCase_Type__r.Level_Required_To_Access_Twitter__c, 
                        WebToCase_Type__r.Level_Required_To_Access_Form__c, WebToCase_Type__r.Level_Required_To_Access_Forum__c, 
                        WebToCase_Type__r.Level_Required_To_Access_Facebook__c, WebToCase_Type__r.Level_Required_To_Access_AmbassadorChat__c, 
                        WebToCase_Type__r.Level_Required_To_Schedule_A_Call__c, WebToCase_Type__r.Level_Required_For_Premium_Chat__c,
                        WebToCase_Type__r.TypeMap__c,

                        WebToCase_Type__r.WebToCase_GameTitle__c,
                        (SELECT Max_Open_Submissions_Per_Gamer__c FROM WebToCase_Forms__r WHERE IsActive__c = TRUE LIMIT 1)
                    FROM WebToCase_SubType__c WHERE Id = :pWebToCaseSubTypeId];
        w2c_t = w2c_st.WebToCase_Type__r;
        w2c_gt = [SELECT Contact_Options_Order__c,  DisableEmail__c, Title__c, Public_Product__c,
                        DisablePhone__c, disablePhoneForm__c,
                        DisableForum__c, ForumUrl__c,
                        DisableChat__c, Chat_Button_Id__c,  Premium_Chat_Button_Id__c, 
                        DisableTwitter__c, TwitterHashTag__c,
                        DisableScheduleACall__c, DisableFacebook__c, DisableAmbassadorChat__c,

                        Phone_Messaging__c, WebToCase_Platform__r.Phone_Messaging__c,

                        Level_Required_To_Access_Phone__c, Level_Required_To_Access_Chat__c, Level_Required_To_Access_Twitter__c,
                        Level_Required_To_Access_Form__c, Level_Required_To_Access_Forum__c, Level_Required_To_Access_Facebook__c,
                        Level_Required_To_Access_AmbassadorChat__c, Level_Required_To_Schedule_A_Call__c, Level_Required_For_Premium_Chat__c,
                        WebToCase_Platform__r.Contact_Options_Order__c, WebToCase_Platform__r.DisableEmail__c, 
                        WebToCase_Platform__r.DisablePhone__c, WebToCase_Platform__r.disablePhoneForm__c,
                        WebToCase_Platform__r.DisableForum__c, WebToCase_Platform__r.ForumUrl__c,
                        WebToCase_Platform__r.DisableChat__c, WebToCase_Platform__r.Chat_Button_Id__c, WebToCase_Platform__r.Premium_Chat_Button_Id__c, 
                        WebToCase_Platform__r.DisableTwitter__c, WebToCase_Platform__r.TwitterHashTag__c,
                        WebToCase_Platform__r.DisableScheduleACall__c, WebToCase_Platform__r.DisableFacebook__c, 
                        WebToCase_Platform__r.DisableAmbassadorChat__c,

                        WebToCase_Platform__r.Level_Required_To_Access_Phone__c, WebToCase_Platform__r.Level_Required_To_Access_Chat__c, 
                        WebToCase_Platform__r.Level_Required_To_Access_Twitter__c, WebToCase_Platform__r.Level_Required_To_Access_Form__c, 
                        WebToCase_Platform__r.Level_Required_To_Access_Forum__c, WebToCase_Platform__r.Level_Required_To_Access_Facebook__c,
                        WebToCase_Platform__r.Level_Required_To_Access_AmbassadorChat__c, WebToCase_Platform__r.Level_Required_To_Schedule_A_Call__c,
                        WebToCase_Platform__r.Level_Required_For_Premium_Chat__c, WebToCase_Platform__r.Name__c,

                        WebToCase_Platform__r.WebtoCase__c
                    FROM WebToCase_GameTitle__c WHERE Id = :w2c_t.WebToCase_GameTitle__c];
        
        w2c_p = w2c_gt.WebToCase_Platform__r;
        w2c = [SELECT TwitterHashTag__c, TwitterEnabled__c, ScheduleACallEnabled__c, Premium_Chat_Button_Id__c,
                        phoneEnabledMessage__c, phoneEnabled__c, phoneDisabledMessage__c, phoneAlertMessage__c,
                        phoneAlertEnabled__c, Phone_Hours__c, Phone_Hours2__c, Level_Required_For_Premium_Chat__c,
                        Phone_Number__c, 
                        Language__c, ForumURL__c, ForumEnabled__c, formAlertMessage__c, Chat_Weekday_End__c,
                        formAlertEnabled__c, FacebookEnabled__c, emailAlertMessage__c, emailAlertEnabled__c,
                        Contact_Options_Order__c, chatOfflineMessage__c, chatEnabledMessage__c, chatEnabled__c,
                        chatDisabledMessage__c, chatAlertMessage__c, chatAlertEnabled__c, Chat_Weekend_Start__c,
                        Chat_Button_Id__c, AmbassadorChatEnabled__c, Chat_Weekend_End__c, Chat_Weekday_Start__c,
                        Phone_Weekend_Start__c, Phone_Weekend_End__c, Phone_Weekday_Start__c, Phone_Weekday_End__c,
                        phoneClosedMessage__c
                 FROM WebToCase__c WHERE Id = :w2c_p.WebtoCase__c];
    }

    WebToCase_SubType__c w2c_st;
    WebToCase_Type__c w2c_t;
    WebToCase_GameTitle__c w2c_gt;
    WebToCase_Platform__c w2c_p;
    WebToCase__c w2c;
    public WebToCase__c getWeb2Case(){
        return w2c;
    }
    public WebToCase_Platform__c getWeb2CasePlatform(){
        return w2c_p;
    }
    public WebToCase_GameTitle__c getWeb2CaseGameTitle(){
        return w2c_gt;
    }
    public WebToCase_Type__c getWeb2CaseType(){
        return w2c_t;
    }
    public WebToCase_SubType__c getWeb2CaseSubType(){
        return w2c_st;
    }

    
    public void calcOptions(Id pWebToCaseSubTypeId, User gamerUser){
        //Contact Options Order
        allowChat = w2c.chatEnabled__c;
        allowPhoneCall = w2c.phoneEnabled__c;
        allowForm = false;
        formOverRequestLimit = false;
        for(WebToCase_Form__c f : w2c_st.WebToCase_Forms__r){
            if(gamerUser.ContactId != null || gamerUser.ContactId == null ){
                Integer openIssues = [SELECT count() FROM Case WHERE ContactId = :gamerUser.ContactId AND IsClosed = FALSE AND WebToCase_Form__c = :f.Id];
                if(openIssues >= f.Max_Open_Submissions_Per_Gamer__c){
                    allowForm = false;
                    formOverRequestLimit = true;
                }else{
                    formId = f.Id;
                    allowForm = true;
                }
            }else{
                allowForm = false;
            }
        }
        allowForums = w2c.forumEnabled__c;
        allowFacebook = w2c.facebookEnabled__c;
        allowChat = w2c.chatEnabled__c;
        allowAmbassadorChat = w2c.ambassadorChatEnabled__c;
        allowScheduleACall = w2c.scheduleACallEnabled__c;
        allowTwitter = w2c.twitterEnabled__c;
        
    /*  if(! getIsLoggedIn()){
            allowChat = false;
            allowAmbassadorChat = false;
            allowPhoneCall = false;
            allowScheduleACall = false;
            allowForm = false;
        } */

        //set default order
        contactOptionsOrder = new string[]{'chat','twitter','facebook','forum','phone','email','form','ambassadorchat','scheduleacall'};
    
        try{
          String foundOrder = getStringValueFromBottomUp('Contact_Options_Order__c');//test from bottom up
          if(foundOrder != null){
            contactOptionsOrder = new string[]{};
            for(String opt : foundOrder.split(';')) contactOptionsOrder.add(opt);
          }
        }catch(Exception e){
          system.debug('CONTACT OPTIONS ORDER ERROR == '+e);
        }
        system.debug('CONTACT OPTIONS ORDER == '+contactOptionsOrder);

        if(allowChat){
            allowChat = ! isDisabled('disableChat__c'); //ensure that CS's control over offering phone is maintained
            if(allowChat){ //cycle saving
                allowChat = doesUserMeetLevelRequirementForAccess(getStringValueFromBottomUpSkipW2C('Level_Required_To_Access_Chat__c'));
            }
        }

        if(allowChat){
            if( (w2c.Chat_Weekday_Start__c == null) || 
                (w2c.Chat_Weekday_End__c == null) || 
                (w2c.Chat_Weekend_Start__c == null) || 
                (w2c.Chat_Weekend_End__c == null))
            {
              chatWindowOpen = false;
            }else{
                Time chatStart, chatEnd;
                String chatStartHour, chatEndHour;
                Datetime dt = DateTime.now();
                Time currentTime = Time.NewInstance(dt.hourGmt(), 0, 0 ,0);
                system.debug('currentType: ' + currentTime);
                system.debug('dt: ' + dt);
                String currentDayOfWeek = dt.format('EEEE');
                if(currentDayOfWeek == 'Saturday' || currentDayOfWeek == 'Sunday'){
                    chatStartHour = w2c.Chat_Weekend_Start__c.left(w2c.Chat_Weekend_Start__c.indexOf(':'));
                    chatEndHour = w2c.Chat_Weekend_End__c.left(w2c.Chat_Weekend_End__c.indexOf(':'));
                }else{
                    chatStartHour = w2c.Chat_Weekday_Start__c.left(w2c.Chat_Weekday_Start__c.indexOf(':')); 
                    chatEndHour = w2c.Chat_Weekday_End__c.left(w2c.Chat_Weekday_End__c.indexOf(':'));
                }
                chatStart = Time.newInstance(Integer.valueOf(chatStartHour), 0, 0, 0);
                chatEnd = Time.newInstance(Integer.valueOf(chatEndHour), 0, 0, 0);
                //calc windowStatus
                if(currentDayOfWeek == 'Saturday' || currentDayOfWeek == 'Sunday'){
                    if(chatStart == chatEnd)
                        chatWindowOpen = false;
                    else if(chatStart < chatEnd)
                        chatWindowOpen = (currentTime >= chatStart && currentTime <= chatEnd);
                    else
                        chatWindowOpen = ! (currentTime <= chatStart && currentTime >= chatEnd);
                }else{
                    if(chatStart == chatEnd)
                        chatWindowOpen = true;
                    else if(chatStart < chatEnd)
                        chatWindowOpen = (currentTime >= chatStart && currentTime <= chatEnd);
                    else
                        chatWindowOpen = ! (currentTime <= chatStart && currentTime >= chatEnd);
                }

                if(chatWindowOpen){ //chat is open, is this user allowed to access chat
                    allowChat = doesUserMeetLevelRequirementForAccess(getStringValueFromBottomUpSkipW2C('Level_Required_To_Access_Chat__c'));
                    if(allowChat){
                        chatButtonId = '';
                        try{
                            String premiumChatButtonId = getStringValueFromBottomUpNoLowerCase('Premium_Chat_Button_Id__c');
                            if(! isBlankOrNull(premiumChatButtonId) && 
                                doesUserMeetLevelRequirementForAccess(getStringValueFromBottomUpSkipW2C('Level_Required_For_Premium_Chat__c')))
                            {
                                chatButtonId = premiumChatButtonId;
                            }else{
                                chatButtonId = getStringValueFromBottomUpNoLowerCase('Chat_Button_Id__c');
                                if(isBlankOrNull(chatButtonId)) chatButtonId = '573U0000000GmaY';
                            }
                        }catch(exception e){
                            chatButtonId = e.getMessage();
                            system.debug('CHAT BUTTON ID ERROR = '+e);
                        }
                        system.debug('CHAT BUTTON ID == '+chatButtonId);
                    }
                }
            }
        }
        if(allowPhoneCall){
            allowPhoneCall = ! isDisabled('DisablePhone__c'); //ensure that CS's control over offering phone is maintained
            if(allowPhoneCall){ //cycle saving
                allowPhoneCall = doesUserMeetLevelRequirementForAccess(getStringValueFromBottomUpSkipW2C('Level_Required_To_Access_Phone__c'));
                if(allowPhoneCall){
                    phoneMessaging = getStringValueFromBottomUpSkipW2C('Phone_Messaging__c', false);
                    skipPhoneForm = isDisabled('disablePhoneForm__c'); 
                }
            }
        }
        if(allowForm){
            allowForm = doesUserMeetLevelRequirementForAccess(getStringValueFromBottomUpSkipW2C('Level_Required_To_Access_Form__c'));
        }
        if(allowForums){
            allowForums = ! isDisabled('DisableForum__c');
            if(allowForums){ //cycle saving 
                allowForums = doesUserMeetLevelRequirementForAccess(getStringValueFromBottomUpSkipW2C('Level_Required_To_Access_Forum__c'));
                if(allowForums){
                    forumURL = getStringValueFromBottomUp('ForumUrl__c');
                    if(! isBlankOrNull(forumURL)) forumURL +=  '/search.jspa?q='+EncodingUtil.urlEncode(w2c_gt.Title__c+' ', 'UTF-8')+EncodingUtil.urlEncode(w2c_st.Description__c,'UTF-8');
                }
            }
        }
        if(allowFacebook){
            allowFacebook = ! isDisabled('DisableFacebook__c');
            if(allowFacebook)
                allowFacebook = doesUserMeetLevelRequirementForAccess(getStringValueFromBottomUpSkipW2C('Level_Required_To_Access_Facebook__c'));
        }
        if(allowAmbassadorChat){
            allowAmbassadorChat = ! isDisabled('DisableAmbassadorChat__c');
            if(allowAmbassadorChat){
                allowAmbassadorChat = doesUserMeetLevelRequirementForAccess(getStringValueFromBottomUpSkipW2C('Level_Required_To_Access_AmbassadorChat__c'));
            }
        }
        if(allowScheduleACall){
            allowScheduleACall = ! isDisabled('DisableScheduleACall__c');
            if(allowScheduleACall){
                allowScheduleACall = doesUserMeetLevelRequirementForAccess(getStringValueFromBottomUpSkipW2C('Level_Required_To_Schedule_A_Call__c'));
            }
        }
        if(allowTwitter){
            allowTwitter = ! isDisabled('DisableTwitter__c');
            if(allowTwitter){ //cycle saving
                allowTwitter = doesUserMeetLevelRequirementForAccess(getStringValueFromBottomUpSkipW2C('Level_Required_To_Access_Twitter__c'));
                if(allowTwitter)
                    twitterHashTag = getStringValueFromBottomUp('TwitterHashTag__c');
            }
        }

    }
    public String getStringValueFromBottomUp(String fieldName){
        //pReturnType is just used to create a unique signature for the method, associated with the return type
        String result = null;
        if(! isBlankOrNull((string) w2c_st.get(fieldName)))
            result = ((string) w2c_st.get(fieldName)).toLowerCase();
        else if(! isBlankOrNull((string) w2c_t.get(fieldName)))
            result = ((string) w2c_t.get(fieldName)).toLowerCase(); 
        else if(! isBlankOrNull((string) w2c_gt.get(fieldName)))
            result = ((string) w2c_gt.get(fieldName)).toLowerCase(); 
        else if(! isBlankOrNull((string) w2c_p.get(fieldName)))
            result = ((string) w2c_p.get(fieldName)).toLowerCase(); 
        else if(! isBlankOrNull((string) w2c.get(fieldName)))
            result = ((string) w2c.get(fieldName)).toLowerCase(); 
        system.debug(fieldName + ' == ' + result);
        return result;
    }
    public String getStringValueFromBottomUpNoLowerCase(String fieldName){
        //pReturnType is just used to create a unique signature for the method, associated with the return type
        String result = null;
        if(! isBlankOrNull((string) w2c_st.get(fieldName)))
            result = ((string) w2c_st.get(fieldName));
        else if(! isBlankOrNull((string) w2c_t.get(fieldName)))
            result = ((string) w2c_t.get(fieldName));
        else if(! isBlankOrNull((string) w2c_gt.get(fieldName)))
            result = ((string) w2c_gt.get(fieldName));
        else if(! isBlankOrNull((string) w2c_p.get(fieldName)))
            result = ((string) w2c_p.get(fieldName));
        else if(! isBlankOrNull((string) w2c.get(fieldName)))
            result = ((string) w2c.get(fieldName));
        system.debug(fieldName + ' == ' + result);
        return result;
    }
    public String getStringValueFromBottomUpSkipW2C(String fieldName, Boolean returnLowerCase){
        String result = null;
        try{
            if(! isBlankOrNull((string) w2c_st.get(fieldName)))
                result = ((string) w2c_st.get(fieldName));
            else if(! isBlankOrNull((string) w2c_t.get(fieldName)))
                result = ((string) w2c_t.get(fieldName));
            else if(! isBlankOrNull((string) w2c_gt.get(fieldName)))
                result = ((string) w2c_gt.get(fieldName)); 
            else if(! isBlankOrNull((string) w2c_p.get(fieldName)))
                result = ((string) w2c_p.get(fieldName));
        }catch(Exception e){//sObject row was queried without calling the field

        }
        if(returnLowerCase && (result != null)) result = result.toLowerCase();
        system.debug(fieldName + ' == ' + result);
        return result;
    }
    public String getStringValueFromBottomUpSkipW2C(String fieldName){
        String result = null;
        if(! isBlankOrNull((string) w2c_st.get(fieldName)))
            result = ((string) w2c_st.get(fieldName)).toLowerCase();
        else if(! isBlankOrNull((string) w2c_t.get(fieldName)))
            result = ((string) w2c_t.get(fieldName)).toLowerCase(); 
        else if(! isBlankOrNull((string) w2c_gt.get(fieldName)))
            result = ((string) w2c_gt.get(fieldName)).toLowerCase(); 
        else if(! isBlankOrNull((string) w2c_p.get(fieldName)))
            result = ((string) w2c_p.get(fieldName)).toLowerCase(); 
        system.debug(fieldName + ' == ' + result);
        return result;
    }
    public Boolean isDisabled(String fieldName){
        //pReturnType is just used to create a unique signature for the method, associated with the return type
        boolean result = ((Boolean) w2c_st.get(fieldName) || 
            (Boolean) w2c_t.get(fieldName) || 
            (Boolean) w2c_gt.get(fieldName) || 
            (Boolean) w2c_p.get(fieldName));
        system.debug(fieldName + ' == ' + result);
        return result;
    }

    public static boolean isBlankOrNull(String s){return s == null || s == '';}
    public boolean doesUserMeetLevelRequirementForAccess(String requiredLevel){
        try{
            if(requiredLevel == null) return true; //no level requirement, open to all
            if(requiredLevel.contains('-')){
                requiredLevel = requiredLevel.split('-')[0];
            }
            system.debug('supportLevelNumber >= decimal.valueOf(requiredLevel) == '+(supportLevelNumber > decimal.valueOf(requiredLevel)));
            return supportLevelNumber >= decimal.valueOf(requiredLevel);
        }catch(Exception e){
            system.debug('doesUserMeetLevelRequirementForAccess ERROR == '+e);
            return false;
        }
    }

    /*Live Agent Chat Enhancements by Sorna (perficient) - start */
  public string caseId {get;set;}
  public void createCaseAF(){
        if(caseId == null){ //condition added by ATVI, users were clicking the button rapidly and creating lots of cases
            Case cs = new Case();
            cs = Contact_Us_Controller.addDispositionsToCase(
                cs, 
                w2c_p.Name__c,
                w2c_gt.Title__c,
                w2c_t.typeMap__c,
                w2c_st.subTypeMap__c
            );
            /*
            cs.Platform__c = selectedPlatform.Name__c;
            cs.Product_L1__c = selectedTitle.Title__c;
            cs.Issue_Type__c = selectedType.typeMap__c;
            cs.Issue_Sub_Type__c = selectedSubType.subTypeMap__c;
            */
            cs.Subject = cs.Issue_Type__c;
            cs.Origin = 'Chat';
            insert cs;
            caseId = cs.Id;
        }
    }
    /* Live Agent Chat Enhancements by Sorna (perficient) - end */

    public Boolean getIsPhoneOpen(){

        Boolean isopen = false;

        system.debug('Phone Weekday Start is '+ w2c.Phone_Weekday_Start__c);
        system.debug('Phone Weekday End is '+ w2c.Phone_Weekday_End__c);

        if( (w2c.Phone_Weekday_Start__c == null) || 
            (w2c.Phone_Weekday_End__c == null) ||
            (w2c.Phone_Weekend_Start__c == null) ||
            (w2c.Phone_Weekend_End__c == null))
            {
              return isopen;
            } else {
                Time phoneStart, phoneEnd;
                String phoneStartHour, phoneEndHour;
                Datetime dt = DateTime.now();
                Time currentTime = Time.NewInstance(dt.hourGmt(), 0, 0 ,0);
                system.debug('currentType: ' + currentTime);
                system.debug('dt: ' + dt);
                String currentDayOfWeek = dt.format('EEEE');
                if(currentDayOfWeek == 'Saturday' || currentDayOfWeek == 'Sunday'){
                    phoneStartHour = w2c.Phone_Weekend_Start__c.left(w2c.Phone_Weekend_Start__c.indexOf(':'));
                    phoneEndHour = w2c.Phone_Weekend_End__c.left(w2c.Phone_Weekend_End__c.indexOf(':'));
                }else{
                    phoneStartHour = w2c.Phone_Weekday_Start__c.left(w2c.Phone_Weekday_Start__c.indexOf(':')); 
                    phoneEndHour = w2c.Phone_Weekday_End__c.left(w2c.Phone_Weekday_End__c.indexOf(':'));
                }
                phoneStart = Time.newInstance(Integer.valueOf(phoneStartHour), 0, 0, 0);
                phoneEnd = Time.newInstance(Integer.valueOf(phoneEndHour), 0, 0, 0);
                //calc windowStatus
                if(currentDayOfWeek == 'Saturday' || currentDayOfWeek == 'Sunday'){
                    if(phoneStart == phoneEnd)
                        isopen = false;
                    else if(phoneStart < phoneEnd)
                        isopen = (currentTime >= phoneStart && currentTime <= phoneEnd);
                    else
                        isopen = ! (currentTime <= phoneStart && currentTime >= phoneEnd);
                }else{
                    if(phoneStart == phoneEnd)
                        isopen = true;
                    else if(phoneStart < phoneEnd)
                        isopen = (currentTime >= phoneStart && currentTime <= phoneEnd);
                    else
                        isopen = ! (currentTime <= phoneStart && currentTime >= phoneEnd);
                }

            return isopen;
        }

    }
    {
        system.debug('gamification score');
    }
}