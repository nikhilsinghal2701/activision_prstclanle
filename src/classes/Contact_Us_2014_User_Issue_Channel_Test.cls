@isTest
private class Contact_Us_2014_User_Issue_Channel_Test {
	
	@isTest static void test_method_1() {
		Contact testcontact = UnitTestHelper.generateTestContact();
		testcontact.isVIP__c = true;
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc', testcontact.Id);
		insert testportaluser;

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US', Contact_Options_Order__c = 'chat;twitter;facebook;forum;phone;email;form;ambassadorchat;scheduleacall', Level_Required_For_Premium_Chat__c = '50-Ultimate Premium', Chat_Weekday_Start__c = '00:00GMT', Chat_Weekday_End__c = '23:00GMT', Chat_Weekend_Start__c = '00:00GMT', Chat_Weekend_End__c = '23:00GMT', Phone_Weekday_Start__c = '00:00GMT', Phone_Weekday_End__c = '23:00GMT', Phone_Weekend_Start__c = '00:00GMT', Phone_Weekend_End__c ='23:00GMT', ForumEnabled__c = true, FacebookEnabled__c = true, AmbassadorChatEnabled__c = true, ScheduleACallEnabled__c = true, TwitterEnabled__c = true, phoneEnabled__c = true);
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(WebToCase__c = w2c1.Id, Name__c = 'Test Platform', isActive__c = true,disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Phone__c = '50-Ultimate Premium', Level_Required_To_Access_AmbassadorChat__c = '50-Ultimate Premium');
		insert w2cp1;

		WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Test Title', disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Form__c = '50-Ultimate Premium', Level_Required_To_Schedule_A_Call__c = '50-Ultimate Premium');
		insert w2cg1;

		WebToCase_Type__c w2ct1 = new WebToCase_Type__c(TypeMap__c = 'Test Type', WebToCase_GameTitle__c = w2cg1.Id, Description__c = 'Test Type', isActive__c = true, disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Forum__c = '50-Ultimate Premium', Level_Required_To_Access_Twitter__c = '50-Ultimate Premium');
		insert w2ct1;

		WebToCase_SubType__c w2cst1 = new WebToCase_SubType__c(SubTypeMap__c = 'Test SubType', Description__c = 'Test SubType', WebToCase_Type__c = w2ct1.Id, isActive__c = true, disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Facebook__c = '50-Ultimate Premium');
		insert w2cst1;

		WebToCase_Form__c w2cf1 = new WebToCase_Form__c(Name__c = 'Test Form', WebToCase_SubType__c = w2cst1.Id, isActive__c = true, Max_Open_Submissions_per_Gamer__c = 1);
		insert w2cf1;

		PageReference currentpage = Page.Contact_Us_2014;

		currentpage.getParameters().put('w2cs',w2cst1.Id);
		Test.setCurrentPage(currentpage);

		system.runAs(testportaluser){
			Contact_Us_2014_User_Issue_Channel uic = new Contact_Us_2014_User_Issue_Channel();
			system.assertEquals(uic.getTheGamer().Id,testcontact.Id);
			system.assertEquals(uic.getWeb2Case().Id,w2c1.Id);
			system.assertEquals(uic.getWeb2CasePlatform().Id,w2cp1.Id);
			system.assertEquals(uic.getWeb2CaseGameTitle().Id,w2cg1.Id);
			system.assertEquals(uic.getWeb2CaseType().Id,w2ct1.Id);
			system.assertEquals(uic.getWeb2CaseSubType().Id,w2cst1.Id);
			system.assertEquals(uic.getIsPhoneOpen(),true);
			system.assertEquals(uic.getStringValueFromBottomUp('Level_Required_To_Access_Form__c'),'50-ultimate premium');
			system.assertEquals(uic.getStringValueFromBottomUpNoLowerCase('Level_Required_To_Access_Facebook__c'), '50-Ultimate Premium');
			system.assertEquals(uic.getStringValueFromBottomUpNoLowerCase('Level_Required_To_Access_Twitter__c'), '50-Ultimate Premium');
			system.assertEquals(uic.getStringValueFromBottomUpNoLowerCase('Level_Required_To_Access_Phone__c'), '50-Ultimate Premium');
			system.assertEquals(uic.getStringValueFromBottomUpNoLowerCase('Level_Required_To_Access_Phone__c'), '50-Ultimate Premium');
			system.assertEquals(uic.getStringValueFromBottomUpNoLowerCase('Level_Required_For_Premium_Chat__c'), '50-Ultimate Premium');
			system.assertEquals(uic.getStringValueFromBottomUpSkipW2C('Level_Required_To_Access_Form__c',false), '50-Ultimate Premium');
			system.assertEquals(uic.getStringValueFromBottomUpSkipW2C('Level_Required_To_Access_Facebook__c',false), '50-Ultimate Premium');
			system.assertEquals(uic.getStringValueFromBottomUpSkipW2C('Level_Required_To_Access_Twitter__c',false), '50-Ultimate Premium');
			system.assertEquals(uic.getStringValueFromBottomUpSkipW2C('Level_Required_To_Access_Phone__c',false), '50-Ultimate Premium');
			system.assertEquals(uic.getStringValueFromBottomUpSkipW2C('Level_Required_To_Access_Phone__c',false), '50-Ultimate Premium');
			system.assertEquals(uic.getIsLoggedIn(),true);			
			uic.createCaseAF();


			Contact_Us_2014_User_Issue_Channel uic2 = new Contact_Us_2014_User_Issue_Channel(w2cst1.Id,testportaluser.Id);

			ApexPages.StandardController scon = new ApexPages.StandardController(testportaluser);

			Contact_Us_2014_User_Issue_Channel uic3 = new Contact_Us_2014_User_Issue_Channel(scon);

			Contact_Us_2014 cu = new Contact_Us_2014();

			Contact_Us_2014_User_Issue_Channel uic4 = new Contact_Us_2014_User_Issue_Channel(cu);
		}

		Contact_Us_2014_User_Issue_Channel uic5 = new Contact_Us_2014_User_Issue_Channel();
	}

	@isTest static void test_method_2() {
		Contact testcontact = UnitTestHelper.generateTestContact();
		testcontact.isVIP__c = true;
		insert testcontact;

		User testportaluser = UnitTestHelper.getCustomerPortalUser('abc', testcontact.Id);
		insert testportaluser;

		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
		insert testproduct;

		WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US', Chat_Weekday_Start__c = '00:00GMT', Chat_Weekday_End__c = '00:00GMT', Chat_Weekend_Start__c = '00:00GMT', Chat_Weekend_End__c = '23:00GMT', ForumEnabled__c = true, FacebookEnabled__c = true, AmbassadorChatEnabled__c = true, ScheduleACallEnabled__c = true, TwitterEnabled__c = true, phoneEnabled__c = true);
		insert w2c1;

		WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(ForumUrl__c = 'Test', WebToCase__c = w2c1.Id, Name__c = 'Test Platform', isActive__c = true,disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Phone__c = '50-Ultimate Premium', Level_Required_To_Access_AmbassadorChat__c = '50-Ultimate Premium');
		insert w2cp1;

		WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(Premium_Chat_Button_Id__c = '573U0000000Gma9', WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Test Title', disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Form__c = '50-Ultimate Premium', Level_Required_To_Schedule_A_Call__c = '50-Ultimate Premium');
		insert w2cg1;

		WebToCase_Type__c w2ct1 = new WebToCase_Type__c(Contact_Options_Order__c = 'chat;twitter;facebook;forum;phone;email;form;ambassadorchat;scheduleacall', TypeMap__c = 'Test Type', WebToCase_GameTitle__c = w2cg1.Id, Description__c = 'Test Type', isActive__c = true, disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Forum__c = '50-Ultimate Premium', Level_Required_To_Access_Twitter__c = '50-Ultimate Premium');
		insert w2ct1;

		WebToCase_SubType__c w2cst1 = new WebToCase_SubType__c(SubTypeMap__c = 'Test SubType', Description__c = 'Test SubType', WebToCase_Type__c = w2ct1.Id, isActive__c = true, disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Facebook__c = '50-Ultimate Premium');
		insert w2cst1;

		WebToCase_Form__c w2cf1 = new WebToCase_Form__c(Name__c = 'Test Form', WebToCase_SubType__c = w2cst1.Id, isActive__c = true, Max_Open_Submissions_per_Gamer__c = 1);
		insert w2cf1;

		Case testcase1 = UnitTestHelper.generateTestCase();
		testcase1.WebToCase_Form__c = w2cf1.Id;
		testcase1.ContactId = testcontact.Id;
		testcase1.Comment_Not_Required__c = true;
		insert testcase1;

		Case testcase2 = UnitTestHelper.generateTestCase();
		testcase2.WebToCase_Form__c = w2cf1.Id;
		testcase2.ContactId = testcontact.Id;
		testcase2.Comment_Not_Required__c = true;
		insert testcase2;

		system.debug('Testcase1 info '+testcase1.ContactId+' '+testcase1.IsClosed+' '+testcase1.WebToCase_Form__c);
		system.debug('testuser ContactId '+testportaluser.ContactId+' Web to case ID is '+w2cf1.Id);

		PageReference currentpage = Page.Contact_Us_2014;

		currentpage.getParameters().put('w2cs',w2cst1.Id);
		Test.setCurrentPage(currentpage);

		system.runAs(testportaluser){
			Contact_Us_2014_User_Issue_Channel uic = new Contact_Us_2014_User_Issue_Channel();
			system.assertEquals(uic.getIsPhoneOpen(),false);

						
		}

		
	}
	
	
	
}