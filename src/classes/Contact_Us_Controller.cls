public with sharing class Contact_Us_Controller {

  //Statics: NOTHING IS CHANGING TEST2
  public string W2C_LANG = 'en_US';
  public static boolean CHAT_DEFAULT = true;
  public static boolean EMAIL_DEFAULT = true;
  public static boolean PHONE_DEFAULT = true;
  public static boolean TWITTER_DEFAULT = true;
  public static boolean FORUM_DEFAULT = true;
  public static boolean FORM_DEFAULT = false;

  //Resources:
  public string CONTACT_US_IMAGES;
  public string BASE_URL;

  //Contact Properties:
  public Contact currentContact { get; set; }
  private string contactIdOfUser { get; set; }
  public string userLanguage { get; set; }
  public boolean userIsAuthenticated { get; set; }

  //WebToCase Objects:
  public WebToCase__c w2c { get; set; }
  
  //Platforms:
  public WebToCase_Platform__c selectedPlatform { get; set; }
  public string platformList { get; set; }
  public string selectedPlatformId { get; set; }
    
  //Titles:
  public string titleList { get; set; }
  public string selectedTitleId { get; set; }
  public WebToCase_GameTitle__c selectedTitle { get; set; }

  //Type / SubType Accordion:
  public string accordionHTML { get; set; }
  public string selectedSubTypeId { get; set; }
  public boolean subTypeHasForm { get; set; }
  
  //Selected Type/SubType:
  public WebToCase_Type__c selectedType { get; set; }
  public WebToCase_SubType__c selectedSubType { get; set; }

  //SubType's Article:
  public KnowledgeArticleVersion kbArticle { get; set; }
  public boolean hasKBArticle { get; set; }
  
  //SubType's Form:
  public WebToCase_Form__c customForm { get; set; }
  public List<WebToCase_FormField__c> formFields { get; set; }
  public boolean formHasSelectList;
  public List<SelectOption> selectOptionList1 { get; set; }
  public List<SelectOption> selectOptionList2 { get; set; }
  public List<SelectOption> selectOptionList3 { get; set; }
  public List<SelectOption> selectOptionList4 { get; set; }
  public List<SelectOption> selectOptionList5 { get; set; }
  public boolean formHasErrorMessage { get; set; } 
  public string formErrorMessage { get; set; }
  
  //Form Uploaded Document
  public Attachment uploadedFile { get ; set; }
  public integer uploadedFileSize { get; set; }
  public string uploadedFileName { get; set; }
  public blob uploadedFileBody { get; set; }
  public string uploadedFileContentType { get; set; }

  //Contact Overrides:
  public boolean chatOverride { get; set; }
  public boolean phoneOverride { get; set; }
  public boolean chatOnline { get; set; }

  //Contact Option Toggles:
  public boolean enableChat { get; set; }
  public boolean enableEmail { get; set; }
  public boolean enablePhone { get; set; }
  public boolean enableTwitter { get; set; }
  public boolean enableForum { get; set; }
  public boolean enableForm { get; set; }

  //Email Form:
  public WebToCase_Form__c emailForm { get; set; }
  public List<WebToCase_FormField__c> emailFormFields { get; set; }
  public boolean submitButtonEnabled { get; set; }

  //Case:
  public Case currentCase { get; set; }
  public string currentCaseNumber { get; set; } 
  
  //Page Nav
  public boolean showConfirmationPage { get; set; }
  
  //Chat Parameters
  public String gamerName {get;set;}
  public String gamerEmail {get;set;}
  public String gamerEliteStatus {get;set;}
  public String contactID {get;set;}

  //Multiplayer Select Options
  map<Id, Multiplayer_Account__c> mpaId2mpa = new map<Id, Multiplayer_Account__c>();
  boolean linkedAccountsPresent = false;

  //Constructor:
  public Contact_Us_Controller(ApexPages.StandardController stdController)
  {
    //Set Contact:
    contactIdOfUser = '';
    string userId = Userinfo.getUserId();    
                
        List<User> userRec = [SELECT Id, Contact.Id FROM User WHERE Id =: userId limit 1];  
        if(userRec.size()>0) {        
            User u=userRec.get(0);        
            if(u.Contact!=null) {            
                contactIdOfUser=u.Contact.Id;            
                system.debug('Contact Id for the user ['+UserInfo.getUserName()+'] is:'+contactIdOfUser);        
            }else {             
                system.debug('No contact associated with this user');               
            }    
        }
        
        List<Contact> contactList = [SELECT Id, Name, Salutation, LastName, 
                                        FirstName, Email, Elite_status__c, 
                                        MailingCountry, Gamertag__c, Country__c
                                        FROM Contact
                                        WHERE id = :contactIdOfUser LIMIT 1];
        if(contactList.size() > 0){
            currentContact = contactList[0];
            contactID = currentContact.id;
            gamerName = AtviUtility.checkNull(currentContact.Name);
            gamerEmail = AtviUtility.checkNull(currentContact.Email);
                //If the gamer is of Elite status, greet by name in the Gamertag                    
                if(currentContact.Elite_Status__c != null) {
                gamerEliteStatus = AtviUtility.checkNull(currentContact.Elite_Status__c);
                }
        }
        else{
          currentContact = new Contact();
        }
    
    //currentContact = (Contact)stdController.getRecord();
    system.debug(currentContact.id);
    
    //Get User Language:
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            userLanguage = tempCookie.getValue();            
        }
        else{
          userLanguage = 'en_US';
        }
        
        //Check for Authentication:
        userIsAuthenticated = UserInfo.getUserType().equalsIgnoreCase('CSPLitePortal');
        
        system.debug(userLanguage);
        //Get W2C object:
        List<WebToCase__c> WebToCases = [SELECT W2CBuild__c, chatAlertEnabled__c, chatAlertMessage__c, phoneAlertEnabled__c, 
                                        phoneAlertMessage__c, emailAlertEnabled__c, emailAlertMessage__c, formAlertEnabled__c, 
                                        formAlertMessage__c, Language__c, Phone_Number__c, Phone_Hours__c, phoneEnabledMessage__c, 
                                        phoneDisabledMessage__c, phoneEnabled__c, Chat_Weekday_Start__c, Chat_Weekday_End__c, 
                                        Chat_Weekend_Start__c, Chat_Weekend_End__c, chatEnabled__c, chatEnabledMessage__c, 
                                        chatDisabledMessage__c, chatOfflineMessage__c, twitterEnabled__c, ForumEnabled__c, forumURL__c, 
                                        Contact_Options_Order__c, Chat_Button_Id__c,
                                        (SELECT Name, isActive__c, Name__c, Image__c, contact_options_order__c, Chat_Button_Id__c 
                                          FROM WebToCase_Platforms__r 
                                          WHERE isActive__c = True ORDER BY Sort_Order__c ASC Nulls last) 
                                        From WebToCase__c WHERE Language__c = :userLanguage LIMIT 1]; 
    if(WebToCases.Size() > 0){
      w2c = WebToCases[0];
    }
    else{
      w2c = [SELECT W2CBuild__c, chatAlertEnabled__c, chatAlertMessage__c, phoneAlertEnabled__c, phoneAlertMessage__c, 
            emailAlertEnabled__c, emailAlertMessage__c, formAlertEnabled__c, formAlertMessage__c, Language__c, Phone_Number__c, 
            Phone_Hours__c, phoneEnabledMessage__c, phoneDisabledMessage__c, phoneEnabled__c, Chat_Weekday_Start__c, Chat_Weekday_End__c, 
            Chat_Weekend_Start__c, Chat_Weekend_End__c, chatEnabled__c, chatEnabledMessage__c, chatDisabledMessage__c, 
            chatOfflineMessage__c, twitterEnabled__c, ForumEnabled__c, forumURL__c, contact_options_order__c, Chat_Button_Id__c,
            (SELECT Name, isActive__c, Name__c, Image__c, contact_options_order__c, Chat_Button_Id__c
              FROM WebToCase_Platforms__r WHERE isActive__c = True) 
            From WebToCase__c WHERE Language__c = 'en_US' LIMIT 1];
    }
    
    //Check URL
    system.debug('ForumURL:'+w2c.forumurl__c);
    //Set Override:
    chatOverride = !w2c.chatEnabled__c;
    phoneOverride = !w2c.phoneEnabled__c;
    
    //Check to see if chat is online:
    Datetime dt = DateTime.now();
    system.debug('DateTime.now(): ' + dt);
    String currentDayOfWeek = dt.format('EEEE');
    system.debug('Current Day of Week' + currentDayOfWeek);
    
    
    if((w2c.Chat_Weekday_Start__c == null) || (w2c.Chat_Weekday_End__c == null) || (w2c.Chat_Weekend_Start__c == null) || (w2c.Chat_Weekend_End__c == null)){
      chatOnline = false;
    }
    else{
      if(currentDayOfWeek == 'Saturday' || currentDayOfWeek == 'Sunday'){
        Time chatStart_weekEnd = Time.NewInstance(
          Integer.valueOf(w2c.Chat_Weekend_Start__c.left(w2c.Chat_Weekend_Start__c.indexOf(':'))), 
          0, 
          0, 
          0);
          system.debug('ChatStart Weekend: ' + chatStart_weekEnd);
        Time chatEnd_weekEnd = Time.NewInstance(
          Integer.valueOf(w2c.Chat_Weekend_End__c.left(w2c.Chat_Weekend_End__c.indexOf(':'))), 
          0, 
          0, 
          0);
          system.debug('ChatEnd Weekend: ' + chatEnd_weekEnd);
        Time currentTime = Time.NewInstance(dt.hourGmt(), 0, 0 ,0);
        system.debug('Current Time: ' + currentTime);
        
        //Check if time overlaps:
        if(chatStart_weekEnd == chatEnd_weekEnd){
          chatOnline = false;
        }
        else if(chatStart_weekEnd < chatEnd_weekEnd){
          if(currentTime >= chatStart_weekEnd && currentTime <= chatEnd_weekEnd){
            chatOnline = true;
          }
          else{
            chatOnline = false;
          }
        }
        else{
          if(currentTime <= chatStart_weekEnd && currentTime >= chatEnd_weekEnd){
            chatOnline = false;
          }
          else{
            chatOnline = true;
          }
        }
      }
      else{
        Time chatStart_weekDay = Time.NewInstance(
          Integer.valueOf(w2c.Chat_Weekday_Start__c.left(w2c.Chat_Weekday_Start__c.indexOf(':'))), 
          0, 
          0, 
          0);
          system.debug('ChatStart WeekDay: ' + chatStart_weekDay);
        Time chatEnd_weekDay = Time.NewInstance(
          Integer.valueOf(w2c.Chat_Weekday_End__c.left(w2c.Chat_Weekday_End__c.indexOf(':'))), 
          0, 
          0, 
          0);
          system.debug('ChatEnd WeekDay: ' + chatEnd_weekDay);
          Time currentTime = Time.NewInstance(dt.hourGmt(), 0, 0 ,0);
          system.debug('Current Time: ' + currentTime);
          
          //Check if time overlaps:
          if(chatStart_weekDay == chatEnd_weekDay){
            chatOnline = true;
          }
          else if(chatStart_weekDay < chatEnd_weekDay){
            if(currentTime >= chatStart_weekDay && currentTime <= chatEnd_weekDay){
              chatOnline = true;
            }
            else{
              chatOnline = false;
            }
          }
          else{
            if(currentTime <= chatStart_weekDay && currentTime >= chatEnd_weekDay){
              chatOnline = false;
            }
            else{
              chatOnline = true;
            }
          }
      }
      system.debug('Chat Online: ' + chatOnline);
    }
    
    
    
    //Retrieve Static Resource of Images:
    CONTACT_US_IMAGES = GetResourceURL('Contact_Us_Images'); 
    BASE_URL = URL.getSalesforceBaseUrl().toExternalForm();
    
    //Gather each platform:
    platformList = '<ul class="console-select">';
    for(WebToCase_Platform__c platform : w2c.WebToCase_Platforms__r)
    {
      platformList += '<li><a href="#" data-id="' + platform.id + '"><img src="' + BASE_URL + CONTACT_US_IMAGES + '/' + platform.Image__c + '.png"/><span>' + platform.Name__c.escapeEcmaScript() + '</span></a></li>';
    }
    platformList += '</ul>';
    
    //Set Contact Defaults:
    enableChat = CHAT_DEFAULT;
    enablePhone = PHONE_DEFAULT;
    enableEmail = EMAIL_DEFAULT;
    enableTwitter = TWITTER_DEFAULT;
    enableForum = FORUM_DEFAULT;
    enableForm = FORM_DEFAULT;
    
    //Init Form:
    customForm = new WebToCase_Form__c();
    uploadedFileName = '';
    uploadedFileSize = 0;
    uploadedFileContentType = '';
    
    //Init Attachment:
    uploadedFile = new Attachment();
    
    //Init CaseNumber:
    currentCaseNumber = '';
    
    //Show standard page:
    showConfirmationPage = false; 
  }

  public void platformChanged()
  {
    //Retrieve Selected Platform:
    selectedPlatform = [SELECT isActive__c, disablePhone__c, disableEmail__c, disableChat__c, disableTwitter__c, disableForum__c, 
                        contact_options_order__c, Chat_Button_Id__c, 
                        Name__c, Id FROM WebToCase_Platform__c WHERE id = :selectedPlatformID];
    
    //Check for Contact Method Toggles:
    resetContactOptions();
    
    //Retrieve List of Titles:
    titleList = '';
    
    //Get Non-Other Titles Alpha Sort:
    for(WebToCase_GameTitle__c title : [SELECT isOther__c, image__c, isActive__c, Public_Product__c, WebToCase_Platform__r.Name__c, 
                                        WebToCase_Platform__r.Id, WebToCase_Platform__r.isActive__c, WebToCase_Platform__c, Title__c, 
                                        Name, priority__c, contact_options_order__c, Chat_Button_Id__c
                                        FROM WebToCase_GameTitle__c where WebToCase_Platform__r.Id = :selectedPlatformId 
                                        AND Language__c = :userLanguage AND isActive__c = true AND isOther__c = false ORDER BY priority__c])
    {
      titleList += '<li><a href="#" data-id="' + title.id + '"><img src="' + BASE_URL + CONTACT_US_IMAGES + '/' + title.Image__c + '.png"/><span>' + title.Title__c.escapeEcmaScript() + '</span></a></li>';
    }
    //Get Other Titles Alpha Sort:
    for(WebToCase_GameTitle__c title : [SELECT isOther__c, image__c, isActive__c, Public_Product__c, WebToCase_Platform__r.Name__c, 
                                        WebToCase_Platform__r.Id, WebToCase_Platform__r.isActive__c, WebToCase_Platform__c, Title__c,
                                        contact_options_order__c, Chat_Button_Id__c, 
                                        Name FROM WebToCase_GameTitle__c WHERE WebToCase_Platform__r.Id = :selectedPlatformId AND 
                                        Language__c = :userLanguage AND isActive__c = true AND isOther__c = true ORDER BY Title__c ASC])
    {
      titleList += '<li><a href="#" data-id="' + title.id + '"><img src="' + BASE_URL + CONTACT_US_IMAGES + '/' + title.Image__c + '.png"/><span>' + title.Title__c.escapeEcmaScript() + '</span></a></li>';
    }
  }
  
  public void titleChanged()
  {
    //Set Title:
    selectedTitle = [SELECT disablePhone__c, disableEmail__c, disableChat__c, disableTwitter__c, disableForum__c, isOther__c, image__c, 
                    isActive__c, Public_Product__c, WebToCase_Platform__r.Name__c, WebToCase_Platform__r.Id, contact_options_order__c, Chat_Button_Id__c,
                    WebToCase_Platform__r.isActive__c, WebToCase_Platform__c, Title__c, Name, Public_Product__r.id 
                    FROM WebToCase_GameTitle__c WHERE id = :selectedTitleID];
    
    //Check for Contact Method Toggles:
    resetContactOptions();
    
    //Retrieve List of Types:
    List<WebToCase_Type__c> typeList = [SELECT Name, isActive__c, Description__c, Sort_Position__c, WebToCase_GameTitle__r.id, 
                                        WebToCase_GameTitle__c, Id, contact_options_order__c, Chat_Button_Id__c
                                        FROM WebToCase_Type__c WHERE 
                                        WebToCase_GameTitle__r.id = :selectedTitleId AND isActive__c = TRUE
                                        ORDER BY Sort_Position__c ASC NULLS FIRST, CreatedDate DESC];

    List<string> typeIDs = new List<String>();
    
    for(WebToCase_Type__c wType : typeList)
    {
      typeIDs.add(wType.id);
    }
    
    //Retrieve SubTypes: 
    List<WebToCase_SubType__c> subTypeList = [SELECT Name, isActive__c, Description__c, Sort_Position__c, WebToCase_Type__r.id, 
                                              WebToCase_Type__c, Id, contact_options_order__c, Chat_Button_Id__c
                                              FROM WebToCase_SubType__c WHERE WebToCase_Type__r.id IN :typeList 
                                              AND isActive__c = True ORDER BY Sort_Position__c ASC NULLS FIRST, CreatedDate DESC];
    
    accordionHTML = '';
    
    for(WebToCase_Type__c wType : typeList)
    {
      accordionHTML += '<h3 class="type-selection">' + wType.Description__c + '</h3><div><ol>';
      
      for(WebToCase_SubType__c subType : subTypeList)
      {
        if(subType.WebToCase_Type__r.id == wType.id)
        {
          accordionHTML += '<li class="sub-type-selection"><a class="sub-selection-a" href="#" data-id="' + subType.id + '">' + subType.Description__c + '</a></li>';
        }
      }
      accordionHTML += '</ol></div>';
    }
  }
  
  public string[] contactOptionsOrder {get; set;}
  public string[] calcContactOptionsOrder(WebToCase__c pw2c, WebToCase_Platform__c pw2c_p, WebToCase_GameTitle__c pw2c_gt, 
                                          WebToCase_Type__c pw2c_t, WebToCase_SubType__c pw2c_st)
  {
    //set default order
    string[] results = new string[]{'chat','twitter','forum','phone','email','form'};
    
    try{
      String foundOrder;
      //test from bottom up

      if(! isBlankOrNull(pw2c_st.Contact_Options_Order__c)){
        foundOrder = pw2c_st.Contact_Options_Order__c.toLowerCase(); 
      }else if(! isBlankOrNull(pw2c_t.Contact_Options_Order__c)){
        foundOrder = pw2c_t.Contact_Options_Order__c.toLowerCase(); 
      }else if(! isBlankOrNull(pw2c_gt.Contact_Options_Order__c)){
        foundOrder = pw2c_gt.Contact_Options_Order__c.toLowerCase(); 
      }else if(! isBlankOrNull(pw2c_p.Contact_Options_Order__c)){
        foundOrder = pw2c_p.Contact_Options_Order__c.toLowerCase(); 
      }else if(! isBlankOrNull(pw2c.Contact_Options_Order__c)){
        foundOrder = pw2c.Contact_Options_Order__c.toLowerCase(); 
      }

      if(foundOrder != null){
        results = new string[]{};
        for(String opt : foundOrder.split(';')) results.add(opt);
      }
    }catch(Exception e){
      system.debug('ERROR == '+e);
    }
    return results;
  }
  public boolean isBlankOrNull(String s){ return s == '' || s == null; }

  public void subTypeSelected(){
    //Retrieve Selected SubType: 
    selectedSubType = [SELECT id, Article_ID__c, Description__c, disableChat__c, disableEmail__c, disablePhone__c, disableTwitter__c, 
                      disableForum__c, isActive__c, SubTypeMap__c, WebToCase_Type__r.Description__c, WebToCase_Type__r.id
                      ,Contact_Options_Order__c, Chat_Button_Id__c
                      FROM WebToCase_SubType__c WHERE id = :selectedSubTypeId LIMIT 1];
    
    //Retrieve Selected Type:
    selectedType = [SELECT disablePhone__c, disableEmail__c, disableChat__c, disableTwitter__c, disableForum__c, TypeMap__c, 
                    Description__c, WebToCase_GameTitle__c, Id, Contact_Options_Order__c, Chat_Button_Id__c
                    FROM WebToCase_Type__c WHERE id = :selectedSubType.WebToCase_Type__r.id];
    
    //Check for RelayState:
    if(selectedTitle == null){
      selectedTitle = [SELECT disablePhone__c, disableEmail__c, disableChat__c, disableTwitter__c, disableForum__c, isOther__c, image__c, 
                        isActive__c, Public_Product__c, WebToCase_Platform__r.Name__c, WebToCase_Platform__r.Id, Contact_Options_Order__c,
                        Chat_Button_Id__c, WebToCase_Platform__r.isActive__c, WebToCase_Platform__c, Title__c, Name, Public_Product__r.id 
                        FROM WebToCase_GameTitle__c where id = :selectedType.WebToCase_GameTitle__c];
      if(selectedPlatform == null){
        selectedPlatform = [SELECT isActive__c, disablePhone__c, disableEmail__c, disableChat__c, disableTwitter__c, disableForum__c, 
                            Name__c, Contact_Options_Order__c, Chat_Button_Id__c
                            FROM WebToCase_Platform__c WHERE id = :selectedTitle.WebToCase_Platform__c];
      }
    }
    
    contactOptionsOrder = calcContactOptionsOrder(w2c, selectedPlatform, selectedTitle, selectedType, selectedSubType);

    //Check for Contact Method Toggles:
    resetContactOptions();
    
    //Retrieve Article:
    string articlelang;
    if(userLanguage.contains('en')){
        articlelang = 'en_US';
    }
    else if(userLanguage.contains('fr')){
        articlelang = 'fr';
    }
    else if(userLanguage.contains('it')){
        articlelang = 'it';
    }
    else if(userLanguage.contains('de')){
        articlelang = 'de';
    }
    else if(userLanguage.contains('pt')){
        articlelang = 'pt_BR';
    }
    else if(userLanguage.contains('sv')){
        articlelang = 'sv';
    }
    else if(userLanguage.contains('nl')){
        articlelang = 'nl_NL';
    }
    else if(userLanguage.contains('es')){
        articlelang = 'es';
    }
    else{
        articlelang = 'en_US';
    }
    
    List<KnowledgeArticleVersion> kbArticleList = new List<KnowledgeArticleVersion>();
    kbArticleList = [Select Id, Title, Summary, PublishStatus, KnowledgeArticleId, Language, IsLatestVersion, UrlName From KnowledgeArticleVersion WHERE Language =: articlelang AND publishStatus = 'online' AND knowledgeArticleId = :selectedSubType.Article_Id__c LIMIT 1];
    system.debug('KBArtSIze: ' + kbArticleList.size());
    if(kbArticleList.size() > 0){
      kbArticle = kbArticleList[0];
      hasKBArticle = true;
      system.debug('kbArticle: ' + kbArticle);
    }
    else{
      hasKBArticle = false;
      kbArticle = new KnowledgeArticleVersion();
    } 
    
    List<WebToCase_Form__c> formList = [SELECT WebToCase_SubType__c, isActive__c, submitButtonEnabled__c, Name__c, Id, Queue_ID__c, 
                                        (SELECT Id, FieldType__c, Checkbox__c, Text__c, TextArea__c, TextAreaLong__c, RichText__c, 
                                          Label__c, PicklistValues__c, PicklistSlot__c, DateText__c, isRequired__c, Name, GTText__c 
                                          FROM WebToCase_FormFields__r ORDER BY Name ASC) 
                                        FROM WebToCase_Form__c WHERE WebToCase_SubType__c = :selectedSubType.id AND isActive__c = true];
    
    system.debug('FormList: ' + formList);
    
    //If a form Exists:
    if(formList.size() > 0){
      //Init Form:
      formHasSelectList = false;
      selectOptionList1 = new List<SelectOption>();
      selectOptionList2 = new List<SelectOption>();
      selectOptionList3 = new List<SelectOption>();
      selectOptionList4 = new List<SelectOption>();
      selectOptionList5 = new List<SelectOption>();
      formHasErrorMessage = false;
      formErrorMessage = '';
      
      
      customForm = formList[0];
      //formFields = customForm.WebToCase_FormFields__r;
      formFields = new List<WebToCase_FormField__c>();
      for(WebToCase_FormField__c tempField : customForm.WebToCase_FormFields__r){
        
        WebToCase_FormField__c fieldToAdd = new WebToCase_FormField__c();
        fieldToAdd.FieldType__c = tempField.FieldType__c;
        fieldToAdd.Label__c = tempField.Label__c;
        fieldToAdd.Checkbox__c = false;
        fieldToAdd.DateText__c = String.ValueOf(Date.Today());
        fieldToAdd.Text__c = '';
        fieldToAdd.TextArea__c = '';
        fieldToAdd.TextAreaLong__c = tempField.TextAreaLong__c;
        fieldToAdd.isRequired__c = tempField.isRequired__c;
        
        if(tempField.PicklistValues__c != null && tempField.PicklistValues__c != ''){
          List<SelectOption> selectOptions = new List<SelectOption>();
          List<String> picklistVals = tempField.PicklistValues__c.split(',');
          for(String pVal : picklistVals){
            selectOptions.add(new SelectOption(pVal, pVal));
          }
          
          //Assign SelectOption:
          if(selectOptionList1.size() == 0){
            selectOptionList1 = selectOptions;
            fieldToAdd.PicklistSlot__c = 1;
          }
          else if(selectOptionList2.size() == 0){
            selectOptionList2 = selectOptions;
            fieldToAdd.PicklistSlot__c = 2;
          }
          else if(selectOptionList3.size() == 0){
            selectOptionList3 = selectOptions;
            fieldToAdd.PicklistSlot__c = 3;
          }
          else if(selectOptionList4.size() == 0){
            selectOptionList4 = selectOptions;
            fieldToAdd.PicklistSlot__c = 4;
          }
          else if(selectOptionList5.size() == 0){
            selectOptionList5 = selectOptions;
            fieldToAdd.PicklistSlot__c = 5;
          }
          else{
            //5 Is Limit.
          }
          formHasSelectList = true;
        }
        
        
        formFields.add(fieldToAdd);
      }
      submitButtonEnabled = customForm.submitButtonEnabled__c;
      
      //Set All Contact Options To False:
      enablePhone = false;
      enableChat = false;
      enableEmail = false;
      enableForm = true;
    }
    
    if(enableEmail){
      system.debug('Email Enabled');
      emailForm = new WebToCase_Form__c();
      emailFormFields = new List<WebToCase_FormField__c>();
      
      emailForm.Name__c = 'Email';
      emailFormFields.add(new WebToCase_FormField__c(
        Label__c = 'Instructions',
        FieldType__c = 'RichText',
        TextAreaLong__c = 'Please Fill out the information below.'));  
      emailFormFields.add(new WebToCase_FormField__c(
        Label__c = 'Please describe your issue:',
        FieldType__c = 'TextAreaLong'));
      system.debug('email form created');    
    }
  }
  
  public PageReference submitForm(){
    system.debug('SUBMIT FORM CALLED.');
    
    currentCase = new Case();
    RecordType generalSupportType = [SELECT id FROM RecordType WHERE Name = 'General Support' LIMIT 1];
    currentCase.RecordTypeId = generalSupportType.id;
    currentCase.ContactId = currentContact.id;
    currentCase.Type = selectedType.TypeMap__c;
    currentCase.Sub_Type__c = selectedSubType.SubTypeMap__c;
    currentCase.Status = 'Open'; 
    currentCase.Product_Effected__c = selectedTitle.Public_Product__r.id;
    currentCase.Origin = 'Web To Case';
    currentCase.W2C_Origin_Language__c = userLanguage;
    currentCase.W2C_Type__c = selectedType.Description__c;
    currentCase.W2C_SubType__c = selectedSubType.Description__c;
    currentCase.subject =  selectedTitle.Title__c + ' (' + selectedPlatform.Name__c + ')' + ' : ' + selectedType.Description__c + ' (' + selectedSubType.Description__c + ')';    
    currentCase = addDispositionsToCase(
        currentCase, 
        selectedPlatform.Name__c,
        selectedTitle.Title__c,
        selectedType.typeMap__c,
        selectedSubType.subTypeMap__c
    );
    system.debug('CASE SETUP');
    
    if(enableEmail){
      system.debug('EMAIL ENABLED');
      for(WebToCase_FormField__c fField : emailFormFields){
        if(fField.FieldType__c == 'TextAreaLong'){
          currentCase.description = fField.TextAreaLong__c;
        }
      }
    }
    else if (enableForm){
      system.debug('CUSTOM FORM ENABLED SETUP');
      currentCase.description = '';
      for(WebToCase_FormField__c fField : formFields){
        system.debug('FIELD TYPE' + fField.FieldType__c);
        system.debug('FIELD REQ' + fField.isRequired__c);
        if(fField.FieldType__c == 'RichText'){
          //Do Nothing
        }
        else if(fField.FieldType__c == 'Checkbox'){
          currentCase.description += fField.Label__c + ': ' + fField.Checkbox__c + '\n';  
        }
        else if(fField.FieldType__c == 'Text'){
          currentCase.description += fField.Label__c + ': ' + fField.Text__c.remove(':').normalizeSpace() + '\n';
        }
        else if(fField.FieldType__c == 'TextArea'){
          currentCase.description += fField.Label__c + ': ' + fField.TextArea__c.remove(':') + '\n';
        }
        else if(fField.FieldType__c == 'TextAreaLong'){
          if(fField.TextAreaLong__c != null && fField.TextAreaLong__c != ''){
            currentCase.description += fField.Label__c + ': ' + fField.TextAreaLong__c.remove(':') + '\n';  //WHY U SO NULLL
          }
        }
        else if(fField.FieldType__c == 'InputFile'){
        }
        else if(fField.FieldType__c == 'Picklist'){
          currentCase.description += fField.Label__c + ': ' + fField.Text__c.remove(':') + '\n';
        }
        else if(fField.FieldType__c == 'DateText'){
          currentCase.description += fField.Label__c + ': ' + fField.DateText__c.remove(':') + '\n';
        }
        else if(fField.FieldType__c == 'GT Picklist'){
          currentCase.description += fField.Label__c + ': ' + fField.GTText__c.remove(':') + '\n';
        }
        else{
          //Do Nothing
        }
      }
    }
    
    insert currentCase;    
    
    //Create Article:
    if(!System.Test.isRunningTest()){
      
      if(selectedSubType.Article_Id__c != null && selectedSubType.Article_Id__c != ''){
        CaseArticle article = new CaseArticle();
        article.KnowledgeArticleId = selectedSubType.Article_Id__c;
        article.CaseId = currentCase.id;
        insert article;
        system.debug('Article Created: ' + selectedSubType.Article_Id__c);
      }
    }
    
    //Get Case Number:
    List<Case> currentCaseList = [SELECT id, CaseNumber FROM Case WHERE id = :currentCase.id];
    if(currentCaseList.size() > 0){ 
      currentCaseNumber = currentCaseList[0].CaseNumber; 
    }
    
    //Add Attachment:
    system.debug('UFB');
    system.debug('Uploaded File Body: ' + uploadedFileBody);
    if(uploadedFileBody != null){
      if(uploadedFileSize > 150000){
        formErrorMessage = 'Image Too Large.';
        formHasErrorMessage = true;
        uploadedFileName = null;
        uploadedFileBody = null;
        return null;
      }
      if(uploadedFileContentType != 'image/jpeg' && uploadedFileContentType != 'image/png'){
        formHasErrorMessage = true;
        formErrorMessage = 'Invalid Image Type.';
        uploadedFileName = null;
        uploadedFileBody = null;
        return null;
      }
      
      Attachment tmpAttach = new Attachment();
      tmpAttach.ParentId = currentCase.id;
      tmpAttach.Name = uploadedFileName;
      tmpAttach.Body = uploadedFileBody;
      insert tmpAttach;
      
      tmpAttach = null;
      uploadedFileName = null;
      uploadedFileBody = null;
    }
    
    
    system.debug('CUSTOM FORM QUEUE: ' + customForm.Queue_ID__c);
    if(customForm.Queue_ID__c != null){
        currentCase.OwnerId = customForm.Queue_ID__c;
        currentCase.W2C_Route_To_Custom_Queue__c = true;
        update currentCase;  
    }
    else{
      //Hardcode Queues:
      if(userLanguage.contains('en') && SelectedTitle.Title__c.contains('Call of Duty')){
        currentCase.OwnerID = '00GU0000000gYws';
      }
      else if((userLanguage == 'en_US') && (SelectedTitle.Title__c.contains('Skylander'))){
        currentCase.OwnerID = '00GU0000000gYwx';
      }
      else if(userLanguage == 'en_US'){
        currentCase.OwnerID = '00GU0000000gYwv';
      }
      else if(userLanguage == 'nl_NL'){
        currentCase.OwnerID = '00GU0000000gYwk';
      }
      else if(userLanguage == 'en_GB'){
        currentCase.OwnerID = '00GU0000000gYwl';
      }
      else if(userLanguage == 'en_AU'){
        currentCase.OwnerID = '00GU0000000gYwl';
      }
      else if(userLanguage == 'en_NO'){
        currentCase.OwnerID = '00GU0000000gYwl';
      }
      else if(userLanguage == 'en_FI'){
        currentCase.OwnerID = '00GU0000000gYwl';
      }
      else if(userLanguage == 'fr_FR'){
        currentCase.OwnerID = '00GU0000000gYwm';
      }
      else if(userLanguage == 'de_DE'){
        currentCase.OwnerID = '00GU0000000gYwn';
      }
      else if(userLanguage == 'it_IT'){
        currentCase.OwnerID = '00GU0000000gYwo';
      }
      else if(userLanguage == 'pt_BR'){
        currentCase.OwnerID = '00GU0000000gYwp';
      }
      else if(userLanguage == 'es_ES'){
        currentCase.OwnerID = '00GU0000000gYwq';
      }
      else if(userLanguage == 'sv_SE'){
        currentCase.OwnerID = '00GU0000000gYwr';
      }
      else{
        currentCase.OwnerID = '00GU0000000gYwz';
      }
      currentCase.W2C_Route_To_Custom_Queue__c = false;
      update currentCase;  
    }
    showConfirmationPage = true;
    return null;
  }
   
  public static String GetResourceURL(String resourceName)  
  {  
      //Fetching the resource  
      List<StaticResource> resourceList = [SELECT Name, NamespacePrefix, SystemModStamp FROM StaticResource WHERE Name = :resourceName];  
                            
      //Checking if the result is returned or not  
      if(resourceList.size() == 1)  
      {  
         //Getting namespace  
         String namespace = resourceList[0].NamespacePrefix;  
         //Resource URL  
         return '/resource/' + resourceList[0].SystemModStamp.getTime() + '/' + (namespace != null && namespace != '' ? namespace + '__' : '') + resourceName;   
      }  
      else return '';  
  }  
  
  public void relayDetected(){
    //Check for RelayState:
        string subTypeParam = ApexPages.CurrentPage().getParameters().get('st');
        if(subTypeParam != null){
      selectedSubTypeId = subTypeParam;
      //subTypeSelected();
        }
  }
  
  public void doNothing(){
    system.debug('Did Nothing');
  }
  
  public void resetContactOptions(){
    //Set Defaults:
    enableChat = CHAT_DEFAULT;
    enablePhone = PHONE_DEFAULT;
    enableEmail = EMAIL_DEFAULT;
    enableTwitter = TWITTER_DEFAULT;
    enableForum = FORUM_DEFAULT;
    enableForm = FORM_DEFAULT;
    
    //Platform
    if(selectedPlatform != null){
      if(selectedPlatform.disablePhone__c){
        enablePhone = false;
      }
      if(selectedPlatform.disableChat__c){
        enableChat = false;
      }
      if(selectedPlatform.disableEmail__c){
        enableEmail = false;
      }
      if(selectedPlatform.disableTwitter__c){
        enableTwitter = false;
      }
      if(selectedPlatform.disableForum__c){
        enableForum = false;
      }
    }
    
    //Title:
    if(selectedTitle != null){
      if(selectedTitle.disablePhone__c){
        enablePhone = false;
      }
      if(selectedTitle.disableChat__c){
        enableChat = false;
      }
      if(selectedTitle.disableEmail__c){
        enableEmail = false;
      }
      if(selectedTitle.disableTwitter__c){
        enableTwitter = false;
      }
      if(selectedTitle.disableForum__c){
        enableForum = false;
      }
    }
    
    //Type:
    if(selectedType != null){
      if(selectedType.disablePhone__c){
        enablePhone = false;
      }
      if(selectedType.disableChat__c){
        enableChat = false;
      }
      if(selectedType.disableEmail__c){
        enableEmail = false;
      }
      if(selectedType.disableTwitter__c){
        enableTwitter = false;
      }
      if(selectedType.disableForum__c){
        enableForum = false;
      }
    }
    
    //SubType:
    if(selectedSubType != null){
      if(selectedSubType.disablePhone__c){
        enablePhone = false;
      }
      if(selectedSubType.disableChat__c){
        enableChat = false;
      }
      if(selectedSubType.disableEmail__c){
        enableEmail = false;
      }
      if(selectedSubType.disableTwitter__c){
        enableTwitter = false;
      }
      if(selectedSubType.disableForum__c){
        enableForum = false;
      }
    }
  }
  
/*build the multiplayer picklist*/
  public selectOption[] getMultiPlayerAccounts(){
      selectOption[] results = new selectOption[]{};
      if(contactIdOfUser != null){
          this.mpaId2mpa = new map<Id, Multiplayer_Account__c>([SELECT Id, Platform__c, Gamertag__c FROM Multiplayer_Account__c WHERE Contact__c = :contactIdOfUser]);
          for(Id mpaId : this.mpaId2mpa.keySet()){
              results.add(new selectOption(this.mpaId2mpa.get(mpaId).Gamertag__c+' ('+this.mpaId2mpa.get(mpaId).Platform__c+')', this.mpaId2mpa.get(mpaId).Gamertag__c+' ('+this.mpaId2mpa.get(mpaId).Platform__c+')'));
              this.linkedAccountsPresent = true;
          }
      }
      return results;
  }
  
  /*Live Agent Chat Enhancements by Sorna (perficient) - start */
    public string caseId {get;set;}
    public void createCaseAF()
    {
        if(caseId == null){ //condition added by ATVI, users were clicking the button rapidly and creating lots of cases
            Case cs = new Case();
            cs = addDispositionsToCase(
                cs, 
                selectedPlatform.Name__c,
                selectedTitle.Title__c,
                selectedType.typeMap__c,
                selectedSubType.subTypeMap__c
            );
            /*
            cs.Platform__c = selectedPlatform.Name__c;
            cs.Product_L1__c = selectedTitle.Title__c;
            cs.Issue_Type__c = selectedType.typeMap__c;
            cs.Issue_Sub_Type__c = selectedSubType.subTypeMap__c;
            */
            cs.Subject = cs.Issue_Type__c;
            cs.Origin = 'Chat';
            insert cs;
            caseId = cs.Id;
        }
    }
    /* Live Agent Chat Enhancements by Sorna (perficient) - end */
    public static case addDispositionsToCase(Case pCase, String pPlat, String pProd, String pItyp, String pIStyp){
        pCase.Platform__c = pPlat;
        if(new set<String>{'Wii','Wii U','DS','3DS','64','Gamecube'}.contains(pPlat)){
            pCase.Platform__c = 'Nintendo '+pPlat;
        }    
        pCase.Product_L1__c = pProd;
        pCase.Issue_Type__c = pItyp;
        pCase.Issue_Sub_Type__c = pIStyp;
        return pCase;
    }
    public string getChatButtonId(){
        String result = '';
        try{
        if(! isBlankOrNull(selectedSubType.Chat_Button_Id__c)){//subtype
            result = selectedSubType.Chat_Button_Id__c;
        }else if(! isBlankOrNull(selectedType.Chat_Button_Id__c)){//type
            result = selectedType.Chat_Button_Id__c;
        }else if(! isBlankOrNull(selectedTitle.Chat_Button_Id__c)){//gametitle
            result = selectedTitle.Chat_Button_Id__c;
        }else if(! isBlankOrNull(selectedPlatform.Chat_Button_Id__c)){//platform
            result = selectedPlatform.Chat_Button_Id__c;
        }else if(! isBlankOrNull(w2c.Chat_Button_Id__c)){//language
            result = w2c.Chat_Button_Id__c;
        }
        }catch(exception e){
            system.debug(e);
        }
        system.debug('chat Button id = '+result);
        return result;
    }
}