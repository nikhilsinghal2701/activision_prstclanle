/////////////////////////////////////////////////////////////////////////////
// @Name             Contact_Us_Controller_Test
// @author           Jon Albaugh
// @date             07-NOV-2042
// @description      Test For (Controller for: Contact_Us_Controller)
/////////////////////////////////////////////////////////////////////////////

@istest
public with sharing class Contact_Us_Controller_Test{

    public static testMethod void test(){
    	
    	//Create Contact:
		Contact testContact = new Contact();
		testContact.FirstName = 'testFirstName';
		testContact.LastName = 'testLastName';
		testContact.Email = 'testEmail@testEmail.com';
		testContact.Birthdate = date.newinstance(1990, 2, 2);
		testContact.UCDID__c = 'abc';
		insert testContact;
		
		//Create User:
		Profile p = [SELECT Id FROM Profile WHERE Name='ATVI OHV Cust Port User']; 
    	User u = new User(Alias = 'standt', Email='ATVIOHVuser@testorg.com', 
      		EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      		LocaleSidKey='en_US', ProfileId = p.Id, 
      		TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
    	u.ContactId = testContact.id;
		
		
		system.runas(u){
        	ApexPages.StandardController stdController = new ApexPages.StandardController(testContact);

        	WebToCase__c w2c = new WebToCase__c();
        	w2c.Language__c = 'en_US';
        	w2c.chatEnabled__c = true;
        	w2c.chatEnabledMessage__c = 'tstmsg';
        	w2c.chatDisabledMessage__c = 'chatDisMsg';
        	w2c.phoneEnabled__c = true;
        	w2c.chatOfflineMessage__c = 'offlineMsg';
        	w2c.phoneDisabledMessage__c = 'pDisabledMsg';
        	w2c.phone_Hours__c = '01:00 GMT';
        	w2c.Phone_Number__c = '100';
        	w2c.Chat_Weekday_Start__c = '01:00GMT';
        	w2c.Chat_Weekday_End__c = '19:00GMT';
        	w2c.Chat_Weekend_Start__c = '01:00GMT';
        	w2c.Chat_Weekend_End__c = '19:00GMT';
        	insert w2c;
        	
        	List<WebToCase_Platform__c> testPlatforms = new List<WebToCase_Platform__c>();
        	//Create Dummy Platforms:
        	WebToCase_Platform__c platform1 = new WebToCase_Platform__c();
        	platform1.Name__c = 'Xbox';
        	platform1.Image__c = 'xbox';
        	platform1.isActive__c = true;
        	platform1.disableChat__c = false;
        	platform1.disablePhone__c = false;
        	platform1.disableEmail__c = false;
        	platform1.WebToCase__c = w2c.id;
        	testPlatforms.add(platform1);
        	
        	//WebToCase_Platform__c platform2 = new WebToCase_Platform__c();
        	//platform2.Name__c = 'Ps3';
        	//platform2.Image__c = 'ps3';
        	//platform2.isActive__c = true;
        	//platform2.disableChat__c = true;
        	//platform2.disablePhone__c = false;
        	//platform2.disableEmail__c = false;
        	//platform2.WebToCase__c = w2c.id;
        	//testPlatforms.add(platform2);
        	
        	//WebToCase_Platform__c platform3 = new WebToCase_Platform__c();
        	//platform3.Name__c = 'Wii';
        	//platform3.Image__c = 'wii';
        	//platform3.isActive__c = true;
        	//platform3.disableChat__c = false;
        	//platform3.disablePhone__c = true;
        	//platform3.disableEmail__c = false;
        	//platform3.WebToCase__c = w2c.id;
        	//testPlatforms.add(platform3);
        	
        	//WebToCase_Platform__c platform4 = new WebToCase_Platform__c();
        	//platform4.Name__c = 'PC';
        	//platform4.Image__c = 'PC';
        	//platform4.isActive__c = true;
        	//platform4.disableChat__c = false;
        	//platform4.disablePhone__c = false;
        	//platform4.disableEmail__c = true;
        	//platform4.WebToCase__c = w2c.id;
        	//testPlatforms.add(platform4);
        	
        	insert testPlatforms;
        	
        	//Start Test:
        	Contact_Us_Controller contactController = new Contact_Us_Controller(stdController);
        	
        	Public_Product__c pubProduct = new Public_Product__c();
        	insert pubProduct;
        	
        	//Create Test Titles:
        	List<WebToCase_GameTitle__c> testTitles = new List<WebToCase_GameTitle__c>();
        	
        	for(WebToCase_Platform__c platform : testPlatforms){
	        	WebToCase_GameTitle__c testTitle1 = new WebToCase_GameTitle__c();
	        	testTitle1.Title__c = 'title';
	        	testTitle1.Image__c = 'titleImg';
	        	testTitle1.isActive__c = true;
	        	testTitle1.disableChat__c = false;
	        	testTitle1.disablePhone__c = false;
	        	testTitle1.disableEmail__c = false;
	        	testTitle1.isOther__c = false;
	        	testTitle1.WebToCase_Platform__c = platform.id;
	        	testTitle1.Language__c = w2c.Language__c;
	        	testTitle1.Public_Product__c = pubProduct.id;
        		testTitles.add(testTitle1);
        	
        	}
        	
        	insert testTitles;
        	
        	//Create Test Types:
        	List<WebToCase_Type__c> testTypes = new List<WebToCase_Type__c>();
        	
        	for(WebToCase_GameTitle__c gTitle : testTitles)
        	{
	        	WebToCase_Type__c testType1 = new WebToCase_Type__c();
	        	testType1.Description__c = 'type1';
	        	testType1.isActive__c = true;
	        	testType1.disableChat__c = false;
	        	testType1.disablePhone__c = false;
	        	testType1.disableEmail__c = false;
	        	testType1.typeMap__c = 'testTypeMap';
	        	testType1.WebToCase_GameTitle__c = gTitle.id;
	    		testTypes.add(testType1);
	    		
        	}
        	
        	insert testTypes;
        	
        	//Create Test SubTypes
        	List<WebToCase_SubType__c> testSubTypes = new List<WebToCase_SubType__c>();
        	
        	//for(WebToCase_Type__c tType : testTypes)
        	//{
        		WebToCase_Type__c tType = testTypes[0];
        	
	        	WebToCase_SubType__c testSubType1 = new WebToCase_SubType__c();
	        	testSubType1.Description__c = 'subType1';
	        	testSubType1.isActive__c = true;
	        	testSubType1.disableChat__c = false;
	        	testSubType1.disablePhone__c = false;
	        	testSubType1.disableEmail__c = false;
	        	testSubType1.SubTypeMap__c = 'testSubTypeMap';
	        	testSubType1.WebToCase_Type__c = tType.id;
	        	testSubType1.Article_ID__C = '';
	    		testSubTypes.add(testSubType1);
	    		
	    		WebToCase_SubType__c testSubType2 = new WebToCase_SubType__c();
	        	testSubType2.Description__c = 'subType2';
	        	testSubType2.isActive__c = true;
	        	testSubType2.disableChat__c = true;
	        	testSubType2.disablePhone__c = false;
	        	testSubType2.disableEmail__c = false;
	        	testSubType2.SubTypeMap__c = 'testSubTypeMap';
	        	testSubType2.WebToCase_Type__c = tType.id;
	        	testSubType2.Article_ID__C = '';
	    		testSubTypes.add(testSubType2);
	    		
	    		WebToCase_SubType__c testSubType3 = new WebToCase_SubType__c();
	        	testSubType3.Description__c = 'subType3';
	        	testSubType3.isActive__c = true;
	        	testSubType3.disableChat__c = false;
	        	testSubType3.disablePhone__c = true;
	        	testSubType3.disableEmail__c = false;
	        	testSubType3.SubTypeMap__c = 'testSubTypeMap';
	        	testSubType3.WebToCase_Type__c = tType.id;
	        	testSubType3.Article_ID__C = '';
	    		testSubTypes.add(testSubType3);
	    		
	    		WebToCase_SubType__c testSubType4 = new WebToCase_SubType__c();
	        	testSubType4.Description__c = 'subType4';
	        	testSubType4.isActive__c = true;
	        	testSubType4.disableChat__c = false;
	        	testSubType4.disablePhone__c = false;
	        	testSubType4.disableEmail__c = true;
	        	testSubType4.SubTypeMap__c = 'testSubTypeMap';
	        	testSubType4.WebToCase_Type__c = tType.id;
	        	testSubType4.Article_ID__C = '';
	    		testSubTypes.add(testSubType4);
        	//}
        	insert testSubTypes;
        	
        	//Create a Subtype with a Form:
        	WebToCase_SubType__c testSubTypeForm = new WebToCase_SubType__c();
        	testSubTypeForm.Description__c = 'subType4';
        	testSubTypeForm.isActive__c = true;
        	testSubTypeForm.disableChat__c = false;
        	testSubTypeForm.disablePhone__c = false;
        	testSubTypeForm.disableEmail__c = true;
        	testSubTypeForm.SubTypeMap__c = 'testSubTypeMap';
        	testSubTypeForm.WebToCase_Type__c = testTypes[0].id;
        	testSubTypeForm.Article_ID__C = '';
    		insert testSubTypeForm;
        	
        	WebToCase_Form__c testForm = new WebToCase_Form__c();
        	testForm.Name__c = 'testForm';
        	testForm.submitButtonEnabled__c = true;
        	
        	testForm.Queue_ID__c = '';
        	testForm.WebToCase_SubType__c = testSubTypeForm.id;
        	insert testForm;
        	
        	List<WebToCase_FormField__c> testFormFields = new List<WebToCase_FormField__c>();
        	WebToCase_FormField__c testFormField1 = new WebToCase_FormField__c();
        	testFormField1.FieldType__c = 'Checkbox';
        	testFormField1.Label__c = 'LABEL';
        	testFormField1.Checkbox__c = true;
        	testFormField1.WebToCase_Form__c = testForm.id;
        	testFormFields.add(testFormField1);
        	
        	WebToCase_FormField__c testFormField2 = new WebToCase_FormField__c();
        	testFormField2.FieldType__c = 'RichText';
        	testFormField2.Label__c = 'LABEL';
        	testFormField2.TextAreaLong__c = 'ABC123';
        	testFormField2.WebToCase_Form__c = testForm.id;
        	testFormFields.add(testFormField2);
        	
        	WebToCase_FormField__c testFormField3 = new WebToCase_FormField__c();
        	testFormField3.FieldType__c = 'Text';
        	testFormField3.Label__c = 'LABEL';
        	testFormField3.Text__c = 'TEST';
        	testFormField3.WebToCase_Form__c = testForm.id;
        	testFormFields.add(testFormField3);
        	
        	WebToCase_FormField__c testFormField4 = new WebToCase_FormField__c();
        	testFormField4.FieldType__c = 'TextArea';
        	testFormField4.Label__c = 'LABEL';
        	testFormField4.TextArea__c = 'TextAreaStuff';
        	testFormField4.WebToCase_Form__c = testForm.id;
        	testFormFields.add(testFormField4);
        	
        	WebToCase_FormField__c testFormField5 = new WebToCase_FormField__c();
        	testFormField5.FieldType__c = 'TextAreaLong';
        	testFormField5.Label__c = 'LABEL';
        	testFormField5.TextAreaLong__c = 'TestAreaLongText';
        	testFormField5.WebToCase_Form__c = testForm.id;
        	testFormFields.add(testFormField5);
        	insert testFormFields;
        	
        	
        	Test.StartTest();
        	for(WebToCase_Platform__c platform : testPlatforms){
        		contactController.selectedPlatformID = platform.id;
        		contactController.platformChanged();
        	}
        	
        	for(WebToCase_GameTitle__c gTitle : testTitles){
        		contactController.selectedTitleID = gTitle.id;
        		contactController.titleChanged();
        	}
        	
        	//Without Form:
    		contactController.selectedSubTypeID = testSubTypes[0].id;
    		contactController.subTypeSelected();
    		contactController.submitForm();
    		
    		//With Form:
    		contactController.selectedSubTypeID = testSubTypeForm.id;
    		contactController.subTypeSelected();
    		contactController.submitForm();
        	
        	
		}
    }
}