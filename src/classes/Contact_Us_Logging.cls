public without sharing class Contact_Us_Logging {
	private final sObject theRec;
	public Contact_Us_Logging(ApexPages.StandardController stdCtrlr) {
		theRec = stdCtrlr.getRecord();
	}
	public void doLog(){
		try{
			Integer currentCount;
			try{
				currentCount = Integer.valueOf(theRec.get('Click_throughs__c'));
			}catch(Exception ex){}
			if(currentCount == null) currentCount = 0;
			currentCount++;
			theRec.put('Click_throughs__c', currentCount);
			update theRec;
		}catch(Exception e){
			system.debug('LOGGING FAIL == '+e);
		}

	}
}