@isTest
private class Contact_Us_Logging_Test {
	
	@isTest static void test_method_1() {
		
		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
        insert testproduct;

        WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US', Contact_Options_Order__c = 'chat;twitter;facebook;forum;phone;email;form;ambassadorchat;scheduleacall', Chat_Weekday_Start__c = '00:00GMT', Chat_Weekday_End__c = '23:00GMT', Chat_Weekend_Start__c = '00:00GMT', Chat_Weekend_End__c = '23:00GMT', ForumEnabled__c = true, FacebookEnabled__c = true, AmbassadorChatEnabled__c = true, ScheduleACallEnabled__c = true, TwitterEnabled__c = true, phoneEnabled__c = true);
        insert w2c1;

        WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(WebToCase__c = w2c1.Id, Name__c = 'Test Platform', isActive__c = true,disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Phone__c = '50-Ultimate Premium', Level_Required_To_Access_AmbassadorChat__c = '50-Ultimate Premium');
        insert w2cp1;

        WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Test Title', disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Form__c = '50-Ultimate Premium', Level_Required_To_Schedule_A_Call__c = '50-Ultimate Premium');
        insert w2cg1;

        WebToCase_Type__c w2ct1 = new WebToCase_Type__c(TypeMap__c = 'Test Type', WebToCase_GameTitle__c = w2cg1.Id, Description__c = 'Test Type', isActive__c = true, disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Forum__c = '50-Ultimate Premium', Level_Required_To_Access_Twitter__c = '50-Ultimate Premium');
        insert w2ct1;

        WebToCase_SubType__c w2cst1 = new WebToCase_SubType__c(SubTypeMap__c = 'Test SubType', Description__c = 'Test SubType', WebToCase_Type__c = w2ct1.Id, isActive__c = true, disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Facebook__c = '50-Ultimate Premium', Click_Throughs__c = 0);
        insert w2cst1;

        ApexPages.StandardController acon = new ApexPages.StandardController(w2cst1);

        Contact_Us_Logging cul = new Contact_Us_Logging(acon);

        cul.doLog();

        system.assertEquals(w2cst1.Click_Throughs__c,1);

    }

    @isTest static void test_method_2() {
		
		Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
        insert testproduct;

        WebToCase__c w2c1 = new WebToCase__c(Language__c = 'en_US', Contact_Options_Order__c = 'chat;twitter;facebook;forum;phone;email;form;ambassadorchat;scheduleacall', Chat_Weekday_Start__c = '00:00GMT', Chat_Weekday_End__c = '23:00GMT', Chat_Weekend_Start__c = '00:00GMT', Chat_Weekend_End__c = '23:00GMT', ForumEnabled__c = true, FacebookEnabled__c = true, AmbassadorChatEnabled__c = true, ScheduleACallEnabled__c = true, TwitterEnabled__c = true, phoneEnabled__c = true);
        insert w2c1;

        WebToCase_Platform__c w2cp1 = new WebToCase_Platform__c(WebToCase__c = w2c1.Id, Name__c = 'Test Platform', isActive__c = true,disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Phone__c = '50-Ultimate Premium', Level_Required_To_Access_AmbassadorChat__c = '50-Ultimate Premium');
        insert w2cp1;

        WebToCase_GameTitle__c w2cg1 = new WebToCase_GameTitle__c(WebToCase_Platform__c = w2cp1.Id, Public_Product__c = testproduct.Id, Title__c = 'Test Title', disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Form__c = '50-Ultimate Premium', Level_Required_To_Schedule_A_Call__c = '50-Ultimate Premium');
        insert w2cg1;

        WebToCase_Type__c w2ct1 = new WebToCase_Type__c(TypeMap__c = 'Test Type', WebToCase_GameTitle__c = w2cg1.Id, Description__c = 'Test Type', isActive__c = true, disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Forum__c = '50-Ultimate Premium', Level_Required_To_Access_Twitter__c = '50-Ultimate Premium');
        insert w2ct1;

        WebToCase_SubType__c w2cst1 = new WebToCase_SubType__c(SubTypeMap__c = 'Test SubType', Description__c = 'Test SubType', WebToCase_Type__c = w2ct1.Id, isActive__c = true, disableForum__c = false, disableTwitter__c = false, disablePhone__c = false, Level_Required_To_Access_Facebook__c = '50-Ultimate Premium', Click_Throughs__c = 0);
        insert w2cst1;

        ApexPages.StandardController acon = new ApexPages.StandardController(w2ct1);

        Contact_Us_Logging cul = new Contact_Us_Logging(acon);

        cul.doLog();
    }
	
	
}