public class Contact_Us_Mobile{
    /*
    ** Contact Us Mobile
    */
    //Switching from Id based maps to String based maps per SFDC Summer '13 known issue of map id failures
    public WebToCase_GameTitle__c[] platforms {get; set;}
    public map<String, WebToCase_SubType__c[]> typeIdToSubTypes {get; set;}
    
    public map<String, FAQ__kav> articleIdToArticle {get; set;}
    public map<String, String> subTypeIdToArticleId {get; set;}
    
    public map<String, WebToCase_Form__c> formIdToForm {get; set;}
    public map<String, String> subTypeIdToFormId {get; set;}   
    public map<String, map<String, FormResponse>> formIdToFormResponses {get; set;}
    
    public Contact_Us_Mobile(String dataCategory, String uLang){
        platforms = [SELECT Id, ref_Platform__c, (SELECT Id, TypeMap__c, Description__c 
                                                  FROM WebToCase_Types__r 
                                                  WHERE isActive__c = TRUE ORDER BY Sort_Position__c ASC)
                     FROM WebToCase_GameTitle__c 
                     WHERE Image__c = :dataCategory AND 
                           IsActive__c = TRUE AND 
                           WebToCase_Platform__r.IsActive__c = TRUE AND
                           ref_Language__c = :uLang 
                     ORDER BY WebToCase_Platform__r.Sort_Order__c ASC];
        if(platforms == null) platforms = new WebToCase_GameTitle__c[]{};
        typeIdToSubTypes = new map<String, WebToCase_SubType__c[]>{null => new WebToCase_SubType__c[]{}};
        
        for(WebToCase_GameTitle__c game : platforms){
            for(WebToCase_Type__c wType : game.WebToCase_Types__r){
                typeIdToSubTypes.put(wType.Id, new WebToCase_SubType__c[]{});
            }
        }
        subTypeIdToArticleId = new map<String, String>{null => null};
        subTypeIdToFormId = new map<String, String>{null => null};
        for(WebToCase_SubType__c subType : [SELECT Id, Description__c, Article_Id__c, WebToCase_Type__c,
                                            (SELECT Id FROM WebToCase_Forms__r WHERE IsActive__c = TRUE LIMIT 1)
                                            FROM WebToCase_SubType__c 
                                            WHERE IsActive__c = TRUE AND WebToCase_Type__c IN :typeIdToSubTypes.keySet() 
                                            ORDER BY Sort_Position__c ASC])
        {
            WebToCase_SubType__c[] subTypeList = typeIdToSubTypes.get(subType.WebToCase_Type__c);
            subTypeList.add(subType);
            typeIdToSubTypes.put(subType.WebToCase_Type__c, subTypeList);
            
            if(subType.Article_Id__c == null){
                subTypeIdToArticleId.put(subType.Id, subType.Id);
            }else{
                subTypeIdToArticleId.put(subType.Id, subType.Article_Id__c);
            }
            
            if(subType.WebToCase_Forms__r != null && subType.WebToCase_Forms__r.size() > 0){
                subTypeIdToFormId.put(subType.Id, subType.WebToCase_Forms__r[0].Id);
            }else{
                subTypeIdToFormId.put(subType.Id, subType.Id);
            }
            
        }
        
        //get Articles
        //Business stores the static FAQ__ka id, which we need to translate into the current version FAQ__kav id
        //subquery would have been perfect, but noooo, it doesn't look like we can subquery from __ka down to __kav
        
        articleIdToArticle = new map<string, FAQ__kav>();
        String kbLang = convertUlang2kbLang(uLang);
        for(FAQ__ka article : [SELECT Id FROM FAQ__ka WHERE Id IN :subTypeIdToArticleId.values()]){
            boolean missingVersion = true;
            for(FAQ__kav version : [SELECT Id, Title, Summary, Answer_Mobile__c FROM FAQ__kav WHERE KnowledgeArticleId = :article.Id AND Language = :kbLang AND PublishStatus = 'ONLINE' LIMIT 1]){
                articleIdToArticle.put(article.Id, version);
                missingVersion = false;
            }
            if(missingVersion)
                articleIdToArticle.put(article.Id, new FAQ__kav());
        }
        for(String key : subTypeIdToArticleId.keySet()){//ensure the article is in the results (maybe it's been archived)
            if(key != null){
                String storedArticleId = subTypeIdToArticleId.get(key);
                try{
                    Id properId = Id.valueOf(storedArticleId);
                    String Lversion = String.valueOf(properId);
                    if(articleIdToArticle.containsKey(Lversion)){ //18 digit version found, add 15 digit version to map
                        articleIdToArticle.put(Lversion.substring(0, 15), articleIdToArticle.get(Lversion));
                    }else if(articleIdToArticle.containsKey(Lversion.substring(0, 15))){
                        articleIdToArticle.put(Lversion, articleIdToArticle.get(Lversion.substring(0, 15))); //15 digit found, add 18 digit version
                    }else{
                        //neither version present add blank article under each key version
                        articleIdToArticle.put(properId, new FAQ__kav()); 
                        articleIdToArticle.put(lVersion.subString(0, 15), new FAQ__kav()); 
                    }
                }catch(Exception e){
                    articleIdToArticle.put(key, new FAQ__kav()); //most likely a id.valueOf error
                }
            }
        }
        
        
        //get Forms
        formIdToForm = new map<String, WebToCase_Form__c>([SELECT Id, Name__c, SubmitButtonEnabled__c, Queue_Id__c, RouteToCustomQueue__c,
                                                           ref_Language__c, ref_Platform__c, ref_GameTitle__c, ref_Type__c, ref_SubType__c,
                                                           WebToCase_SubType__r.WebToCase_Type__r.WebToCase_GameTitle__r.Public_Product__c,
                                                           WebToCase_SubType__r.WebToCase_Type__r.WebToCase_GameTitle__r.Title__c,
                                                           WebToCase_SubType__r.WebToCase_Type__r.Description__c,
                                                           WebToCase_SubType__r.WebToCase_Type__r.TypeMap__c,
                                                           WebToCase_SubType__r.SubTypeMap__c,
                                                           WebToCase_SubType__r.Description__c, WebToCase_SubType__r.Article_Id__c,
                                                           (SELECT Id, Label__c, FieldType__c, RichText__c, TextAreaLong__c, DateText__c,
                                                            IsRequired__c, PicklistSlot__c, PicklistValues__c, GTText__c, WebToCase_Form__c
                                                            FROM WebToCase_FormFields__r ORDER BY Name ASC) 
                                                           FROM WebToCase_Form__c WHERE Id IN :subTypeIdToFormId.values() AND IsActive__c = true]);
        if(formIdToForm == null) formIdToForm = new map<String, WebToCase_Form__c>();
        for(String key : subTypeIdToFormId.keySet()){
            if(! formIdToForm.containsKey(subTypeIdToFormId.get(key))){
                formIdToForm.put(key, new WebToCase_Form__c());
            }
        }
        formIdToFormResponses = new map<String, map<String, FormResponse>>();
        for(String key : formIdToForm.keySet()){
            map<String, FormResponse> responses = new map<String, FormResponse>();
            for(WebToCase_FormField__c answer : formIdToForm.get(key).WebToCase_FormFields__r){
                responses.put(answer.Id, new FormResponse(answer));
            }
            formIdToFormResponses.put(key, responses);
        }
        
    }
    public string convertUlang2kbLang(String pUlang){
        if(pUlang == null || pUlang == '') return 'en_US';
        if(pUlang.toLowerCase().contains('de')) return 'de';
        if(pUlang.toLowerCase().contains('fr')) return 'fr';
        if(pUlang.toLowerCase().contains('it')) return 'it';
        if(pUlang.toLowerCase().contains('es')) return 'es';
        return 'en_US';
    }
    public class FormResponse{
        public WebToCase_FormField__c question {get; set;}
        public string answer {get; set;}
        public boolean boolAnswer {get; set;}
        public selectOption[] selectOptions {get; set;}
        public FormResponse(WebToCase_FormField__c pQuestion){
            if(pQuestion.FieldType__c == 'Picklist'){
                this.selectOptions = new selectOption[]{};
                for(String option : pQuestion.PicklistValues__c.split(',')){
                    this.selectOptions.add(new selectOption(option, option));
                }
            }
            this.answer = '';
            this.question = pQuestion;
        }
    }

}