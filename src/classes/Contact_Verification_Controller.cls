/////////////////////////////////////////////////////////////////////////////
// @Name             Contact_Verification_Controller
// @author           Jon Albaugh
// @date created            24-SEP-2012
// @date LastModified       07-OCT-2012
// @description      Controller for: Contact_Verification page. 
//                     This controller will email the customer with a verfication code.
/////////////////////////////////////////////////////////////////////////////

public class Contact_Verification_Controller{
    
    public Contact selectedContact { get; set; }
    public string verificationCode{ get; set; }
    public boolean sendButtonDisabled { get; set; }
    public string sendButtonText { get; set; }
    public boolean userHasVerificationCode { get; set; }
    
    //Constructor: Set the selected contact.
    public Contact_Verification_Controller(ApexPages.StandardController controller){
        selectedContact = (Contact)controller.getRecord();
        selectedContact = [SELECT id, UCDID__c, Name, Verification_Code__c, email FROM Contact WHERE id = :selectedContact.id]; 
        verificationCode = selectedContact.Verification_Code__c;
        sendButtonDisabled = false;
        if (verificationCode != null && verificationCode != '')
        {
            sendButtonText = 'RESEND';
            userHasVerificationCode = true;
        }
        else
        {
            sendButtonText = 'SEND';
            userHasVerificationCode = false;
        }
    }

    public pagereference sendEmail(){
        if(selectedContact.Verification_Code__c != null && selectedContact.Verification_Code__c != '')
        {
            verificationCode =  selectedContact.Verification_Code__c;
        }
        else
        {
            system.debug('null verification code');
            system.debug(selectedContact.Verification_Code__c);
            datetime myDateTime = datetime.now();
            
           	string timeString = String.valueof(myDateTime.getTime());
           	integer timeLen = timeString.length();
            
            string verLeft = timeString.subString((timeLen - 6), (timeLen - 3));
            string verRight = timeString.subString((timeLen -3), timeLen);
            
            verificationCode = verLeft + '-' + verRight;
            selectedContact.Verification_Code__c = verificationCode;
            update selectedContact;
        }
        
        //Retrieve Email Template:
        EmailTemplate eTemplate = [Select Id, Name From EmailTemplate Where Name = 'Contact Verification Template' LIMIT 1];        
               
        Messaging.reserveSingleEmailCapacity(1);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setReplyTo('noreply@activision.com');
        mail.setOrgWideEmailAddressId('0D2U0000000GmvH');
        mail.setTargetObjectId(selectedContact.id);
		mail.setTemplateId(eTemplate.id);
		
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
        sendButtonDisabled = true;
        sendButtonText = 'SENT';
        
        return null;
    }   
}