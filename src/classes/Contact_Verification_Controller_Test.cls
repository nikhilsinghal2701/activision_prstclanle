@isTest
public class Contact_Verification_Controller_Test {
	
	public static testMethod void testFunction(){
		//Create Contact:
		Contact testContact = new Contact();
		testContact.FirstName = 'testFirstName';
		testContact.LastName = 'testLastName';
		testContact.Email = 'testEmail@testEmail.com';
		testContact.Birthdate = date.newinstance(1960, 2, 17);
		testContact.UCDID__c = 'abc';
		insert testContact;
		
		//Create Controller:
		ApexPages.StandardController stdController = new ApexPages.StandardController(testContact);
		Contact_Verification_Controller extController = new Contact_Verification_Controller(stdController);
		
		PageReference pRef = new PageReference('');
		
		extController.sendEmail();
		
		//Test Resend:
		Contact_Verification_Controller extController2 = new Contact_Verification_Controller(stdController);
		extController2.sendEmail();
	}
}