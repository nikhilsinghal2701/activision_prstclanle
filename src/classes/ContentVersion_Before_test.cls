@isTest
public class ContentVersion_Before_test{
    public static testMethod void test1(){
        ContentVersion[] cvs = new ContentVersion[]{};
        Id recId = [SELECT id FROM RecordType WHERE Name = 'Code repository codes'].Id;
        ContentWorkspace wp = [SELECT id FROM ContentWorkspace WHERE Name LIKE '%repository%' LIMIT 1];
        for(String month : new String[]{'January','February','March','April','May',
                                        'June','July','August','September','October','November','December'})
        {
            
            ContentVersion cv = new ContentVersion();
            cv.VersionData = Blob.valueOf('asdf');
            cv.PathOnClient = '/'+month+'.txt';
            cv.FirstPublishLocationId = wp.Id;
            cv.RecordTypeId = recId;
            cv.Activation_Year__c = '2014';
            cv.Activation_Month__c = month;
            cv.Activation_Day__c = '1';
            
            cv.Expiration_Year__c = '2015';
            cv.Expiration_Month__c = month;
            cv.Expiration_Day__c = '1';
            
            cv.Title = month+'.txt';
            cvs.add(cv);
        }
        
        insert cvs;   
    }
}