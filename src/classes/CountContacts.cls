global class CountContacts implements Database.Batchable<sObject>{
	global integer conWithTitleCount;
	global integer allConCount;
	
	global final String gstrQuery = 'SELECT id, (Select Id From My_Titles__r) FROM Contact';
    
    global Database.QueryLocator start(Database.BatchableContext BC){
    	
		return Database.getQueryLocator(gstrQuery);
    } 
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    	conWithTitleCount = 0;
    	allConCount = 0;
 		List<Contact> listAccount = new List<Contact>();
      		for(SObject objSObject : scope){
      			Contact tmpContact = (Contact)objSOBject;
      			//system.debug(tmpContact + 'X');
      			//ystem.debug(tmpContact.My_Titles__r + 'nullCheck');
      			//system.debug(tmpContact.My_Titles__r.size());
      			//List<Asset_Custom__c> testList = (List<Asset_Custom__c>)objSObject.get('My_Titles__r');
      			//system.debug(testList);
      			
      			system.debug('MyTitles:' + objSobject);
				if (tmpContact.My_Titles__r.size() != 0){
					conWithTitleCount++;
				}
				system.debug(allConCount);
				allConCount++;	 		
	       }	 
	       Contact me = new Contact();
	       me = [select Address_Line_1__c, Address_Line_2__c from Contact where id='003U000000Ekdx2IAB'];
           me.Address_Line_1__c=string.valueof(integer.valueof(me.Address_Line_1__c) + conWithTitleCount);
           me.Address_Line_2__c=string.valueof(integer.valueof(me.Address_Line_2__c) + allConCount);
           update me;

		  
    }
    
    	global void finish(Database.BatchableContext BC){
    		system.debug('Contacts with Titles: ' + conWithTitleCount);
    		system.debug('All Contacts: ' + allConCount);
    }

}