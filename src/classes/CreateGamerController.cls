public class CreateGamerController {

    


    
    public String searchUserMail { get; set; }
    public Boolean Display{get;set;}
    public Boolean Display2{get;set;}
    public Boolean Display3{get;set;}
    public Boolean Display4{get;set;}
    
    public Contact contactObj{get;set;} 
    public list<Contact> cnt{get;set;}
    
    public PageReference Search() {

        cnt = [select id,Email,LastName,phone,FirstName from Contact where Email = : contactObj.EMail ];
       
        if(cnt.size()==1) {
            pagereference p = new pagereference('/'+ cnt[0].id);
            p.setredirect(true);
            return p;
        } 
        else if(cnt.size()>1) {
            Display = false;
            Display2 = false ;
            Display3 = false;
            Display4 = true ;    
        }
        else 
        {
            Display = false ; 
            Display2 = false ;
            Display3 = true;
               
        }
        return null;
    }
    
    public CreateGamerController () {
        
        Display = true ;
        contactObj = new Contact();
        //contactObj.Country__c = 'United States Of America';
         
    }
    
    public PageReference Save() {
        
        //Display = false ; 
        //Display2 = true;
        //Display3 = false ;
        
        contact c = new contact();
        //c.firstname = contactObj.firstname;
        c.LastName = contactObj.LastName;
        c.Email = contactObj.EMail;
        c.firstname = contactObj.firstname ;
        c.phone =contactObj.phone;
        //c.Country__c = contactObj.Country__c ;
        insert c ;
        pagereference p = new pagereference('/'+c.id);
        p.setredirect(true);
        return p;
    }
    
    public PageReference ReturnPage() {
        
        pagereference p = new pagereference('/apex/CreateGamerPage');
        p.setredirect(true);
        return p;
        
    }


    public PageReference CreateGamer() {
        
        Display = false ; 
        Display2 = true;
        Display3 = false ;
        return null;
        
    }
    
    public pagereference EditRecord() {
        string s = ApexPages.currentPage().getParameters().get('sendId');
       
        if(s != null) {
            // pagereference p = new pagereference('/'+s+'/e?retURL=%2F'+s);
             pagereference p = new pagereference('/'+s);
             p.setRedirect(true);
             return p;    
        }
        
        return null;
    }

}