public with sharing class CreateNewsFlashController{

    public Newsflash__c theNewsflash{get;set;}
    public Case theCase{get;set;}
    public selectOption[] l3options{get;set;}
    public boolean GrayOut{get;set;}
    public boolean ppReq{get;set;}

    public Id RecTypeSU {get;set;}
    public Id RecTypeOA {get;set;}
    public Id RecTypePN {get;set;}
      
    public CreateNewsFlashController(ApexPages.StandardController stdController){
     try{
         RecTypeSU = newsflash__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('System Update').getRecordTypeId();
         RecTypeOA = newsflash__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Outage Alert').getRecordTypeId();
         RecTypePN = newsflash__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Product Newsflash').getRecordTypeId();
            theCase= new Case();
            this.theNewsflash = (Newsflash__c)stdController.getRecord();
            if(ApexPages.currentPage().getParameters().get('RecordType')!=null)
                this.theNewsflash.recordtypeid=ApexPages.currentPage().getParameters().get('RecordType');
            if(ApexPages.currentPage().getParameters().get('clone')!=null && ApexPages.currentPage().getParameters().get('clone')=='1'){
                this.theNewsflash=this.theNewsflash.clone(false, true);
             }
            
            GrayOut = true;
            if(this.theNewsflash.Product_L1__c != null && this.theNewsflash.Product_L1__c != ''){
                if(this.theNewsflash.Product_L1__c == this.theNewsflash.Product_L2__c){
                    this.theNewsflash.Product_L1__c = 'Other';
                }
    
                getL3s(); //load the picklist with the values
                if(this.theNewsflash.L3__c != null && this.theNewsflash.L3__c != ''){ //an l3 has already been chosen, let's make it shown as selected
                    l3Options.remove(0); //remove none from the list
                    for(integer i = 0; i < l3Options.size(); i++){ //remove the duplicate entry
                        if(l3Options[i].getValue() == theNewsflash.L3__c){
                            l3Options.remove(i);
                            break;
                        }
                    }
                    if(l3Options.isEmpty()) l3Options.add(new selectOption(this.theNewsflash.L3__c, this.theNewsflash.L3__c));
                    else l3Options.add(0, new selectOption(this.theNewsflash.L3__c, this.theNewsflash.L3__c)); //add existing l3 as selected
                }
            }else setupL3();
            
            theCase.Product_L1__c=theNewsflash.Product_L1__c;
            theCase.Product_L2__c=theNewsflash.Product_L2__c;
            theCase.Sub_Product_DLC__c=theNewsflash.Sub_Product_DLC__c;
            theCase.Issue_Type__c=theNewsflash.Issue_Type__c;
            theCase.Issue_Sub_Type__c=theNewsflash.Issue_Sub_Type__c;
            theCase.L3__c=theNewsflash.L3__c;
            theCase.Platform__c=theNewsflash.Platform__c;
        }
        catch(Exception e){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error in Create Newsflash Page!')); system.debug('Error in Create Newsflash Page'+e);}

    }
    public pageReference setupL3(){
        l3Options = new selectOption[]{new selectOption('','--None--')};
        ppReq = false;
        return null;
    }
    public pageReference getL3s(){
        system.debug('>>>>'+GrayOut);
        GrayOut = false;
        setupL3();
        /*this.theNewsflash.Issue_Type__c = this.theCase.Issue_Type__c == null ? '' : this.theCase.Issue_Type__c;
        this.theNewsflash.Issue_Sub_Type__c = this.theCase.Issue_Sub_Type__c== null ? '' : this.theCase.Issue_Sub_Type__c;*/
        
        this.theCase.Issue_Type__c = this.theCase.Issue_Type__c == null ? '' : this.theCase.Issue_Type__c;
        this.theCase.Issue_Sub_Type__c = this.theCase.Issue_Sub_Type__c== null ? '' : this.theCase.Issue_Sub_Type__c;
        
        String dispositionQuery = 'SELECT L3__c FROM Disposition__c WHERE L1__c = \''+String.escapeSingleQuotes(this.theCase.Issue_Type__c);
        dispositionQuery += '\' AND L2__c = \''+String.escapeSingleQuotes(this.theCase.Issue_Sub_Type__c)+'\' AND ';
        if(this.theCase.Product_L1__c != null && (this.theCase.Product_L1__c.containsIgnoreCase('elite') || this.theCase.Product_L1__c.containsIgnoreCase('app')))
            dispositionQuery += 'ELITE__c';
        else
            dispositionQuery += 'GAME__c';
        dispositionQuery += ' = TRUE ORDER BY L3__c';
        for(Disposition__c l3 : database.query(dispositionQuery )) l3Options.add(new selectOption(l3.L3__c, l3.L3__c));
        
        l3Options.add(new selectOption('Needs L3','Needs L3'));
        ppReq = new set<String>{'Toy','Portal','Disc','Peripheral','Special Edition 3rd Party'}.contains(this.theCase.Issue_Sub_Type__c);
        system.debug(dispositionQuery+'>>>>'+GrayOut);
        return null;
    }
   
    public pagereference Save(){
    system.debug('>>>>>>>>>>');
        theNewsflash.Product_L1__c=theCase.Product_L1__c;
        theNewsflash.Product_L2__c=theCase.Product_L2__c;
        theNewsflash.Sub_Product_DLC__c=theCase.Sub_Product_DLC__c;
        theNewsflash.Issue_Type__c=theCase.Issue_Type__c;
        theNewsflash.Issue_Sub_Type__c=theCase.Issue_Sub_Type__c;
        theNewsflash.L3__c=theCase.L3__c;
        theNewsflash.Platform__c=theCase.Platform__c;
        
        //system.debug('--'+theNewsflash.Platform__c+'--');
        /*if(theNewsflash.Product_L1__c==null||(theNewsflash.Product_L1__c=='Others'&&theNewsflash.Product_L2__c==null)||theNewsflash.Sub_Product_DLC__c==null||theNewsflash.Issue_Type__c==null
        ||theNewsflash.Issue_Sub_Type__c==null||theNewsflash.L3__c==null||theNewsflash.Platform__c==null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please Enter All Dispositioning Fields'));
            return null;
        }*/
        
        if(theNewsflash.Approval_Status__c==null)  theNewsflash.Approval_Status__c='Open';
        try{
            upsert theNewsflash;
        }
        catch(Exception e){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error Saving the Newsflash !')); system.debug('Error Saving the Newsflash'+e);}
        return (new pagereference('/'+theNewsflash.id));
    }    
}