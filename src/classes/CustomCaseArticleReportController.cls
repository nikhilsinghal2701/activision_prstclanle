public class CustomCaseArticleReportController 
{
    public set<string> CaseOwnerset ;
    public DateTime Startdate,EndDate;         
        
    public list<CasesWraperClass> CaseWraperlist{get;set;}      
    
    public string selectedcompany{get;set;}
    public string Strstartdate{get;set;}
    public string StrEndDate{get;set;}    
    public string casecountquery;           
        
    public CustomCaseArticleReportController()
    {
        Strstartdate = string.valueof(system.now().adddays(-7).format('MM/dd/yyyy')); 
        StrEndDate = string.valueof(system.now().format('MM/dd/yyyy'));
                
        Startdate = ConvertStringTODateTime(Strstartdate,'startdate'); 
        EndDate = ConvertStringTODateTime(StrEndDate,'Enddate');
        selectedcompany = 'Sutherland';         
    }
    
    public class CasesWraperClass
    {
        public string caseowner{get;set;}
        public integer TotalCases{get;set;}
        public integer CasesWithArticles{get;set;}
        public integer CasesWithoutArticles{get;set;}
        public integer CasesWOANeedsNewPageClicked{get;set;}       
        public string ArticleAssociationRate{get;set;}         
        public string UserCompany{get;set;}
        public string UserDivision{get;set;}       
    }    
    
    public list<CasesWraperClass> getCasesToDisplay()            
    {
            
        CaseWraperlist = new list<CasesWraperClass>();        
        string StrRecordType=[SELECT Id FROM RecordType WHERE Name = 'General Support' limit 1].id;
        string strquery,strquery1;
        casecountquery = 'select ownerid,count(id) from case where ownerid in : Setids and owner.type =\''+'user'+'\' and RecordTypeId = : StrRecordType ';                
        
        set<string> Setids = new set<string>();  
        set<string> ownerset = new set<string>(); 
        set<string> userset = new set<string>();           
        set<id> CaseIdsSet = new set<id>();        
        map<string,User> usermap = new map<string,user>();       
        
        map<string,integer> caseMapwitharticles = new map<string,integer>();
        map<string,integer> caseMapwithoutarticles = new map<string,integer>(); 
        map<string,integer> NeedsNewPageClickedMap = new map<string,integer>();
        map<string,CaseArticle> CaseArticlesMap = new  map<string,CaseArticle>();
        map<string,integer> CaseCountMap = new  map<string,integer>();               
        
        integer caselist = 0;
        integer caselistWA = 0;
        integer NeedsNewPageClickedList = 0;         
        
        List<UserLicense> ulics=[Select Id From UserLicense  where Name != 'Overage High Volume Customer Portal' ];
        usermap = new map<string ,user >([select id ,Name,CompanyName,Division,ProfileId,IsActive from user where ProfileId in (Select Id From Profile  where UserLicenseId  in :ulics) and IsActive = true ]) ;
        userset = usermap.keyset();
                
        if(StrRecordType!=null && StrRecordType!='')  
        {                 
             strquery = 'select id,ownerid,Needs_New_Page__c,RecordTypeId,Owner_Company__c,CreatedDate,owner.type  from case  where  owner.type = \''+ 'user'+'\' and RecordTypeId = \''+ StrRecordType+'\'  And ownerid in : userset  ';                             
        }    
        
        if(selectedcompany!=null && selectedcompany!='' && selectedcompany!='--Select--')
        {
            strquery+= ' And Owner_Company__c = \''+selectedcompany+'\'';
            casecountquery+= ' And Owner_Company__c = \''+selectedcompany+'\'';
        }         

        if(strquery!=null && strquery!='')
        {
            strquery+= ' And CreatedDate >=: Startdate And CreatedDate <= : EndDate order by ownerid' ;                                                             
                                                                         
        }
        if(casecountquery!=null && casecountquery!='')
        {
            casecountquery += '  And  CreatedDate >= :Startdate And CreatedDate <= : EndDate GROUP BY ownerid'  ;                                              
        }       
        
        system.debug('casecountquery ===='+strquery);
        system.debug('startdate===='+Startdate+'======EndDate====='+EndDate);        
        for(case CasesFetched : Database.query(strquery))
        {
            Setids.add(CasesFetched.ownerid); 
            CaseIdsSet.add(CasesFetched.id);          
        }              
        
        string querystr = 'select CaseId,KnowledgeArticleId from CaseArticle where caseid in (select id from case where ownerid in : Setids ) And Caseid in : CaseIdsSet' ;
                        
        for(CaseArticle CAvar: Database.query(querystr))
        {
            CaseArticlesMap .put(CAvar.Caseid,CAvar);          
        }                        
        for (AggregateResult ar :  Database.query(casecountquery))  {
          
            CaseCountMap.put(string.valueof(ar.get('ownerid')),integer.valueof(ar.get('expr0')));
        }        
        
        integer count = 0;
        for(Case casevar :Database.query(strquery))        
        {    
             count++; 
             integer casecountmapsize =   (CaseCountMap!=null && CaseCountMap.size()>0)?CaseCountMap.get(casevar.ownerid):0;                                   
             CaseArticle CAVar = CaseArticlesMap.get(casevar.id);
                          
             if(CAVar!=null && CAVar.KnowledgeArticleId!=null )
             caselistWA ++;
             else
             {                      
                 caselist++;
                 if(casevar.Needs_New_Page__c == true)
                 NeedsNewPageClickedList++;                     
             }             
             if(count == casecountmapsize)
             {
                  caseMapwitharticles.put(casevar.ownerid,caselistWA ); 
                  caseMapwithoutarticles.put(casevar.ownerid,caselist);  
                  NeedsNewPageClickedMap.put(casevar.ownerid,NeedsNewPageClickedList);                                    

                  caselist = 0;
                  caselistWA = 0;
                  NeedsNewPageClickedList = 0;
                  count = 0; 
             }                                                 
        }                
                    
         CaseOwnerset = new set<string>();
        //for(Case casevar :Database.query(strquery))
        for(string str:CaseCountMap.keyset())
        {        
         CasesWraperClass wraperclassvar = new CasesWraperClass();           
             user uservar = usermap.get(str); 
            // system.debug('caseMapwitharticles.get(str)========='+caseMapwitharticles.get(str)+'====='+uservar.Name+'======str===='+str);                      
             integer casesWithArticles = (caseMapwitharticles!=null && caseMapwitharticles.size()>0 && caseMapwitharticles.get(str)!=null) ? caseMapwitharticles.get(str) : 0 ;
             integer casesWithOutArticles =  (caseMapwithoutarticles!=null && caseMapwithoutarticles.size()>0 && caseMapwithoutarticles.get(str)!=null) ? caseMapwithoutarticles.get(str) : 0 ;
             integer needsnewpageclicked = (NeedsNewPageClickedMap!=null && NeedsNewPageClickedMap.size()>0 && NeedsNewPageClickedMap.get(str)!=null)?NeedsNewPageClickedMap.get(str):0;
                         
            if(uservar!= null && uservar.Name!=null && uservar.Name!='' && !CaseOwnerset.contains(uservar.Name))
            {                
                wraperclassvar.caseowner = uservar .Name;                
                wraperclassvar.TotalCases = casesWithArticles +casesWithOutArticles ;                
                wraperclassvar.CasesWithArticles = casesWithArticles ;                
                wraperclassvar.CasesWithoutArticles = casesWithOutArticles ;               
                wraperclassvar.CasesWOANeedsNewPageClicked = needsnewpageclicked ;
                                
                if(wraperclassvar.TotalCases>0)
                {                    
                    integer AssociationRateValue = integer.valueof(double.valueof(wraperclassvar.TotalCases - (wraperclassvar.CasesWithoutArticles - wraperclassvar.CasesWOANeedsNewPageClicked))/double.valueof(wraperclassvar.TotalCases)*100);
                    wraperclassvar.ArticleAssociationRate = string.valueof(AssociationRateValue)+'%';                    
                }
                wraperclassvar.userCompany = uservar.CompanyName;
                wraperclassvar.userDivision = uservar.Division;
                
                //system.debug('CaseOwnerset======='+wraperclassvar );
                CaseOwnerset.add(uservar.Name);
                
                CaseWraperlist.add(wraperclassvar );     
            }           
        }
        CaseOwnerset = new set<string>();
        //system.debug('size======'+CaseWraperlist.size());
        return CaseWraperlist;     
    }
    
    public PageReference GotoExportPage() {               
        pagereference p = new pagereference('/apex/CasesCustomExportPage');
        p.setredirect(false);
        return p;
    }
        
    public list<selectoption>  getcompanynamesset()
    {
        List<SelectOption> options = new List<SelectOption>();
        set<string> SetCompanyNames = new set<string>();
        List<UserLicense> ulics=[Select Id From UserLicense  where Name != 'Overage High Volume Customer Portal'];
        
        for(user u:[select id,CompanyName,ProfileId from user where ProfileId in (Select Id From Profile p where p.UserLicenseId in :ulics) Order by CompanyName])
        {            
            SetCompanyNames.add(u.CompanyName);
        }
        options.add(new SelectOption('--Select--','--Select--'));
        for(string s:SetCompanyNames)
        {
            if(s!=null && s!='')
            options.add(new SelectOption(s,s));
        }
        return options;
    } 
    
    public pagereference GenerateReport()      
    {     
          system.debug('Strstartdate====='+Strstartdate+'===StrEndDate=='+StrEndDate);     
          if(Strstartdate!=null && Strstartdate!='')         
             Startdate = ConvertStringTODateTime(Strstartdate,'startdate');                  
         if(StrEndDate!=null && StrEndDate!='')
             EndDate = ConvertStringTODateTime(StrEndDate,'Enddate');          
         system.debug('Startdate====='+Startdate+'===EndDate=='+EndDate);     
        return null;

    } 
    
    public datetime ConvertStringTODateTime(string StrValue,string StrDate)
    {
        Date tempDate;
        String[] tempStr;
        DateTime tempDateTime;        
        if(StrValue.contains('/'))        
        tempStr = StrValue.split('/');        
        //system.debug('tempStr===='+tempStr);
        Integer m = Integer.valueOf(tempStr[0]);
        Integer d = Integer.valueOf(tempStr[1]);
        Integer y = Integer.valueOf(tempStr[2]);
        tempDate = Date.newInstance(y,m,d);        
        
        if(strdate!=null && strdate!='' && strdate == 'startdate')        
        tempDateTime = Datetime.newInstance(y, m,d,0,0,0);
        else if(strdate!=null && strdate!='' && strdate == 'Enddate')
        tempDateTime = Datetime.newInstance(y, m,d,23,59,59);

        //tempDateTime = Datetime.newInstance(y, m,d);        
        //system.debug('tempDateTime ======'+tempDateTime);        
        return  tempDateTime;
    }    
}