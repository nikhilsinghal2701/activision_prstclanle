public class DISP_Utills {
//this section of the utility class returns a list of the external systems that are currently using the disposition
    public map<String, sObject[]> dispUsedLocs {get; set;}
    public map<String, List<String[]>> obj2fieldList {get; set;}
    public map<String, String[]> obj2fields {get; set;}
    public map<String, Integer> obj2NumResults {get; set;}
    public map<String, String> obj2ObjQuery {get; set;}
    public set<String> dispUsedLocsKeySet {get; set;}
    public Disposition__c theRec {get; set;}
    public sObject theSobj {get; set;}
    public boolean replacementValueNeeded {get; set;}
    public boolean activeOnLoad {get; set;}
    public SelectOption[] validReplacements {get; set;}
    public String chosenReplacement {get; set;}
    public DISP_Utills(ApexPages.StandardController cont) { 
        theRec = [SELECT Id, L1__c, L2__c, L3__c, IsActive__c FROM Disposition__c WHERE id = :(Id) cont.getId()];
        replacementValueNeeded = false;
        activeOnLoad = theRec.isActive__c;
    }
    public pageReference toggleReplacement(){
        if(activeOnLoad && hasL3FieldsToUpdate){//if it was active on load we need to replace it when it's being deactivated
            replacementValueNeeded = ! theRec.isActive__c;
            if(replacementValueNeeded){
                system.debug('theRec.L1__c == '+theRec.L1__c+' | theRec.L2__c == '+theRec.L2__c);
                validReplacements = new SelectOption[]{new selectOption('','--Choose--')};
                for(Disposition__c disp : [SELECT L3__c FROM Disposition__c WHERE 
                                           L1__c = :theRec.L1__c AND 
                                           L2__c = :theRec.L2__c AND 
                                           IsActive__c = TRUE AND 
                                           Id != :theRec.Id
                                           ORDER BY L3__c]){
                    validReplacements.add(new SelectOption(disp.L3__c, disp.L3__c));
                }
            }else{
                chosenReplacement = null;
            }
        }
        return null;
    }
    public pageReference custSave(){
        try{
            map<String, String> obj2fieldToUpdate = new map<String, String>();
            if(chosenReplacement != null){
                sObject[] toUpdate = new sObject[]{}; //holds all the records to update
                for(String key : dispUsedLocs.keyset()){ //get the name of the field to update
                    String fieldName = '';
                    for(String[] fieldPairsForThisObject : obj2fieldList.get(key)){
                        if(fieldPairsForThisObject[1] == 'L3__c'){
                            obj2fieldToUpdate.put(key, fieldPairsForThisObject[0]);
                            break;
                        }
                    }
                }
                for(String obj : obj2fieldToUpdate.keyset()){ //get the query that contains the field to update from the object that has it
                    for(sObject sObj : database.query(obj2ObjQuery.get(obj).replaceAll('LIMIT 200',''))){ //for each record in that query
                        sObj.put(obj2fieldToUpdate.get(obj), chosenReplacement);//change the value of the field to the new value
                        toUpdate.add(sObj);//add it to the list to update
                    }
                }
                update toUpdate; //update the list
            }
            update theRec; //update the record
        }catch(Exception e){
            return null;
        }
        return new ApexPages.standardController(theRec).view();
    }
    public Disp_Utills(){} //used by the componenet 
    public pageReference calcDispUsedIn(){
        if(theSobj != null) theRec = (Disposition__c) theSobj;
        if(theRec.Id == null) return null;
        
        theRec = [SELECT L1__c, L2__c, L3__c, isActive__c FROM Disposition__c WHERE Id = :theRec.Id];
        
        dispUsedLocs = new map<String, sObject[]>();
        obj2fields = new map<String, String[]>();
        obj2NumResults = new map<String, Integer>();
        
        
        //store the values that we're searching for against their disposition field location
        map<String, String> field2val = new map<String, String>{
            'L1__c'=>theRec.L1__c,'l1__c'=>theRec.L1__c,
            'L2__c'=>theRec.L2__c,'l2__c'=>theRec.L2__c,
            'L3__c'=>theRec.L3__c,'l3__c'=>theRec.L3__c
        };
        
        //prepare the list of "for this object, this disposition field is stored in this field on that object"
        obj2fieldList = new map<String, List<String[]>>();
        for(Disposition_Object_Map__c pMap : [SELECT Object__c, Field_Name__c, Linked_Disposition_Field__c 
                                                FROM Disposition_Object_Map__c WHERE Object__c != 'Case'])
        {
            //next line ensures users don't hit VF errors as we try to dynamically render data columns
            if(! obj2fieldsToQuery.containsKey(pMap.Object__c)) obj2fieldsToQuery.put(pMap.Object__c, new String[]{});
        
            List<String[]> fieldList4thisObj = new List<String[]>();
            if(obj2fieldList.containsKey(pMap.Object__c)) fieldList4thisObj = obj2fieldList.get(pMap.Object__c);
            fieldList4thisObj.add(new String[]{pMap.Field_Name__c,pMap.Linked_Disposition_Field__c});
            obj2fieldList.put(pMap.Object__c, fieldList4thisObj);
            
            String[] fields = new String[]{};
            if(obj2fields.containsKey(pMap.Object__c)) fields = obj2fields.get(pMap.Object__c);
            fields.add(pMap.Field_Name__c);
            obj2fields.put(pMap.Object__c, fields);
            
            dispUsedLocs.put(pMap.Object__c, new sObject[]{});
            obj2NumResults.put(pMap.Object__c, 0);
        }
        
        //now build the queries to determine if the current disposition is being used anywhere
        obj2ObjQuery = new map<String, String>();
        //this next map allows us to answer the question "are there any records with a matching L3 value that we need to update if this is deactivated?"
        hasL3FieldMap = new map<String, Integer>(); 
        matchedFieldList = new map<String, String>();
        for(String objName : obj2fieldList.keyset()){
            String query = 'SELECT ';
            set<String> fieldsToQuery = new set<String>();
            for(String[] fields : obj2fieldList.get(objName)){
                fieldsToQuery.add(fields[0]);
            }
            if(obj2fieldsToQuery.containsKey(objName)){
                fieldsToQuery.addAll(obj2fieldsToQuery.get(objName));
            }
            String[] iterableFieldsToQuery = new String[]{};
            iterableFieldsToQuery.addAll(fieldsToQuery);
            query += String.join(iterableFieldsToQuery,',') + ' FROM '+objName+' WHERE ';
            List<String[]> pairList = obj2fieldList.get(objName);
            String mFields = '';
            for(Integer i = 0; i<pairList.size(); i++){
                mFields += pairList[i][0]+' & ';
                query += pairList[i][0] +' = \''+field2val.get(pairList[i][1])+'\' AND ';
                if(pairList[i][1] == 'L3__c'){
                    hasL3FieldMap.put(objName, 0);
                }
            }
            matchedFieldList.put(objName, mFields.subString(0, (mFields.length() - 3)));
            if(objName.contains('__ka')) query += 'PublishStatus = \'Online\' AND Language = \'en_US\' AND ';
            query = query.subString(0, (query.length() - 5)) + ' ORDER BY CreatedDate DESC LIMIT 200';
            system.debug('query == '+query);
            obj2ObjQuery.put(objName, (query));
        }
        
        for(String query : obj2ObjQuery.values()){
            sObject[] usedLocations = database.query(query);
            if(usedLocations != null && usedLocations.size() > 0){
                String locName = usedLocations[0].getsObjectType().getDescribe().getName();
                dispUsedLocs.put(locName, usedLocations);
                obj2NumResults.put(locName, usedLocations.size());
                if(hasL3FieldMap.containsKey(locName)) hasL3FieldsToUpdate = true;
            }
        }

        dispUsedLocsKeySet = dispUsedLocs.keySet();

        return null;
    }
    public map<String, Integer> hasL3FieldMap {get; set;}
    public boolean hasL3FieldsToUpdate = false;
    public map<String, String[]> obj2fieldsToQuery = new map<String, String[]>{
        'WebToCase_SubType__c'=> new String[]{'Name','ref_Language__c','ref_Platform__c','ref_GameTitle__c','ref_Type__c'},
        'WebToCase_Type__c'=> new String[]{'Name','ref_Language__c','ref_Platform__c','ref_GameTitle__c'},
        'CS_Form__c'=> new String[]{'Name','Name__c','Is_Active__c'},
        'FAQ__kav'=> new String[]{'Title','ArticleNumber','Language'}
    };
    public map<String, String[]> getObj2fieldsToQuery(){ return obj2fieldsToQuery; }
    public map<String, String> matchedFieldList {get; set;}

}