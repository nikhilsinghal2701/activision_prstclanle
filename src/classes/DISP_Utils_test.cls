@isTest
public class DISP_Utils_test{
    public static testMethod void createTestData(){
        
        insert new List<Disposition_Object_Map__c>{
            new Disposition_Object_Map__c(Object__c = 'FAQ__kav',
                Field_Name__c = 'WebToCase_Type_L1__c',Linked_Disposition_Field__c = 'L1__c'
            ),
            new Disposition_Object_Map__c(Object__c = 'FAQ__kav',
                Field_Name__c = 'WebToCase_SubType_L2__c',Linked_Disposition_Field__c = 'L2__c'
            ),
            new Disposition_Object_Map__c(Object__c = 'WebToCase_SubType__c',
                Field_Name__c = 'SubTypeMap__c',Linked_Disposition_Field__c = 'L2__c'
            ),
            new Disposition_Object_Map__c(Object__c = 'WebToCase_Type__c',
                Field_Name__c = 'TypeMap__c',Linked_Disposition_Field__c = 'L1__c'
            ),
            new Disposition_Object_Map__c(Object__c = 'CS_Form__c',
                Field_Name__c = 'Issue_Sub_Type__c',Linked_Disposition_Field__c = 'L2__c'
            ),
            new Disposition_Object_Map__c(Object__c = 'CS_Form__c',
                Field_Name__c = 'Issue_Type__c',Linked_Disposition_Field__c = 'L1__c'
            ),
            new Disposition_Object_Map__c(Object__c = 'CS_Form__c',
                Field_Name__c = 'L3__c',Linked_Disposition_Field__c = 'L3__c'
            )
        };
    }
    public static testMethod void test1(){
        //test empty constructor
        DISP_Utills tstCls = new DISP_Utills();
        tstCls.getObj2fieldsToQuery();
        //test constructor w/standard controller
        Disposition__c disp = new Disposition__c(L1__c = 'l1', L2__c = 'l2', L3__c = 'l3',
            isActive__c = false);
        Disposition__c disp2 = new Disposition__c(L1__c = 'l1', L2__c = 'l2', L3__c = 'l3a',
            isActive__c = true);
            
        insert new Disposition__c[]{disp, disp2};
        tstCls = new DISP_Utills(new apexPages.standardController(disp));
        system.assertEquals(false, tstCls.activeOnLoad);
        disp.IsActive__c = true;
        update disp;
        tstCls = new DISP_Utills(new apexPages.standardController(disp));
        system.assert(tstCls.activeOnLoad);
        system.assertEquals(false, tstCls.replacementValueNeeded);       
        
        //method 1 = toggleReplacement
        set<String> replacementVals = new set<String>{'','l3a'};
        tstCls.hasL3FieldsToUpdate = true;
        tstCls.chosenReplacement = 'i should be nulled out';
        tstCls.toggleReplacement();
        system.assertEquals(null, tstCls.chosenReplacement);
        tstCls.theRec.isActive__c = false;
        tstCls.toggleReplacement();
        for(SelectOption so : tstCls.validReplacements){
            system.assert(replacementVals.contains(so.getValue()));
        }
        
        createTestData();

        //method 3 = calcDispUsedIn()
        tstCls.calcDispUsedIn();
        
        //method 2 = custSave()
        Parent_CS_Form__c pForm = new Parent_CS_Form__c(Name__c = 'test');
        insert pForm;
        insert new CS_Form__c(
            Parent_CS_Form__c = pForm.Id,
            Name__c = '123',
            Form_Language__c = 'en_US',
            Submission_Message__c = 'test',
            Issue_Type__c = 'l1',
            Issue_Sub_Type__c = 'l2',
            L3__c = 'l3'
        );
        tstCls.chosenReplacement = 'l3a';
        tstCls.custSave();
        tstCls.calcDispUsedIn();
    }
}