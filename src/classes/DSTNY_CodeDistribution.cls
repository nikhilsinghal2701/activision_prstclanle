global class DSTNY_CodeDistribution implements Database.Batchable<sObject>, Schedulable {
    //SCHEDULEABLE
    global void execute(SchedulableContext SC){
        database.executeBatch(new DSTNY_CodeDistribution());
    }

    global static void setSchedule(){
        system.schedule('DSTNY_CodeDistribution', '0 4 * * * ?', new DSTNY_CodeDistribution());
    }

    //BATCH
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Region__c, Platform__c, First_Party_Code__c, Email__c';
        query += ' , Code_Type__c, Unique_Key__c';
        query += ' FROM Destiny_Support_Issue__c';
        query += ' WHERE Resolution__c = \'Issue Code\' AND';
        query += ' First_Party_Code__c = null AND';
        query += ' X2nd_First_Party_Code__c = null AND';
        query += ' X3rd_First_Party_Code__c = null AND';
        query += ' Duplicate__c = false AND';
        query += ' RecordType.DeveloperName = \'Launch_Code\' AND';
        query += ' Region__c != \'My country is not listed\'';
        query += ' ORDER BY CreatedDate ASC';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        //filter out duplicates
        map<String, Destiny_Support_Issue__c> email2Issue = new map<String, Destiny_Support_Issue__c>();
        Destiny_Support_Issue__c[] dupes = new Destiny_Support_Issue__c[]{};

        //duplicate check 1, in case we have duplciates in scope
        //also preps our data for out of scope duplicate check 2
        for(Destiny_Support_Issue__c issue : (Destiny_Support_Issue__c[]) scope){
            if(email2Issue.containsKey(issue.Unique_Key__c)){ 
                issue.Duplicate__c = true;
                issue.Resolution__c = 'Duplicate';
                issue.Status__c = 'Closed';
                dupes.add(issue);
                continue;
            }
            email2Issue.put(issue.Unique_Key__c, issue);
        }

        //duplicate check 2, more likely event that the duplicate exists "out of scope" 
        for(Destiny_Support_Issue__c dupeIssue : [SELECT Unique_Key__c, Email__c FROM Destiny_Support_Issue__c 
                                                  WHERE Email__c IN :email2Issue.keySet() 
                                                    AND RecordType.DeveloperName = 'Launch_Code'
                                                    AND First_Party_Code__c != null])
        {
            Destiny_Support_Issue__c dupe = email2Issue.get(dupeIssue.Unique_Key__c);
            dupe.Duplicate__c = true;
            dupe.Resolution__c = 'Duplicate';
            dupe.Status__c = 'Closed';
            dupes.add(dupe);
            email2Issue.remove(dupeIssue.Unique_Key__c);
        }
        update dupes;

        //do distribution
        Destiny_Support_Issue__c[] toUpdate = new Destiny_Support_Issue__c[]{};
        map<String, Destiny_Support_Issue__c[]> issueKey2issues = new map<String, Destiny_Support_Issue__c[]>();
        for(Destiny_Support_Issue__c rec : email2Issue.values()){
            String issueKey = rec.Platform__c.toLowerCase() + rec.Code_Type__c.toLowerCase();

            Destiny_Support_Issue__c[] innerLoopList = new Destiny_Support_Issue__c[]{};
            if(issueKey2issues.containsKey(issueKey)) innerLoopList = issueKey2Issues.get(issueKey);
            innerLoopList.add(rec);
            issueKey2issues.put(issueKey, innerLoopList);
        
        }
        for(String key : issueKey2issues.keySet()){
            Destiny_Support_Issue__c[] results = doAssignment(issueKey2issues.get(key),key);
            for(Destiny_Support_Issue__c result : results){
                system.debug(LoggingLevel.ERROR, result);
            }
            toUpdate.addAll(results);
        }
        update toUpdate;
    }

    public Destiny_Support_Issue__c[] doAssignment(Destiny_Support_Issue__c[] recs, String recType){
        Destiny_Support_Issue__c[] results = new Destiny_Support_Issue__c[]{};
        if(recs.size() > 0){
            Integer loopCount = 0;
            String codeTypeName = 'DestinyLaunchCS-'+recType;
            system.debug(LoggingLevel.ERROR, 'codeTypeName == '+codeTypeName);
            
            set<String> contactEmails = new set<String>();
            for(Destiny_Support_Issue__c iss : recs){
                contactEmails.add(iss.Email__c.toLowerCase());
            }
            
            map<String, Contact> email2Contact = new map<String, Contact>();
            for(Contact c : [SELECT Id, Email FROM Contact WHERE Email IN :contactEmails]){
                email2Contact.put(c.Email.toLowerCase(), c);
            }

            Integer foundCodeCount = 0;
            CRR_Code__c[] availCodes, codes;
            String codeQuery;
            for(CRR_Code_Type__c codeType : [SELECT Id FROM CRR_Code_Type__c WHERE Name = :codeTypeName LIMIT 1]){
                codeQuery = 'SELECT Code__c FROM CRR_Code__c WHERE Code_Type__c = \'' + codeType.Id + '\'';
                codeQuery += ' AND Redeemed_by__c = null LIMIT '+recs.size();
                
                codes = new CRR_Code__c[]{};
                availCodes = (CRR_Code__c[]) database.query(codeQuery);
                foundCodeCount = availCodes.size();
                
                while(loopCount < recs.size() && loopCount < foundCodeCount){
                    system.debug('fgw emailOfContact == '+recs[loopCount].Email__c.toLowerCase());
                    if(email2Contact.containsKey(recs[loopCount].Email__c.toLowerCase())){
                        recs[loopCount].First_Party_Code__c = availCodes[loopCount].Code__c;
                        availCodes[loopCount].Redeemed_by__c = email2Contact.get(recs[loopCount].Email__c.toLowerCase()).Id;
                        codes.add(availCodes[loopCount]);
                    }
                    loopCount++;
                }
                
                update codes;
            }

            if(recs.size() > foundCodeCount){
                Integer neededCodeCount = recs.size() - foundCodeCount;
                codeQuery = 'SELECT Code__c FROM CRR_Code__c WHERE CS_Code_Name__c = \'' + String.escapeSingleQuotes(codeTypeName) + '\'';
                codeQuery += ' AND Redeemed_by__c = null LIMIT '+neededCodeCount;
                codes = new CRR_Code__c[]{};
                Integer newCodeLocation = 0;
                for(CRR_Code__c code : database.query(codeQuery)){
                    if(recs.size() <= loopCount) break;
                    else{
                        recs[loopCount].First_Party_Code__c = code.Code__c;
                        code.Redeemed_by__c = email2Contact.get(recs[loopCount].Email__c.toLowerCase()).Id;
                        codes.add(code);
                        loopCount++;
                        newCodeLocation++;
                    }
                }
                update codes;
            }

            for(Destiny_Support_Issue__c rec : recs){
                if(rec.First_Party_Code__c != null){
                }else{
                    rec.Resolution__c = 'No Codes Available';
                }
                rec.status__c = 'Closed';
                results.add(rec);
            }

        }
        return results;
    }
    
    global void finish(Database.BatchableContext BC) {}
    
}