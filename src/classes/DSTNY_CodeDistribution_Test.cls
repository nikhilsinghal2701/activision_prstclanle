@isTest
public class DSTNY_CodeDistribution_Test {
    
    @isTest 
    public static void createTestData() {
        Destiny_Support_Issue__c[] testData = new Destiny_Support_Issue__c[]{};
        Contact[] testContacts = new Contact[]{};
        RecordType rt = [SELECT Id FROM RecordType WHERE sObjectType = 'Destiny_Support_Issue__c' AND DeveloperName = 'Launch_Code'];    
        for(integer ix = 0; ix < 449; ix++){
            String i = '';
            if(ix < 10){
                i = '00'+string.valueOf(ix);
            }else if(ix < 100){
                i = '0'+string.valueOf(ix);
            }else{
                i = string.valueOf(ix);
            }
            Destiny_Support_Issue__c tstData = new Destiny_Support_Issue__c(
                Email__c = 'testData'+i+'@griffinwarburton.com',
                Bungie_Username__c = 'testData'+i,
                Bungie_User_Id__c = 'testData'+i,
                Error_message__c = 'testData'+i,
                Code__c = '333-333-333',
                Alternate_Email__c = 'testData'+i+'@griffinwarburton.com',
                Platform__c = '',
                Preorder_date__c = system.today(),
                Region__c = 'United States',
                Retailer__c = 'sdf',
                RecordTypeId = rt.Id
            );
            testContacts.add(new Contact(
                Email = tstData.Email__c,
                LastName = tstData.Bungie_Username__c
            ));
            //0 - 199 successful distribution of each type of code: scea, scee, scej & xbox
            //200 - 400
            //400 - 450 test no distribution to unknown countries
            if(ix < 49){
                tstData.Platform__c = 'Playstation 4 (North & South America)';
                if(ix < 16){tstData.Code_Type__c = 'Vanguard';}else if(ix < 32){tstData.Code_Type__c = 'Collector\'s Edition Code';}else{tstData.Code_Type__c = 'Expansion Pass';}
            }else if(ix < 99){ 
                tstData.Platform__c = 'Playstation 4 (Europe)';
                if(ix < 66){tstData.Code_Type__c = 'Vanguard';}else if(ix < 82){tstData.Code_Type__c = 'Collector\'s Edition Code';}else{tstData.Code_Type__c = 'Expansion Pass';}
            //}else if(ix < 149){ 
              //  if(ix < 116){tstData.Code_Type__c = 'Vanguard';}else if(ix < 132){tstData.Code_Type__c = 'Collector\'s Edition Code';}else{tstData.Code_Type__c = 'Expansion Pass';}
                //tstData.Platform__c = 'Playstation 4 (Asia)';
            }else if(ix < 199 && ix > 149){ 
                tstData.Platform__c = 'Xbox 360';
                if(ix < 166){tstData.Code_Type__c = 'Vanguard';}else if(ix < 182){tstData.Code_Type__c = 'Collector\'s Edition Code';}else{tstData.Code_Type__c = 'Expansion Pass';}
            }else if(ix < 249){ 
                tstData.Platform__c = 'Playstation 3 (Europe)';
                if(ix < 216){tstData.Code_Type__c = 'Vanguard';}else if(ix < 232){tstData.Code_Type__c = 'Collector\'s Edition Code';}else{tstData.Code_Type__c = 'Expansion Pass';}
            //}else if(ix < 299){
              //  tstData.Platform__c = 'Playstation 3 (Asia)';
                //if(ix < 266){tstData.Code_Type__c = 'Vanguard';}else if(ix < 282){tstData.Code_Type__c = 'Collector\'s Edition Code';}else{tstData.Code_Type__c = 'Expansion Pass';}
            }else if(ix < 349 && ix > 299){ 
                tstData.Platform__c = 'Playstation 3 (North & South America)';
                if(ix < 316){tstData.Code_Type__c = 'Vanguard';}else if(ix < 332){tstData.Code_Type__c = 'Collector\'s Edition Code';}else{tstData.Code_Type__c = 'Expansion Pass';}
            }else if(ix < 399){ 
                tstData.Platform__c = 'Xbox One';
                if(ix < 366){tstData.Code_Type__c = 'Vanguard';}else if(ix < 382){tstData.Code_Type__c = 'Collector\'s Edition Code';}else{tstData.Code_Type__c = 'Expansion Pass';}
            }else if(ix < 449){ 
                tstData.Platform__c = 'Playstation 3 (Asia)';
                tstData.Region__c = 'My country is not listed';
                if(ix < 416){tstData.Code_Type__c = 'Vanguard';}else if(ix < 432){tstData.Code_Type__c = 'Collector\'s Edition Code';}else{tstData.Code_Type__c = 'Expansion Pass';}
            }
            if(ix == 349) tstData.Email__c = 'testData348@griffinwarburton.com'; //do not give code to duplicate request
            testData.add(tstData);
        }
        insert testData;
        insert testContacts;
        CRR_Code_Type__c scee_ps4_v = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-playstation 4 (europe)vanguard');
        CRR_Code_Type__c scee_ps4_e = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-playstation 4 (europe)expansion pass');
        CRR_Code_Type__c scee_ps4_c = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-playstation 4 (europe)collector\'s edition code');
        CRR_Code_Type__c scea_ps4_v = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-playstation 4 (north & south america)vanguard');
        CRR_Code_Type__c scea_ps4_e = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-playstation 4 (north & south america)expansion pass');
        CRR_Code_Type__c scea_ps4_c = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-playstation 4 (north & south america)collector\'s edition code');
        
        CRR_Code_Type__c scee_ps3_v = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-playstation 3 (europe)vanguard');
        CRR_Code_Type__c scee_ps3_e = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-playstation 3 (europe)expansion pass');
        CRR_Code_Type__c scee_ps3_c = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-playstation 3 (europe)collector\'s edition code');
        CRR_Code_Type__c scea_ps3_v = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-playstation 3 (north & south america)vanguard');
        CRR_Code_Type__c scea_ps3_e = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-playstation 3 (north & south america)expansion pass');
        CRR_Code_Type__c scea_ps3_c = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-playstation 3 (north & south america)collector\'s edition code');
        
        CRR_Code_Type__c xbox_360_v = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-xbox 360vanguard');
        CRR_Code_Type__c xbox_360_c = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-xbox 360collector\'s edition code');
        CRR_Code_Type__c xbox_360_e = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-xbox 360expansion pass');
        CRR_Code_Type__c xbox_one_v = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-xbox onevanguard');
        CRR_Code_Type__c xbox_one_c = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-xbox onecollector\'s edition code');
        CRR_Code_Type__c xbox_one_e = new CRR_Code_Type__c(Name = 'DestinyLaunchCS-xbox oneexpansion pass');
        insert new CRR_Code_Type__c[]{
            scee_ps4_v,scee_ps4_e,scee_ps4_c,scea_ps4_v,scea_ps4_e,scea_ps4_c,
            scee_ps3_v,scee_ps3_e,scee_ps3_c,scea_ps3_v,scea_ps3_e,scea_ps3_c,
            xbox_360_v,xbox_360_c,xbox_360_e,
            xbox_one_v,xbox_one_c,xbox_one_e
        };//18
        CRR_Code__c[] codes = new CRR_Code__c[]{}; 
        for(integer i = 0; i < 1440; i++){
            CRR_Code__c code = new CRR_Code__c();
            code.Code__c = string.valueOf(i);
            code.First_Party__c = 'Microsoft';
            if(i < 960) code.First_Party__c = 'Sony';
            
            if(i < 60){code.Code_Type__c = scee_ps4_v.Id;}else if(i < 120){code.Code_Type__c = scee_ps4_e.Id;}else if(i < 180){code.Code_Type__c = scee_ps4_c.Id;
            }else if(i < 240){code.Code_Type__c = scea_ps4_v.Id;}else if(i < 300){code.Code_Type__c = scea_ps4_e.Id;}
            else if(i < 360){code.Code_Type__c = scea_ps4_c.Id;
            }else if(i < 420){code.Code_Type__c = scee_ps3_v.Id;}else if(i < 480){code.Code_Type__c = scee_ps3_e.Id;}
            else if(i < 540){code.Code_Type__c = scee_ps3_c.Id;
            }else if(i < 600){code.Code_Type__c = scea_ps4_v.Id;}else if(i < 660){code.Code_Type__c = scea_ps4_e.Id;}
            else if(i < 720){code.Code_Type__c = scea_ps4_c.Id;
            }else if(i < 780){code.Code_Type__c = scea_ps3_v.Id;}else if(i < 840){code.Code_Type__c = scea_ps3_e.Id;}
            else if(i < 900){code.Code_Type__c = scea_ps3_c.Id;

            }else if(i < 960){code.Code_Type__c = xbox_360_v.Id;}else if(i < 1020){code.Code_Type__c = xbox_360_c.Id;}
            else if(i < 1080){code.Code_Type__c = xbox_360_e.Id;
            }else if(i < 960){code.Code_Type__c = xbox_one_v.Id;}else if(i < 1020){code.Code_Type__c = xbox_one_c.Id;}
            else if(i < 1080){code.Code_Type__c = xbox_one_e.Id;
            }
            codes.add(code);
        }
        insert codes;
    }
    
    @isTest static void testFullDistribution() {
        //prep data 
        createTestData();
        Destiny_Support_Issue__c[] issues = [SELECT Id FROM Destiny_Support_Issue__c 
                                             ORDER BY CreatedDate ASC LIMIT 200];
        for(Destiny_Support_Issue__c issue : issues){
            issue.Resolution__c = 'Issue Code';
        }
        update issues;

        //run code
        Test.startTest();
        database.executeBatch(new DSTNY_CodeDistribution());
        Test.stopTest();
        
        //validate results
        for(Destiny_Support_Issue__c issue : [SELECT First_Party_Code__c FROM Destiny_Support_Issue__c 
                                              ORDER BY CreatedDate ASC LIMIT 200])
        {
            system.assert(issue.First_Party_Code__c != '' && issue.First_Party_Code__c != null);
        }
        system.assertEquals(200, [SELECT count() FROM CRR_Code__c WHERE Redeemed_By__c != null]);
    }

    @isTest static void testDuplicateAndExcludedCountries() {
        //prep data
        createTestData();
        Destiny_Support_Issue__c[] issues = [SELECT Id FROM Destiny_Support_Issue__c 
                                             ORDER BY Bungie_User_Id__c DESC LIMIT 200];
        for(Destiny_Support_Issue__c issue : issues){
            issue.Resolution__c = 'Issue Code';
        }
        update issues;

        //execute code
        Test.startTest();
        database.executeBatch(new DSTNY_CodeDistribution());
        Test.stopTest();

        //validate results
        //The 2 records with the same email address only gets 1 code, 49
        system.assertEquals(49, [SELECT count() FROM CRR_Code__c WHERE Redeemed_By__c != null]);
        Destiny_Support_Issue__c dupe1 = [SELECT First_Party_Code__c FROM Destiny_Support_Issue__c 
                                          WHERE Email__c = 'testData348@griffinwarburton.com' AND First_Party_Code__c != null];
        Destiny_Support_Issue__c dupe2 = [SELECT First_Party_Code__c FROM Destiny_Support_Issue__c 
                                          WHERE Email__c = 'testData348@griffinwarburton.com' AND First_Party_Code__c = null AND Id != :dupe1.id];
        //the queries act as the assertion, if either of them fail the code didn't work
    }

    @isTest static void testNoCodes() {
        //prep data
        createTestData();
        Destiny_Support_Issue__c[] issues = [SELECT Id FROM Destiny_Support_Issue__c ORDER BY Bungie_User_Id__c LIMIT 200];
        for(Destiny_Support_Issue__c issue : issues){
            issue.Resolution__c = 'Issue Code';
        }
        update issues;
        delete [SELECT Id FROM CRR_Code__c]; //delete all the codes

        //execute code
        Test.startTest();
        database.executeBatch(new DSTNY_CodeDistribution());
        Test.stopTest();

        //validate results
        system.assertEquals(0, [SELECT count() FROM CRR_Code__c WHERE Redeemed_By__c != null]);
    }
    
    @isTest static void testOutOfScopeDuplicate() {
        //prep data
        createTestData();
        Destiny_Support_Issue__c[] issues = [SELECT Id FROM Destiny_Support_Issue__c WHERE Email__c = 'testData348@griffinwarburton.com'];
        issues[0].First_Party_Code__c = 'asdf';
        issues[1].Resolution__c = 'Issue Code';
        update issues;

        //execute code
        Test.startTest();
        database.executeBatch(new DSTNY_CodeDistribution());
        Test.stopTest();

        //validate results
        //the queries act as the assertion, if either of them fail the code didn't work
        Destiny_Support_Issue__c dupe1 = [SELECT First_Party_Code__c FROM Destiny_Support_Issue__c 
                                          WHERE Email__c = 'testData348@griffinwarburton.com' AND First_Party_Code__c != null];
        Destiny_Support_Issue__c dupe2 = [SELECT First_Party_Code__c FROM Destiny_Support_Issue__c 
                                          WHERE Email__c = 'testData348@griffinwarburton.com' AND First_Party_Code__c = null AND Id != :dupe1.id];
        
    }

    @isTest static void testSetSchedule(){
        try{
            //remove the trigger if already exists
            for(CronTrigger ct : [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name = 'DSTNY_CodeDistribution'])
                system.abortJob(ct.Id);
            
            //set the new version
            DSTNY_CodeDistribution.setSchedule();
            
            //assert
            boolean jobExists = false;
            for(CronTrigger ct : [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name = 'DSTNY_CodeDistribution']){
                jobExists = true;
            }
            system.assert(jobExists);
        }catch(Exception e){
        }
    }

    @isTest static void testSchedule(){
        Test.StartTest();
        DSTNY_CodeDistribution sh1 = new DSTNY_CodeDistribution();
        String sch = '0 0 23 * * ?'; system.schedule('Test DSTNY Code Dist', sch, sh1); 
        Test.stopTest();
    }

    @isTest static void testDistributeFromCodeInsteadOfCodeType_parta() {
        //prep data
        createTestData();
        Destiny_Support_Issue__c[] issues = new Destiny_Support_Issue__c[]{};
        Destiny_Support_Issue__c[] toDel = new Destiny_Support_Issue__c[]{};
        for(Destiny_Support_Issue__c issue : [SELECT Id FROM Destiny_Support_Issue__c]){
            issue.Code_Type__c = 'Vanguard';
            issue.Platform__c = 'Xbox 360';
            issue.Resolution__c = 'Issue Code';
            if(issues.size() == 200){
                toDel.add(issue);
            }else{
                issues.add(issue);
            }
        }
        delete toDel;
        update issues; //when the test runs all the Xbox 360 vanguard codes will be used up
        
        //execute code
        Test.startTest();
        database.executeBatch(new DSTNY_CodeDistribution(), 200);
        Test.stopTest();
        
        //validate results
        //batch testing limited to 1 batch, 1 batch is limited to 200 codes
        //only 60 xbox 360vanguard, codes
        system.assert([SELECT count() FROM Destiny_Support_Issue__c] == 200);
        system.assert([SELECT count() FROM Destiny_Support_Issue__c WHERE First_Party_Code__c != null] == 60);
    } 

    @isTest static void testDistributeFromCodeInsteadOfCodeType_partb() {
        //prep data
        createTestData();
        Destiny_Support_Issue__c[] issues = new Destiny_Support_Issue__c[]{};
        Destiny_Support_Issue__c[] toDel = new Destiny_Support_Issue__c[]{};
        for(Destiny_Support_Issue__c issue : [SELECT Id FROM Destiny_Support_Issue__c]){
            issue.Code_Type__c = 'Vanguard';
            issue.Platform__c = 'Xbox 360';
            issue.Resolution__c = 'Issue Code';
            if(issues.size() == 200){
                toDel.add(issue);
            }else{
                issues.add(issue);
            }
        }
        delete toDel;
        update issues; //when the test runs all the Xbox 360 vanguard codes will be used up
        CRR_Code_Type__c codeType = [SELECT Id FROM CRR_Code_Type__c WHERE Name = 'DestinyLaunchCS-xbox 360vanguard'];
        CRR_Code__c[] codes = [SELECT Id FROM CRR_Code__c WHERE Code_Type__c != :codeType.Id];
        system.debug(LoggingLevel.ERROR, 'codes.size() == '+codes.size());
        for(CRR_Code__c code : codes){
            code.CS_Code_Name__c = 'DestinyLaunchCS-xbox 360vanguard';
        }
        update codes; //now the new apexcode will fidn these codes and assign them to the leftover

        //execute code
        Test.startTest();
        database.executeBatch(new DSTNY_CodeDistribution(), 200);
        Test.stopTest();
        
        //validate results
        //batch testing limited to 1 batch, 1 batch is limited to 200 codes
        Integer withCodes = [SELECT count() FROM Destiny_Support_Issue__c WHERE First_Party_Code__c != null];
        system.debug(LoggingLevel.ERROR, withCodes);
        system.assert(200 == withCodes);
        
    } 

}