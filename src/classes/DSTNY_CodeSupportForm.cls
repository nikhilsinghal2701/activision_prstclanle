public with sharing class DSTNY_CodeSupportForm {
    public ApexPages.StandardController theController; 
    public boolean isValid {get; set;}
    public transient blob attachment {get; set;}
    public string attachmentName {get; set;}

    public string getLanguage(){
        reCAPTCHA_controller cls = new reCAPTCHA_controller();
        return cls.getLanguage();
    }
    public DSTNY_CodeSupportForm(ApexPages.StandardController cont){
        try{
            theController = cont;
            this.isValid = false;
            if(cont.getRecord().get('id') != null){
                //validate that they are populating the fields for the first time
                if(isBlankOrNull([SELECT Bungie_User_Id__c FROM Destiny_Support_Issue__c WHERE Id = :theController.getId()].Bungie_User_Id__c))
                {
                    this.isValid = true;
                }
                for(RecordType rt : [SELECT Id FROM RecordType WHERE sObjectType = 'Destiny_Support_Issue__c' AND IsActive = true AND DeveloperName = 'Launch_Code']){
                    cont.getRecord().put('RecordTypeId',rt.Id);
                }
            }
        }catch(Exception e){
            system.debug(e);
        }
    }
    public pageReference customSave(){
        try{
            Destiny_Support_Issue__c theRec = (Destiny_Support_Issue__c) theController.getRecord();
            if(theRec.Code_Type__c != 'Vanguard' && attachment == null){
                return null;
            }else{
                createContact(theRec.Email__c, theRec.Bungie_Username__c);
                update theRec;
                if(attachment != null){
                    insert new Attachment(
                        body = attachment,
                        name = attachmentName,
                        parentId = theRec.Id
                    );
                }
                pageReference pr = Page.DestinyCodeSupportForm;
                pr.getParameters().put('id',theRec.Id);
                pr.setRedirect(true);
                return pr;
            }
        }catch(Exception e){
            system.debug(e);
        }
        return null;
    }
    //there must be a contact record to enable distribution of a code to this customer
    //the futureness is to avoid the customer from being delayed by the potentially long running query 
    @future 
    public static void createContact(String emailAddress, String bungieUserName){
        Integer existingContact = [SELECT count() FROM Contact WHERE Email = :emailAddress LIMIT 1];
        if(existingContact == 0){
            insert new Contact(
                Email = emailAddress,
                LastName = bungieUserName
            );
        }
    }
    public boolean isBlankOrNull(String s){ return s == null || s == ''; }
}