public class DSTNY_PreOrderSupportForm{

    public ApexPages.StandardController theController; 
    public boolean isValid {get; set;}
    public boolean showInvalidEntryMessage {get; set;}
    public boolean showInvalidDateFormatMessage {get; set;}
    public String preOrderDate {get; set;}

    public string getLanguage(){
        reCAPTCHA_controller cls = new reCAPTCHA_controller();
        return cls.getLanguage();
    }
    public string getDefaultCode(){
        return 'ABC-ABC-ABC';
    }
    public DSTNY_PreOrderSupportForm(ApexPages.StandardController cont){
        try{
            theController = cont;
            this.isValid = false;
            this.showInvalidEntryMessage = false;
            this.showInvalidDateFormatMessage = false;
            if(cont.getRecord().get('id') != null){
                //validate that they are populating the fields for the first time
                if(isBlankOrNull([SELECT Bungie_User_Id__c FROM Destiny_Support_Issue__c WHERE Id = :theController.getId()].Bungie_User_Id__c))
                {
                    this.isValid = true;
                    cont.getRecord().put('Code__c',getDefaultCode());
                }
                /* Removed for security 
                map<String, String> mapHeaders = ApexPages.currentPage().getHeaders();
                String ipAddress = '';
                if(mapHeaders != null){
                    ipAddress = mapHeaders.get('True-Client-IP');
                    if(ipAddress == null) ipAddress = mapHeaders.get('X-Salesforce-SIP');
                    if(ipAddress != ''){
                        cont.getRecord().put('Ip_Address__c', ipAddress);
                    }
                }*/
                for(RecordType rt : [SELECT Id FROM RecordType WHERE sObjectType = 'Destiny_Support_Issue__c' AND IsActive = true AND DeveloperName = 'Beta_Go_Live']){
                    cont.getRecord().put('RecordTypeId',rt.Id);
                }
            }
        }catch(Exception e){
            system.debug(e);
        }
    }
    public boolean availableCodes{
        get{
            boolean result = false;
            try{
                String plat = ((Destiny_Support_Issue__c) theController.getRecord()).Platform__c;
                if(plat == null || plat == '') 
                    return result;
                else{
                    String codeTypeId = '';
                    String ctName = 'DestinyPreOrderSupport-';
                    if(plat.containsIgnoreCase('xbox')){
                        if(plat.contains('360')) ctName += 'xbox_360';
                        else ctName += 'xbox_one';
                        for(CRR_Code_Type__c typ : [SELECT Id FROM CRR_Code_Type__c WHERE Name = :ctName LIMIT 1]){
                            codeTypeId += typ.Id;
                        }
                    }else if(
                        plat.containsIgnoreCase('asia') || 
                        plat.containsIgnoreCase('azie') || 
                        plat.containsIgnoreCase('asie') || 
                        plat.containsIgnoreCase('asien'))
                    {
                        if(plat.contains('3')) ctName += 'scej_ps3';
                        else ctName += 'scej_ps4';
                        for(CRR_Code_Type__c typ : [SELECT Id FROM CRR_Code_Type__c WHERE Name = :ctName LIMIT 1]){
                            codeTypeId += typ.Id;
                        }
                    }else if(plat.containsIgnoreCase('america')){
                        if(plat.contains('3')) ctName += 'scea_ps3';
                        else ctName += 'scea_ps4';
                        for(CRR_Code_Type__c typ : [SELECT Id FROM CRR_Code_Type__c WHERE Name = :ctName LIMIT 1]){
                            codeTypeId += typ.Id;
                        }
                    }else if(plat.containsIgnoreCase('europ')){
                        if(plat.contains('3')) ctName += 'scee_ps3';
                        else ctName += 'scee_ps4';
                        for(CRR_Code_Type__c typ : [SELECT Id FROM CRR_Code_Type__c WHERE Name = :ctName LIMIT 1]){
                            codeTypeId += typ.Id;
                        }
                    }
                    String countQuery = 'SELECT count() FROM CRR_Code__c WHERE Code_Type__c = \'';
                    if(codeTypeId == '') codeTypeId = 'invalidcodetypeid';
                    countQuery += codeTypeId ;
                    countQuery += '\' AND Redeemed_by__c = null LIMIT 1';
                    return Database.countQuery(countQuery) > 0;
                }
            }catch(Exception e){
                system.debug('ERROR == '+e);
                return false;
            }
        }
        set;
    }
    public pageReference customSave(){
        this.showInvalidEntryMessage = false;
        this.showInvalidDateFormatMessage = false;
        try{
            Destiny_Support_Issue__c theRec = (Destiny_Support_Issue__c) theController.getRecord();
            if(checkForBlankFields(theRec))
                return null;
            else{
                createContact(
                    theRec.Email__c, 
                    IsBlankOrNull(theRec.Bungie_Username__c) ? theRec.Email__c : theRec.Bungie_Username__c
                    );
                if(theRec.Region__c == 'My country is not listed'){
                    theRec.Resolution__c = 'No action';
                    theRec.Status__c = 'Closed';
                }
                update theRec;
                this.isValid = false; //no longer an initial request
            }
        }catch(Exception e){
            system.debug(e);
        }
        return null;
    }
    //there must be a contact record to enable distribution of a code to this customer
    //the futureness is to avoid the customer from being delayed by the potentially long running query 
    @future 
    public static void createContact(String emailAddress, String bungieUserName){
        Integer existingContact = [SELECT count() FROM Contact WHERE Email = :emailAddress LIMIT 1];
        if(existingContact == 0){
            insert new Contact(
                Email = emailAddress,
                LastName = bungieUserName
            );
        }
    }
    public boolean isBlankOrNull(String s){ return s == null || s == ''; }
    public boolean checkForBlankFields(Destiny_Support_Issue__c pTheRec){
        String errorLbl = Label.Contact_Us_Required;
        boolean emptyFields = false;
        /*
        if(isBlankOrNull(pTheRec.Bungie_Username__c)){
            emptyFields = true;
            pTheRec.Bungie_Username__c.addError(errorLbl);
        }
        if(isBlankOrNull(pTheRec.Bungie_User_Id__c)){
            emptyFields = true;
            pTheRec.Bungie_User_Id__c.addError(errorLbl);
        }
        if(isBlankOrNull(pTheRec.Error_Message__c)){
            emptyFields = true;
            pTheRec.Error_Message__c.addError(errorLbl);
        }*/
        if(isBlankOrNull(pTheRec.Email__c)){
            emptyFields = true;
            pTheRec.Email__c.addError(errorLbl);
        }
        if(isBlankOrNull(pTheRec.Alternate_Email__c)){
            emptyFields = true;
            pTheRec.Alternate_Email__c.addError(errorLbl);
        }
        if(isBlankOrNull(pTheRec.Platform__c)){
            emptyFields = true;
            pTheRec.Platform__c.addError(errorLbl);
        }/*
        if(isBlankOrNull(pTheRec.Region__c)){
            emptyFields = true;
            pTheRec.Region__c.addError(errorLbl);
        }
        if(isBlankOrNull(pTheRec.Retailer__c)){
            emptyFields = true;
            pTheRec.Retailer__c.addError(errorLbl);
        }
        if(isBlankOrNull(preOrderDate)){
            emptyFields = true;
        }else{*/
            try{
                ptheRec.Preorder_date__c = Date.valueOf(preOrderDate);
            }catch(Exception e){
                //emptyFields = true;
                //pTheRec.Preorder_date__c.addError(Label.DSTNY_INV_DATE);
            }
        //}
        //boolean validCode = validateCode(pTheRec); //code no longer required
        //if(emptyFields == false){ //no empty fields, would have proceeded
          //  if(validCode == false) emptyFields = true; //invalid code, block progress
        //}
        return emptyFields;
        
    }
    /*
    public boolean validateCode(Destiny_Support_Issue__c pTheRec){
        boolean result = true;
        String origCode = pTheRec.Code__c;
        //cleanup of input
        origCode = origCode.trim().toUpperCase(); 
        //no these are not the same characters, isn't encoding fun!!
        origCode = origCode.replaceAll('-','');
        origCode = origCode.replaceAll('–','');
        if(origCode.length() > 9 || origCode.length() < 9){
            pTheRec.Code__c.addError(Label.Invalid_entry_Please_try_again);
            return false; //code format is 'xxx-xxx-xxx' but we trimmed out the hyphens, so max & min is 9
        }
        
        set<String> validChars = new set<String>{'A','C','D','F','G','H','J','K','L','M','N','P','R','T','V','X','Y','3','4','6','7','9'};
        String testedCode = origCode, formattedCode = '';
        for(Integer i = 1; i < testedCode.length()+1; i++){
            String theChar = testedCode.subString(i-1, i);
            system.debug(theChar);
            if(i==4||i==7){//hyphen locations
                formattedCode+='-';
            }
            //test character to see if valid
            if(! validChars.contains(theChar)){
                pTheRec.Code__c.addError(Label.Invalid_entry_Please_try_again);
                result = false;
            }
            formattedCode+=theChar;
        }
        
        pTheRec.Code__c = formattedCode;
        return result;
    }
    */
    
}