@isTest
public class DSTNY_PreOrderSupportForm_test{
    public static testMethod void test1(){
        DSTNY_PreOrderSupportForm tstCls = new DSTNY_PreOrderSupportForm(
            new ApexPages.StandardController(
                new Destiny_Support_Issue__c()
            )
        );
        tstCls.customSave();
        boolean tstBool = tstCls.isValid;
        tstCls.getLanguage();
        Destiny_Support_Issue__c tstRec = new Destiny_Support_Issue__c(
            Bungie_User_Id__c = '123',
            Code__c = '123',
            Error_Message__c = '123'
        );
        insert tstRec;
        tstCls = new DSTNY_PreOrderSupportForm(
            new ApexPages.standardController(
                tstRec
            )
        );
        tstCls.customSave();

        tstCls.theController.getRecord().put('Bungie_Username__c','123');
        tstCls.theController.getRecord().put('Bungie_User_Id__c','123');
        tstCls.theController.getRecord().put('Error_Message__c','123');
        tstCls.theController.getRecord().put('Code__c','333-333-313'); //invalid code
        tstCls.theController.getRecord().put('Email__c','2@2.com');
        tstCls.theController.getRecord().put('Platform__c','xbox');
        tstCls.theController.getRecord().put('Alternate_Email__c','3@3.com');
        tstCls.theController.getRecord().put('Region__c','Bulgaria');
        tstCls.theController.getRecord().put('Retailer__c','Best Buy');
        tstCls.preOrderDate = '1/1/2014'; //invalid date
        tstCls.customSave(); //error on the date
        tstCls.preOrderDate = '2014-01-01'; //valid date?
        tstCls.theController.getRecord().put('Code__c','333-333-333'); //valid code
        tstCls.customSave(); //success
        system.debug(LoggingLevel.ERROR, 'tstCls.isValid == ');
    }
    public static testMethod void test2(){
        Destiny_Support_Issue__c tstRec = new Destiny_Support_Issue__c();
        insert tstRec;
        DSTNY_PreOrderSupportForm tstCls = new DSTNY_PreOrderSupportForm(
            new ApexPages.StandardController(tstRec)
        );
        //system.assertEquals(tstCls.getDefaultCode(), 'ABC-ABC-ABC');
        
        //blank platform = no codes
        system.assert(tstCls.AvailableCodes == false);

        //Actual platforms but no codes loaded
        tstCls.theController.getRecord().put('Platform__c','asia');
        tstCls.AvailableCodes = true;
        system.assert(tstCls.AvailableCodes == false); //the get method returns false
        
        tstCls.theController.getRecord().put('Platform__c', 'xbox');
        tstCls.AvailableCodes = true;
        system.assert(tstCls.AvailableCodes == false);
        
        tstCls.theController.getRecord().put('Platform__c', 'europe');
        tstCls.AvailableCodes = true;
        system.assert(tstCls.AvailableCodes == false);
        
        tstCls.theController.getRecord().put('Platform__c', 'america');
        tstCls.AvailableCodes = true;
        system.assert(tstCls.AvailableCodes == false);

        //load codes
        DSTNY_CodeDistribution_Test.createTestData();
    }
}