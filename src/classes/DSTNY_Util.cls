public class DSTNY_Util{
    @future
    public static void deleteBlankRecords(Id[] idsToDel){
        try{
            database.delete([SELECT Id FROM Destiny_Support_Issue__c WHERE Id IN :idsToDel], false);
        }catch(Exception e){
            system.debug(e);
        }
    }
}