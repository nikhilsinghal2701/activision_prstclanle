public class DWSingleIdentity implements Schedulable{
   
    ApexPages.standardController cont;
    boolean isProd = UserInfo.getOrganizationId() == '00DU0000000HMgwMAG';
    public DWSingleIdentity(ApexPages.StandardController pcont){
        cont = pcont;
    }
    public DWSingleIdentity(){}
    public static void setSchedule(){
        System.schedule('DW-SI-Hourly-Token-Refresh', '0 30 * * * ?', new DWSingleIdentity());
    }
    public static void execute(SchedulableContext ctxt){
        getSFDCClientTokenAsynchronously();
    }

    public pageReference updateLinkedAccounts(){
       
        Contact theContact;
        Atvi_GetBunchballGamficationScore cls = new Atvi_GetBunchballGamficationScore();
        ATVI_GetClanLeaderInfo cls1 = new ATVI_GetClanLeaderInfo();
        try{
            theContact = [SELECT UCDID__c,isClan_Leader__c, (SELECT Contact__c, DWID__c, Gamertag__c, Is_Current__c, Platform__c FROM ELITE_Platform__r) 
                              FROM Contact WHERE Id = :cont.getId()];
            cls.vcontactid=cont.getId();
            cls.getScore(); 
            if(cls!=null&&cls.thepoints!=null&&cls.thepoints.nitro!=null&&cls.thepoints.nitro.balance!=null&&cls.thepoints.nitro.balance.lifetimebalance!=null)                      
            theContact.Gamification_Score__c=decimal.valueof(cls.thepoints.nitro.balance.lifetimebalance);                      
        }
        catch(exception e){system.debug('exception in customersegmentation'+e);}
        
        try{
                       if(theContact.isClan_Leader__c!=true){
                cls1.vcontactid=cont.getId();
                cls1.isClanleader();
                if(cls1!=null)                      
                    theContact.isClan_Leader__c=cls1.isClnLdr;
            }
                   }
        catch(exception e){system.debug('exception in customersegmentation'+e);}
        
        
        
        if(theContact.UCDID__c == null || theContact.UCDID__c == '') return new pageReference('/'+theContact.Id+'?nooverride=1');
        String accessToken = getSFDCClientTokenSynchronously();
        String getUrl = 'https://';
        if(isProd) getUrl += 'prod';
        else getUrl += 'dev';
        getUrl += '.umbrella.demonware.net/v1.0/users/uno/'+theContact.UCDID__c+'/?client=sfdc';
        getUrl += '@headers-exist@@Authorization#Bearer ';
        getUrl += accessToken;
        
        DwProxy.service svc = new DwProxy.service();
        String response = svc.passEncryptedGetData(getUrl);
        SI_UmbrellaResponse linkedAccountResponse = (SI_UmbrellaResponse) JSON.deserialize(response, SI_UmbrellaResponse.class);

        List<Multiplayer_Account__c> gamertagsToUpsert = new List<Multiplayer_Account__c>();
        List<Multiplayer_Account__c> dupes = new List<Multiplayer_Account__c>();
        for(Multiplayer_Account__c mpAcct : theContact.ELITE_Platform__r){
            mpAcct.Is_Current__c = false;//mark all existing GTs as inactive
        }
        if(linkedAccountResponse == null || linkedAccountResponse.accounts == null){ //no linked accounts, mark all as inactive
            update theContact.ELITE_Platform__r;
        }
        if(linkedAccountResponse != null && linkedAccountResponse.accounts != null){ //linked accounts present
            for(SI_Account linkedAccount : linkedAccountResponse.accounts){
                if(linkedAccount.username != null){
                    String accountId = String.valueOf(linkedAccount.accountId);
                    //Check for a duplicate gamertag on another account, set this "dupe" and inactive 
                    for(Multiplayer_Account__c otherUserGamertag : [SELECT DWID__c FROM Multiplayer_Account__c WHERE 
                                                                    Contact__c != :theContact.id AND 
                                                                    Platform__c = :linkedAccount.provider AND 
                                                                    Gamertag__c = :linkedAccount.username AND
                                                                    DWID__c = :accountId])
                    {
                        otherUserGamertag.Is_Current__c = false;
                        dupes.add(otherUserGamertag);
                    }
    
                    gamertagsToUpsert.add(
                        new Multiplayer_Account__c(
                            DWID__c = accountId,
                            Platform__c = linkedAccount.provider,
                            Gamertag__c = linkedAccount.username,
                            Contact__c = theContact.Id,
                            Is_Current__c = true
                        )
                    );
                }
            }
        }
        //Update gamertags:     
        if (gamertagsToUpsert.size() > 0){
            try{
                list<string> xuidList = new list<string>();
                for(Multiplayer_Account__c mp: gamertagsToUpsert){
                    xuidList.add(mp.DWID__c);
                }
                List<ban__c> banlist = new List<ban__c>();
                List<ban__c> dirtyBans = [select id from ban__c where xuid__c in :xuidList];
                System.Debug(gamertagsToUpsert);
                //gamertagsToUpsert[0].DWID__c='744306';
                for(Multiplayer_Account__c mp:gamertagsToUpsert){
                    string console;
                    if(mp.Platform__c == 'XBL'){
                        console = 'XBOX_360';
                    }else if(mp.Platform__c == 'PSN'){
                        console = 'PLAYSTATION_3';
                    }
                    if(console != null){
                        system.debug('Making the Callout..');
                        LASService.BasicHttpBinding_ILinkedAccounts worker = new LASService.BasicHttpBinding_ILinkedAccounts();
                        worker.timeout_x = 120000; //update from 1000
                        system.debug('Calling ('+mp.DWID__c+','+console+')');
                        CSServiceLib.ArrayOfBan results = new CSServiceLib.ArrayOfBan();
                        try{
                            results = worker.GetBansByXuid(mp.DWID__c, console);
                        }catch(Exception e){
                            system.debug(e);
                        }
                        if(results.ban!=null){
                            //ban__c temp;
                            for(CSServiceLib.ban ban: results.ban){
                                ban__c temp = new ban__c();
                                temp.Category__c = ban.m_category;
                                temp.Start_Date__c = ban.m_startDate;
                                temp.End_Date__c = ban.m_endDate;
                                temp.Guid__c = ban.m_guid;
                                temp.Platform__c = ban.m_platform;
                                temp.Reason__c = ban.m_reason;
                                temp.Title__c = ban.m_title;
                                temp.type__c = ban.m_type;
                                temp.XUID__c = string.valueof(ban.m_xuid);
                                temp.Multiplayer_Account__r = new Multiplayer_Account__c(DWID__c=mp.DWID__c);
                                banlist.add(temp);
                            }
                        } 
                    }
                }
                update theContact.ELITE_Platform__r; //mark all of the user's current linked accounts as inactive
                update dupes; //mark the similarly named accounts as inactive
                database.upsert(gamertagsToUpsert, Multiplayer_Account__c.fields.DWID__c); //add the new accounts
                delete dirtyBans;
                upsert banlist;
            }catch(Exception ex){
                Pagereference redirectURL = new PageReference('/' + theContact.id + '?nooverride=1&error='+ex.getMessage());
                return redirectURL;
            }
        }
      
        try{
        update thecontact;
        }
        catch(exception e){system.debug('exception in customersegmentation'+e);}
        
        Pagereference redirectURL = new PageReference('/' + theContact.id + '?nooverride=1');
        return redirectURL;
    }

    public String getSFDCClientTokenSynchronously(){
        Integration_Credential__c cred;
        for(Integration_Credential__c cre : [SELECT Big_Token__c, LastModifiedDate FROM Integration_Credential__c 
                                             WHERE Application_Name__c = 'SingleIdentityUmbrellaToken'])
        {
            cred = cre;
        }
        if(cred == null || cred.LastModifiedDate < system.now().addMinutes(-50)){   
            DwProxy.service svc = new DwProxy.service();
            String response;
            String reqEndpoint = 'https://dev.umbrella.demonware.net/v1.0/tokens/clientcredentials/?client=sfdc';
            String reqBody = '{"clientSecret":"8a33a363a543a7837e28a09f124073b3"}';
            
            if(isProd) reqEndpoint = 'https://prod.umbrella.demonware.net/v1.0/tokens/clientcredentials/?client=sfdc';
            if(isProd) reqBody = '{"clientSecret":"34ce89d59a8615d267967fc53550fe89aa7dff99d6b8ff54"}';

            response = svc.passEncryptedPostData(reqEndpoint, reqBody, 'application/json');

            try{
                SI_UmbrellaTokenResponse resp = (SI_UmbrellaTokenResponse)JSON.deserialize(response, SI_UmbrellaTokenResponse.class);
                return resp.accessToken;
            }catch(Exception e){
                return '';
            }
        }
        return cred.Big_Token__c;
    }
    @future(callout=true)
    public static void getSFDCClientTokenAsynchronously(){
        DwProxy_utils dwUtils = new DwProxy_utils();
        String sessionId = dwUtils.getSessionId('Demonware Promotion Code Id Sync');
        DwProxy.service svc = new DwProxy.service();
        svc.timeout_x = 120000;
        String response;
        String reqEndpoint = 'https://dev.umbrella.demonware.net/v1.0/tokens/clientcredentials/?client=sfdc';
        String reqBody = '{"clientSecret":"8a33a363a543a7837e28a09f124073b3"}';

        boolean isProd = UserInfo.getOrganizationId() == '00DU0000000HMgwMAG';
        if(isProd) reqEndpoint = 'https://prod.umbrella.demonware.net/v1.0/tokens/clientcredentials/?client=sfdc';
        if(isProd) reqBody = '{"clientSecret":"34ce89d59a8615d267967fc53550fe89aa7dff99d6b8ff54"}';

        try{
            response = svc.passEncryptedPostData(reqEndpoint, reqBody, 'application/json', sessionId);
            SI_UmbrellaTokenResponse resp = (SI_UmbrellaTokenResponse)JSON.deserialize(response, SI_UmbrellaTokenResponse.class);
            Integration_Credential__c cred = new Integration_Credential__c(
                Application_Name__c = 'SingleIdentityUmbrellaToken',
                Authorization_Type__c = 'UN / PW'
            );
            cred.Big_Token__c = resp.accessToken;
            database.upsert(cred, Integration_Credential__c.fields.Application_Name__c);
        }catch(Exception e){
            system.debug('ERROR == '+e);
        }

    }
    public class SI_UmbrellaTokenResponse{
        public long expires {get; set;}
        public string accessToken {get; set;}
    }
    public class SI_UmbrellaResponse{
        public SI_Account[] accounts {get; set;}
        public String umbrellaID {get; set;}
    }
    public class SI_Account{
        public string accountId {get; set;}
        public string secondaryAccountID {get; set;}
        public boolean authorized {get; set;}
        public string provider {get; set;}
        public string username {get; set;}
    }

}