@isTest
public class DWSingleIdentity_Test{
    public static testMethod void createTestData(){
        insert new Contact[]{
            new Contact(
                FirstName = 'Mr.',
                LastName = 'Tester',
                Email = '123@123.123',
                UCDID__c = '1111111111',
                Birthdate = system.today().addDays(-10000)
            ),
            new Contact(
                LastName = 'Twitter_User'
            )
        };
    }
    public static testMethod void test1(){
        createTestData();
        Test.setMock(WebServiceMock.class, new DwSingleIdentity_MockCallout());
        test.startTest();
            DWSingleIdentity tstCls = new DWSingleIdentity();
            Contact c = [SELECT Id FROM Contact LIMIT 1];
            tstCls = new DWSingleIdentity(new ApexPages.standardController(c));
            tstCls.updateLinkedAccounts();
        test.stopTest();
    }
}