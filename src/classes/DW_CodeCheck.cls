public class DW_CodeCheck{
    //Variables for Query
    public string code {get; set;}
    public string promotionId {get; set;}
    public string ucdId {get; set;}
    string env = UserInfo.getOrganizationId() != '00DU0000000HMgwMAG' ? 'dev' : 'prod';
    
    //Variables for response
    public string response {get; set;}
    public map<String, object> objResponse {get; set;}
    public string[] objRespKeys {get; set;}
    public boolean showResponse {get; set;}
    public Long redeemCode {get; set;}
    public boolean canRedeem {get; set;}
    public string promotionName {get; set;}
    
    //service
    DWproxy.service serv = new DWproxy.service();
    
    //Gamer search
    public contact gamer {get; set;}
    String gamerQuery = 'SELECT Id, Name, Email, UCDID__c, Account.Name FROM Contact WHERE ';
    public case gamerCase {get; set;}
    public boolean gamerChosen {get; set;}
    public contact[] foundGamers {get; set;}
    
    //Controller - maybe we've come from a gamer or case page
    public DW_CodeCheck(ApexPages.StandardController cont){init(cont.getId());}
    public DW_CodeCheck(){
        String recId;
        if(ApexPages.currentPage() != null && ApexPages.currentPage().getParameters().containsKey('id')) 
            recId = ApexPages.currentPage().getParameters().get('id');
        init(recId);
    }
    void init(String recId){
        serv.timeout_x = 120000;
        this.gamer = new Contact();
        this.gamerCase = new Case();
        if(recId != null && recId != ''){
            String idPrefix = recId.subString(0, 3);
            if(idPrefix == '003'){ //contact
                for(Contact c : database.query(gamerQuery+'Id = :recId LIMIT 1')){
                    this.gamer = c;
                    this.ucdid = this.gamer.UCDID__c;
                }
            }else if(idPrefix == '500'){
                for(Case c : database.query('SELECT Id, ContactId FROM Case WHERE Id = :recId LIMIT 1')){
                    this.gamerCase = c;
                    if(this.gamerCase.ContactId != null){
                        this.gamer = database.query(gamerQuery+'Id = \''+this.gamerCase.ContactId+'\' LIMIT 1');
                        this.ucdid = this.gamer.UCDID__c;
                    }
                }
            }
            if(isBlankOrNull(this.ucdid)) this.ucdid = this.gamer.account.name;
        }
    }
    
    public void initForGamer(){//Call this method when using the system for a gamer
        try{
            for(User u : [SELECT id, ContactId FROM User WHERE Id = :UserInfo.getUserId() AND ContactId != null]){ //there can be only one
                system.debug('ucdid == '+this.ucdid);
                for(Contact c :database.query(gamerQuery+'Id = \''+u.ContactId+'\' LIMIT 1')){ //there will be only one
                    this.gamer = c;
                    this.ucdid = this.gamer.UCDID__c;
                    if(isBlankOrNull(this.ucdid)){//this will happen when they register and login at the same time
                        this.ucdid = c.Account.Name; //
                    }
                }
            }
            spamPrevention();
        }catch(Exception e){
            system.debug('ERROR = '+e);
        }
    }
    public boolean enablePage {get; set;}
    public void spamPrevention(){
        enablePage = false;
        try{
            if(ApexPages.currentPage() != null && ApexPages.currentPage().getParameters().containsKey('n')){
                String strPassKey = ApexPages.currentPage().getParameters().get('n');
                String urlDecoded = EncodingUtil.urlDecode(strPassKey,'UTF-8');
                Blob passKey = EncodingUtil.base64decode(urlDecoded);
                Blob passPhrase = Crypto.decryptWithManagedIV('AES256', Blob.valueOf('asdfqwerzxcvpoiu;lkj.,mn12345678'), passKey);
                String[] twoFactor = passPhrase.toString().split(':::');
                if(twoFactor[0] == UserInfo.getUserId()){
                    DateTime dt = DateTime.newInstance(Long.valueOf(twoFactor[1]));
                    if((system.now().getTime() - dt.getTime()) <= 1200){
                        enablePage = true; //less than 13 seconds between request and load
                    }
                }
            }
        }catch(Exception e){
            system.debug('ERROR === '+e.getMessage());
        }
    }
    public void reset(){
        this.ucdId = '';
        this.gamerChosen = false;
        this.response = '';
        this.objResponse = new map<String, object>();
        this.objRespKeys = new String[]{};
        this.redeemCode = null;
        this.canRedeem = null;
    }
    public pageReference searchGamers(){
        if(this.gamer.email != null && this.gamer.email != ''){
            reset();
            this.foundGamers = database.query(gamerQuery+' Email = \''+String.escapeSingleQuotes(this.gamer.email)+'\'');
        }else
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Must specify email address'));
        return null;
    }
    
    public pageReference chooseGamer(){
        if(this.ucdid != null && this.ucdid != ''){
            this.foundGamers = null;
            this.gamerChosen = true;
        }
        return null;
    }
    map<String, DW_Promotion_Code_Id__c> promoMap; //so we can eventually determine if we query DW or callofduty.com
    public selectOption[] getPromotionCodeIds(){
        promoMap = new map<String, DW_Promotion_Code_Id__c>();
        String promotionQuery = 'SELECT Id, Name, Enabled_for_Customer_Portal__c,';
        promotionQuery += 'Friendly_Name__c, Promotion_Id__c, System_To_Query__c FROM DW_Promotion_Code_Id__c ';
        if(UserInfo.getUserType() != 'Standard'){
            promotionQuery += 'WHERE Enabled_for_Customer_Portal__c = true ';
        }
        promotionQuery += 'ORDER BY Name';
        selectOption[] results = new selectOption[]{};
        for(DW_Promotion_Code_Id__c id : database.query(promotionQuery)){
            String displayName = isBlankOrNull(id.Friendly_name__c) ? id.Name : id.Friendly_Name__c;
            results.add(new selectOption(id.Name, displayName));
            promoMap.put(id.Promotion_Id__c, id);
        }
        return results;
    }
    
    String systemToQuery = 'ucd.demonware.net'; //default
    public String getSystemToQuery(){
        return systemToQuery;
    }
    //compute the DW URL
    public String getDwQuery(){
        code = code.trim().replaceAll(' ','').replaceAll('-','');
        //legacy, now default value
        String dwQuery = 'https://'+env+'.ucd.demonware.net/ucd/service/promotions/'+promotionId.trim()+'/'+code.trim();
        if(! isBlankOrNull(this.ucdId)) dwQuery += '/?user='+this.ucdId.trim();
        
        if(promoMap.containsKey(promotionId.trim())){
            systemToQuery = promoMap.get(promotionId.trim()).System_To_Query__c;
            if(systemToQuery == 'callofduty.com'){
                String altEnv = env == 'dev' ? 'uat' : 'profile';
                dwQuery = 'https://'+altEnv+'.callofduty.com/promotions/service/promotion/codeCheck/'+code.trim();
            }
        }
        system.debug('dwQuery == '+dwQuery);
        return dwQuery;
    }
    public pageReference choosePromo(){
        try{
            if(promoMap.containsKey(promotionId.trim())){
                systemToQuery = promoMap.get(promotionId.trim()).System_To_Query__c;
            }
        }catch(exception e){
            
        }
        return null;
    }

    //have they provided enough information for us?
    public boolean doDataCheck(){
        return ! (code == null || promotionId == null || code == '' || promotionId == '');
    }
    public codResponse theCodResp {get; set;}
    public pageReference doQuery(){
        try{
            if(doDataCheck()){
                //ucd.demonware.net response
                //this.response = '{"Promotion": {"updated": "2013-08-19T16:29:48Z", "code": "ghosts-monster", "entitlement": "2XP15", "created": ';
                //this.response += '"2013-08-15T13:05:12Z", "platformId": "2535285329497070", "redeemReason": "promotion already redeemed", "region"';
                //this.response += ': "US", "redeemed": true, "community": "elite", "redeemUser": "/ucd/service/user/13473556723800790228/", "fulfilled"';
                //this.response += ': false, "platform": "xbl", "redeemedByUser": true, "value": "4H360Y91SW2G5", "redeemCode": 1902, "redeemUserId";
                //this.response += ': 13473556723800790228, "canRedeem": false, "type": 4}}';
                
                //callofduty.com response format
                //class codResponse{
                this.response = serv.passEncryptedGetData(getDwQuery());
                system.debug('this.response == '+this.response);
                this.showResponse = true;
                this.redeemCode = null;
                this.canRedeem = null;
                objResponse = new map<String, object>();
                if(systemToQuery == 'callofduty.com'){
                    this.showResponse = false;
                    theCodResp = (codResponse) JSON.deserialize(this.response, codResponse.class);
                    if(theCodResp.promotionName == 'CODE NOT FOUND'){
                        this.canRedeem = false;
                    }/*else if(theCodResp.burnUserId == null){ //burnUserId blank does not mean the current user can redeem
                        this.canRedeem = true;
                    }else if(! isBlankOrNull(this.ucdId) && theResp.burnUserId == this.ucdId){
                        this.canRedeem = false; //already 
                    }*/
                }else{
                    if(this.response.contains('"Promotion":')){
                        try{
                            //resp is a JSON string
                            JSONParser parser = JSON.createParser(this.response);
                            objResponse = new map<String, object>();
                            JSONToken typ;
                            while (parser.nextToken() != null) {
                                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                                    String fieldName = parser.getText();
                                    if(fieldName == 'code') fieldName = 'promotion';
                                    else if(fieldName == 'value') fieldName = 'code';
                                    parser.nextToken();
                                    typ = parser.getCurrentToken();
                                    if(typ == JSONToken.VALUE_NUMBER_INT){
                                        objResponse.put(fieldName, parser.getLongValue());
                                    }else if(typ == JSONToken.VALUE_NUMBER_FLOAT){
                                        objResponse.put(fieldName, parser.getIntegerValue());
                                    }else if(typ == JSONToken.VALUE_STRING){
                                        try{
                                            objResponse.put(fieldName, parser.getDatetimeValue());
                                        }catch(Exception e){
                                            try{
                                                objResponse.put(fieldName, parser.getText());
                                            }catch(Exception e1){
                                            }
                                        }
                                    }else if(typ == JSONToken.VALUE_FALSE){
                                        objResponse.put(fieldName, false);
                                    }else if(typ == JSONToken.VALUE_TRUE){
                                        objResponse.put(fieldName, true);
                                    }                                
                                }
                            }
                            this.objRespKeys = new String[]{};
                            this.objRespKeys.addAll(this.objResponse.keyset());
                            this.objRespKeys.sort();
                            this.showResponse = false;
                            if(this.objResponse.containsKey('redeemCode')){
                                this.redeemCode = (Long) this.objResponse.get('redeemCode');
                                /*if(this.redeemCode == 1907 && UserInfo.getUserType() != 'Standard' && ! isBlankOrNull(this.ucdId)){
                                    if(thisFaultyCodeHasNotAlreadyBeenMadeGood()){
                                        findEntitlementToGrant();
                                    }
                                }*/
                            }
                            if(this.objResponse.containsKey('canRedeem')){
                                this.canRedeem = (boolean) this.objResponse.get('canRedeem');
                            }
                        }catch(Exception e){
                            system.debug('ERROR == '+e);
                            ApexPages.addMessages(e);
                        }
                    }
                }//end ucd.demonware.net response parsing
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'You must specify a code and promotion'));
            }
        }catch(Exception e){
            response = e.getMessage();
        }
        return null;
    }
    public class codResponse{
        public string promotionName {get; set;}
        public string burnUserId {get; set;}
        public string firstPartyCode {get; set;}
        public string platform {get; set;}
        public string region {get; set;}
        public string title {get; set;}
        public string redeemDateUtc {get; set;}
        public string fulfillDateUtc {get; set;}
        public boolean valid {get; set;}
        public boolean redeemed {get; set;}
        public boolean fulfilled {get; set;}
    }
    
    //public boolean thisFaultyCodeHasNotAlreadyBeenMadeGood(){
      //  return [SELECT count() FROM Entitlement_Grant_Log__c WHERE Code__c = :code AND Promotion__c = :promotionId LIMIT 1] == 0;
    //}
    public set<String> alreadyGranted = new set<String>();
    /*
    public String getUserQuery(){
        String dwQuery = 'https://'+env+'.ucd.demonware.net/ucd/service/user/'+this.ucdId;
        system.debug('dwQuery == '+dwQuery);
        return dwQuery;
    }
    public pageReference findEntitlementToGrant(){
        alreadyGranted = new set<String>();
        //internal error with the code we gave them, add entitlement
        map<String, Object> ucdUserResponse = (map<String, Object>) JSON.deserializeUntyped(
            serv.passEncryptedGetData(
                getUserQuery()
            )
        );
       
        map<String, Object> ucdUserDetails = (map<String, Object>) ucdUserResponse.get('UserResponse');
        if(ucdUserDetails.containsKey('promotionList')){
            map<String, Object> promotionArray = (map<String, Object>) ucdUserDetails.get('promotionList');
            for(Object promoList : promotionArray.values()){//only 1 value
                for(Object aPromo : (Object[]) promoList){
                    system.debug('aPromo == '+aPromo);
                    map<String, Object> promo = (map<String, Object>) aPromo; //so we can access by .get('key')
                    if(promo.containsKey('promotionCode') && //we have the promotionCode "id"
                       promo.get('promotionCode') == promotionId && //test it
                       promo.containsKey('entitlement') //we have a entitlement "id"
                      ){
                          alreadyGranted.add((String) promo.get('entitlement')); //add it as already granted
                      }   
                }
            }
        }
        boolean hasXBL = [SELECT count() FROM Entitlement_Promotion_Junction__c WHERE Promotion__r.Name = :promotionId AND 
                          (NOT Entitlement__r.Entitlement_Api_Name__c IN :alreadyGranted) AND Entitlement__r.Platform__c = 'xbl'
                          LIMIT 1] > 0;
        boolean hasPSN = [SELECT count() FROM Entitlement_Promotion_Junction__c WHERE Promotion__r.Name = :promotionId AND 
                          (NOT Entitlement__r.Entitlement_Api_Name__c IN :alreadyGranted) AND Entitlement__r.Platform__c = 'psn'
                          LIMIT 1] > 0;
        if(hasXBL && hasPSN){
            platformPrompt = true;
        }else{
            return grantEntitlement();
        }
        return null;
    }
    public pageReference grantEntitlement(){
        Entitlement__c toBeGranted;
        Entitlement_Promotion_Junction__c junc;
        if(platformPrompt){
            for(Entitlement_Promotion_Junction__c jun : [SELECT Entitlement__c FROM Entitlement_Promotion_Junction__c 
                                                      WHERE Promotion__r.Name = :promotionId AND 
                                                       (NOT Entitlement__r.Entitlement_Api_Name__c IN :alreadyGranted) AND
                                                        Entitlement__r.Platform__c = :chosenPlatform
                                                      ORDER BY Grant_Sequence__c LIMIT 1])
            {
                junc = jun;
            }
        }else{
            for(Entitlement_Promotion_Junction__c jun : [SELECT Entitlement__c FROM Entitlement_Promotion_Junction__c 
                                                      WHERE Promotion__r.Name = :promotionId AND 
                                                       (NOT Entitlement__r.Entitlement_Api_Name__c IN :alreadyGranted) 
                                                      ORDER BY Grant_Sequence__c LIMIT 1])
            {
                junc = jun;
            }
        }
        if(junc != null){
            for(Entitlement__c tbg : [SELECT Id, Game__c, Entitlement_Api_Name__c, Entitlement_Type__c, Grant_Value__c, 
                                      Entitlement_description__c, Group__c, Entitlement_Grant_Method__c, Platform__c 
                                      FROM Entitlement__c WHERE Id = :junc.Entitlement__c])
            {
                toBeGranted = tbg;
            }
        }
        
        if(toBeGranted == null){
            //show message that they have already redeemed all entitlements for this platform
            return null;
        }
        Boolean isBoolEntitlement = toBeGranted.Entitlement_Type__c.toLowerCase() == 'boolean';
        Integer intGrantVal;
        Boolean boolGrantVal;
        if(isBoolEntitlement){
            boolGrantVal = toBeGranted.Grant_Value__c == 'true' || toBeGranted.Grant_Value__c == '1';
        }else{
            intGrantVal = Integer.valueOf(toBeGranted.Grant_Value__c);
        }
        Gamer_Entitlements cls = new Gamer_Entitlements();
        cls.newEntitlementValue = isBoolEntitlement ? (object) boolGrantVal : (object) intGrantVal;
        cls.ent = toBeGranted;
        cls.gamer = this.gamer;
        cls.selectedEntitlement = toBeGranted;
        cls.entitlementApiName = toBeGranted.Entitlement_Api_Name__c;
        cls.addEntitlement(); 
        return null;
    }
    public string chosenPlatform{get;set;}
    public boolean platformPrompt{get;set;}
    public pageReference doRedeem(){
        a_PromotionResponse request = new a_PromotionResponse(
            'elite', '123', '123', '3'
        );
        String JSONmsg = JSON.serialize(response);
        system.debug(JSONmsg);
        return null;
    }
    public class a_PromotionResponse{
        String communityId, promotionCode, promotionValue, promotionType, region, platform, platformId;
        Boolean fulfilled;
        
        public a_PromotionResponse(
            String pCommunityId, String pPromotionCode, String pPromotionValue, String pPromotionType
        ){
            this.communityId = pCommunityId;
            this.promotionCode = pPromotionCode; //theId of the promotion
            this.promotionValue = pPromotionValue; //theCode to be redeemeed
            this.promotionType = pPromotionType; //3, 4 or 5
        }
        public void addRegion(String pRegion){ this.region = pRegion; }
        public void addPlatform(String pPlatform){ this.platform = pPlatform; }
        public void addPlatformId(String pPlatformId){ this.platformId = pPlatformId; }
        public void addFulfilled(Boolean pFulfilled){ this.fulfilled = pFulfilled; }
    }
    */
    public boolean isBlankOrNull(String s){ return s == '' || s == null; }
}