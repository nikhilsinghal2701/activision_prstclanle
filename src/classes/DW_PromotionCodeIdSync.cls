global class DW_PromotionCodeIdSync implements Schedulable{
    global void execute(SchedulableContext sc){
        DW_PromotionCodeIdSync.syncPromotionIds();
    }
    //system.schedule('Hourly_DW_PromotionIdSync', '0 0 * * * ?', new DW_PromotionCodeIdSync());
    public static void syncPromotionIds(){
        DW_PromotionCodeIdSync.syncPromotionIds('');
    }
    @future(callout=true)
    public static void syncPromotionIds(String fakeResponse){//for test code coverage
        DwProxy_utils utils = new DwProxy_utils();
        String sessionId = utils.getSessionId('Demonware Promotion Code Id Sync');
        DWProxy.service serv = new DWProxy.service();
        serv.timeout_x = 120000;
        //compile data
        String method = 'GET';
        String host = utils.env + '.ucd.demonware.net';
        String path = '/ucd/service/promotions/codes/';
        String query = '';
        String response = fakeResponse;
        if(! test.isRunningTest()){
            response = serv.DwGetProxy(utils.signRequest(method, host, path, query), sessionId);
        }
        system.debug('response == '+response);
        try{
            if(response.contains('"data":')){
                dwResponse obj = (dwResponse) JSON.deserialize(response, dwResponse.class);
                DW_Promotion_Code_Id__c[] ids = new DW_Promotion_Code_Id__c[]{};
                for(String s : obj.data){
                    ids.add(new DW_Promotion_Code_Id__c(
                        Name = s,
                        Promotion_Id__c = s
                    ));
                }
                if(! ids.isEmpty()) database.upsert(ids, DW_Promotion_Code_Id__c.Promotion_Id__c, false);
            }
        }catch(Exception e){
        }
    }
    public class dwResponse{
        public string[] data;
    }
}