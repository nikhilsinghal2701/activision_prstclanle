@isTest
public class DW_PromotionCodeIdSync_test{
    public static testMethod void test2(){
        test.startTest();
        DW_PromotionCodeIdSync.syncPromotionIds('badresponse');
        test.stopTest();
    }
    public static testMethod void test3(){
        test.startTest();
        DW_PromotionCodeIdSync.syncPromotionIds('{"data":["code1","code2"]}');
        test.stopTest();
    }
    public static testMethod void test4(){
        test.startTest();
        DW_PromotionCodeIdSync.syncPromotionIds(null);
        test.stopTest();
    }
    public static testMethod void test5(){
        DW_PromotionCodeIdSync tstCls = new DW_PromotionCodeIdSync();
        DW_PromotionCodeIdSync.dwResponse tstInnerCls = new DW_PromotionCodeIdSync.dwResponse();
        
        tstInnerCls .data = new String[]{'hi'};
        system.assert(tstInnerCls .data == new String[]{'hi'});
    }
    public static testMethod void test6(){
        test.startTest();
            Datetime dt = Datetime.now().addMinutes(1);
            String cron_schedule = '0 '+dt.minute()+' * '+dt.day()+' '+dt.month()+' ? '+dt.year();
            String jobId = System.schedule(
                'ScheduleSync',
                cron_schedule, 
                new DW_PromotionCodeIdSync()
            );
        test.stopTest();
    }
}