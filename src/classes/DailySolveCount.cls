global class DailySolveCount implements Schedulable{
	
	global string chron = '0 0 3 * * ?';
    
    global void execute(SchedulableContext sc){
        CountDeflections cd = new CountDeflections();
        cd.DeflectionCount();
    }
    
    global string dailySchedule(){
    	string jobId=system.schedule('Daily Solve Count', chron, new DailySolveCount());
        return jobId;
    }
    
    public class CountDeflections { 
    //Pull all new Article Deflection records for the previous day  
    private PKB_Article_Feedback_Deflection__c[] newPKBDeflections = [Select Id,Article_ID__c,CreatedDate From PKB_Article_Feedback_Deflection__c Where CreatedDate=YESTERDAY];
    private map <Id,integer> deflectionCounts = new map <Id,integer>();
    
    
    public void DeflectionCount (){
    
    // Only process if there are new deflection records for the previous day    
      if ((newPKBDeflections != NULL) && (!(newPKBDeflections.isEmpty()))) {    
        
        // Populate the deflectionCounts map with the article IDs and counts for each ID from the previous day's new deflection records     
        for (PKB_Article_Feedback_Deflection__c dc : NewPKBDeflections) {
            if (deflectionCounts.containsKey(dc.Article_ID__c)) {
            deflectionCounts.put(dc.Article_ID__c,deflectionCounts.get(dc.Article_ID__c)+1);
            } else deflectionCounts.put(dc.Article_ID__c,1);
            }
        
        //Pull Article Solve Tracking records that match the Article IDs of the new Deflection records
        PKB_Article_Solve_Tracking__c[] currentSolveCounts = [Select Id,Name,Article_Solve_Count__c From PKB_Article_Solve_Tracking__c Where Name IN :deflectionCounts.keySet()];
        
        //Only process if there are matching Article Solve Records
        if ((currentSolveCounts != NULL) && (!(currentSolveCounts.isEmpty()))) {
            
        //Iterate through the list of Article Solve Records and update the Article Solve Count for the matching articles and remove them from the map
        for (PKB_Article_Solve_Tracking__c ast : currentSolveCounts) {
                ast.Article_Solve_Count__c = ast.Article_Solve_Count__c + deflectionCounts.get(ast.Name);
                deflectionCounts.remove(ast.Name);           
          }
        }
        //Iterate through the remaining article Ids in the map and add new Article Solve Tracking records with the current count to the list
        for (string Ids : deflectionCounts.keySet()) {
            PKB_Article_Solve_Tracking__c temp = new PKB_Article_Solve_Tracking__c (Name=Ids, Article_Solve_Count__c=deflectionCounts.get(Ids));
            currentSolveCounts.add(temp);
        }
        //Upsert the new and updated records
        upsert currentSolveCounts;
        
    }
  }
  } 
}