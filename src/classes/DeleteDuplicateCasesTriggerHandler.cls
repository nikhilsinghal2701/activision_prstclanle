public class DeleteDuplicateCasesTriggerHandler 
{
	public static void onAfterUpdate(Map<Id,Case> oldMap, Map<Id,Case> newMap)
	{
		set<Id> caseIds = new set<Id>();
		Map<Id,Id> mergedCaseMap = new Map<Id,Id>();
		
		for(Case c : newMap.values())
		{
			if(c.Status != oldMap.get(c.Id).Status && c.Status == 'Merge - Duplicate'
				&& c.Merge_To_Case__c != null)
			{
				caseIds.add(c.Id);
				mergedCaseMap.put(c.Id,c.Merge_To_Case__c);
			}
		}
		
		if(mergedCaseMap.size() > 0)
		{
			transferChatTranscripts(mergedCaseMap);
		}
	}
	
	public static void transferChatTranscripts(Map<Id,Id> mergedCaseMap)
	{
		List<LiveChatTranscript> liveChatTranscripts = new List<LiveChatTranscript>();
		
		for(LiveChatTranscript transcript : [Select Id, CaseId from LiveChatTranscript where CaseId in :mergedCaseMap.keyset()])
		{
			if(mergedCaseMap.get(transcript.CaseId) != null)
			{
				transcript.CaseId = mergedCaseMap.get(transcript.CaseId);
				liveChatTranscripts.add(transcript);
			}
		}
		
		if(liveChatTranscripts.size() > 0)
		{
			update liveChatTranscripts;
		}
	}
}