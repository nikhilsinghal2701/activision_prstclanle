global class DeleteMergeToDuplicateCasesBatch implements Database.Batchable<sObject>
{

   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      return Database.getQueryLocator([Select Id from Case where Status = 'Merge - Duplicate' and Merge_To_Case__c != :null]);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope)
   {
       if(scope.size() > 0)
       {
           Database.delete(scope, false);
       }
   }

   global void finish(Database.BatchableContext BC)
   {
       
   }
}