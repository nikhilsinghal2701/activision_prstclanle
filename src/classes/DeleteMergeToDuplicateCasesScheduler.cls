global class DeleteMergeToDuplicateCasesScheduler implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
        DeleteMergeToDuplicateCasesBatch obj = new DeleteMergeToDuplicateCasesBatch();
        database.executeBatch(obj);
    }
}