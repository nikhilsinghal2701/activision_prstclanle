@isTest
public class Destiny_Support_Issue_Before_test{
    public static testMethod void test1(){
        
        Destiny_Support_Issue__c tstRec = new Destiny_Support_Issue__c();
        insert tstRec;
        system.assert([SELECT count() FROM Destiny_Support_Issue__c] == 1);
        tstRec.Status__c = 'Closed';
        test.startTest();
            update tstRec;
        test.stopTest();
        system.assert([SELECT count() FROM Destiny_Support_Issue__c] == 0);
    }
}