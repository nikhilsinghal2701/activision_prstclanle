public class DispositionPicklistController{

    public case theCase{get;set;}
    public selectOption[] l3options{get;set;}
    public boolean GrayOut{get;set;}
    public boolean ppReq{get;set;}
    public CaseComment theCaseComment {get; set;}

    Id rmaRecType = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('RMA').getRecordTypeId();
    boolean pIsRma = false;
    public boolean getIsRMA(){
        return pIsRma;
    }
    public boolean getIsExcluded(){
        boolean excluded = false;
        try{
            Id wcrId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Web Content Request').getRecordTypeId();
            for(String fv : new Set<String>{'RecordTypeId-'+wcrId}){ //format is field-matchValue
                excluded = ((sObject) this.theCase).get('RecordTypeId') == fv.split('-')[1];
                if(excluded) break;
            }
        }catch(Exception e){
            system.debug(e);
        }
        return excluded;
    }
    public string getExcludeURL(){
        String url = '/500/e?nooverride=1&';
        for(String key : ApexPages.currentPage().getParameters().keySet()){
            if(! new set<String>{'save_new'}.contains(key.toLowerCase())){
                url += key+'='+ApexPages.currentPage().getParameters().get(key)+'&';
            }
        }
        return url.subString(0,url.length() - 1);
    }
    public map<String, SelectOption[]> eL12L2s {get; set;}
    public map<String, SelectOption[]> gL12L2s {get; set;}
    public SelectOption[] gDispsL1s {get; set;}
    public SelectOption[] eDispsL1s {get; set;}
    public boolean useEmaps {get; set;}
    public boolean disableL1 {get; set;}
    public boolean disableL2 {get; set;}
    public void loadDispositions(){
        disableL1 = true;
        disableL2 = true;
        useEmaps = false;
        gDispsL1s = new SelectOption[]{new selectOption('','--None--')};
        gL12L2s = new map<String, SelectOption[]>();
        gL12L2s.put('',new SelectOption[]{new selectOption('','--None--')});
        gL12L2s.put(null,new SelectOption[]{new selectOption('','--None--')});
        
        eDispsL1s = new SelectOption[]{new selectOption('','--None--')};
        eL12L2s = new map<String, SelectOption[]>();
        eL12L2s.put('',new SelectOption[]{new selectOption('','--None--')});
        eL12L2s.put(null,new SelectOption[]{new selectOption('','--None--')});
        
        //these variables ensure that we only have 1 version of each L2 presented
        set<String> gL2s = new set<String>(); 
        set<String> eL2s = new set<String>();
        for(AggregateResult arg : [SELECT L1__c l1,L2__c l2, Game__c g, Elite__c e FROM Disposition__c 
                                    WHERE IsActive__c=TRUE 
                                    GROUP BY L1__c,L2__c,Game__c,Elite__c
                                    ORDER BY L1__c,L2__c])
        {
            Boolean gDisp = (Boolean) arg.get('g'), eDisp = (Boolean) arg.get('e');
            String l1 = (String) arg.get('l1'), l2 = (String) arg.get('l2');
            if(gDisp){
                SelectOption[] gOpts = new SelectOption[]{new selectOption('','--None--')};
                if(gL12L2s.containsKey(l1)) gOpts = gL12L2s.get(l1);
                if(gL2s.add(l1+':'+l2)) gOpts.add(new selectOption(l2, l2));
                gL12L2s.put(l1, gOpts);
            }
            if(eDisp){
                SelectOption[] eOpts = new SelectOption[]{new selectOption('','--None--')};
                if(eL12L2s.containsKey(l1)) eOpts = eL12L2s.get(l1);
                if(eL2s.add(l1+':'+l2)) eOpts.add(new selectOption(l2, l2));
                eL12L2s.put(l1, eOpts);
            }
        }
        String[] tmpDisps = new String[]{};//this ensures that --None-- remains in position 0
        tmpDisps.addAll(gL12L2s.keyset());
        tmpDisps.sort();
        for(String l1 : tmpDisps){
            if(! isBlankOrNull(l1)){
                gDispsL1s.add(new selectOption(l1,l1));
            }
        }
        
        tmpDisps.clear();
        tmpDisps.addAll(eL12L2s.keyset());
        tmpDisps.sort();
        for(String l1 : tmpDisps){
            if(! isBlankOrNull(l1)){
                eDispsL1s.add(new selectOption(l1,l1));
            }
        }
    }
    public boolean isBlankOrNull(String s){ return s == '' || s == null; }
    public DispositionPicklistController(ApexPages.StandardController stdController){
        loadDispositions();
        Id ctrlId = stdController.getId();
        this.theCase = (Case) stdController.getRecord();
        String caseQry = 'SELECT ContactId,RecordTypeId,Appeasement_Product__c,Product_Effected__c,W2C_Origin_Language__c,Description,';
        caseQry += 'Subject,Origin,Product_L1__c,Product_L2__c,Sub_Product_DLC__c,Platform__c,Issue_Type__c,';
        caseQry += 'Issue_Sub_Type__c,L3__c,Contact.Phone,Contact.City__c,Contact.Email,Contact.Name,';
        caseQry += 'Contact.Address_Line_1__c,Contact.Address_Line_2__c,Contact.Zipcode__c,Contact.ValueOfState__c,';
        caseQry += 'Contact.ValueOfCountry__c,Problem_Description__c,Associated_General_Support_Case__c,';
        caseQry += 'Comment_Not_Required__c FROM Case';
        if(ctrlId != null){
            this.theCase = database.query(caseQry+' WHERE Id = \''+ctrlId+'\'');
        }
        if(ApexPages.currentPage() != null){
            map<String, String> pageParams = ApexPages.currentPage().getParameters();
            if(pageParams.containsKey('def_contact_id')){ //new case from contact related list
                try{
                    this.theCase.ContactId = (Id) ApexPages.currentPage().getParameters().get('def_contact_id');
                    selectedGamerId = theCase.ContactId;
                }catch(exception e){
                    system.debug(e);
                }
            }
            try{
                if(this.theCase.RecordTypeId == rmaRecType){
                    this.pIsRma = true;
                    if(pageParams.containsKey('CF00NJ0000000W6iX_lkid')){
                        String origCaseId = pageParams.get('CF00NJ0000000W6iX_lkid');
                        for(Case origCase : database.query(caseQry+' WHERE Id = \''+origCaseId+'\'')){
                            this.selectedGamerId = origCase.contactId;
                            this.theCase.Product_L1__c = origCase.Product_L1__c;
                            this.theCase.Product_L2__c = origCase.Product_L2__c;
                            this.theCase.Sub_Product_DLC__c = origCase.Sub_Product_DLC__c;
                            this.theCase.Platform__c = origCase.Platform__c;
                            this.theCase.Issue_Type__c = origCase.Issue_Type__c;
                            this.theCase.Issue_Sub_Type__c= origCase.Issue_Sub_Type__c;
                            this.theCase.L3__c = origCase.L3__c;
                            this.theCase.Associated_General_Support_Case__c = origCase.Id;
                            this.theCase.Origin = origCase.Origin;
                            this.theCase.Subject = origCase.Subject;
                            this.theCase.W2C_Origin_Language__c = origCase.W2C_Origin_Language__c;
                            this.theCase.Problem_Description__c = origCase.Description;
                            if(origCase.ContactId != null){
                                this.theCase.Phone__c = origCase.Contact.Phone;
                                this.theCase.Email__c = origCase.Contact.Email;
                                this.theCase.Ship_To__c = origCase.Contact.Name;
                                if(this.theCase.Ship_To__c.length() > 35) this.theCase.Ship_To__c.subString(0, 35);
                                this.theCase.Address_1__c = origCase.Contact.Address_Line_1__c;
                                this.theCase.Address_2__c = origCase.Contact.Address_Line_2__c;
                                this.theCase.City__c = origCase.Contact.City__c;
                                this.theCase.Zipcode__c = origCase.Contact.Zipcode__c;
                                this.theCase.State__c = origCase.Contact.ValueOfState__c;
                                this.theCase.Country__c = origCase.Contact.ValueOfCountry__c;
                            }
                            if(origCase.Appeasement_Product__c != null){
                                this.theCase.Product_Effected__c = origCase.Appeasement_Product__c;
                            }else if(origCase.Product_Effected__c != null){
                                this.theCase.Product_Effected__c = origCase.Product_Effected__c;
                            }
                        }
                    }
                }
            }catch(Exception e){
                system.debug('Dispositions Fail');
                system.debug(e);
            }
        }
        theCaseComment = new CaseComment();
        GrayOut = true;
        if(this.theCase.Product_L1__c != null && this.theCase.Product_L1__c != ''){
            if(this.theCase.Product_L1__c == this.theCase.Product_L2__c){
                this.theCase.Product_L1__c = 'Other';
            }

            getL3s(); //load the picklist with the values
            
            calcMapUsage();
            
            if(! isBlankOrNull(this.theCase.Issue_Type__c)){//l1 already chosen...
                disableL1 = false;
                try{
                    if(useEmaps) 
                        eDispsL1s.remove(0); //remove --None-- from the list
                    else
                        gDispsL1s.remove(0); //remove --None-- from the list
                }catch(Exception e){
                    system.debug('ERROR == '+e);
                }
            }
            if(! isBlankOrNull(this.theCase.Issue_Sub_Type__c)){//l2 already chosen...
                disableL2 = false;
                if((useEmaps && eL12L2s.containsKey(this.theCase.Issue_Type__c)) || 
                    (!useEmaps && gL12L2s.containsKey(this.theCase.Issue_Type__c))
                ){
                    SelectOption[] theOpts;
                    if(useEmaps) theOpts = eL12L2s.get(this.theCase.Issue_Type__c);
                    else theOpts = gL12L2s.get(this.theCase.Issue_Type__c);
                    theOpts.remove(0); //remove --None-- from the list
                }
            }
            if(! isBlankOrNull(this.theCase.L3__c)){ //an l3 has already been chosen, let's make it shown as selected
                l3Options.remove(0); //remove none from the list
                for(integer i = 0; i < l3Options.size(); i++){ //remove the duplicate entry
                    if(l3Options[i].getValue() == theCase.L3__c){
                        l3Options.remove(i);
                        break;
                    }
                }
                if(l3Options.isEmpty()) l3Options.add(new selectOption(this.theCase.L3__c, this.theCase.L3__c));
                else l3Options.add(0, new selectOption(this.theCase.L3__c, this.theCase.L3__c)); //add existing l3 as selected
            }
        }else setupL3();
    }
    public pageReference calcMapUsage(){
        try{useEmaps = this.theCase.Product_L1__c.containsIgnoreCase('elite') || this.theCase.Product_L1__c.containsIgnoreCase('app');
        }catch(Exception e){system.debug('e == '+e);useEmaps = false;}
        system.debug('useEmaps == '+useEmaps);
        return null;
    }
    public selectOption[] getDispsL1s(){
        if(useEmaps) return eDispsL1s;
        return gDispsL1s;
    }
    public selectOption[] getL12L2s(){
        if(useEmaps){
            if(eL12L2s.containsKey(this.theCase.Issue_Type__c)) return eL12L2s.get(this.theCase.Issue_Type__c);
        }else{
            if(gL12L2s.containsKey(this.theCase.Issue_Type__c)) return gL12L2s.get(this.theCase.Issue_Type__c);
        }
        return null;
    }
    public pageReference setupL3(){
        l3Options = new selectOption[]{new selectOption('','--None--')};
        ppReq = false;
        return null;
    }
    public string innerIssueSubType{get;set;} //sometimes the setter method on theCase.Issue_Sub_Type__c wasn't being called
    public pageReference getL3s(){
        if(innerIssueSubType != null) theCase.Issue_Sub_Type__c = String.escapeSingleQuotes(innerIssueSubType);
        GrayOut = false;
        setupL3();
        this.theCase.Issue_Type__c = this.theCase.Issue_Type__c == null ? '' : this.theCase.Issue_Type__c;
        this.theCase.Issue_Sub_Type__c = this.theCase.Issue_Sub_Type__c== null ? '' : this.theCase.Issue_Sub_Type__c;
        String dispositionQuery = 'SELECT L3__c FROM Disposition__c WHERE L1__c = \''+String.escapeSingleQuotes(theCase.Issue_Type__c);
        dispositionQuery += '\' AND L2__c = \''+String.escapeSingleQuotes(theCase.Issue_Sub_Type__c)+'\' AND ';
        if(this.theCase.Product_L1__c != null && (this.theCase.Product_L1__c.containsIgnoreCase('elite') || this.theCase.Product_L1__c.containsIgnoreCase('app')))
            dispositionQuery += 'ELITE__c';
        else
            dispositionQuery += 'GAME__c';
        dispositionQuery += ' = TRUE AND IsActive__c = TRUE AND L3__c != \'\' ORDER BY L3__c';
        system.debug('dispQuery == '+dispositionQuery);
        for(Disposition__c l3 : database.query(dispositionQuery )) l3Options.add(new selectOption(l3.L3__c, l3.L3__c));
        
        l3Options.add(new selectOption('Needs L3','Needs L3'));
        ppReq = new set<String>{'Toy','Portal','Disc','Peripheral','Special Edition 3rd Party'}.contains(theCase.Issue_Sub_Type__c);
        return null;
    }
    public pageReference save(){
        try{
            if(this.theCase.Product_L1__c=='Other') this.theCase.Product_L1__c = this.theCase.Product_L2__c;
            this.theCase.Type = theCase.Issue_Type__c;
            for(String reqField : new string[]{'Platform__c','L3__c'}){ //always required fields
                if(isBlankOrNull((String) this.theCase.get(reqField))){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Must specify '+reqField.replaceAll('__c','')));
                    return null;
                }
            }
            if(this.theCase.Id == null){//only on insert required fields
                string[] reqFields = new string[]{'Subject'};
                if(pIsRMA){
                    reqFields.add('Problem_Description__c');
                    reqFields.add('Associated_General_Support_Case__c');
                }else{reqFields.add('Description');}
                for(String reqField : reqFields){
                    if(isBlankOrNull((String) this.theCase.get(reqField))){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Must specify '+reqField.replaceAll('__c','')));
                        return null;
                    }
                }
            }
            
            if(this.theCase.Product_Effected__c == null && pIsRma == false){
                String platform = theCase.Platform__c;
                if(casePlat2ppPlat.containsKey(theCase.Platform__c)){
                    platform = casePlat2ppPlat.get(theCase.Platform__c);
                }
                for(Public_Product__c prod : [SELECT Id FROM Public_Product__c WHERE Platform__c INCLUDES (:platform) AND Medallia_Product_2__c = :theCase.Product_L1__c LIMIT 1]){
                    this.theCase.Product_Effected__c = prod.Id;
                }
            }
            if(ppReq || pIsRMA){
                if(this.theCase.Product_Effected__c == null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Must specify Product Affected'));
                    return null;
                }
            }
            if(pIsRMA){
                for(Public_Product__c prod : [SELECT Name, RMA_Available__c FROM Public_Product__c WHERE Id = :this.theCase.Product_Effected__c]){
                    if(prod.RMA_Available__c != true){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, prod.Name+' is not available for RMA'));
                        return null;
                    }
                }
            }
            //need to find an upsert method instead of insert/update
            if(this.theCase.id == null){
                if(
                    isBlankOrNull(this.theCaseComment.CommentBody)
                    &&
                    pIsRMA == false
                ){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Internal comment is required.'));
                    return null;
                }
                if(selectedGamerId != null) this.theCase.ContactId = selectedGamerId; //use the search results
                this.theCase.Comment_Not_Required__c = true;
                insert this.theCase;
                this.theCaseComment.ParentId = theCase.Id;
                if(! pIsRMA){//Comment for RMAs are on the 2nd page
                    insert this.theCaseComment;
                    this.theCase.Comment_Not_Required__c = false;
                    update this.theCase;
                }
                PageReference pageRef = new PageReference('/'+this.theCase.id+'/e?retURL='+this.theCase.Id);
                return pageRef;
            }else{
                update this.theCase;
            }
        }catch(exception e){
            system.debug('ERROR == '+e);
            ApexPages.addMessages(e);
        }
        return null;
    }
    
    //Search support methods
    public Id selectedGamerId {get; set;}
    public void doNothing(){}
    public selectOption[] getGamerSearchFields(){
        return new selectOption[]{
            new selectOption('Email','Email'),
            new selectOption('Name','Full Name'),
            new selectOption('Phone','Phone')
        };
    }
    public String selectedSearchField {get; set;}
    public String searchFieldValue {get; set;}
    public Contact[] foundGamers {get; set;}
    public void searchGamerRecords(){
        selectedSearchField = String.escapeSingleQuotes(selectedSearchField);
        searchFieldValue = String.escapeSingleQuotes(searchFieldValue);
        String gamerQuery = 'SELECT Id, Name, Email, (SELECT Id, CaseNumber, Subject, Status, CreatedDate, ClosedDate FROM ';
        gamerQuery += 'Cases ORDER BY CaseNumber DESC) FROM Contact WHERE '+selectedSearchField+' = \''+searchFieldValue;
        gamerQuery += '\' ORDER BY Name LIMIT 1000';
        foundGamers = database.query(gamerQuery);
    }
    map<String, String> casePlat2ppPlat = new map<String,String>{
        'Playstation 3'=>'PS3',
        'Playstation 4'=>'PS4',
        'Playstation 2'=>'PS2',
        'Nintendo DS'=>'NDS',
        'Nintendo 3DS'=>'3DS',
        'Nintendo Wii'=>'Wii',
        'Nintendo Wii U'=>'Wii U',
        'PC'=>'PC/MAC',
        'Mac'=>'PC/MAC'
    };
}