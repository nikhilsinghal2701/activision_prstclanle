@isTest
public class DispositionPicklistController_test{

    public static void createTestData(){
        insert new sObject[]{
            new Contact(
                FirstName='TestFirstName',
                LastName='TestContact',
                Email='test@email.com',
                Birthdate=system.today().addDays(-7000),
                UCDID__c='1234567890'),
    
            new Case(Subject='elite',Comment_not_Required__c=true,Issue_Type__c='e1a',Issue_Sub_Type__c='e2a',L3__c='e3a',Product_L1__c='elite'),
            new Case(Subject='game',Comment_not_Required__c=true,Issue_Type__c='g1a',Issue_Sub_Type__c='g2a',L3__c='g3a',Product_L1__c='game'),
    
            new Disposition__c(L1__c = 'e1a',L2__c = 'e2a',L3__c = 'e3a',Game__c = false,Elite__c = true),
            new Disposition__c(L1__c = 'e1a',L2__c = 'e2a',L3__c = 'e3b',Game__c = false,Elite__c = true),
            new Disposition__c(L1__c = 'e1a',L2__c = 'e2b',L3__c = 'e3c',Game__c = false,Elite__c = true),
            new Disposition__c(L1__c = 'e1a',L2__c = 'e2b',L3__c = 'e3d',Game__c = false,Elite__c = true),
            new Disposition__c(L1__c = 'e2a',L2__c = 'e2c',L3__c = 'e3e',Game__c = false,Elite__c = true),
            new Disposition__c(L1__c = 'e2a',L2__c = 'e2c',L3__c = 'e3f',Game__c = false,Elite__c = true),
            new Disposition__c(L1__c = 'e2a',L2__c = 'e2d',L3__c = 'e3g',Game__c = false,Elite__c = true),
            new Disposition__c(L1__c = 'e2a',L2__c = 'e2d',L3__c = 'e3h',Game__c = false,Elite__c = true),
            
            new Disposition__c(L1__c = 'g1a',L2__c = 'g2a',L3__c = 'g3a',Game__c = true,Elite__c = false),
            new Disposition__c(L1__c = 'g1a',L2__c = 'g2a',L3__c = 'g3b',Game__c = true,Elite__c = false),
            new Disposition__c(L1__c = 'g1a',L2__c = 'g2b',L3__c = 'g3c',Game__c = true,Elite__c = false),
            new Disposition__c(L1__c = 'g1a',L2__c = 'g2b',L3__c = 'g3d',Game__c = true,Elite__c = false),
            new Disposition__c(L1__c = 'g2a',L2__c = 'g2c',L3__c = 'g3e',Game__c = true,Elite__c = false),
            new Disposition__c(L1__c = 'g2a',L2__c = 'g2c',L3__c = 'g3f',Game__c = true,Elite__c = false),
            new Disposition__c(L1__c = 'g2a',L2__c = 'g2d',L3__c = 'g3g',Game__c = true,Elite__c = false),
            new Disposition__c(L1__c = 'g2a',L2__c = 'g2d',L3__c = 'g3h',Game__c = true,Elite__c = false),
            
            new Disposition__c(L1__c = 'b1a',L2__c = 'b2a',L3__c = 'b3a',Game__c = true,Elite__c = true),
            new Disposition__c(L1__c = 'b1a',L2__c = 'b2a',L3__c = 'b3b',Game__c = true,Elite__c = true),
            new Disposition__c(L1__c = 'b1a',L2__c = 'b2b',L3__c = 'b3c',Game__c = true,Elite__c = true),
            new Disposition__c(L1__c = 'b1a',L2__c = 'b2b',L3__c = 'b3d',Game__c = true,Elite__c = true),
            new Disposition__c(L1__c = 'b2a',L2__c = 'b2c',L3__c = 'b3e',Game__c = true,Elite__c = true),
            new Disposition__c(L1__c = 'b2a',L2__c = 'b2c',L3__c = 'b3f',Game__c = true,Elite__c = true),
            new Disposition__c(L1__c = 'b2a',L2__c = 'b2d',L3__c = 'b3g',Game__c = true,Elite__c = true),
            new Disposition__c(L1__c = 'b2a',L2__c = 'b2d',L3__c = 'b3h',Game__c = true,Elite__c = true)

        };
    }
    
    public static testMethod void test1(){
        String caseQuery = 'SELECT Id, Issue_Type__c, Issue_Sub_Type__c, Product_L1__c FROM Case WHERE Subject = \'';
        createTestData();
        Contact c = [SELECT Id FROM Contact LIMIT 1];
        Test.setCurrentPage(Page.DispositionPicklists);
        ApexPages.currentPage().getParameters().put('def_contact_id','asdf');//bad id to trip the try/catch block
        DispositionPicklistController tstCls = new DispositionPicklistController(
            new ApexPages.standardController(new Case())
        );
        
        //test public get/set methods
        tstCls.selectedGamerId = null;
        tstCls.doNothing();
        tstCls.getGamerSearchFields();
        tstCls.selectedSearchField = 'Email';
        tstCls.searchFieldValue = 'test@email.com';
        tstCls.foundGamers = null;
        tstCls.theCase = null;
        tstCls.l3Options = null;
        tstCls.GrayOut = false;
        tstCls.ppReq = false;
        tstCls.theCaseComment = new CaseComment();
        
        ApexPages.currentPage().getParameters().put('def_contact_id',c.Id);//good id to trip the try/catch block
        tstCls = new DispositionPicklistController(
            new ApexPages.standardController(new Case())
        );
        
        tstCls = new DispositionPicklistController(
            new ApexPages.standardController(database.query(caseQuery+'elite\' LIMIT 1'))
        );
        Case bla = database.query(caseQuery+'game\' LIMIT 1');
        bla.ContactId = [SELECT id FROM Contact LIMIT 1].Id;
        update bla;
        tstCls = new DispositionPicklistController(
            new ApexPages.standardController(bla)
        );
        
        tstCls.getIsRMA();
        
        ApexPages.currentPage().getParameters().put('CF00NJ0000000W6iX_lkid',bla.Id);
        
        tstCls = new DispositionPicklistController(
            new ApexPages.standardController(new Case(
                RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('RMA').getRecordTypeId()
            ))
        );
        
        try{
            //test save
            tstCls.save();
            
            //test search
            tstCls.searchGamerRecords();
        }catch(Exception e){
        }
        
    }

}