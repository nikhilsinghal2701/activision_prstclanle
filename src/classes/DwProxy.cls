/*
Do not "refresh" from WSDL by deleting and reimporting a WSDL file, this class has been "optimized" for re-use 
*/
public class DwProxy {
    public class DwGetProxyResponse_element {
        public String DwGetProxyResult;
        private String[] DwGetProxyResult_type_info = new String[]{'DwGetProxyResult','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'DwGetProxyResult'};
    }

    public class service {
        //public String endpoint_x = 'http://64.111.160.91:4815/Service.svc';
        public String endpoint_x = 'https://sfd0.activision.com/Service.svc';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        public String serviceSessionId; //another deviation
        private String[] ns_map_type_info = new String[]{'http://tempuri.org/', 'DwProxy'};
        public String DwGetProxy(String dwGetQuery) {
            DwProxy.DwGetProxy_element request_x = new DwProxy.DwGetProxy_element();
            DwProxy.DwGetProxyResponse_element response_x;
            request_x.SFDCInstance = getSFDCinstance();
            request_x.SFDCOrgId = UserInfo.getOrganizationId();
            request_x.SFDCUserId = UserInfo.getUserId();
            request_x.SFDCSessionId = UserInfo.getSessionId();
            request_x.dwGetQuery = dwGetQuery;
            Map<String, DwProxy.DwGetProxyResponse_element> response_map_x = new Map<String, DwProxy.DwGetProxyResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://tempuri.org/ILinkedAccounts/DwGetProxy',
              'http://tempuri.org/',
              'DwGetProxy',
              'http://tempuri.org/',
              'DwGetProxyResponse',
              'DwProxy.DwGetProxyResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.DwGetProxyResult;
        }
        public String DwGetProxy(String dwGetQuery, String sessionId) {//this one is used for scheduled jobs
            DwProxy.DwGetProxy_element request_x = new DwProxy.DwGetProxy_element();
            DwProxy.DwGetProxyResponse_element response_x;
            request_x.SFDCInstance = getSFDCinstance();
            request_x.SFDCOrgId = UserInfo.getOrganizationId();
            request_x.SFDCUserId = UserInfo.getUserId();
            request_x.SFDCSessionId = sessionId;
            request_x.dwGetQuery = dwGetQuery;
            Map<String, DwProxy.DwGetProxyResponse_element> response_map_x = new Map<String, DwProxy.DwGetProxyResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://tempuri.org/ILinkedAccounts/DwGetProxy',
              'http://tempuri.org/',
              'DwGetProxy',
              'http://tempuri.org/',
              'DwGetProxyResponse',
              'DwProxy.DwGetProxyResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.DwGetProxyResult;
        }
        public String passEncryptedGetData(String getQuery){
            return passEncryptedData(
                'GET', getSFDCinstance(), UserInfo.getOrganizationId(), UserInfo.getUserId(), UserInfo.getSessionId(),
                getQuery,'','',''
            );
        }
        public String passEncryptedGetData(String getQuery, String sessionId){//for scheduled jobs
            return passEncryptedData(
                'GET', getSFDCinstance(), UserInfo.getOrganizationId(), UserInfo.getUserId(), sessionId,getQuery,'','',''
            );
        }
        public String passEncryptedPutData(String putURL,String putBody, String contentType){
            return passEncryptedData('PUT',getSFDCinstance(),UserInfo.getOrganizationId(),UserInfo.getUserId(),
                UserInfo.getSessionId(),'',putURL,putBody,contentType
            );
        }
        public String passEncryptedPutData(String putURL,String putBody, String sessionId, String contentType){//for scheduled jobs
            return passEncryptedData('PUT',getSFDCinstance(),UserInfo.getOrganizationId(),UserInfo.getUserId(),
                sessionId,'',putURL,putBody,contentType
            );
        }
        public String passEncryptedPostData(String postURL, String postBody, String contentType){
            return passEncryptedData('POST',getSFDCinstance(),UserInfo.getOrganizationId(),UserInfo.getUserId(),
                UserInfo.getSessionId(),'',postURL,postBody,contentType
            );
        }
        public String passEncryptedPostData(String postURL, String postBody, String contentType, String sessionId){ //for scheduled jobs
            return passEncryptedData('POST',getSFDCinstance(),UserInfo.getOrganizationId(),UserInfo.getUserId(),
                sessionId,'',postURL,postBody,contentType
            );
        }
        public String passEncryptedDeleteData(String delUrl){
            return passEncryptedData(
                'DELETE', getSFDCinstance(), UserInfo.getOrganizationId(), UserInfo.getUserId(), UserInfo.getSessionId(),
                delUrl,'','',''
            );
        }
        private String passEncryptedData(String method, String instance, String orgId, String userId, 
            String sessionId, String getQuery, String putURL, String putBody, String contentType
        ){
            string psk = 'IIEvgIBADANBkqhkiG9w0BAQEFAASCMJ';

            String inputString = 'method::'+method+'|instance::'+instance+'|orgId::'+orgId+'|userId::'+userId+'|sessionId::';
            inputString += sessionId+'|getQuery::'+getQuery+'|putUrl::'+putUrl+'|putBody::'+putBody+'|contentType::'+contentType;
            system.debug('inputString == '+inputString);
            String encryptedData = EncodingUtil.base64encode(
                Crypto.encryptWithManagedIV(
                    'AES256',
                    Blob.valueOf(psk), 
                    Blob.valueOf(inputString)
                )
            );
            system.debug('encrypted string == '+encryptedData);
            //do callout
            DwProxy.DwEncrypted_element request_x = new DwProxy.DwEncrypted_element();
            DwProxy.DwEncryptedResponse_element response_x;
            request_x.base64EncodedEncryptedBlob = encryptedData;
            Map<String, DwProxy.DwEncryptedResponse_element> response_map_x = new Map<String, DwProxy.DwEncryptedResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://tempuri.org/ILinkedAccounts/DwEncrypted',
              'http://tempuri.org/',
              'DwEncrypted',
              'http://tempuri.org/',
              'DwEncryptedResponse',
              'DwProxy.DwEncryptedResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            system.debug('response == '+response_x.DwEncryptedResult);
            return response_x.DwEncryptedResult;
        }
        public String DwPutProxy(String dwPutUrl,String dwPutBody) {
            DwProxy.DwPutProxy_element request_x = new DwProxy.DwPutProxy_element();
            DwProxy.DwPutProxyResponse_element response_x;
            request_x.SFDCInstance = getSFDCinstance();
            request_x.SFDCOrgId = UserInfo.getOrganizationId();
            request_x.SFDCUserId = UserInfo.getUserId();
            request_x.SFDCSessionId = serviceSessionId == null ? UserInfo.getSessionId() : serviceSessionId;
            request_x.dwPutUrl = dwPutUrl;
            request_x.dwPutBody = dwPutBody;
            Map<String, DwProxy.DwPutProxyResponse_element> response_map_x = new Map<String, DwProxy.DwPutProxyResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://tempuri.org/ILinkedAccounts/DwPutProxy',
              'http://tempuri.org/',
              'DwPutProxy',
              'http://tempuri.org/',
              'DwPutProxyResponse',
              'DwProxy.DwPutProxyResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.DwPutProxyResult;
        }
        private String getSFDCinstance(){
            String result = '';
            if(UserInfo.getOrganizationId().contains('00DU0000000HMgw')){
                result = 'na12';
            }else{
                String host = System.URL.getSalesforceBaseUrl().getHost();                
                if(host.contains('.force.')){//c.cs11.visual.force.com
                    result = host.split('\\.')[1];
                }else{//cs11.salesforce.com
                    result = host.split('\\.')[0];
                }
            }
            return result;
        }
         public void codeRepoProcessFile(Boolean isProd,String fileId) {
            DwProxy.codeRepoProcessFile_element request_x = new DwProxy.codeRepoProcessFile_element();
            request_x.isProd = isProd;
            request_x.fileId = fileId;
            DwProxy.codeRepoProcessFileResponse_element response_x;
            Map<String, DwProxy.codeRepoProcessFileResponse_element> response_map_x = new Map<String, DwProxy.codeRepoProcessFileResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://tempuri.org/ILinkedAccounts/codeRepoProcessFile',
              'http://tempuri.org/',
              'codeRepoProcessFile',
              'http://tempuri.org/',
              'codeRepoProcessFileResponse',
              'DwProxy.codeRepoProcessFileResponse_element'}
            );
            response_x = response_map_x.get('response_x');
        }
        public void codeRepoUpload(Boolean isProd,String jobId) {
            DwProxy.codeRepoUpload_element request_x = new DwProxy.codeRepoUpload_element();
            request_x.isProd = isProd;
            request_x.jobId = jobId;
            DwProxy.codeRepoUploadResponse_element response_x;
            Map<String, DwProxy.codeRepoUploadResponse_element> response_map_x = new Map<String, DwProxy.codeRepoUploadResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://tempuri.org/ILinkedAccounts/codeRepoUpload',
              'http://tempuri.org/',
              'codeRepoUpload',
              'http://tempuri.org/',
              'codeRepoUploadResponse',
              'DwProxy.codeRepoUploadResponse_element'}
            );
            response_x = response_map_x.get('response_x');
        }
        public string codeRepoPrepDownload(Boolean isProd,String distRequestId) {
            DwProxy.codeRepoPrepDownload_element request_x = new DwProxy.codeRepoPrepDownload_element();
            request_x.isProd = isProd;
            request_x.distRequestId = distRequestId;
            DwProxy.codeRepoPrepDownloadResponse_element response_x;
            Map<String, DwProxy.codeRepoPrepDownloadResponse_element> response_map_x = new Map<String, DwProxy.codeRepoPrepDownloadResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://tempuri.org/ILinkedAccounts/codeRepoPrepDownload',
              'http://tempuri.org/',
              'codeRepoPrepDownload',
              'http://tempuri.org/',
              'codeRepoPrepDownloadResponse',
              'DwProxy.codeRepoPrepDownloadResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.codeRepoPrepDownloadResult;
        }
    
        public String generateAWSessionId(String unoId) {
            DwProxy.generateAWsessionId_element request_x = new DwProxy.generateAWsessionId_element();
            request_x.unoId = unoId;
            DwProxy.generateAWsessionIdResponse_element response_x;
            Map<String, DwProxy.generateAWsessionIdResponse_element> response_map_x = new Map<String, DwProxy.generateAWsessionIdResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://tempuri.org/ILinkedAccounts/generateAWsessionId',
              'http://tempuri.org/',
              'generateAWsessionId',
              'http://tempuri.org/',
              'generateAWsessionIdResponse',
              'DwProxy.generateAWsessionIdResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.generateAWsessionIdResult;
        }
    }
    public class DwGetProxy_element {
        public String SFDCInstance;
        public String SFDCOrgId;
        public String SFDCUserId;
        public String SFDCSessionId;
        public String dwGetQuery;
        private String[] SFDCInstance_type_info = new String[]{'SFDCInstance','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] SFDCOrgId_type_info = new String[]{'SFDCOrgId','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] SFDCUserId_type_info = new String[]{'SFDCUserId','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] SFDCSessionId_type_info = new String[]{'SFDCSessionId','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] dwGetQuery_type_info = new String[]{'dwGetQuery','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'SFDCInstance','SFDCOrgId','SFDCUserId','SFDCSessionId','dwGetQuery'};
    }
    public class DwEncrypted_element {
        public string base64EncodedEncryptedBlob;
        private String[] base64EncodedEncryptedBlob_type_info = new String[]{'base64EncodedEncryptedBlob','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'base64EncodedEncryptedBlob'};
    }
    public class DwEncryptedResponse_element {
        public String DwEncryptedResult;
        private String[] DwEncryptedResult_type_info = new String[]{'DwEncryptedResult','http://tempuri.org/',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'DwEncryptedResult'};
    }
    public class DwPutProxy_element {
        public String SFDCInstance;
        public String SFDCOrgId;
        public String SFDCUserId;
        public String SFDCSessionId;
        public String dwPutUrl;
        public String dwPutBody;
        private String[] SFDCInstance_type_info = new String[]{'SFDCInstance','http://tempuri.org/',null,'0','1','true'};
        private String[] SFDCOrgId_type_info = new String[]{'SFDCOrgId','http://tempuri.org/',null,'0','1','true'};
        private String[] SFDCUserId_type_info = new String[]{'SFDCUserId','http://tempuri.org/',null,'0','1','true'};
        private String[] SFDCSessionId_type_info = new String[]{'SFDCSessionId','http://tempuri.org/',null,'0','1','true'};
        private String[] dwPutUrl_type_info = new String[]{'dwPutUrl','http://tempuri.org/',null,'0','1','true'};
        private String[] dwPutBody_type_info = new String[]{'dwPutBody','http://tempuri.org/',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'SFDCInstance','SFDCOrgId','SFDCUserId','SFDCSessionId','dwPutUrl','dwPutBody'};
    }
    public class DwPutProxyResponse_element {
        public String DwPutProxyResult;
        private String[] DwPutProxyResult_type_info = new String[]{'DwPutProxyResult','http://tempuri.org/',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'DwPutProxyResult'};
    }
    
    
    //code repository
    public class codeRepoProcessFile_element {
        public Boolean isProd;
        public String fileId;
        private String[] isProd_type_info = new String[]{'isProd','http://tempuri.org/',null,'0','1','false'};
        private String[] fileId_type_info = new String[]{'fileId','http://tempuri.org/',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'isProd','fileId'};
    }
    public class codeRepoProcessFileResponse_element {
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class codeRepoPrepDownload_element {
        public Boolean isProd;
        public String distRequestId;
        private String[] isProd_type_info = new String[]{'isProd','http://tempuri.org/',null,'0','1','false'};
        private String[] distRequestId_type_info = new String[]{'distRequestId','http://tempuri.org/',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'isProd','distRequestId'};
    }
    public class codeRepoPrepDownloadResponse_element {
        public String codeRepoPrepDownloadResult;
        private String[] codeRepoPrepDownloadResult_type_info = new String[]{'codeRepoPrepDownloadResult','http://tempuri.org/',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'codeRepoPrepDownloadResult'};
    }
    public class codeRepoUpload_element {
        public Boolean isProd;
        public String jobId;
        private String[] isProd_type_info = new String[]{'isProd','http://tempuri.org/',null,'0','1','false'};
        private String[] jobId_type_info = new String[]{'jobId','http://tempuri.org/',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'isProd','jobId'};
    }
    public class codeRepoUploadResponse_element {
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{};
    }
    //AW clan
    public class generateAWsessionId_element {
        public String unoId;
        //private String[] unoId_type_info = new String[]{'unoId','http://tempuri.org/',null,'0','1','true'};
        private String[] unoId_type_info = new String[]{'unoId','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'unoId'};
    }
    public class generateAWsessionIdResponse_element {
        public String generateAWsessionIdResult;
        private String[] generateAWsessionIdResult_type_info = new String[]{'generateAWsessionIdResult','http://tempuri.org/',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'generateAWsessionIdResult'};
    }
    
    
}