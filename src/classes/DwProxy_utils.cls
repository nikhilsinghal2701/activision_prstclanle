public class DwProxy_utils{

    public boolean isProd = UserInfo.getOrganizationId() != '00DU0000000HMgwMAG' ? false : true;
    public string env = isProd ? 'prod' : 'dev';
    private string key = isProd ? 'zFzHG7aE+GkfCoPSnwngBw==' : '74y+LsSyMKLOHlkaS8xiIw==';
    private string app = '94215';
    public class dwException extends Exception {}
    
    public string signRequest(String method, String host, String path, String query){
        //String method='GET',host='dev.ucd.demonware.net',path='/ucd/service/promotions/codes/',query='';
        if(isNullOrBlank(method) || isNullOrBlank(host) || isNullOrBlank(path)){
            throw new dwException('Neither Method, Host nor Path parameters can be null or blank');
        }
        if(query == null) query = ''; //change query from null to blank
        String timestamp = generateTimestamp(system.now());
        
        //format data
        String unEncoded = generateDataToSign(key, app, method, host, path, query, timestamp);
        
        //sign
        String signature = generateHmacSHA256Signature(unEncoded, key);
        
        //use
        String endpoint = 'https://'+host+path+'?app='+app+'&signature='+signature+'&timestamp='+timestamp;
        System.debug('Endpoint : '+endpoint);
        return endPoint;
    }
    
    String generateTimeStamp(DateTime dt){
        return dt.yearGMT()+'-'+formatNum(dt.monthGMT())+'-'+formatNum(dt.dayGMT())+'T'+formatNum(dt.hourGMT())+':'+formatNum(dt.minuteGMT())+':'+formatNum(dt.secondGMT())+'Z';
    }
    public String formatNum(Integer i){
        if(i < 10) return '0'+string.valueOf(i);
        return string.valueOf(i);
    }
    String generateDataToSign(String pKey, String pApp, String pMethod, String pHost, String pPath, String pQuery, String pTimestamp){
        return pMethod.toUpperCase()+'\n'+pHost+'\n'+pPath+'\n'+pQuery+'\n'+pTimestamp;
    }
    String generateHmacSHA256Signature(String dataValue, String keyValue ) {
        String algorithmName = 'HmacSHA256';
        Blob hmacData = Crypto.generateMac(algorithmName, Blob.valueOf(dataValue), EncodingUtil.base64decode(keyValue));
        return urlSafeBase64(EncodingUtil.base64Encode(hmacData));
    }
    String urlSafeBase64(String s){
        return s.replaceAll('\\+','-').replaceAll('/','_');
    }
    Boolean isNullOrBlank(String s){
        return s == '' || s == null;
    }
    
    public String getSessionId(String appName){
        for(Integration_Credential__c cred : [SELECT Id, Authorization_Type__c, Username__c, Password__c, Access_Token__c, 
                                              Refresh_Token__c FROM Integration_Credential__c WHERE Application_Name__c = :appName])
        {
            if(cred.Authorization_Type__c == 'oAuth 2.0'){
            }else if(cred.Authorization_Type__c == 'SAML 2.0'){
            }else if(cred.Authorization_Type__c == 'UN / PW'){
                return getSessionFromUnPw(cred.Username__c, cred.Password__c);
            }
        }
        return null;
    }
    string SOAP_START = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    public String getSessionFromUnPw(String UN, String PW){
        string loginURL = isProd ? 'https://login' : 'https://test';
        loginUrl += '.salesforce.com/services/Soap/u/28.0';
 
        //This beginning piece of XML can be re-used for the subsequent calls         
        string bodyToSendLogin = SOAP_START + '<soap:Body><login xmlns="urn:partner.soap.sforce.com"><username>'+UN+
                    '</username><password>' + PW + '</password></login></soap:Body></soap:Envelope>';
        system.debug('bodyToSendLogin == '+bodyToSendLogin);
        string outCallResultLogin = makeHTTPCall(loginURL,bodyToSendLogin);
        system.debug('outCallResultLogin Output: ' + outCallResultLogin);
        return getValueFromXMLString(outCallResultLogin, 'sessionId');
    }
    
    private string makeHTTPCall(string endPoint, string soapBody){
        Http hLLogin = new Http();
        HttpRequest reqLLogin = new HttpRequest();
        reqLLogin.setTimeout(120000);
        reqLLogin.setEndpoint(endPoint);
        reqLLogin.setMethod('POST');
        reqLLogin.setHeader('SFDC_STACK_DEPTH', '1');
        reqLLogin.setHeader('SOAPAction','DoesNotMatter');
        //reqLLogin.setHeader('User-Agent', 'SFDC-Callout/22.0');<br>
        reqLLogin.setHeader('Accept','text/xml');
        reqLLogin.setHeader('Content-type','text/xml');
        reqLLogin.setHeader('charset','UTF-8');
        system.debug('Request: ' + reqLLogin);
        reqLLogin.setBody(soapBody);
        HttpResponse resLLogin = hLLogin.send(reqLLogin);
        return resLLogin.getBody();
    }
 
    private string getValueFromXMLString(string xmlString, string keyField){
        string valueFound = '';
        if(xmlString.contains('<' + keyField + '>') && xmlString.contains('</' + keyField + '>')){
            try{
                valueFound = xmlString.substring(xmlString.indexOf('<' + keyField + '>') + keyField.length() + 2, xmlString.indexOf('</' + keyField + '>'));
            }catch (exception e){
                system.debug('Error in getValueFromXMLString.  Details: ' + e.getMessage() + ' keyfield: ' + keyfield);
            }
        }
        return valueFound;
    }
}