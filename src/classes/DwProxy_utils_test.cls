@isTest
public class DwProxy_utils_test{
    public static testMethod void test1(){
        DWProxy_Utils tstCls = new DWProxy_Utils();
        if(tstCls.isProd){}
        String s=tstCls.env;
        Boolean errorThrown = false;
        try{
            tstCls.signRequest(null, null, null, null);
        }catch(Exception e){
            errorThrown = true;
        }
        system.assert(errorThrown);
        tstCls.signRequest('gibberish','gibberish','gibberish',null);
        system.assertEquals(tstCls.formatNum(9), '09');
        system.assertEquals(tstCls.formatNum(19), '19');
    }
}