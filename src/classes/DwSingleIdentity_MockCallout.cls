@isTest
public class DwSingleIdentity_MockCallout implements WebServiceMock {

    public void doInvoke(
       Object stub,
       Object request,
       Map<String, Object> response,
       String endpoint,
       String soapAction,
       String requestName,
       String responseNS,
       String responseName,
       String responseType
    ){//get the request body out then decrypt
        String toDecrypt = String.valueOf(request).split('base64EncodedEncryptedBlob=')[1].split(', base64EncodedEncryptedBlob_type_info=')[0];
        system.debug('toDecrypt == '+toDecrypt);
        String psk = 'IIEvgIBADANBkqhkiG9w0BAQEFAASCMJ';
        Blob encryptedData = EncodingUtil.base64decode(toDecrypt);
        Blob resultBlob = Crypto.decryptWithManagedIV('AES256', Blob.valueOf(psk), encryptedData);
        String requestInStringFormat = resultBlob.toString();
        String customResponse = '';
        if(requestInStringFormat.containsIgnoreCase('demonware.net/v1.0/tokens')){
            //requesting a DW token
            customResponse = '{"expires": 1411497196, "accessToken": "faketoken"}';
        }else if(requestInStringFormat.containsIgnoreCase('umbrella.demonware.net/v1.0/users')){
            //requesting linked accounts
            customResponse = '{"umbrellaID": 2702782069577670754, "accounts": [';
            customResponse += '{"username": "christopher.bruens", "accountID": ';
            customResponse += '100000908855631, "authorized": true, "provider": ';
            customResponse += '"facebook"}, {"username": "Bruens", "accountID": ';
            customResponse += '76561197972579797, "authorized": true, "provider": ';
            customResponse += '"steam"}, {"username": "vicarious_83", "accountID": ';
            customResponse += '71428788, "authorized": true, "provider": "twitch"}, ';
            customResponse += '{"username": "TechBru", "accountID": 362093881, ';
            customResponse += '"authorized": true, "provider": "twitter"}, {"username":';
            customResponse += ' "cbruensOG", "accountID": 17144909217217914082, "authorized"';
            customResponse += ': true, "provider": "uno"}]}';
        }
        
        DwProxy.DwEncryptedResponse_element responseElement = new DwProxy.DwEncryptedResponse_element();
        responseElement.DwEncryptedResult = customResponse;
        response.put('response_x', responseElement);
   }
}