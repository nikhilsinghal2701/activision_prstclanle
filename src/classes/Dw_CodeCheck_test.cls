@isTest
public class Dw_CodeCheck_test{
    public static testMethod void createData(){
        Account a = new Account(name = 'test');
        insert a;
        Contact c = new Contact(
            FirstName = 'test',
            LastName = 'test', 
            AccountId = a.Id, 
            BirthDate = system.today().addDays(-7300), //20 years old
            email = 'test@test.test', 
            UcdId__c = '123'
        );
        insert c;
        Case cas = new Case(Comment_Not_required__c = true, ContactId = c.Id);
        insert cas;
    }
    private static testMethod void Dw_CodeCheck_1(){
        createData();
        Contact c = [SELECT Id, Email, UcdId__c FROM Contact LIMIT 1];
        Case cas = [SELECT Id FROM Case LIMIT 1];
        //test constructors
        //Constructor 1 - no param
        Dw_CodeCheck tstCls = new Dw_CodeCheck();
        
        //Constructor 2 - param
        Test.setCurrentPage(Page.DW_CodeCheck);
        ApexPages.currentPage().getParameters().put('id',c.Id);
        tstCls = new DW_CodeCheck();
        
        //Constructor 3 - Standard controller with no id
        tstCls = new Dw_CodeCheck(new ApexPages.standardController(new Contact()));
        
        //Constructor 4 - Standard controller w/ Contact Id
        tstCls = new Dw_CodeCheck(new ApexPages.standardController(c));
        
        //Constructor 5 - Standard controller w/ Case Id (with Contact)
        tstCls = new Dw_CodeCheck(new ApexPages.standardController(cas));

        //Constructor 6 - Standard controller w/ Case Id (w/o Contact)
        cas.ContactId = null;
        update cas;
        tstCls = new Dw_CodeCheck(new ApexPages.standardController(cas));
        
        tstCls.doQuery(); //trip the must specify code & promotion error
        
        tstCls.searchGamers(); //trip the "must provide email address" warning
        
        tstCls.Gamer.Email = c.Email;
        
        tstCls.searchGamers();
        
        tstCls.UcdId = c.UcdId__c;
        
        tstCls.chooseGamer();
        
        tstCls.PromotionId = 'test';
        tstCls.Code = 'test';
        
        Test.setMock(WebServiceMock.class, new DwProxy_MockResponse_DwGet());
        Test.startTest();
            tstCls.doQuery();
        Test.stopTest();

        
    }

}