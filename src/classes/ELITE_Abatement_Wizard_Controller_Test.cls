@isTest
public class ELITE_Abatement_Wizard_Controller_Test {
	
	public static testMethod void startTest()
	{
		//Create Contact:
		Contact testContact = new Contact();
		testContact.FirstName = 'testFirstName';
		testContact.LastName = 'testLastName';
		testContact.Email = 'testEmail@testEmail.com';
		testContact.Birthdate = date.newinstance(1960, 2, 17);
		testContact.UCDID__c = 'abc';
		insert testContact;
		
		//Create 4 MP Accounts for each platform and Subscription Date. 
		Multiplayer_Account__c testMPAccount_30_X = new Multiplayer_Account__c();
		testMPAccount_30_X.Contact__c = testContact.id;
		testMPAccount_30_X.Platform__c = 'XBL';
		testMPAccount_30_X.Gamertag__c = 'testMPAccount_30_X';
		testMPAccount_30_X.DWID__c = 'xyx';
		testMPAccount_30_X.Is_Current__c = True;
		testMPAccount_30_X.Is_Premium__c = true;
		testMPAccount_30_X.Premium_Start__c = date.newInstance(2012, 10, 10); //30 Day
		insert testMPAccount_30_X;
		
		Multiplayer_Account__c testMPAccount_30_P = new Multiplayer_Account__c();
		testMPAccount_30_P.Contact__c = testContact.id;
		testMPAccount_30_P.Platform__c = 'PSN';
		testMPAccount_30_P.Gamertag__c = 'testMPAccount_30_P';
		testMPAccount_30_P.DWID__c = 'asdf';
		testMPAccount_30_P.Is_Current__c = True;
		testMPAccount_30_P.Is_Premium__c = true;
		testMPAccount_30_P.Premium_Start__c = date.newInstance(2012, 10, 10); //30 Day
		insert testMPAccount_30_P;
		
		Multiplayer_Account__c testMPAccount_X = new Multiplayer_Account__c();
		testMPAccount_X.Contact__c = testContact.id;
		testMPAccount_X.Platform__c = 'XBL';
		testMPAccount_X.Gamertag__c = 'testMPAccount_X';
		testMPAccount_X.DWID__c = 'gadsg';
		testMPAccount_X.Is_Current__c = True;
		testMPAccount_X.Is_Premium__c = true;
		testMPAccount_X.Premium_Start__c = date.newInstance(2012, 1, 10);
		insert testMPAccount_X;
		
		Multiplayer_Account__c testMPAccount_P = new Multiplayer_Account__c();
		testMPAccount_P.Contact__c = testContact.id;
		testMPAccount_P.Platform__c = 'PSN';
		testMPAccount_P.Gamertag__c = 'testMPAccount_P';
		testMPAccount_P.DWID__c = 'jfgj';
		testMPAccount_P.Is_Current__c = True;
		testMPAccount_P.Is_Premium__c = true;
		testMPAccount_P.Premium_Start__c = date.newInstance(2012, 1, 10);
		insert testMPAccount_P;
		
		//Create Controller:
		ApexPages.StandardController stdController = new ApexPages.StandardController(testContact);
		Elite_Abatement_Wizard_Controller extController = new Elite_Abatement_Wizard_Controller(stdController);
		
		PageReference pRef = new PageReference('');
		
		//SENTIMENT
		//Get contact method failure:
		pRef = extController.sentimentCompleted();
		//Set Contact method, get Sentiment Failure:
		extController.currentAbatement.Contact_Method__c = 'Phone';
		pRef = extController.sentimentCompleted();
		//Set Sentiment: Upset:
		extController.currentAbatement.Sentiment__c = 'Upset';
		pRef = extController.sentimentCompleted();
		
		
		//BENEFITS:
		//Get Failure:
		pRef = extController.benefitsCompleted();
		//Set Benefits to Yes, get Result Check:
		extController.currentAbatement.Reiterated_ELITE_Benefits__c = 'Yes';
		pRef = extController.benefitsCompleted();
		//Set Result to Satisfied:
		extController.currentAbatement.Reiterated_ELITE_Result__c = 'Satisfied';
		pRef = extController.benefitsCompleted();
		//Set Result to Not Satisfied (Phone):
		extController.currentAbatement.Reiterated_ELITE_Result__c = 'Not Satisfied';
		pRef = extController.benefitsCompleted();
		//Set Result to Not Satisfied (Chat):
		extController.currentAbatement.Contact_Method__c = 'Chat';
		extController.currentAbatement.Reiterated_ELITE_Result__c = 'Not Satisfied';
		pRef = extController.benefitsCompleted();
		
		//IDENTITY VERIFICATION:
		//Get Failure:
		pRef = extController.identityVerificationCompleted();
		//Set User Verified:
		extController.currentAbatement.User_Is_Verified__c = 'Yes';
		pRef = extController.identityVerificationCompleted();
		
		//LINKED ACCOUNTS VERIFICATION:
		//Get Failure:
		pRef = extController.linkedAccountsVerificationCompleted();
		//Set Wrong SelectedGamertag 
		extController.selectedGamertag = 'wrongGamertag';
		pRef = extController.linkedAccountsVerificationCompleted();
		//Set Correct selectedGamertag:
		extController.selectedGamertag = 'testMPAccount_30_X';
		pRef = extController.linkedAccountsVerificationCompleted();
		
		//AWARD REMEDY BUTTON:
		//Get Failure:
		pRef = extController.awardRemedy();
		
		//REMEDY COMPLETED:
		//Get Failure:
		pRef = extController.remedyCompleted();
		//Set Result to Satisfied & Get Null Awarded:
		extController.currentAbatement.awarded2XP_Result__c = 'Satisfied';
		pRef = extController.remedyCompleted();
		//Set Awarded 2xp to Yes:
		extController.currentAbatement.awarded2XP_Result__c = 'Satisfied';
		extController.currentAbatement.awarded2XP__c = 'Yes';
		pRef = extController.remedyCompleted();
		//Set Result to Not Satisfied:
		extController.currentAbatement.awarded2XP_Result__c = 'Not Satisfied';
		//Run with XBOX 30 Days:
		pRef = extController.remedyCompleted();
		//Run with XBOX Not-30 Days:
		extController.selectedGamertag = 'testMPAccount_X';
		pRef = extController.linkedAccountsVerificationCompleted();
		pRef = extController.remedyCompleted();
		//Run with PS3 30 Days:
		extController.selectedGamertag = 'testMPAccount_30_P';
		pRef = extController.linkedAccountsVerificationCompleted();
		pRef = extController.remedyCompleted();
		//Run with PS3 Not-30 Days:
		extController.selectedGamertag = 'testMPAccount_P';
		pRef = extController.linkedAccountsVerificationCompleted();
		pRef = extController.remedyCompleted();
		
		//REFUND COMMUNICATED:
		//Start with PS3 - Catch Error:
		pRef = extController.refundCommunicated();
		//Set Communicated PS3 Process:
		extController.currentAbatement.Communicated_PS3_Refund_Process__c = 'Yes';
		pRef = extController.refundCommunicated();
		//Run with Xbox (Failure):
		extController.selectedGamertag = 'testMPAccount_X';
		pRef = extController.linkedAccountsVerificationCompleted();
		pRef = extController.refundCommunicated();
		//Run with xbox (Success):
		extController.currentAbatement.Communicated_XBOX_Refund_Process__c = 'Yes';
		pRef = extController.refundCommunicated();
		
		//PS3 Info Collected:
		//Catch Locale Failure:
		pRef = extController.ps3InfoCollected();
		//Set Locale:
		extController.currentAbatement.Locale__c = 'North America';
		//Catch Sign in Id Failure:
		pRef = extController.ps3InfoCollected();
		//Set Sign in Id:
		extController.currentAbatement.PS3_Sign_In_ID__c = 'test@test.org';
		pRef = extController.ps3InfoCollected();
		
		//REVIEW COMPLETED:
		//Get Insert Failure:
		extController.currentAbatement.Multiplayer_Account__c = null;
//add elite product
insert new Public_Product__c(Name = 'Call of Duty: ELITE');
		pRef = extController.reviewCompleted();
		//Fix Missing MP Account & Run Successfully:
		extController.currentAbatement.Multiplayer_Account__c = extController.selectedMPAccount.id;
		pRef = extController.reviewCompleted();
		
		
		
		
		//CALL MICROSOFT PAGE:
		//Get Redirected Answer Failure:
		extController.currentAbatement.redirectedToMSFT__c = '';
		pRef = extController.callMSFTComplete();
		//Set Redirect Answer:
		extController.currentAbatement.redirectedToMSFT__c = 'Yes';
		pRef = extController.callMSFTComplete();
		
		//GoTo Case Function:
		pRef = extController.goToCase();
		
		//Back Button:		
		extController.setAllPagesFalse();
		extController.showBenefitsPage = true;
		pRef = extController.previousPage();
		
		extController.setAllPagesFalse();
		extController.showIdentityVerificationPage = true;
		extController.previousPage = 'Sentiment';
		pRef = extController.previousPage();
		extController.setAllPagesFalse();
		extController.showIdentityVerificationPage = true;
		extController.previousPage = 'Benefits';
		pRef = extController.previousPage();
		
		extController.setAllPagesFalse();
		extController.showLinkedAccountsPage = true;
		extController.previousPage = 'Sentiment';
		pRef = extController.previousPage();
		extController.setAllPagesFalse();
		extController.showLinkedAccountsPage = true;
		extController.previousPage = 'Benefits';
		pRef = extController.previousPage();
		extController.setAllPagesFalse();
		extController.showLinkedAccountsPage = true;
		extController.previousPage = 'Identity';
		pRef = extController.previousPage();
		
		extController.setAllPagesFalse();
		extController.showRemedyPage = true;
		extController.previousPage = 'Linked';
		pRef = extController.previousPage();
		
		extController.setAllPagesFalse();
		extController.showReviewPage = true;
		extController.previousPage = 'Benefits';
		pRef = extController.previousPage();
		extController.setAllPagesFalse();
		extController.showReviewPage = true;
		extController.previousPage = 'Remedy';
		pRef = extController.previousPage();
		extController.setAllPagesFalse();
		extController.showReviewPage = true;
		extController.previousPage = 'XBOXRefund';
		pRef = extController.previousPage();
		extController.setAllPagesFalse();
		extController.showReviewPage = true;
		extController.previousPage = 'PS3Refund';
		pRef = extController.previousPage();
		extController.setAllPagesFalse();
		extController.showReviewPage = true;
		extController.previousPage = 'PS3Info';
		pRef = extController.previousPage();
		extController.setAllPagesFalse();
		extController.showReviewPage = true;
		extController.previousPage = 'CallMSFT';
		pRef = extController.previousPage();
		
		extController.setAllPagesFalse();
		extController.showCallMSFTPage = true;
		extController.previousPage = 'Linked';
		pRef = extController.previousPage();
		extController.setAllPagesFalse();
		extController.showCallMSFTPage = true;
		extController.previousPage = 'Remedy';
		pRef = extController.previousPage();
		
		extController.setAllPagesFalse();
		extController.showPS3InfoPage = true;
		pRef = extController.previousPage();
		
		extController.setAllPagesFalse();
		extController.showCommunicatePS3Refund = true;
		extController.previousPage = 'Linked';
		pRef = extController.previousPage();
		extController.setAllPagesFalse();
		extController.showCommunicatePS3Refund = true;
		extController.previousPage = 'Remedy';
		pRef = extController.previousPage();
		
		extController.setAllPagesFalse();
		extController.showCommunicateXBOXRefund = true;
		pRef = extController.previousPage();
		
		pRef = extController.cancel();
		
		//START OVER WITH LEGAL ROUTE
		//Set Sentiment: Legal:
		extController.currentAbatement.Sentiment__c = 'Legal';
		pRef = extController.sentimentCompleted();
		//PS3 Linked Accounts - 30:
		extController.selectedGamertag = 'testMPAccount_30_P';
		pRef = extController.linkedAccountsVerificationCompleted();
		//Xbox Linked Accounts - 30:
		extController.selectedGamertag = 'testMPAccount_30_X';
		pRef = extController.linkedAccountsVerificationCompleted();
		//PS3 Linked Accounts:
		extController.selectedGamertag = 'testMPAccount_P';
		pRef = extController.linkedAccountsVerificationCompleted();
		//PS3 Linked Accounts:
		extController.selectedGamertag = 'testMPAccount_X';
		pRef = extController.linkedAccountsVerificationCompleted();
		
		//Duplicate Submission error:
		Abatement__c duplicate = new Abatement__c();
		duplicate.Multiplayer_Account__c = extController.selectedMPAccount.id;
		duplicate.sentiment__c = 'Legal';
		duplicate.Gamer__c = extController.selectedContact.id;
		insert duplicate;
		extController.selectedGamertag = extController.currentAbatement.Multiplayer_Account__r.Gamertag__c;
		extController.currentAbatement.Sentiment__c = 'Legal';
		extController.currentAbatement.Multiplayer_Account__c = extController.selectedMPAccount.id;
		pRef = extController.linkedAccountsVerificationCompleted();
		
		extController.currentAbatement.Sentiment__c = 'Legal';
		pRef = extController.sentimentCompleted();
		//PS3 Linked Accounts - 30:
		extController.selectedGamertag = 'testMPAccount_30_P';
		pRef = extController.linkedAccountsVerificationCompleted();
		//Xbox Linked Accounts - 30:
		extController.selectedGamertag = 'testMPAccount_30_X';
		pRef = extController.linkedAccountsVerificationCompleted();
		//PS3 Linked Accounts:
		extController.selectedGamertag = 'testMPAccount_P';
		pRef = extController.linkedAccountsVerificationCompleted();
		//PS3 Linked Accounts:
		extController.selectedGamertag = 'testMPAccount_X';
		pRef = extController.linkedAccountsVerificationCompleted();
	}
}