/**
    * Apex Class: ELITE_Contest_VerificationSearchCtrlTest
    * Description: A test class that check the quality of the controller ELITE_Contest_Verification_SearchCtrl code and 
    *              also validate the functionality performed by it.
    * Created Date: 22 August 2012
    * Created By: Sudhir Kr. Jagetiya
    */
@isTest(seeAllData = false)
private class ELITE_Contest_VerificationSearchCtrlTest {
    static Integer NUM_OF_RECORDS = 8;
    //A test Method that check the quality of the code and validate the functionality performed by controller.
    static testMethod void myUnitTest() {
        
        ELITE_EventOperation__c event;
        List<ELITE_EventOperation__c> contestList = new List<ELITE_EventOperation__c>();
        for(Integer sNo = 0; sNo < NUM_OF_RECORDS; sNo++) {
            event = ELITE_TestUtility.createContest(false);
            event.Name = 'Contest' + sNo;
            event.Contest_Status__c = 'Planned';
            event.Start_Date__c = dateTime.now().addDays((sNo+1) * 10);
            event.End_Date__c = dateTime.now().addDays((sNo+1) * 20);
            contestList.add(event);
        }
        insert contestList;
        
        Test.startTest();
            //prepare a Reference of Page DealRentStepSpeciality
        PageReference pg = Page.ELITE_Contest_Verification_Search;
        //Set Current page
        Test.setCurrentPageReference(pg); 
        ELITE_Contest_Verification_SearchCtrl ctrl = new ELITE_Contest_Verification_SearchCtrl();
        
        ctrl.dummyContest.Contest_Status__c = 'Planned';
        ctrl.dummyContest.Start_Date__c = dateTime.now().addDays(-30);
        ctrl.dummyContest.End_Date__c = dateTime.now().addDays(10);
        ctrl.search();
        system.assertEquals(ctrl.contestList.size(), 1);
        
        ctrl.dummyContest.Contest_Status__c = 'Planned';
        ctrl.dummyContest.Start_Date__c = dateTime.now().addDays(-30);
        ctrl.dummyContest.End_Date__c = null;
        ctrl.search();
        system.assertEquals(ctrl.contestList.size(), 8);
        
        ctrl.dummyContest.Contest_Status__c = 'Planned';
        ctrl.dummyContest.Start_Date__c = null;
        ctrl.dummyContest.End_Date__c = dateTime.now();
        ctrl.search();
        system.assertEquals(ctrl.contestList.size(), 0);
        ctrl.doSort();
        system.assertEquals(ctrl.isAscending, true);
        Test.stopTest();
    }
}