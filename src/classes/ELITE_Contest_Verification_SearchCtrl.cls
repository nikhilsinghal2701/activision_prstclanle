/**
    * Apex Class: ELITE_Contest_Verification_SearchCtrl
    * Description: A controller for ELITE_Contest_Verification_Search page 
    *              that is used to provide the User interface to search contest.
    * Created Date:21 August 2012
    * Created By: Sudhir Kr. Jagetiya
    */
public without sharing class ELITE_Contest_Verification_SearchCtrl {
	//record Limit
	public final static Integer MAX_QUERY_LIMIT = 250;
	//Sorting order  
	public Boolean isAscending {get;set;}
	//indicate ther is no records with provided search text
	public Boolean isEmpty {get;set;}
    
	
	public ELITE_EventOperation__c dummyContest {get;set;}
	
	private String previousSortField = '';
	
	public List<ELITE_EventOperation__c> contestList {get; set;}
	
	String status;
	Date startDate, endDate;
	public ELITE_Contest_Verification_SearchCtrl() {
		SortField = 'Name';
		dummyContest = new ELITE_EventOperation__c();
		dummyContest.Start_Date__c = null;
		dummyContest.End_Date__c = null;
	}
	
  //----------------------------------------------------------------------------------------------------  
  // Action Function : Search (Return the results on behalf of the provided Filters)
  //----------------------------------------------------------------------------------------------------
  public PageReference search() {
  	queryData();
  	return null;
  }
  
  //----------------------------------------------------------------------------------------------------
  // Getter and setter to set or get sorting field name
  //----------------------------------------------------------------------------------------------------
  public String sortField {
    get;
    set {     
      this.previousSortField = sortField;
      this.sortField = value;
      if(previousSortField == sortField) {
        isAscending = !isAscending;
        return;
      }
      this.isAscending = true;  
     }
   }
   //----------------------------------------------------------------------------------------------------
	// To retrieve the data
	//----------------------------------------------------------------------------------------------------
	private void queryData() {
		contestList = Database.query(buildSearchQuery()); 
	  if(contestList.size() > 0)
	  	isEmpty = false;
	  else 
	  	isEmpty = true;
	}
    
	//--------------------------------------------------------------------------------------------------
	// Prepares query with filter + search string and order by sortfield
	//--------------------------------------------------------------------------------------------------
	private String buildSearchQuery() {
	            
    String sortingOrder = 'ASC';
    String strWhere = '', dateFilterString = '', statusFilter = '';
    if (isAscending == false) {
      sortingOrder = 'DESC';
    }
    
          
    String strQuery = 'SELECT Type__c, Total_Winners__c, End_Date__c, Elite_Event_Id__c, Platform__c, Contest_Status__c, Name , Prize__c'
                      + '  FROM ELITE_EventOperation__c ';
    
    if(dummyContest.Contest_Status__c != null) {
    	status = dummyContest.Contest_Status__c;
    	statusFilter = ' Contest_Status__c = :status';
    }
    
    if(dummyContest.Start_Date__c != null && dummyContest.End_Date__c != null) {
    	startDate = dummyContest.Start_Date__c.dateGMT();
    	endDate = dummyContest.End_Date__c.dateGMT();
    	dateFilterString = ' (DAY_ONLY(Start_Date__c) >= :startDate AND DAY_ONLY(Start_Date__c) <= :endDate)';
    	dateFilterString += ' OR (DAY_ONLY(End_Date__c) >= :startDate AND DAY_ONLY(End_Date__c) <= :endDate)';
    } 
    else if(dummyContest.Start_Date__c != null && dummyContest.End_Date__c == null ) {
    	startDate = dummyContest.Start_Date__c.dateGMT();
    	dateFilterString = ' (DAY_ONLY(Start_Date__c) >= :startDate)';
    } 
    else if(dummyContest.Start_Date__c == null && dummyContest.End_Date__c != null) {
    	endDate = dummyContest.End_Date__c.dateGMT();
    	dateFilterString = ' (DAY_ONLY(End_Date__c) <= :endDate)';
    }
    
    if(statusFilter != '' || dateFilterString != '') {
    	strWhere = 'WHERE';
    	strWhere += statusFilter != '' ? statusFilter : '';
    	strWhere += (statusFilter != '' && dateFilterString != '') ? ' AND (' + dateFilterString + ')' : dateFilterString ;
    } 
    
    strQuery += strWhere;
    
    strQuery = strQuery + ' ORDER BY ' + SortField;
    strQuery = strQuery + ' ' + sortingOrder;
    strQuery = strQuery + ' NULLS LAST Limit '+MAX_QUERY_LIMIT;
    system.debug('strQuery '+ strQuery);
    return strQuery;
	}
    
	//---------------------------------------------------------------------------------------------------
	//Action methods for sorting
	//---------------------------------------------------------------------------------------------------
	public PageReference DoSort() {     
    queryData();
    return null;    
	}
}