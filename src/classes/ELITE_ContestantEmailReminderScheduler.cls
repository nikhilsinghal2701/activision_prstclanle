/**
    * Apex Class :  ELITE_ContestantEmailReminderScheduler
    * Description: 
    * Created Date: 16 Nov 2012
    * Created By: Sudhir Kumar Jagetiya
    * Modified By: R. Washington
    */

global without sharing class ELITE_ContestantEmailReminderScheduler implements Schedulable {
    //static final Integer scheduleInterval = Integer.valueOf(system.Label.ELITE_ScheduleInterval);
    //static final Integer scheduleIntervalMinutes = Integer.valueOf(system.Label.ELITE_ScheduleInterval_Minutes);
                                                        
    global void execute(SchedulableContext SC) {
        List<ELITE_EventContestant__c> contestantsToBeUpdate = new List<ELITE_EventContestant__c>();
        List<ELITE_EventContestant__c> contestantListForReminderEmail = [SELECT Reminder_Email_Sent__c, Reminder_Date_Prize__c
                                                                         FROM ELITE_EventContestant__c 
                                                                         WHERE Reminder_Email_Sent__c = false 
                                                                         AND Reminder_Date_Prize__c != null 
                                                                         AND Reminder_Date_Prize__c <= :datetime.now()
                                                                         AND Contestant_Responded__c = false];
        
        for(ELITE_EventContestant__c contestant : contestantListForReminderEmail) {
            contestant.Reminder_Email_Sent__c = true;
            contestantsToBeUpdate.add(contestant);
        } 
        
        List<ELITE_EventContestant__c> contestantListForPrizeForfitureEmail = [SELECT Prize_Forfeiture_Email_Sent__c, Forfeiture_Date__c
                                                                                 FROM ELITE_EventContestant__c 
                                                                                 WHERE Prize_Forfeiture_Email_Sent__c = false 
                                                                                 AND Forfeiture_Date__c != null
                                                                                 AND Forfeiture_Date__c <= :datetime.now()
                                                                                 AND Contestant_Responded__c = false
                                                                                 AND Id NOT IN :contestantListForReminderEmail];
        
        for(ELITE_EventContestant__c contestant : contestantListForPrizeForfitureEmail) {
            contestant.Prize_Forfeiture_Email_Sent__c = true;
            contestantsToBeUpdate.add(contestant);
        }
        update contestantsToBeUpdate;
        /*
        Datetime scheduleDateTime = DateTime.now().adddays(scheduleInterval);
        scheduleDateTime = scheduleDateTime.addMinutes(scheduleIntervalMinutes);
        String CRON_EXPRESSION = '0 ' + String.valueOf(scheduleDateTime.minute()) + ' ' +
                                                         String.valueOf(scheduleDateTime.hour()) + ' ' + 
                                                         String.valueOf(scheduleDateTime.day()) + ' ' +
                                                         String.valueOf(scheduleDateTime.month()) + ' ? ' + 
                                                         String.valueOf(scheduleDateTime.year());
        ELITE_ContestantEmailReminderScheduler schObj = new ELITE_ContestantEmailReminderScheduler();
        system.schedule('ELITE Reminder and Prize Forfeiture Email Scheduler ' + dateTime.now(), CRON_EXPRESSION, schObj);*/
    }
    
    @isTest 
  public static void unitTests() {
    
    Contact contactGamer = ELITE_TestUtility.createGamer(true);
    Multiplayer_Account__c MultiplayerAccount = ELITE_TestUtility.createMultiplayerAccount(contactGamer.Id, true);
    ELITE_EventOperation__c contest = ELITE_TestUtility.createContest(true); 
    ELITE_EventContestant__c contestant = ELITE_TestUtility.createContestant(contest.Id, MultiplayerAccount.Id, 1, false);
    contestant.Reminder_Date_Prize__c = dateTime.now().addDays(-1);
    insert contestant;
    
    String CRON_EXP = '0 0 0 16 11 ? 2022';
    
    Test.startTest();
        String jobId = System.schedule('ELITE '+System.now(),  CRON_EXP, new ELITE_ContestantEmailReminderScheduler());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];    
        System.assertEquals(CRON_EXP, ct.CronExpression);
    Test.stopTest();
    
  }  
}