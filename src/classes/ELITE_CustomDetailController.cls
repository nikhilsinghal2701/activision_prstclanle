/**
    * Apex Class: ELITE_PrizeCustomDetailController
    * Dexcription: 
    * Created By: Sudhir Kumar Jagetiya
    * Created Date: Sep 18, 2012
    */
public without sharing class ELITE_CustomDetailController {
    
    public boolean isPortalUser {get;set;}
    
    public ELITE_CustomDetailController(ApexPages.StandardController con){
        isPortalUser = false;
        isPortalUser = ELITE_Utility.isPortalUser(); 
    }
}