/**
 * Apex Class: ELITE_CustomDetailControllerTest
 * Description: 
 * Created By: Sudhir kumar Jagetiya
 * Created Date: Sep 20, 2012
 */
@isTest
private class ELITE_CustomDetailControllerTest {
		static final String PORTAL_PROFILE_NAME = 'ATVI OHV Cust Port User';
    static testMethod void myUnitTest() {
    	//Create Test Data
      Contact contact = ELITE_TestUtility.createGamer(true);
      ELITE_EventOperation__c event = ELITE_TestUtility.createContest(true);
      
      List<Profile> profiles = [SELECT Id
																FROM Profile 
																WHERE Name = :PORTAL_PROFILE_NAME LIMIT 1];
	    
	    system.assert(profiles.size() > 0);
      User user = ELITE_TestUtility.createPortalUser('Test', profiles.get(0).Id, contact.Id, true);
      
      system.runAs(user) {
	     
	      Test.startTest();
	      	ApexPages.StandardController stdCtrl = new ApexPages.StandardController(event);
	      	ELITE_CustomDetailController ctrl = new ELITE_CustomDetailController(stdCtrl);
	      	system.assertEquals(ctrl.isPortalUser , true);
	      Test.stopTest();
	      
      }
    }
    
    
}