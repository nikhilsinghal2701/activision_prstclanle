/**
    * Apex Class: ELITE_EmailServiceHandler 
    * Description: Used to send an Email with attachments to all winners
    * Created By: Sudhir Kr. Jagetiya
    * Created Date: Sep 15, 2012
    */
global class ELITE_EmailServiceHandler implements Database.Batchable<SObject> {
        
        global ELITE_EmailServiceHandler () {
            
        }
        
    //Start method of Batch Class
    global Database.QueryLocator start(Database.BatchableContext BC){
        String soql = 'SELECT Send_Email__c, Email__c, '
                                     + 'Event_Operation__r.ROW_Legal_Rules_File_Id__c, '
                                     + 'Event_Operation__r.US_Canada_Legal_Rules_File_Id__c, '
                                     + 'Event_Operation__r.Eligible_Countries__c, '
                                     + 'Event_Operation__r.Name, Event_Operation__c, '
                                     + 'ELITE_Platform__r.Contact__c, ELITE_Platform__c '
                                     + 'FROM ELITE_EventContestant__c '
                                     + 'WHERE Email__c != null AND Send_Email__c = true '
                                     + 'AND ELITE_Platform__r.Contact__r.Email != null';
        return Database.getQueryLocator(soql);
    }
    
    //Excute method of Batch class   
    global void execute(Database.BatchableContext BC,List<ELITE_EventContestant__c> contestantList){
        try{
            ELITE_EventContestantManagement.sendEmail(contestantList);
        }catch(Exception ex){
            System.debug('--Exception Occur while sending an Email ---'+ ex.getMessage());
        }
    }
    
    //finish Method
    global void finish(Database.BatchableContext BC){ 
        List<ELITE_EventContestant__c> contestantList = 
                             [SELECT Send_Email__c, Email__c, 
                                                 Event_Operation__r.ROW_Legal_Rules_File_Id__c, 
                                                 Event_Operation__r.US_Canada_Legal_Rules_File_Id__c, 
                                                 Event_Operation__r.Eligible_Countries__c, 
                                                 Event_Operation__r.Name, Event_Operation__c, 
                                                 ELITE_Platform__r.Contact__c, ELITE_Platform__c 
                                  FROM ELITE_EventContestant__c 
                                  WHERE Email__c != null AND Send_Email__c = true AND ELITE_Platform__r.Contact__r.Email != null];
        for(ELITE_EventContestant__c eContestant:contestantList){
            eContestant.Send_Email__c = false;
        }
        update contestantList;
    }
}