/**
    * Apex Class: ELITE_EventContestantManagement
    * Description: Helper class for the ELITE_EventContestantTrigger which update Status and contestant Tier of contestant.
    * Created Date: 16 August 2012
    * Created By: Sudhir Kr. Jagetiya
    * Modified By: Varun Vatsa
    * Description: Added Method to Find Next contestent in case the winner is forfeited. 
    * Method Added: getNextWinningContestant,setContestPrizeStatus
    */
public without sharing class ELITE_EventContestantManagement {
    
    static final String TIER_STATUS_WINNER = 'Winner';
    static final String TIER_STATUS_ALTERNATE = 'Alternate';
    static final String CONTESTANT_STATUS_WINNER = 'Winner - unverified';
    static final String CONTESTANT_STATUS_WINNER_VERIFIED = 'Winner - verified';
    static final String CONTESTANT_STATUS_ALTERNATE = 'Alternate - unverified';
    static final String CONTESTANT_STATUS_ALTERNATE_VERIFIED = 'Alternate - verified';
    static final String CONTESTANT_STATUS_NO_RESPONSE_1ST_EMAIL = 'No response - 1st Email';
    static final String CONTESTANT_COUNTRY_US = 'The United States of America';
    static final String CONTESTANT_COUNTRY_CANADA = 'Canada';
    static Boolean isUpdateInProgress = false; 
    static Boolean isUpdateInProgress2 = false; 
    static EmailTemplate emailTemplate;
    static Set<String> contestantWinningStatus;
    static {
    		contestantWinningStatus = new Set<String>{CONTESTANT_STATUS_WINNER, CONTESTANT_STATUS_WINNER_VERIFIED, CONTESTANT_STATUS_ALTERNATE, CONTESTANT_STATUS_ALTERNATE_VERIFIED, CONTESTANT_STATUS_NO_RESPONSE_1ST_EMAIL};
        if(emailTemplate == null) {
            for(EmailTemplate eTemplate : [SELECT Name, Id, Body, HtmlValue
                                                 FROM EmailTemplate 
                                               WHERE Name like :system.Label.ELITE_TEMPLATE_NAME
                                                    LIMIT 1])
            emailTemplate = eTemplate;
        }
    }
    
    public static void beforeInsertUpdate(List<ELITE_EventContestant__c> newList, Map<Id, ELITE_EventContestant__c> oldMap) {
        Boolean isInsert = oldMap == null;
        updateStatusAndContestantTier(newList, isInsert);
        validateNumOfWinner(newList, oldMap);
    }
    
    public static void afterInsert(List<ELITE_EventContestant__c> newList) {
    	setEncryptedId(newList);
    }
    
    public static void afterInsertUpdate(List<ELITE_EventContestant__c> newList) {
        if(newList.size() < 10) {
            if(!isUpdateInProgress) {   
                List<ELITE_EventContestant__c> contestantList = [SELECT Send_Email__c, Email__c, Name,
                                                                    Event_Operation__r.ROW_Legal_Rules_File_Id__c, 
                                                                    Event_Operation__r.US_Canada_Legal_Rules_File_Id__c, 
                                                                    Event_Operation__r.Eligible_Countries__c, 
                                                                    Event_Operation__r.Name, Event_Operation__r.Contest_Status__c, Event_Operation__c, 
                                                                    ELITE_Platform__r.Contact__c, ELITE_Platform__c,
                                                                    ELITE_Platform__r.Gamertag__c
                                                         FROM ELITE_EventContestant__c 
                                                        WHERE Email__c != null 
                                                        AND Send_Email__c = true
                                                        AND ELITE_Platform__r.Contact__r.Email != null
                                                        AND Id IN: newList];
	            if(contestantList.size() > 0) {
	            	sendEmail(contestantList);
	        
                for(ELITE_EventContestant__c eContestant : contestantList) {
                    eContestant.Send_Email__c = false;
                }
                isUpdateInProgress = true;
                update contestantList;
                isUpdateInProgress = false;
	            }
                
            }
        } 
        else {
            Boolean isSendMail = false;
            for(ELITE_EventContestant__c eContestant : newList) {
                if(eContestant.Send_Email__c){
                    isSendMail = true;
                    break;
                }
            }
            if(isSendMail){
                Database.executeBatch(new ELITE_EmailServiceHandler(),10);
            }
        }
    }
    
    static void setEncryptedId(List<ELITE_EventContestant__c> newList) {
    	List<ELITE_EventContestant__c> contestantToBeUpdate = new List<ELITE_EventContestant__c>();
    	Blob contestantId, encryptedId;
    	String encryptedString;
    	for(ELITE_EventContestant__c eContestant : newList) {
    		contestantId = Blob.valueOf(eContestant.Id);
				encryptedId = Crypto.generateDigest('SHA1',contestantId);
				encryptedString = EncodingUtil.convertToHex(encryptedId);
				contestantToBeUpdate.add(new ELITE_EventContestant__c(Id = eContestant.Id, Encrypted_Id__c = encryptedString));
    	}
	    update contestantToBeUpdate;
    }
    
    static void updateStatusAndContestantTier(List<ELITE_EventContestant__c> newList, Boolean isInsert) {
        Set<Id> eventIds = new Set<Id>();
        Set<Id> multiplayerAccountIds = new Set<Id>();
        
        
        
        //Determine the ids of the events and multiplayerAccounts.
        for(ELITE_EventContestant__c contestant : newList) {
            eventIds.add(contestant.Event_Operation__c);
            multiplayerAccountIds.add(contestant.ELITE_Platform__c);
        }
        if(isInsert) setContestantEmailAndContestStatus(newList, eventIds, multiplayerAccountIds);
    		setContestantTier(newList, eventIds,isInsert);
    		
    }
    
    static void setContestantEmailAndContestStatus(List<ELITE_EventContestant__c> contestantList, Set<Id> eventIds, Set<Id> multiplayerAccountIds) {
    	Map<Id, Multiplayer_Account__c> multiplayerAccountMap;
    	multiplayerAccountMap = new Map<Id, Multiplayer_Account__c>([SELECT Contact__r.Email, Contact__c 
                                                                     FROM Multiplayer_Account__c 
                                                                    WHERE Id IN :multiplayerAccountIds]);
        
        
	    //To Update the ELITE_EventContestant.Contest_Status__c by ELITE_EventOperation.Contest_Status__c
	    Map<Id, ELITE_EventOperation__c> contestMap = 
	                        new Map<Id, ELITE_EventOperation__c>([SELECT Contest_Status__c
	                                                            FROM ELITE_EventOperation__c
	                                                            WHERE Id IN :eventIds]);
	     //update Status And Contestant Tier if contestant rank is belong to existiong tiers of event.
        for(ELITE_EventContestant__c contestant : contestantList) {
	        //Update Contestant Contest_Status__c
	        if(contestant.Event_Operation__c != null && contestMap.containsKey(contestant.Event_Operation__c))
	            contestant.Contest_Status__c = contestMap.get(contestant.Event_Operation__c).Contest_Status__c;
	        
	        //Update Contestant Email Address
	        if(contestant.ELITE_Platform__c != null && multiplayerAccountMap.containsKey(contestant.ELITE_Platform__c))
	            contestant.Email__c = multiplayerAccountMap.get(contestant.ELITE_Platform__c).Contact__r.Email;
        }
    }
    
    static void setContestantTier(List<ELITE_EventContestant__c> contestantList, Set<Id> eventIds, Boolean isInsert) {
    	String fieldName = isInsert ? 'Rank__c' : 'Prize_Rank__c';
			Map<Id, List<ELITE_Tier__c>> eventTierMap = new Map<Id, List<ELITE_Tier__c>>();
			//Populate the map for all the tiers those are associated to the ELITE_EventContestant__c.event
	    for(ELITE_Tier__c tier : [SELECT Id, Type__c, Name, Last_Position__c, First_Position__c, Event_Id__c 
	                                                        FROM ELITE_Tier__c 
	                                                        WHERE Event_Id__c IN :eventIds]) {
	        if(!eventTierMap.containsKey(tier.Event_Id__c)) {
	            eventTierMap.put(tier.Event_Id__c, new List<ELITE_Tier__c>());
	        }
	        eventTierMap.get(tier.Event_Id__c).add(tier);
	    }
	    
    	for(ELITE_EventContestant__c contestant : contestantList) {
    		 //Update Contestant Tier and their Status
        if(contestant.get(fieldName) != null && eventTierMap.containsKey(contestant.Event_Operation__c)) {
            
	        for(ELITE_Tier__c tier : eventTierMap.get(contestant.Event_Operation__c)) {
        		if(Double.valueOf(contestant.get(fieldName)) >= tier.First_Position__c && Double.valueOf(contestant.get(fieldName)) <= tier.Last_Position__c) {
                
	            if(isInsert && tier.Type__c != null) {
	                if(tier.Type__c.equalsIgnoreCase(TIER_STATUS_WINNER)) {
	                    contestant.Status__c = CONTESTANT_STATUS_WINNER;
	                } else if(tier.Type__c.equalsIgnoreCase(TIER_STATUS_ALTERNATE)) {
	                    contestant.Status__c = CONTESTANT_STATUS_ALTERNATE;
	                }
	               // contestant.Prize_Rank__c = contestant.Rank__c;
	            }
	            contestant.Contestant_Tier__c = tier.Id;
	            break;
            }
	          
	        }
        }
      }
    }
    
    public static void updateContestantPrizeRank(List<ELITE_EventContestant__c> newList, Map<Id, ELITE_EventContestant__c> oldMap) {
    	if(!isUpdateInProgress2) {
	    	Boolean isInsert = oldMap == null;
	    	Boolean isUpdate = (newList != null && oldMap != null);
	    	Set<Id> eventIds = new Set<Id>();
	    	for(ELITE_EventContestant__c contestant : newList) {
	    		if(isInsert ||( isUpdate && (contestant.Status__c != oldMap.get(contestant.Id).Status__c))) {
	    			eventIds.add(contestant.Event_Operation__c);
	    		}
	    	}
	    	List<ELITE_EventOperation__c> contestList = [SELECT (SELECT Id,Contestant_Tier__c, Rank__c, Prize_Rank__c, Status__c
	                                  													FROM Event_Contestants__r 
	                                  													ORDER BY Rank__c ASC)
	  																									FROM ELITE_EventOperation__c e
	          																					WHERE Id IN :eventIds];
	      Integer numOfDisqualified;
				List<ELITE_EventContestant__c> contestantToBeUpdate = new List<ELITE_EventContestant__c>();
				for(ELITE_EventOperation__c contest : contestList) {
	      	numOfDisqualified = 0;
	      	for(ELITE_EventContestant__c contestant : contest.Event_Contestants__r) {
	      		if(contestantWinningStatus.contains(contestant.Status__c)) {
	      			contestant.Prize_Rank__c = contestant.Rank__c - numOfDisqualified;
	      		} else {
	      			numOfDisqualified++;
	      			contestant.Prize_Rank__c = 0;
	      			contestant.Contestant_Tier__c = null;
	      		}
	      		contestantToBeUpdate.add(contestant);
	      	}
	      } 
	      isUpdateInProgress2 = true;
		    update contestantToBeUpdate;
		    isUpdateInProgress2 = false;
    	}                    																													
    }
    //Method Added By Varun Vatsa
    public static void getNextWinningContestant(list<ELITE_EventContestant__c> forfeitedContestants){
        list<Id> eventOpIds = new list<ID>();
        list<Id> forfeitedContestantsIds = new list<ID>();
        list<ELITE_EventContestant__c> secondListOfContestants = new list<ELITE_EventContestant__c>();
        
        for(ELITE_EventContestant__c eventContestant:forfeitedContestants){
            eventOpIds.add(eventContestant.Event_Operation__c);
            forfeitedContestantsIds.add(eventContestant.id);
        }
        
        if(eventOpIds!=null && eventOpIds.size()>0){
            
            for(ELITE_EventOperation__c 
                            events:[Select (Select Id, Rank__c, Status__c, isWinningAlternate__c
                                                          From Event_Contestants__r 
                                                          where id not in:forfeitedContestantsIds
                                                                    and Status__c = :CONTESTANT_STATUS_ALTERNATE_VERIFIED
                                                          order by  Rank__c asc
                                                          limit 1)
                                  From ELITE_EventOperation__c e
                                  where id in:eventOpIds]){
                                    
                if(events.Event_Contestants__r !=null 
                            && events.Event_Contestants__r.size()>0){
                                ELITE_EventContestant__c contestant = events.Event_Contestants__r.get(0);
                                contestant.isWinningAlternate__c = true;
                                contestant.Status__c = CONTESTANT_STATUS_WINNER_VERIFIED;
                                secondListOfContestants.add(contestant);    
                }
            }
        }
        
        if(secondListOfContestants.size()>0){
            update secondListOfContestants;
        }
    }
    
    //Method Added by Varun Vatsa to update hasUnfulfilledPrizes__c
    public static void setContestPrizeStatus(list<Id> eventOpIds){
        list<ELITE_EventOperation__c> contests = new list<ELITE_EventOperation__c>();
        
        for(ELITE_EventOperation__c events:[Select id,hasUnfulfilledPrizes__c 
                                                                                from ELITE_EventOperation__c
                                                                                where id in:eventOpIds]){
            events.hasUnfulfilledPrizes__c = true;
            contests.add(events);                                                   
        }
        
        if(contests.size()>0){
            update contests;
        }
    }
    
    //Send an email
    public static void sendEmail(List<ELITE_EventContestant__c> newList) {
        
        Set<Id> usCanadaContestIds = new Set<Id>();
        Set<Id> restContestIds = new Set<Id>();
        
        for(ELITE_EventContestant__c contestant : newList) {
            if(contestant.Event_Operation__r.Eligible_Countries__c != null 
                 && (contestant.Event_Operation__r.Eligible_Countries__c.contains(CONTESTANT_COUNTRY_CANADA)
                 || contestant.Event_Operation__r.Eligible_Countries__c.contains(CONTESTANT_COUNTRY_US))) {
                
                usCanadaContestIds.add(contestant.Event_Operation__r.US_Canada_Legal_Rules_File_Id__c);
            } 
            else {
                restContestIds.add(contestant.Event_Operation__r.ROW_Legal_Rules_File_Id__c);
            }
            
        }
        
        Map<Id, Messaging.Emailfileattachment> mapOfAttachments = new MAp<Id, Messaging.Emailfileattachment>();
        Messaging.Emailfileattachment efa;
        for(Attachment attachment : [SELECT Id, ParentId, Name, Description, 
                                                                                ContentType, BodyLength, Body 
                                                                    FROM Attachment
                                                                    WHERE Id IN :usCanadaContestIds 
                                                                         OR Id IN :restContestIds]) {
                                                                            
            efa = new Messaging.Emailfileattachment();
            efa.setFileName(attachment.Name);
                efa.setBody(attachment.Body);
            mapOfAttachments.put(attachment.parentId, efa);
        }
        assignAttachments(newList, mapOfAttachments);
        
    }
    
    //To assign a attachment with the mail
    private static void assignAttachments(List<ELITE_EventContestant__c> contestantList,
                                                                Map<Id, Messaging.Emailfileattachment> mapOfAttachments) {
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail;
        if(emailTemplate != null){
        		String str = '';
            for(ELITE_EventContestant__c contestant : contestantList) {
            		/*str = 'Name >>>>' + contestant.Name;
            		str += '<br/>Email__c >>> '+ contestant.Email__c;
            		str += '<br/>Contact Id >>>> '+  contestant.ELITE_Platform__r.Contact__c;
            		str += '<br/>Wide Address >>>> '+  system.Label.ELITE_OrganizationWideAddresses;
            		str += '<br/>Template Id >>>> '+  emailTemplate.Id;
            		str += '<br/>Contestant Id >>>> '+  contestant.Id;
            		str += '<br/>Event Id >>>> '+  contestant.Event_Operation__c;
        		 		mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new String[] {'sudhir.jagetiya@metacube.com'});
                mail.setHtmlBody(str);
                mailList.add(mail);*/
            		
                if(contestant.Email__c != null && contestant.ELITE_Platform__r.Contact__c != null) {
                    mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(new String[] {contestant.Email__c});
                    mail.setOrgWideEmailAddressId(system.Label.ELITE_OrganizationWideAddresses);
                    mail.setTemplateId(emailTemplate.Id);
                    mail.setTargetObjectId(contestant.ELITE_Platform__r.Contact__c);
                    mail.setSaveAsActivity(false);
                    if(mapOfAttachments.containsKey(contestant.Event_Operation__c)) {
                        mail.setFileAttachments(new List<Messaging.Emailfileattachment>{mapOfAttachments.get(contestant.Event_Operation__c)}); 
                    }
                    
                  	mail.setWhatId(contestant.Id);
                    mailList.add(mail);
                }
            }
            if(mailList.size() > 0) {
                try{
                      Messaging.sendEmail(mailList);
                   }
                Catch(exception ex) {
                    throw ex;
                }
          }
        }
    }
    
    private static void validateNumOfWinner(List<ELITE_EventContestant__c> contestantList, Map<Id, ELITE_EventContestant__c> oldMap) {
    	Boolean isInsert = oldMap == null;
    	Boolean isUpdate = contestantList != null && oldMap != null;
    	Set<Id> eventIds = new Set<Id>();
    	Set<String> contestantStatus = new Set<String>{CONTESTANT_STATUS_WINNER_VERIFIED,CONTESTANT_STATUS_ALTERNATE_VERIFIED};
    	List<ELITE_EventContestant__c> contestants = new List<ELITE_EventContestant__c>();
    	
    	for(ELITE_EventContestant__c contestant : contestantList) {
    		if(contestant.Status__c != null 
    				&& ((isInsert && contestantStatus.contains(contestant.Status__c)) 
    							|| (isUpdate && (contestant.Status__c != oldMap.get(contestant.Id).Status__c) 
    													 && (contestantStatus.contains(contestant.Status__c) 
    													 		|| contestantStatus.contains(oldMap.get(contestant.Id).Status__c))))) {
    			eventIds.add(contestant.Event_Operation__c);
    			contestants.add(contestant);
    		}
    	}
    	
    	List<ELITE_EventOperation__c> contestList = [SELECT Max_Winners__c, Total_Winners__c, 
    																										Total_Alternates__c, Max_Alternates__c,
    																										(SELECT Status__c 
    																										 FROM Event_Contestants__r
    																										 WHERE Status__c IN :contestantStatus
    																										 AND Id NOT IN :contestants) 
    																						 FROM ELITE_EventOperation__c
    																						 WHERE Id IN :eventIds];
    																						 
    	
    	Map<Id, ContestWrapper> contestMap = new Map<Id, ContestWrapper>();
    	
    	for(ELITE_EventOperation__c contest : contestList) {
    		contestMap.put(contest.Id, new ContestWrapper(contest.Max_Winners__c, contest.Max_Alternates__c, 0.0, 0.0));
    		
    		for(ELITE_EventContestant__c contestant : contest.Event_Contestants__r) {
    			
    			if(contestant.Status__c.equalsIgnoreCase(CONTESTANT_STATUS_WINNER_VERIFIED)) {
    				contestMap.get(contest.Id).numOfWinner += 1;
    			} 
    			else if(contestant.Status__c.equalsIgnoreCase(CONTESTANT_STATUS_ALTERNATE_VERIFIED)) {
    				contestMap.get(contest.Id).numOfAlternate += 1;
    			} 
    		}
    	}
    	
    	for(ELITE_EventContestant__c contestant : contestants) {
    		
    		if(contestant.Event_Operation__c != null && contestMap.containsKey(contestant.Event_Operation__c)) {
    			
    			if(contestant.Status__c.equalsIgnoreCase(CONTESTANT_STATUS_WINNER_VERIFIED)) {
    				contestMap.get(contestant.Event_Operation__c).numOfWinner += 1;
    				if(contestMap.get(contestant.Event_Operation__c).numOfWinner > contestMap.get(contestant.Event_Operation__c).maxWinner) {
    					contestant.addError('Number of Winner can not be greater than ' + contestMap.get(contestant.Event_Operation__c).maxWinner);
    				}
    			} 
    			else if(contestant.Status__c.equalsIgnoreCase(CONTESTANT_STATUS_ALTERNATE_VERIFIED)) {
    				contestMap.get(contestant.Event_Operation__c).numOfAlternate += 1;
    				if(contestMap.get(contestant.Event_Operation__c).numOfAlternate > contestMap.get(contestant.Event_Operation__c).maxAlternate) {
    					contestant.addError('Number of Alternates can not be greater than ' + contestMap.get(contestant.Event_Operation__c).maxAlternate);
    				}
    			} 
    		}
    		
    	}
    }
    
    public class ContestWrapper {
    	Integer maxWinner, maxAlternate, numOfWinner, numOfAlternate;
    	
    	public ContestWrapper(Decimal maxWinner, Decimal maxAlternate, Decimal numOfWinner, Decimal numOfAlternate) {
    		this.maxWinner = maxWinner != null ? Integer.valueOf(maxWinner) : 0;
    		this.maxAlternate = maxAlternate != null ? Integer.valueOf(maxAlternate) : 0;
    		this.numOfWinner = numOfWinner != null ? Integer.valueOf(numOfWinner) : 0;
    		this.numOfAlternate = numOfAlternate != null ? Integer.valueOf(numOfAlternate) : 0;
    	}
    }
}