/**
    * Apex Class: ELITE_EventContestantTriggerTest
    * Description: Test class for the ELITE_EventContestantTrigger which update Status and contestant Tier of contestant.
    * Created Date: 16 August 2012
    * Created By: Sudhir Kr. Jagetiya
    * Last Modified By: Kirti Agarwal
    */
@isTest
private class ELITE_EventContestantTriggerTest {
        //A test method that is used to check the quality of the code and functionality performed by trigger.
        // Trigger update contestant's status and contestant Tier.
    static testMethod void myUnitTest() {
        //Create test data
        //Create test data
        List<Contact> listContacts = new List<Contact>();
        Contact con = ELITE_TestUtility.createGamer(false);
        con.Gamertag__c = 'record1';
        con.UCDID__c = '0001';
        con.Email = 'record1@gmail.com';
        listContacts.add(con);
        
        con = ELITE_TestUtility.createGamer(false);
        con.Gamertag__c = 'record2';
        con.UCDID__c = '0002';
        con.Email = 'record2@gmail.com';
        listContacts.add(con);
        
        con = ELITE_TestUtility.createGamer(false);
        con.Gamertag__c = 'record3';
        con.UCDID__c = '0003';
        con.Email = 'record3@gmail.com';
        listContacts.add(con);
        
        insert listContacts;
        
        ELITE_EventOperation__c event = ELITE_TestUtility.createContest(true);
        
        List<ELITE_Tier__c> tiers = new List<ELITE_Tier__c>();
    
        tiers.add(ELITE_TestUtility.createTier(event.Id, 1, 5, false));
        tiers.add(ELITE_TestUtility.createTier(event.Id, 6, 10, false));
        tiers.add(ELITE_TestUtility.createTier(event.Id, 11, 15, false));
        insert tiers;
        
        List<Multiplayer_Account__c> muiltiplayerAccountList = new List<Multiplayer_Account__c>();
        muiltiplayerAccountList.add(ELITE_TestUtility.createMultiplayerAccount(listContacts.get(0).Id, false));
        muiltiplayerAccountList.add(ELITE_TestUtility.createMultiplayerAccount(listContacts.get(1).Id, false));
        muiltiplayerAccountList.add(ELITE_TestUtility.createMultiplayerAccount(listContacts.get(2).Id, false));
        insert muiltiplayerAccountList;
        
        List<ELITE_EventContestant__c> listContestants = new List<ELITE_EventContestant__c>();
        listContestants.add(ELITE_TestUtility.createContestant(event.Id, muiltiplayerAccountList.get(0).Id, 5, false));
        listContestants.add(ELITE_TestUtility.createContestant(event.Id, muiltiplayerAccountList.get(1).Id, 7, false));
        listContestants.add(ELITE_TestUtility.createContestant(event.Id, muiltiplayerAccountList.get(2).Id, 11, false));
        
        Test.startTest();
	        insert listContestants;
	        system.assertEquals(retrieveContestant(listContestants.get(0).Id).Contestant_Tier__c, tiers.get(0).Id);
	        system.assertEquals(retrieveContestant(listContestants.get(1).Id).Contestant_Tier__c, tiers.get(1).Id);
	        system.assertEquals(retrieveContestant(listContestants.get(2).Id).Contestant_Tier__c, tiers.get(2).Id);
	        
	        listContestants.get(2).Status__c = 'Winner - verified';
	        update listContestants.get(2);
	        
	        listContestants.get(0).Status__c = 'Prize Forfeited-No Doc Rec\'d';
	        listContestants.get(0).isWinningAlternate__c = true;
	        update listContestants.get(0);
	        
	        listContestants.get(2).Status__c = 'Prize Forfeited-No Doc Rec\'d';
	        update listContestants.get(2);
	        
	        listContestants.get(1).Status__c = 'Winner - verified';
	        listContestants.get(1).Send_Email__c = true; 
	        update listContestants.get(1);
	        
	        listContestants.clear();
	        for(Integer index = 0; index < 10; index++) {
	        	ELITE_EventContestant__c cont = ELITE_TestUtility.createContestant(event.Id, muiltiplayerAccountList.get(0).Id, index + 1, false);
	        	cont.Send_Email__c = true;
	        	listContestants.add(cont);
	        }
	        insert listContestants;
	      Test.stopTest();
    }
    static ELITE_EventContestant__c retrieveContestant(Id contestantId) {
        return [SELECT Contestant_Tier__c, Status__c FROM ELITE_EventContestant__c WHERE Id = :contestantId];
    }
}