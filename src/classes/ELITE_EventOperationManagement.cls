/**
	* Apex Class: ELITE_EventOperationManagement
	* Description: A Management class for trigger which update the eligible states 
	*							 to all states,if country is US on event of before insert/update 
	*							 of Contest.
	* Created By: Sudhir Kumar Jagetiya
	* Created Date: Sep 10, 2012
	*/
public without sharing class ELITE_EventOperationManagement {
	
	static final String ALL_STATES;
	static final String ELIGIBLE_COUNTRY_NAME;
	
	static {
		ALL_STATES = System.Label.ELITE_ELIGIBLE_STATES;
		ELIGIBLE_COUNTRY_NAME = 'The United States of America';
	}
	
	public static void beforeInsertUpdate(List<ELITE_EventOperation__c> newList,
																			Map<Id, ELITE_EventOperation__c> oldMap) {
		updateStates(newList, oldMap);
	}
	
	public static void afterUpdate(List<ELITE_EventOperation__c> newList,
																			Map<Id, ELITE_EventOperation__c> oldMap) {
		updateStatus(newList, oldMap);
	}
	
	private static void updateStates(List<ELITE_EventOperation__c> newList,
																			Map<Id, ELITE_EventOperation__c> oldMap) {
		Boolean isInsert = oldMap == null;
		Boolean isUpdate = oldMap != null;
		
		for(ELITE_EventOperation__c contest : newList) {
			
			if(contest.Eligible_Countries__c != null 
				&& contest.Eligible_Countries__c.Contains(ELIGIBLE_COUNTRY_NAME) 
				&& (isInsert || (isUpdate 
						&& ((oldMap.get(contest.Id).Eligible_Countries__c == null 
									|| !oldMap.get(contest.Id).Eligible_Countries__c.Contains(ELIGIBLE_COUNTRY_NAME)))))) {
				contest.Eligible_States__c = ALL_STATES;
			}
			
		}
	}
	
	private static void updateStatus(List<ELITE_EventOperation__c> newList,
																			Map<Id, ELITE_EventOperation__c> oldMap) {
																				
		Map<Id, ELITE_EventOperation__c> contestMap = new Map<Id, ELITE_EventOperation__c>();
		for(ELITE_EventOperation__c contest : newList) {
			
			if(contest.Contest_Status__c != oldMap.get(contest.Id).Contest_Status__c) {
				contestMap.put(contest.Id, contest);
			}
			
		}
		
		List<ELITE_EventContestant__c> contestantToBeUpdate = new List<ELITE_EventContestant__c>();
		for(ELITE_EventContestant__c contestant : [SELECT Contest_Status__c, 
																							  Event_Operation__c
																							  FROM ELITE_EventContestant__c
																							  WHERE Event_Operation__c IN :contestMap.keySet()]) {
																							  	
			
			system.debug('====> This is the error location 1: ' + contestant);
			contestant.Contest_Status__c = contestMap.get(contestant.Event_Operation__c).Contest_Status__c;
			contestantToBeUpdate.add(contestant);							
								  	
		}
		system.debug('====> This is the error location 2: ' + contestantToBeUpdate);
		
		if (contestantToBeUpdate.size() > 0) {
			update contestantToBeUpdate;
		}
	}
}