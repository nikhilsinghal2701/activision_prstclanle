/**
	* Apex Class: ELITE_EventOperationTriggerTest
	* Description: A Test class for trigger which update the eligible states 
	*							 to all states,if country is US on event of before insert/update 
	*							 of Contest.
	* Created By: Sudhir Kumar Jagetiya
	* Created Date: Sep 10, 2012
	*/
@isTest
private class ELITE_EventOperationTriggerTest {

    static testMethod void myUnitTest() {
       
       Contact con = ELITE_TestUtility.createGamer(false);
       con.Gamertag__c = 'record3';
       con.UCDID__c = '0003';
       con.Email = 'record3@gmail.com';
       insert con;
       
       Multiplayer_Account__c mAccount = ELITE_TestUtility.createMultiplayerAccount(con.Id, true);
       
       Test.startTest();
       	ELITE_EventOperation__c contest= ELITE_TestUtility.createContest(false);
       	contest.Eligible_Countries__c = 'The United States of America';
       	insert contest;
       	
       	system.assertEquals(getContest(contest.Id).Eligible_States__c, 
       											System.Label.ELITE_ELIGIBLE_STATES);
       											
       	ELITE_EventContestant__c contestant = ELITE_TestUtility.createContestant(contest.Id, mAccount.Id, 1, true);
       	
       	contest.Contest_Status__c = 'Live';
       	update contest; 
       	system.assertEquals(getContestant(contestant.Id).Contest_Status__c, 'Live');
       	
       Test.stopTest();
       
    }
    
    static ELITE_EventOperation__c getContest(Id contestId) {
    	
    	for(ELITE_EventOperation__c contest : [SELECT Eligible_Countries__c, 
    																							Eligible_States__c
    																			   FROM ELITE_EventOperation__c
    																			   WHERE Id = :contestId]) {
    		return contest;
    	}
    	
    	return null;
    }
    static ELITE_EventContestant__c getContestant(Id contestantId) {
    	
    	for(ELITE_EventContestant__c contestant : [SELECT Name, 
    																							Contest_Status__c
    																			   FROM ELITE_EventContestant__c
    																			   WHERE Id = :contestantId]) {
    		return contestant;
    	}
    	
    	return null;
    }
}