/**
    * Apex Class :  ELITE_GamerPrizeEmailReminderScheduler
    * Description: 
    * Created Date: 16 Nov 2012
    * Created By: Sudhir Kumar Jagetiya
    * Modified By: R. Washington
    */
global without sharing class ELITE_GamerPrizeEmailReminderScheduler implements Schedulable {
    
    //static final Integer scheduleInterval = Integer.valueOf(system.Label.ELITE_ScheduleInterval);
    //static final Integer scheduleIntervalMinutes = Integer.valueOf(system.Label.ELITE_ScheduleInterval_Minutes);
    
    static final String GAMER_PRIZE_STATUS_ACCEPTED = 'Accepted - Documentation Required';
    static final String GAMER_PRIZE_STATUS_NO_RESPONSE = 'No response - 1st Legal Doc Reminder';
    static Set<String> countries = new Set<String>{'The United States of America','United States'};                     
                                
    global void execute(SchedulableContext SC) {
        List<ELITE_GamerPrize__c> gamerPrizeToBeUpdate = new List<ELITE_GamerPrize__c>();
        List<ELITE_GamerPrize__c> gamerPrizeListForReminderEmail = [SELECT Document_Submission_Reminder_Email_Sent__c, 
                                                                                     Legal_Documentation_Reminder_Date__c
                                                                                     FROM ELITE_GamerPrize__c 
                                                                                     WHERE Document_Submission_Reminder_Email_Sent__c = false 
                                                                                     AND Legal_Documentation_Reminder_Date__c != null
                                                                                     AND Legal_Documentation_Reminder_Date__c <= :datetime.now()
                                                                                     AND isDocumentRecieved__c = false
                                                                                     AND Status__c = :GAMER_PRIZE_STATUS_ACCEPTED
                                                                                     AND Country__c IN :countries];
        
        for(ELITE_GamerPrize__c gamerPrize : gamerPrizeListForReminderEmail) {
            gamerPrize.Document_Submission_Reminder_Email_Sent__c = true;
            gamerPrizeToBeUpdate.add(gamerPrize);
        }
        
        List<ELITE_GamerPrize__c> gamerPrizeListForPrizeForfitureEmail = [SELECT Prize_Forfeiture_Email_Sent__c, 
                                                                                     Legal_Documentation_Forfeiture_Date__c
                                                                                     FROM ELITE_GamerPrize__c 
                                                                                     WHERE Prize_Forfeiture_Email_Sent__c = false 
                                                                                     AND Legal_Documentation_Forfeiture_Date__c != null
                                                                                     AND Legal_Documentation_Forfeiture_Date__c <= :datetime.now()
                                                                                     AND isDocumentRecieved__c = false
                                                                                     AND Status__c = :GAMER_PRIZE_STATUS_NO_RESPONSE
                                                                                     AND Id NOT IN :gamerPrizeListForReminderEmail];
        
        for(ELITE_GamerPrize__c gamerPrize : gamerPrizeListForPrizeForfitureEmail) {
            gamerPrize.Prize_Forfeiture_Email_Sent__c = true;
            gamerPrizeToBeUpdate.add(gamerPrize);
        }
        update gamerPrizeToBeUpdate;
        /*
        Datetime scheduleDateTime = DateTime.now().adddays(scheduleInterval);
        scheduleDateTime = scheduleDateTime.addMinutes(scheduleIntervalMinutes);
        String CRON_EXPRESSION = '0 ' + String.valueOf(scheduleDateTime.minute()) + ' ' +
                                                         String.valueOf(scheduleDateTime.hour()) + ' ' + 
                                                         String.valueOf(scheduleDateTime.day()) + ' ' +
                                                         String.valueOf(scheduleDateTime.month()) + ' ? ' + 
                                                         String.valueOf(scheduleDateTime.year());
        ELITE_GamerPrizeEmailReminderScheduler schObj = new ELITE_GamerPrizeEmailReminderScheduler();
        system.schedule('ELITE Document Reminder and Prize Forfeiture Email Scheduler ' + dateTime.now(), CRON_EXPRESSION, schObj); */
    }
    
    @isTest 
  public static void unitTests() {
    
    Contact contactGamer = ELITE_TestUtility.createGamer(true);
    Multiplayer_Account__c MultiplayerAccount = ELITE_TestUtility.createMultiplayerAccount(contactGamer.Id, true);
    ELITE_EventOperation__c contest = ELITE_TestUtility.createContest(true); 
    ELITE_GamerPrize__c gamerPrize = ELITE_TestUtility.createGamerPrize(contactGamer.Id, MultiplayerAccount.Id, false);
    gamerPrize.Legal_Documentation_Forfeiture_Date__c = datetime.now().addDays(-1);
    gamerPrize.Status__c = GAMER_PRIZE_STATUS_ACCEPTED;
    gamerPrize.Country__c = 'The United States of America';
    insert gamerPrize;
    String CRON_EXP = '0 0 0 16 11 ? 2022';
    
    Test.startTest();
        String jobId = System.schedule('ELITE Document Reminder and Prize Forfeiture Email SchedulerTest',  CRON_EXP, new ELITE_GamerPrizeEmailReminderScheduler());
        //CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];    
        //System.assertEquals(CRON_EXP, ct.CronExpression);
    Test.stopTest();
    
  }  
}