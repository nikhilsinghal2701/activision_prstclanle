/**
    * Apex Class: ELITE_GamerPrizeManagement 
    * Description: Utility Class to support trigger written on ELITE_GamerPrize__c
    * Created Date: 10-Sept-2012
    * Created By: Varun Vatsa
*/
public without sharing class ELITE_GamerPrizeManagement {

    static final String CONTEST_STATUS_FULFILLMENT_COMPLETE = 'Fulfillment Complete';
    static final String CONTEST_STATUS_FULFILLMENT_CLOSED = 'Fulfillment Closed - Prizes remaining';
    static final String GAMER_PRIZE_STATUS_PENDING_FULFILLMENT =  'Pending Fulfillment';
    static final String GAMER_PRIZE_STATUS_PENDING_ALTERNATE_FULFILLMENT =  'Pending Alternate Fulfillment';
    static final String ACCEPTED_DOCUMENT_REQUIRED =  'Accepted - Documentation Required';
    static final String CONTESTANT_STATUS_NO_RESPONSE = 'No response - 1st Email';
    static final String GAMER_PRIZE_STATUS_FORFEITED_NO_DOC_RECEIVED = 'Prize Forfeited-No Doc Received';          
    static final String CONTESTANT_STATUS_FORFEITED_NO_DOC_RECEIVED = 'Prize Forfeited-No Doc Rec\'d';    
		                                                     
    static Set<String> forfeitedStatus = new Set<String>{'Prize Forfeited-No Doc Received'};
    /*
        @author: Varun Vatsa
        @created date: 10-Sept-2012
        @description: Sets status of Contest related to a gamerPrize to "Fulfillment Closed
                                    Prizes" Remaining or "Fulfillment Complete", depending upon the number
                                    of gamerPrizes with status = "Delivered" is equal to the number of winners
                                    for the contest.
    */
    public static void setContestStatus(list<ELITE_GamerPrize__c> deliveredPrizes){
        
        list<ELITE_EventOperation__c> contestsToCheck = getContestOnGamerPrize(deliveredPrizes);
        
        list<ELITE_EventOperation__c> contestsToUpdate = new list<ELITE_EventOperation__c>(getContestsToUpdate(contestsToCheck));
        
        if(contestsToUpdate!=null && contestsToUpdate.size()>0){
            
            update contestsToUpdate;
        }
    }
    
    /*
        @author: Varun Vatsa
        @created date: 11-Sept-2012
        @description: Get Contest from GamerPrize-->TierPrize-->Tier-->Contest
    */
    public static list<ELITE_EventOperation__c> getContestOnGamerPrize(list<ELITE_GamerPrize__c> deliveredPrizes){
        
        list<id> tierPrizeId = new list<id>();
        list<id> contestId = new list<id>();
        list<ELITE_EventOperation__c> contests = new list<ELITE_EventOperation__c>();
        
        for(ELITE_GamerPrize__c gamerPrize:deliveredPrizes){
            tierPrizeId.add(gamerPrize.Tier_Prize__c);
        }
        
        for(ELITE_Tier_Prize__c tierPrize:[select id,Name,Tier__c,Tier__r.Event_Id__c 
                                                                             from ELITE_Tier_Prize__c 
                                                                             where id in:tierPrizeId]){
            contestId.add(tierPrize.Tier__r.Event_Id__c);                                                                       
        }
        
        for(ELITE_EventOperation__c contest:[select id,Name,Elite_Event_Id__c,Contest_Status__c,
                                                                                                Total_Winners__c,Total_Number_of_Contestants__c
                                                                                 from ELITE_EventOperation__c
                                                                                 where id in:contestId ]){
            contests.add(contest);                                                                      
        }
        
        return contests;
    }
    
    /*
        @author: Varun Vatsa
        @created date: 11-Sept-2012
        @description: Get Contest to update basis the number of Gamers with delivered 
                                    Status and number of winners for that Contest.
        @return: list<ELITE_EventOperation__c>
    */
    public static set<ELITE_EventOperation__c> getContestsToUpdate(list<ELITE_EventOperation__c> contests){
        
        list<id> tierIds = getTierIdsOnContests(contests);
        set<ELITE_EventOperation__c> contestsToUpdate = new set<ELITE_EventOperation__c>();
        
        for(ELITE_Tier_Prize__c tierPrize:[Select e.Tier__r.Id, e.Tier__c,Tier__r.Event_Id__c,
                                                                                            Tier__r.Event_Id__r.Total_Winners__c,
                                                                                            Tier__r.Event_Id__r.Contest_Status__c,
                                                                                            Tier__r.Event_Id__r.Name,
                                                                                            (Select Id, IsDeleted, Name, CreatedDate,
                                                                                                     CreatedById, LastModifiedDate, LastModifiedById,
                                                                                                     SystemModstamp, LastActivityDate, Status__c,
                                                                                                     Tracking_Number__c, Contact_Id__c, Tier_Prize__c,
                                                                                                     Address_1__c, Address_2__c, City__c, State__c,
                                                                                                     Zip_Code__c, Country__c, First_Name__c,
                                                                                                     Last_Name__c, Special_Fullfilment__c, Notes__c,
                                                                                                     Carrier__c, Multiplayer_Account__c, Value__c,
                                                                                                     Date_Shipped__c
                                                                                          From Gamer_Prizes__r
                                                                                          where Status__c ='Delivered')
                                                                                From ELITE_Tier_Prize__c e
                                                                                where e.Tier__c in :tierIds]){
            
            ELITE_EventOperation__c contest = new ELITE_EventOperation__c(id = tierPrize.Tier__r.Event_Id__c,
                                                                Name =tierPrize.Tier__r.Event_Id__r.Name);
            
            if(tierPrize.Tier__r!=null && tierPrize.Tier__r.Event_Id__r!=null && tierPrize.Gamer_Prizes__r!=null){
                
                if(tierPrize.Tier__r.Event_Id__r.Total_Winners__c == tierPrize.Gamer_Prizes__r.size()){
                    contest.Contest_Status__c = CONTEST_STATUS_FULFILLMENT_COMPLETE;
                }else{
                    contest.Contest_Status__c = CONTEST_STATUS_FULFILLMENT_CLOSED;
                }       
            }
                    contestsToUpdate.add(contest);
      }
      
      return contestsToUpdate;
    }
    
    /*
        @author: Varun Vatsa
        @created date: 11-Sept-2012
        @description: Get  list of TierIds to be used in fetching Contest
                                    and TierPrize Map.
        @return: list<id> tierIDs
    */
    public static list<id> getTierIdsOnContests(list<ELITE_EventOperation__c> contests){
        
        list<id> tierIds = new list<id>();
        
        for(ELITE_EventOperation__c contest:[Select (Select Id From Tiers__r) 
                                                                                 From ELITE_EventOperation__c e
                                                                                 where e.id in:contests]){
            for(ELITE_Tier__c tier:contest.Tiers__r){
                tierIds.add(tier.id);
            }                                               
        }
     
      return tierIds;       
    }
    
    /*
        @author: Varun Vatsa
        @created date: 5-Oct-2012
        @description: Sets status of Contest related to a gamerPrize to "Fulfillment Closed
                                    Prizes" Remaining or "Fulfillment Complete", depending upon the number
                                    of gamerPrizes with status = "Delivered" is equal to the number of winners
                                    for the contest.
    */
    public static map<id,String> setGamerPrizeStatus(
                                                        list<ELITE_GamerPrize__c> readyForFulfillmentPrizes){
        map<id,String> mapToReturn = new map<id,String>();
        //map<id,id> tierIdgpIdMap = new map<id,id>();  
            Set<Id> tierPrizeIds = new Set<Id>();
            for(ELITE_GamerPrize__c gamerPrize: readyForFulfillmentPrizes){
                tierPrizeIds.add(gamerPrize.Tier_Prize__c);
            }
            
            for(ELITE_Tier_Prize__c eTier: [SELECT Tier__r.Type__c, Tier__c, Id 
                                                                FROM ELITE_Tier_Prize__c 
                                                                WHERE Id IN :tierPrizeIds ]){
                mapToReturn.put(eTier.Id,string.valueOf(eTier.Tier__r.Type__c));
            }
        return mapToReturn;
    }
    
    /*
        @author: Varun Vatsa
        @created date: 11-Oct-2012
        @description: Sets Values for total prize money won by a contestant in a Calendar Year.
    */
    public static void setToalPrizeValue(Set<Id> multiPlayerAccountIds){
        Set<String> countries = new Set<String>{'The United States of America','United States'};
        Set<Id> processedRecordIds = new Set<Id>();
        List<Multiplayer_Account__c> multiplayerAccountList = new List<Multiplayer_Account__c>();
        Id multiplayerAccountId;
        system.debug('multiplayerAccountId '+multiplayerAccountId);
        for(AggregateResult gamerPrize:[SELECT SUM(value__c) totalVal,Multiplayer_Account__c 
                                                                                FROM ELITE_GamerPrize__c
                                                                                WHERE Multiplayer_Account__c IN :multiPlayerAccountIds 
                                                                                            AND CreatedDate = THIS_YEAR 
                                                                                            AND Country__c IN :countries
                                                                                            AND Status__c NOT IN :forfeitedStatus
                                                                                GROUP BY Multiplayer_Account__c]){
            multiplayerAccountId = Id.valueOf(gamerPrize.get('Multiplayer_Account__c')+'');
            system.debug('multiplayerAccountId '+multiplayerAccountId);
            if(multiplayerAccountId != null && gamerPrize.get('totalVal') != null){
                processedRecordIds.add(multiplayerAccountId);
                multiplayerAccountList.add(new Multiplayer_Account__c(Id = multiplayerAccountId,
                                                                        YTD_Account_Prize_Total__c = Decimal.valueOf(gamerPrize.get('totalVal')+'')));

            }                                                               
        }
        boolean isContainsExtraRecord = multiPlayerAccountIds.removeAll(processedRecordIds);
        for(Id accId : multiPlayerAccountIds) {
            multiplayerAccountList.add(new Multiplayer_Account__c(Id = accId,
                            YTD_Account_Prize_Total__c = null));
        }
        update multiplayerAccountList;
    }
    
    public static Map<id,Multiplayer_Account__c> getContestantToalPrizeValue(Set<Id> multiplayerAccountIds) {
        Map<id,Multiplayer_Account__c> multiplayerAccountMap = new Map<id,Multiplayer_Account__c>();
        for(Multiplayer_Account__c multiplayerAccount :[SELECT Id, Contact__r.YTD_Gamer_Prize_Total__c
                                                        FROM Multiplayer_Account__c
                                                        WHERE Id IN :multiplayerAccountIds
                                                        AND Contact__c != null]) {
                multiplayerAccountMap.put(multiplayerAccount.Id,multiplayerAccount);                                                                                    
        }
        
        return multiplayerAccountMap;
    }
    
    public static void updateContestantStatus(Map<Id, ELITE_GamerPrize__c> newMap, Map<Id, ELITE_GamerPrize__c> oldMap) {
        Boolean isInsert = oldMap == null;
        Boolean isUpdate = newMap != null && oldMap != null;
        Map<Id, ELITE_EventContestant__c> contestantToBeUpdate = new Map<Id, ELITE_EventContestant__c>();
        for(ELITE_GamerPrize__c gamerPrize : newMap.values()) {
            if(gamerPrize.Contestant__c != null && gamerPrize.Status__c != null 
                                    && gamerPrize.Status__c.equalsIgnoreCase(GAMER_PRIZE_STATUS_FORFEITED_NO_DOC_RECEIVED) && 
                                    (isInsert || (isUpdate && (gamerPrize.Status__c != oldMap.get(gamerPrize.Id).Status__c)))) {
                   if(!contestantToBeUpdate.containsKey(gamerPrize.Contestant__c))
                    contestantToBeUpdate.put(gamerPrize.Contestant__c, new ELITE_EventContestant__c(Id = gamerPrize.Contestant__c, Status__c = CONTESTANT_STATUS_FORFEITED_NO_DOC_RECEIVED));                    
            }
        }
        update contestantToBeUpdate.values(); 
    }
    
    
    public static void setSpecialFulFillmentFlag(List<ELITE_GamerPrize__c> newList, Map<Id, ELITE_GamerPrize__c> oldMap) {
        Boolean isInsert = oldMap == null;
        Boolean isUpdate = newList != null && oldMap != null;
        List<ELITE_GamerPrize__c> gamerPrizeList = new List<ELITE_GamerPrize__c>();
        Set<Id> tierPrizeIds = new Set<Id>();
        for(ELITE_GamerPrize__c gamerPrize : newList) {
            if(gamerPrize.Tier_Prize__c != null && (isInsert || (isUpdate && (gamerPrize.Tier_Prize__c != oldMap.get(gamerPrize.Id).Tier_Prize__c)))) {
                tierPrizeIds.add(gamerPrize.Tier_Prize__c);
                gamerPrizeList.add(gamerPrize);
            }
        }
        Map<Id, Boolean> specialFulfillmentFlagMap = new Map<Id, Boolean>();
        for(ELITE_Tier_Prize__c tierPrize : [SELECT Prize__r.Special_Fulfillment__c, Prize__c, 
                                                    Bundle__r.Special_Fulfillment__c, Bundle__c 
                                             FROM ELITE_Tier_Prize__c
                                             WHERE Id IN :tierPrizeIds] ) {
            if(tierPrize.Prize__c != null) {
                specialFulfillmentFlagMap.put(tierPrize.Id, tierPrize.Prize__r.Special_Fulfillment__c);
            } else if(tierPrize.Bundle__c != null){
                specialFulfillmentFlagMap.put(tierPrize.Id, tierPrize.Bundle__r.Special_Fulfillment__c);
            }
        }
        
        for(ELITE_GamerPrize__c gamerPrize : gamerPrizeList) {
            if(specialFulfillmentFlagMap.containsKey(gamerPrize.Tier_Prize__c)) {
                gamerPrize.Special_Fullfilment__c = specialFulfillmentFlagMap.get(gamerPrize.Tier_Prize__c);
            }
        }
    }
    
}