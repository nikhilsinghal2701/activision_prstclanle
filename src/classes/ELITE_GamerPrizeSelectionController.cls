/**
    * Apex Class: ELITE_GamerPrizeSelectionController
    * Description: A controller for ELITE_GamerPrizeSelection component 
    *              that is used to provide the User interface for prize selection and update shipping address.
    * Created Date: 1 August 2012
    * Created By: Sudhir Kr. Jagetiya
    * Modified by: Rita Washington 10/12/2012
    */
public without sharing class ELITE_GamerPrizeSelectionController {
    static final String GAMER_PRIZE_STATUS_PENDING_ACCEPTANCE = '   Pending Acceptance';
    static final String DML_EXCEPTION_ERROR_MSG = 'Error: Record not saved.Please contact to system administrator';
     
    //Added by  Rita Washington 10/12/2012 - so that prizes are only displayed for the appropriate Contestants
    static final String CONTESTANT_STATUS_DISQUALIFIED = 'Disqualified';
    static final String CONTESTANT_STATUS_PRIZE_FORFIETED_NO_DOC_RECD = 'Prize Forfeited-No Doc Rec\'d';
    static final String CONTESTANT_STATUS_PRIZE_FORFIETED_NO_RESPONSE = 'Prize Forfeited-No Response';    
    
    public ELITE_EventContestant__c eventContestant {get;set;}
    public ELITE_GamerPrize__c gamerPrize {get;set;} 
    public List<BundleAndPrizeWrapper> bundleAndPrizeWrapperList {get;set;}
    
    public Boolean isSuccess {get;set;}
    public Boolean isApplicableForPrize {get; set;}
    public Boolean isCurrentUserPage {get; set;}
    public String statusMessage {get;set;}
    public Integer selectedIndex {get;set;}
    
    String eventContestantId;
    Map<Id,Id> bundlePrizeAndTierPrizeIdMap;
    List<ELITE_GamerPrize__c> gamerPrizeList;
    List<User> currentUserList;
    Set<String> countries;
    
    //Constructor
    public ELITE_GamerPrizeSelectionController() {
        isApplicableForPrize = false;
        isCurrentUserPage = false;
        statusMessage = '';
        countries = new Set<String>{'The United States of America','United States'};
        eventContestantId = ApexPages.currentPage().getParameters().get('Id');
        
        init(eventContestantId);
    }
    //----------------------------------------------------------------------------------------------------------------------------------------
    // initilaize 
    //----------------------------------------------------------------------------------------------------------------------------------------
    private void init(String eventContestantId) {
    		
        selectedIndex = 0;
        gamerPrize = new ELITE_GamerPrize__c();
        bundlePrizeAndTierPrizeIdMap = new Map<Id,Id>();
        bundleAndPrizeWrapperList = new List<BundleAndPrizeWrapper>();
        Set<Id> bundleIds = new Set<Id>();
        Set<Id> prizeIds = new Set<Id>();
        currentUserList = [SELECT ContactId, Contact.Elite_Status__c FROM User WHERE ContactId != null AND Id = :UserInfo.getUserId()];
	    	if(currentUserList.size() > 0 && currentUserList.get(0).ContactId != null) {
	        for(ELITE_EventContestant__c eContestant : [SELECT Status__c, Rank__c, Score__c, Event_Operation__r.Name, Prize_Rank__c,
	                                                           Event_Operation__c, Event_Operation__r.End_Date__c, Email__c,
	                                                           Event_Operation__r.Eligible_Countries__c,
	                                                           ELITE_Platform__r.Contact__r.Elite_Status__c, 
	                                                           Prize_Claimed__c, ELITE_Platform__c,
	                                                           ELITE_Platform__r.Gamertag__c, 
	                                                           ELITE_Platform__r.Contact__c,
	                                                           ELITE_Platform__r.Contact__r.Address_Line_1__c,
	                                                           ELITE_Platform__r.Contact__r.Address_Line_2__c,
	                                                           ELITE_Platform__r.Contact__r.City__c,
	                                                           ELITE_Platform__r.Contact__r.Country__c,
	                                                           ELITE_Platform__r.Contact__r.State__c,
	                                                           ELITE_Platform__r.Contact__r.Zipcode__c,
	                                                           ELITE_Platform__r.Contact__r.Name, 
	                                                           ELITE_Platform__r.Contact__r.Phone,
	                                                           ELITE_Platform__r.Contact__r.MobilePhone,
	                                                           Event_Operation__r.Elite_Event_Id__c,
	                                                           Forfeiture_Date__c,
	                                                           Contestant_Tier__c
	                                                    FROM ELITE_EventContestant__c
	                                                    WHERE Encrypted_Id__c = :eventContestantId limit 1]) {
	
	            eventContestant = eContestant;
	        }
	        if(eventContestant != null && eventContestant.ELITE_Platform__r.Contact__c == currentUserList.get(0).ContactId) {
	        		isCurrentUserPage = true;
	            if(CONTESTANT_STATUS_DISQUALIFIED.equals(eventContestant.Status__c)) {
	            	statusMessage = System.Label.ELITE_Disqualified_Msg;
	            } 
	            else if(CONTESTANT_STATUS_PRIZE_FORFIETED_NO_DOC_RECD.equals(eventContestant.Status__c)) {
	            	statusMessage = System.Label.ELITE_Prize_Forfeited_No_Doc_Rec_Msg1;
	            } 
	            else if(CONTESTANT_STATUS_PRIZE_FORFIETED_NO_RESPONSE.equals(eventContestant.Status__c)) {
	            	String formattedDate = eventContestant.Forfeiture_Date__c != null ? eventContestant.Forfeiture_Date__c.format('MMM dd, yyyy') : '';
	            	statusMessage = System.Label.ELITE_Prize_Forfeited_No_Response_Msg1 + ' ' + formattedDate + '. ' + System.Label.ELITE_Prize_Forfeited_Msg2;
	            } 
	            else {
	            	isApplicableForPrize = true;
								gamerPrize = populateGamerShippingAddress(eventContestant);
		            gamerPrizeList = new List<ELITE_GamerPrize__c>();
		            for(ELITE_GamerPrize__c gPrize : [SELECT Zip_Code__c, State__c, Country__c, 
		                                                     City__c, Phone__c, Name__c,
		                                                     Address_2__c, Address_1__c, 
		                                                     Value__c, LastModifiedDate
		                                            FROM ELITE_GamerPrize__c 
		                                            WHERE Multiplayer_Account__c = :eventContestant.ELITE_Platform__c
		                                            AND Contestant__c = :eventContestant.Id
		                                            ORDER By LastModifiedDate DESC]) {
		                gamerPrizeList.add(gPrize);
		            } 
	            
		            //Updated by Rita Washington 10/12/2012 - so that prizes are only displayed for the appropriate Contestants
		            if((!eventContestant.Prize_Claimed__c) 
		                && (eventContestant.Status__c != CONTESTANT_STATUS_DISQUALIFIED) 
		                && (eventContestant.Status__c != CONTESTANT_STATUS_PRIZE_FORFIETED_NO_DOC_RECD ) 
		                && (eventContestant.Status__c != CONTESTANT_STATUS_PRIZE_FORFIETED_NO_RESPONSE )) {
		                List<ELITE_Prize__c> prizeList = new List<ELITE_Prize__c>();
		                for(ELITE_Tier_Prize__c tierPrize : [SELECT Id, Tier__r.Event_Id__c, Tier__c, Prize__c, Bundle__c,
		                                                              Prize__r.Name, Prize__r.Value__c, Prize__r.Image__c,
		                                                              Prize__r.Description__c,
		                                                              Tier__r.Last_Position__c, Tier__r.First_Position__c
		                                                         FROM ELITE_Tier_Prize__c 
		                                                         WHERE Tier__c = :eventContestant.Contestant_Tier__c
		                                                         ORDER BY Prize__r.Name]) {
		                    if(tierPrize.Prize__c != null) prizeList.add(tierPrize.Prize__r); 
		                    bundleIds.add(tierPrize.Bundle__c); 
		                    if(tierPrize.Prize__c != null && !bundlePrizeAndTierPrizeIdMap.containsKey(tierPrize.Prize__c)) {
		                    bundlePrizeAndTierPrizeIdMap.put(tierPrize.Prize__c, tierPrize.Id);
		                    }           
		                    if(tierPrize.Bundle__c != null && !bundlePrizeAndTierPrizeIdMap.containsKey(tierPrize.Bundle__c)) {
		                    bundlePrizeAndTierPrizeIdMap.put(tierPrize.Bundle__c, tierPrize.Id);
		                    }    
		                }
		                populateAvailablePrizeList(bundleIds, prizeList);
		            } else {
		                if(gamerPrizeList.size() > 0) {
		                    gamerPrize = gamerPrizeList.get(0);
		                }
		            }            	
	          	}
	        } 
	     }
    }
    
    private ELITE_GamerPrize__c populateGamerShippingAddress(ELITE_EventContestant__c contestant) {
        ELITE_GamerPrize__c gamerPrize = new ELITE_GamerPrize__c();
        gamerPrize.Address_1__c = contestant.ELITE_Platform__r.Contact__r.Address_Line_1__c;
        gamerPrize.Address_2__c = contestant.ELITE_Platform__r.Contact__r.Address_Line_2__c;
        gamerPrize.City__c = contestant.ELITE_Platform__r.Contact__r.City__c;
        gamerPrize.Country__c = contestant.ELITE_Platform__r.Contact__r.Country__c;
        gamerPrize.State__c = contestant.ELITE_Platform__r.Contact__r.State__c;
        gamerPrize.Name__c = contestant.ELITE_Platform__r.Contact__r.Name;
        gamerPrize.Zip_Code__c = contestant.ELITE_Platform__r.Contact__r.Zipcode__c;
        gamerPrize.Phone__c = contestant.ELITE_Platform__r.Contact__r.Phone != null ?
                                                    contestant.ELITE_Platform__r.Contact__r.Phone : 
                                                    contestant.ELITE_Platform__r.Contact__r.MobilePhone;
        return gamerPrize;
    }
    
    //----------------------------------------------------------------------------------------------------
    // To retrieve the data and set the page size 
    //----------------------------------------------------------------------------------------------------    
    private void populateAvailablePrizeList(Set<Id> bundleIds, List<ELITE_Prize__c> prizeList) {
       Integer index = 0;
       for(ELITE_Bundle__c bundle : [SELECT Id, Name, Description__c, Image__c, 
                                                 (SELECT Prize__c, Prize_Name__c, Value__c 
                                                    FROM Prize_Bundles__r ORDER By Prize_Name__c) 
                                         FROM ELITE_Bundle__c 
                                         WHERE Id IN :bundleIds ORDER BY Name]) {
        if(bundle.Prize_Bundles__r.size() > 0) {                                                                
            bundleAndPrizeWrapperList.add(new BundleAndPrizeWrapper(index, null, true, bundle, bundle.Prize_Bundles__r));
            index++;
        }
       }
       for(ELITE_Prize__c prize : prizeList) {
        bundleAndPrizeWrapperList.add(new BundleAndPrizeWrapper(index, prize, false, null, null));
        index++;
       }
    }
 
    
    //---------------------------------------------------------------------------------------------------
    //Action methods To save Reords
    //---------------------------------------------------------------------------------------------------
    public PageReference saveRecords() { 
        Savepoint savePoint;
        List<ELITE_GamerPrize__c> gamerPrizeToBeInsert = new List<ELITE_GamerPrize__c>();
        try {
            savePoint = Database.setSavepoint(); 
            if(eventContestant != null && eventContestant.ELITE_Platform__c != null && eventContestant.ELITE_Platform__r.Contact__c != null) {                
            
            BundleAndPrizeWrapper wrapperObject = bundleAndPrizeWrapperList.get(selectedIndex);
            if(wrapperObject.isBundle && bundlePrizeAndTierPrizeIdMap.containsKey(wrapperObject.bundle.Id)) {
                gamerPrizeToBeInsert.add(createGammerPrize(eventContestant, gamerPrize, 
                   bundlePrizeAndTierPrizeIdMap.get(wrapperObject.bundle.Id), wrapperObject.value));
            } else if(bundlePrizeAndTierPrizeIdMap.containsKey(wrapperObject.prize.Id)){
                gamerPrizeToBeInsert.add(createGammerPrize(eventContestant, gamerPrize, 
                   bundlePrizeAndTierPrizeIdMap.get(wrapperObject.prize.Id), wrapperObject.value));
            }
            if(gamerPrizeToBeInsert.size() > 0) {
                eventContestant.Prize_Claimed__c = true;
                eventContestant.Contestant_Responded__c = true;
                insert gamerPrizeToBeInsert;
            }
            update eventContestant;
            isSuccess = true;
             }
          } catch(Exception ex) {
                eventContestant.Prize_Claimed__c = false;
                isSuccess = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , DML_EXCEPTION_ERROR_MSG));
                Database.rollback(savePoint);
        }
        return null;    
    }
    
    private ELITE_GamerPrize__c createGammerPrize(ELITE_EventContestant__c eContestant, ELITE_GamerPrize__c gPrize, 
                                     Id tierPrizeId, Double value) {
        Double totalAmount = 0;
        ELITE_GamerPrize__c egPrize = new ELITE_GamerPrize__c();
        egPrize.Contact_Id__c = eContestant.ELITE_Platform__r.Contact__c;
        egPrize.Multiplayer_Account__c = eContestant.ELITE_Platform__c;
        egPrize.Contestant__c = eContestant.Id;
        egPrize.Email__c = eContestant.Email__c;
        egPrize.Zip_Code__c = gPrize.Zip_Code__c;
        egPrize.State__c = gPrize.State__c;
        egPrize.Country__c = gPrize.Country__c;
        egPrize.City__c = gPrize.City__c;
        egPrize.Address_2__c = gPrize.Address_2__c;
        egPrize.Address_1__c = gPrize.Address_1__c;
        egPrize.Name__c = gPrize.Name__c;
        egPrize.Phone__c = gPrize.Phone__c;
        egPrize.Tier_Prize__c = tierPrizeId;
        egPrize.Value__c = value;
        egPrize.Status__c = GAMER_PRIZE_STATUS_PENDING_ACCEPTANCE;
        return egPrize;
    }
    
    private String formatNumber(Integer num) {
        List<String> ordinalList = new List<String>{'th','st','nd','rd','th','th','th','th','th','th'};
            if (math.mod(num, 100) >= 11 && math.mod(num, 100) <= 13)
               return  num + ordinalList.get(0);
            else
              return  num + ordinalList.get(math.mod(num, 10));
    }
    
    public String rank {
        get {
            if(eventContestant != null) {
                return formatNumber(Integer.valueOf(eventContestant.Rank__c));
            }
            return '';
        }   
    }
    
    public class BundleAndPrizeWrapper {
        public Integer index {get;set;}
        public ELITE_Prize__c prize {get;set;}
        public boolean isBundle {get; set;}
        public ELITE_Bundle__c bundle {get; set;}
        public Double value;
        
        public BundleAndPrizeWrapper(Integer index, ELITE_Prize__c prize, boolean isBundle, ELITE_Bundle__c bundle, List<ELITE_PrizeCatalog__c> prizeList) {
            this.index = index;
            this.prize = prize; 
            this.isBundle = isBundle;
            if(isBundle) {
                this.bundle = bundle;
                this.value = 0;
                for(ELITE_PrizeCatalog__c prizeCatalog : prizeList) {
                this.value += prizeCatalog.Value__c != null ? prizeCatalog.Value__c : 0;
                }
            } else {
                this.value = prize.Value__c != null ? prize.Value__c : 0;
            }
        }
    }
}