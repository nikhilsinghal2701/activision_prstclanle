/**
    * Apex Class: ELITE_GamerPrizeSelectionController
    * Description: created to validte functionality of ELITE_GamerPrizeSelectionController class
    * Created Date: 06-Sep-2012
    * Created By: Deepak Sharma
    * Last Modified By : Kirti Agarwal
    */
@isTest(seeAllData = false)
private class ELITE_GamerPrizeSelectionControllerTest {
    static Integer NUM_OF_RECORDS = 8;
    static final String PORTAL_PROFILE_NAME = 'ATVI OHV Cust Port User';
    
     static testMethod void gamerPrizeSelectionControllerTest() {
    	
    	//Create test data
        Contact contactGamer = ELITE_TestUtility.createGamer(true);
        
        List<Profile> profiles = [SELECT Id
																FROM Profile 
																WHERE Name = :PORTAL_PROFILE_NAME LIMIT 1];
	    
	    	system.assert(profiles.size() > 0);
      	User user = ELITE_TestUtility.createPortalUser('Test1', profiles.get(0).Id, contactGamer.Id, true);
        
        ELITE_EventOperation__c event = ELITE_TestUtility.createContest(true);
        
        Multiplayer_Account__c MultiplayerAccount = ELITE_TestUtility.createMultiplayerAccount(contactGamer.Id, true);
        
        ELITE_GamerPrize__c gamerPrize = ELITE_TestUtility.createGamerPrize(contactGamer.Id, MultiplayerAccount.Id, true);
        
        ELITE_Prize__c prize = ELITE_TestUtility.createPrize('testtPrizeName', 5000, true);
       
        ELITE_Bundle__c bundle = ELITE_TestUtility.createBundle('testBundleName', true);
        
        ELITE_PrizeCatalog__c prizeBundle = ELITE_TestUtility.createPrizeBundle('test prize bundle', bundle.Id , prize.Id, true);
        
        ELITE_EventOperation__c contest = ELITE_TestUtility.createContest(true); 
        
        ELITE_Tier__c tier = ELITE_TestUtility.createTier(contest.Id, 1, 5, true);
        
        
        //Valiation Rule is there: You can select either Bundle or Prize.
        ELITE_Tier_Prize__c tierBundle = ELITE_TestUtility.createTierPrize('testTierPrizeName', tier.Id, bundle.id,null, true);
       
        List<ELITE_EventContestant__c> listContestants = new List<ELITE_EventContestant__c>();
        for(Integer sNo = 0; sNo < NUM_OF_RECORDS; sNo++) {
            listContestants.add(ELITE_TestUtility.createContestant(contest.Id, MultiplayerAccount.Id, sNo + 1, false));
        }
        insert listContestants;
    	
    	ELITE_EventContestant__c contestant = [SELECT Id, Encrypted_Id__c FROM ELITE_EventContestant__c WHERE Id = :listContestants.get(0).Id limit 1];
    	
    	 system.runAs(user) { 
	    	ApexPages.currentPage().getParameters().put('Id',contestant.Encrypted_Id__c);
	      ELITE_GamerPrizeSelectionController ctrl = new ELITE_GamerPrizeSelectionController();
	    	ctrl.saveRecords();
    	}
    }
    
}