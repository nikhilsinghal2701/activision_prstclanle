/**
    * Apex Class: ELITE_GamerPrizeTriggerTest
    * Description: created to validte functionality of ELITE_GamerPrizeTrigger Trigger
    * Created Date: 13-Sep-2012
    * Created By: Kirti Agarwal
    */
@isTest(seeAllData = false)
private with sharing class ELITE_GamerPrizeTriggerTest {
	    
	    static testMethod void ELITE_GamerPrizeTriggerTest1() {
	    	
	        Contact contactGamer = ELITE_TestUtility.createGamer(true);
	        
	        ELITE_EventOperation__c event = ELITE_TestUtility.createContest(true);
	        
	        Multiplayer_Account__c MultiplayerAccount = ELITE_TestUtility.createMultiplayerAccount(contactGamer.Id, true);
	        
	        ELITE_Prize__c prize = ELITE_TestUtility.createPrize('testtPrizeName', 5000, true);
	        
	        ELITE_EventOperation__c contest = ELITE_TestUtility.createContest(true); 
	       
	        ELITE_Tier__c tier = ELITE_TestUtility.createTier(contest.Id, 1, 5, true);
	        
	        ELITE_Tier_Prize__c tierPrize = ELITE_TestUtility.createTierPrize('testTierPrizeName', tier.Id, null, prize.Id, true);
	       
	        
	        ELITE_GamerPrize__c gamerPrize = ELITE_TestUtility.createGamerPrize(contactGamer.Id, MultiplayerAccount.Id, true);
	        gamerPrize.Status__c = 'Pending Fulfillment';
	        gamerPrize.Tier_Prize__c = tierPrize.id;
	        update gamerPrize;
	        
	        gamerPrize.Status__c = 'Delivered';
	        update gamerPrize;
	        
	        ELITE_EventOperation__c updatedcontest = [select id,Contest_Status__c from ELITE_EventOperation__c where id =:contest.id limit 1];
	        system.assertEquals(updatedcontest.Contest_Status__c,'Fulfillment Closed - Prizes remaining');
	    }
}