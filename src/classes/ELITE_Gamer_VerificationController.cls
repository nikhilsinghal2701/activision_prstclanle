/**
    * Apex Class: ELITE_Gamer_VerificationController
    * Description: A controller for ELITE_Gamer_Verification page 
    *              that is used to provide the User interface for for mass update of Contestants Status.
    * Created Date: 9 August 2012
    * Created By: Sudhir Kr. Jagetiya
    */
public without sharing class ELITE_Gamer_VerificationController {
    public static final String CONTESTANT_STATUS_PARTICIPANT {get;set;}
    public static final String CONTESTANT_STATUS_DISQUALIFIED {get;set;}
    public static final String CONTESTANT_STATUS_ALTERNATE_VERFIED {get;set;}
    public static final String CONTESTANT_STATUS_WINNER_VERFIED {get;set;}
    public static final String CONTEST_STATUS_ON_SUBMIT {get;set;}
    public static final String CONTEST_STATUS_ON_APPROVE {get;set;}
    
    static final String DML_EXCEPTION_ERROR_MSG;
    static final String TIER_ALTERNATE;
    static final String TIER_WINNER;
    static final String CONTESTANT_STATUS_ALTERNATE_UNVERFIED;
    static final String CONTESTANT_STATUS_WINNER_UNVERFIED;
    
    
    
    
    //No. of records to display per page
    static final Integer DEFAULT_RESULTS_PER_PAGE;
    static final Integer DEFAULT_LINKS_AT_A_TIME;
    static final Integer DEFAULT_NUMBER_OF_LINKS_BEFORE_CURRENT;
    
    static {
        CONTESTANT_STATUS_PARTICIPANT = 'Participant';
        CONTESTANT_STATUS_ALTERNATE_UNVERFIED = 'Alternate - unverified';
        CONTESTANT_STATUS_ALTERNATE_VERFIED = 'Alternate - verified';
        CONTESTANT_STATUS_WINNER_UNVERFIED = 'Winner - Unverified';
        CONTESTANT_STATUS_WINNER_VERFIED = 'Winner - verified';
        CONTESTANT_STATUS_DISQUALIFIED = 'Disqualified';
        
        DML_EXCEPTION_ERROR_MSG = 'Error: Record not saved.Please contact to system administrator'; 
        
        TIER_ALTERNATE = 'Alternate';
        TIER_WINNER = 'Winner';
        
        CONTEST_STATUS_ON_SUBMIT = 'Pending Approval';
        CONTEST_STATUS_ON_APPROVE = 'Approved - In Fulfillment';
        
        DEFAULT_RESULTS_PER_PAGE = 20;
        DEFAULT_LINKS_AT_A_TIME = 10;
        DEFAULT_NUMBER_OF_LINKS_BEFORE_CURRENT = 4;
        
    }
    
    
    public ELITE_EventOperation__c contest {get;set;}
    
    public List<PrizeAndAlternateWrapper> prizeList {get;set;}
    public List<ContestantWrapper> contestants {get;set;}
    public List<List<ContestantWrapper>> allResults;
    
    
    public Integer selectedPageNumber {get;set;}
    public Integer selectedRecordNumber {get;set;}
    public Integer numOfAlternates {get;set;}
    public Integer totalRecords {get;set;}
    
    Id eventId;
    List<ContestantWrapper> contestantsList;
    List<ELITE_Tier__c> tierList;
    List<ELITE_EventOperation__c>  eventsList;
    Integer numOfTiers, numOfWinners, numOfWinnerTier, numOfAlternateTier;
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Constructor
    //----------------------------------------------------------------------------------------------------------------------------------------
    public ELITE_Gamer_VerificationController() {
            contestantsList = new List<ContestantWrapper>();
            selectedPageNumber = 1;
            eventId = ApexPages.currentPage().getParameters().get('Id');
            init();
    }
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Initialize Block
    //----------------------------------------------------------------------------------------------------------------------------------------
   
    private void init() {
        contestants = new List<ContestantWrapper>();
        eventsList = new List<ELITE_EventOperation__c>();
        eventsList = [SELECT Type__c, Total_Winners__c, Total_Alternates__c,
                                                 Max_Alternates__c, Max_Winners__c,
                                                 Elite_Event_Id__c, Platform__c, Contest_Status__c, 
                             Total_Number_of_Contestants__c, Prize__c, Name 
                    FROM ELITE_EventOperation__c 
                    WHERE Id = :eventId];
        if(eventsList.size() > 0) {
            contest = eventsList.get(0);
            
            numOfWinners = Integer.valueOf(contest.Total_Winners__c == null ? 0 : contest.Total_Winners__c);
            //Populate Prize List
            populatePrizeList(contest.Id);
            //populate Contestants
            populateContestants(contest.Id);
        }
    }
    public Double numOfPercentVerified {
            get {
            if(numOfTiers != null && numOfTiers != 0) {
                Double tempPercent = (numOfWinners + numOfAlternates) * 100 / numOfTiers;
                return tempPercent > 100 ? 100 : tempPercent;
            }
            else {
                return 0.0;
            }
         }
         set;
    }
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Method that is used to populate Prize List
    //----------------------------------------------------------------------------------------------------------------------------------------
    private void populatePrizeList(Id eventId) {
        numOfTiers = numOfWinnerTier = numOfAlternateTier  = 0;
        prizeList = new List<PrizeAndAlternateWrapper>();
        tierList = new List<ELITE_Tier__c>();
        Integer difference = 0;
        for(ELITE_Tier__c tier : [SELECT Type__c, Name, Last_Position__c, Id, First_Position__c, Event_Id__c 
                                    FROM ELITE_Tier__c 
                                    WHERE Event_Id__c = :eventId ORDER By First_Position__c]) {
            prizeList.add(new PrizeAndAlternateWrapper(tier));
            tierList.add(tier);
            difference = Integer.valueOf(tier.Last_Position__c - tier.First_Position__c + 1);
            numOfTiers += difference;
            if(tier.Type__c != null) {
            if(tier.Type__c.equalsIgnoreCase(TIER_ALTERNATE)) {
            numOfAlternateTier += difference;
            } else if(tier.Type__c.equalsIgnoreCase(TIER_WINNER)) {
            numOfWinnerTier += difference;
            }
            }
        }
    }
    
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Action Function for submit for approval
    //----------------------------------------------------------------------------------------------------------------------------------------
    public PageReference submitForApproval() {
        updateContest(CONTEST_STATUS_ON_SUBMIT);
        return null;
    }
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Action Function for approve Contest
    //----------------------------------------------------------------------------------------------------------------------------------------
    public PageReference ApproveContest() {
        updateContest(CONTEST_STATUS_ON_APPROVE);
        return null;
    }
    //----------------------------------------------------------------------------------------------------------------------------------------
    // A method that is used to update the status of the Contest.
    //----------------------------------------------------------------------------------------------------------------------------------------
    private void updateContest(String status) {
        Savepoint savePoint;
        try {
            savePoint = Database.setSavepoint(); 
            if(contest != null) {
            contest.Contest_Status__c = status;
            update contest;
            }
        } catch(Exception ex) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , DML_EXCEPTION_ERROR_MSG));
        Database.rollback(savePoint);
        }
    }
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Action Function that is used to verify the contestants
    //----------------------------------------------------------------------------------------------------------------------------------------
    public PageReference verifyAll() {
        verifyOrUnverifyAll(CONTESTANT_STATUS_WINNER_UNVERFIED, CONTESTANT_STATUS_WINNER_VERFIED, 
                            CONTESTANT_STATUS_ALTERNATE_UNVERFIED, CONTESTANT_STATUS_ALTERNATE_VERFIED);
        return null;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Action Function that is used to unverify the contestants
    //----------------------------------------------------------------------------------------------------------------------------------------
    public PageReference unVerifyAll() {
            verifyOrUnverifyAll(CONTESTANT_STATUS_WINNER_VERFIED, CONTESTANT_STATUS_WINNER_UNVERFIED,  
                                CONTESTANT_STATUS_ALTERNATE_VERFIED, CONTESTANT_STATUS_ALTERNATE_UNVERFIED);
        return null;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Common method That is used to verify or unverify the contestants
    //----------------------------------------------------------------------------------------------------------------------------------------
    private void verifyOrUnverifyAll(String checkWinner, String updateWinner, String checkAlternate, String updateAlternate) {
        Savepoint savePoint;
        List<ELITE_EventContestant__c> listToBeUpdate = new List<ELITE_EventContestant__c>();
        try {
            savePoint = Database.setSavepoint(); 
            for(ContestantWrapper wrapperObj :contestantsList) {
            if(wrapperObj.contestant.Status__c != null) {
                if(wrapperObj.contestant.Status__c.equalsIgnoreCase(checkWinner)) {
                wrapperObj.contestant.Status__c = updateWinner;
                listToBeUpdate.add(wrapperObj.contestant);
                } else if(wrapperObj.contestant.Status__c.equalsIgnoreCase(checkAlternate)) {
                wrapperObj.contestant.Status__c = updateAlternate;
                listToBeUpdate.add(wrapperObj.contestant);
                }
            }
            }
            if(listToBeUpdate.size() > 0) {
            update listToBeUpdate;
            init();
            }
        } catch(Exception ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , DML_EXCEPTION_ERROR_MSG));
                Database.rollback(savePoint);
            }
    }
    //----------------------------------------------------------------------------------------------------------------------------------------
    // A method which update the contestant records
    //----------------------------------------------------------------------------------------------------------------------------------------
    public PageReference updateStatus() {
        Savepoint savePoint;
        Integer currentNoOfWinner = 0, currentNoOfAlternate = 0, currentNoOfDisqualified = 0;
        List<ELITE_EventContestant__c> listToBeUpdate = new List<ELITE_EventContestant__c>();
        try {
        savePoint = Database.setSavepoint();
        if(selectedRecordNumber != null) {
                for(ContestantWrapper contestantWrapper : contestantsList) {
                    if(contestantWrapper.contestant.Status__c != null) {
                        if(contestantWrapper.index < selectedRecordNumber) {
                            //determine the no of winners
                            if(contestantWrapper.contestant.Status__c.contains('Winner')) {
                        currentNoOfWinner++;
                    } 
                    //determine the no of Alternates
                    else if(contestantWrapper.contestant.Status__c.contains('Alternate')) {
                        currentNoOfAlternate++;
                    } 
                    //determine the no of Disqualified 
                    else if(contestantWrapper.contestant.Status__c.equalsIgnoreCase(CONTESTANT_STATUS_DISQUALIFIED)) {
                        currentNoOfDisqualified++;
                    }
                        } 
                        else {
                            if(contestantWrapper.index == selectedRecordNumber) {
                                if(contestantWrapper.contestant.Status__c.equalsIgnoreCase(CONTESTANT_STATUS_DISQUALIFIED)) {
                                    contestantWrapper.contestant.Prize_Rank__c = 0;
                                contestantWrapper.contestant.Contestant_Tier__c = null;
                                listToBeUpdate.add(contestantWrapper.contestant);
                                currentNoOfDisqualified++;
                                } else if(contestantWrapper.contestant.Status__c.contains('Winner')) {
                                    contestantWrapper.contestant.Prize_Rank__c = contestantWrapper.contestant.Rank__c - currentNoOfDisqualified;
                            listToBeUpdate.add(contestantWrapper.contestant);
                                    currentNoOfWinner++;
                                } else if(contestantWrapper.contestant.Status__c.contains('Alternate')) {
                                    contestantWrapper.contestant.Prize_Rank__c = contestantWrapper.contestant.Rank__c - currentNoOfDisqualified;
                        listToBeUpdate.add(contestantWrapper.contestant);
                        currentNoOfAlternate++;
                                }
                            } else {
                                        if(!contestantWrapper.contestant.Status__c.equalsIgnoreCase(CONTESTANT_STATUS_DISQUALIFIED)) {
                                            //Assigned winning Tier
                                if(currentNoOfWinner < numOfWinnerTier) {
                                if(!contestantWrapper.contestant.Status__c.equalsIgnoreCase(CONTESTANT_STATUS_WINNER_VERFIED)) {
                                    if(contestantWrapper.contestant.Status__c.equalsIgnoreCase(CONTESTANT_STATUS_ALTERNATE_VERFIED)) {
                                        contestantWrapper.contestant.Status__c = CONTESTANT_STATUS_WINNER_VERFIED;
                                    } else {
                                        contestantWrapper.contestant.Status__c = CONTESTANT_STATUS_WINNER_UNVERFIED;
                                    }
                                }
                                contestantWrapper.contestant.Prize_Rank__c = contestantWrapper.contestant.Rank__c - currentNoOfDisqualified;
                                listToBeUpdate.add(contestantWrapper.contestant);
                                currentNoOfWinner++;
                            } 
                            //Assigned Alternate Tier
                            else if(numOfTiers > (currentNoOfWinner + currentNoOfAlternate)) {
                                if(!contestantWrapper.contestant.Status__c.equalsIgnoreCase(CONTESTANT_STATUS_ALTERNATE_VERFIED)) {
                                    if(contestantWrapper.contestant.Status__c.equalsIgnoreCase(CONTESTANT_STATUS_WINNER_VERFIED)) {
                                        contestantWrapper.contestant.Status__c = CONTESTANT_STATUS_ALTERNATE_VERFIED;
                                    } else {
                                        contestantWrapper.contestant.Status__c = CONTESTANT_STATUS_ALTERNATE_UNVERFIED;
                                    }
                                }
                                contestantWrapper.contestant.Prize_Rank__c = contestantWrapper.contestant.Rank__c - currentNoOfDisqualified;
                            listToBeUpdate.add(contestantWrapper.contestant);
                            currentNoOfAlternate++;
                            } 
                            //No Tier available to assign so break the iteration
                            else {
                                  if(contestantWrapper.contestant.Status__c.contains('Winner') 
                                        || contestantWrapper.contestant.Status__c.contains('Alternate')) {   
                                    contestantWrapper.contestant.Status__c = CONTESTANT_STATUS_PARTICIPANT;
                                    contestantWrapper.contestant.Prize_Rank__c = 0;
                                        contestantWrapper.contestant.Contestant_Tier__c = null;
                                        listToBeUpdate.add(contestantWrapper.contestant);
                                  } 
                            }
                                        }    
                                        //Increment the counter of disqualified Contestant
                                        else {
                                            currentNoOfDisqualified++;
                                        } 
                                    } 
                                    
                        }
                    }
            }
            update listToBeUpdate;
            init();
          }
      } catch(Exception ex) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , DML_EXCEPTION_ERROR_MSG));
        Database.rollback(savePoint);
        init();
        }
        return null;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Method that is used to populate Contestants
    //----------------------------------------------------------------------------------------------------------------------------------------
    private void populateContestants(Id eventId) {
        contestantsList.clear();
        numOfAlternates = 0;
        Integer counter = 0;
        for(ELITE_EventContestant__c contestant : [SELECT Status__c, Score__c, Rank__c, Notification_Email_Sent__c, Name,
                                                                            Contestant_Tier__c, Contestant_Responded__c, Event_Operation__c,
                                                                            ELITE_Platform__r.Gamertag__c, ELITE_Platform__r.Contact__c,
                                                                            ELITE_Platform__r.Contact__r.Name, CreatedBy.Name, CreatedDate, 
                                                                           (SELECT Id, IsDeleted, ParentId, CreatedBy.Name, CreatedDate, Field, OldValue, NewValue 
                                                                            FROM Histories 
                                                                            WHERE Field = 'Status__c' ORDER By CreatedDate DESC)
                                                                                FROM ELITE_EventContestant__c
                                                                                WHERE Event_Operation__c = :contest.Id ORDER By Rank__c]) {
            if(CONTESTANT_STATUS_ALTERNATE_VERFIED.equalsIgnoreCase(contestant.Status__c)) numOfAlternates++;
            contestantsList.add(new ContestantWrapper(contestant, counter));   
            counter++;                      
        }
        setPagination(contestantsList, DEFAULT_RESULTS_PER_PAGE);
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Method to Define Paginator Object for Pagination
    //----------------------------------------------------------------------------------------------------------------------------------------
    private void setPagination(List<ContestantWrapper> ContestantWrapperList, Integer recordsPerPage){
        allResults = new List<List<ContestantWrapper>>();
        totalRecords = ContestantWrapperList.size();
        for(ContestantWrapper wrapperObj : ContestantWrapperList) {
            if(allResults.size() == 0 || allResults.get(allResults.size() - 1).size() == recordsPerPage) {
                    allResults.add(new List<ContestantWrapper>());
            }
            allResults.get(allResults.size() - 1).add(wrapperObj);
        }
        getPage();
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Method that is used to get selected page
    //----------------------------------------------------------------------------------------------------------------------------------------
    public PageReference getPage() {
        //contestants = new List<ContestantWrapper>();
        if(allResults != null && allResults.size() > 0)
                contestants = allResults.get(selectedPageNumber - 1);
        return null;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Property that returs the total number of pages
    //----------------------------------------------------------------------------------------------------------------------------------------
    public Integer totalPage {
        get {
            if(allResults != null) {
                    return allResults.size();
            }
            return 0;
        }
        set;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Property which return the list of page links to be display
    //----------------------------------------------------------------------------------------------------------------------------------------
    public List<Integer> listOfLinks {
        get {
            Integer current = selectedPageNumber;
            Integer startFrom = current - DEFAULT_NUMBER_OF_LINKS_BEFORE_CURRENT;
            if(startFrom <= 0) startFrom = 1;
            
            List<Integer> links = new List<Integer>();
            
            for(Integer linkNumber = startFrom; linkNumber <= totalPage; linkNumber++) {
                links.add(linkNumber);
                if(links.size() >= DEFAULT_LINKS_AT_A_TIME) break;
            }
            return links;
        }
        set;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Property that return true if there are more links (in backward direction) to be display
    //----------------------------------------------------------------------------------------------------------------------------------------
    public Boolean isMorePrevious {
        get {
            if(listOfLinks.size() > 0 && Integer.valueOf(listOfLinks.get(0)) != 1) {
                    return true;
            }
            return false;
        }
        set; 
    }
                
                //----------------------------------------------------------------------------------------------------------------------------------------
    // Property that return true if there are more links (in forward direction) to be display 
    //----------------------------------------------------------------------------------------------------------------------------------------
    public Boolean isMoreNext {
        get {
            if(listOfLinks.size() > 0 && Integer.valueOf(listOfLinks.get(listOfLinks.size() - 1)) < totalPage) {
                    return true;
            }
            return false;
        }
        set;
    }
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Property that is used to numbering the record on the page (exa : 1-10 of 100)
    //----------------------------------------------------------------------------------------------------------------------------------------
    public String recordNumbering {
        get {
        if(contestants != null ) {
          return '(showing ' + getShowingFrom() + '-' + getShowingTo() + ' of ' + totalRecords + ')';
        }
        return '';
        }
        set;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Method that return the Index number of first record on the page
    //----------------------------------------------------------------------------------------------------------------------------------------
    private Integer getShowingFrom() {
       if(totalRecords != 0) {
            Integer showingFrom = (selectedPageNumber * DEFAULT_RESULTS_PER_PAGE ) - DEFAULT_RESULTS_PER_PAGE + 1;
            return showingFrom;
       }
       return 0;
    }
                
                //----------------------------------------------------------------------------------------------------------------------------------------
    // Method that return the Index number of last record on the page
    //----------------------------------------------------------------------------------------------------------------------------------------
    public Integer getShowingTo() {
      Integer showingTo = selectedPageNumber * DEFAULT_RESULTS_PER_PAGE ;
      if (totalRecords < showingTo) {
        showingTo = totalRecords;
      }
      return showingTo;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Return the String for Page Numbering
    //----------------------------------------------------------------------------------------------------------------------------------------
    public String pageNumbering {
        get {
            if(contestants != null ) {
                return 'Page '+ selectedPageNumber + ' of ' + totalPage;  
            }
            return '';
        }
        set;
    }
    
    
    /**
        * Wrapper Class for Prize Details
        */
    public class PrizeAndAlternateWrapper {
    public String prizeName {get;set;}
    public String places {get;set;}
    public Integer numOfPlayers {get;set;}
    
    public PrizeAndAlternateWrapper(ELITE_Tier__c tier) {
        if(TIER_ALTERNATE.equalsIgnoreCase(tier.Type__c)) {
                prizeName = System.Label.ELITE_Alternate;
        } else {
                prizeName = tier.Name;
        }
        places = formatNumber(Integer.valueOf(tier.First_Position__c)) + '-' + formatNumber(Integer.valueOf(tier.Last_Position__c));
        numOfPlayers = Integer.valueOf(tier.Last_Position__c - tier.First_Position__c + 1);
    }
    private String formatNumber(Integer num) {
        List<String> ordinalList = new List<String>{'th','st','nd','rd','th','th','th','th','th','th'};
            if (math.mod(num, 100) >= 11 && math.mod(num, 100) <= 13)
               return  num + ordinalList.get(0);
            else
              return  num + ordinalList.get(math.mod(num, 10));
    }
    }
    
    /**
        * Wrapper Class for Contestant Details
        */
    public class ContestantWrapper {
        public ELITE_EventContestant__c contestant {get;set;}
        public Integer index {get; set;}
        public String partialHistory {get;set;}
        public String completeHistory {get;set;}
        Boolean isFirst;
        String status;
        public ContestantWrapper(ELITE_EventContestant__c contestant, Integer index) {
        isFirst = true;
        status = '';
        this.contestant = contestant;
        this.completeHistory = '';
        this.index = index;
        
        this.partialHistory = 'Created By ' + contestant.createdBy.Name + ', ' + contestant.CreatedDate + '<br/>';
        
        for(ELITE_EventContestant__History contestantHistory : contestant.Histories) {
            
             if(contestantHistory.NewValue != null)
                 status += '<u>' + contestantHistory.NewValue + ':</u>' + contestantHistory.createdBy.Name + ', ' 
                                                       + contestantHistory.CreatedDate + '<br/>';
             else 
                 status += '<u>None:</u>' + contestantHistory.createdBy.Name + ', ' 
                                                       + contestantHistory.CreatedDate + '<br/>';
                
                 if(isFirst) {
                     this.partialHistory = status;
                     status = '';
                     isFirst = false;
                 }
        }
        
        if(contestant.Histories.size() > 0) {
            this.completeHistory = status + 'Created By ' + contestant.createdBy.Name + ', ' + contestant.CreatedDate + '<br/>';
        }
        
        }
    }
}