/**
    * Apex Class: ELITE_Gamer_VerificationControllerTest
    * Description: A test class that check the quality of the controller ELITE_Gamer_VerificationController and 
    *              also validate the functionality performed by it.
    * Created Date: 9 August 2012
    * Created By: Sudhir Kr. Jagetiya
    */
@isTest(seeAllData = false)
private class ELITE_Gamer_VerificationControllerTest {
        static Integer NUM_OF_RECORDS = 8;
        //A test method to check the Quality of the code and validate the functionality.
    static testMethod void myUnitTest() {
        //Create test data
        List<Contact> listContacts = new List<Contact>();
        listContacts = createGamers(NUM_OF_RECORDS, true);
        
        ELITE_EventOperation__c event = ELITE_TestUtility.createContest(true);
        
        Multiplayer_Account__c MultiplayerAccount = ELITE_TestUtility.createMultiplayerAccount(listContacts.get(0).Id, true);
        
        List<ELITE_Tier__c> tiers = new List<ELITE_Tier__c>();
        tiers.add(ELITE_TestUtility.createTier(event.Id, 1, 2, false));
        tiers.add(ELITE_TestUtility.createTier(event.Id, 3, 4, false));
        tiers.add(ELITE_TestUtility.createTier(event.Id, 5, 6, false));
        tiers.get(tiers.size() - 1).Type__c = 'Alternate';
        insert tiers;
        
        List<ELITE_EventContestant__c> listContestants = new List<ELITE_EventContestant__c>();
        for(Integer sNo = 0; sNo < NUM_OF_RECORDS; sNo++) {
           listContestants.add(ELITE_TestUtility.createContestant(event.Id, MultiplayerAccount.Id, sNo + 1, false));
        }
        insert listContestants;
        
        
        
        Test.startTest();
            //prepare a Reference of Page DealRentStepSpeciality
            PageReference pg = Page.ELITE_Gamer_Verification;
            //Set Current page
            Test.setCurrentPageReference(pg); 
            //Put the Parameters  to the Current page
            ApexPages.currentPage().getParameters().put('Id',event.Id); 
            ELITE_Gamer_VerificationController ctrl = new ELITE_Gamer_VerificationController();
            
            //At the starting no one is verified so it should be zero.
            system.assertEquals(ctrl.numOfPercentVerified , 0);
            //We have created a records of contestant for the particular event so there should be at least one link on the VF page.
            system.assert(ctrl.listOfLinks.size() > 0);
            system.assertEquals(ctrl.isMorePrevious , false);
            system.assertEquals(ctrl.isMoreNext , false);
            system.assertEquals(ctrl.recordNumbering, '(showing 1-' + ctrl.totalRecords + ' of ' + ctrl.totalRecords + ')');
            system.assertEquals(ctrl.pageNumbering, 'Page '+ ctrl.selectedPageNumber + ' of ' + ctrl.totalPage);
            
            ELITE_EventContestant__c contestant;
            
            ctrl.contestants.get(6).contestant.Status__c = ELITE_Gamer_VerificationController.CONTESTANT_STATUS_PARTICIPANT;
            ctrl.selectedRecordNumber = ctrl.contestants.get(6).index;
            ctrl.updateStatus();
            contestant = retrieveContestant(ctrl.contestants.get(6).contestant.Id);
            system.assertEquals(contestant.Status__c, ELITE_Gamer_VerificationController.CONTESTANT_STATUS_PARTICIPANT);
            
            ctrl.contestants.get(7).contestant.Status__c = ELITE_Gamer_VerificationController.CONTESTANT_STATUS_PARTICIPANT;
            ctrl.selectedRecordNumber = ctrl.contestants.get(7).index;
            ctrl.updateStatus();
            contestant = retrieveContestant(ctrl.contestants.get(7).contestant.Id);
            system.assertEquals(contestant.Status__c, ELITE_Gamer_VerificationController.CONTESTANT_STATUS_PARTICIPANT);
            
            //Verify All
            ctrl.verifyAll();
            system.assertEquals(ctrl.numOfPercentVerified , 100);
            
            //Verify All
            ctrl.unVerifyAll();
            system.assertEquals(ctrl.numOfPercentVerified , 0);
            
            ctrl.submitForApproval();
            system.assertEquals(ctrl.contest.Contest_Status__c, 'Pending Approval');
            
            ctrl.contestants.get(3).contestant.Status__c = ELITE_Gamer_VerificationController.CONTESTANT_STATUS_DISQUALIFIED;
            ctrl.selectedRecordNumber = ctrl.contestants.get(3).index;
            ctrl.updateStatus();
            contestant = retrieveContestant(ctrl.contestants.get(3).contestant.Id);
            system.assertEquals(contestant.Status__c, ELITE_Gamer_VerificationController.CONTESTANT_STATUS_DISQUALIFIED);
            
        Test.stopTest();
    }
    static ELITE_EventContestant__c retrieveContestant(Id contestantId) {
        return [SELECT Rank__c,Notification_Email_Sent__c, Status__c FROM ELITE_EventContestant__c WHERE Id = :contestantId];
    }
    static List<Contact> createGamers(Integer numOfRecords, Boolean isInsert) {
        List<Contact> listContacts = new List<Contact>();
        Contact con;
            for(Integer sNo = 0; sNo < numOfRecords; sNo++) {
                con = ELITE_TestUtility.createGamer(false);
                con.Gamertag__c = 'record' + sNo;
            con.UCDID__c = '000' + sNo;
            con.Email = 'record' + sNo + '@gmail.com';
            listContacts.add(con);
            }
            if(isInsert)
                insert listContacts;
            return listContacts;
    }
    
}