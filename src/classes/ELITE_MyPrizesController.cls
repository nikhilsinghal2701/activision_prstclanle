/**
    * Apex Class: ELITE_MyPrizesController 
    * Description: A controller for MyPrize component 
    *              that is used to provide the User interface for MyPrize.
    * Created Date: 6 August 2012
    * Created By: Sudhir Kr. Jagetiya
    */
public without sharing class ELITE_MyPrizesController {
    public List<PrizeWrapper> currentPrizes {get;set;}
    public List<PrizeWrapper> pastPrizes {get;set;}
    public Boolean isSuccess {get;set;}
    
    static final String SHIPPED = 'Shipped';
    static final String DELIVERED = 'Delivered';
    
    List<ELITE_GamerPrize__c> gamePrizeList;
    Set<String> countries;
    //Id eventId, gamerId;
    
    List<User> currentUserList;
    public ELITE_MyPrizesController () {
            countries = new Set<String>{'The United States of America'};
        init();
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // initilaize 
    //----------------------------------------------------------------------------------------------------------------------------------------
    private void init() {
        currentUserList = [SELECT ContactId, Contact.Elite_Status__c FROM User WHERE ContactId != null AND Id = :UserInfo.getUserId()];
        if(currentUserList.size() > 0 && currentUserList.get(0).ContactId != null) {
        gamePrizeList = new List<ELITE_GamerPrize__c>();
        gamePrizeList = [SELECT Id, Tracking_Number__c, Status__c, Country__c,
                                    Contact_Id__c, Date_Shipped__c, LastModifiedDate,
                                    Contestant__c, 
                                    Multiplayer_Account__r.Contact__c, 
                                                                    Multiplayer_Account__r.Contact__r.Elite_Status__c,
                                                Multiplayer_Account__r.DWID__c, 
                                                Contestant__r.Status__c, 
                                                Contestant__r.Score__c, 
                                    Contestant__r.Prize_Rank__c, 
                                                Contestant__r.Event_Operation__r.Name, 
                                                Contestant__r.Event_Operation__c,
                                                Contestant__r.Prize_Claimed__c, 
                                                Contestant__r.Event_Operation__r.Elite_Event_Id__c,
                                                Contestant__r.Event_Operation__r.Eligible_Countries__c,
                                    Tier_Prize__c, Value__c, 
                                    Tier_Prize__r.Prize__c,
                                    Tier_Prize__r.Prize__r.Image__c, 
                                    Tier_Prize__r.Prize__r.Name, 
                                    Tier_Prize__r.Prize__r.Description__c,
                                    Tier_Prize__r.Bundle__c,
                                    Tier_Prize__r.Bundle__r.Image__c, 
                                    Tier_Prize__r.Bundle__r.Name, 
                                    Tier_Prize__r.Bundle__r.Description__c
                            FROM ELITE_GamerPrize__c
                            WHERE Multiplayer_Account__r.Contact__c = :currentUserList.get(0).ContactId];
   
        if(gamePrizeList.size() > 0) {
             //Populate Prize lists
             populatePrizeList(gamePrizeList);
        
           }  
            } 
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------
    // Method that is used to populate prize List
    //----------------------------------------------------------------------------------------------------------------------------------------
    private void populatePrizeList(List<ELITE_GamerPrize__c> gamePrizeList) {
        String status = '';
        currentPrizes = new List<PrizeWrapper>();
        pastPrizes = new List<PrizeWrapper>();
        
        for(ELITE_GamerPrize__c gamerPrize : gamePrizeList) {
            if(gamerPrize.Tier_Prize__r.Bundle__c != null || gamerPrize.Tier_Prize__r.Prize__c != null) {
                if(gamerPrize.Status__c != null) {
                if(gamerPrize.Status__c.equalsIgnoreCase(SHIPPED)) {
                    status = gamerPrize.Status__c;
                    status = gamerPrize.Tracking_Number__c != null ? status +' Tracking # ' + '<a href="#" onClick="openFedEx(\'' + gamerPrize.Tracking_Number__c +'\');">' + gamerPrize.Tracking_Number__c + '</a>' : status;
                    currentPrizes.add(new PrizeWrapper(gamerPrize,status));
                } 
                else if(gamerPrize.Status__c.equalsIgnoreCase(DELIVERED)) {
                    status = gamerPrize.Status__c;
                    status = gamerPrize.Tracking_Number__c != null ? status +' Tracking # ' + '<a href="#" onClick="openFedEx(\'' + gamerPrize.Tracking_Number__c +'\');">' + gamerPrize.Tracking_Number__c + '</a>' : status;
                    pastPrizes.add(new PrizeWrapper(gamerPrize,status));
                } 
                else {
                    currentPrizes.add(new PrizeWrapper(gamerPrize,gamerPrize.Status__c));
                }
                }
            }
        }
    }
    /**
        * Prize Wrapper Class
        */
    public class PrizeWrapper {
        public ELITE_EventContestant__c eventContestant {get; set;}
        public String contestantRank {get; set;}
        public String prizeId {get;set;}
    public String prizeName {get;set;}
    public String description {get;set;}
    public String imageURL {get;set;}
    public String status  {get;set;}
    public Date shippedDate {get; set;}
    public String eligibleCountries {get;set;}
    public PrizeWrapper(ELITE_GamerPrize__c prize, String status) {
        this.eligibleCountries = prize.Contestant__r.Event_Operation__r.Eligible_Countries__c;
        this.eventContestant = prize.Contestant__r;
        this.contestantRank = '';
        if(prize.Contestant__r.Prize_Rank__c != null) 
            this.contestantRank = formatNumber(Integer.valueOf(prize.Contestant__r.Prize_Rank__c));
        this.prizeId = prize.Id;
        this.status = status;
        this.shippedDate = prize.Date_Shipped__c;
        
        if(prize.Tier_Prize__r.Bundle__c != null) {
            this.prizeName = prize.Tier_Prize__r.Bundle__r.Name;
            this.imageURL = prize.Tier_Prize__r.Bundle__r.Image__c;
            this.description = prize.Tier_Prize__r.Bundle__r.Description__c;
        } 
        else if(prize.Tier_Prize__r.Prize__c != null) {
            this.prizeName = prize.Tier_Prize__r.Prize__r.Name;
            this.imageURL = prize.Tier_Prize__r.Prize__r.Image__c;
            this.description = prize.Tier_Prize__r.Prize__r.Description__c;
        } 
        else {
            this.prizeName = '';
            this.imageURL = '';
            this.description = '';
        } 
    }
    }
    
    private static String formatNumber(Integer num) {
        List<String> ordinalList = new List<String>{'th','st','nd','rd','th','th','th','th','th','th'};
    Integer modOfRank = math.mod(num, 100);
    if (modOfRank >= 11 && modOfRank <= 13)
       return  num + ordinalList.get(0);
    else
      return  num + ordinalList.get(math.mod(num, 10));
    }

}