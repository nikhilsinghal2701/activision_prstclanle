/**
    * Apex Class: ELITE_MyPrizesControllerTest 
    * Description: A Test Class for the controller ELITE_MyPrizesController,
    *              (Validate the functionality performed by the controller)
    * Created Date: 22 August 2012
    * Created By: Sudhir Kr. Jagetiya
    */
@isTest
private class ELITE_MyPrizesControllerTest {
		static final String PORTAL_PROFILE_NAME = 'ATVI OHV Cust Port User';
    //A test method that is used to check the quality of the code and also validate the functionality performed by controller.
    static testMethod void myUnitTest() {
        
        List<Contact> listContacts = new List<Contact>();
        Contact con = ELITE_TestUtility.createGamer(false);
        con.Elite_Status__c = 'Premium';
        con.Gamertag__c = 'record1';
        con.UCDID__c = '0001';
        con.Email = 'record1@gmail.com';
        listContacts.add(con);
        
		    insert listContacts;
		    
		    List<Profile> profiles = [SELECT Id
																FROM Profile 
																WHERE Name = :PORTAL_PROFILE_NAME LIMIT 1];
	    
	    	system.assert(profiles.size() > 0);
      	User user = ELITE_TestUtility.createPortalUser('Test1', profiles.get(0).Id, listContacts.get(0).Id, true);
      	
		    Multiplayer_Account__c mAccount = ELITE_TestUtility.createMultiplayerAccount(listContacts.get(0).Id, true);
		    
		    ELITE_EventOperation__c event = ELITE_TestUtility.createContest(true);
		    
		    List<ELITE_Tier__c> toInsertOrUpdate = new List<ELITE_Tier__c>();
            toInsertOrUpdate.add(ELITE_TestUtility.createTier(event.Id, 1, 5, false));
            toInsertOrUpdate.add(ELITE_TestUtility.createTier(event.Id, 6, 10, false));
		    insert toInsertOrUpdate;
		    
		    
		    List<ELITE_EventContestant__c> listContestants = new List<ELITE_EventContestant__c>();   
		    listContestants.add(ELITE_TestUtility.createContestant(event.Id, mAccount.Id, 1, false));
		    insert listContestants;
		    
		    
		    List<ELITE_Prize__c> listOfPrize = new List<ELITE_Prize__c>();
		    for(Integer sNo = 0; sNo < 2; sNo++) {
		        listOfPrize.add(ELITE_TestUtility.createPrize('Test' + sNo, sNo * 10, false));
		    }
		    insert listOfPrize;
		    
		    ELITE_Bundle__c bundle = ELITE_TestUtility.createBundle('Bundle1', true);
		    ELITE_PrizeCatalog__c prizeCatalog = ELITE_TestUtility.createPrizeBundle('PrizeBundle1', bundle.Id, listOfPrize.get(0).Id, true);
		    
		    List<ELITE_Tier_Prize__c> listOfTierPrize = new List<ELITE_Tier_Prize__c>();
		    listOfTierPrize.add(ELITE_TestUtility.createTierPrize('TierPrize1', toInsertOrUpdate.get(0).Id, bundle.Id, null, false));
		    listOfTierPrize.add(ELITE_TestUtility.createTierPrize('TierPrize1', toInsertOrUpdate.get(0).Id, null, listOfPrize.get(1).Id, false));
		    insert listOfTierPrize;
		    
		    List<ELITE_GamerPrize__c> listOfGamerPrize = new List<ELITE_GamerPrize__c>();
            ELITE_GamerPrize__c gamerPrize;
        
        gamerPrize = ELITE_TestUtility.createGamerPrize(mAccount.Contact__c, mAccount.Id, false);
        gamerPrize.Tier_Prize__c = listOfTierPrize.get(0).Id;
        gamerPrize.Status__c = 'Accepted - Submitted for Fulfillment';
		    listOfGamerPrize.add(gamerPrize);
		    
		    gamerPrize = ELITE_TestUtility.createGamerPrize(mAccount.Contact__c, mAccount.Id, false);
        gamerPrize.Tier_Prize__c = listOfTierPrize.get(1).Id;
        gamerPrize.Status__c = 'Delivered';
		    listOfGamerPrize.add(gamerPrize);
		    insert listOfGamerPrize;
		    
		    system.runAs(user) {
			    Test.startTest();
			        //prepare a Reference of Page DealRentStepSpeciality
			        PageReference pg = Page.ELITE_my_support_prizes;
			        //Set Current page
			        Test.setCurrentPageReference(pg); 
			        
			        ELITE_MyPrizesController ctrl = new ELITE_MyPrizesController();
			        system.assertEquals(ctrl.currentPrizes.size(), 1); 
			        system.assertEquals(ctrl.pastPrizes.size(), 1);
			        
			    Test.stopTest();
		    }
		    }
}