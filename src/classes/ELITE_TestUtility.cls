/**
    * Apex Class: ELITE_TestUtility 
    * Description: A Helper Class for all the test classes.
    * Created Date: 3 August 2012
    * Created By: Sudhir Kr. Jagetiya
    */
public without sharing class ELITE_TestUtility {
    //--------------------------------------------------------------------------------------------------------------------------------------
    // Method that return test record for a Contact object. 
    //--------------------------------------------------------------------------------------------------------------------------------------
    public static Contact createGamer(Boolean isInsert) {
        Contact con = new Contact();
        con.FirstName = 'Test Contact';
        con.LastName = dateTime.now() + '';
        con.Country__c = 'India';
        con.Email = 'test@gmail.com';
        con.Birthdate = date.today().addYears(-20);
        if(isInsert)
            insert con;
        return con;
    }
    
    //--------------------------------------------------------------------------------------------------------------------------------------
    // Method that return test record for a Contact object. 
    //--------------------------------------------------------------------------------------------------------------------------------------
    public static ELITE_Tier__c createTier(Id contestId, Integer firstPosition, Integer lastPosition, Boolean isInsert) {
        ELITE_Tier__c tier = new ELITE_Tier__c();
        tier.Name = 'Test Tier' + date.today();
        tier.Event_Id__c = contestId;
        tier.First_Position__c = firstPosition;
        tier.Last_Position__c = lastPosition;
        tier.Type__c = 'Winner';
        if(isInsert)
            insert tier;
        return tier;
    }
    
    public static ELITE_Tier_Prize__c createTierPrize(String Name, Id tierId, Id bundleId, Id prizeId, Boolean isInsert) {
        ELITE_Tier_Prize__c tierPrize = new ELITE_Tier_Prize__c();
        tierPrize.Name = Name;
        tierPrize.Bundle__c = bundleId;
        tierPrize.Prize__c = prizeId;
        tierPrize.Tier__c = tierId;
        if(isInsert)
            insert tierPrize;
        return tierPrize;
    }
    
    //--------------------------------------------------------------------------------------------------------------------------------------
    // Method that return test record for a Contact object. 
    //--------------------------------------------------------------------------------------------------------------------------------------
    public static ELITE_EventOperation__c createContest(Boolean isInsert) {
        ELITE_EventOperation__c contest = new ELITE_EventOperation__c();
        contest.Name = 'Test Contest' + date.today();
        if(isInsert)
            insert contest;
        return contest;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------
    // Method that return test record for a Contact object. 
    //--------------------------------------------------------------------------------------------------------------------------------------
    public static ELITE_EventContestant__c createContestant(Id eventId, Id multiplayerAccountId, Integer rank, Boolean isInsert) {
        ELITE_EventContestant__c contestant = new ELITE_EventContestant__c();
        contestant.Event_Operation__c = eventId;
        contestant.ELITE_Platform__c = multiplayerAccountId;
        contestant.Rank__c = rank;
        if(isInsert)
            insert contestant;
        return contestant;
    }
   
    //--------------------------------------------------------------------------------------------------------------------------------------
    // Create Gamer Prize Record 
    //--------------------------------------------------------------------------------------------------------------------------------------
    public static ELITE_GamerPrize__c createGamerPrize(Id contactId, Id multiplayerAccountId, Boolean isInsert) {
        ELITE_GamerPrize__c gamerPrize = new ELITE_GamerPrize__c();
        gamerPrize.Contact_Id__c = contactId;
        gamerPrize.Multiplayer_Account__c = multiplayerAccountId;
        gamerPrize.Tracking_Number__c = '123456'; 
        if(isInsert)
        insert gamerPrize;
        return gamerPrize;
    }
    
    public static Multiplayer_Account__c createMultiplayerAccount(Id contactId, Boolean isInsert) {
        Multiplayer_Account__c multiplayerAccount = new Multiplayer_Account__c();
        multiplayerAccount.Contact__c = contactId;
        multiplayerAccount.DWID__c = 'Test ' + contactId ;      
        
        if(isInsert)
        insert multiplayerAccount;
      return multiplayerAccount;
    }
    
    public static ELITE_Prize__c createPrize(String Name, Double value, Boolean isInsert) {
        ELITE_Prize__c prize = new ELITE_Prize__c();
        prize.Value__c = value;
        prize.Name = Name;
        if(isInsert)
            insert prize;
        return prize;
    }
    
    public static ELITE_Bundle__c createBundle(String Name, Boolean isInsert) {
        ELITE_Bundle__c bundle = new ELITE_Bundle__c();
        bundle.Name = Name;
        if(isInsert)
            insert bundle;
        return bundle;
    }
    
    //--------------------------------------------------------------------------------------------------------------------------------------
    // Create Prize Catalog Records 
    //--------------------------------------------------------------------------------------------------------------------------------------
    public static ELITE_PrizeCatalog__c createPrizeBundle(String Name, Id bundleId, Id prizeId, Boolean isInsert) {
        ELITE_PrizeCatalog__c prize = new ELITE_PrizeCatalog__c();
        prize.Name = name;
        prize.Bundle__c = bundleId;
        prize.Prize__c = prizeId;
        if(isInsert)
            insert prize;
        return prize;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------
    // Create Portal User 
    //--------------------------------------------------------------------------------------------------------------------------------------
    public static User createPortalUser(String uniqueKey, Id profileId, Id contactId, Boolean isInsert) {
        Account acc = new Account(Name = 'Test');
        insert acc;
        Contact con = new Contact(Id = contactId, AccountId = acc.Id);
        update con;
        User user = new User(Alias = uniqueKey,
                                        Email = uniqueKey + '@testorg.com',
                                        EmailEncodingKey = 'UTF-8',
                                        LastName = 'Test' + uniqueKey,
                                        FirstName = uniqueKey,
                                        LanguageLocaleKey = 'en_US',
                                        LocaleSidKey = 'en_US',
                                        ProfileId = profileId,
                                        TimeZoneSidKey = 'America/Chicago',
                                        Username = uniqueKey + '@test' + Math.round(Math.random() * 10000) + '.com',
                                        Country = 'Great Britain',
                                        IsActive = true,
                                        ContactId = contactId);
            if(isInsert) 
                insert user;
            return user;
    }
    
      
}