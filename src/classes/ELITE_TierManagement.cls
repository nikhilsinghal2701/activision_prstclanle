/**
    * Apex Class: ELITE_TierManagement 
    * Description: A Helper Class for TierTrigger that prevents 2 tiers on the same Contest from overlapping .
    * Created Date: 3 August 2012
    * Created By: Sudhir Kr. Jagetiya
    */
public without sharing class ELITE_TierManagement {
    //---------------------------------------------------------------------------------------------------------
    // Execute on before insert or update event of trigger
    //---------------------------------------------------------------------------------------------------------
    public static void beforeInsertUpdate(List<ELITE_Tier__c> newList, Map<Id,ELITE_Tier__c> oldMap) {
        detectOverLapping(newList,oldMap);
    }
    //---------------------------------------------------------------------------------------------------------
    // Method is Used to detect overlapping of Tiers
    //---------------------------------------------------------------------------------------------------------
    private static void detectOverLapping(List<ELITE_Tier__c> newList, Map<Id,ELITE_Tier__c> oldMap) {
        
        Boolean isInsert = oldMap == null ? true : false;
        Boolean isUpdate = oldMap != null ? true : false;
        
        Set<Id> contestIds = new Set<Id>();
        List<ELITE_Tier__c> recordsToBeProcess = new List<ELITE_Tier__c>();
        Map<Id, List<ELITE_Tier__c>> mapContestTier = new Map<Id,List<ELITE_Tier__c>>();
        
        for(ELITE_Tier__c tier : newList) {
            if(isInsert || (isUpdate && (tier.First_Position__c != oldMap.get(tier.Id).First_Position__c || 
                                                                                        tier.Last_Position__c != oldMap.get(tier.Id).Last_Position__c))) {
                contestIds.add(tier.Event_Id__c);
                recordsToBeProcess.add(tier);
            } else {
                //Populate Map For contest Id or tier records thore are exist in Trigger.new
                if (!mapContestTier.containsKey(tier.Event_Id__c)) {
            mapContestTier.put(tier.Event_Id__c, new List<ELITE_Tier__c>());
          }
          mapContestTier.get(tier.Event_Id__c).add(tier);
            }
        }
        
        //Populate Map For contest Id or tier records those are not available in Trigger.new
        for(ELITE_Tier__c tier : [SELECT Last_Position__c, First_Position__c, Event_Id__c 
                                                                FROM ELITE_Tier__c
                                                                WHERE Event_Id__c IN :contestIds AND Id NOT IN :newList]) {
            if (!mapContestTier.containsKey(tier.Event_Id__c)) {
        mapContestTier.put(tier.Event_Id__c, new List<ELITE_Tier__c>());
      }
      mapContestTier.get(tier.Event_Id__c).add(tier);
        }
        
        
        for(ELITE_Tier__c tierToBeProcess : recordsToBeProcess) {
            if(mapContestTier.containsKey(tierToBeProcess.Event_Id__c)) {
                for(ELITE_Tier__c tier : mapContestTier.get(tierToBeProcess.Event_Id__c)) {
                    if(isOverlap(tierToBeProcess, tier)) {
                        tierToBeProcess.addError('This contest is already associated with Tier for the slot ' + 
                                                                            tierToBeProcess.First_Position__c + ' - ' + tierToBeProcess.Last_Position__c);
                        break;
                    }
                }
            }
            
            if(!mapContestTier.containsKey(tierToBeProcess.Event_Id__c)) {
        mapContestTier.put(tierToBeProcess.Event_Id__c, new List<ELITE_Tier__c>());
      }
      mapContestTier.get(tierToBeProcess.Event_Id__c).add(tierToBeProcess);
        }
    }
    //---------------------------------------------------------------------------------------------------------
    // Method is Used to check two record are overlaping to each other or not.
    //---------------------------------------------------------------------------------------------------------
    private static Boolean isOverlap(ELITE_Tier__c currentRecord, ELITE_Tier__c previousRecord) {
        if(currentRecord.First_Position__c >= previousRecord.First_Position__c && 
                                                                        currentRecord.First_Position__c <= previousRecord.Last_Position__c) {
            return true;
        } else if(currentRecord.Last_Position__c >= previousRecord.First_Position__c && 
                                                                        currentRecord.Last_Position__c <= previousRecord.Last_Position__c) {
            return true;
        } else {
            return false;
        }
    }
}