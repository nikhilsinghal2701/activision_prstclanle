/**
    * Apex Class: ELITE_TierTriggerTest 
    * Description: A Test class for ELITE_TierTrigger that validate the functionality performed by trigger on event of insert or update
    * Created Date: 3 August 2012
    * Created By: Sudhir Kr. Jagetiya
    */
@isTest(seeAllData = false)
private class ELITE_TierTriggerTest {
    //A test method to check the Quality of the code 
  static testMethod void validateTrigger() {
        //Create Test Data
    ELITE_EventOperation__c contest = ELITE_TestUtility.createContest(true);
    
    List<ELITE_Tier__c> toInsertOrUpdate = new List<ELITE_Tier__c>();
    
        toInsertOrUpdate.add(ELITE_TestUtility.createTier(contest.Id, 1, 5, false));
        toInsertOrUpdate.add(ELITE_TestUtility.createTier(contest.Id, 6, 10, false));
        toInsertOrUpdate.add(ELITE_TestUtility.createTier(contest.Id, 11, 15, false));
        toInsertOrUpdate.add(ELITE_TestUtility.createTier(contest.Id, 3, 8, false));
        Test.startTest();
            
            try{
                Database.insert(toInsertOrUpdate,false);
            } catch(DMLException e) {
                System.assert(e.getMessage().contains('This contest is already associated with Tier for the slot ' + 
                            toInsertOrUpdate.get(3).First_Position__c + ' - ' + toInsertOrUpdate.get(3).Last_Position__c),e.getMessage());
            }
            
            try{
                toInsertOrUpdate.remove(3);
                toInsertOrUpdate.remove(2);
                toInsertOrUpdate.get(0).First_Position__c = 3;
                toInsertOrUpdate.get(0).Last_Position__c = 8;
                toInsertOrUpdate.get(1).Name = 'SKJ';
                Database.update(toInsertOrUpdate,false);
            } catch(DMLException e) {
                System.assert(e.getMessage().contains('This contest is already associated with Tier for the slot ' + 
                            toInsertOrUpdate.get(0).First_Position__c + ' - ' + toInsertOrUpdate.get(0).Last_Position__c),e.getMessage());
            }
        Test.stopTest();
        
  }
}