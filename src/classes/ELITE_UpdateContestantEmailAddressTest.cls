/**
	* Apex Class: ELITE_UpdateContestantEmailAddressTest
	* Description: Test class for the ELITE_UpdateContestantEmailAddress which update Email Addres of contestant.
	* Created Date: 13 oct 2012
	* Created By: Sudhir Kr. Jagetiya
	*/
@isTest
private class ELITE_UpdateContestantEmailAddressTest {

    static testMethod void myUnitTest() {
	    Contact con = ELITE_TestUtility.createGamer(false);
	    con.Gamertag__c = 'record1';
	    con.UCDID__c = '0001';
	    con.Email = 'record1@gmail.com';
	    
	    insert con;
	    
	    ELITE_EventOperation__c event = ELITE_TestUtility.createContest(true);
	    Multiplayer_Account__c mAccount = ELITE_TestUtility.createMultiplayerAccount(con.Id, true);
	    ELITE_EventContestant__c contestant = ELITE_TestUtility.createContestant(event.Id, mAccount.Id, 1, true);
	    
	    Test.startTest();
	    	con.Email = 'test1@gmail.com';
	    	update con;
	    	system.assertEquals(retrieveContestant(contestant.Id).Email__c, con.Email);
	    	
	    Test.stopTest();
    }
    
    static ELITE_EventContestant__c retrieveContestant(Id contestantId) {
    	return [SELECT Email__c FROM ELITE_EventContestant__c WHERE Id = :contestantId];
    }
}