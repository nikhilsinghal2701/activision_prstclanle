/**
	* Apex Class: ELITE_Utility
	* Dexcription: Utility Class.
	* Created By: Sudhir Kumar Jagetiya
	* Created Date: Sep 18, 2012
	*/
public without sharing class ELITE_Utility {
	
	static final String PORTAL_PROFILE_NAME = 'ATVI OHV Cust Port User';
	
	public static Boolean isPortalUser() {
		List<Profile> profiles = [SELECT Id
															FROM Profile 
															WHERE Name = :PORTAL_PROFILE_NAME LIMIT 1];
										
		if(profiles.size() > 0){
			if(Userinfo.getProfileId() == profiles.get(0).Id){
				return true;
			}
		}
		return false;
	}
}