public class Elite_Abatement_Wizard_Controller {

	//Set up variables to maintain state:
	public Contact selectedContact {get; set;}
	public Abatement__c currentAbatement { get; set; }
	public Remedy__c currentRemedy { get; set; }
	public Case currentCase { get; set; }
	
	public List<Multiplayer_Account__c> linkedPremiumAccounts {get; set;}
	public boolean contactHasLinkedPremiumAccounts {get; set;}
	
	public List<SelectOption> premiumAccountList { get; set; }
	
	public String selectedGamertag { get; set; }
	public Multiplayer_Account__c selectedMPAccount { get; set; }
	
	//Navigation Variables:
	public integer pageNumber { get; set; }
	public string errorMessage { get; set; }
	public boolean showSentimentPage { get; set; }
	public boolean showBenefitsPage { get; set; }
	public boolean showIdentityVerificationPage { get; set; }
	public boolean showLinkedAccountsPage { get; set; }
	public boolean showRemedyPage { get; set; }
	public boolean showReviewPage { get; set; }
	public boolean showCallMSFTPage { get; set; }
	public boolean showPS3InfoPage { get; set; }
	public boolean showCommunicatePS3Refund { get; set; }
	public boolean showCommunicateXBOXRefund { get; set; }
	public boolean showFinalPage { get; set; }
	
	public string confirmationPageMessage { get; set; }
	public string confirmationPageMessageScript { get; set; }
	
	public string previousPage { get; set; }
	
	//Helper Methods:
	public void setAllPagesFalse()
	{
		showSentimentPage = false;
		showBenefitsPage = false;
		showIdentityVerificationPage = false;
		showLinkedAccountsPage = false;
		showRemedyPage = false;
		showReviewPage = false;
		showCallMSFTPage = false;
		showPS3InfoPage = false;
		showCommunicatePS3Refund = false;
		showCommunicateXBOXRefund = false;
		showFinalPage = false;
	}
	//Method to cancel Wizard:
	public PageReference cancel()
	{
		PageReference selectedContactPage = new ApexPages.StandardController(selectedContact).view();
		selectedContactPage.setRedirect(true);
		return selectedContactPage;
	}
	
	public PageReference previousPage()
	{
		if(showBenefitsPage){
			//Rollback:
			currentAbatement.Reiterated_ELITE_Benefits__c = null;
			currentAbatement.Reiterated_ELITE_Result__c = null;
			//Navigate to Sentiment Page:
			setAllPagesFalse();
			showSentimentPage = true;
		}
		else if(showIdentityVerificationPage){
			//Rollback:
			currentAbatement.User_Is_Verified__c = null;
			setAllPagesFalse();
			//Navigate to proper page:
			if(previousPage == 'Sentiment'){
				setAllPagesFalse();
				showSentimentPage = true;
			}
			else if (previousPage == 'Benefits'){
				setAllPagesFalse();
				showBenefitsPage = true;
			}
		}
		else if(showLinkedAccountsPage){
			//Rollback:
			selectedMPAccount = new Multiplayer_Account__c();
			//Navigate to proper page:
			if(previousPage == 'Sentiment'){
				setAllPagesFalse();
				showSentimentPage = true;
			}
			else if(previousPage == 'Benefits'){
				setAllPagesFalse();
				showBenefitsPage = true;
			}
			else if(previousPage == 'Identity'){
				setAllPagesFalse();
				showIdentityVerificationPage = true;
			}
		}
		else if(showRemedyPage){
			//Rollback:
			currentAbatement.awarded2XP__c = null;
			currentAbatement.awarded2XP_Result__c = null;
			currentAbatement.Customer_Requested_Refund__c = null;
			//Navigate to proper page:
			if(previousPage == 'Linked')
			{
				setAllPagesFalse();
				showLinkedAccountsPage = true;
			}
		}
		else if(showReviewPage){
			//No Rollback needed:
			//Navigate to proper page:
			if(previousPage == 'Benefits'){
				setAllPagesFalse();
				showBenefitsPage = true;
			}
			else if(previousPage == 'Remedy'){
				setAllPagesFalse();
				showRemedyPage = true;
			}
			else if(previousPage == 'XBOXRefund'){
				setAllPagesFalse();
				showCommunicateXBOXRefund = true;
			}
			else if(previousPage == 'PS3Refund'){
				setAllPagesFalse();
				showCommunicatePS3Refund = true;
			}
			else if(previousPage == 'PS3Info'){
				setAllPagesFalse();
				showPS3InfoPage = true;
			}
			else if(previousPage == 'CallMSFT'){
				setAllPagesFalse();
				showCallMSFTPage = true;
			}
		}
		else if(showCallMSFTPage){
			//Rollback Changes:
			currentAbatement.redirectedToMSFT__c = null;
			//Navigate:
			if(previousPage == 'Linked'){
				setAllPagesFalse();
				showLinkedAccountsPage = true;
			}
			else if(previousPage == 'Remedy'){
				setAllPagesFalse();
				showRemedyPage = true;
			}
		}
		else if(showPS3InfoPage){
			//Rollback Changes:
			currentAbatement.Locale__c = null;
			currentAbatement.PS3_Sign_In_ID__c = null;
			//Navigate:
			setAllPagesFalse();
			showCommunicatePS3Refund = true;			
		}
		else if(showCommunicatePS3Refund){
			//Rollback Changes:
			currentAbatement.Communicated_PS3_Refund_Process__c = null;
			//Navigate:
			if(previousPage == 'Linked'){
				setAllPagesFalse();
				showLinkedAccountsPage = true;
			}
			else if(previousPage == 'Remedy'){
				setAllPagesFalse();
				showRemedyPage = true;
			}
		}
		else if(showCommunicateXBOXRefund){
			//Rollback Changes:
			currentAbatement.Communicated_XBOX_Refund_Process__c = null;
			//Navigate:
			setAllPagesFalse();
			showLinkedAccountsPage = true;
		}
		return null;
	}
	
	
	//Constructor:
	public Elite_Abatement_Wizard_Controller(ApexPages.StandardController controller)
	{
		errorMessage = '';
		confirmationPageMessage = '';
		
		setAllPagesFalse();
		
		selectedContact = (Contact)controller.getRecord();
		selectedContact = [SELECT id, Name, Email FROM Contact WHERE id = :selectedContact.id];
		
		selectedMPAccount = new Multiplayer_Account__c();
		
		//Create new Abatement:
		if(currentAbatement == null)
		{
			currentAbatement = new Abatement__c();
			currentAbatement.Gamer__c = selectedContact.id;
		}
		
		//Retrieve Linked Premium Accounts:
		if(linkedPremiumAccounts ==  null)
		{
			linkedPremiumAccounts = [SELECT id, Name, Contact__c, Gamertag__c, DWID__c, Platform__c, Is_Current__c, Is_Premium__c, Is_Founder__c, Premium_Start__c, Premium_End__c FROM Multiplayer_Account__c WHERE Contact__c = :selectedContact.Id AND Is_Current__c = True AND Is_Premium__c = True];
			if(linkedPremiumAccounts.size() > 0)
			{
				contactHasLinkedPremiumAccounts = true;
				premiumAccountList = new List<SelectOption>();
				for(Multiplayer_Account__c mpAcct : linkedPremiumAccounts)
				{
					premiumAccountList.add(new SelectOption(mpAcct.Gamertag__c, mpAcct.Gamertag__c));
				}
			}
			else
			{
				contactHasLinkedPremiumAccounts = false;			
			}
		}
		//Send to Sentiment Page:
		showSentimentPage = true;	
	}
	
	public pageReference sentimentCompleted()
	{ 
		if(currentAbatement.Contact_Method__c == 'Phone' || currentAbatement.Contact_Method__c == 'Chat')
		{
		}
		else{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'You must select a Contact Method to proceed.');
			ApexPages.addMessage(myMsg);
			return null;
		}
		
		if(currentAbatement.Sentiment__c == 'Upset')
		{
			setAllPagesFalse();
			showBenefitsPage = true;
			previousPage = 'Sentiment';
			return null;
		}
		else if(currentAbatement.Sentiment__c == 'Legal')
		{
			setAllPagesFalse();
			if(currentAbatement.Contact_Method__c == 'Phone')
			{
				showIdentityVerificationPage = true;
				previousPage = 'Sentiment';
			}
			else
			{
				showLinkedAccountsPage = true;
				previousPage = 'Sentiment';
			}
			return null;
		}
		else
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'You must select a sentiment to proceed.');
			ApexPages.addMessage(myMsg);
			return null;
		}
	}
	
	public pageReference benefitsCompleted()
	{
		if(currentAbatement.Reiterated_ELITE_Benefits__c == 'Yes')
		{
			if(currentAbatement.Reiterated_ELITE_Result__c == 'Not Satisfied')
			{
				setAllPagesFalse();
				if(currentAbatement.Contact_Method__c == 'Phone')
				{
					showIdentityVerificationPage = true;
					previousPage = 'Benefits';
				}
				else
				{
					showLinkedAccountsPage = true;
					previousPage = 'Benefits';
				}
				return null;
			}
			else if(currentAbatement.Reiterated_ELITE_Result__c == 'Satisfied')
			{
				confirmationPageMessageScript = 'I hope that I was able to address any questions or concerns you may have had regarding this announcement and we hope that you continue to enjoy the benefits of your ELITE Premium. Thank you very much for contacting Activision Customer Support and have a fantastic day!';
				confirmationPageMessage = 'Please Review the Information below and press SAVE to create a case';
				setAllPagesFalse();
				showReviewPage = true;
				previousPage = 'Benefits';
				return null;
			}
			else
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'You must supply a customer response.');
				ApexPages.addMessage(myMsg);
				return null;
			}
		}
		else
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'You must reiterate the ELITE Premium Benefits before proceeding.');
			ApexPages.addMessage(myMsg);
			return null;
		}	
	}
	
	public pageReference identityVerificationCompleted()
	{
		if(currentAbatement.User_Is_Verified__c == 'Yes')
		{
			setAllPagesFalse();
			showLinkedAccountsPage = true;
			previousPage = 'Identity';
			return null;
		}
		else
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'You must verify the contact\'s identity before proceeding.');
			ApexPages.addMessage(myMsg);
			return null;
		} 
	}
	
	public pageReference linkedAccountsVerificationCompleted()
	{
		if(selectedGamertag != null)
		{
			for(Multiplayer_Account__c mpAcct : linkedPremiumAccounts)
			{
				if(mpAcct.Gamertag__c == selectedGamertag)
				{
					//Check for Duplicate Submission:
					List<Abatement__c> previousAbatements = [SELECT id, Multiplayer_Account__r.DWID__c, Sentiment__c, createdDate FROM Abatement__c WHERE Sentiment__c = :currentAbatement.Sentiment__c AND Multiplayer_Account__r.DWID__c = :mpAcct.DWID__c LIMIT 1];
					if(previousAbatements.size() > 0)
					{
						ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, mpAcct.Gamertag__c + 'had a previous ' + previousAbatements[0].Sentiment__c + ' submission on: ' + previousAbatements[0].createdDate + 'GMT');
						ApexPages.addMessage(myMsg);
						if(currentAbatement.Sentiment__c == 'Legal')
						{
							ApexPages.Message msg2 = new ApexPages.Message(ApexPages.Severity.INFO, 'After review, it looks as if there is already a refund request in queue for this account and it is currently being processed. Please allow 4 to 6 weeks for the request to be completed.');
							ApexPages.addMessage(msg2);
						}
						return null;
					}
					
					currentAbatement.Multiplayer_Account__c = mpAcct.id;
					selectedMPAccount = mpAcct;
					
					if(currentAbatement.Sentiment__c == 'Upset')
					{
						setAllPagesFalse();
						showRemedyPage = true;
						previousPage = 'Linked';			
						return null;
					}
					else //LEGAL
					{
						date startDate = date.parse('09/15/2012');
						if(selectedMPAccount.Premium_Start__c > startDate)
						{
							if(selectedMPAccount.Platform__c == 'XBL')
							{
								//Redirect to Microsoft:
								currentAbatement.redirectedToMSFT__c = 'Yes';
								setAllPagesFalse();
								showCallMSFTPage = true;
								previousPage = 'Linked';			
								return null;
							}
							else
							{
								//Communicate PS3 Refund:
								setAllPagesFalse();
								showCommunicatePS3Refund = true;	
								previousPage = 'Linked';
								return null;
							}
						}
						else
						{
							if(selectedMPAccount.Platform__c == 'XBL') 
							{
								//Send to Communicate Xbox Flow:
								setAllPagesFalse();
								showCommunicateXBOXRefund = true;
								previousPage = 'Linked';
								return null;
							}
							else //PS3
							{
								//Send to Communicate PS3 Refund Flow.	
								setAllPagesFalse();
								showCommunicatePS3Refund = true;
								previousPage = 'Linked';
								return null;
							}
						}
					}
				}
			}
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Invalid Gamertagx.');
			ApexPages.addMessage(myMsg);
			return null;
		}
		else
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'You must select a gamertag to proceed.');
			ApexPages.addMessage(myMsg);
			return null;
		} 
	}
	
	public pageReference awardRemedy()
	{		
		currentRemedy = new Remedy__c();
		currentRemedy.Award__c = 'Double XP';
		currentRemedy.DXP_Interval__c = '2 Hours';
		currentRemedy.Game_Title__c = 'MW3';
		currentRemedy.Comments__c = 'This Remedy Was Generated from the ELITE Premium Wizard on: ' + dateTime.now() + ' GMT';
		currentRemedy.Multiplayer_Account__c = SelectedMPAccount.id; 
		currentRemedy.remedyAwarded__c = false;
		try
		{
			insert currentRemedy;	
			return null;
		}
		catch(exception ex)
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'An exception occured: ' + ex.getMessage());
			ApexPages.addMessage(myMsg);
			return null;
		}
	}
	
	public pageReference remedyCompleted()
	{
		if(currentAbatement.awarded2XP_Result__c == 'Satisfied')
		{
			if(currentAbatement.awarded2XP__c != 'Yes')
			{
				currentAbatement.awarded2XP_Result__c = '';
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'You must Award Double XP to Proceed.');
				ApexPages.addMessage(myMsg);
				return null;
			}
			
			confirmationPageMessage = 'Please review the following information, then click SAVE to create a case.';
			confirmationPageMessageScript = 'Please enjoy your Double XP! Thanks again for contacting Activsion Customer Support - SCRIPT NEEDED HERE';
			setAllPagesFalse();
			showReviewPage = true;	
			previousPage = 'Remedy';			
			return null;
		}
		else if(currentAbatement.awarded2XP_Result__c == 'Not Satisfied' || (currentAbatement.Customer_Requested_Refund__c == 'Yes'))
		{
			//Check for 30 Days:
			date startDate = date.parse('09/15/2012');
			if(selectedMPAccount.Premium_Start__c > startDate)
			{
				if(selectedMPAccount.Platform__c == 'XBL')
				{
					//Redirect to Microsoft:
					currentAbatement.redirectedToMSFT__c = 'Yes';
					setAllPagesFalse();
					showCallMSFTPage = true;
					previousPage = 'Remedy';	
					return null;
				}
				else
				{
					setAllPagesFalse();
					showCommunicatePS3Refund = true;	
					previousPage = 'Remedy';
					return null;
				}
			}
			else
			{
				confirmationPageMessage = 'This customer purchased ELITE more than 30 days ago press SAVE to create this case';
				confirmationPageMessageScript = 'I\'m sorry but double XP is all I am authorized to offer at this time.';
				setAllPagesFalse();
				showReviewPage = true;
				previousPage = 'Remedy';
				return null;
			}
		}
		else
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'You must select a customer response.');
			ApexPages.addMessage(myMsg);
			return null;
		}
	}
	
	public pageReference refundCommunicated()
	{
		if(selectedMPAccount.Platform__c == 'XBL')
		{
			if(currentAbatement.Communicated_XBOX_Refund_Process__c != 'Yes')
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'You must communicate the Xbox Refund Process before proceeding.');
				ApexPages.addMessage(myMsg);
				return null;
			}
			
			
			//Send to Confirmation Page:
			confirmationPageMessageScript = 'I have processed your request.';
			confirmationPageMessage = 'Please confirm these details and press SAVE to create a case.';
			setAllPagesFalse();
			showReviewPage = true;
			previousPage = 'XBOXRefund';
			return null;
		}
		else
		{
			if(currentAbatement.Communicated_PS3_Refund_Process__c != 'Yes')
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'You must communicate the PS3 Refund Process before proceeding.');
				ApexPages.addMessage(myMsg);
				return null;
			}
			
			//Send to PS3 Details Page:
			setAllPagesFalse();	
			showPS3InfoPage = true;
			previousPage = 'PS3Refund';
			return null;
		}
	}
	
	public pageReference ps3InfoCollected()
	{
		if(currentAbatement.Locale__c == '' || currentAbatement.Locale__c == null)
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'You must collect the customer\'s Locale.');
			ApexPages.addMessage(myMsg);
			return null;
		}
		
		if(currentAbatement.PS3_Sign_In_ID__c != '' && currentAbatement.PS3_Sign_In_ID__c != null)
		{
			confirmationPageMessage = 'Please confirm these details.';
			setAllPagesFalse();
			showReviewPage = true;
			previousPage = 'PS3Info';
			return null;
		}
		else
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'You must collect the customer\'s Sign-In ID.');
			ApexPages.addMessage(myMsg);
			return null;
		}
	}
	
	public pageReference reviewCompleted()
	{
		try
		{
			insert currentAbatement;
		}
		catch(Exception ex)
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'An error occured while writing to the database: ' + ex.getMessage());
			ApexPages.addMessage(myMsg);
			return null;
		}
		
		//Create Case:
		currentCase = new Case();
		RecordType tokenRequestType = [SELECT id FROM RecordType WHERE Name = 'Token Request' LIMIT 1];
		currentCase.RecordTypeId = tokenRequestType.id;
		currentCase.ContactId = selectedContact.id;
		currentCase.Type = 'Administration';
		currentCase.Sub_Type__c = 'Refund';
		currentCase.Status = 'New';
		Public_Product__c eliteProduct = [SELECT id FROM Public_Product__c WHERE Name LIKE '%Call of Duty: ELITE%' LIMIT 1]; 
		currentCase.Product_Effected__c = eliteProduct.id;
		if(currentAbatement.Contact_Method__c == 'Chat')
		{
			currentCase.Origin = 'Chat';
		}
		else
		{
			currentCase.Origin = 'Phone Call';
		}
		
		
		currentCase.subject = 'ELITE Premium Membership';
		if(selectedMPAccount.Platform__c != null)
		{
			currentCase.subject +=  '- ' + selectedMPAccount.Platform__c;
		}
		currentCase.description = '';
		insert currentCase;
		
		//Create Mandatory Comment:
		CaseComment comment = new CaseComment();
		comment.CommentBody = ' Abatement Reference ID: ' + currentAbatement.ID;
		comment.ParentId = currentCase.id;
		insert comment;
		
		//Create Article:
		CaseArticle article = new CaseArticle();
		//article.KnowledgeArticleId = 'kA0J0000000CdfU'; //DEV
		article.KnowledgeArticleId = 'kA0U0000000D3be'; //PROD
		article.CaseId = currentCase.id;
		insert article;
		
		RecordType generalSupportType = [SELECT id FROM RecordType WHERE Name = 'General Support' LIMIT 1];
		currentCase.RecordTypeId = generalSupportType.id;
		update currentCase;

		setAllPagesFalse();
		showFinalPage = true;
		return null;
	}
	
	public pageReference callMSFTComplete()
	{
		if(currentAbatement.redirectedToMSFT__c == 'Yes')
		{
			confirmationPageMessage = 'Please review the following information then choose SAVE to create a case.';
			confirmationPageMessageScript = 'I have provided you with the phone number for Microsoft, is there anything else I can assist you with today?';
			setAllPagesFalse();
			showReviewPage = true;
			previousPage = 'CallMSFT';
		}
		else
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Please direct the customer to Microsoft before you continue');
			ApexPages.addMessage(myMsg);
		}
		return null;
	}
	
	public pageReference goToCase()
	{
		PageReference casePage = new PageReference('/' + currentCase.id);
		return casePage;
	}
	
}