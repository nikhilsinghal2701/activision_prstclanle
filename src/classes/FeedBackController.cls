// @lastModified     29-NOV-2012 - Security Review: JA

public with sharing Class FeedBackController{
    //passed in from component
    public string ArticleID{get;
    
    set{
        ArticleID=value;
        checkSubmitted();
    }
    }
    public string ArticleNum{get;set;}
    public string ArticleTitle{get;set;}
    public string Game{get;set;}
    
    //Derived
    public string Comments{get;set;}
    public string contactIdOfUser{get;set;}
    public boolean Deflection{get;set;}
    public string FBsource{get;set;}
    public string Keyword{get;set;}
    public string SessionID{get;set;}
    public boolean Recorded{get;set;}
    public boolean Recheck{get;set;}
    
    
    public FeedBackController(){
        getContactIdOfUser();
        Recheck=True;
        //checkSubmitted();
    }
    
    public void CreateFB(){    
        system.debug('+++++++++++++++recorded++++'+ Recorded );
        if (Recorded != True){
        pkb_Article_Feedback_Deflection__c fb = new pkb_Article_Feedback_Deflection__c();
        fb.Article_ID__c = ArticleID;
        fb.Article_Number__c = ArticleNum;
        fb.Article_Title__c = ArticleTitle;
        fb.Comments__c = Comments;
        fb.Deflection__c = True;
        fb.Feedback_Source__c = ApexPages.currentPage().getParameters().get('fs');
        fb.Keyword_Searched__c = ApexPages.currentPage().getParameters().get('q');
        fb.Session_ID__c = userinfo.getSessionId();
        fb.game__c = game.replace('Game_Title:','');
        insert fb;
        }        
        Recorded=True;
        Recheck=False;       
    }
    public void CreateFBComment(){
        if (Recorded != True){
        system.debug('******' + ArticleID + '******');
        system.debug('*******' + ArticleNum + '*****');
        pkb_Article_Feedback_Deflection__c fb = new pkb_Article_Feedback_Deflection__c();
        fb.Article_ID__c = ArticleID;
        fb.Article_Number__c = ArticleNum;
        fb.Article_Title__c = ArticleTitle;
        fb.Comments__c = Comments;
        fb.Deflection__c = False;
        fb.Feedback_Source__c = ApexPages.currentPage().getParameters().get('fs');
        fb.Keyword_Searched__c = ApexPages.currentPage().getParameters().get('q');
        fb.Session_ID__c = userinfo.getSessionId();
        fb.game__c = game.replace('Game_Title:','');
        insert fb;
        }
        
        Recorded=True;
        Recheck=False;
    }
    
    public void checkSubmitted(){
        if (UserInfo.getname()=='Atvi Support Site Site Guest User' && recheck==True){
            Recorded=False;
        }
        else{
            list<pkb_Article_Feedback_Deflection__c> fbList = new list<pkb_Article_Feedback_Deflection__c>();
            fbList = [Select Id from PKB_Article_Feedback_Deflection__c where Article_Number__c =:ArticleNum and Session_ID__c =:userinfo.getSessionId()];
            if (fbList.size()>0){
                Recorded=True;
            }
            Else{
                Recorded=False;
            }
        }
    }
    
    //Determines which Contact record is associated with the User record of the current user
    public void getContactIdOfUser() {
        string userId = Userinfo.getUserId();    
                
        List<User> userRec = [SELECT Id, Contact.Id FROM User 
                                                  WHERE Id =: userId limit 1];  
        if(userRec.size()>0) {        
            User u=userRec.get(0);        
            if(u.Contact!=null) {            
                contactIdOfUser=u.Contact.Id;            
                system.debug('Contact Id for the user ['+UserInfo.getUserName()+'] is:'+contactIdOfUser);        
            }else {             
                system.debug('No contact associated with this user');               
            }    
        }
    }
}