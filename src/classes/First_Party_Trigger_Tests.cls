@isTest
public class First_Party_Trigger_Tests{
    public static testMethod void test(){
        User u = [SELECT id FROM User WHERE ContactId = null AND IsActive = TRUE LIMIT 1];
        system.runAs(u){
            //test that if the field is empty the owner will not change
            First_Party_Partner_Relations_Task__c rec = new First_Party_Partner_Relations_Task__c();
            rec.Due_Date__c = system.TODAY();
            rec.Assigned_To__c = '1234_fake_name_for_test';
            insert rec;
            rec = [SELECT Id, OwnerId FROM First_Party_Partner_Relations_Task__c WHERE Id = :rec.Id];
            system.assertEquals(rec.OwnerId, u.Id);
            
            //test that if the field has a name of an active user that the field does change
            User u2 = [SELECT Id, Name FROM User WHERE IsActive = TRUE AND ContactId = null LIMIT 1];
            rec = new First_Party_Partner_Relations_Task__c();
            rec.Due_Date__c = system.TODAY();
            rec.Assigned_To__c = u2.Name;
            insert rec;
            rec = [SELECT Id, OwnerId FROM First_Party_Partner_Relations_Task__c WHERE Id = :rec.Id];
            system.assertEquals(rec.OwnerId, u2.Id);
            
            //test that if the field has a name of an inactive user that the field does NOT change
        }
    }
}