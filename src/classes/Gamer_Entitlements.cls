public class Gamer_Entitlements{

    //Constructors
    public Gamer_Entitlements(ApexPages.StandardController con){init(con.getId());}
    public Gamer_Entitlements(){init(null);}
    public void init(Id possId){
        MultiPlayerAccounts = new selectOption[]{};
        platformSelect = new selectOption[]{};
        platformSelect.add(new selectOption('','--None--'));
        showAW=false;
        showGhosts=false;
        showNotate=false;
        curUser = [SELECT Id, Tier__c FROM User WHERE Id = :UserInfo.getUserId()];
        if(curUser.Tier__c != null && curUser.Tier__c != '' && curUser.Tier__c.toLowerCase() == 'tier 3') isCurUserTier3 = true;
        else isCurUserTier3 = false;

        system.debug('isProd == '+isProd);
        system.debug('ghostsEndpoint == '+ghostsEndpoint);
        try{
            showdetail = false;
            ent = new Entitlement__c();
            String urlId = possId == null ? ApexPages.currentPage().getParameters().get('id') : possId;
            system.debug('urlId == '+urlId);
            for(Contact ctc : [SELECT Id, Name, UCDID__c FROM Contact WHERE Id = :urlId]){
                gamer = ctc;
                ent.Platform__c = 'all';
                helpIter = 'Contact';
                if(gamer != null || !isBlankOrNull(gamer.ucdid__c)){
                    curMPs = [select id,platform__c,gamertag__c from multiplayer_Account__c where contact__c=:urlId and is_current__c=true];//TODO: Update this so the fact that gamertag is now the id of the multiplayer account is handled.
                }
            }
            if(gamer == null || isBlankOrNull(gamer.ucdid__c)){
                for(Multiplayer_Account__c mp : [SELECT id,platform__c,Contact__c FROM multiplayer_Account__c WHERE id=:urlId]){
                    gamer = [SELECT Id, Name, UCDID__c FROM Contact WHERE Id = :mp.Contact__c];
                    ent.Platform__c = mp.platform__c;
                    helpIter='MP';
                    if(gamer != null || !isBlankOrNull(gamer.ucdid__c)){
                        curMPs = [select id,platform__c,gamertag__c from multiplayer_Account__c where id=:urlId and is_current__c=true];//TODO: Update this so the fact that gamertag is now the id of the multiplayer account is handled.
                    }
                }
            }
            if(gamer == null || isBlankOrNull(gamer.ucdid__c)){
                for(Case c : [SELECT Id, ContactId FROM Case WHERE Id = :urlId]){
                    theCase = c;
                    if(! isBlankOrNull(c.ContactId)){
                        gamer = [SELECT Id, Name, UCDID__c FROM Contact WHERE Id = :theCase.ContactId];
                        ent.Platform__c = 'all';
                        helpIter='Case';
                        if(gamer != null || !isBlankOrNull(gamer.ucdid__c)){
                            curMPs = [select id,platform__c,gamertag__c from multiplayer_Account__c where contact__c=:gamer.id and is_current__c=true];//TODO: Update this so the fact that gamertag is now the id of the multiplayer account is handled.
                        }
                    }
                }
            }          
        }catch(Exception e){
            system.debug('ERROR == '+e);
        }
        if(isProd && !test.isRunningTest()){
            if(gamer.id != null){
                Linked_Accounts_Redirect_Controller larc = new Linked_Accounts_Redirect_Controller(
                    new ApexPages.standardController(new Contact(Id = gamer.id))
                );
                larc.updateProfile();
            }
        }
    }

    //Public properties / variables
    public user curUser;
    public string helpIter{get;set;}
    public list<multiplayer_Account__c> curMPs{get;set;}
    public list<AWEntitlementData> ed{get;set;}
    public selectOption[] MultiPlayerAccounts{get;set;}
    public string gamertag{get;set;}
    public boolean showAW{get;set;}
    public boolean showGhosts{get;set;}
    public boolean showNotate{get;set;}
    public string englishName{get;set;}
    public string itemID{get;set;}
    public string reason{get;set;}
    public selectOption[] platformSelect{get;set;}
    public string ucdid {get; set;}
    public Contact gamer {get; set;}
    public boolean showdetail{get;set;}
    public entitlement__c ent{get;set;} //used to store the current game & platform
    public string[] objRespKeys {get; set;}
    public boolean isCurUserTier3{get; set;}
    public entitlement__c[] entList{get;set;}
    public Id selectedEntitlementId {get; set;}
    public String entitlementApiName {get; set;}
    public Object newEntitlementValue {get; set;}
    public map<String, Object> objResponse {get; set;}
    public Entitlement__c selectedEntitlement {get; set;}

    //these next ones were previously private, but were made public so they could be used by the Clan_Wars class
    public Case theCase {get; set;}
    public map<Id, Entitlement__c> entMap;
    public string headerKey = '@headers-exist@';
    public string bhsGhostsCreds = '@Authorization#Basic Y21zOkVsaXQz';
    public boolean isProd = UserInfo.getOrganizationId() == '00DU0000000HMgwMAG';
    public string devEndpoint = 'https://penv2-api.dev-ca.ghosts.callofduty.com/';
    public string prodEndpoint = 'https://papi.live-ca.ghosts.callofduty.com/';
    public string devAWEndpoint = 'http://shg-gavel.activision.com/';
    //public string env = isProd ? 'live' : 'dev';
    public string env = 'live';
    public string ghostsEndpoint = isProd ? prodEndpoint : devEndpoint;
    public string awDevEndpoint = 'https://aw-dev-api.dev.beachheadstudio.com';
    public string awProdEndpoint = 'https://prod-api.prod.beachheadstudio.com';
    public string awEndpoint = isProd ? awProdEndpoint : awDevEndpoint;
    public string endpoint = ghostsEndpoint;
    public integer priorIntValue {get; set;}
    map<Id, Multiplayer_Account__c> multiPlayerAccountMap = new map<Id, Multiplayer_Account__c>();
    
    public string gameName {get; set;}
    public pageReference chooseGame(){
        if(! isBlankOrNull(gameName)){
            if(gameName.containsIgnoreCase('ghosts')){
                endpoint = ghostsEndpoint;
            }else{
                endpoint = awEndpoint;
            }
        }
        return null;
    }

    public pagereference fetchEntitlements(){
        String chsnPlatform = '';
        MultiplayerAccounts = new SelectOption[]{};
        showAW=false;
        showGhosts=false;
        if(ent.game__c == 'Call of Duty: Ghosts'){
            showGhosts=true;
            ed = new list<awentitlementdata>();
            if(! isBlankOrNull(ent.Platform__c)){
                chsnPlatform = ent.Platform__c.toLowerCase();
            }else{
                chsnPlatform = 'all';
            }
            resetOutput();
            entMap = new map<Id, Entitlement__c>([SELECT Id, Game__c, Platform__c, Group__c, Image__c,  Award_Type__c, Grant_value__c,
                                                         Entitlement_Type__c, Entitlement_Name__c, Entitlement_Api_Name__c,
                                                         Entitlement_Description__c, Entitlement_Grant_Method__c
                                                 FROM Entitlement__c WHERE Game__c = :ent.Game__c AND 
                                                 Platform__c != '' AND
                                                 (Platform__c INCLUDES (:chsnPlatform))]);
            entList = entMap.values();
            entList.sort();

            platformSelect = new selectOption[]{
            new selectOption('all','all')
                ,new selectOption('xbl','xbl')
                ,new selectOption('psn','psn')
                ,new selectOption('pc','pc')
                //,new selectOption('wiiu','Wii U')
            };
        }else if(ent.game__c == 'Call of Duty: Advanced Warfare'){
            showAW=true;
            entList = new entitlement__c[]{};
            platformSelect = new selectOption[]{new selectOption('','--None--')};
            string resp; 
            if(!curMPs.isEmpty()){
                MultiPlayerAccounts.add(new selectOption('','--None--'));
                multiPlayerAccountMap = new map<Id, Multiplayer_Account__c>(curMPs);
                for(multiplayer_Account__c m:curMPs){
                    MultiPlayerAccounts.add(new selectOption(m.id,m.gamertag__c+'('+m.platform__c+')'));
                }
            }       
            try{
                string tempEndPoint = devAWEndpoint;
                tempEndPoint += 'svc/blacksmith/'+env+'/pc/getPossibleEntitlements';
                tempEndPoint += '@headers-exist@@usecsnetworkcreds#true';
                DwProxy.service serv = new DwProxy.service();
                Serv.timeout_x = 120000;
                resp = serv.passEncryptedGetData(tempEndPoint);
                ed = (list<AWEntitlementData>)json.deserialize(resp,list<AWEntitlementData>.class);
            }catch(Exception e){
                if(resp == null) resp = e.getMessage();
                addMessageToPage(ApexPages.SEVERITY.FATAL, resp);
                system.debug(e);
            }
        }else{
            showAW=false;
            showGhosts=false;
            ed = new list<awentitlementdata>();
            entList = new entitlement__c[]{};
        }
        return null;
    }
    
    public Entitlement__c loadEntitlement(Id entId){
        return [SELECT Id, entitlement_description__c, entitlement_type__c, entitlement_api_name__c, Group__c,
                Entitlement_Grant_Method__c 
                FROM Entitlement__c WHERE Id = :entId];
    }
    
    public pageReference getEntitlement(){
        String resp;
        try{
            selectedEntitlement = loadEntitlement(selectedEntitlementId);
            DwProxy.service serv = new DwProxy.service();
            Serv.timeout_x = 120000;
            if(ent.Game__c == 'Call of Duty: Ghosts'){
                String call = ghostsEndpoint+'entitlements?ucdid='+gamer.ucdid__c+'&key='+entitlementApiName+headerKey+bhsGhostsCreds;
                priorIntValue = null;
                system.debug('call == '+call);
                resp = serv.dwGetProxy(call);
                system.debug('response == '+resp);
                parseJSON(resp);
                showDetail = true;
            }
        }catch(Exception e){
            if(resp == null) resp = e.getMessage();
            addMessageToPage(ApexPages.SEVERITY.FATAL, resp);
            system.debug(e);
        }
        return null;
    }

    public pageReference addEntitlement(){
        try{
            //if(! isCurUserTier3){ //SD#00125512
              //  addMessageToPage(ApexPages.Severity.FATAL, 'Unauthorized');
                //return null;
            //}
            DwProxy.service serv = new DwProxy.service();
            serv.timeout_x = 120000;
            string baseUrl = '', body = '', result = '';
            boolean boolVal;
            integer intVal;
            
            system.debug('newEntitlementValue == '+newEntitlementValue);
            
            if(isBlankOrNull(gamer.ucdId__c)){
                addMessageToPage(ApexPages.Severity.FATAL, 'ucdid is missing, cannot process');
                return null;
            }
            if(isBlankOrNull(entitlementApiName)){
                addMessageToPage(ApexPages.Severity.FATAL, 'entitlement api name is missing, cannot process');
                return null;
            }
            if(isBlankOrNull(ent.Platform__c)){
                addMessageToPage(ApexPages.Severity.FATAL, 'platform is missing, cannot process');
                return null;
            }
            if(selectedEntitlement.Entitlement_Type__c.toLowerCase() != 'boolean'){
                try{
                    Integer didTheyEnterLetters = Integer.valueOf(newEntitlementValue);
                }catch(Exception e){
                    addMessageToPage(ApexPages.Severity.FATAL, 'Whole numbers only');
                    return null;
                }
            }
            
            if(ent.Game__c == 'Call of Duty: Ghosts'){
                baseUrl = ghostsEndpoint+'entitlements'+headerKey+bhsGhostsCreds;
                if(selectedEntitlement.Entitlement_Type__c.toLowerCase() == 'boolean'){
                    boolVal = Boolean.valueOf(newEntitlementValue);
                    body = JSON.serialize(new boolEntitlement(gamer.ucdId__c, entitlementApiName, boolVal, ent.Platform__c));
                }else{
                    intVal = Integer.valueOf(newEntitlementValue);
                    body = JSON.serialize(new inteEntitlement(gamer.ucdId__c, entitlementApiName, intVal, ent.Platform__c));
                }
                system.debug('body == '+body);
            }
            
            if(selectedEntitlement.Entitlement_Grant_Method__c == 'POST'){
                result = serv.passEncryptedPostData(baseUrl,body,'application/json');
            }else if(selectedEntitlement.Entitlement_Grant_Method__c == 'PUT'){
                result = serv.passEncryptedPutData(baseUrl,body,'');
            }else if(selectedEntitlement.Entitlement_Grant_Method__c == 'GET'){
                result = serv.passEncryptedGetData(baseUrl);
            }
            system.debug('result == '+result);
            //handle result by updating the log and reporting the status to the agent
            Entitlement_Grant_Log__c log = new Entitlement_Grant_Log__c(
                Game__c = ent.Game__c,
                Entitlement__c = entitlementApiName,
                Granted_By__c = UserInfo.getUserId(),
                Granted_To__c = gamer.Id,
                New_Value__c = boolVal == null ? string.valueOf(intVal) : string.valueOf(boolVal),
                Raw_command__c = body,
                Raw_response__c = result
            );
            if(theCase != null){
                log.Case__c = theCase.id;
            }
            log.Status__c = testResult(result);
            resetOutput();
            insert log;
        }catch(Exception e){
            system.debug('ERROR == '+e);
        }
        return null;
    }
    
    public void addMessageToPage(ApexPages.SEVERITY level, String message){
        system.debug('ERROR == '+message);
        if(! isBlankOrNull(message) && ApexPages.currentPage() != null){
            ApexPages.addMessage(new ApexPages.Message(level, message));
        }
    }
    
    public string testResult(String resultStr){
        boolean successful = false;
        Object result;
        if(resultStr.contains('"value"')){
            parseJson(resultStr);
            result = objResponse.get('value');
            if(entMap.get(selectedEntitlementId).Entitlement_Type__c.toLowerCase() == 'boolean'){
                if(result == true || result == 1 || result == '1' || result == 'true'){
                    successful = boolean.valueOf(newEntitlementValue) == true;
                }else{
                    successful = boolean.valueOf(newEntitlementValue) == false;
                }
            }else{
                //this will only work for additive integer entitlements
                system.debug('priorIntValue == '+priorIntValue);
                system.debug('newEntitlementValue == '+newEntitlementValue);
                system.debug('result == '+result);
                successful = priorIntValue + integer.valueOf(newEntitlementValue) == result;
            }
        }
        system.debug('test result == '+result+'~~newEntitlementvalue == '+newEntitlementValue+'......successful == '+successful);
        if(successful){
            priorIntValue = null;
            return 'Successful';
        }
        addMessageToPage(ApexPages.Severity.FATAL, resultStr);
        return 'Failed';
    }
    
    public boolean isBlankOrNull(String s){ return s == '' || s == null; }
    
    public class inteEntitlement{
        public string ucdid {get; set;}
        public string key {get; set;}
        public integer value {get; set;}
        public string platform {get; set;}
        public inteEntitlement(String pUcdid, String pKey, Integer pValue, String pPlatform){
            this.ucdid = pUcdid;
            this.key = pKey;
            this.value = pValue;
            this.platform = pPlatform;
        }
    }
    
    public class boolEntitlement{
        public string ucdid {get; set;}
        public string key {get; set;}
        public integer value {get; set;}
        public string platform {get; set;}
        public boolEntitlement(String pUcdid, String pKey, Boolean pValue, String pPlatform){
            this.ucdid = pUcdid;
            this.key = pKey;
            this.value = pValue == true ? 1 : 0;
            this.platform = pPlatform;
        }
    }
    
    public void resetOutput(){
        objResponse = new map<String, object>();
        objRespKeys = new string[]{};
        showDetail = false;
    }
    //instead of returning <ent_id>:0 for entitlements that a gamer doesn't have
    //BHS is returning "entitlements":{}, so we have to test for the absence 
    public boolean entitlementPresent {get; set;}
    public void parseJSON(String response){//resp is a JSON string
        entitlementPresent = false;
        JSONParser parser = JSON.createParser(response);
        resetOutput();
        JSONToken typ;
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                String fieldName = parser.getText();
                if(fieldName.contains(':') && ! fieldName.contains(':'+ent.Platform__c)){
                    continue;
                }
                if(fieldName.contains(entitlementApiName+':'+ent.Platform__c)){
                    entitlementPresent = true;
                    fieldName = '_'+fieldName;
                }
                parser.nextToken();
                typ = parser.getCurrentToken();
                if(typ == JSONToken.VALUE_NUMBER_INT){
                    Long lVal = parser.getLongValue();
                    if(entMap.get(selectedEntitlementId).Entitlement_Type__c.toLowerCase() == 'boolean'){
                        try{
                            Boolean b;
                            if(lVal == 0) b = false;
                            else if(lVal == 1) b = true;
                            objResponse.put(fieldName, b);
                        }catch(Exception e){
                            objResponse.put(fieldName, parser.getLongValue());
                        }
                    }else{
                        if(! fieldName.equalsIgnoreCase('ucdid') && 
                           ! fieldName.equalsIgnoreCase('value') && 
                           priorIntValue == null)
                        {
                            priorIntValue = parser.getIntegerValue();
                            objResponse.put(fieldName, '');
                        }else{
                            objResponse.put(fieldName, lVal);
                        }
                    }
                }else if(typ == JSONToken.VALUE_NUMBER_FLOAT){
                    objResponse.put(fieldName, String.valueOf(parser.getIntegerValue()));
                }else if(typ == JSONToken.VALUE_STRING){
                    objResponse.put(fieldName, parser.getText());
                }else if(typ == JSONToken.VALUE_FALSE){
                    objResponse.put(fieldName, false);
                }else if(typ == JSONToken.VALUE_TRUE){
                    objResponse.put(fieldName, true);
                }                                
            }
        }
        if(! entitlementPresent){
            Object val = false;
            if(entMap.get(selectedEntitlementId).Entitlement_Type__c.toLowerCase() != 'boolean'){
                val = 0;
            }
            objResponse.put('_'+entitlementApiName, val);
        }
        this.objRespKeys.addAll(this.objResponse.keyset());
        this.objRespKeys.sort();
    }

    public pagereference refreshOptions(){
        if(ent.game__c == 'Call of Duty: Ghosts'){
            showGhosts = true;
        }else if(ent.game__c == 'Call of Duty: Advanced Warfare'){
            showAW = true;
        }
        return null;
    }
    public pageReference GTEntitlements(){
        if(ent.game__c == 'Call of Duty: Advanced Warfare'){
            string resp;
            system.debug('gamertag:'+gamertag);
            gamertag = getGamerTagFromMap(gamertag);
            system.debug('entPlatform:'+ent.platform__c);
            system.debug('env:'+env);
            ed = new list<AWEntitlementData>();
            try{
                string tempEndPoint = devAWEndpoint;
                tempEndPoint += 'svc/blacksmith/'+env+'/'+ent.platform__c+'/getEntitlements/'+gamertag;
                system.debug('tempEndpoint:'+tempEndpoint);
                tempEndPoint += '@headers-exist@@usecsnetworkcreds#true';
                DwProxy.service serv = new DwProxy.service();
                Serv.timeout_x = 120000;
                if(!test.isRunningTest()){
                    resp = serv.passEncryptedGetData(tempEndPoint);
                }else{
                    resp='';
                }
                if(resp.startswith('<')){
                    resp = 'User ID not found';
                    addMessageToPage(ApexPages.SEVERITY.FATAL, resp);
                }else{
                    ed = (list<AWEntitlementData>)json.deserialize(resp,list<AWEntitlementData>.class);
                }
            }catch(exception e){
                if(resp == null) resp = e.getMessage();
                resp = 'Service is Down Notify Admin';
                addMessageToPage(ApexPages.SEVERITY.FATAL, resp);    
                system.debug(e);
            }
        }
        return null;
    }
    public pageReference ToggleEntitlement(){
        string resp;
        string body;
        string stat = 'Successful';
        try{
            string tempEndPoint = devAWEndpoint;
            tempEndPoint += 'svc/blacksmith/'+env+'/'+ent.platform__c+'/grantEntitlement';
            tempEndPoint += '@headers-exist@@usecsnetworkcreds#true';
            string contenttype = 'application/json';
            gamertag = getGamerTagFromMap(gamertag);
            body = '{"DurationInMinutes":1.26743233E+15,"EntitlementId":"'+itemID+'","UserId":"'+gamertag+'"}';
            system.debug('body:'+body);
            DwProxy.service serv = new DwProxy.service();
            Serv.timeout_x = 120000;
            if(!test.isRunningTest()){
                resp = serv.passEncryptedPostData(tempEndPoint, body, contenttype);
            }else{
                resp='test';
            }
            if(resp.startswith('<')){
                resp = 'Error: Malformed Request';
                addMessageToPage(ApexPages.SEVERITY.FATAL, resp);
                stat='Failed';
            }
        }catch(exception e){
            if(resp == null) resp = e.getMessage();
            resp = 'Service is Down Notify Admin';
            system.debug(e);
        }

        Entitlement_Grant_Log__c log = new Entitlement_Grant_Log__c(
            Game__c = ent.Game__c,
            Entitlement__c = englishName,
            Granted_By__c = UserInfo.getUserId(),
            Granted_To__c = gamer.Id,
            New_Value__c = resp,
            Raw_command__c = body,
            Raw_response__c = resp
        );
        if(theCase != null){
            log.Case__c = theCase.id;
            if(stat=='Successful'){
                case_comment__c c = new case_comment__c();
                c.case__c = theCase.id;
                c.comments__c = reason;
                insert c;
            }
        }
        log.Status__c = stat;
        insert log;
        showNotate = !showNotate;
        return null;
    }
    public string getGamerTagFromMap(String multiplayerAccountId){
        String result;
        try{
            result = multiPlayerAccountMap.get(multiplayerAccountId).Gamertag__c;
        }catch(Exception e){}
        return result;
    }
    public selectOption[] getConsolePicklist(){
        if(ent.game__c == 'Call of Duty: Ghosts'){
            return new selectOption[]{
            new selectOption('all','all')
                ,new selectOption('xbl','xbl')
                ,new selectOption('psn','psn')
                ,new selectOption('pc','pc')
                //,new selectOption('wiiu','Wii U')
                };
        }else if(ent.game__c == 'Call of Duty: Advanced Warfare'){
            return new selectOption[]{
                new selectOption('','--None--')
                ,new selectOption('ps4','PS4')
                ,new selectOption('xboxone','XB1')
                ,new selectOption('ps3','PS3')
                ,new selectOption('xbox','Xbox360')
                ,new selectOption('pc','PC')
                //,new selectOption('wiiu','Wii U')
            };
        }
        return new selectOption[]{new selectOption('','--none--')};
    }

    public pageReference refreshPlatform(){
        system.debug('multiplayeraccountid == '+gamertag);
        selectOption[] temp = new selectOption[]{};
        temp.add(new selectOption('','--None--'));
        for(multiplayer_Account__c m:curMPs){
            if(m.id == gamertag){
                if(m.platform__c == 'XBL'){
                    temp.add(new selectOption('xboxone','XB1'));
                    temp.add(new selectOption('xbox','Xbox360'));
                }else if(m.platform__c == 'PSN'){
                    temp.add(new selectOption('ps4','PS4'));
                    temp.add(new selectOption('ps3','PS3'));
                }else if(m.platform__c == 'steam' || m.platform__c == 'pc'){
                    temp.add(new selectOption('pc','PC'));
                }
            }
        }
        platformSelect = temp;    
        return null;
    }

    public pageReference toggleNotate(){
        system.debug('gamertag:'+gamertag);//TODO: Update this so the fact that gamertag is now the id of the multiplayer account is handled.
        system.debug('ent.Platform__c:'+ent.platform__c);
        if(gamertag==null || ent.platform__c==null)//TODO: Update this so the fact that gamertag is now the id of the multiplayer account is handled.
        {
            addMessageToPage(ApexPages.SEVERITY.FATAL, 'No Changes Committed. Please select UserID and Platform');
            return null;
        }
        system.debug('toggleNotate get called');
        showNotate = !showNotate;//true;
        system.debug('showNotate now ='+showNotate);
        return null;
    }
}