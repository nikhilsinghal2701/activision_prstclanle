public class Gamer_Entitlements_Clan_Wars{
    DwProxy.service serv = new DwProxy.service();
    Gamer_Entitlements ge;
    public string game {get; set;}
    public string platform {get; set;}
    public Id theCaseId {get; set;}
    String ucdIdDec, selectedNetwork = '';
    public Gamer_Entitlements_Clan_Wars(Gamer_Entitlements geCont){
        ge = geCont;
        if(ge.theCase != null){theCaseId = ge.theCase.Id;}
        init();
    }
    public Gamer_Entitlements_Clan_Wars(ApexPages.StandardController cont){
        init();
        theCaseId = cont.getId();
    }
    void init(){
        serv.timeout_x = 120000;
        displayClanDetails = false;
        displayChangePopOver = false;
        showDataLayer3 = false;
        resetLog();
    }
    public boolean getIsCurUserTier3(){
        if(ge == null) ge = new Gamer_Entitlements();
        return ge.isCurUserTier3;
    }
    
    //Step 1 - Find the clan
    public string searchString{get; set;}
    public string debugResponse;
    public pageReference searchClans(){
        searchResponseObject = new bhsSearchResponse();
        searchError = null;
        String response;
        try{
            ge.init(null);
            searchString = EncodingUtil.urlEncode(String.escapeSingleQuotes(searchString),'UTF-8');
            //String query = ge.ghostsEndpoint+'search?query='+searchString+'&clansOnly=true'+ge.headerKey+ge.bhsGhostsCreds;
            String query = ge.endpoint+'search?query='+searchString+'&clansOnly=true';
            if(game.containsIgnoreCase('ghosts')) query += ge.headerKey+ge.bhsGhostsCreds;
            else{ //TODO: Update for AW
            
                DwProxy.service svc = new DwProxy.service();
                 String sesssIon_token ='';
                if(!test.isrunningtest())sesssIon_token = svc.generateAWsessionId(Label.AWclanUcdid);
                string qSuffix = '@headers-exist@@usecsnetworkcreds#true@bh-network#'+platform;
                serv.timeout_x = 120000;
                query = ge.awEndpoint +'/aw/search?session_token='+sesssIon_token+'&query='+searchString +qSuffix;
                system.debug('query ?????'+query);

            }
            response = serv.passEncryptedGetData(query);

            if(isError(response)) searchError = (errorResponse) JSON.deserialize(response, errorResponse.class);
            else searchResponseObject = (bhsSearchResponse) JSON.deserialize(response, bhsSearchResponse.class);
        }catch(Exception e){
            tellUser(response, e);
        }
        return null;
    }

    /*DATA FORMAT FOR RESPONSE*/
    public bhsSearchResponse searchResponseObject{get; set;}
    public errorResponse searchError{get;set;}
    public class bhsSearchResponse{
        //{"players":null,"clans":{"count":59,"results":[{"teamId":4658,"name":"a\nCRk0Z","tag":null},{"teamId":2297,"name":"a*2","tag":null}]}}
        public playerObjectStructure players {get; set;}
        public clanObjectStructure clans {get; set;}
        public void reset(){
            this.players = new playerObjectStructure();
            this.players.results = new player[]{};
            this.players.count = 0;
            this.clans = new clanObjectStructure();
            this.clans.results = new clan[]{};
            this.clans.count = 0;
        }
    }
    public class playerObjectStructure{
        public Integer count {get; set;}
        public player[] results {get; set;}
    }
    public class player{
        public string username {get; set;}
        public string dwid {get; set;}
        public string image {get; set;}
        public string platform {get; set;}
        public string network {get; set;}
        public string ucdid {get; set;}
    }
    public class clanObjectStructure{
        public Long count {get; set;}
        public clan[] results {get; set;}
        public clan[] getSortedResults(){
            clan[] sortedResults = new clan[]{};
            map<String, clan> clanMap = new map<String, clan>();
            for(clan c : results){
                clanMap.put(c.name, c);
            }
            string[] clanNames = new string[]{};
            clanNames.addAll(clanMap.keySet());
            clanNames.sort();
            for(string c : clanNames){
                sortedResults.add(clanMap.get(c));
            }
            return sortedResults;
        }
    }
    public class clan{
        public Long teamId {get; set;}
        public String name {get; set;}
        public String tag {get; set;}
        public integer memberCount {get; set;}
    }
    /*END DATA FORMAT*/
    
    //Step 2 - Select a clan (VF ActionFunction passes the Id of the selected clan into selectedClanId)
    public string selectedClanId {get; set;}
    public pageReference selectClan(){ //load all tabs
        getClanProfile(); //tab 1
        getClanEmblem(); //tab 2
        getClanMembers(); //tab 3
        getClanEntitlements(); //tab 4
        getClanLogs(); //tab 5
        displayClanDetails = true;
        return null;
    }

    //BEGIN TABS
    //Tab 0 - Emblem
    public string b64img {get; set;}
    public pageReference getClanEmblem(){
        String response;
        try{
            //String query = ge.ghostsEndpoint
            String query = ge.endpoint 
                +'emblems/clanEmblem?size=100&title='+game+'&imgtype=png&background=0&clan_id='+selectedClanId;
            if(game.containsIgnoreCase('ghosts')){
                query += ge.headerKey+ge.bhsGhostsCreds+'|rtnB64::true';
            }else{ //TODO update for AW
                DwProxy.service svc = new DwProxy.service();
                String sesssIon_token ='';
                if(!test.isrunningtest())sesssIon_token = svc.generateAWsessionId(Label.AWclanUcdid);
                string qSuffix = '@headers-exist@@usecsnetworkcreds#true@bh-network#'+platform;
                serv.timeout_x = 120000;
                query = ge.awEndpoint +'/aw/clans/'+selectedClanId+'/emblem?&imgtype=png&background=0&session_token='+sesssIon_token+qSuffix;
                system.debug('query ?????'+query);
            }
            b64img = serv.passEncryptedGetData(query);
        }catch(Exception e){
            tellUser(response, e);
        }
        return null;
    }
    //Tab 1 - Clan overview
    public pageReference getClanProfile(){
        String response;
        try{
            theClanProf = new clanProfile();
            response = doClanQuery('clans/','/profile');
            
            if(isError(response)) theClanProfError = (errorResponse) JSON.deserialize(response, errorResponse.class);
            else theClanProf = (clanProfile) JSON.deserialize(response, clanProfile.class);
        }catch(Exception e){
            tellUser(response, e);
        }
        return null;
    }
    //DATA FORMAT FOR /clans/<clan_id>/profile 
    public clanProfile theClanProf {get;set;}
    public errorResponse theClanProfError {get;set;}
    public class clanProfile{
        public long teamId {get;set;}
        public string name {get;set;}
        public blob emblem {get;set;} //this is a propritary binary format, to see the image use
        // {emblem_server_URL}/emblems/clanEmblem?size=200&title=ghosts&imgtype=png&background=0&clan_id={clan_id}
        public string tag {get;set;}        
        public integer mottoBg {get;set;}
        public string motto {get;set;}
        public integer entitlements {get;set;}
        public long cxp {get;set;}
        public long cxpNeeded {get;set;}
        public integer level {get;set;}
        public decimal progress {get;set;}
        public decimal kdr {get;set;}
        public decimal winp {get;set;}
        public string motd {get;set;}
        public integer nextLevelXp{get;set;}
        public integer nextLevel{get;set;}
        public decimal cxpPercent{get;set;}
    }
    //END DATA FORMAT

    public string newClanDetailValue {get;set;}
    public pageReference editClanProfile(){
        String response;
        try{
            //validate
            if(newClanDetailValue == null) newClanDetailValue = ''; 
            changeUpdateError = new errorResponse();
            if(changeModalAction.contains('-name')){
                //name has its own endpoint and data requirements
                String alphaNum = '[a-zA-Z0-9 ]+$';
                Pattern thePattern = Pattern.compile(alphaNum);
                Matcher theMatcher = thePattern.matcher(newClanDetailValue);
                if(isBlankOrNull(newClanDetailValue)){
                    changeUpdateError.error = 'Clan Name cannot be blank';
                    return null;
                }else if(! theMatcher.matches()){
                    changeUpdateError.error = 'New value can only be alpha numeric (A-Z and 0-9)';
                    return null;
                }else{
                    response = doClanPut('clans/', '/name', '{"clan_name":"'+newClanDetailValue+'"}');
                }
            }else{
                String postBody = '{"';
                if(changeModalAction.contains('-tag')) postBody += 'clantag';
                else if(changeModalAction.contains('-mottobg')) postBody += 'mottobg';
                else if(changeModalAction.contains('-motto')) postBody += 'motto';
                else if(changeModalAction.contains('Emblem')) postBody += 'emblem';
                if(postBody != '{"'){
                    postBody += '":"'+newClanDetailValue+'"}';
                    response = doClanPost('clans/', '', postBody);
                }else{
                    changeUpdateError.error = 'Attempt to change unsupported attribute.';
                    return null;
                }   
            }        
            if(isError(response)) changeUpdateError = (errorResponse) JSON.deserialize(response, errorResponse.class);
            else toggleChangePopOver();
        }catch(Exception e){
            tellUser(response, e);
        }
        return null;
    }
    //Tab 2 - Members
    public pageReference getClanMembers(){
        clanMembers = new membershipResponse();
        String response;
        try{
            response = doClanQuery('clans/','/members');
            system.debug('fgw memresp == '+response);
            if(isError(response)) clanMembersError = (errorResponse) JSON.deserialize(response, errorResponse.class);
            else{
                clanMembers = (membershipResponse) JSON.deserialize(response, membershipResponse.class);
                boolean mem1found = false;
                string idOfUserToImpersonate;
                for(clanMember mem : clanMembers.teamMembers){
                    //look for the leader, if not found use co-leader, if not found use member
                    //this logic put in place because according to CS there are leaderless clans
                    if(mem.membershipType == 2){ //leader found stop looking
                        selectedNetwork = mem.network;
                        idOfUserToImpersonate = mem.userId;
                        break;
                    }else{
                        if(mem.membershipType == 1){
                            selectedNetwork = mem.network;
                            idOfUserToImpersonate = mem.userId;
                            mem1found = true;
                        }else if(! mem1found){
                            selectedNetwork = mem.network;
                            idOfUserToImpersonate = mem.userId;
                        }
                    }
                }
                ucdidDec = decryptUcdId(idOfUserToImpersonate);
                
            }
        }catch(Exception e){
            tellUser(response, e);
        }
        return null;
    }
    
    //DATA FORMAT FOR /clans/<clan_id>/members 
    public errorResponse clanMembersError {get;set;}
    public membershipResponse clanMembers {get; set;}
    public class membershipResponse{
        public clanMember[] teamMembers {get; set;}
        public integer getTeamMembersSize(){
            if(teamMembers == null) return 0;
            return teamMembers.size();
        }
        public boolean isClanMember(String userName){
            //assumes that username is unique
            for(clanMember mem : this.teamMembers){
                if(mem.userName.containsIgnoreCase(userName)){
                    return true;
                }
            }
            return false;
        }
    }
    public class clanMember{
        public string userName {get; set;}
        public string userId {get; set;}
        public integer membershipType {get; set;}
        public string platform {get; set;}
        public string network {get; set;}
        public string image {get; set;}
        public long kills {get; set;}
        public long deaths {get; set;}
        public long wins {get; set;}
        public long losses {get; set;}
    }
    //END DATA FORMAT

    public string selectedMemberId{get;set;}
    public string newMembershipLevel{get;set;}
    public pageReference editMembershipLevel(){
        String response;
        try{//do membership position update
            response = doClanPut('clans/','/members/'+selectedMemberId,'{"membershipType":"'+newMembershipLevel+'"}');
            if(isError(response)) changeUpdateError = (errorResponse) JSON.deserialize(response, errorResponse.class);
            else{
                selectedMemberId = '';
                toggleChangePopOver();
            }
        }catch(Exception e){
            tellUser(response, e);
        }
        return null;
    }
    
    public string removalType {get; set;}
    public string dwIdToRemove {get; set;}
    //public string dwAWIdToRemove {get; set;}
    public errorResponse removalResponse {get; set;}
    public pageReference removeMember(){
        String response;
        try{
            if(removalType == 'leave'){
             
                dwIdToRemove = decryptUcdId(dwIdToRemove);
                if(game.containsIgnoreCase('ghosts'))
                    response = doClanPut('clans/','/'+removalType, '{"ucdid":"'+dwIdToRemove+'"}');
                else
                    response = doClanPost('clans/','/'+removalType,'{"ucdid":"'+dwIdToRemove+'"}', dwIdToRemove);
            }else{
                response = doClanPut('clans/','/'+removalType, '{"dwid":"'+dwIdToRemove+'"}');
            }
            if(isError(response)) removalResponse = (errorResponse) JSON.deserialize(response, errorResponse.class);
            else toggleChangePopOver();
            
        }catch(Exception e){
            tellUser(response, e);
        }
        return null;
    }
    
    public boolean showDataLayer3 {get; set;}
    public applicationResponse clanApplications {get; set;}
    public errorResponse applicationsError {get; set;}
    public pageReference getApplications(){
        clanApplications = new applicationResponse();
        String response;
        try{
            response = doClanQuery('clans/','/applications');
            if(isError(response)) applicationsError = (errorResponse) JSON.deserialize(response, errorResponse.class);
            else clanApplications = (applicationResponse) JSON.deserialize(response, applicationResponse.class);
        }catch(Exception e){
            tellUser(response, e);
        }
        return null;
    }
    public class applicationResponse{
        public clanApplication[] applications {get; set;}
    }
    public class clanApplication{
        public string userId {get; set;}
        public string userName {get; set;}
        public long updatedOn;
        public string getUpdatedOn(){
            return DateTime.newInstance(updatedOn*1000).format('MM/dd/yy h:mm a');
        }
        public string message {get; set;}
        public integer state {get; set;}
        public integer teamId {get; set;}
        public string teamName {get; set;}
        public string network {get; set;}
        public string image {get; set;}
        public integer kills {get; set;}
        public integer deaths {get; set;}
        public string platform {get; set;}
        public decimal kdRatio {get; set;}
        public integer wins {get; set;}
        public integer losses {get; set;}
    }
    public string dwIdToApplyToClan {get; set;}
    public string ucdIdToApplyToClan {get; set;}
    public string gamerTagToSearch {get; set;}
    public bhsSearchResponse gtSearchResults {get; set;}
    public pageReference searchForGamerTag(){
        if(!IsBlankOrNull(gamerTagToSearch)){
            gtSearchResults = new bhsSearchResponse();
            gtSearchResults.reset();
            String response;
            try{
                gamerTagToSearch = EncodingUtil.urlEncode(String.escapeSingleQuotes(gamerTagToSearch),'UTF-8');
                //String query = ge.ghostsEndpoint+'search?query='+gamerTagToSearch+'&clansOnly=false'+ge.headerKey+ge.bhsGhostsCreds;
                String query = ge.endpoint+'search?query='+gamerTagToSearch+'&clansOnly=false';
                if(game.containsIgnoreCase('ghosts')) query += ge.headerKey+ge.bhsGhostsCreds;
                else{ //TODO: Update for AW
                    DwProxy.service svc = new DwProxy.service();
                    String sesssIon_token = svc.generateAWsessionId(UcdIdDec);
                    string qSuffix = '@headers-exist@@usecsnetworkcreds#true@bh-network#'+platform;
                    serv.timeout_x = 120000;
                    query = ge.awEndpoint +'/aw/search?session_token='+sesssIon_token+'&query='+gamerTagToSearch +qSuffix;
                    system.debug('query ?????'+query);
                }
                response = serv.passEncryptedGetData(query);
                
                if(isError(response)) searchError = (errorResponse) JSON.deserialize(response, errorResponse.class);
                else{
                    bhsSearchResponse innerResults = (bhsSearchResponse) JSON.deserialize(response, bhsSearchResponse.class);
                    for(player p : innerResults.players.results){ //search spans networks
                        if(! clanMembers.isClanMember(p.userName)){ //if the result isn't already a clan member
                            gtSearchResults.players.results.add(p); //add to results
                        }
                    }                    
                } 
            }catch(Exception e){
                tellUser(response, e);
            }
        }
        return null;
    }
    public pageReference cancelGamerSearch(){
        showDataLayer3 = false;
        dwIdToApplyToClan =  '';
        clanApplications = null;
        gamerTagToSearch = '';
        gtSearchResults = new bhsSearchResponse();
        return null;
    }

    public errorResponse addToClanError {get; set;}
    public pageReference applyToClan(){
        system.debug('ucdIdToApplyToClan == '+ucdIdToApplyToClan);
        if(! isBlankOrNull(selectedClanId) && ! isBlankOrNull(dwIdToApplyToClan)){
            String response;
            ucdIdToApplyToClan = decryptUcdId(ucdIdToApplyToClan);
            try{
                response = doClanPost('clans/', '/applications', 
                                      '{"dwid":"'+dwIdToApplyToClan+'","message":"Application by ATVI Customer Support"}',
                                      ucdIdToApplyToClan
                                     );
                resetLog();
                toggleChangePopOver();
                if(isError(response)) addToClanError = (errorResponse) JSON.deserialize(response, errorResponse.class);
            }catch(Exception e){
                tellUser(response, e);
            }
        }
        return null;
    }
    public pageReference deleteApplication(){
        String response;
        try{
            response = doClanDelete('clans/','/user/'+appIdToProcess+'/application');
            resetLog();
            toggleChangePopOver();
            if(isError(response)) acceptanceError = (errorResponse) JSON.deserialize(response, errorResponse.class);
        }catch(Exception e){
            tellUser(response, e);
        }
        return null;
    }
    public string appIdToProcess {get; set;}
    public errorResponse acceptanceError {get; set;}
    public pageReference acceptApplication(){
        String response;
        try{
            response = doClanPut('clans/','/user/'+appIdToProcess+'/application', '{}');
            resetLog();
            toggleChangePopOver();
            if(isError(response)) acceptanceError = (errorResponse) JSON.deserialize(response, errorResponse.class);
        }catch(Exception e){
            tellUser(response, e);
        }
        return null;
    }
    
    //Tab 4 - Clan Entitlements
    public pageReference getClanEntitlements(){
        theClansEntitlements = new clanEntitlement[]{};
        String response = doClanQuery('clans/','/entitlements'); 
        try{
            if(isError(response)) theClansEntitlementsError = (errorResponse) JSON.deserialize(response, errorResponse.class);
            else{
                JSONParser parser = JSON.createParser(response);
                while (parser.nextToken() != null) {
                    // Start at the array of invoices.
                    if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                        while (parser.nextToken() != null) {
                            // Advance to the start object marker to
                            //  find next invoice statement object.
                            if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                                // Read entire invoice object, including its array of line items.
                                clanEntitlement cEnt = (clanEntitlement)parser.readValueAs(clanEntitlement.class);
                                if(cEnt.hide == 'N'||cEnt.hide == null) theClansEntitlements.add(cEnt);

                                // Skip the child start array and start object markers.
                                parser.skipChildren();
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            tellUser(response, e);
        }
    return null;
    }
    //DATA FORMAT FOR /entitlements
    public clanEntitlement[] theClansEntitlements {get;set;}
    public errorResponse theClansEntitlementsError {get;set;}
    public class clanEntitlement{
        public integer id {get;set;}
        public string key {get;set;}
        public string name {get;set;}
        String aDesc; //desc is a sdfc apex reserved keyword, so we can't use it
        public void setDesc(String s){aDesc = s;}
        public string getDesc(){return aDesc;}
        public integer unlock_level {get;set;}
        public string unlock_req {get;set;}
        public integer bit_offset {get;set;}
        public string type {get;set;}
        public string image {get;set;}
        public string category {get;set;}
        public string hide {get;set;}
        public boolean unlocked {get;set;}
    }

    //Tab 5 - Get Clan Logs
    public Entitlement_Grant_Log__c[] theClanLogs {get; set;}
    public void getClanLogs(){
        theClanLogs = [SELECT Id,Raw_command__c,Raw_response__c,Clan_Id__c,Entitlement__c,Code__c,Game__c,Granted_By__c,Granted_To__c,Promotion__c,New_Value__c,Status__c,User_provided_reason__c FROM Entitlement_Grant_Log__c WHERE Clan_Id__c = :selectedClanId ORDER BY CreatedDate DESC];
        if(theClanLogs == null) theClanLogs = new Entitlement_Grant_Log__c[]{};
    }

    //Step 2.5 choose another clan
    public boolean displayClanDetails {get; set;}
    public void closeClanDetails() {
        displayClanDetails = false;
        clanMembers = new membershipResponse();
        theClansEntitlements = new clanEntitlement[]{};
        removalType = '';
        newMembershipLevel = '';
        selectedMemberId = '';
        clanMembers = new membershipResponse();
        theClanProf = new clanProfile();
        
        changeUpdateError = null;
        clanMembersError = null;
        theClanProfError = null;
        theClansEntitlementsError = null;
        acceptanceError = null;
        applicationsError = null;
        removalResponse = null;
    }

    //Utilities
    public void tellUser(String response, Exception ex){
        system.debug('!!!!ERROR!!!!response == '+response+'::::::exception == '+ex);
        if(response != null) ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.FATAL, response));
        else ApexPages.addMessages(ex);
    }

    public string errorKey = 'error":';
    public class errorResponse{
        string error = '';
        public string getError(){
            string response = '';
            if(code != null) response += code;
            if(error != null) response += ' '+error;
            else if(message != null) response += ' '+message;
            return response;
        }
        public void setError(String err){
            error = err;
        }
        public string code {get; set;}
        public string message {get; set;}
    }
    
    public boolean isBlankOrNull(String s){return s == null || s == '';}
/*    not used?
    public string doCustomGet(String urlAfterEndpoint){
        String response = '';
        try{
            String query = ge.ghostsEndpoint+urlAfterEndpoint;
            system.debug('query == '+query);
            response = serv.passEncryptedGetData(query);
            system.debug(response);
        }catch(Exception e){
            system.debug('ERROR == '+e);
            ApexPages.addMessages(e);
        }
        return response;
    }
*/
    public string doClanQuery(String urlBeforeClanId, String urlAfterClanId){
        String response = '';
        try{
           
            if(! isBlankOrNull(selectedClanId)){
                //String query = ge.ghostsEndpoint+urlBeforeClanId+selectedClanId+urlAfterClanId+ge.headerKey+ge.bhsGhostsCreds;
                String query = ge.endpoint+urlBeforeClanId+selectedClanId+urlAfterClanId;
                if(game.containsIgnoreCase('ghosts')) query += ge.headerKey+ge.bhsGhostsCreds;
                else{ //TODO Update for AW
                    if(urlAfterClanId.contains('/entitlements'))urlAfterClanId='/unlocks';
                    DwProxy.service svc = new DwProxy.service();
                    String sesssIon_token ='';
                if(!test.isrunningtest())sesssIon_token = svc.generateAWsessionId(Label.AWclanUcdid);
                    string qSuffix = '@headers-exist@@usecsnetworkcreds#true@bh-network#'+platform;
                    serv.timeout_x = 120000;
                    query = ge.awEndpoint +'/aw/'+urlBeforeClanId+selectedClanId+urlAfterClanId+'?session_token='+sesssIon_token+qSuffix;
                    system.debug('query ?????'+query);
                }
                system.debug('getUrl == '+query);
                response = serv.passEncryptedGetData(query);
                system.debug('getUrl response == '+response);
            }else response = '{"error":"clan id blank"}';
        }catch(Exception e){
            system.debug('ERROR == '+e);
            ApexPages.addMessages(e);
        }   
        return response;
    }

    public string doClanDelete(String urlBeforeClanId, String urlAfterClanId){
        String response = '';
        try{
            if(! isBlankOrNull(selectedClanId)){
                //String query = ge.ghostsEndpoint+urlBeforeClanId+selectedClanId+urlAfterClanId+ge.headerKey+ge.bhsGhostsCreds;
                String query = ge.endpoint+urlBeforeClanId+selectedClanId+urlAfterClanId;
                if(game.containsIgnoreCase('ghosts')) query += ge.headerKey+ge.bhsGhostsCreds;
                else{ //TODO Update for AW
                    
                     DwProxy.service svc = new DwProxy.service();
                   String sesssIon_token ='';
                if(!test.isrunningtest())sesssIon_token = svc.generateAWsessionId(Label.AWclanUcdid);
                    string qSuffix = '@headers-exist@@usecsnetworkcreds#true@bh-network#'+platform;
                    serv.timeout_x = 120000;
                    query = ge.awEndpoint +'/aw/'+urlBeforeClanId+selectedClanId+urlAfterClanId+'?session_token='+sesssIon_token+qSuffix;
                    system.debug('query ?????'+query);

                }
                system.debug('delUrl == '+query);
                response = serv.passEncryptedDeleteData(query);
                rawResponse(response);
                system.debug('delUrl response == '+response);
            }else response = '{"error":"clan id blank"}';
        }catch(Exception e){
            system.debug('ERROR == '+e);
            ApexPages.addMessages(e);
        }   
        return response;
    }
    
    public string doClanPut(String putUrlBeforeClanId, String putUrlAfterClanId, String putBody){
        String response = '';
        try{
            
            if(! isBlankOrNull(selectedClanId)){
                if(! IsBlankOrNull(log.User_provided_reason__c)){
                    String fullPutUrl = ge.endpoint;
                    if(game.containsIgnoreCase('ghosts')){
                        fullPutUrl = ge.ghostsEndpoint+putUrlBeforeClanId+selectedClanId+putUrlAfterClanId;
                        fullPutUrl += '?ucdid='+ucdidDec+'&network='+selectedNetwork;
                        fullPutUrl += ge.headerKey+ge.bhsGhostsCreds;
                    }else{ //UPDATE for AW
                        
                        DwProxy.service svc = new DwProxy.service();
                        String sesssIon_token='';

                            sesssIon_token= svc.generateAWsessionId(UcdIdDec);                        
                        string qSuffix = '@headers-exist@@usecsnetworkcreds#true@bh-network#'+platform;
                        serv.timeout_x = 120000;
                        
                        //query = ge.awEndpoint +'/aw/'+urlBeforeClanId+selectedClanId+urlAfterClanId+'?session_token='+sesssIon_token+qSuffix;
                       // system.debug('query ?????'+query);
                        fullPutUrl = ge.awendpoint+'/aw/';
                        fullPutUrl += putUrlBeforeClanId+selectedClanId+putUrlAfterClanId;
                        fullPutUrl += '?session_token='+sesssIon_token+qSuffix ;
                        //fullPutUrl += ge.headerKey+ge.bhsGhostsCreds;
                            
                    }
                    system.debug('putUrl == '+fullPutUrl);
                    system.debug('putBody == '+putBody);
                    //do action
                    response = serv.passEncryptedPutData(fullPutUrl, putBody,'application/json');
                    system.debug('putResponse == '+response);
                    rawResponse(response);
                    
                    //log action
                    log.Raw_command__c = putBody;
                    log.Raw_response__c = response;
                    resetLog();
                }else response = '{"error":"reason cannot be blank"}';
            }else response = '{"error":"clan id blank"}';
        }catch(Exception e){
            system.debug('ERROR == '+e);
            ApexPages.addMessages(e);
        }
        return response;
    }
    
    public string doClanPost(String postUrlBeforeClanId, String postUrlAfterClanId, String postBody){
        return doClanPost(postUrlBeforeClanId, postUrlAfterClanId, postBody, null);
    }
    public string doClanPost(String postUrlBeforeClanId, String postUrlAfterClanId, String postBody, String ucdIdToImpersonate){
        String response = '';
        String ucdId = ucdIdToImpersonate == null ? ucdidDec : ucdIdToImpersonate;
        try{
            if(! isBlankOrNull(selectedClanId)){
                if(! IsBlankOrNull(log.User_provided_reason__c)){
                    String fullPostUrl = ge.endpoint;
                    if(game.containsIgnoreCase('ghosts')){
                        //String fullPostUrl = ge.ghostsEndpoint+postUrlBeforeClanId+selectedClanId+postUrlAfterClanId;
                        fullPostUrl += postUrlBeforeClanId+selectedClanId+postUrlAfterClanId;
                        fullPostUrl += '?ucdid='+ucdId+'&network='+selectedNetwork;
                        fullPostUrl += ge.headerKey+ge.bhsGhostsCreds;
                        
                    }else{ //TODO Update for AW
                        DwProxy.service svc = new DwProxy.service();
                        String sesssIon_token=svc.generateAWsessionId(ucdIdToImpersonate);   
                        string qSuffix = '@headers-exist@@usecsnetworkcreds#true@bh-network#'+platform;
                        serv.timeout_x = 120000;
                        //query = ge.awEndpoint +'/aw/'+urlBeforeClanId+selectedClanId+urlAfterClanId+'?session_token='+sesssIon_token+qSuffix;
                       // system.debug('query ?????'+query);
                        fullPostUrl = ge.awendpoint+'/aw/'+postUrlBeforeClanId+selectedClanId+postUrlAfterClanId;
                        fullPostUrl += '?session_token='+sesssIon_token+qSuffix ;
                        //fullPutUrl += ge.headerKey+ge.bhsGhostsCreds;
                      
                    }
                    system.debug('postUrl == '+fullPostUrl);
                    system.debug('postBody == '+postBody);
                    
                    //do action
                    response = serv.passEncryptedPostData(fullPostUrl, postBody,'application/json');
                    system.debug('postResponse == '+response);
                    rawResponse(response);
                    
                    //log action
                    log.Raw_command__c = postBody;
                    log.Raw_response__c = response;
                    resetLog();
                }else response = '{"error":"reason cannot be blank"}';
            }else response = '{"error":"clan id blank"}';
        }catch(Exception e){
            system.debug('ERROR == '+e);
            ApexPages.addMessages(e);
        }
        return response;
    }
    
    //Change variables
    public errorResponse changeUpdateError {get; set;}
    public string changeModalTitle{get;set;}
    public string changeModalQuestion{get;set;}
    public string changeModalAction {get; set;}
    public boolean displayChangePopOver {get; set;}
    public Entitlement_Grant_Log__c log {get; set;}
    
    public pageReference toggleChangePopOver(){
        displayChangePopOver = !displayChangePopOver;
        if(displayChangePopOver == false){//popOver being closed reset everything
            changeModalAction = '';
            selectedMemberId = '';
            changeUpdateError = null;
            log.User_provided_reason__c = '';
            newClanDetailValue = '';
        }
        return null;
    }
    
    public void resetLog(){
        try{//do the inserts
            system.debug('v>>>>'+selectedClanId);
            if(log != null){ 
                log.Clan_Id__c = selectedClanId;
                insert log;
                if(theCaseId != null){
                    insert new Case_Comment__c(
                        Comments__c  = log.User_provided_reason__c,
                        Case__c = theCaseId
                    );
                }
                system.debug('--->'+log);
            }
            //do the reset
            log = new Entitlement_Grant_Log__c(
                Game__c = 'Clan Wars',
                Granted_By__c = UserInfo.getUserId(),
                Clan_Id__c = selectedClanId
            );
        }catch(Exception e){
            system.debug('>>>>'+e);
        }
    }
    
    public string decryptUcdId(String encrypted){
        try{
            if(game.containsIgnoreCase('ghosts')){
                return serv.passEncryptedGetData(ge.ghostsEndpoint+'decrypt/'+encrypted+ge.headerKey+ge.bhsGhostsCreds).replaceAll('ucd-','').replaceAll('"','');
            }else{ //UDPATE for AW
                return encrypted;
            }
        }catch(Exception e){
            tellUser(null,e);
        }
        return '';
    }
    
    public boolean isError(String response){
        if(response == null) return true;
        boolean result = response.containsIgnoreCase(errorKey); //check for common error vals
        if(! result){
            result = response.contains('<html>'); //return should be JSON data
        }
        return result;
    }
    public pageReference doNothing(){return null;}
    public void rawResponse(String pResponse){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, pResponse));
    }
}