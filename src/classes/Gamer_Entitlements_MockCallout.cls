@isTest
public class Gamer_Entitlements_MockCallout implements WebServiceMock {
    String requestedResponse;
    public Gamer_Entitlements_MockCallout(String pRequiredResopnse){
        requestedResponse = pRequiredResopnse;
    }
    public void doInvoke(
        Object stub,
       Object request,
       Map<String, Object> response,
       String endpoint,
       String soapAction,
       String requestName,
       String responseNS,
       String responseName,
       String responseType
    ){// Create response element from the autogenerated class.
        DwProxy.DwGetProxyResponse_element responseElement = new DwProxy.DwGetProxyResponse_element();
        // Populate response element.
        responseElement.DwGetProxyResult = requestedResponse;
     
        // Add response element to the response parameter, as follows:
        response.put('response_x', responseElement); 
   }
}