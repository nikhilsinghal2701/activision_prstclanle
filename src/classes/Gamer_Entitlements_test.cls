@isTest
public class Gamer_Entitlements_test{

    private static testMethod void createTestData(){
        Account a = new Account(Name = 'test');
        insert a;
        
        Contact c = new Contact(
            AccountId = a.Id,
            FirstName = 'first',
            LastName = 'last', 
            Email = '123@test.test',
            UCDID__c = '123',
            Birthdate = system.today().addDays(-7300)
        );
        insert c;
        
        Multiplayer_Account__c mp = new Multiplayer_Account__c(Contact__c = c.Id);
        insert mp;
        
        Case cas1 = new Case(ContactId = c.Id, Comment_Not_Required__c = true);
        insert cas1;
        
        Case cas2 = new Case(Comment_Not_Required__c = true);
        insert cas2;
        
        insert new Entitlement__c[]{
            new Entitlement__c(
                Entitlement_description__c = 'this is a super cool thing', 
                Entitlement_type__c = 'boolean', 
                Entitlement_api_name__c = 'woop', 
                Group__c = 'asdf',
                Entitlement_Grant_Method__c = 'POST',
                Game__c = 'Call of Duty: Ghosts',
                Grant_Value__c = '1'
            ),
            new Entitlement__c(
                Entitlement_description__c = 'this is a super cool thing', 
                Entitlement_type__c = 'boolean', 
                Entitlement_api_name__c = 'woop', 
                Group__c = 'asdf',
                Entitlement_Grant_Method__c = 'PUT',
                Game__c = 'Call of Duty: Ghosts',
                Grant_Value__c = '1'
            ),
            new Entitlement__c(
                Entitlement_description__c = 'this is a super cool thing', 
                Entitlement_type__c = 'boolean', 
                Entitlement_api_name__c = 'woop', 
                Group__c = 'asdf',
                Entitlement_Grant_Method__c = 'GET',
                Game__c = 'Call of Duty: Ghosts',
                Grant_Value__c = '1'
            ),
            new Entitlement__c(
                Entitlement_description__c = 'this is a super cool thing', 
                Entitlement_type__c = 'integer', 
                Entitlement_api_name__c = 'woop', 
                Group__c = 'asdf',
                Entitlement_Grant_Method__c = 'POST',
                Game__c = 'Call of Duty: Ghosts',
                Grant_Value__c = '15'
            ),
            new Entitlement__c(
                Entitlement_description__c = 'this is a super cool thing', 
                Entitlement_type__c = 'integer', 
                Entitlement_api_name__c = 'woop', 
                Group__c = 'asdf',
                Entitlement_Grant_Method__c = 'PUT',
                Game__c = 'Call of Duty: Ghosts',
                Grant_Value__c = '15'
            ),
            new Entitlement__c(
                Entitlement_description__c = 'this is a super cool thing', 
                Entitlement_type__c = 'integer', 
                Entitlement_api_name__c = 'woop', 
                Group__c = 'asdf',
                Entitlement_Grant_Method__c = 'GET',
                Game__c = 'Call of Duty: Ghosts',
                Grant_Value__c = '15'
            )
        };
        
    }
    public static testMethod void test1_instantiation(){
        Gamer_Entitlements tstCls;
        tstCls = new Gamer_Entitlements();
        createTestData();
        Contact ctc = [SELECT Id FROM Contact LIMIT 1];
        Multiplayer_Account__c mp = [SELECT Id FROM Multiplayer_Account__c LIMIT 1];
        Case cas1 = [SELECT Id FROM Case WHERE ContactId != null LIMIT 1];
        Case cas2 = [SELECT Id FROM Case WHERE ContactId = null LIMIT 1];

        test.setCurrentPage(Page.Gamer_Entitlements);
        ApexPages.currentPage().getParameters().put('id', ctc.Id);
        tstCls = new Gamer_Entitlements();
        
        ApexPages.currentPage().getParameters().put('id', mp.Id);
        tstCls = new Gamer_Entitlements();
        
        ApexPages.currentPage().getParameters().put('id', cas1.Id);
        tstCls = new Gamer_Entitlements();
        
        ApexPages.currentPage().getParameters().put('id', cas2.Id);
        tstCls = new Gamer_Entitlements();
    }
    private static testMethod void test2_isCurrentUserTier3(){
        User u = [SELECT Id, Tier__c FROM User WHERE Tier__c = '' AND IsActive = TRUE LIMIT 1];
        system.runAs(u){
            Gamer_Entitlements tstCls = new Gamer_Entitlements(new ApexPages.standardController(new Case()));
            system.assertEquals(false,tstCls.isCurUserTier3);
            tstCls.addEntitlement();
        }
        u.Tier__c = 'Tier 3';
        update u;
        system.runAs(u){
            Gamer_Entitlements tstCls = new Gamer_Entitlements(new ApexPages.standardController(new Case()));
            system.assertEquals(true,tstCls.isCurUserTier3);
            tstCls.gamer = new Contact(UCDID__c = '');
            tstCls.addEntitlement();
            tstCls.gamer = new Contact(UCDID__c = '123');
            tstCls.addEntitlement();
            tstCls.entitlementApiName = '123';
            tstCls.addEntitlement();
            tstCls.ent = new Entitlement__c(platform__c = 'xbl', Game__c = 'Call of Duty: Ghosts');
            tstCls.addEntitlement();
            tstCls.selectedEntitlement = new Entitlement__c(Entitlement_Type__c = 'integer');
            tstCls.newEntitlementValue = 'asdf'; 
            tstCls.addEntitlement();
            tstCls.newEntitlementValue = '15'; 
            tstCls.addEntitlement();
        }
    }
    public static testMethod void test3_methods(){
        Gamer_Entitlements tstCls = new Gamer_Entitlements();
        
        system.assertEquals(tstCls.showDetail, false);
        if(tstCls.isProd) system.assertEquals(tstCls.ghostsEndpoint, tstCls.prodEndpoint);
        else system.assertEquals(tstCls.ghostsEndpoint, tstCls.devEndpoint);
        tstCls.ent.game__c = 'Call of Duty: Ghosts';
        tstCls.fetchEntitlements();        
        system.assertEquals(tstCls.entList.size(), 0);
        
        //mock callout 1 - getEntitlement
        //mock callout 2 - addEntitlement
        Entitlement__c ent = new Entitlement__c(Entitlement_Type__c = 'boolean');
        tstCls.entMap.put(ent.Id, ent);
        
        tstCls.addMEssageToPage(ApexPages.SEVERITY.CONFIRM, '');
        tstCls.objResponse = new map<String, Object>();
        tstCls.objResponse.put('value','true');
        tstCls.newEntitlementValue = true;
        tstCls.entitlementApiName = 'test_class';
        
        String tstStr = tstCls.testResult('fail');
        system.assertEquals('Failed',tstStr);
        
        tstStr = tstCls.testResult('{"value":true}');
        system.assertEquals('Successful',tstStr);
        
        tstStr = tstCls.testResult('{"value":false}');
        system.assertEquals('Failed',tstStr);
        
        
        ent.Entitlement_Type__c = 'integer';
        tstCls.entMap.put(ent.Id, ent);
        tstCls.objResponse.put('value',0);
        tstCls.newEntitlementValue = 1;
        tstCls.priorIntValue = 1;
        tstStr = tstCls.testResult('{"value":1}');
        system.assertEquals('Failed',tstStr);
        
        tstCls.objResponse.put('value',1);
        tstCls.newEntitlementValue = 1;
        tstCls.priorIntValue = 0;
        tstStr = tstCls.testResult('{"value":1}');
        system.assertEquals('Successful',tstStr);
        
        system.assert(tstCls.isBlankOrNull(null));
        system.assert(tstCls.isBlankOrNull(''));
        system.assert(! tstCls.isBlankOrNull(' '));
        system.assert(! tstCls.isBlankOrNull('asdf'));
        
        Gamer_Entitlements.inteEntitlement intClass = new Gamer_Entitlements.inteEntitlement('ucdid','key',12345,'platform');
        
        Gamer_Entitlements.boolEntitlement bolClass = new Gamer_Entitlements.boolEntitlement('ucdid','key',false,'platform');
        
        
        ent.Entitlement_Type__c = 'boolean';
        tstCls.entMap.put(ent.Id, ent);
        tstCls.objResponse.put('value',true);
        tstCls.newEntitlementValue = true;
        String jsonVal = '{"test_class":1,'; //if(fieldName.contains(entitlementApiName) && if(val == 1
        jsonVal += '"field2":0,'; //if(val == 0
        jsonVal += '"field3":9223372036854775807,';
        jsonVal += '"field4":90.9,';
        jsonVal += '"field5":"hey",';
        jsonVal += '"field6":true,';
        jsonVal += '"field7":false}';
        tstCls.parseJSON(jsonVal);
        
        ent.Entitlement_Type__c = 'integer';
        tstCls.entMap.put(ent.Id, ent);
        tstCls.objResponse.put('value',30);
        tstCls.newEntitlementValue = 30;
        jsonVal = '{"test_class":30,'; //if(fieldName.contains(entitlementApiName) && if(val == 1
        jsonVal += '"field7":false}';
        tstCls.parseJSON(jsonVal);
        
        tstCls.parseJSON('{"class":30,"field7":false}');
        
        ent.Entitlement_Type__c = 'boolean';
        tstCls.entMap.put(ent.Id, ent);
        tstCls.objResponse.put('value',true);
        tstCls.newEntitlementValue = true;
        tstCls.parseJSON('{"class":30,"field7":false}');
    }
    
    public static testMethod void test4_callout1(){
        createTestData();
        Contact ctc = [SELECT Id FROM Contact LIMIT 1];
        ApexPages.currentPage().getParameters().put('id',ctc.Id);
        Gamer_Entitlements tstCls = new Gamer_Entitlements();
        system.assertEquals('123',tstCls.gamer.UCDID__c);
        Entitlement__c ent = [SELECT Id FROM Entitlement__c LIMIT 1];
        tstCls.selectedEntitlementId = ent.Id;
        //tstCls.ent.Game__c = 'Call of Duty: Ghosts'; //trip try catch

        Test.setMock(WebServiceMock.class, new Gamer_Entitlements_MockCallout('malformed json response'));
        test.startTest();
            tstCls.getEntitlement();
        test.stopTest();
    }
    
    public static testMethod void test4_callout2(){
        createTestData();
        Contact ctc = [SELECT Id FROM Contact LIMIT 1];
        ApexPages.currentPage().getParameters().put('id',ctc.Id);
        Gamer_Entitlements tstCls = new Gamer_Entitlements();
        system.assertEquals('123',tstCls.gamer.UCDID__c);
        Entitlement__c ent = [SELECT Id FROM Entitlement__c LIMIT 1];
        tstCls.selectedEntitlementId = ent.Id;
        tstCls.ent.Game__c = 'Call of Duty: Ghosts'; //trip try catch
        
        Test.setMock(WebServiceMock.class, new Gamer_Entitlements_MockCallout('{"woop":"true"}'));
        test.startTest();
            tstCls.getEntitlement();
        test.stopTest();
    }
    
    public static testMethod void test4_callout3(){
        createTestData();
        Contact ctc = [SELECT Id FROM Contact LIMIT 1];
        ApexPages.currentPage().getParameters().put('id',ctc.Id);
        Gamer_Entitlements tstCls = new Gamer_Entitlements();
        system.assertEquals('123',tstCls.gamer.UCDID__c);
        Entitlement__c ent = [SELECT Id FROM Entitlement__c LIMIT 1];
        tstCls.selectedEntitlementId = ent.Id;
        
        Test.setMock(WebServiceMock.class, new Gamer_Entitlements_MockCallout('{"woop":"true"}'));
        test.startTest();
            tstCls.getEntitlement();
        test.stopTest();
    }
    public static testMethod void test4_callout4(){
        Gamer_Entitlements tstCls = new Gamer_Entitlements(new ApexPages.standardController(new Case()));
        tstCls.isCurUserTier3 = true;
        tstCls.addEntitlement();
        tstCls.gamer = new Contact(UCDID__c = '123');
        tstCls.addEntitlement();
        tstCls.entitlementApiName = '123';
        tstCls.addEntitlement();
        tstCls.ent = new Entitlement__c(platform__c = 'xbl', Game__c = 'Call of Duty: Ghosts');
        tstCls.addEntitlement();
        tstCls.selectedEntitlement = new Entitlement__c(Entitlement_Type__c = 'integer');
        tstCls.newEntitlementValue = 'asdf'; 
        tstCls.addEntitlement();
        tstCls.newEntitlementValue = '15'; 
        tstCls.addEntitlement();
    }
    
    public static testMethod void test5_methods(){
        Account a = new Account(Name = 'test');
        insert a;
        
        Contact c = new Contact(
            AccountId = a.Id,
            FirstName = 'first',
            LastName = 'last', 
            Email = '123@test.test',
            UCDID__c = '123',
            Birthdate = system.today().addDays(-7300)
        );
        insert c;
        
        Multiplayer_Account__c mp = new Multiplayer_Account__c(Contact__c = c.Id, platform__c = 'xbl',gamertag__c='test');
        insert mp;
        
        Case cas1 = new Case(ContactId = c.Id, Comment_Not_Required__c = true);
        insert cas1;
        
        Gamer_Entitlements tstCls = new Gamer_Entitlements();
        tstCls.ent.game__c = 'Call of Duty: Ghosts';
		tstCls.refreshOptions();
        tstCls.getConsolePicklist();
        tstCls.ent.game__c = 'Call of Duty: Advanced Warfare';
        tstCls.refreshOptions();
        tstCls.GTEntitlements();
        tstCls.englishName = 'test';
        //Contact ctc = [SELECT Id FROM Contact LIMIT 1];
        tstCls.gamer = c;
        tstCls.theCase = cas1;
        tstCls.gamertag = 'test';
        tstCls.ToggleEntitlement();
        tstCls.curMPs = [select id,platform__c,gamertag__c from multiplayer_Account__c];
        tstCls.fetchEntitlements();
        tstCls.getConsolePicklist();
        
        tstCls.refreshPlatform();
        tstCls.curMPs[0].platform__c = 'psn';
        tstCls.refreshPlatform();
        tstCls.curMPs[0].platform__c = 'steam';
        tstCls.refreshPlatform();
        tstCls.toggleNotate();
    }
    
}