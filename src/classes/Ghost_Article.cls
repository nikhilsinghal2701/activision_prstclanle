public class Ghost_Article{
    public FAQ__kav faq {get; set;}
    public static final string uLang {get; set;}
    
    static{ //detect language
        try{
            uLang = Ghost_Template.uLang;
        }catch(Exception e){ 
            system.debug('ERROR '+e);
            uLang = 'en_US';
        }
    }
    public Ghost_Article(){
        if(ApexPages.currentPage().getParameters().containsKey('id') &&
           ApexPages.currentPage().getParameters().get('id') != null)
        {
            try{
                for(FAQ__kav lFaq : [SELECT Id, Title, Answer_Mobile__c, ArticleNumber FROM FAQ__kav WHERE Id = :ApexPages.currentPage().getParameters().get('id') UPDATE VIEWSTAT]){
                    this.faq = lFaq;
                }
            }catch(Exception e){
                system.debug(e);
            }
        } 
    }
    PKB_Article_Feedback_Deflection__c afd;
    public boolean boolArticleWasHelpful {get;set;}
    public void recordHelpful(){
        try{
            if(boolArticleWasHelpful){
                afd = new PKB_Article_Feedback_Deflection__c(
                    Article_ID__c = faq.id,
                    Article_Number__c = faq.articleNumber,
                    Article_Title__c = faq.title,
                    Feedback_Source__c = 'ghsots support micro-site',
                    Deflection__c = true
                );
                upsert afd;
            }else if(afd != null && afd.Id != null){
                delete afd;
                afd = new PKB_Article_Feedback_Deflection__c();
            }
        }catch(Exception e){
            system.debug('ERROR == '+e);
        }
    }

}