public class Ghost_Template{
    public static final string uLang {get; set;}
    
    static{ //detect language
        try{
            //Multilingual support
            string lang = '';
            string[] searchLocations = new string[]{'uil','lang','ulang'};
            for(String searchLocation : searchLocations){//url is checked first
                if(ApexPages.currentPage().getParameters().containsKey(searchLocation)){
                    lang = ApexPages.currentPage().getParameters().get(searchLocation);
                    system.debug('lang found in URL == '+lang);
                    break;
                }
            }
            if(lang == ''){//if not in url check cookie
                for(String searchLocation : searchLocations){//url is checked first
                    Cookie aCookie = ApexPages.currentPage().getCookies().get(searchLocation);
                    if(aCookie != null && aCookie.getValue() != null && aCookie.getValue() != ''){
                        lang = aCookie.getValue();
                        system.debug('lang found in cookie == '+lang);
                        break;
                    }
                }
            }

            //lang could be present but set to some other value, filter those out 
            if(lang.contains('de')) lang = 'de_DE';
            else if(lang.contains('fr')) lang = 'fr_FR';
            else if(lang.contains('it')) lang = 'it_IT';
            else if(lang.contains('es')) lang = 'es_ES';
            else lang = 'en_US';
            uLang = lang;
        }catch(Exception e){ 
            system.debug('ERROR == '+e);
            uLang = 'en_US';
        }
    }
    
}