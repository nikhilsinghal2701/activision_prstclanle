public class Ghosts {
    //the ghosts app supports: English, French, German, Italian, Spanish (EFGIS)
    //wrap everything in try{}catch(){}
    public static final string gCtgy = 'Call_of_Duty_Ghosts';
    public static final string uLang {get; set;}
    
    static{ //detect language
        try{
            uLang = Ghost_Template.uLang;
        }catch(Exception e){ 
            system.debug('ERROR '+e);
            uLang = 'en_US';
        }
    }
    
    //Contact Us - Mobile
    public String typeId {get; set;} 
    public WebToCase_GameTitle__c[] platforms {get; set;}
    public map<String, WebToCase_SubType__c[]> typeIdToSubTypes {get; set;}
    
    public map<String, Id> subTypeIdToArticleId {get; set;}
    public map<String, FAQ__kav> articleIdToArticle {get; set;}
    
    public map<String, String> subTypeIdToFormId {get; set;}
    public map<String, WebToCase_Form__c> formIdToForm {get; set;}
    
    public map<String, map<String, Contact_Us_Mobile.FormResponse>> submittedFormValues {get; set;}
    
    public string currentCaseNumber {get; set;}
    
    public String SubmittedFormId {get; set;}
    
    public Ghosts(){
        system.debug('currentCaseNumber == '+currentCaseNumber);
        try{ //init contact_us_mobile
            Contact_Us_Mobile cuMobile = new Contact_Us_Mobile(gCtgy, uLang);
            
            platforms = cuMobile.platforms;
            typeIdToSubTypes = cuMobile.typeIdToSubTypes;
            
            subTypeIdToArticleId = cuMobile.subTypeIdToArticleId;
            articleIdToArticle = cuMobile.articleIdToArticle;
            
            subTypeIdToFormId = cuMobile.subTypeIdToFormId;
            formIdToForm = cuMobile.formIdToForm;
            
            submittedFormValues = cuMobile.formIdToFormResponses;
        }catch(Exception e){
            system.debug('ERROR '+e);
        }
    }

    public string errorMessage {get; set;}
    public boolean isBlankOrNull(String s){return s == null || s == '';}
    public pageReference submitForm(){
        system.debug(LoggingLevel.ERROR, 'fgw submittedFormId == '+submittedFormId);
        pageReference pr;
        try{
            if(SubmittedFormId != null && SubmittedFormId != ''){
                system.debug(LoggingLevel.ERROR, 'BEGIN CASE SETUP');
                Case currentCase = new Case(
                    ContactId = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()].ContactId,
                    Status = 'Open',
                    Origin = 'Web To Case',
                    W2C_Origin_Language__c = uLang,
                    Description = ''
                    
                );
                for(RecordType rt : [SELECT id FROM RecordType WHERE Name = 'General Support' LIMIT 1]){
                    currentCase.RecordTypeId = rt.Id;
                }
                WebToCase_Form__c theForm = formIdToForm.get(SubmittedFormId);
                currentCase.Type = theForm.WebToCase_SubType__r.WebToCase_Type__r.TypeMap__c;
                currentCase.Sub_Type__c = theForm.WebToCase_SubType__r.SubTypeMap__c;
                currentCase.Product_Effected__c = theForm.WebToCase_SubType__r.WebToCase_Type__r.WebToCase_GameTitle__r.Public_Product__c;
                currentCase.W2C_Type__c = theForm.WebToCase_SubType__r.WebToCase_Type__r.Description__c; //= selectedType.Description__c;
                currentCase.W2C_SubType__c = theForm.WebToCase_SubType__r.Description__c; //selectedSubType.Description__c;
                currentCase.subject =  theForm.ref_GameTitle__c+' ('+theForm.ref_Platform__c+')'+' : '+currentCase.W2C_Type__c+' ('+currentCase.W2C_SubType__c+')';
                currentCase = Contact_Us_Controller.addDispositionsToCase(
                    currentCase, 
                    theForm.ref_Platform__c, //selectedPlatform.Name__c,
                    theForm.WebToCase_SubType__r.WebToCase_Type__r.WebToCase_GameTitle__r.Title__c, //selectedTitle.Title__c,
                    theForm.WebToCase_SubType__r.WebToCase_Type__r.TypeMap__c,//selectedType.typeMap__c,
                    theForm.WebToCase_SubType__r.subTypeMap__c//selectedSubType.subTypeMap__c
                );
                system.debug(LoggingLevel.ERROR, 'END CASE SETUP');
                
                /*
                if(enableEmail){
                    system.debug('EMAIL ENABLED');
                    for(WebToCase_FormField__c fField : emailFormFields){
                        if(fField.FieldType__c == 'TextAreaLong'){
                            currentCase.description = fField.TextAreaLong__c;
                        }
                    }
                }
                else if (enableForm) open brace
                system.debug('CUSTOM FORM ENABLED SETUP');
                */
                for(Contact_Us_Mobile.FormResponse fFieldWrapper : submittedFormValues.get(SubmittedFormId).values()){
                    WebToCase_FormField__c fField = fFieldWrapper.question;
                    
                    system.debug(LoggingLevel.ERROR, 'FIELD TYPE' + fField.FieldType__c);
                    system.debug(LoggingLevel.ERROR, 'FIELD REQ' + fField.isRequired__c);
                    if(fField.FieldType__c == 'Checkbox'){
                        currentCase.description += fField.Label__c + ': ' + fFieldWrapper.boolAnswer + '\n';  
                    }
                    else if(new set<String>{'Text','TextArea','TextAreaLong','Picklist'}.contains(fField.FieldType__c)){
                        if(fField.isRequired__c && isBlankOrNull(fFieldWrapper.answer.remove(':').normalizeSpace())){
                            errorMessage = Label.CONTACT_US_Required;
                            return null;
                        }
                        currentCase.description += fField.Label__c + ': ' + fFieldWrapper.answer.remove(':').normalizeSpace() + '\n';
                    }/*
                    else if(fField.FieldType__c == 'DateText'){
                        currentCase.description += fField.Label__c + ': ' + fField.DateText__c.remove(':') + '\n';
                    }
                    else if(fField.FieldType__c == 'GT Picklist'){
                        currentCase.description += fField.Label__c + ': ' + fField.GTText__c.remove(':') + '\n';
                    }*/
                }                

                insert currentCase;    
                
                try{
                    //Create Article:
                    if(theForm.WebToCase_SubType__r.Article_Id__c != null && theForm.WebToCase_SubType__r.Article_Id__c != ''){
                        insert new CaseArticle(
                            KnowledgeArticleId = theForm.WebToCase_SubType__r.Article_Id__c,
                            CaseId = currentCase.id
                        );
                        system.debug(LoggingLevel.ERROR, 'Article Created: ' + theForm.WebToCase_SubType__r.Article_Id__c);
                    }
                }catch(Exception e){
                    system.debug(LoggingLevel.ERROR, 'Case Article Error: '+e);
                }
                
                /*
                //Add Attachment:
                system.debug('UFB');
                system.debug('Uploaded File Body: ' + uploadedFileBody);
                if(uploadedFileBody != null){
                    if(uploadedFileSize > 150000){
                        formErrorMessage = 'Image Too Large.';
                        formHasErrorMessage = true;
                        uploadedFileName = null;
                        uploadedFileBody = null;
                        return null;
                    }
                    if(uploadedFileContentType != 'image/jpeg' && uploadedFileContentType != 'image/png'){
                        formHasErrorMessage = true;
                        formErrorMessage = 'Invalid Image Type.';
                        uploadedFileName = null;
                        uploadedFileBody = null;
                        return null;
                    }
                    
                    Attachment tmpAttach = new Attachment();
                    tmpAttach.ParentId = currentCase.id;
                    tmpAttach.Name = uploadedFileName;
                    tmpAttach.Body = uploadedFileBody;
                    insert tmpAttach;
                    
                    tmpAttach = null;
                    uploadedFileName = null;
                    uploadedFileBody = null;
                }
                
                */
                system.debug(LoggingLevel.ERROR, 'CUSTOM FORM QUEUE: ' + theForm.Queue_ID__c);
                if(theForm.Queue_ID__c != null){
                    currentCase.OwnerId = theForm.Queue_ID__c;
                    currentCase.W2C_Route_To_Custom_Queue__c = true;
                }else{
                    //Hardcode Queues:
                    if(uLang.contains('en')){
                        currentCase.OwnerID = '00GU0000000gYws';
                    }else if(uLang == 'nl_NL'){
                        currentCase.OwnerID = '00GU0000000gYwk';
                    }else if(uLang == 'fr_FR'){
                        currentCase.OwnerID = '00GU0000000gYwm';
                    }else if(uLang == 'de_DE'){
                        currentCase.OwnerID = '00GU0000000gYwn';
                    }else if(uLang == 'it_IT'){
                        currentCase.OwnerID = '00GU0000000gYwo';
                    }else if(uLang == 'pt_BR'){
                        currentCase.OwnerID = '00GU0000000gYwp';
                    }else if(uLang == 'es_ES'){
                        currentCase.OwnerID = '00GU0000000gYwq';
                    }else if(uLang == 'sv_SE'){
                        currentCase.OwnerID = '00GU0000000gYwr';
                    }else{
                        currentCase.OwnerID = '00GU0000000gYwz';
                    }
                    currentCase.W2C_Route_To_Custom_Queue__c = false;
                }
                update currentCase;
                system.debug(LoggingLevel.ERROR, 'fgw currentCase == '+currentCase);
                pr = Page.Ghosts_Confirmation;
                pr.setRedirect(true);
                pr.getParameters().put('ulang',ulang);
                pr.getParameters().put('c',currentCase.Id);
             }
        }catch(exception e){
            system.debug(LoggingLevel.ERROR, 'ERROR '+e);
        }
        return pr;
    }
    public pageReference loadCaseNum(){        
        currentCaseNumber = 'ERROR';
        for(Case c : [SELECT CaseNumber FROM Case WHERE CreatedById = :UserInfo.getUserId() AND CreatedDate > :system.now().addMinutes(-2) ORDER BY CreatedDate DESC LIMIT 1]){
            currentCaseNumber = c.CaseNumber;
        }
        return null;
    }
    public pageReference doNothing(){return null;}
    
    
    /*
    ** Article Search
    */
    public FAQ__kav[] searchResults {
        get{
            try{
                if(searchResults == null){
                    return new FAQ__kav[]{};
                }else{
                    return searchResults;
                }
            }catch(Exception e){
                system.debug('ERROR '+e);
                return null;
            }
        } 
        set;
    }
    public void searchArticles(){
        try{
            String filter = ' WHERE PublishStatus = \'Online\' AND Language = \''+uLang+'\'';
            String suffix = ' WITH DATA CATEGORY Game_Title__c AT Call_of_Duty_Ghosts__c';
            String searchKey = ApexPages.CurrentPage().getParameters().containsKey('q') ? 'q' : 'search';
            if(ApexPages.CurrentPage().getParameters().containsKey(searchKey) && 
               ApexPages.CurrentPage().getParameters().get(searchKey) != '' &&  
               ApexPages.CurrentPage().getParameters().get(searchKey) != null
            ){//we have a clause so it is a search
                string clause = String.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get(searchKey));
                searchResults = (FAQ__kav[]) search.query('FIND \'*'+String.escapeSingleQuotes(clause)+'*\' IN ALL FIELDS RETURNING FAQ__kav (Id, Title, Summary '+filter+')'+suffix)[0];
            }else{//no clause, show all
                searchResults = database.query('SELECT Id, Title, Summary FROM FAQ__kav '+filter+suffix);
            }
        }catch(Exception e){
            system.debug('ERROR '+e);
        }
    }
    
    /*
    ** "fast" logged in function
    */  
    public boolean getIsloggedIn(){
        try{
            return UserInfo.getUserType()=='CSPLitePortal';
        }catch(Exception e){
            system.debug('ERROR '+e);
            return false;
        }
    }
    
    /*
    **
    ** If the page needs to display Alerts, use the methods below
    **
    */
    AlertWrapper[] alerts;
    public AlertWrapper[] getAlerts(){
        try{
            system.debug('getting alerts');
            system.debug('Ghosts.uLang == '+Ghosts.uLang);
            system.debug('static Ghost_Template.uLang == '+Ghost_Template.uLang);
            if(alerts == null){
                alerts = new AlertWrapper[]{};
                for(Alert__c alert : [SELECT TheLink__c, English__c, German__c, French__c, Spanish__c, Italian__c
                                      FROM Alert__c WHERE isActive__c = true AND Category__c INCLUDES(:gCtgy)
                                      ORDER by CreatedDate Desc])
                {
                    alerts.add(new AlertWrapper(alert));
                }
            }
            return alerts;
        }catch(Exception e){
            system.debug('ERROR '+e);
            return null;
        }
    }
    
    public class AlertWrapper{
        public String text {get; set;}
        public String articleId {get; set;}
        public AlertWrapper(Alert__c alert){
            try{
                String articleUrlName = '';
                String lang = 'en_US';

                for(String chunk : alert.TheLink__c.split('/')) articleUrlName = chunk;
                
                if(uLang.startsWith('de')){
                    this.text = alert.German__c;
                    lang = 'de';
                }else if(uLang.startsWith('fr')){
                    this.text = alert.French__c;
                    lang = 'fr';
                }else if(uLang.startsWith('it')){
                    this.text = alert.Italian__c;
                    lang = 'it';
                }else if(uLang.startsWith('es')){
                    this.text = alert.Spanish__c;
                    lang = 'es';
                }else{
                    this.text = alert.English__c;
                }
                for(FAQ__kav faq : [SELECT KnowledgeArticleId FROM FAQ__kav WHERE Language = :lang AND PublishStatus = 'ONLINE' AND UrlName = :articleUrlName]){
                    this.articleId = faq.Id ;
                }
            }catch(Exception e){
                system.debug('ERROR '+e);
            }
        }
    }
    
    public pageReference goHome(){
        pageReference pr = Page.ghostsmobile;
        pr.setRedirect(true);
        pr.getParameters().put('uil',uLang);
        return pr;
    }
}