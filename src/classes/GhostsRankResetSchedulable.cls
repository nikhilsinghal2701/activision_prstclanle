global class GhostsRankResetSchedulable implements Schedulable {
    
    global void execute(SchedulableContext ctx) {
        pullUnreadRecords();
    }
    global static void setSchedule(){
        try{
            system.schedule('GhostsRankResetSchedulable', '0 0 * * * ? *' , new GhostsRankResetSchedulable());
        }catch(Exception e){
            system.debug(e);
        }
    }
    @future (callout=true)
    public static void pullUnreadRecords(){ 
        doCallout(system.now().addHours(-1), system.now());        
    }
    public static void doCallout(DateTime openWindow, DateTime closeWindow){
        list<scrupdate> scrlist;
        list<case> caselist;
        map<string,scrupdate> scrmap = new map<string,scrupdate>(); 
        
        DwProxy.service serv = new dwProxy.service();
        DwProxy_utils utils = new DwProxy_utils();
        String sessionId = utils.getSessionId('Demonware Promotion Code Id Sync');
        serv.timeout_x = 120000;
        Integer hourOffset = -8;
        //Long prevHour = (datetime.now().addHours(hourOffset).getTime()/1000)-3600; //orig
        Long prevHour = openWindow.addHours(hourOffset).getTime()/1000;
        //All Time String
        //string query = 'https://contra.infinityward.net/svc/ghosts/live/getStatResetStatusUpdatesByInterval/'+'0'+'/'+string.valueof(datetime.now().getTime()/1000);
        
        //Last Hour string            
        //string query = 'https://contra.infinityward.net/svc/ghosts/live/getStatResetStatusUpdatesByInterval/'+string.valueof(prevHour)+'/'+string.valueof(datetime.now().addHours(hourOffset).getTime()/1000);//orig
        string query = 'https://contra.infinityward.net/svc/ghosts/live/getStatResetStatusUpdatesByInterval/'+string.valueof(prevHour)+'/'+string.valueof(closeWindow.addHours(hourOffset).getTime()/1000);
        
        string resp;
        query += '@headers-exist@@usecsnetworkcreds#true';
        if(!test.isRunningTest()){
            resp = serv.passEncryptedGetData(query, sessionId);
        }
        else{
            resp = '[{"IWAgentComment":"Masked","Id":"20","Platform":"xbox","Status":"REPUTATION_BAN"},{"IWAgentComment":"IW Test Return Comment","Id":"21","Platform":"ps4","Status":"SUSPENDED"}]';
        }
        
        
        scrlist = (list<scrUpdate>) json.deserialize(resp,list<scrupdate>.class);  
        
        for(scrupdate scr: scrlist){
            scrmap.put(scr.platform + scr.id,scr);
            system.debug('IWAgentComment:' + scr.IWAgentComment);
            system.debug('DBID:' + scr.id);
            system.debug('Platform:' + scr.platform);
            system.debug('Status:' + scr.status);
        }
        
        system.debug('Map:' + scrmap);
        list<id> caseidtoupdate = new list<id>();
        list<case> casetoupdate = new list<case>();
        list<case> tempcaselist = new list<case>();
        list<stat_corruption_report__c> scrtoupdate = new list<stat_corruption_report__c>();
        list<stat_corruption_report__c> scrtoget = [select id, schedulekey__c, case__r.status from stat_corruption_report__c where schedulekey__c in :scrmap.keyset()];
        if(scrtoget.size()!=0){
            for(stat_corruption_report__c scr:scrtoget){
                caseidtoupdate.add(scr.case__c);
            }
            tempcaselist = [select id from case where id in:caseidtoupdate];
            
            for (case c:tempcaselist){
                c.status='Updated';
                casetoupdate.add(c);
            }
            update casetoupdate;
        }
        
        if(scrtoget.size()==0){
            system.debug('List is empty');
        }
        else{
            system.debug(scrtoget);
            for(stat_corruption_report__c scr:scrtoget){
                scr.status__c = scrmap.get(scr.schedulekey__c).status;
                scr.iwagentcomment__c = scrmap.get(scr.schedulekey__c).IWAgentComment;
                scr.iwupdatetime__c = datetime.newinstance(long.valueof(scrmap.get(scr.schedulekey__c).IWUpdateTime)*1000L);
                scr.case__r.status = 'Updated';
                scrtoupdate.add(scr);                
            }
            update scrtoupdate;    
        }  
    }
    
    
    public class scrUpdate{
        public string IWAgentComment{get;set;}
        public string IWUpdateTime{get;set;}
        public string Id{get;set;}
        public string platform{get;set;}
        public string status{get;set;}        
    }   
    
}