@isTest
public class Ghosts_Tests{

//Testing Ghost_template.class
    public static testMethod void test1(){
        Ghost_Template tstCls = new Ghost_Template(); //this should trip the try{}catch(){}
    }

    public static testMethod void test2(){
        test.setCurrentPage(page.ghosts_confirmation);
        ApexPages.currentPage().getParameters().put('uil','en');
        system.assertEquals(Ghost_Template.uLang, 'en_US');
    }
    public static testMethod void test3(){
        test.setCurrentPage(page.ghosts_confirmation);
        ApexPages.currentPage().getParameters().put('uil','de');
        system.assertEquals(Ghost_Template.uLang, 'de_DE');
    }
    public static testMethod void test4(){
        test.setCurrentPage(page.ghosts_confirmation);
        ApexPages.currentPage().getParameters().put('uil','fr');
        system.assertEquals(Ghost_Template.uLang, 'fr_FR');
        system.debug('Ghosts.uLang == '+Ghosts.uLang);
    }
    public static testMethod void test5(){
        test.setCurrentPage(page.ghosts_confirmation);
        ApexPages.currentPage().getParameters().put('uil','it');
        system.assertEquals(Ghost_Template.uLang, 'it_IT');
    }
    public static testMethod void test6(){
        test.setCurrentPage(page.ghosts_confirmation);
        ApexPages.currentPage().getParameters().put('uil','es');
        system.assertEquals(Ghost_Template.uLang, 'es_ES');
    }
    
    public static testMethod void testCookie(){
        test.setCurrentPage(page.ghosts_confirmation);
        Cookie cookie = new Cookie('uil', 'en', null, -1, false);
        ApexPages.currentPage().setCookies(new Cookie[]{cookie});
        system.assertEquals(Ghost_Template.uLang, 'en_US');
    }

//Testing Ghost_Article.class
    public static testMethod void testGA_1(){
        Ghost_Article tstCls = new Ghost_Article();
        tstCls.recordHelpful(); //trip error
        FAQ__kav theFaq = new FAQ__kav(
            Language = 'en_US',
            Title = 'hi you guys',
            UrlName = 'hi-you-guys'
        );
        insert theFaq;
        test.setCurrentPage(Page.Ghost_Article);
        ApexPages.currentPage().getParameters().put('id',theFaq.id);
        tstCls = new Ghost_Article();
        tstCls.boolArticleWasHelpful = true;
        boolean tstBool = tstCls.boolArticleWasHelpful;
        tstCls.recordHelpful();
        tstCls.boolArticleWasHelpful = false;
        tstCls.recordHelpful();
    }

//Testing Ghosts.class
    /*String tStr; 
    WebToCase_GameTitle__c[] tPlatforms;
    map<String, WebToCase_SubType__c[]> tTypeIdToSubTypes;
    map<String, Id> tSubTypeIdToArticleId;
    map<String, FAQ__kav> tArticleIdToArticle;
    map<String, String> tSubTypeIdToFormId;
    map<String, WebToCase_Form__c> tFormIdToForm;
    map<String, map<String, Contact_Us_Mobile.FormResponse>> tSubmittedFormValues;*/
    public static testMethod void testGhosts_initalization(){
        Ghosts tstCls = new Ghosts();
        tstCls.submitForm();
        tstCls.doNothing();
        insert new case(Subject = 'test', comment_not_required__c = true);
        tstCls.loadCaseNum();
        Faq__kav[] tFAQs = tstCls.SearchResults;
        tstCls.searchArticles();
        tstCls.getIsLoggedIn();
        system.assertEquals(Ghosts.uLang, 'en_US');
    }  
    public static testMethod void createAlert(){
        Alert__c alert = new Alert__c(
            TheLink__c = 'https://csuat-activisionsupport.cs9.force.com/articles/en_US/FAQ/hi-you-guys',
            English__c = 'EN Link Text',
            German__c = 'DE Link Text',
            French__c = 'FR Link Text',
            Spanish__c = 'ES Link Text',
            Italian__c = 'IT Link Text',
            isActive__c = true,
            Category__c = Ghosts.gCtgy
        );
        insert alert;
        FAQ__kav theFaq = new FAQ__kav(
            Language = 'en_US',
            Title = 'hi you guys',
            UrlName = 'hi-you-guys'
        );
        insert theFaq;
        theFaq = [SELECT Id, KnowledgeArticleId FROM FAQ__kav WHERE Id = :theFaq.id];
        KbManagement.PublishingService.publishArticle(theFaq.KnowledgeArticleId, true);
    }
    public static testMethod void testGhosts_Alerts1(){
        test.setCurrentPage(page.ghosts_confirmation);
        ApexPages.currentPage().getParameters().put('ulang','en');
        Ghosts tstCls = new Ghosts();
        createAlert();
        tstCls.getAlerts();
    }
    public static testMethod void testGhosts_Alerts2(){
        test.setCurrentPage(page.ghosts_confirmation);
        ApexPages.currentPage().getParameters().put('ulang','de');
        Ghosts tstCls = new Ghosts();
        createAlert();
        tstCls.getAlerts();
    }
    public static testMethod void testGhosts_Alerts3(){
        test.setCurrentPage(page.ghosts_confirmation);
        ApexPages.currentPage().getParameters().put('ulang','es');
        Ghosts tstCls = new Ghosts();
        createAlert();
        tstCls.getAlerts();
    }
    public static testMethod void testGhosts_Alerts4(){
        test.setCurrentPage(page.ghosts_confirmation);
        ApexPages.currentPage().getParameters().put('ulang','it');
        Ghosts tstCls = new Ghosts();
        createAlert();
        tstCls.getAlerts();
    }
    public static testMethod void testGhosts_Alerts5(){
        test.setCurrentPage(page.ghosts_confirmation);
        ApexPages.currentPage().getParameters().put('ulang','fr');
        Ghosts tstCls = new Ghosts();
        createAlert();
        tstCls.getAlerts();
    }
    public static testMethod void testGhosts_Search1(){
        FAQ__kav theFaq = new FAQ__kav(
            Language = 'en_US',
            Title = 'hi you guys',
            UrlName = 'hi-you-guys'
        );
        insert theFaq;
        FAQ__DataCategorySelection fdcs = new FAQ__DataCategorySelection(
            ParentId = theFaq.id,
            DataCategoryGroupName = 'Game_Title',
            DataCategoryName = 'Call_of_Duty_Ghosts'
        );
        insert fdcs;
        test.setCurrentPage(page.ghosts_confirmation);
        ApexPages.currentPage().getParameters().put('q','guys');
        Ghosts tstCls = new Ghosts();
        tstCls.searchArticles();
    }
    public static testMethod void testFormSubmission(){
        WebToCase__c w2c = new WebToCase__c(
          Language__c = 'en_US',
          chatEnabled__c = true,
          chatEnabledMessage__c = 'tstmsg',
          chatDisabledMessage__c = 'chatDisMsg',
          phoneEnabled__c = true,
          chatOfflineMessage__c = 'offlineMsg',
          phoneDisabledMessage__c = 'pDisabledMsg',
          phone_Hours__c = '01:00 GMT',
          Phone_Number__c = '100',
          Chat_Weekday_Start__c = '01:00GMT',
          Chat_Weekday_End__c = '19:00GMT',
          Chat_Weekend_Start__c = '01:00GMT',
          Chat_Weekend_End__c = '19:00GMT'
        );
        insert w2c;
        
        WebToCase_Platform__c w2cp = new WebToCase_Platform__c(Name__c = 'Xbox', Image__c = 'xbox', isActive__c = true, WebToCase__c = w2c.id);
        insert w2cp;
          
        Public_Product__c pubProduct = new Public_Product__c();
        insert pubProduct;
          
        WebToCase_GameTitle__c testTitle1 = new WebToCase_GameTitle__c(
            Title__c = 'title', Image__c = 'Call_of_Duty_Ghosts', isActive__c = true,
            WebToCase_Platform__c = w2cp.id, Language__c = w2c.Language__c, Public_Product__c = pubProduct.id
        );
        insert testTitle1;
          
        WebToCase_Type__c testType1 = new WebToCase_Type__c(
            Description__c = 'type1', isActive__c = true, typeMap__c = 'testTypeMap', 
            WebToCase_GameTitle__c = testTitle1.id
        );
        insert testType1;
        
        WebToCase_SubType__c[] testSubTypes = new WebToCase_SubType__c[]{};
        for(String s : new String[]{'1','2','3','4','5'}){
            testSubTypes.add(
                new WebToCase_SubType__c(
                  Description__c = 'subType'+s,isActive__c = true, SubTypeMap__c = 'testSubTypeMap', WebToCase_Type__c = testType1.id
              )
            );
        }
        insert testSubTypes;

        WebToCase_SubType__c testSubTypeForm = new WebToCase_SubType__c(
            Description__c = 'subType6',
            isActive__c = true,
            SubTypeMap__c = 'testSubTypeMap',
            WebToCase_Type__c = testType1.id
        );
        insert testSubTypeForm;
          
        WebToCase_Form__c testForm = new WebToCase_Form__c(
            Name__c = 'testForm',
            submitButtonEnabled__c = true,
            IsActive__c = true,
            Queue_ID__c = '',
            WebToCase_SubType__c = testSubTypeForm.id
        );
        insert testForm;
          
        insert new WebToCase_FormField__c[]{
            new WebToCase_FormField__c(FieldType__c = 'Checkbox',Label__c = 'LABEL',Checkbox__c = true,WebToCase_Form__c = testForm.id),
            new WebToCase_FormField__c(FieldType__c = 'RichText',Label__c = 'LABEL',TextAreaLong__c = 'ABC123',WebToCase_Form__c = testForm.id),
            new WebToCase_FormField__c(FieldType__c = 'Text',Label__c = 'LABEL',Text__c = 'TEST',WebToCase_Form__c = testForm.id),
            new WebToCase_FormField__c(FieldType__c = 'TextArea',Label__c = 'LABEL',TextArea__c = 'TextAreaStuff',WebToCase_Form__c = testForm.id),
            new WebToCase_FormField__c(FieldType__c = 'TextAreaLong',Label__c = 'LABEL',TextAreaLong__c = 'TestAreaLongText',WebToCase_Form__c = testForm.id)
        };
        
        //Data prepped now TEST
        Ghosts tstCls = new Ghosts(); //this will load a form into the controller
        system.debug('tstCls.formIdToForm == '+tstCls.formIdToForm);
        for(String key : tstCls.formIdToForm.keySet()){
            if(key != '' && key != null && tstCls.formIdToForm.get(key) != new WebToCase_Form__c()){
                tstCls.SubmittedFormId = key;
                system.debug('key found == '+key);
                break;
            }
        }
        User testUser = [SELECT Id FROM User WHERE IsActive = TRUE AND Profile.Name LIKE '%OHV%' LIMIT 1];
        system.runAs(testUser){
            tstCls.submitForm();
        }
        
    }

}