@isTest
public class InboundSPHandler_Test {
    static testmethod void myTest(){
        case par = new case();
        par.status = 'new';
        par.subject = 'test';
        par.description = 'test';
        par.comments__c = 'F this validation';
        insert par;

        socialpost sp = new socialpost();
        sp.name = 'test';
        sp.parentid = par.id;
        sp.Provider = 'TWITTER';
        //insert sp;
        
        Account testaccount=new Account();
        testaccount.Name='Test account';
        testaccount.Type__c='Agency';
        insert testaccount;

        Contact testContact=new Contact();
        testContact.LastName='Test User';
        testContact.Languages__c='English';
        testContact.MailingCountry='US';
        testContact.AccountId=testaccount.id;
        insert testContact;

        socialpersona persona = new socialpersona();
        persona.name='test';
        persona.provider='TWITTER';
        persona.parentid=testContact.id;

        //insert persona;
        
        Map<string,Object> rawData = new Map<string,Object>{
            'originalScreenName'=>null,
            'assignedToValue'=>null,
            'topicProfileName'=>'@ATVIAssist',
            'r6TopicId'=>'507745',
            'kloutScore'=>null,
            'mediaUrls'=>null,
            'analyzerScore'=>null,
            'isFollowingUsValue'=>null,
            'sentiment'=>'Neutral',
            'r6PostId'=>'306752180503',
            'followers'=>'28',
            'friends'=>null,
            'inboundlinkcount'=>null,
            'topicType'=>'Managed',
            'extendedSocialData'=>null,
            'r6SourceId'=>'4407732675',
            'areWeFollowing'=>null,
            'originalFullName'=>null,
            'postTags'=>'',
            'listed'=>0,
            'lastName'=>null,
            'classifiers'=>null,
            'status'=>null,
            'privacy'=>null,
            'externalPostId'=>'524338166749474816',
            'isBlacklisted'=>null,
            'statusMessage'=>null,
            'content'=>'@ATVIAssist thanks',
            'shares'=>null,
            'profileUrl'=>'http://twitter.com/IDoomzz',
            'bio'=>'ZomB Killa',
            'keywordGroupName'=>null,
            'origins'=>null,
            'postPriority'=>null,
            'viewCountValue'=>null,
            'recipientType'=>'Person',
            'jobId'=>'risJobId:134689:00DU0000000HMgwMAG_1532582728',
            'postDate'=>'2014-10-20T23:15:35Z',
            'threadSize'=>null,
            'likesAndVotes'=>null,
            'spamRating'=>'NotSpam',
            'mediaType'=>'Twitter',
            'author'=>'IDoomzz',
            'skipCreateCase'=>false,
            'following'=>139,
            'profileIconURL'=>'http://pbs.twimg.com/profile_images/508619610334457856/-DH73j2__normal.jpeg',
            'mediaProvider'=>'TWITTER',
            'firstName'=>null,
            'salesforcePostId'=>null,
            'originalAvatar'=>null,
            'authorType'=>'Person',
            'externalUserId'=>'2744685581',
            'postUrl'=>'http://twitter.com/IDoomzz/statuses/524338166749474816',
            'sourceTags'=>'',
            'messageType'=>'Reply',
            'commentCount'=>null,
            'engagementLevel'=>null,
            'recipientId'=>'85215038',
            'source='=>'TWEET FROM: IDoomzz',
            'classification'=>null,
            'replyToExternalPostId'=>'524337626921582592',
            'realName'=>'Its DoomZz',
            'notes'=>null,
            'harvestDate'=>'2014-10-20T23:15:36Z',
            'tweets'=>132,
            'uniqueCommentors'=>null
        };
        
        test.startTest();
        InboundSocialPostHandlerImplCustom cls = new InboundSocialPostHandlerImplCustom();
        cls.getMaxNumberOfDaysClosedToReopenCase();
        cls.getDefaultAccountId();
        cls.handleInboundSocialPost(sp, persona, rawData);
        cls.handleInboundSocialPost(sp, new socialpersona(), rawData);
        test.stopTest();
    }
}