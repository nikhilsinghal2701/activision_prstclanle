global virtual class InboundSocialPostHandlerImplCustom implements Social.InboundSocialPostHandler
{
    // Reopen case if it has not been closed for more than this number
    global virtual Integer getMaxNumberOfDaysClosedToReopenCase() {
        //Original R6 closed days was 0. Recreate new case of thread continues
        return 0;
    }
    global virtual String getDefaultAccountId() {
        return null;
    }
    global Social.InboundSocialPostResult handleInboundSocialPost(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        system.debug('*****RawData******'+rawData);
        for(string s:rawData.keySet()){
            system.debug('Key:' + s + 'Value:' + rawdata.get(s) );
        }
        Social.InboundSocialPostResult result = new Social.InboundSocialPostResult();
        result.setSuccess(true);
        matchPost(post);
        matchPersona(persona);
        if (post.Id != null) {
            handleExistingPost(post, persona);
            return result;
        }
        setReplyTo(post, persona, rawData);
        buildPersona(persona);
        Case parentCase = buildParentCase(post, persona, rawData);
        setRelationshipsOnPost(post, persona, parentCase);
        upsert post;
        return result;                                                                    
    }
    private void handleExistingPost(SocialPost post, SocialPersona persona) {
        update post;
        if (persona.id != null)
            //Here?
            update persona;
    }
    private void setReplyTo(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        SocialPost replyTo = findReplyTo(post, persona, rawData);
        if(replyTo.id != null) {
            post.replyToId = replyTo.id;
            post.replyTo = replyTo;
        }
    }
    private SocialPersona buildPersona(SocialPersona persona) {
        if (persona.Id == null)
            createPersona(persona);
        else
            update persona;
        return persona;
    }
    private Case buildParentCase(SocialPost post, SocialPersona persona, Map<String, Object> rawData){
        Case parentCase = findParentCase(post, persona);
        if (caseShouldBeReopened(parentCase))
            reopenCase(parentCase);
        else if(! hasSkipCreateCaseIndicator(rawData) && (parentCase.id == null ||
                                                          parentCase.isClosed))
            parentCase = createCase(post, persona, rawData);
        return parentCase;
    }
    private boolean caseShouldBeReopened(Case c){
        return c.id != null && c.isClosed && System.now() <
            c.closedDate.addDays(getMaxNumberOfDaysClosedToReopenCase());
    }
    private void setRelationshipsOnPost(SocialPost postToUpdate, SocialPersona persona, Case parentCase) {
        if (persona.Id != null)
            postToUpdate.PersonaId = persona.Id;
        if(parentCase.id != null)
            postToUpdate.ParentId = parentCase.Id;
    }
    private Case createCase(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        Case newCase = new Case(subject = post.Name);
        if (persona != null && persona.ParentId != null) {
            if (persona.ParentId.getSObjectType() == Contact.sObjectType)
                newCase.ContactId = persona.ParentId;
        }
        newCase.RecordTypeId = [select id, name from recordtype where name='Social Media'].id;
        newcase.OwnerId = [select id from group where name='Radian6 Social' and type='Queue'].id;
        newCase.Status = 'Open';
        newCase.R6Service__MediaProvider__c = string.valueOf(rawData.get('mediaProvider'));
        insert newCase;
        return newCase;
    }
    private Case findParentCase(SocialPost post, SocialPersona persona) {
        Case parentCase = new Case();
        if (post.ReplyTo != null && (post.ReplyTo.IsOutbound || post.ReplyTo.PersonaId ==
                                     persona.Id))
            parentCase = findParentCaseFromPostReply(post,persona);
        else if((post.messageType == 'Direct' || post.messageType == 'Private') &&
                post.Recipient != null && String.isNotBlank(post.Recipient))
            parentCase = findParentCaseFromRecipient(post, persona);
        else{
            list<case> cases = [select id, isClosed, status, closeddate from case where recordtype.name='Social Media' and isclosed=false and contactid=:persona.ParentId limit 1];
            if(cases.size()>0){
                parentCase = cases[0];
            }
        }
        return parentCase;
    }
    private Case findParentCaseFromPostReply(SocialPost post,SocialPersona persona){
        list<case> threadcases = [select id, isClosed, status, closeddate from case where recordtype.name='Social Media' and isclosed=false and contactid=:persona.ParentId limit 1];
            if(!threadcases.isEmpty()){
                return threadcases[0];
            }
        List<Case> cases = [SELECT Id, IsClosed, Status, ClosedDate FROM Case WHERE Id =
                            :post.ReplyTo.ParentId LIMIT 1];
        if(!cases.isEmpty())
            return cases[0];
        return new Case();
    }
    private Case findParentCaseFromRecipient(SocialPost post, SocialPersona persona){
        list<case> threadcases = [select id, isClosed, status, closeddate from case where recordtype.name='Social Media' and isclosed=false and contactid=:persona.ParentId limit 1];
            if(!threadcases.isEmpty()){
                return threadcases[0];
            }
        List<Case> cases = [SELECT Id, IsClosed, Status, ClosedDate FROM Case WHERE id =
                            :findReplyToBasedOnRecipientsLastPostToSender(post, persona).parentId LIMIT 1];
        if(!cases.isEmpty())
            return cases[0];
        
        return new Case();
    }
    private void reopenCase(Case parentCase) {
        SObject[] status = [SELECT MasterLabel FROM CaseStatus WHERE IsClosed = false AND
                            IsDefault = true];
        parentCase.Status = ((CaseStatus)status[0]).MasterLabel;
        update parentCase;
    }
    private void matchPost(SocialPost post) {
        if (post.Id != null || post.R6PostId == null) return;
        List<SocialPost> postList = [SELECT Id FROM SocialPost WHERE R6PostId =
                                     :post.R6PostId LIMIT 1];
        if (!postList.isEmpty())
            post.Id = postList[0].Id;
    }
    private SocialPost findReplyTo(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        if(post.replyToId != null && post.replyTo == null)
            return findReplyToBasedOnReplyToId(post);
        if(rawData.get('replyToExternalPostId') != null &&
           String.isNotBlank(String.valueOf(rawData.get('replyToExternalPostId'))))
            return findReplyToBasedOnExternalPostIdAndProvider(post, String.valueOf(rawData.get('replyToExternalPostId')), persona);
            //return findReplyToBasedOnExternalPostIdAndProvider(post, String.valueOf(rawData.get('replyToExternalPostId')));            
        return new SocialPost();
    }
    private SocialPost findReplyToBasedOnReplyToId(SocialPost post){
        List<SocialPost> posts = [SELECT Id, ParentId, IsOutbound, PersonaId FROM SocialPost
                                  WHERE id = :post.replyToId LIMIT 1];
        if(posts.isEmpty())
            return new SocialPost();
        return posts[0];
    }
    private SocialPost findReplyToBasedOnExternalPostIdAndProvider(SocialPost post, String externalPostId, SocialPersona persona){
        List<SocialPost> posts = [SELECT Id, ParentId, IsOutbound, PersonaId FROM SocialPost
                                  WHERE Provider = :post.provider AND ExternalPostId = :externalPostId LIMIT 1];
        if(posts.isEmpty())
            return new SocialPost();
        return posts[0];
    }
    private SocialPost findReplyToBasedOnRecipientsLastPostToSender(SocialPost post, SocialPersona persona){
        List<SocialPost> posts = [SELECT Id, ParentId, IsOutbound, PersonaId FROM SocialPost
                                  WHERE provider = :post.provider AND OutboundSocialAccount.ProviderUserId = :post.Recipient
                                  AND ReplyTo.Persona.id = :persona.id ORDER BY CreatedDate DESC LIMIT 1];
        if(posts.isEmpty())
            return new SocialPost();
        return posts[0];
    }
    private void matchPersona(SocialPersona persona) {
        if (persona != null && persona.ExternalId != null &&
            String.isNotBlank(persona.ExternalId)) {
                List<SocialPersona> personaList = [SELECT Id, ParentId FROM SocialPersona WHERE
                                                   Provider = :persona.Provider AND
                                                   ExternalId = :persona.ExternalId LIMIT 1];
                if ( !personaList.isEmpty()) {
                    persona.Id = personaList[0].Id;
                    persona.ParentId = personaList[0].ParentId;
                }
            }
    }
    private void createPersona(SocialPersona persona) {
        if (persona == null || (persona.Id != null && String.isNotBlank(persona.Id)) ||
            !isThereEnoughInformationToCreatePersona(persona))
            return;
        SObject parent = createPersonaParent(persona);
        persona.ParentId = parent.Id;
        insert persona;
    }
    private boolean isThereEnoughInformationToCreatePersona(SocialPersona persona){
        return persona.ExternalId != null && String.isNotBlank(persona.ExternalId) &&
            persona.Name != null && String.isNotBlank(persona.Name) &&
            persona.Provider != null && String.isNotBlank(persona.Provider) &&
            persona.provider != 'Other';
    }
    private boolean hasSkipCreateCaseIndicator(Map<String, Object> rawData) {
        Object skipCreateCase = rawData.get('skipCreateCase');
        return skipCreateCase != null &&
            'true'.equalsIgnoreCase(String.valueOf(skipCreateCase));
    }
    global virtual SObject createPersonaParent(SocialPersona persona) {
        String name = persona.Name;
        if (persona.RealName != null && String.isNotBlank(persona.RealName))
            name = persona.RealName;
        String firstName = '';
        String lastName = 'unknown';
        if (name != null && String.isNotBlank(name)) {
            firstName = name.substringBeforeLast(' ');
            lastName = name.substringAfterLast(' ');
            if (lastName == null || String.isBlank(lastName))
                lastName = firstName;
        }
        
        firstName = firstName.substring(0, firstName.length() > 40 ? 40 : firstName.length());
        lastName = lastName.substring(0, lastName.length() > 80 ? 80 : lastName.length());
        
        Contact contact = new Contact(LastName = lastName, FirstName = firstName);
        String defaultAccountId = getDefaultAccountId();
        if (defaultAccountId != null)
            contact.AccountId = defaultAccountId;
        System.debug('Handler Debug:'+contact);
        insert contact;
        return contact;
    }
}