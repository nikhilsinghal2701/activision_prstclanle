global class JIVE_ArticleIntegration implements Schedulable{

    static final boolean isProd = UserInfo.getOrganizationId() == '00DU0000000HMgwMAG';
    //Jive6
    //public string jiveEndpoint = isProd ? 'http://activision-public.hosted.jivesoftware.com/api/rest/salesforce/articles' : 'http://activision-public.uat5.hosted.jivesoftware.com/api/rest/salesforce/articles';
    //Jive7
    public string jiveEndpoint = isProd ? 'https://community.activision.com/api/core/ext/sfdc-article-service/v3/articles' : 'https://community.uat.activision.com/api/core/ext/sfdc-article-service/v3/articles';
    public Integer minutesFromNowToOpenModWindow = -3;
    public Integer minutesFromNowToCloseModWindow = 0;
    
    global void execute(SchedulableContext sc){
        try{
            syncArticles();
        }catch(Exception e){
            system.debug(e);
        }
        try{
            //updateSchedule();
        }catch(Exception e){
            system.debug(e);
        }
    }
    public void setSchedule(){
        Savepoint sp = Database.setSavepoint();
        try{
        /*API v 28, just use 30 slots*/
            for(Integer i = 0; i < 60; i+=2){
                system.schedule(
                    'JiveArticleSyndication_'+string.valueOf(i), 
                    '0 '+string.valueOf(i)+' * * * ?', 
                    new JIVE_ArticleIntegration()
                );
            }
            
        /*API v 29
            //remove current job
            ApexClass self = [SELECT Id FROM ApexClass WHERE Name = 'JIVE_ArticleIntegration'];
            for(CronTrigger ct : [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name = 'JiveArticleSyndication' OR CronJobDetail.Name = :self.Id]){
                system.abortjob(ct.id);
            }
            
            //add new schedule
            //second minute hour day_of_month month day_of_week (year (optional))
            String schedule = '0 '+(system.now().minute()+2)+' * * * ?'; //at zero seconds of the next minute
            system.schedule('JiveArticleSyndication', schedule, new JIVE_ArticleIntegration());
        */
        }catch(Exception e){
            Database.rollback(sp);
        }
    }
    
    public class articleWrapper{
        //do not add, remove or change this class.  Doing so will break the integration
        String artid, title, summary, action, url, articlenumber;
        public String[] spaces = new String[]{};
        DateTime lastmoddate;
        public articleWrapper(FAQ__kav art, String pCommand){
            action = pCommand;
            if(isProd) url = 'http://support.activision';
            else url = 'http://csuat-activisionsupport.cs11.force';
            url += '.com/articles/en_US/FAQ/'+art.UrlName;
            artid = art.KnowledgeArticleId+','+art.Id;
            title = art.Title;
            articlenumber = art.ArticleNumber;
            summary = art.Summary;
            lastmoddate = art.LastModifiedDate;
        }
    }
    public void calcSpaces(ArticleWrapper[] arts){
        map<String, String> dataCategory2jiveSpace = new map<String, String>();
        for(Jive_Article_Mapping__c mapping : [SELECT Data_Category_Name__c, Jive_Space_URL__c FROM Jive_Article_Mapping__c]){
            dataCategory2jiveSpace.put(mapping.Data_Category_Name__c, mapping.Jive_Space_URL__c);
        }
        map<Id, ArticleWrapper> artVersionToArt = new map<Id, ArticleWrapper>();
        for(ArticleWrapper art : arts){
            String artVerId = art.artid.split(',')[1];
            art.artId = art.artid.split(',')[0]; 
            artVersionToArt.put(artVerId, art);
        }
        for(FAQ__DataCategorySelection dcs : [SELECT ParentId, DataCategoryName FROM FAQ__DataCategorySelection WHERE ParentId = :artVersionToArt.keySet()]){
            if(dataCategory2jiveSpace.containsKey(dcs.DataCategoryName)){
                artVersionToArt.get(dcs.ParentId).spaces.add(dataCategory2jiveSpace.get(dcs.DataCategoryName));
            }
        }
        for(Id key : artVersionToArt.keySet()){
            if(artVersionToArt.get(key).spaces.isEmpty()){
                artVersionToArt.get(key).spaces.add('/community/activision');
            }else{
                set<String> deDupe = new set<String>();
                for(String space : artVersionToArt.get(key).spaces) deDupe.add(space);
                artVersionToArt.get(key).spaces = new String[]{};
                artVersionToArt.get(key).spaces.addAll(deDupe);
            }
        }
    }
    public void transmitArticle(Id articleId){
        String articleQuery = 'SELECT Id, KnowledgeArticleId, Title, PublishStatus, Summary, LastModifiedDate, IsVisibleInPKB, UrlName, ArticleNumber FROM FAQ__kav ';
        for(FAQ__kav art : database.query(articleQuery+' WHERE Id = \''+articleId+'\'')){
            String command = 'upsert';
            if(art.PublishStatus != 'Online' || art.IsVisibleInPKB == false) command = 'delete';
            articleWrapper[] articlesToTransmit = new articleWrapper[]{new articleWrapper(art, command)};
            calcSpaces(articlesToTransmit);
            doTransmit(articlesToTransmit);
            break;
        }
    }
    public void syncArticles(){
        DateTime modWindowOpen = system.now().addMinutes(minutesFromNowToOpenModWindow);
        DateTime modWindowClose = system.now().addMinutes(minutesFromNowToCloseModWindow);
        String articleQuery = 'SELECT Id, KnowledgeArticleId, Title, PublishStatus, Summary, LastModifiedDate, IsVisibleInPKB, UrlName, ArticleNumber FROM FAQ__kav ';      
        articleQuery += 'WHERE LastModifiedDate > :modWindowOpen AND LastModifiedDate < :modWindowClose AND Language = \'en_US\' AND PublishStatus = ';
        articleWrapper[] articlesToTransmit = new articleWrapper[]{};
        system.debug('modWindowOpen == '+modWindowOpen);
        
        String onlineArticleQuery = articleQuery + '\'Online\'';
        String offlineArticleQuery = articleQuery + '\'Archived\'';
        //have to do two loops because the "PublishStatus" filter can only have 1 values
        for(FAQ__kav newArt : database.query(onlineArticleQuery)){
            String command = 'upsert';
            if(! newArt.IsVisibleInPKB){
               command = 'delete';
            }
            articlesToTransmit.add(new articleWrapper(newArt, command));
        }
        for(FAQ__kav oldArt : database.query(offlineArticleQuery)){
            articlesToTransmit.add(new articleWrapper(oldArt, 'delete'));
        }
        if(! articlesToTransmit.isEmpty()){
            calcSpaces(articlesToTransmit);
            doTransmit(articlesToTransmit);
        }
    }
    public void doTransmit(articleWrapper[] arts){
        for(articleWrapper aw : arts)
            if(aw.artId.contains(',')) aw.artId = aw.artId.split(',')[0];
        String JSONmsg = JSON.serialize(arts);
        system.debug('JSONmsg == '+JSONmsg);
        doTransmit(JSONmsg, jiveEndPoint);
    }
    @future(callout=true)
    public static void doTransmit(String JSONmsg, String jiveEndPoint){
        system.debug('JSONmsg == '+JSONmsg);
        HttpResponse resp; 
        
        http h = new http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(jiveEndPoint);
        req.setHeader('Authorization', 'Basic c2ZkYy1hcnRpY2xlLXNlcnZpY2U6SiF2ZTRzZmRj');
        req.setHeader('Content-Type','application/json');
        req.setMethod('POST');
        req.setBody(JSONmsg);
        req.setTimeout(120000);
        system.debug('req debug beginning here****************************************************');
        system.debug(req);
        resp = h.send(req); 
        system.debug('resp debug beginning here****************************************************');
        system.debug(resp);
        try{
        system.debug('resp.getBody() == '+resp.getBody());
        }catch(Exception e){ //if resp is null
        }
        
        if(resp.getStatusCode() == 400){
            try{
                JIVE_Response[] responses = (List<JIVE_ArticleIntegration.JIVE_Response>) JSON.deserialize(resp.getBody(), List<JIVE_ArticleIntegration.JIVE_Response>.class);
                JIVE_ArticleIntegrationFailure__c[] failureLogs = new JIVE_ArticleIntegrationFailure__c[]{};
                for(JIVE_Response rsp : responses){
                    //Error options are: 
                    //*Community UnauthorizedException for forums 
                    //**Usually means that the space does not allow documents to be created so the Jive Administrators would need to fix it
                    //*Space not found: [xxx]
                    //**Means that the space no longer exists or has a typo
                    failureLogs.add(new JIVE_ArticleIntegrationFailure__c(
                        Article_Id__c = rsp.artId, 
                        Error__c = rsp.error
                    ));
                }
                insert failureLogs;
            }catch(Exception e){
                system.debug('ERROR: '+e);
                system.debug('HttpResponse: '+resp);
            }
        }
    }
    class JIVE_Response{
        public String artId {get; set;}
        public String error {get; set;}
    }
    
}