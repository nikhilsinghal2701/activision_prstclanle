@isTest
public class JIVE_ArticleIntegration_Test{

    public static testMethod void createData(){
        FAQ__kav[] faqKAVs = new FAQ__kav[]{
            new FAQ__kav(UrlName = '123',Title = '123',Language = 'en_US',IsVisibleInPkb = true,IsVisibleInCsp = true),
            new FAQ__kav(UrlName = '456',Title = '456',Language = 'en_US',IsVisibleInPkb = true,IsVisibleInCsp = true),
            new FAQ__kav(UrlName = '789',Title = '789',Language = 'en_US',IsVisibleInPkb = true,IsVisibleInCsp = true),
            new FAQ__kav(UrlName = '012',Title = '012',Language = 'en_US',IsVisibleInPkb = false,IsVisibleInCsp = true)
        };
        insert faqKAVs;
        FAQ__ka[] kas = [SELECT Id FROM FAQ__ka];
        Integer kaCount = [SELECT count() FROM FAQ__ka];
        system.debug('fgw kaCount == '+kaCount);
        KbManagement.PublishingService.publishArticle(kas[0].id, true);
        KbManagement.PublishingService.publishArticle(kas[1].id, true);
        KbManagement.PublishingService.publishArticle(kas[2].id, true);
        KbManagement.PublishingService.publishArticle(kas[3].id, true);
        KbManagement.PublishingService.archiveOnlineArticle(kas[1].id, null);
        
        //string[] categories = new string[]{};
        try {
            //Creating a list of pair objects to use as a parameter for the describe call
            DataCategoryGroupSobjectTypePair[] pairs = new DataCategoryGroupSobjectTypePair[]{};
            
            //Looping throught the describeDataCategoryGroups to create the list of pairs for the second describe call
            for(DescribeDataCategoryGroupResult singleResult : Schema.describeDataCategoryGroups(new String[]{'KnowledgeArticleVersion'})){
                DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
                p.setSobject(singleResult.getSobject());
                p.setDataCategoryGroupName(singleResult.getName());
                pairs.add(p);
            }
            
            //Getting data from the result
            
            for(DescribeDataCategoryGroupStructureResult singleResult : Schema.describeDataCategoryGroupStructures(pairs, false)){
                DataCategory[] allCategories = getAllCategories(singleResult.getTopCategories());//Recursively get all the categories
                FAQ__DataCategorySelection cat = new FAQ__DataCategorySelection();
                cat.ParentId = faqKAVs[2].Id;
                cat.DataCategoryGroupName = singleResult.label;
                Jive_Article_Mapping__c[] jMaps = new Jive_Article_Mapping__c[]{};
                for(DataCategory category : allCategories) {
                    Jive_Article_Mapping__c jMap = new Jive_Article_Mapping__c();
                    jMap.Data_Category_Name__c = category.getName();
                    jMap.Jive_Space_URL__c = '/xyz';
                    jMaps.add(jMap);
                    cat.DataCategoryName = category.getName();
                    if(jMaps.size() == 2) break;
                }
                insert jMaps;
                insert cat;
                break; 
            }
        } catch (Exception e){
            system.debug('ERROR == '+e);
        }
    }

    private static DataCategory[] getAllCategories(DataCategory [] categories){
        if(categories.isEmpty()){
            return new DataCategory[]{};
        } else {
            DataCategory [] categoriesClone = categories.clone();
            DataCategory category = categoriesClone[0];
            DataCategory[] allCategories = new DataCategory[]{category};
            categoriesClone.remove(0);
            categoriesClone.addAll(category.getChildCategories());
            allCategories.addAll(getAllCategories(categoriesClone));
            return allCategories;
        }
    }

    public static testMethod void genericTests(){
        JIVE_ArticleIntegration cls = new JIVE_ArticleIntegration();
        //remove schedules if they exist
        for(CronTrigger ct : [SELECT Id FROM CronTrigger]){
            system.abortJob(ct.Id);
        }
        cls.setSchedule(); //set schedules
        cls.setSchedule(); //set schedules again so they fail
    }
    public static testMethod void testTransmitSinglePublishedArticle(){
        JIVE_ArticleIntegration cls = new JIVE_ArticleIntegration();
        createData();
        Id faqId = [SELECT Id FROM FAQ__kav WHERE PublishStatus = 'ONLINE' AND Language = 'en_US' LIMIT 1].Id;
        test.setMock(HttpCalloutMock.class, new JIVE_ArticleIntegration_mockResponse(true));
        test.startTest();
        cls.transmitArticle(faqId);
        test.stopTest();
    }
    public static testMethod void testTransmitSingleArchivedArticle(){
        JIVE_ArticleIntegration cls = new JIVE_ArticleIntegration();
        createData();
        Id faqId = [SELECT Id FROM FAQ__kav WHERE PublishStatus = 'Archived' AND Language = 'en_US' LIMIT 1].Id;
        test.setMock(HttpCalloutMock.class, new JIVE_ArticleIntegration_mockResponse(true));
        test.startTest();
        cls.transmitArticle(faqId);
        test.stopTest();
    }
    public static testMethod void testTransmitFailure(){
        JIVE_ArticleIntegration cls = new JIVE_ArticleIntegration();
        createData();
        Id faqId = [SELECT Id FROM FAQ__kav WHERE PublishStatus = 'Archived' AND Language = 'en_US' LIMIT 1].Id;
        test.setMock(HttpCalloutMock.class, new JIVE_ArticleIntegration_mockResponse(false));
        test.startTest();
        cls.transmitArticle(faqId);
        test.stopTest();
    }
    public static testMethod void testSyncArticles(){
        JIVE_ArticleIntegration cls = new JIVE_ArticleIntegration();
        cls.minutesFromNowToOpenModWindow = -5;
        cls.minutesFromNowToCloseModWindow = 2;
        createData();
        test.setMock(HttpCalloutMock.class, new JIVE_ArticleIntegration_mockResponse(true));
        test.startTest();
        cls.SyncArticles();
        test.stopTest();
    }
    
    
}