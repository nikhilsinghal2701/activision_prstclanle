@isTest
public class JIVE_ArticleIntegration_mockResponse implements HttpCalloutMock {
    public JIVE_ArticleIntegration_mockResponse(Boolean returnSuccessResponse){
        this.successResult = returnSuccessResponse;
    }
    boolean successResult;
    public HTTPResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        if(successResult == true){
            res.setStatusCode(200);
        }else{
            res.setBody('[{"artId":"errorMessage"}]');
            res.setStatusCode(400);
        }
        
        return res;
   }
}