public class JIVE_QuestionIntegration{
    static final boolean isProd = UserInfo.getOrganizationId() == '00DU0000000HMgwMAG';
    //Jive6
    //public static final string jiveEndpoint = isProd ? 'http://activision-public.hosted.jivesoftware.com/api/v2/rest/sfdc/v1/answer/' : 'http://activision-public.uat5.hosted.jivesoftware.com/api/v2/rest/sfdc/v1/answer/';
    //Jive7
    public static final string jiveEndpoint = isProd ? 'https://community.activision.com/api/core/ext/sfdc-cases/v3/answer' : 'https://community.uat.activision.com/api/core/ext/sfdc-cases/v3/answer';
    
    public JIVE_QuestionIntegration(ApexPages.StandardController cont){}
    public JIVE_QuestionIntegration(){}
    
    public Id caseId {get; set;}
    public String answerText {get; set;}
    public jiveQuestionAnswer[] answers {get; set;}
    public pageReference doQuestionAnswer(){
        if(this.caseId != null && this.answerText != null){
            if(answers == null){
                answers = new jiveQuestionAnswer[]{new jiveQuestionAnswer(caseId, answerText)};
            }
        }
        if(answers != null){
            String JSONmsg = JSON.serialize(answers);
            system.debug(JSONmsg);
            JIVE_ArticleIntegration.doTransmit(JSONmsg, jiveEndpoint);
        }
        return null;
    }
    public class jiveQuestionAnswer{
        public string id;
        public string answer;
        public string ucdid;
        public jiveQuestionAnswer(String caseId, String pAnswer){
            this.id = caseId;
            this.answer = pAnswer;
            this.ucdid = [SELECT Contact.UCDID__c FROM Case WHERE Id = :caseId].Contact.UCDID__c;
        }
    }
    
    
}