/////////////////////////////////////////////////////////////////////////////
// @Name             Linked_Accounts_Redirect_Controller_Test
// @author           Jon Albaugh
// @date created            06-SEP-2012
// @date LastModified       07-SEP-2012
// @description      Test for: Linked_Accounts_Redirect page. 
/////////////////////////////////////////////////////////////////////////////

@isTest
public with sharing class LAR_Ban_Test {

	public static testMethod void setupTest()
	{
		//Create 2 test contacts: 
		Contact testContact = new Contact();
		testContact.FirstName = 'testFirstName';
		testContact.LastName = 'testLastName';
		testContact.Email = 'testEmail@testEmail.com';
		testContact.Birthdate = date.newinstance(1960, 2, 17);
		testContact.UCDID__c = '123';
		insert testContact;
		
		Contact testContact2 = new Contact();
		testContact.FirstName = 'testFirstName2';
		testContact.LastName = 'testLastName2';
		testContact.Email = 'testEmail2@testEmail.com';
		testContact.Birthdate = date.newinstance(1960, 2, 17);
		testContact.UCDID__c = '123';
		insert testContact2;
		
		Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_26());
        test.startTest();
		//Create controller:
		ApexPages.StandardController stdController = new ApexPages.StandardController(testContact);
		Linked_Accounts_Redirect_Controller larController = new Linked_Accounts_Redirect_Controller(stdController);
		
		//Set Controller's contact to testContact:
		larController.selectedContact = testContact;
		
		//Retrieved XBL / PSN linked accounts (UCDID|123):
		pageReference pr0 = larController.updateProfile();
        
        test.stopTest();
		

	}


}