@isTest
public class LASService_Test {
    public static testMethod void runTest()
    {
        LASService testService = new LASService();
        LASService.AwardPrestigeToken_DEV_element element0 = new LASService.AwardPrestigeToken_DEV_element();
        LASService.GetLinkedAccountsResponse_element element1 = new LASService.GetLinkedAccountsResponse_element();
        //LASService.GetDoubleXP_PRODResponse_element element2 = new LASService.GetDoubleXP_PRODResponse_element();
        LASService.AwardPrestigeToken_PRODResponse_element element3 = new LASService.AwardPrestigeToken_PRODResponse_element();
        LASService.AwardDoubleXP_PRODResponse_element element4 = new LASService.AwardDoubleXP_PRODResponse_element();
        LASService.GetLinkedAccountsTestResponse_element element5 = new LASService.GetLinkedAccountsTestResponse_element();
        //LASService.GetDoubleXP_PROD_element element6 = new LASService.GetDoubleXP_PROD_element();
        //LASService.GetPrestigeToken_DEVResponse_element element7 = new LASService.GetPrestigeToken_DEVResponse_element();
        LASService.AwardPrestigeToken_PROD_element element8 = new LASService.AwardPrestigeToken_PROD_element();
        LASService.AwardDoubleXP_DEV_element element9 = new LASService.AwardDoubleXP_DEV_element();
        LASService.AwardPrestigeToken_DEVResponse_element element10 = new LASService.AwardPrestigeToken_DEVResponse_element();
        LASService.GetLinkedAccountsTest_element element11 = new LASService.GetLinkedAccountsTest_element();
        
        //LASService.GetDoubleXP_DEV_element element12 = new LASService.GetDoubleXP_DEV_element();
        LASService.GetLinkedAccounts_element element13 = new LASService.GetLinkedAccounts_element();
        //LASService.GetPrestigeToken_PRODResponse_element element14 = new LASService.GetPrestigeToken_PRODResponse_element();
        //LASService.GetDoubleXP_DEVResponse_element element15 = new LASService.GetDoubleXP_DEVResponse_element();
        //LASService.GetPrestigeToken_DEV_element element16 = new LASService.GetPrestigeToken_DEV_element();
        //LASService.GetPrestigeToken_PROD_element element17 = new LASService.GetPrestigeToken_PROD_element();
        LASService.AwardDoubleXP_DEVResponse_element element18 = new LASService.AwardDoubleXP_DEVResponse_element();
        LASService.AwardDoubleXP_PROD_element element19 = new LASService.AwardDoubleXP_PROD_element();
    
        LASService.BasicHttpBinding_ILinkedAccounts service = new LASService.BasicHttpBinding_ILinkedAccounts();    
        
        LASService.Award_CE_Entitlements_DEVResponse_element tst1 = new LASService.Award_CE_Entitlements_DEVResponse_element();
        Boolean b = tst1.Award_CE_Entitlements_DEVResult;
        LASService.Award_BK_Entitlements_DEVResponse_element tst2 = new LASService.Award_BK_Entitlements_DEVResponse_element();
        LASService.GetEliteStatusResponse_element tst3 = new LASService.GetEliteStatusResponse_element();
        LASService.Award_BK_Entitlements_DEV_element tst4 = new LASService.Award_BK_Entitlements_DEV_element();
        LASService.Award_GS_Entitlements_DEVResponse_element tst5 = new LASService.Award_GS_Entitlements_DEVResponse_element();
        LASService.CreateStatRequest_element tst6 = new LASService.CreateStatRequest_element();
        LASService.GetStatRequestsByNameResponse_element tst7a = new LASService.GetStatRequestsByNameResponse_element();
        LASService.GetEliteStatus_Test_element tst7b = new LASService.GetEliteStatus_Test_element();
        LASService.Award_BK_Entitlements_PRODResponse_element tst8 = new LASService.Award_BK_Entitlements_PRODResponse_element();
        LASService.Award_CE_Entitlements_PRODResponse_element tst9 = new LASService.Award_CE_Entitlements_PRODResponse_element();
        LASService.DecryptToken_element tst10 = new LASService.DecryptToken_element();
        LASService.Award_GS_Entitlements_PRODResponse_element tst11 = new LASService.Award_GS_Entitlements_PRODResponse_element();
        LASService.Award_GS_Entitlements_PROD_element tst12 = new LASService.Award_GS_Entitlements_PROD_element();
        LASService.GetStatRequestsByName_element tst13 = new LASService.GetStatRequestsByName_element();
        LASService.CreateStatRequestResponse_element tst14 = new LASService.CreateStatRequestResponse_element();
        LASService.GetEliteStatus_TestResponse_element tst15 = new LASService.GetEliteStatus_TestResponse_element();
        LASService.GetStatRequestByGuid_element tst16 = new LASService.GetStatRequestByGuid_element();
        LASService.GetUpdatedStatRequestsResponse_element tst17 = new LASService.GetUpdatedStatRequestsResponse_element();
        LASService.Award_CE_Entitlements_PROD_element tst18 = new LASService.Award_CE_Entitlements_PROD_element();
        LASService.DecryptTokenResponse_element tst19 = new LASService.DecryptTokenResponse_element();
        LASService.Award_CE_Entitlements_DEV_element tst20 = new LASService.Award_CE_Entitlements_DEV_element();
        LASService.GetStatRequestByGuidResponse_element tst21 = new LASService.GetStatRequestByGuidResponse_element();
        LASService.GetEliteStatus_element tst22 = new LASService.GetEliteStatus_element();
        LASService.GetUpdatedStatRequests_element tst23 = new LASService.GetUpdatedStatRequests_element();
        LASService.Award_BK_Entitlements_PROD_element tst24 = new LASService.Award_BK_Entitlements_PROD_element(); 
        LASService.Award_GS_Entitlements_DEV_element tst25 = new LASService.Award_GS_Entitlements_DEV_element(); 
        LASService.GetBansByXuid_TEST_element tst26 = new LASService.GetBansByXuid_TEST_element();
        LASService.GetBansByXuid_TESTResponse_element tst27 = new LASService.GetBansByXuid_TESTResponse_element();
        LASService.GetBansByXuid_element tst28 = new LASService.GetBansByXuid_element();
        LASService.GetBansByXuidResponse_element tst29 = new LASService.GetBansByXuidResponse_element();
        LASService.GetNewBans_BO2_TESTResponse_element tst30 = new LASService.GetNewBans_BO2_TESTResponse_element();
        LASService.GetBansByDate_element tst31 = new LASService.GetBansByDate_element();
        LASService.GetNewBans_BO2_TEST_element tst32 = new LASService.GetNewBans_BO2_TEST_element();
        LASService.GetNewBans_BO2Response_element tst33 = new LASService.GetNewBans_BO2Response_element();
        LASService.GetBansByDateResponse_element tst34 = new LASService.GetBansByDateResponse_element();
        LASService.GetBansByDate_TESTResponse_element tst35 = new LASService.GetBansByDate_TESTResponse_element();
        LASService.GetNewBans_BO2_element tst36 = new LASService.GetNewBans_BO2_element();
        LASService.GetTicket_element tst37 = new LASService.GetTicket_element();
        LASService.GetBansByDate_TEST_element tst38 = new LASService.GetBansByDate_TEST_element();
        LASService.OpenTicketResponse_element tst39 = new LASService.OpenTicketResponse_element();
        LASService.UpdateTicketResponse_element tst40 = new LASService.UpdateTicketResponse_element();
        LASService.UpdateTicketStatus_element tst41 = new LASService.UpdateTicketStatus_element();
        LASService.GetTicketResponse_element tst42 = new LASService.GetTicketResponse_element();
        LASService.OpenTicket_element tst43 = new LASService.OpenTicket_element();
        LASService.UpdateTicket_element tst44 = new LASService.UpdateTicket_element();
        LASService.UpdateTicketStatusResponse_element tst45 = new LASService.UpdateTicketStatusResponse_element();
    }
    @isTest
    private static void testCallout1(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_01());
        test.startTest();
            tstService.Award_CE_Entitlements_PROD('dwid_csv','platform','client ip','clientsession');
        test.stopTest();
    }
    @isTest
    private static void testCallout2(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_02());
        test.startTest();
            tstService.GetStatRequestsByName('name');
        test.stopTest();
    }
    @isTest
    private static void testCallout3(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_03());
        test.startTest();
            tstService.Award_GS_Entitlements_PROD('dwid_csv','platform','client ip','clientsession'); 
        test.stopTest();
    }
    @isTest
    private static void testCallout4(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();        
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_04());
        test.startTest();
            tstService.GetLinkedAccountsTest('ucdid'); 
        test.stopTest();
    }
    @isTest
    private static void testCallout5(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();        
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_05());
        test.startTest();
            tstService.AwardDoubleXP_DEV('dwid',0,'platform'); 
        test.stopTest();
    }
    @isTest
    private static void testCallout6(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_06());
        test.startTest();
            tstService.DecryptToken('token');
        test.stopTest();
    }
    /*@isTest
    private static void testCallout7(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_07());
        test.startTest();
            tstService.GetDoubleXP_DEV('dwid');
        test.stopTest();
    }*/
    @isTest
    private static void testCallout8(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_08());
        test.startTest();
            tstService.Award_BK_Entitlements_PROD('dwid_csv','platform','client ip','clientsession');
        test.stopTest();
    }
    @isTest
    private static void testCallout9(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_09());
        test.startTest();
            tstService.GetEliteStatus('dwid','platform');
        test.stopTest();
    }
    @isTest
    private static void testCallout10(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_10());
        test.startTest();
            tstService.AwardDoubleXP_PROD('dwid',0,'platform');
        test.stopTest();
    }
    /*@isTest
    public static void testCallout11(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_11());
        test.startTest();
            tstService.GetPrestigeToken_PROD('dwid','platform');
        test.stopTest();
    }*/
    @isTest
    public static void testCallout12(){ 
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();       
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_12());
        test.startTest();
            tstService.Award_GS_Entitlements_DEV('dwid_csv','platform','client ip','clientsession');
        test.stopTest();
    }
    /*@isTest
    public static void testCallout13(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_13());
        test.startTest();
            tstService.GetPrestigeToken_DEV('dwid','platform');
        test.stopTest();
    }*/
    @isTest
    public static void testCallout14(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_14());
        test.startTest();
            tstService.Award_CE_Entitlements_DEV('dwid_csv','platform','client ip','clientsession');
        test.stopTest();
    }
    @isTest
    public static void testCallout15(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_15());
        test.startTest();
            tstService.GetEliteStatus_Test('dwid','platform');
        test.stopTest();
    }
    @isTest
    public static void testCallout16(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_16());
        test.startTest();
            tstService.GetUpdatedStatRequests(0);
        test.stopTest();
    }
    /*@isTest
    public static void testCallout17(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_17());
        test.startTest();
            tstService.GetDoubleXP_PROD('dwid');
        test.stopTest();
    }*/
    @isTest
    public static void testCallout18(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_18());
        test.startTest();
            tstService.GetLinkedAccounts('ucdid');
        test.stopTest();
    }
    @isTest
    public static void testCallout19(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_19());
        test.startTest();
            tstService.AwardPrestigeToken_PROD('dwid','platform');
        test.stopTest();
    }
    @isTest
    public static void testCallout20(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_20());
        test.startTest();
            tstService.AwardPrestigeToken_DEV('dwid','platform');
        test.stopTest();
    }
    @isTest
    public static void testCallout21(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_21());
        test.startTest();
            tstService.GetStatRequestByGuid(0);
        test.stopTest();
    }
    @isTest
    public static void testCallout22(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_22());
        test.startTest();
            tstService.CreateStatRequest(
                'platform',
                'name',
                false,false,false,false,false,
                false,false,false,false,false,false,false,false,false,false,'other description',false,false,false,'comments'
            );
        test.stopTest();
    }
    @isTest
    public static void testCallout23(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_23());
        test.startTest();
            tstService.Award_BK_Entitlements_DEV('dwid_csv','platform','clientip','clientsession');  
        test.stopTest();
    }
    @isTest
    public static void testCallout24(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_24());
        test.startTest();
            tstService.GetNewBans_BO2_Test(0);
        test.stopTest();
    }
    @isTest
    public static void testCallout25(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_25());
        test.startTest();
            tstService.GetBansByXuid_test('dwid','platform');
        test.stopTest();
    }
    @isTest
    public static void testCallout26(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_26());
        test.startTest();
            tstService.GetBansByXuid('dwid','platform');
        test.stopTest();
    }
    @isTest
    public static void testCallout27(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_27());
        test.startTest();
            tstService.Getbansbydate_test(datetime.now(),datetime.now(),0);
        test.stopTest();
    }
    @isTest
    public static void testCallout28(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_28());
        test.startTest();
            tstService.Getbansbydate(datetime.now(),datetime.now(),0);
        test.stopTest();
    }
    @isTest
    public static void testCallout29(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_29());
        test.startTest();
            tstService.OpenTicket('name','comments','platform','title','tickettype','ticketstatus');
        test.stopTest();
    }
    @isTest
    public static void testCallout30(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_30());
        test.startTest();
            tstService.GetNewBans_BO2(0);
        test.stopTest();
    }
    @isTest
    public static void testCallout31(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_31());
        test.startTest();
            tstService.UpdateTicket(123,new csservicelib.Ticket());
        test.stopTest();
    }
    @isTest
    public static void testCallout32(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_32());
        test.startTest();
            tstService.GetTicket(123);
        test.stopTest();
    }
    @isTest
    public static void testCallout33(){
        LASService.BasicHttpBinding_ILinkedAccounts tstService = new LASService.BasicHttpBinding_ILinkedAccounts();
        Test.setMock(WebServiceMock.class, new LASService_test_mockCallout_33());
        test.startTest();
            tstService.UpdateTicketStatus(123,'status');
        test.stopTest();
    }
}