@isTest
global class LASService_test_mockCallout_32 implements WebServiceMock {
   global void doInvoke(
       //LASService.GetTicketResponse_element
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        
        // Create response element from the autogenerated class.
        LASService.GetTicketResponse_element responseElement = new LASService.GetTicketResponse_element();
        // Populate response element.
        //ATVI skipped
        // Add response element to the response parameter, as follows:
        response.put('response_x', responseElement); 
   }
}