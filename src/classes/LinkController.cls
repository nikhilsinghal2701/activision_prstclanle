public with sharing class LinkController {
    public string links{get;set;}
    public List<String> Masterlinks{get;set;}
    public List<Custom_Links_for_Agents__c> linksList{get;set;}
    public List<Master_Links__c> MasterlinksList{get;set;}
    public List<String> linksListDisplay{get;set;}
    public string uneditedLink;
    public string editedLink;
    public Custom_Links_for_Agents__c CusLink{get;set;}
    public LIST<RemoveLink> rmvLnk {get;set;}
    public Class RemoveLink{
        Public boolean chk{get;set;}
        Public String lnk{get;set;}
        
        Public removeLink(boolean c,String l){
            chk = c;
            lnk = l;
        }
    
    }
    public LinkController(ApexPages.StandardController controller) {
    system.debug('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
        linksListDisplay = new List<String>();
        linksList = new List<Custom_Links_for_Agents__c>();
        //uneditedLink = new List<String>();
        rmvLnk = new List<RemoveLink>();
        CusLink= new Custom_Links_for_Agents__c();
        CusLink.user__c = UserInfo.getUserId();
        system.debug('+++++++++UserInfo.getUserId()++++++++++++++'+UserInfo.getUserId());
        system.debug('+++***************rl:rmvLnk**********++++'+rmvLnk);
    }
    public void PageLoad(){
        system.debug('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
        MasterlinksList=[select id,Master_Links__c,Master_Link_Name__c from Master_Links__c where Is_Active__c=:true ];
        system.debug('+++++++++linksListAAAAAAAAAAAAAAAAAAA+++++++******************'+rmvLnk);
        linksList = [select id,user__c ,Links__c from Custom_Links_for_Agents__c where user__c =:UserInfo.getUserId()] ;
        system.debug('+++++++++linksList++++++++++++++>>>>>>>>>>>>>>'+linksList);
        if(linksList.size() > 0){
        system.debug('++++++++>>>>>>>>>>>>>'+linksList[0]+'>>>>>>>>>>>>>>>>>>'+linksList[0].Links__c+'>>>>>>>>>>>>>>>>>>'+uneditedLink );
            if(linksList[0].Links__c != null){
                uneditedLink = linksList[0].Links__c;
                system.debug('$$$$$$$$$$$$$$$$$$$$$$$$$$'+uneditedLink+'&&&&&&&&&&&&&&&&'+uneditedLink.split(';'));
                linksListDisplay= uneditedLink.split(';');
            }
            else 
            return ;
        }
        if(linksListDisplay !=null || !linksListDisplay.isEmpty()){
            for(String ln:linksListDisplay){
            system.debug('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'+uneditedLink+'&&&&&&&&&&&&&&&&'+uneditedLink.split(';'));
                rmvLnk.add(new RemoveLink(false,ln));
            }
        }
        system.debug('+++++++++linksListDisplay+++++++******************'+rmvLnk+'+++++++'+linksListDisplay);
    }
    
    public pagereference saveLink(){ 
        system.debug('***************rmvLnk**********'+rmvLnk);
        
        if(uneditedLink =='' || uneditedLink==null){
        if(!(linkslist.size() >0)){
            //String[] ss=Links.split(';',7);
            //system.debug('+++++++++Links++++++++++++++'+ss);
            CusLink.Links__c =Links ;
            system.debug('+++++++++Links++++++++++++++'+CusLink);
            insert CusLink;
            links='';
            }
        else{
            //rmvLnk.clear();
            CusLink = new Custom_Links_for_Agents__c(id=linksList[0].id);
            if(links.contains('https://')){
                Links = links.replace('https://','');
            }
            else{
            if(links.contains('http://')){
                Links = links.replace('http://','');
            }
            else
            Links = links;
            }
            system.debug('+++++++++444444+++++++++'+uneditedLink);
            system.debug('+++++++44444++rmvLnk+++++++++'+rmvLnk);
            CusLink.Links__c ='';
            if(uneditedLink != null )
            CusLink.Links__c = uneditedLink+';'+Links ;
            else
            CusLink.Links__c = Links ;
            //linksListDisplay= CusLink.Links__c;
            update CusLink;
            //linksList = [select id,user__c ,Links__c from Custom_Links_for_Agents__c where user__c =:UserInfo.getUserId()] ;
            //uneditedLink = linksList[0].Links__c;
            //linksListDisplay= uneditedLink.split(';');
            //rmvLnk.clear();
            //system.debug('%%%%%%%%**'+uneditedLink+'**%%%%%%%%%%+++'+linksListDisplay+'+++%%%%%%%%%%%%%%%%%%%'+rmvLnk);
            //for(String ln:linksListDisplay){
              //     rmvLnk.add(new RemoveLink(false,ln));
            //}
            system.debug('******************4444******************'+rmvLnk);
            links='';
            
            }
            
        }
        Else{
        linksListDisplay.clear();
        //uneditedLink='';
        rmvLnk.clear();
        CusLink = new Custom_Links_for_Agents__c(id=linksList[0].id);
        if(links.contains('https://')){
            Links = links.replace('https://','');
        }
        else{
        if(links.contains('http://')){
            Links = links.replace('http://','');
        }
        else
        Links = links;
        }
        system.debug('++++++++++++++++++'+uneditedLink);
        system.debug('+++++++++rmvLnk+++++++++'+rmvLnk);
        CusLink.Links__c ='';
        CusLink.Links__c = uneditedLink+';'+Links ;
        
        //linksListDisplay= CusLink.Links__c;
        update CusLink;
        //linksList = [select id,user__c ,Links__c from Custom_Links_for_Agents__c where user__c =:UserInfo.getUserId()] ;
        //uneditedLink = linksList[0].Links__c;
        //linksListDisplay= uneditedLink.split(';');
        //rmvLnk.clear();
        //system.debug('%%%%%%%%**'+uneditedLink+'**%%%%%%%%%%+++'+linksListDisplay+'+++%%%%%%%%%%%%%%%%%%%'+rmvLnk);
        //for(String ln:linksListDisplay){
          //     rmvLnk.add(new RemoveLink(false,ln));
        //}
        system.debug('************************************'+rmvLnk);
        links='';
        }
       //pagereference p = new pagereference('/apex/customLinks');
       //p.setRedirect(true);
       system.debug('***************>>>>>>>>>>>>>'+rmvLnk);
       return page.CustomLinks;
      
    }
    public pagereference removeLink2(){
    List<String> STrngLst = new List<String>();
        system.debug('+++rl:rmvLnk++++'+rmvLnk);
        for(RemoveLink rl:rmvLnk){
            if(rl.chk != true){
                system.debug('============1========================>>>>'+rl.lnk);
                STrngLst.add(rl.lnk);
            }
        }
        system.debug('======2====='+rmvLnk+'=============='+STrngLst);
        linksListDisplay.clear();
        uneditedLink=null;
        rmvLnk.clear();
        CusLink = new Custom_Links_for_Agents__c(id=linksList[0].id);
        CusLink.Links__c ='';
        
        for(string st:StrngLst){
        system.debug('=====3======='+CusLink.Links__c+'====='+st);
        if(CusLink.Links__c !=''){
         system.debug('=====3=--1=====');
        CusLink.Links__c = CusLink.Links__c+';'+st ;   
        }
        Else{ 
        CusLink.Links__c =st;
        }
        }
        system.debug('===***********>>>>---2--->>>>>>>>>>>>>>>'+rmvLnk);
        update CusLink;
        system.debug('===****---1---*******>>>>>>>>>>>>>>>>>>>');
        return page.CustomLinks;
    }

}