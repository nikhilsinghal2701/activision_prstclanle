/////////////////////////////////////////////////////////////////////////////
// @Name             Linked_Accounts_Redirect_Controller 
// @author           Jon Albaugh
// @date created            07-AUG-2012
// @date LastModified       07-SEP-2012
// @description      Controller for: Linked_Accounts_Redirect page. 
//                     This controller will make the web service callout, 
//                     then redirect to the home page.
/////////////////////////////////////////////////////////////////////////////

public class Linked_Accounts_Redirect_Controller {
    
    public Contact selectedContact { get; set; }

    //Constructor: Set the selected contact.
    public Linked_Accounts_Redirect_Controller(ApexPages.StandardController controller){
        selectedContact = (Contact)controller.getRecord();
        }
        
    //This method is called from the Action header on Linked_Accounts_Redirect Page:
    public PageReference updateProfile()
    {   /*
        boolean otherXBLGamertagFound = false;
        boolean otherPSNGamertagFound = false;
        
        List<Multiplayer_Account__c> contactXboxGamertags = new List<Multiplayer_Account__c>();
        List<Multiplayer_Account__c> contactPS3Gamertags = new List<Multiplayer_Account__c>();
        List<Multiplayer_Account__c> contactInactiveGamertags = new List<Multiplayer_Account__c>();
        
        //Retrieve all current contact Gamertags and Sort:  
        for(Multiplayer_Account__c mpAcct : [SELECT id, Contact__c, DWID__c, Gamertag__c, Is_Current__c,Platform__c FROM Multiplayer_Account__c WHERE Contact__c = :selectedContact.id])
        {
            if(mpAcct.Platform__c == 'XBL')
            {
                if(mpAcct.Is_Current__c == True)
                {
                    contactXboxGamertags.add(mpAcct);
                }
                else
                {
                    contactInactiveGamertags.add(mpAcct);
                }
            }
            else if(mpAcct.Platform__c == 'PSN')
            {
                if(mpAcct.Is_Current__c == True)
                {
                    contactPS3Gamertags.add(mpAcct);
                }
                else
                {
                    contactInactiveGamertags.add(mpAcct);
                }
            }
        }
               
        //Create a List for gamertags to be inserted / updated:
        List<Multiplayer_Account__c> gamertagsToUpsert = new List<Multiplayer_Account__c>();
        
        //Retrieve the UCDID from the selected Contact:
        Contact currentContact = [SELECT id, UCDID__c FROM Contact WHERE id = :selectedContact.id];
        
        //Check to see if Current Contact has an associated UCDID (If the contact doesn't have a UCDID, Return)
        if (currentContact.UCDID__c != null && currentContact.UCDID__c != '')
        {   
            LASResponse.LinkedAccountsResponse response = new LASResponse.LinkedAccountsResponse();
            if(!System.Test.isRunningTest())
            {
                //Call the Linked Accounts Service:
                LASService.BasicHttpBinding_ILinkedAccounts service = new LASService.BasicHttpBinding_ILinkedAccounts();
                //service.timeout_x = 3000;
                try
                {
                    response = service.GetLinkedAccounts(currentContact.UCDID__c);
                }
                catch(Exception ex)
                {
                    response.token = ex.getMessage();
                    system.debug(ex.getMessage());
                    Pagereference redirectURL = new PageReference('/' + currentContact.id + '?nooverride=1&error=True');
                    return redirectURL;
                }   
            }
            else
            {
                if (currentContact.UCDID__c == '123')
                {
                    response.linkedAccounts = new LASResponse.LinkedAccounts();
                    response.linkedAccounts.psn = new LASResponse.psn();
                    response.linkedAccounts.psn.username = 'testPSNAcct';
                    response.linkedAccounts.psn.dwid = '0123456789';
                    response.linkedAccounts.xbl = new LASResponse.xbl();
                    response.linkedAccounts.xbl.username = 'testXBLAcct';
                    response.linkedAccounts.xbl.xuid = '0987654321';
                }
                else if (currentContact.UCDID__c == '456')
                {
                    response.linkedAccounts = new LASResponse.LinkedAccounts();
                    response.linkedAccounts.psn = new LASResponse.psn();
                    response.linkedAccounts.psn.username = 'testPSNAcct';
                    response.linkedAccounts.psn.dwid = '0123456789';
                }
                
                else if (currentContact.UCDID__c == '789')
                {
                    response.linkedAccounts = new LASResponse.LinkedAccounts();
                    response.linkedAccounts.xbl = new LASResponse.xbl();
                    response.linkedAccounts.xbl.username = 'testXBLAcct';
                    response.linkedAccounts.xbl.xuid = '0987654321';
                }
                else if (currentContact.UCDID__c == 'abc')
                {
                    response.linkedAccounts = new LASResponse.LinkedAccounts();
                    response.linkedAccounts.psn = new LASResponse.psn();
                    response.linkedAccounts.psn.username = 'testPSNAcct3';
                    response.linkedAccounts.psn.dwid = '01234567893';
                    response.linkedAccounts.xbl = new LASResponse.xbl();
                    response.linkedAccounts.xbl.username = 'testXBLAcct3';
                    response.linkedAccounts.xbl.xuid = '09876543213';
                }
            }
            
            //Check to see if contact has linked accounts:
            if (response.LinkedAccounts != null)
            {    
                //Check to see if contact has a linked Xbox Account:
                if (response.LinkedAccounts.xbl != null)
                {
                    //Check to see if Linked Xbox Account has a gamertag value:
                    if (response.LinkedAccounts.xbl.username != null)
                    {
                        //Search all Xbox gamertags for a duplicate current gamertag
                        List<Multiplayer_Account__c> otherUserXboxGamertags = [SELECT id, Gamertag__c, DWID__c, Platform__c, Is_Current__c, Contact__c FROM Multiplayer_Account__c WHERE Contact__c != :selectedContact.id AND Platform__c = 'XBL' AND (Gamertag__c = :response.LinkedAccounts.xbl.username OR DWID__c = :response.LinkedAccounts.xbl.xuid)];
                        
                        //If a duplicate gamertag is found:
                        if (otherUserXboxGamertags.size() > 0)
                        {
                            otherXBLGamertagFound = true;
                            for(Multiplayer_Account__c otherXboxGamertag : otherUserXboxGamertags)
                            {
                                //Check for new UCD Account:
                                if (otherXboxGamertag.DWID__c == response.LinkedAccounts.xbl.xuid)
                                {
                                    //Check for Name Change and UCD Change:
                                    if(otherXboxGamertag.Gamertag__c != response.LinkedAccounts.xbl.username)
                                    {
                                        otherXboxGamertag.Gamertag__c = response.LinkedAccounts.xbl.username;
                                    }
                                    //Update the gamertag with the new contact id:
                                    otherXboxGamertag.Contact__c = selectedContact.id;
                                    otherXboxGamertag.Is_Current__c = true;
                                    GamertagsToUpsert.add(otherXboxGamertag);
                                
                                }
                                else
                                {
                                    //Otherwise A name Change Occurred, set the duplicate to not-current:
                                    otherXboxGamertag.Is_Current__c = False;
                                    GamertagsToUpsert.add(otherXboxGamertag);
                                }
                            }
                        }
                        
                        //If the user has a current Xbox Gamertag:
                        if (contactXboxGamertags.size() > 0)
                        {
                            for (Multiplayer_Account__c tmpGamertag : contactXboxGamertags)
                            {   
                                 //If the returned DWID matches the current DWID, do nothing:
                                 if (tmpGamertag.DWID__c == response.LinkedAccounts.xbl.xuid)
                                 {
                                     //Check for Name Change
                                     if(tmpGamertag.Gamertag__c != response.LinkedAccounts.xbl.username)
                                     {
                                        //A name change has occured:
                                        tmpGamertag.Gamertag__c = response.LinkedAccounts.xbl.username;
                                        gamertagsToUpsert.add(tmpGamertag);
                                     }
                                     
                                     //Update to check for change in ELITE Status:
                                     gamertagsToUpsert.add(tmpGamertag);
                                 }
                                 else
                                 {
                                     //Mark previous Gamertag as not current:
                                     tmpGamertag.Is_Current__c = false;
                                     gamertagsToUpsert.add(tmpGamertag);
                                     
                                     boolean accountInactiveFound = false;
                                     for(Multiplayer_Account__c mpAccount : contactInactiveGamertags)
                                     {
                                        if (mpAccount.DWID__c == response.LinkedAccounts.xbl.xuid)
                                        {   
                                            mpAccount.Is_Current__c = true;
                                            gamertagsToUpsert.add(mpAccount);
                                            accountInactiveFound = true;
                                        }
                                     }
                                     
                                     if(!accountInactiveFound && !otherXBLGamertagFound)
                                     {
                                         //Create new Gamertag Object:
                                         Multiplayer_Account__c newGamertag = new Multiplayer_Account__c();
                                         newGamertag.Contact__c = currentContact.id;
                                         newGamertag.Platform__c = 'XBL';
                                         newGamertag.Gamertag__c = response.LinkedAccounts.xbl.username;
                                         newGamertag.DWID__c = response.LinkedAccounts.xbl.xuid;
                                         newGamertag.Is_Current__c = true;
                                         
                                         gamertagsToUpsert.add(newGamertag);
                                     }
                                 } 
                            }
                        }
                        //If the user doesn't currently have an acitve Xbox Gamertag:
                        else
                        {
                            boolean accountInactiveFound = false;
                             for(Multiplayer_Account__c mpAccount : contactInactiveGamertags)
                             {
                                if (mpAccount.DWID__c == response.LinkedAccounts.xbl.xuid)
                                {   
                                    mpAccount.Is_Current__c = true;
                                    gamertagsToUpsert.add(mpAccount);
                                    accountInactiveFound = true;
                                }
                             }
                             
                             if(!accountInactiveFound && !otherXBLGamertagFound)
                             {
                                 //Create new Gamertag Object:
                                 Multiplayer_Account__c newGamertag = new Multiplayer_Account__c();
                                 newGamertag.Contact__c = currentContact.id;
                                 newGamertag.Platform__c = 'XBL';
                                 newGamertag.Gamertag__c = response.LinkedAccounts.xbl.username;
                                 newGamertag.DWID__c = response.LinkedAccounts.xbl.xuid;
                                 newGamertag.Is_Current__c = true;
                                 
                                 gamertagsToUpsert.add(newGamertag);  
                             }
                        }
                    }
                }
                //If server returns no linked Xbox gamertags:
                else
                {
                    //Set all contact's Xbox Gamertags to not current:
                    for (Multiplayer_Account__c tmpGamertag : contactXboxGamertags)
                    {
                        tmpGamertag.Is_Current__c = false;
                        gamertagsToUpsert.add(tmpGamertag);
                    }
                }
                
                //Check to see if contact has any currently linked PSN accounts:
                if (response.LinkedAccounts.psn != null)
                {
                    //Check to see if the linked PSN account has a gamertag value:
                    if (response.LinkedAccounts.psn.username != null)
                    {
                        //Search all PS3 gamertags for a duplicate current gamertag
                        List<Multiplayer_Account__c> otherUserPS3Gamertags = [SELECT id, Gamertag__c, DWID__c, Platform__c, Is_Current__c, Contact__c FROM Multiplayer_Account__c WHERE Contact__c != :selectedContact.id AND Platform__c = 'PSN' AND Gamertag__c = :response.LinkedAccounts.psn.username];
                        
                        //If a duplicate gamertag is found:
                        if (otherUserPS3Gamertags.size() > 0)
                        {
                            otherPSNGamertagFound = true;
                            for(Multiplayer_Account__c otherPS3Gamertag : otherUserPS3Gamertags)
                            {
                                //Check for new UCD Account:
                                if (otherPS3Gamertag.DWID__c == response.LinkedAccounts.psn.dwid)
                                {
                                    //Update the gamertag with the new contact id:
                                    otherPS3Gamertag.Contact__c = selectedContact.id;
                                    otherPS3Gamertag.Is_Current__c = true;
                                    GamertagsToUpsert.add(otherPS3Gamertag);
                                }
                                else
                                {
                                    //Otherwise A name Change Occurred, set the duplicate to not-current:
                                    otherPS3Gamertag.Is_Current__c = False;
                                    GamertagsToUpsert.add(otherPS3Gamertag);
                                }
                            }
                        }
                        
                        //If the user has a current PS3 Gamertag:    
                        if (contactPS3Gamertags.size() > 0)
                        {
                            for (Multiplayer_Account__c tmpGamertag : contactPS3Gamertags)
                            {
                                //If the returned gamertag matches the current gamertag, do nothing:
                                 if (tmpGamertag.Gamertag__c == response.LinkedAccounts.psn.username)
                                 {
                                    //Added to check for ELITE Status update;
                                     gamertagsToUpsert.add(tmpGamertag);
                                 }
                                 else
                                 {
                                     //Mark previous Gamertag as not current:
                                     tmpGamertag.Is_Current__c = false;
                                     gamertagsToUpsert.add(tmpGamertag);
                                     
                                     boolean accountInactiveFound = false;
                                     for(Multiplayer_Account__c mpAccount : contactInactiveGamertags)
                                     {
                                        if (mpAccount.DWID__c == response.LinkedAccounts.psn.dwid)
                                        {   
                                            mpAccount.Is_Current__c = true;
                                            gamertagsToUpsert.add(mpAccount);
                                            accountInactiveFound = true;
                                        }
                                     }
                                     
                                     if(!accountInactiveFound && !otherPSNGamertagFound)
                                     {
                                         //Create new Gamertag Object:
                                         Multiplayer_Account__c newGamertag = new Multiplayer_Account__c();
                                         newGamertag.Contact__c = currentContact.id;
                                         newGamertag.Platform__c = 'PSN';
                                         newGamertag.Gamertag__c = response.LinkedAccounts.psn.username;
                                         newGamertag.DWID__c = response.LinkedAccounts.psn.dwid;
                                         newGamertag.Is_Current__c = true;
                                         
                                         gamertagsToUpsert.add(newGamertag);
                                     }
                                 } 
                            }
                        }
                        //If the user doesn't currently have an acitve PS3 Gamertag:
                        else
                        {
                             boolean accountInactiveFound = false;
                             for(Multiplayer_Account__c mpAccount : contactInactiveGamertags)
                             {
                                if (mpAccount.DWID__c == response.LinkedAccounts.psn.dwid)
                                {   
                                    mpAccount.Is_Current__c = true;
                                    gamertagsToUpsert.add(mpAccount);
                                    accountInactiveFound = true;
                                }
                             }
                            
                            if(!accountInactiveFound && !otherPSNGamertagFound)
                            {
                                 //Create new Gamertag Object:
                                 Multiplayer_Account__c newGamertag = new Multiplayer_Account__c();
                                 newGamertag.Contact__c = currentContact.id;
                                 newGamertag.Platform__c = 'PSN';
                                 newGamertag.Gamertag__c = response.LinkedAccounts.psn.username;
                                 newGamertag.DWID__c = response.LinkedAccounts.psn.dwid;
                                 newGamertag.Is_Current__c = true;
                            
                                 gamertagsToUpsert.add(newGamertag);
                             }
                        }
                    }
                }
                //If server returns no linked PS3 gamertags:
                else
                {
                    //Set all contact's PS3 Gamertags to not current:
                    for (Multiplayer_Account__c tmpGamertag : contactPS3Gamertags)
                    {
                        tmpGamertag.Is_Current__c = false;
                        gamertagsToUpsert.add(tmpGamertag);
                    }
                }
            }
            //If Server returns no linked accounts, Set all contact's Xbox and Gamertags to not current:
            else
            {
                for (Multiplayer_Account__c tmpGamertag : contactPS3Gamertags)
                {
                    tmpGamertag.Is_Current__c = false;
                    gamertagsToUpsert.add(tmpGamertag);
                }
                for (Multiplayer_Account__c tmpGamertag : contactXboxGamertags)
                {
                    tmpGamertag.Is_Current__c = false;
                    gamertagsToUpsert.add(tmpGamertag);
                }
            }
            
            //Update any modified gamertags:     
            if (gamertagsToUpsert.size() > 0)
            {
                //Retrieve all ELITE Info for upserted gamertags:
                
                //Set up service:
                LASService.BasicHttpBinding_ILinkedAccounts eliteService = new LASService.BasicHttpBinding_ILinkedAccounts();
                eliteService.timeout_x = 1000;
                for(Multiplayer_Account__c tmpGamertag : gamertagsToUpsert)
                {
                    //Create temp ELITE response object:
                    LASResponse.EliteStatusResponse eliteResponse = new LASResponse.EliteStatusResponse();
                    
                    try
                    {
                        string CorrectedPlatform = '';
                        if(tmpGamertag.Platform__c == 'XBL')
                        {
                            CorrectedPlatform = 'xbox'; 
                        }
                        else if (tmpGamertag.Platform__c == 'PSN')
                        {
                            CorrectedPlatform = 'ps3';  
                        }
                        
                        if(CorrectedPlatform != '')
                        {
                            if(!System.Test.isRunningTest())
                            {
                                eliteResponse = eliteService.GetEliteStatus(tmpGamertag.DWID__c, CorrectedPlatform);
                                tmpGamertag.Is_Founder__c = eliteResponse.isFounder;
                                tmpGamertag.Is_Premium__c = eliteResponse.isPremium;
                                
                                system.debug(eliteResponse.errorMessage);
                                
                                Date MW3LaunchDate = date.parse('11/8/2010');
                                if(eliteResponse.premiumStartDate > MW3LaunchDate)
                                {
                                    tmpGamertag.Premium_Start__c = Date.valueOf(eliteResponse.premiumStartDate);
                                    tmpGamertag.Premium_End__c = Date.valueOf(eliteResponse.premiumEndDate);
                                }
                                else
                                {
                                    tmpGamertag.Premium_Start__c = null;
                                    tmpGamertag.Premium_End__c = null;
                                }
                                
                            }
                            else
                            {
                                tmpGamertag.Is_Founder__c = true;
                                tmpGamertag.Is_Premium__c = true;
                                tmpGamertag.Premium_Start__c = date.parse('11/8/2011');
                                tmpGamertag.Premium_End__c = date.parse('11/8/2011');
                            }
                        }
                        else
                        {
                            Pagereference redirectURL = new PageReference('/' + currentContact.id + '?nooverride=1&error=E_Plat');
                            return redirectURL;
                        }
                        
                    }
                    catch(Exception ex)
                    {
                        Pagereference redirectURL = new PageReference('/' + currentContact.id + '?nooverride=1&error=E_Error');
                        return redirectURL;
                    }
                }
                

                
                try
                {
                    list<string> xuidList = new list<string>();
                    for(Multiplayer_Account__c mp: gamertagsToUpsert){
                        xuidList.add(mp.DWID__c);
                    }
                    List<ban__c> banlist = new List<ban__c>();
                    List<ban__c> dirtyBans = [select id from ban__c where xuid__c in :xuidList];
                    System.Debug(gamertagsToUpsert);
                    //gamertagsToUpsert[0].DWID__c='744306';
                    for(Multiplayer_Account__c mp:gamertagsToUpsert){
                        system.debug('Gamertag Focus:' + mp.Gamertag__c);
                        
                        string console;

                        if(mp.Platform__c == 'XBL'){
                            console = 'XBOX_360';
                        }
                        else if(mp.Platform__c == 'PSN'){
                            console = 'PLAYSTATION_3';
                        }
                        system.debug('Making the Callout..');
                        LASService.BasicHttpBinding_ILinkedAccounts worker = new LASService.BasicHttpBinding_ILinkedAccounts();
                        worker.timeout_x = 120000; //update from 1000
                        system.debug('Calling ('+mp.DWID__c+','+console+')');
                        CSServiceLib.ArrayOfBan results = new CSServiceLib.ArrayOfBan();
                        try{
                            results = worker.GetBansByXuid(mp.DWID__c, console);
                        }catch(Exception e){
                            system.debug(e);
                        }
                        if(results.ban!=null){
                            //ban__c temp;
                            for(CSServiceLib.ban ban: results.ban){
                                ban__c temp = new ban__c();
                                system.debug('I am composing the ban');
                                temp.Category__c = ban.m_category;
                                temp.Start_Date__c = ban.m_startDate;
                                temp.End_Date__c = ban.m_endDate;
                                temp.Guid__c = ban.m_guid;
                                temp.Platform__c = ban.m_platform;
                                temp.Reason__c = ban.m_reason;
                                temp.Title__c = ban.m_title;
                                temp.type__c = ban.m_type;
                                temp.XUID__c = string.valueof(ban.m_xuid);
                                temp.Multiplayer_Account__r = new Multiplayer_Account__c(DWID__c=mp.DWID__c);
                                system.debug('This is the DemonWare ID:'+mp.DWID__c);
                                banlist.add(temp);
                            }
                        }
                        system.debug('I am in the outer for SCOPE');
                    }
                    
                    system.debug('I am about to Upsert the GAMERTAGS...');
                    upsert gamertagsToUpsert;
                    system.debug('Cleaning Bans...');
                    delete dirtyBans;
                    system.debug('GamertagstoUpsert UPSERTING....');
                    upsert banlist;
                    system.debug('Banlist UPSERTING....');
                }
                catch(Exception ex)
                {
                    Pagereference redirectURL = new PageReference('/' + currentContact.id + '?nooverride=1&error=DML');
                    return redirectURL;
                }
            }       
            
        }
        else
        {
            for (Multiplayer_Account__c tmpGamertag : contactPS3Gamertags)
            {
                tmpGamertag.Is_Current__c = false;
                gamertagsToUpsert.add(tmpGamertag);
            }
            for (Multiplayer_Account__c tmpGamertag : contactXboxGamertags)
            {
                tmpGamertag.Is_Current__c = false;
                gamertagsToUpsert.add(tmpGamertag);
            }
            
            //Update any modified gamertags:     
            if (gamertagsToUpsert.size() > 0)
            {
                upsert gamertagsToUpsert;
            }
        }
*/
        /*if(gamertagsToUpsert.size()>0){
            map<string,string> mpMap = new map<string,string>();
            for(Multiplayer_Account__c mp:gamertagsToUpsert){
                mpMap.put(mp.id,mp.DWID__c);
            }
            BanCallout(mpMap);
        }*/
        
        
        
        /*list<ban__c> BansToUpsert = new list<ban__c>();
        for(Multiplayer_Account__c mp: gamertagsToUpsert){
            system.debug('Im here***************');
            list<ban__c> banlist = [select id from ban__c where xuid__c=:mp.DWID__c];
            system.debug('********'+banlist);
            if(banlist.size()>0){
                system.debug('In the IF Statement******');
                for(ban__c biter:banlist){
                    system.debug('In the 2nd For Loop*****');
                    biter.Multiplayer_Account__c = mp.id;
                    BansToUpsert.add(biter);
                }
                upsert BansToUpsert;
            }
        }*/
        
        //Create a redirect url with '?nooverride=1' to disable an infinite loop:
        Pagereference redirectURL = new PageReference('/' + selectedContact.id + '?nooverride=1');
        return redirectURL;
    }

    /*@future (callout=true)
    public static void BanCallout(Map<string,string> gamertagsToUpsert){
        for(string str:gamertagsToUpsert){

        }*/
        /*for(Multiplayer_Account__c mp: gamertagsToUpsert){
            
            string console;

            if(mp.Platform__c == 'XBL'){
                console = 'XBOX_360';
            }
            else if(mp.Platform__c == 'PSN'){
                console = 'PLAYSTATION_3';
            }
            LASService.BasicHttpBinding_ILinkedAccounts worker = new LASService.BasicHttpBinding_ILinkedAccounts();
            worker.timeout_x = 1000;
            CSServiceLib.ArrayOfBan results = worker.GetBansByXuid_Test(mp.DWID__c, console);
            if(results!=null){
                ban__c temp;
                temp.Category__c = results.ban[0].m_category;
                temp.Start_Date__c = results.ban[0].m_startDate;
                temp.End_Date__c = results.ban[0].m_endDate;
                temp.Guid__c = results.ban[0].m_guid;
                temp.Platform__c = results.ban[0].m_platform;
                temp.Reason__c = results.ban[0].m_reason;
                temp.Title__c = results.ban[0].m_title;
                temp.type__c = results.ban[0].m_type;
                temp.XUID__c = string.valueof(results.ban[0].m_xuid);
                temp.Multiplayer_Account__c = mp.id;
                BansToUpsert.add(temp);
            }

        }
        if(BansToUpsert.size()>0){
            upsert BansToUpsert;
        }*/
    //}
}