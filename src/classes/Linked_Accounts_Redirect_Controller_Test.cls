/////////////////////////////////////////////////////////////////////////////
// @Name             Linked_Accounts_Redirect_Controller_Test
// @author           Jon Albaugh
// @date created            06-SEP-2012
// @date LastModified       07-SEP-2012
// @description      Test for: Linked_Accounts_Redirect page. 
/////////////////////////////////////////////////////////////////////////////

@isTest
public with sharing class Linked_Accounts_Redirect_Controller_Test {

	public static testMethod void setupTest()
	{
		//Create 2 test contacts: 
		Contact testContact = new Contact();
		testContact.FirstName = 'testFirstName';
		testContact.LastName = 'testLastName';
		testContact.Email = 'testEmail@testEmail.com';
		testContact.Birthdate = date.newinstance(1960, 2, 17);
		testContact.UCDID__c = '123';
		insert testContact;
		
		Contact testContact2 = new Contact();
		testContact.FirstName = 'testFirstName2';
		testContact.LastName = 'testLastName2';
		testContact.Email = 'testEmail2@testEmail.com';
		testContact.Birthdate = date.newinstance(1960, 2, 17);
		testContact.UCDID__c = '123';
		insert testContact2;
		
		//Create controller:
		ApexPages.StandardController stdController = new ApexPages.StandardController(testContact);
		Linked_Accounts_Redirect_Controller larController = new Linked_Accounts_Redirect_Controller(stdController);
		
		//Set Controller's contact to testContact:
		larController.selectedContact = testContact;
		
		//Retrieved XBL / PSN linked accounts (UCDID|123):
		pageReference pr0 = larController.updateProfile();
		
		//Duplicate same request (To catch no change):
		pageReference prx = larController.updateProfile();
		
		//Set LAS to return two new Linked Accounts (Setting prevoius to inactive):
		testContact.UCDID__c = 'abc';
		update testContact;
		pageReference pr1x = larController.updateProfile();
		
		//Set LAS Call to return only PSN Account (Setting  XBL to inactive):
		testContact.UCDID__c = '456';
		update testContact;
		pageReference pr1 = larController.updateProfile();
		
		//Set LAS to return two new Linked Accounts (Setting prevoius to inactive):
		testContact.UCDID__c = 'abc';
		update testContact;
		pageReference pr1x2 = larController.updateProfile();
		
		//Set LAS Call to return only XBL Account (Setting PSN to inactive):
		testContact.UCDID__c = '789';
		update testContact;
		pageReference pr2 = larController.updateProfile();
		
		//Reconfigure to set all accounts back to active:
		testContact.UCDID__c = '123';
		update testContact;
		pageReference prmx = larController.updateProfile();
		
		//Set LAS Call to return no linked accounts (Setting XBL / PSN to inactive):
		testContact.UCDID__c = 'xxx';
		update testContact;
		pageReference pr3 = larController.updateProfile();
		
		//Reconfigure to set all accounts back to active:
		testContact.UCDID__c = '123';
		update testContact;
		pageReference prmx2 = larController.updateProfile();
		
		//Set LAS Call to return no linked accounts (Via no UCDID) (Setting XBL / PSN to inactive):
		testContact.UCDID__c = '';
		update testContact;
		pageReference pr4 = larController.updateProfile();
		
		//Set LAS inactive accounts back to actie:
		testContact.UCDID__c = '123';
		update testContact;
		pageReference pr0x = larController.updateProfile();
		
		//Replicate a UCD Change (Set the original Multiplayer Accounts to a different Contact ID):
		for(Multiplayer_Account__c tmpAcct : [SELECT id, Contact__c FROM Multiplayer_Account__c WHERE Contact__r.id = :testContact.id])
		{
			tmpAcct.Contact__c = testContact2.id;
			update tmpAcct;
		}
		
		testContact.UCDID__c = '123';
		update testContact;
		pageReference pr5 = larController.updateProfile();
		
		//Replicate a name change (Name associated with a different DWID).
		Multiplayer_Account__c mpAcct_psn = new Multiplayer_Account__c();
		mpAcct_psn.Contact__c = testContact2.id;
		mpAcct_psn.DWID__c = '0123456789x';
		mpAcct_psn.Gamertag__c = 'testPSNAcct'; 
		mpAcct_psn.Is_Current__c = true;
		mpAcct_psn.Platform__c = 'PSN';
		insert mpAcct_psn;
		
		Multiplayer_Account__c mpAcct_xbl = new Multiplayer_Account__c();
		mpAcct_xbl.Contact__c = testContact2.id;
		mpAcct_xbl.DWID__c = '0987654321x';
		mpAcct_xbl.Gamertag__c = 'testXBLAcct';
		mpAcct_xbl.Is_Current__c = true;
		mpAcct_xbl.Platform__c = 'XBL';
		insert mpAcct_xbl;
		

            pageReference pr6 = larController.updateProfile();
		
			//Submit a duplicate request:
			pageReference pr7 = larController.updateProfile();
		

	}


}