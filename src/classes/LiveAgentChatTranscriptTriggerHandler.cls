public class LiveAgentChatTranscriptTriggerHandler 
{
    public static void onBeforeInsert(List<LiveChatTranscript> newTranscripts)
    {
        set<Id> caseIds = new set<Id>();
        Map<Id,LiveChatTranscript> transcriptsToTransfer = new Map<Id,LiveChatTranscript>();
        
        for(LiveChatTranscript transcript : newTranscripts)
        {
            if(transcript.Status == 'Missed' && transcript.CaseId != null)
            {
                caseIds.add(transcript.CaseId);
            }
            if(transcript.CaseId != null)
            {
                transcriptsToTransfer.put(transcript.CaseId,transcript);
            }
        }
        
        if(caseIds.size() > 0)
        {
            autoCloseCases(caseIds);
        }
        
        if(transcriptsToTransfer.size() > 0)
        {
            transferTranscriptsToMergedCases(transcriptsToTransfer);
        }
    }
    
    /*
    public static void onBeforeUpdate(Map<Id,LiveChatTranscript> oldTranscriptsMap, Map<Id,LiveChatTranscript> newTranscriptsMap)
    {
        set<Id> caseIds = new set<Id>();
        
        for(LiveChatTranscript transcript : newTranscriptsMap.values())
        {
            if(transcript.Status != oldTranscriptsMap.get(transcript.Id).Status 
             && transcript.Status == 'Missed' && transcript.CaseId != null)
            {
                caseIds.add(transcript.CaseId);
            }
        }
        
        if(caseIds.size() > 0)
        {
            autoCloseCases(caseIds);
        }
    }
    */
    
    public static void autoCloseCases(set<Id> caseIds)
    {
        List<Case> cases = new List<Case>();
        
        for(Case c : [Select Status from Case where Id in :caseIds and Status != 'Chat - Abandoned'])
        {
            c.Status = 'Chat - Abandoned';
            c.Comment_Not_Required__c = true;
            cases.add(c);
        }
        
        if(cases.size() > 0)
        {
            update cases;
        }
    }
    
    public static void transferTranscriptsToMergedCases(Map<Id,LiveChatTranscript> transcriptsToTransfer)
    {
        for(Case c : [Select Id, Status, Merge_To_Case__c from Case where Id in :transcriptsToTransfer.keyset() and Status = 'Merge - Duplicate' and Merge_To_Case__c != :null])
        {
            transcriptsToTransfer.get(c.Id).CaseId = c.Merge_To_Case__c;
        }
    }
}