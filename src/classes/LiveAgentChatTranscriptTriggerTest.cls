@isTest
public class LiveAgentChatTranscriptTriggerTest
{
    public static testMethod void TransferTranscripts()
    {
        Account acc = LiveAgentCustomizationsTestUtil.createAccount('Test Class Account');
        Contact con = LiveAgentCustomizationsTestUtil.createContact('TestClass', 'Contact', 'testClass@Contact.com', acc.Id);
        List<Case> cases = LiveAgentCustomizationsTestUtil.createCases(2, acc.Id, con.Id, 'Open');
        cases[1].Merge_To_Case__c = cases[0].Id;
        cases[1].Status = 'Merge - Duplicate';
        cases[1].Comment_Not_Required__c = true;
        update cases[1];
        
        test.startTest();
        
        LiveChatTranscript transcript = LiveAgentCustomizationsTestUtil.createChatTranscript(con.Id, cases[1].Id, 'Completed');
        
        system.assertEquals(cases[0].Id, [Select CaseId from LiveChatTranscript where Id = :transcript.Id].CaseId, 'Chat Transcript should merged to Parent Case');
        
        test.stopTest();
    }
    
    public static testMethod void AutoCloseCasesOnInsert()
    {
        Account acc = LiveAgentCustomizationsTestUtil.createAccount('Test Class Account');
        Contact con = LiveAgentCustomizationsTestUtil.createContact('TestClass', 'Contact', 'testClass@Contact.com', acc.Id);
        List<Case> cases = LiveAgentCustomizationsTestUtil.createCases(1, acc.Id, con.Id, 'Open');
        
        test.startTest();
        
        LiveChatTranscript transcript = LiveAgentCustomizationsTestUtil.createChatTranscript(con.Id, cases[0].Id, 'Missed');
        
        system.assertEquals('Chat - Abandoned', [Select Status from Case where Id = :Cases[0].Id].Status, 'Case should be Chat Abandoned for Missed Chats');
        
        test.stopTest();
    }
}