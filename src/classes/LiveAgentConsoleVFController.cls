public with sharing class LiveAgentConsoleVFController 
{
    private string contactId;
    private string caseId;
    
    public Contact contact {get;set;}
    public Case autoCreatedCase {get;set;}
    public Case workThisCase {get;set;}
    public List<wrapper> openCases {get;set;}
    
    public Boolean showContact {get;set;}
    public Boolean showOpenCases {get;set;}
    public Boolean selectedCaseToWork {get;set;}
    
    public LiveAgentConsoleVFController()
    {
        contact = new Contact();
        autoCreatedCase = new Case();
        openCases = new List<Wrapper>();
        workThisCase = new Case();
        
        showContact = showOpenCases = selectedCaseToWork = false;
    }
    public void populateContactAndCaseId()
    {
        try{
            contactId = Apexpages.currentPage().getParameters().get('conId');
            caseId = Apexpages.currentPage().getParameters().get('csId');
            
            if(contactId != null)
            {
                if(caseId == null || caseId == ''){
                    for(Case c : [SELECT Id FROM Case WHERE CreatedDate = TODAY AND CreatedBy.ContactId = :contactId ORDER BY CreatedDate DESC LIMIT 1]){
                      caseId = c.Id;
                    }
                }
                
                contact = [Select Id, Name, Email, Languages__c, Elite_Status__c,
                                  Age__c, Account.Name from Contact where Id = :contactId];
                showContact = true;
                
                for(Case c : queryOpenCases(contact.Id))
                {
                    if(c.Id != caseId)
                        openCases.add(new Wrapper(false, c));
                }
                
                if(openCases.size() > 0)
                {
                    showOpenCases = true;
                }
                else
                {
                    showOpenCases = false;
                }
            }
            
            if(caseId != null)
            {
                autoCreatedCase = [Select Id, CaseNumber, Subject, Priority, CreatedDate, Owner.Name 
                                          from Case where Id = :caseId];
            }
        }catch(Exception e){
            system.debug(e);
        }
    }
    
    public List<Case> queryOpenCases(Id conId)
    {
        List<Case> cases = new List<Case>();
        cases = [Select Id, CaseNumber, Subject, Priority, CreatedDate, Owner.Name 
                                      from Case where ContactId = :conId and Status = 'Open' order by CreatedDate];
        return cases;
    }
    
    public void selectOpenCase()
    {
        string selectedOpenCaseId = Apexpages.currentPage().getParameters().get('selectedOpenCase');
        
        if(selectedOpenCaseId != null && selectedOpenCaseId != '')
        {
            for(Wrapper w : openCases)
            {
                if(w.CaseRecord.Id == selectedOpenCaseId)
                {
                    w.flag = true;
                }
                else
                {
                    w.flag = false;
                }
            }
        }
    }
    
    //public void workTheSelectedCase()
    public pageReference workTheSelectedCase() //ATVI modification by FGW, fixes Dispositions not showing and edit redirect issue
    {       
        for(Wrapper w : openCases)
        {
            if(w.Flag)
            {
                workThisCase = w.CaseRecord;
                break;
            }
        }
        
        if(workThisCase != null && autoCreatedCase != null)
        {
            autoCreatedCase.Merge_To_Case__c = workThisCase.Id;
            autoCreatedCase.Status = 'Merge - Duplicate';
            autoCreatedCase.Comment_Not_Required__c = true;
            update autoCreatedCase;
            
            autoCreatedCase = null;
            selectedCaseToWork = true;
        }
        return new ApexPages.standardController(workThisCase).view();
    }
    
//    public void thisIsANewCase()
    public pageReference thisIsANewCase() //ATVI modification by FGW, fixes Dispositions not showing and edit redirect issue
    {
        workThisCase = autoCreatedCase;
        selectedCaseToWork = true;
        return new ApexPages.standardController(workThisCase).view();
    }
    
    public class wrapper
    {
        public Boolean flag {get;set;}
        public Case caseRecord {get;set;}
        
        public wrapper(Boolean f, Case c)
        {
            flag = f;
            caseRecord = c; 
        }
    }
}