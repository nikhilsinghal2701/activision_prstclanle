@isTest
public class LiveAgentConsoleVFControllerTest
{
    public static testMethod void WorkExistingCase()
    {
        Account acc = LiveAgentCustomizationsTestUtil.createAccount('Test Class Account');
        Contact con = LiveAgentCustomizationsTestUtil.createContact('TestClass', 'Contact', 'testClass@Contact.com', acc.Id);
        List<Case> cases = LiveAgentCustomizationsTestUtil.createCases(5, acc.Id, con.Id, 'Open');
        
        test.startTest();
        
        test.setCurrentPage(Page.LiveAgentConsoleVF);
        ApexPages.currentPage().getParameters().put('conId', con.Id);
        ApexPages.currentPage().getParameters().put('csId', cases[4].Id);
        
        LiveAgentConsoleVFController controller = new LiveAgentConsoleVFController();
        controller.populateContactAndCaseId();
        
        system.assertEquals(true, controller.ShowContact, 'ShowContact flag should be true to render the contact details in VF Page');
        system.assertEquals(true, controller.ShowOpenCases, 'ShowOpenCases flag should be true to render the Open Cases Section in VF Page');
        system.assertEquals(4, controller.OpenCases.size(), 'Total of 4 Open cases');
        
        ApexPages.currentPage().getParameters().put('selectedOpenCase', cases[0].Id);
        controller.selectOpenCase();
        controller.workTheSelectedCase();
        
        system.assertEquals(cases[0].Id, controller.workThisCase.Id, 'Existing Case is selected');
        system.assertEquals('Merge - Duplicate', [Select Status from Case where Id = :cases[4].Id].Status, 'Auto Created Case should be set to Merge - Duplicate');
        system.assertEquals(cases[0].Id, [Select Merge_To_Case__c from Case where Id = :cases[4].Id].Merge_To_Case__c, 'Merge to Case should be the selected case');
        
        test.stopTest();
    }
    
    public static testMethod void WorkOnNewCase()
    {
        Account acc = LiveAgentCustomizationsTestUtil.createAccount('Test Class Account');
        Contact con = LiveAgentCustomizationsTestUtil.createContact('TestClass', 'Contact', 'testClass@Contact.com', acc.Id);
        List<Case> cases = LiveAgentCustomizationsTestUtil.createCases(1, acc.Id, con.Id, 'Open');
        
        test.startTest();
        
        test.setCurrentPage(Page.LiveAgentConsoleVF);
        ApexPages.currentPage().getParameters().put('conId', con.Id);
        ApexPages.currentPage().getParameters().put('csId', cases[0].Id);
        
        LiveAgentConsoleVFController controller = new LiveAgentConsoleVFController();
        controller.populateContactAndCaseId();
        
        system.assertEquals(true, controller.ShowContact, 'ShowContact flag should be true to render the contact details in VF Page');
        system.assertEquals(false, controller.ShowOpenCases, 'ShowOpenCases flag should be false as there is no open case other than auto created case');
        system.assertEquals(0, controller.OpenCases.size(), 'There is no open cases other than auto created case');
        
        controller.thisIsANewCase();
        
        system.assertEquals(cases[0].Id, controller.workThisCase.Id, 'New Case is selected');
        
        test.stopTest();
    }
}