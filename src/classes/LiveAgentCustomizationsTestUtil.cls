public class LiveAgentCustomizationsTestUtil
{
    public static Account createAccount(string accName)
    {
        Account acc = new Account(Name = accName);
        insert acc;
        
        return acc;
    }
    
    public static Contact createContact(string firstName, string lastName, string email, Id accId)
    {
        Contact con = new Contact(FirstName = firstName, LastName = lastName, Email = email, AccountId = accId);
        insert con;
        
        return con;
    }
    
    public static List<Case> createCases(Integer count, Id accId, Id ConId, string status)
    {
        List<Case> cases = new List<Case>();
        Case c;
        
        for(Integer i=0; i<count; i++)
        {
            c = new Case(Subject = 'Case Created From Test Class', Description = 'Case Created From Test Class', 
                         AccountId = accId, ContactID = conId, Status = status, Comment_Not_Required__c = true);
            cases.add(c);
        }
        
        insert cases;
        
        return cases;
    }
    
    public static LiveChatTranscript createChatTranscript(Id conId, Id caseId, string status)
    {
        LiveChatVisitor visitor = new LiveChatVisitor();
        insert visitor;
        
        LiveChatTranscript transcript = new LiveChatTranscript(ContactId = conId, CaseId = caseId, 
                                                               LiveChatVisitorId = visitor.Id, Status = status);
        insert transcript;
        
        return transcript;
    }
}