@isTest
public class LiveAgentMergeToCasesTest
{
    public static testMethod void MergeCases()
    {
        Account acc = LiveAgentCustomizationsTestUtil.createAccount('Test Class Account');
        Contact con = LiveAgentCustomizationsTestUtil.createContact('TestClass', 'Contact', 'testClass@Contact.com', acc.Id);
        List<Case> cases = LiveAgentCustomizationsTestUtil.createCases(2, acc.Id, con.Id, 'Open');
        LiveChatTranscript transcript = LiveAgentCustomizationsTestUtil.createChatTranscript(con.Id, cases[1].Id, 'Completed');
        
        test.startTest();
        
        cases[1].Merge_To_Case__c = cases[0].Id;
        cases[1].Status = 'Merge - Duplicate';
        cases[1].Comment_Not_Required__c = true;
        update cases[1];
        
        DeleteMergeToDuplicateCasesScheduler scheduler = new DeleteMergeToDuplicateCasesScheduler();
        DateTime startDate = system.Now().addDays(10);
        string exp = '0 0 0 ' + startDate.day() + ' ' + startDate.Month() + ' ? ' + startDate.Year();
        Id cronId = system.schedule('Delete Duplicate Cases Test Class', exp, scheduler);
        
        system.assertEquals(cases[0].Id, [Select CaseId from LiveChatTranscript where Id = :transcript.Id].CaseId, 'Chat Transcript should merged to Parent Case');
        
        test.stopTest();
        
        //system.assertEquals(0, [Select count() from Case where Id = :cases[1].Id], 'Duplicate Cases should be deleted by Batch');
    }
}