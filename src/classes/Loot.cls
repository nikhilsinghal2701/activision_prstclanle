public without sharing class Loot {
	
	public case theCase{get;set;}
    public loot_recovery__c lootr{get;set;}
    public string checkURL{get;set;}
    public string eligible{get;set;}
    public boolean freeform{get;set;}
    public string query{get;set;}
    public string contentType{get;set;}
    public string body{get;set;}
    public string env{get;set;}
    public list<list<string>> selectedItems{get;set;} 
    public boolean throttleWarning{get;set;}
    public integer throttle{get;set;}

	public Loot() {
	}

	public Loot(ApexPages.StandardController stdController){

		query = 'http://shg-gavel.activision.com/svc/blacksmith';
		env = 'live';
		contentType = 'application/json';
		selectedItems = new list<list<string>>();
		showError = false;
		errorMsg = '';
		freeform = false;
		throttleWarning = false;
		lootr = (loot_recovery__c)stdController.getRecord();
		ldata = new list<awlootdata>();
		system.debug(lootr);
		if(lootr.case__c != null){
		theCase = [select id, contactid from case where id=:lootr.case__c];
		lootr.contact__c = theCase.contactid;
		}
		else{
			freeform = true;
		}
	}

	public pageReference insertLootRequest(){
		list<loot_recovery__c> lootToInsert = new list<loot_recovery__c>();
		if(string.isBlank(lootr.userid__c) || 
				string.isBlank(lootr.console__c)
				){
				showError = true;
				errorMsg = 'Please Select UserID, Console';
				return null;
			}

		DwProxy.service serv = new dwProxy.service();
		serv.timeout_x = 120000;
		string tempquery = query;
        tempquery+= '/'+env+'/'+console.trim()+'/restoreDeletedItem';
        String qSuffix = '@headers-exist@@usecsnetworkcreds#true';
        tempquery += qSuffix;
		for(list<string> l:selectedItems){
			body = '{"Desc":"'+lootr.description__c+'","LootEventId":'+l[0]+',"SelfService":false,"UserId":"'+gamertag+'"}';
			system.debug(body);
            if(!test.isRunningTest()){
			resp = serv.passEncryptedPostData(tempquery, body, contenttype);
            }
            else{
                resp = '{"AutoResolved":true,"CaseId":2147483647}';
            }
			if(!resp.startswith('<')){
            	ent = (entry)json.deserialize(resp,entry.class);
            }

            if(ent.autoresolved){
				lootToInsert.add( new loot_recovery__c(
					case__c = lootr.case__c,
					contact__c = lootr.contact__c,
					console__c = console,
					description__c = lootr.description__c,
					item_name__c = l[1],
					lootEventId__c = l[0],
					rarity__c = l[2],
					userid__c = gamertag,
					gid__c = ent.CaseId,
					status__c = 'Restored',
					autoresolved__c = ent.autoresolved
					)
					);
			}
			else{
				lootToInsert.add( new loot_recovery__c(
					case__c = lootr.case__c,
					contact__c = lootr.contact__c,
					console__c = console,
					description__c = lootr.description__c,
					item_name__c = l[1],
					lootEventId__c = l[0],
					rarity__c = l[2],
					userid__c = gamertag,
					gid__c = ent.CaseId,
					status__c = 'Submitted',
					autoresolved__c = ent.autoresolved
					)
					);
			}
			
		}
		insert lootToInsert;
		return new pageReference('/'+lootr.case__c);
	}

	public string gamertag{get;set;}
	public string console{get;set;}
	public string resp{get;set;}
	public boolean showError{get;set;}
	public string errorMsg{get;set;}
	public list<AWLootData> ldata {get;set;}
	public entry ent{get;set;}

	public void getResponse(){
		showError = false;
		errorMsg = '';
		throttleWarning = false;
		ldata.clear();
		if(! isBlankOrNull(gamerTag) && ! isBlankOrNull(console)){
			if(gamertag.contains('STEAM_')){
				gamertag = gamertag.trim();            
				string[] parts = gamertag.split(':');
				long V = 76561197960265728L;
				long Y = long.valueOf( parts[ 1 ] );
				long Z = long.valueOf( parts[ 2 ] );
				long W = Z * 2 + V + Y;
				system.debug('w == '+w);
				gamertag = string.valueof(W);
			}
			lootr.userid__c = gamertag;
			lootr.console__c = console;

			integer selfCount = [select id from loot_recovery__c where userid__c=:gamertag and console__c=:console and selfservice__c =true].size();
    	integer agentCount = [select id from loot_recovery__c where userid__c=:gamertag and console__c=:console and selfservice__c =false].size();
    	throttle = [select id from loot_recovery__c where userid__c=:gamertag and console__c=:console and createddate = last_n_days:182].size();

    	if(throttle > 0){
    		throttleWarning = true;
    	}

    	eligible = string.valueof(selfCount) + ' Self Submissions & ' + string.valueof(agentCount) + ' Agent Submissions';

			DwProxy.service serv = new dwProxy.service();
			serv.timeout_x = 120000;
			//http://shg-gavel.activision.com/svc/blacksmith/dev/pc/otardy1
			//string query = 'http://shg-gavel.activision.com/svc/blacksmith/dev';
			//string query = 'https://contra.infinityward.net/svc/ghosts/live';
            //query += UserInfo.getOrganizationId() == '00DU0000000HMgwMAG' ? 'live' : 'dev';
            string tempquery = query;
            tempquery+= '/'+env+'/'+console.trim()+'/generateRecentlyDeletedItems';//+type;        
            String qSuffix = '@headers-exist@@usecsnetworkcreds#true';
            tempquery += qSuffix;
            body = '{"UserId":"'+gamertag+'"}';
        	system.debug('Query:'+tempquery);
        	system.debug('Body:'+body);
        	system.debug('Content Type:'+contenttype);
        	try{
	            resp = serv.passEncryptedPostData(tempquery, body, contenttype);

	            if(!resp.startswith('<')){
	            	ldata = (list<AWLootData>)json.deserialize(resp,list<awlootdata>.class);
	            }
	            else{
	            	showError = true;
	            	errorMsg = 'Invalid GT & Console combination';
	            }
            }
            catch(exception e){
            	showError = true;
            	errorMsg = 'Service Down. Please log service ticket';
            }
            
            //Check The Try/Catch PlayerData2
        }
    }
    public boolean isBlankOrNull(String s){ return s == null || s == ''; }

    public static selectOption[] getConsolePicklist(){
    	return new selectOption[]{
    		new selectOption('','--None--')
    		,new selectOption('ps4','PS4')
    		,new selectOption('xboxone','XB1')
    		,new selectOption('ps3','PS3')
    		,new selectOption('xbox','Xbox360')
    		,new selectOption('pc','PC')
    	};
    }

    public pageReference selectItem(){
    	for(list<string> l:selectedItems){
    		if (l[0]==lootr.lootEventId__c){
    			return null;
    		}
    	}
    	selectedItems.add(new list<string>{lootr.lootEventId__c,lootr.item_name__c,lootr.rarity__c});

    	return null;
    }

    public pageReference clearSelected(){
    	selectedItems.clear();
    	return null;
    }

    public class entry{
    	public boolean AutoResolved{get;set;}
    	public string CaseId{get;set;}
    }
}