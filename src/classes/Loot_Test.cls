@isTest
private class Loot_Test {
	
	@isTest static void test_method_one() {
		Account a = new Account(Name = 'test');
        insert a;
        
        Contact c = new Contact(
            AccountId = a.Id,
            FirstName = 'first',
            LastName = 'last', 
            Email = '123@test.test',
            UCDID__c = '123',
            Birthdate = system.today().addDays(-7300)
        );
        insert c;

        Case cas1 = new Case(ContactId = c.Id, Comment_Not_Required__c = true);
        insert cas1;

		loot_recovery__c lr = new loot_recovery__c(
			AutoResolved__c = true,
			Case__c = cas1.Id,
			Console__c = 'PS4',
			Contact__c = c.Id,
			Description__c = 'test',
			Gid__c = '123',
			Ip__c = '0.0.0.0',
			Item_Name__c = 'Test',
			LootEventId__c = '123',
			Rarity__c = 'Enlisted',
			SelfService__c = false,
			Status__c = 'New',
			UserID__c = 'TestGT'
			);
		insert lr;
		ApexPages.StandardController sc = new ApexPages.StandardController(lr);
		loot l = new loot(sc);
		l.console = 'ps3';
		l.gamertag = 'Test';
		l.selectedItems.add(new list<string>{'123','345','456'});
		l.insertLootRequest();
		l.getResponse();
		loot.getConsolePicklist();
		l.selectItem();
		l.clearSelected();
		l.gamertag = 'STEAM_0:1:111111';
		l.getResponse();
	}
	
}