public class MedalliaSettingController{
    public Medallia_Setting__c settings{get;set;}
    public string curl{get;set;}
    public boolean autofire {get;set;}
    
    public MedalliaSettingController(){
        String qry = 'SELECT Sampling__c, Delay__c, Mobile_Delay__c, Skylanders_Sampling_Rate__c FROM Medallia_Setting__c WHERE Name = \'';
        try{
            autofire = true;
            settings = new Medallia_Setting__c(
                Sampling__c = 0, Delay__c = 0, Mobile_Delay__c = 0, Skylanders_Sampling_Rate__c = 0
            ); //set the default values for "no records" scenario
            for(Medallia_Setting__c sett : database.query(qry+'All\'')){
                settings = sett; //load the "All" value
            }
            //test for a custom page setting
            //get the current pagename
            curl = ApexPages.currentPage().getUrl();
            system.debug(curl);
            String pageName = curl.split('\\?')[0]; //this returns everything to the left of the ?
            pageName = pageName.replaceAll('/\\?','\\?'); //remove any occurance of '[pageName]/?'
            for(String chunk : pageName.split('/')){
                pageName = chunk; //this ensures that pageName is now the last segment of the URL
            }
            //check to see if there are settings for this page
            for(Medallia_Setting__c sett : database.query(qry+pageName+'\'')){
                settings = sett;
            }
            //now we have one of 3 possible values
            //1 - Default values if no Medallia_Setting__c records exist
            //2 - 'All' setting, if it exists
            //3 - PageName specific setting, if it exists
        }catch(Exception e){
            system.debug('ERRRRRRRRRR == '+e);
        }
    }
    public boolean getIsMobile(){
        String userAgent = '';
        if(ApexPages.currentPage() != null && ApexPages.currentPage().getHeaders().containsKey('USER-AGENT')){
            userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
        }
        return userAgent.containsIgnoreCase('mobi');
    }
    public integer getDelay(){
        if(getIsMobile()){
            return integer.valueOf(settings.Mobile_Delay__c);
        }
        return integer.valueOf(settings.Delay__c);
    }
    public String getSurvLang(){
        try{
            //Multilingual support
            string lang = '';
            string[] searchLocations = new string[]{'uil','lang','ulang','apex__uLang','apex__uil','apex__lang','apex__ulang'};
            for(String searchLocation : searchLocations){//url is checked first
                if(ApexPages.currentPage().getParameters().containsKey(searchLocation)){
                    lang = ApexPages.currentPage().getParameters().get(searchLocation);
                    system.debug('lang found in URL == '+lang);
                    break;
                }
            }
            if(lang == ''){//if not in url check cookie
                for(String searchLocation : searchLocations){//url is checked first
                    Cookie aCookie = ApexPages.currentPage().getCookies().get(searchLocation);
                    if(aCookie != null && aCookie.getValue() != null && aCookie.getValue() != ''){
                        lang = aCookie.getValue();
                        system.debug('lang found in cookie == '+lang);
                        break;
                    }
                }
            }

            //lang could be present but set to some other value, filter those out 
            if(lang.containsIgnoreCase('de')) lang = 'German';
            else if(lang.containsIgnoreCase('fr')) lang = 'French';
            else if(lang.containsIgnoreCase('it')) lang = 'Italian';
            else if(lang.containsIgnoreCase('es')) lang = 'Spanish';
            else if(lang.containsIgnoreCase('nl')) lang = 'Dutch';
            else if(lang.containsIgnoreCase('sv')) lang = 'Swedish';
            else if(lang.containsIgnoreCase('pt')) lang = 'Portuguese (Brazilian)';
            else lang = 'English';
            return lang;
        }catch(Exception e){ 
            system.debug('ERROR == '+e);
            return 'English';
        }
    }
}