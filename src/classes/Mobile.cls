public class Mobile{
    public string cat{get;set;}
    public string catquery{get;set;}
    public string lang{get;set;} //for labels / translations
    public string artid{get;set;}
    public string artno{get;set;}
    public string arttitle{get;set;}
    public boolean catError{get;set;}
    
    public map<string,PKB_Article_Feedback_Deflection__c> oldartid{get;set;}
    
    public list<faq__kav> article{get;set;}
    //public list<Mobile_title_Support__c> mts{get;set;}
    public Mobile_Title_Support__c mts{get;set;}
    public Mobile(){
        catError = false;
        cat = ApexPages.currentPage().getparameters().get('t');
        lang = getLang();
        
        oldartid = new map<string,PKB_Article_Feedback_Deflection__c>();
        
        mts = new Mobile_Title_Support__c(
            Data_category__c = 'Call_of_Duty_Ghosts',
            Static_Resource_Name__c = 'Mobile',
            Custom_CSS__c = '',
            Image__c = ''
        );
        
        if(!isBlank(cat)){         
            try{
                mts = [SELECT Custom_CSS__c, Data_Category__c, Image__c, Static_Resource_Name__c FROM Mobile_title_Support__c WHERE data_category__c =:cat limit 1];
                try{
                    if(mts.Image__c.containsIgnoreCase(' class="')){
                        mts.Image__c = mts.Image__c.replaceAll(' class="', ' class="logo ');
                    }else{
                        mts.Image__c = mts.Image__c.replaceAll('<img ', '<img class="logo" ');
                    }
                    system.debug('mts.Image__c == '+mts.Image__c);
                }catch(Exception ex){}
                catquery =cat+'__c';
                try{
                    article = database.query('SELECT title, answer__c, ArticleNumber FROM faq__kav WHERE publishstatus = \'Online\' and language = \''+getArtLang(lang)+'\' WITH DATA CATEGORY Game_Title__c AT ('+catquery+')');
                }catch(Exception e){//this try/catch allows the proper branding to be shown while allowing the article query to fail.
                    system.debug('art query err == '+e);
                }
            }
            catch(Exception e){
                system.debug('error == '+e);
                catError=true;
            }
        }
        //article = [select id,title,answer__c from faq__kav where publishstatus = 'Online' and language = 'en_US' WITH DATA CATEGORY Game_Title__c AT (:cat)];

    }
    
    public boolean isBlank(string s){ return s=='' || s==null; }
    
    PKB_Article_Feedback_Deflection__c afd;
    public boolean boolArticleWasHelpful {get;set;}
    public FAQ__kav faq {get; set;}
    public void recordHelpful(){
        if(!oldartid.containskey(artid)){
            
            afd = new PKB_Article_Feedback_Deflection__c(
                Article_ID__c = artid,
                Article_Number__c = artno,
                Article_Title__c = arttitle,
                Feedback_Source__c = 'Mobile Site',
                game__c = cat,
                Session_ID__c = userinfo.getSessionId(),
                Deflection__c = true
            );
            upsert afd;
            oldartid.put(artid,afd);
        }
        else{
            oldartid.get(artid).deflection__c = !oldartid.get(artid).deflection__c;
            //afd.Deflection__c = !afd.Deflection__c;
            update oldartid.get(artid);
        }
    }
    public string getLang(){ //for labels / translations
        String lang = '';
        if(ApexPages.currentPage() != null && ApexPages.currentPage().getParameters().containsKey('uLang')){
            lang = ApexPages.currentPage().getparameters().get('ulang');
        }
        if(isBlank(lang)) lang = '';
        lang = lang.toLowerCase();
        if(lang.contains('de')) lang = 'de_DE';
        else if(lang.contains('fr')) lang = 'fr_FR';
        else if(lang.contains('it')) lang = 'it_IT';
        else if(lang.contains('es')) lang = 'es_ES';
        else if(lang.contains('pt')) lang = 'pt_BR';
        else if(lang.contains('nl')) lang = 'nl_NL';
        else if(lang.contains('sv')) lang = 'sv_SE';
        else lang = 'en_US';
        return lang;
    }
    public string getArtLang(String lang){//for article query
        string artLang = 'en_US';
        if(! new set<String>{'nl_NL','en_US','fr','de','it','pt_BR','es','sv'}.contains(lang)){
            lang = lang.toLowerCase();
            if(lang.contains('de')) artLang = 'de';
            else if(lang.contains('fr')) artLang = 'fr';
            else if(lang.contains('it')) artLang = 'it';
            else if(lang.contains('es')) artLang = 'es';
            else if(lang.contains('pt')) artLang = 'pt_BR';
            else if(lang.contains('nl')) artLang = 'nl_NL';
            else if(lang.contains('sv')) artLang = 'sv';
            else artLang = 'en_US';
        }
        return artLang;
    }
    public component.apex.outputText getCustomCSSwithMergeFields(){
        component.apex.outputText dText = new component.apex.outputText();
        if(mts != null && mts.Custom_CSS__c.contains('{!')){
            dText.expressions.value = mts.Custom_CSS__c;
        }else{
            dText.value = '';
        }
        return dText;
    }
    /*public void recordHelpful(){
try{
if(boolArticleWasHelpful){
afd = new PKB_Article_Feedback_Deflection__c(
Article_ID__c = artid,
Article_Number__c = artno,
Article_Title__c = arttitle,
Feedback_Source__c = 'Mobile Site',
Deflection__c = true
);
upsert afd;
}else if(afd != null && afd.Id != null){
delete afd;
afd = new PKB_Article_Feedback_Deflection__c();
}
}catch(Exception e){
system.debug('ERROR == '+e);
}
}*/
    
}