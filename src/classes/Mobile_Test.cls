@isTest
public class Mobile_Test{
    static testmethod void Mobile_Test(){
        Mobile_Title_Support__c mts = new Mobile_Title_Support__c(
            Data_category__c = 'Call_of_Duty_Ghosts',
            Static_Resource_Name__c = 'Mobile',
            Custom_CSS__c = '',
            Image__c = ''
        );
        insert mts;
        
        PageReference p=Page.Mobile; 
        
        p.getparameters().put('t','Call_of_Duty_Ghosts');
        p.getparameters().put('lang','en_US');
        
        system.Test.setCurrentPage(p);
        
        test.startTest();

        Mobile mo = new Mobile();
        mo.recordHelpful();
        mo.recordHelpful();
        mo.getArtLang('');
        mo.mts = mts;
        mo.getCustomCSSwithMergeFields();
        
        
        test.stopTest();
    }
}