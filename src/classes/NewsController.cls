public with sharing class NewsController {
    public list<news__c> newslist {get;set;}
    public list<news__c> totallist {get;set;}
    public string year{get;set;}
    public string hour{get;set;}
    public string minute{get;set;}
    
    public integer page{get;set;}
    public integer currPage{get;set;}
    public integer totalRecords{get;set;}
    public integer totalPages{get;set;}
    
    public boolean showPrev {get;set;}
    public boolean showNext {get;set;}
    
    /*Checks if user is logged in*/
    public boolean isPortalUser{
        get{
            if(UserInfo.getUserType().equalsIgnoreCase('CSPLitePortal')) return true;
            else return false;
        }
        set;
    }
    
    /*Used to set the language of the Apex page acccording to the user's choice*/    
    public String lang {get;set;}
    public void getUserLanguage()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            system.debug('tempCookie not null');
            lang = tempCookie.getValue();            
        }else{
            system.debug('tempCookie is null');
            lang='en_USDefault';
        }
    }
    
     /**language code used to construct the Banning Policy article url displayed on the home page
    ** created 03.29.2012 - abpal
    **/
    public String bannedFaqLanguage{
        set;
        get{
            string bannedLang;
            if(lang=='en_US'||lang=='en_GB')
                bannedLang='en_US';
            else if (lang=='de_DE')
                bannedLang='de';
            else if (lang=='fr_FR')
                bannedLang='fr';
            else if (lang=='es_ES')
                bannedLang='es';
            else if (lang=='it_IT')
                bannedLang='it';
            else if (lang=='es_ES')
                bannedLang='es';
            else if (lang=='nl_NL')
                bannedLang='nl_NL';
            else if (lang=='sv_SE')
                bannedLang='se';  
            else if (lang=='pt_BR')
                bannedLang='pt_BR';             
                
            system.debug('bannedFaqLanguage being returned:'+bannedLang);
            return bannedLang;
        }       
    }
    
    /*Constructor*/
    public NewsController(){
        getUserLanguage();
        currPage=1;
        page=0;
        /*Grabs the 6 records to display*/
        newslist = [select id, 
                    name, 
                    news_Title__c,
                    news_Title_Dutch__c,
                    news_Title_French__c,
                    news_Title_German__c,
                    news_Title_Italian__c,
                    news_Title_Portuguese__c,
                    news_Title_Spanish__c,
                    news_Title_Swedish__c,
                    body__c,
                    body_Dutch__c,
                    body_French__c,
                    body_German__c,
                    body_Italian__c,
                    body_Portuguese__c,
                    body_Spanish__c,
                    body_Swedish__c,
                    brand__c,
                    thelink__c,
                    thelink_Dutch__c,
                    thelink_French__c,
                    thelink_German__c,
                    thelink_Italian__c,
                    thelink_Portuguese__c,
                    thelink_Spanish__c,
                    thelink_Swedish__c,
                    lastmodifieddate 
                    from news__c where isActive__c=true order by lastmodifieddate desc limit 6];
        /*Counts the total amount of records to calculate the page*/
        totallist = [select id, name, news_Title__c, body__c, brand__c,lastmodifieddate from news__c where isActive__c=true];
        totalRecords = totallist.size();
        totalPages = (totalRecords/6)+1;
        showPrev = false;
        /*Checks to see if next should be available*/
        if (totalRecords>6){
            showNext = true;
        }
        else{
            showNext = false;
        }
    
    }
    
    /*Gets the 6 records corresponding to the page*/
    public void getPage(integer i){
        page=(i-1)*6;
        newslist = [select id, 
                    name, 
                    news_Title__c, 
                    news_Title_Dutch__c,
                    news_Title_French__c,
                    news_Title_German__c,
                    news_Title_Italian__c,
                    news_Title_Portuguese__c,
                    news_Title_Spanish__c,
                    news_Title_Swedish__c,
                    body__c, 
                    brand__c,
                    body_Dutch__c,
                    body_French__c,
                    body_German__c,
                    body_Italian__c,
                    body_Portuguese__c,
                    body_Spanish__c,
                    body_Swedish__c,
                    thelink__c,
                    thelink_Dutch__c,
                    thelink_French__c,
                    thelink_German__c,
                    thelink_Italian__c,
                    thelink_Portuguese__c,
                    thelink_Spanish__c,
                    thelink_Swedish__c,
                    lastmodifieddate 
                    from news__c where isActive__c=true order by lastmodifieddate desc limit 6 offset :page];
        if (page+6<totalRecords){
            showNext=true;
        }
        else{
            showNext=false;
        }
        if (page-6>=0){
            showPrev=true;
        }
        else{
            showPrev=false;
        }
    }
    
    /*Pagination controls*/
    public pagereference nextPage(){
        currPage=currPage+1;
        getPage(currPage);
        return null;       
    }
    public pagereference next2Page(){
        currPage=currPage+2;
        getPage(currPage);
        return null;       
    }
    public pagereference next3Page(){
        currPage=currPage+3;
        getPage(currPage);
        return null;       
    }
    public pagereference next4Page(){
        currPage=currPage+4;
        getPage(currPage);
        return null;       
    }
    
    public void prevPage(){
        currPage=currPage-1;
        getPage(currPage);
    }
    public void prev2Page(){
        currPage=currPage-2;
        getPage(currPage);
    }
    public void prev3Page(){
        currPage=currPage-3;
        getPage(currPage);
    }
    public void prev4Page(){
        currPage=currPage-4;
        getPage(currPage);
    }
}