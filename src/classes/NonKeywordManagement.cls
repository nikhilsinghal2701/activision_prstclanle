public class NonKeywordManagement {
    public string sObjectName{get;set;}
    public string LanguageCode {get;set;}
    public string LanguageName {get;set;}
    public id recordId {get;set;}
    public string SObjName;    
    public Map<String,Schema.SObjectType> Getobjlist;
    public Non_Keywords__c NonKeyWordsObj{get;set;}  
    pagereference pageref;  
    //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'my error msg');
    public NonKeywordManagement(ApexPages.StandardController controller) {
        recordId = ApexPages.currentPage().getParameters().get('id');
        NonKeyWordsObj = new Non_Keywords__c();  
        if(recordId!=null)
        {
          Non_Keywords__c nonkeyvar = [select id,Name,Language_code__c,Language_Name__c,Object_Name__c,Object_Prefix__c,Non_Keywords__c from Non_Keywords__c where id=:recordId];
          NonKeyWordsObj.Non_Keywords__c = nonkeyvar.Non_Keywords__c;
          sObjectName  = nonkeyvar.Object_Name__c;
          system.debug('nbasdhbshbdhbsd===='+nonkeyvar.Language_Name__c+'====LanguageCode ===='+LanguageCode );       
          //LanguageCode = nonkeyvar.Language_Name__c;    
          LanguageCode = nonkeyvar.Language_code__c;
          system.debug('asjdjjsajd====='+LanguageCode);       
        }     
    }
    
    public NonKeywordManagement(){} 
    
    public List<SelectOption> getItems() {
        set<string> sObjectNameList = schema.getGlobalDescribe().keyset();

        List<SelectOption> options = new List<SelectOption>();
        For (string obj: sObjectNameList){
        options.add(new SelectOption(obj,obj));

        }
        return options;
    }
    
    public List<SelectOption> getName(){
        List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();     
        List<SelectOption> options = new List<SelectOption>();
        
        for(Schema.SObjectType f : gd)
        {
           Schema.DescribeSObjectResult r =  f.getDescribe();                        
           if(r.getKeyPrefix()!=null && r.isAccessible() && r.isCreateable() && !r.isCustomSetting() && r.isSearchable()){                       
           //options.add(new SelectOption(f.getDescribe().getKeyPrefix(),f.getDescribe().getLabel()));
           options.add(new SelectOption(f.getDescribe().getLabel(),f.getDescribe().getLabel()));
                
           }
        }
        return options;
   }
   
    public List<SelectOption> getLanguages() {
        List<SelectOption> options = new List<SelectOption>();                 
      /*  List<Language_object__c> mcs = Language_object__c.getall().values();
        for(Language_object__c LObj:mcs)
        {
             options.add(new SelectOption(LObj.Name,LObj.Name));
        }  */
        for(Schema.PicklistEntry ple : User.LocaleSidKey.getDescribe().getPicklistValues())
        {
            options.add(new SelectOption(ple.getValue(),ple.getLabel())); 
            //system.debug('ple == '+ple);
        }
        
        return options;
    }
    
    public pagereference saveRecord(){
       // system.debug('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'+LanguageCode );
       // system.debug('************************************'+LanguageName );
        String tempPrefix,StrLanguageName;
       
       SObjName = sObjectName;         
       tempPrefix =  getkeyprefix(SObjName);        
       //Language_object__c Lanobj = Language_object__c.getValues(LanguageCode);
       system.debug('LanguageCode======'+LanguageCode);
       string strdesc = NonKeyWordsObj.Non_Keywords__c;    
      if(recordId!=null)
      NonKeyWordsObj = [select id,Name,Language_code__c,Language_Name__c,Object_Name__c,Object_Prefix__c,Non_Keywords__c from Non_Keywords__c where id=:recordId];      
       
       for(Schema.PicklistEntry ple : User.LocaleSidKey.getDescribe().getPicklistValues())
       {
           if(LanguageCode == ple.getValue())
           StrLanguageName =  ple.getLabel();   
       }
  
      NonKeyWordsObj.Language_code__c =  LanguageCode ;
      NonKeyWordsObj.Language_Name__c =  StrLanguageName ;      
      NonKeyWordsObj.Object_Name__c =  sObjectName;     
      NonKeyWordsObj.Object_Prefix__c = tempPrefix;
      NonKeyWordsObj.Non_Keywords__c = strdesc;
      
              
      try
      {        
          upsert NonKeyWordsObj; 
          pageref  = new pagereference('/'+NonKeyWordsObj.id);
          pageref.setredirect(true);                     
      }
      catch(Exception e)
      {
        //Apexpages.addMessages(e);
        NonKeyWordsObj.addError('There is one existing record with same combination of Object and Language'); 
       // Apexpages.addMessages(myMsg);
        pageref = null;        
      }
      
      return pageref;           
    }
    
    public pagereference CancelAction()    
    {
      SObjName = 'Non-Keywords';
      if(recordId!=null)
      {
           pageref = new pagereference('/'+recordId);                
      }
      else
      {        
          string strprefix =  getkeyprefix(SObjName);
          pageref = new pagereference('/'+strprefix+'/o'); 
      }        
      pageref.setredirect(true);        
      return pageref;   
    }
    public string getkeyprefix(string SObjName)
    {
        string strkeyprefix;    
        Getobjlist = Schema.getGlobalDescribe();  
        for(String sObj : Getobjlist.keyset())   
        {   
            Schema.DescribeSObjectResult r =  Getobjlist.get(sObj).getDescribe();               
            if(SObjName!=null && SObjName!='' && r.getLabel() == SObjName) 
            {      
                strkeyprefix = r.getKeyPrefix();    
            }            
        }
        return strkeyprefix;
    }
        
    //public static void populateKeywordFields(Case[] records){
      public void populateKeywordFields(Case[] records){
        string objectPrefix;
        
        for(Case CaseObj:records){
            string objid = CaseObj.id;
            if(objid!=null && objid!='')
            objectPrefix= objid.subString(0,3);             
            }
            
        if(objectPrefix == null || objectPrefix == '')
        objectPrefix = getkeyprefix('case');

        Map<String, Case[]> languageToRecords = new Map<String, Case[]>();
        //system.debug('objectPrefix======='+objectPrefix);
        /*
        For (Case CaseRecord:records){
            List<Case> CaseList = new List<Case>();
            if(languageToRecords.containsKey(CaseRecord.W2C_Origin_Language__c))
                CaseList = languageToRecords.get(CaseRecord.W2C_Origin_Language__c);
            CaseList.add(CaseRecord);
            languageToRecords.put(CaseRecord.W2C_Origin_Language__c,CaseList);
        }
        */
        //system.debug('objectPrefix======='+languageToRecords);        
        
        Map<String,list<String>> LanguageToNonKeyWordMap = new Map<String,list<String>>();
        //for(Non_Keywords__c  nonKeyWordObj : [SELECT Language_code__c, Non_Keywords__c FROM Non_Keywords__c WHERE Object_Prefix__c = :objectPrefix AND Language_code__c IN :languageToRecords.keySet()]){
        for(Non_Keywords__c  nonKeyWordObj : [SELECT Language_code__c, Non_Keywords__c FROM Non_Keywords__c WHERE Object_Prefix__c = :objectPrefix]){
            if( nonKeyWordObj.Non_Keywords__c != null && nonKeyWordObj.Non_Keywords__c !='' ){
                String nonkeywrd = nonKeyWordObj.Non_Keywords__c ;
                String cleanednonkeywrd = nonkeywrd.replaceAll('[\\\\!@#%\\^&\\*\\(\\)\\-=\\+_{}\\[\\]\\|\'":/\\?\\.><,`~]','');
                LanguageToNonKeyWordMap.put(nonKeyWordObj.Language_code__c,cleanednonkeywrd.split(';'));
            }
        }
                   
            for(Case record : records){
            
                if(record.W2C_Origin_Language__c == null || record.description == null || record.description == '' || record.W2C_Origin_Language__c == '' || !LanguageToNonKeyWordMap.keyset().contains(record.W2C_Origin_Language__c)){
                    if (record.Keyword_Group_1__c != null && record.Keyword_Group_1__c != '')
                        record.Keyword_Group_1__c = '';
                    if (record.Keyword_Group_2__c != null && record.Keyword_Group_2__c != '')
                        record.Keyword_Group_2__c = '';
                }
                Else{
                String dataToClean = record.description;
                //-----
                //list<string> nkwset = new list<string>();
                //nkwset = nonKeyWordObj.Non_Keywords__c.split(';');
                //system.debug('%%%%%%%%%%%%%%%%%%%%%'+nkwset );
                //---------
                for(String nonKeyword : LanguageToNonKeyWordMap.get(record.W2C_Origin_Language__c)){
                    Integer Len = nonKeyword.Length();
                    if(dataToClean!=''&&dataToClean!=null ){
                        String nonKeyword1 = ' '+'(?i)'+nonKeyword.trim()+' ';
                        dataToClean = dataToClean.replaceAll(nonKeyword1, ' ');
                        system.debug('##############DATA###########'+nonKeyword1 );
                    }
                    boolean check ;
                    boolean check1;
                    system.debug('=============================>'+check);
                    if (dataToClean != null && dataToClean != ''){
                        if(dataToClean.startsWithIgnoreCase(nonKeyword) && dataToClean.length() == Len)
                            dataToClean = '';
                        if(dataToClean.startsWithIgnoreCase(nonKeyword)&& dataToClean.length() > Len){
                            String nonkey = dataToClean.substring(len,len+1);
                            system.debug('%%%%%%%%%%^^^^^^^^^^%%%%%%%%%%%'+nonkey);
                             
                            if(nonKey ==' ')
                                check =  true;
                            Else
                                check = false;
                            if(check)
                                dataToClean = dataToClean.substring(len,dataToClean.length());
                        }
                    
                        system.debug('------------------->'+check1);
                        if(dataToClean.endsWithIgnoreCase(nonKeyword)){
                            String nonkey1 = dataToClean.substring(dataToClean.length()- (len+1),dataToClean.length()- (len));
                            system.debug('%%%%%%%%%%%*********%%%%%%%%%%'+nonkey1);
                            if(nonKey1 ==' ')
                                check1 =  true;
                            Else
                                check1 = false;
                            if(check1)
                                dataToClean = dataToClean.substring(0,dataToClean.length()- len);
                        }
                    }  
                }
                system.debug('@@@@@@@@@@@@@@@@@@@@@@@@'+dataToClean);
                set<String> remainingKeywords = new set<String>();
                if(dataToClean != null && dataToClean != '' )
                    remainingKeywords.addAll(dataToClean.split(' '));
                
                system.debug('+++++++++++++remainingKeywords++++++++++++'+remainingKeywords);
                
                //String RemainingKW = dataToClean.replace(' ', '\n');
                String RemainingKW = dataToClean;
                //system.debug('****************RemainingKW***************'+RemainingKW.length());
                if(dataToClean != null && dataToClean != '' ) {
                    if(RemainingKW.length()>255)
                    {
                        integer index = RemainingKW.substring(0,255).lastIndexOf(' ');
                        //record.Keyword_Group_1__c=RemainingKW.substring(0,255); commented by *
                        system.debug('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&'+index);
                        if(index <= 0)
                            index = 255 ;
                        record.Keyword_Group_1__c=RemainingKW.substring(0,index);
                        
                        //record.Keyword_Group_2__c=RemainingKW.substring(255,RemainingKW.length());
                        //record.Keyword_Group_2__c=RemainingKW.substring(255,500); commented by *
                        integer index2 = RemainingKW.substring(index,RemainingKW.length()).length();
                        system.debug('index2================================'+index2);
                        //string str=RemainingKW.substring(index+1,RemainingKW.length());
                        if(index2<=255) { 
                            record.Keyword_Group_2__c=RemainingKW.substring(index,RemainingKW.length());
                        }
                        else {
                             integer index3 = RemainingKW.substring(index,index+255).lastIndexOf(' ');  
                             system.debug('index3==============================='+index3);
                             if (index3<=0) 
                                 index3 = 255;
                             record.Keyword_Group_2__c= RemainingKW.substring(index,index+index3);//RemainingKW.substring(index,RemainingKW.length()); commented by * 
                        } 
                    }
                    else
                    {
                        record.Keyword_Group_1__c = RemainingKW;
                        record.Keyword_Group_2__c='';
                    }
                    
                } 
                else {
                    record.Keyword_Group_1__c = '';
                    record.Keyword_Group_2__c='';
                }
                }
                }
        
        //}      
        //return records;
    }
    
}