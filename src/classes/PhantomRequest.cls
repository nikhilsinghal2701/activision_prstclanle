public with sharing class PhantomRequest{
/*
*
* See SD#00062299 and/or CS#02467845 for verbose details
*
*/
    /*public variables*/
    public Phantom_Request__c[] previousReports {get; private set;}
    public Phantom_Request__c newReport {get; set;}
    public list<CRR_Code__c> code {get;set;}
    public boolean mpaRefreshComplete {get; set;}
    public Integer errorCount {get; set;}
    public string errorMessage {get; set;}
    public User gamerUser {get; set;}
    public Contact con {get;set;}
    public Attachment attachment {get;set;}
    public String strCode {get;set;}
    public String CTParam {get;set;}

    //Form Uploaded Document
    public Attachment uploadedFile { get ; set; }
    public integer uploadedFileSize { get; set; }
    public string uploadedFileName { get; set; }
    public blob uploadedFileBody { get; set; }
    public string uploadedFileContentType { get; set; }
    
    /*private variables, private is default, no need to redeclare*/
    static final integer MAX_ERRORS = 2;
    map<Id, Multiplayer_Account__c> mpaId2mpa = new map<Id, Multiplayer_Account__c>();
    boolean linkedAccountsPresent = false;
    
    /*Constructor*/
    public PhantomRequest(){
        //CTParam = ApexPages.currentPage().getParameters().get('ct');
        CTParam = 'a1GU00000011Xnv';
        //CTParam = 'a1ZK00000003aiR';
        newReport = new Phantom_Request__c();
        con = new contact();
        this.errorCount = 0;
        this.errorMessage = '';
        this.mpaRefreshComplete= false;
        this.newReport = new Phantom_Request__c();
        this.gamerUser = [SELECT ContactId, Contact.UCDID__c,Contact.email, FederationIdentifier FROM User WHERE Id = :UserInfo.getUserId()];
        if(this.gamerUser.contactId != null){
            this.previousReports = [SELECT Id, Name FROM Phantom_Request__c WHERE Multiplayer_Account__r.Contact__c = :this.gamerUser.ContactId ORDER BY Name DESC];
        }

        //Init File Upload
        uploadedFileName = '';
        uploadedFileSize = 0;
        uploadedFileContentType = '';
    }
    
    /*page init method*/
    public pageReference pageInitAction(){
        //after the page finishes loading, begin refresh the linked account data
        if(this.gamerUser.ContactId != null){
            if((this.gamerUser.Contact.UCDID__c == null || this.gamerUser.Contact.UCDID__c == '') && 
               (this.gamerUser.FederationIdentifier != null && this.gamerUser.FederationIdentifier != ''))
            {
                try{
                update new Contact(Id = this.gamerUser.ContactId, UCDID__c = this.gamerUser.FederationIdentifier);
                }catch(Exception e){
                //will fail if contact.birthdate is empty, contact.birthdate is empty when TIBCO hasn't updated yet
                }
            }
            Linked_Accounts_Redirect_Controller larc = new Linked_Accounts_Redirect_Controller
            (
                new ApexPages.standardController(new Contact(Id = this.gamerUser.ContactId))
            );
            larc.updateProfile();
        }
        this.mpaRefreshComplete = true;
        return null;
    }
    
    /*build the multiplayer picklist*/
    public selectOption[] getMultiPlayerAccounts(){
        selectOption[] results = new selectOption[]{};
        if(this.gamerUser.ContactId != null){
            this.mpaId2mpa = new map<Id, Multiplayer_Account__c>([SELECT Id, Platform__c, Gamertag__c FROM Multiplayer_Account__c WHERE Contact__c = :this.gamerUser.ContactId]);
            system.debug('Multiplayer Account Map:' + this.mpaID2mpa);
            for(Id mpaId : this.mpaId2mpa.keySet()){
                results.add(new selectOption(mpaId, this.mpaId2mpa.get(mpaId).Gamertag__c+' ('+this.mpaId2mpa.get(mpaId).Platform__c+')'));
                this.linkedAccountsPresent = true;
            }
        }
        return results;
    }
    
    /*get primary issue types from object, iterating over schema */
    public selectOption[] getPrimaryIssueType(){
        selectOption[] results = new selectOption[]{};
        for(Schema.PicklistEntry ple : Stat_Corruption_Report__c.Issue__c.getDescribe().getPicklistValues()){
            results.add(new selectOption(ple.getValue(), ple.getLabel()));
        }
        return results;
    }
    
    /*exit method from the page, either success or failure*/
    public pageReference exit(){
        return new pageReference('/');
    }
    
    /*clear the error message to hide the pop-over*/
    public pageReference acknowledgeError(){
        this.errorMessage = '';
        return null;
    }
    
    /*the work method*/
    public pageReference submit(){
        try{
            this.errorMessage = '';
            //validation 1, do they have accounts linked?
            if(this.linkedAccountsPresent == false){
                //this.errorMessage = 'No associated gamertags, please link your accounts to submit a report';
                this.errorMessage = Label.Atvi_NoGamertags;
                return null;
            }
            
            //validation 2, does this gamertag have an open request?
            String platform = this.mpaId2mpa.get(this.newReport.Multiplayer_Account__c).Platform__c;
            list<Phantom_Request__c> tempPR = [SELECT Actual_Code__c, code__c, verification__c FROM Phantom_Request__c WHERE Multiplayer_Account__c = :this.newReport.Multiplayer_Account__c];
            if(tempPR.size() > 0){
                if(tempPR[0].verification__c == '' || tempPR[0].verification__c == null){
                    //this.errorMessage = 'Your request is still being reviewed';
                    this.errorMessage = Label.Atvi_RequestReviewed;
                }
                else if (tempPR[0].verification__c=='denied'){
                    //this.errorMessage = 'Your request has been declined';
                    this.errorMessage = Label.Atvi_RequestDenied;   
                }
                else{
                    this.errorMessage = this.mpaId2mpa.get(this.newReport.Multiplayer_Account__c).Gamertag__c+' ('+platform+') '+ label.Atvi_AlreadyUsed+' <br /><span class="confirm-message-code">' + tempPR[0].Actual_Code__c + '</span><br/>' + label.Atvi_YouCanEnter + ' <a href="http://www.callofduty.com/camo-redemption">http://www.callofduty.com/camo-redemption</a>';         
                }
                uploadedFileBody = null;
                return null;
            }
            
            //validation 3, Count of Submissions Limit = 2
            integer count = [select id from Phantom_Request__c where Multiplayer_Account__r.Contact__c =:this.gamerUser.contactId].size();
            if(count > 2){
                //this.errorMessage = 'Request Limit Reached';
                this.errorMessage = Label.Atvi_RequestLimit;
                return null;
            }

            // File Upload
            if(uploadedFileBody != null){
                if(uploadedFileSize > 150000){
                    //this.ErrorMessage = 'Image Too Large.';
                    this.ErrorMessage = Label.Atvi_ImageLarge;
                    uploadedFileName = null;
                    uploadedFileBody = null;
                    return null;
                }
                if(uploadedFileContentType != 'image/jpeg' && uploadedFileContentType != 'image/png' & uploadedFileContentType != 'image/gif'){
                    //this.ErrorMessage = 'Invalid Image Type.';
                    this.ErrorMessage = Label.Atvi_InvalidImage;
                    uploadedFileName = null;
                    uploadedFileBody = null;
                    return null;
                }
            }
            else{
                //this.errorMessage = 'Missing Attchment';
                this.errorMessage = Label.Atvi_MissingAttachment;
                return null;
            }


            ////code = [select id,code__c from CRR_Code__c where Code_Type__c =:CTParam and redeemed_by__c = null and Code_Distribution_Request__r.Distribution_type__c = 'Customer Service' limit 1];
            //FGW - Needed to delete the Code_Type__c field, and this system was shut down the day after it went live
            //code = [select id,code__c from CRR_Code__c where Code_Type__c =:CTParam and redeemed_by__c = null limit 1];

            
            if(code.size()>0){

                newReport.code__c = code[0].id;
                code[0].Redeemed_by__c = gamerUser.contactId;
                strCode = code[0].code__c;
                newReport.country__c = con.Country__c;
                newReport.email__c = gamerUser.contact.email;
                newReport.language__c = userLang;
                upsert code;
                upsert newReport;
                
                Attachment tmpAttach = new Attachment();
                tmpAttach.ParentId = newReport.id;
                tmpAttach.Name = uploadedFileName;
                tmpAttach.Body = uploadedFileBody;
                insert tmpAttach;

                tmpAttach = null;
                uploadedFileName = null;
                uploadedFileBody = null;
                
            }
            //upsert newReport;
            //newReport.Mock_Upload__c = attachment.name;
            
            //data prep 1, calc platform enum required by LAS
            /*if(platform == 'PSN'){
                platform = 'PLAYSTATION_3';
            }else{
                platform = 'XBOX_360';
            }*/
            
            //data prep 2, calc booleans required by LAS
            /*Boolean mpReset = false, mpRollback = false, zmReset = false, zmRollback = false;
            String issue = this.newReport.Issue__c == null ? '' : this.newReport.Issue__c;
            if(issue.containsIgnoreCase('multi')){
                if(issue.containsIgnoreCase('roll')) mpRollback = true;
                else mpReset = true;
            }else{
                if(issue.containsIgnoreCase('roll')) zmRollback = true;
                else zmReset = true;
            }*/
            
            //send information to LAS
            /*LASService.BasicHttpBinding_ILinkedAccounts service = new LASService.BasicHttpBinding_ILinkedAccounts();
            
            Integer lasGUID = 0;
            if(test.isRunningTest()){
                if(this.gamerUser.FederationIdentifier == '3'){
                    lasGUID = 3;
                }
            }else{
                lasGUID = service.CreateStatRequest(
                    platform, 
                    this.mpaId2mpa.get(this.newReport.Multiplayer_Account__c).Gamertag__c,
                    mpReset, 
                    mpRollback, 
                    zmReset, 
                    zmRollback, 
                    this.newReport.General_Multiplayer_Stats__c, 
                    this.newReport.General_Zombie_Stats__c, 
                    this.newReport.Weapon_Stats__c, 
                    this.newReport.Weapon_Unlocks__c, 
                    this.newReport.Perks__c, 
                    this.newReport.Scorestreaks__c, 
                    this.newReport.Player_Card_Backings__c, 
                    this.newReport.Emblems__c, 
                    this.newReport.Unlock_Tokens__c, 
                    this.newReport.Prestige_Awards__c, 
                    this.newReport.Other_please_specify__c, 
                    this.newReport.Other_details__c,
                    this.newReport.Played_offline_match_recently__c, 
                    this.newReport.Received_Corrupt_Stats_Message__c, 
                    this.newReport.Current_Previous_ban__c, 
                    this.newReport.Comments__c
                 );
             }*/

            /*if(lasGUID > 0){
                this.newReport.LAS_GUID__c = lasGUID;
                this.newReport.Status__c = 'OPEN';
                insert this.newReport;
            }else{//Error with LAS, tell user to try again, unless they've already tried MAX_ERRORS times
                this.errorCount++;
                if(this.errorCount < MAX_ERRORS){
                    this.errorMessage = 'There has been an error, please try again.';                    
                }else{
                    //they've already tried MAX_ERRORS times, dont tell them to do it again
                    insert this.newReport;
                    this.newReport = [SELECT Id, Name FROM Stat_Corruption_Report__c WHERE Id = :this.newReport.Id];
                    this.newReport.LAS_GUID__c = integer.valueOf(this.newReport.Name) * -1;
                    update this.newReport;
                    this.errorMessage = 'We are currently unable to submit your request; the incident has been logged for investigation.';
                }
            }*/
        }catch(Exception e){
            system.debug('error == '+e);
            ApexPages.addMessages(e);
            this.errorMessage = e.getMessage();
        }
        return null;
    }
    
    public String userLang;
    //Returns the language selected from the language cookie
    public String getUserLang() {
        String tempLang;
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null)
            tempLang = tempCookie.getValue();            
        else{ tempLang = 'en_US';}
        
        //Added AU as special case Non supported and keeps original en_US labeling
        if(tempLang.contains('en'))
            userLang = 'en_US';    
        else if(tempLang.contains('de'))
            userLang = 'de';
        else if(tempLang.contains('fr'))
            userLang = 'fr';
        else if(tempLang.contains('it'))
            userLang = 'it';
        else if(tempLang.contains('es'))
            userLang = 'es';
        else if(tempLang.contains('sv'))
            userLang = 'sv';
        else if(tempLang.contains('nl'))
            userLang = 'nl_NL';
        else if(tempLang.contains('pt'))
            userLang = 'pt_BR';         
        
        return userLang;
    }
}