@isTest
public with sharing class PhantomRequest_test{
    
    @isTest static void test1_executeAllMethods(){
        PhantomRequest tstCls = new PhantomRequest();
        tstCls.getUserLang();
        
        Cookie counter = new Cookie('uLang', 'de',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{counter});
        tstCls.getUserLang();
        
        counter = new Cookie('uLang', 'fr',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{counter});
        tstCls.getUserLang();
        
        counter = new Cookie('uLang', 'it',null,-1,false);
        tstCls.getUserLang();
        ApexPages.currentPage().setCookies(new Cookie[]{counter});
        
        counter = new Cookie('uLang', 'es',null,-1,false);
        tstCls.getUserLang();
        ApexPages.currentPage().setCookies(new Cookie[]{counter});
        
        counter = new Cookie('uLang', 'sv',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{counter});
        tstCls.getUserLang();
        
        counter = new Cookie('uLang', 'nl',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{counter});
        tstCls.getUserLang();
        
        counter = new Cookie('uLang', 'pt',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{counter});
        tstCls.getUserLang();
        
        counter = new Cookie('uLang', 'es',null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{counter});
        tstCls.getUserLang();
        
        tstCls.pageInitAction();
        tstCls.getMultiPlayerAccounts();
        tstCls.getPrimaryIssueType();
        tstCls.exit();
        tstCls.acknowledgeError();
        tstCls.submit(); //should trip try/catch as it does a callout
    }
    
    @isTest 
    static void test2_inUserContext1(){
        account a = new account(name='test account');
        insert a;
        Profile OHVprof = [SELECT Id FROM Profile WHERE Name LIKE 'ATVI%OHV%' LIMIT 1];
        contact c1 = new contact(AccountId = a.Id, FirstName='test', lastName='1', email='test1@test.test');
       
        insert c1;
        user u1 = new user(ContactId = c1.Id, IsActive = true,
            lastName = '1',email=c1.email,ProfileId=OHVprof.Id,alias='t1',username=c1.email,communityNickname='1',
            TimeZoneSidKey='America/Los_Angeles',LocaleSidKey='en_US',EmailEncodingKey='ISO-8859-1',LanguageLocaleKey='en_US'
        );
        insert u1;
        
        system.runAs(u1){//test pulling the federationId from User onto contact, then no mpas
            test.startTest();
                PhantomRequest tstCls = new PhantomRequest();
                tstCls.pageInitAction();
                tstCls.submit();
            test.stopTest();
        }
        
         
    }
   @isTest 
    static void test2_inUserContext2(){
        account a = new account(name='test account');
        insert a;
        Profile OHVprof = [SELECT Id FROM Profile WHERE Name LIKE 'ATVI%OHV%' LIMIT 1];
        contact c2 = new contact(AccountId = a.Id, FirstName='test', lastName='2', email='test2@test.test', Birthdate = system.today());
        insert c2;
        user u2 = new user(ContactId = c2.Id, FederationIdentifier = '2', IsActive = true,
            lastName = '2',email=c2.email,ProfileId=OHVprof.Id,alias='t2',username=c2.email,communityNickname='2',
            TimeZoneSidKey='America/Los_Angeles',LocaleSidKey='en_US',EmailEncodingKey='ISO-8859-1',
            LanguageLocaleKey='en_US'
        );
        insert u2;
        Multiplayer_Account__c mpa2_xbl = new Multiplayer_Account__c(Contact__c = c2.Id, Platform__c = 'XBL', Gamertag__c = 'tstXBLgamerTag');
        Multiplayer_Account__c mpa2_psn = new Multiplayer_Account__c(Contact__c = c2.Id, Platform__c = 'PSN', Gamertag__c = 'tstPSNgamerTag');
        insert new Multiplayer_Account__c[]{mpa2_xbl, mpa2_psn};
        /*CRR_Code_Distribution_Recipient__c Recip = new CRR_Code_Distribution_Recipient__c();
        Recip.name = 'test';
        Recip.Available_for_Customer_Support__c = true;
        recip.Recipient__c = u2.id;
        insert Recip;
        CRR_Code_Distribution_Request__c CDR = new CRR_Code_Distribution_Request__c();
        CDR.Distribution_type__c = 'Customer Service';
        CDR.Code_Distribution_Recipient__c = Recip.id;
        insert CDR;
        CRR_Code__c code = new CRR_Code__c();
        code.code__c = '123';
        code.Code_Distribution_Request__c = CDR.id;
        insert code;*/


        system.runAs(u2){ //test pulling the federationId from User onto contact, then mpas
            test.startTest();
                PhantomRequest tstCls = new PhantomRequest();
                tstCls.pageInitAction();
                tstCls.getMultiplayerAccounts();
                tstCls.newReport.Multiplayer_Account__c = tstCls.getMultiplayerAccounts()[0].getValue();//XBL
                //tstcls.newReport.Issue__c = 'testing';
                tstCls.submit();
                tstCls.submit();
                tstCls.submit();
                tstCls.submit();
                //Stat_Corruption_Report__c testRec = [SELECT Name, LAS_GUID__c FROM Stat_Corruption_Report__c WHERE Id = :tstCls.newReport.Id];
                //system.assertEquals(integer.valueOf(testRec.Name)*-1, testRec.LAS_GUID__c);
            test.stopTest();
        }
    }
    
    @isTest 
    static void test2_inUserContext3(){
        account a = new account(name='test account');
        insert a;
        Profile OHVprof = [SELECT Id FROM Profile WHERE Name LIKE 'ATVI%OHV%' LIMIT 1];
        contact c3 = new contact(AccountId = a.Id, FirstName='test', lastName='3', email='test3@test.test', 
            UCDID__c = '3', Birthdate = system.today()
        );
        insert c3;
        user u3 = new user(ContactId = c3.Id, FederationIdentifier = '3', IsActive = true,
            lastName = '3',email=c3.email,ProfileId=OHVprof.Id,alias='t3',username=c3.email,communityNickname='3',
            TimeZoneSidKey='America/Los_Angeles',LocaleSidKey='en_US',EmailEncodingKey='ISO-8859-1',
            LanguageLocaleKey='en_US'
        );
        insert u3;
        Multiplayer_Account__c mpa3_xbl = new Multiplayer_Account__c(Contact__c = c3.Id, Platform__c = 'XBL', Gamertag__c = 'tstXBLgamerTag');
        Multiplayer_Account__c mpa3_psn = new Multiplayer_Account__c(Contact__c = c3.Id, Platform__c = 'PSN', Gamertag__c = 'tstPSNgamerTag');
        insert new Multiplayer_Account__c[]{mpa3_xbl, mpa3_psn};
        system.runAs(u3){ //test success (user with UCDID & MPAs)
            test.startTest();
                PhantomRequest tstCls = new PhantomRequest();
                tstCls.pageInitAction();
                tstCls.getMultiplayerAccounts();
                tstCls.newReport.Multiplayer_Account__c = tstCls.getMultiplayerAccounts()[1].getValue();//PSN
                //tstcls.newReport.Issue__c = 'multi';
                tstCls.submit();
                //system.assertEquals(3, tstCls.newReport.LAS_GUID__c);
            test.stopTest();
        } 
    }

}