public class Phone{
    public string msg{get;set;}
    public contact currContact{get;set;}
    public string userLanguage{get;set;}
    public case currentCase{get;set;}
    public user tempUser{get;set;}
    public Boolean showWholeForm{get;set;}
    public Boolean showGTform{get;set;}
    public Boolean showConfirm{get;set;}
    public Boolean userIsAuthenticated{get;set;}
    public Boolean preRender{get;set;}
    
    public string CaseNoConfirm{get;set;}
    
    public string user{get;set;}
    public string gamertag{get;set;}
    public string platform{get;set;}
    public string game{get;set;}
    public string mainIssue{get;set;}
    public string subIssue{get;set;}
    public string nature{get;set;}
    
    public contact_us_controller cuc{get;set;}
    public list<webtocase__c> w2c{get;set;}
    public list<webtocase_platform__c> pf{get;set;}
    public list<webtocase_gametitle__c> gtitle{get;set;}
    public list<webtocase_type__c> issueType{get;set;}
    public list<webtocase_subtype__c> subType{get;set;}
    
    public selectOption[] results{get;set;}
    public list<selectoption> ConsolePicklist{get;set;}
    public list<selectoption> GamePicklist{get;set;}
    public list<selectoption> MainPicklist{get;set;}
    public list<selectoption> SubPicklist{get;set;}
    
    public webtocase_platform__c oplatform{get;set;}
    public webtocase_gametitle__c ogame{get;set;}
    public webtocase_type__c omainIssue{get;set;}
    public webtocase_subtype__c osubIssue{get;set;}
    
    map<Id, Multiplayer_Account__c> mpaId2mpa = new map<Id, Multiplayer_Account__c>();
    public Phone(ATVICustomerPortalController acpc){//this constructor necessary to avoid using the desktop only template
        //since Tim R. has dictated that CS's initiatives must be mobile ready
        this(); //call the original constructor, 
    }
    public Phone(){
        userLanguage = 'en_US';
        //Get User Language:
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            userLanguage = tempCookie.getValue();            
        }
        
        userIsAuthenticated = UserInfo.getUserType().equalsIgnoreCase('CSPLitePortal');
        if(userIsAuthenticated){
            showWholeForm = true;
            showConfirm = false;
            prerender = false;
            
            if(apexpages.currentpage().getparameters().containskey('platform')){
                platform = apexpages.currentpage().getparameters().get('platform');//id
                game = apexpages.currentpage().getparameters().get('game');//id
                mainissue = apexpages.currentpage().getparameters().get('type');//id
                subissue = apexpages.currentpage().getparameters().get('subtype');//id
                
                preRender = true;
                
                tempUser = [SELECT id,name, ContactId, Contact.UCDID__c,Contact.email, FederationIdentifier FROM User WHERE Id = :UserInfo.getUserId()];
                //getResults();
                
                oplatform = [select id,name__c from webtocase_platform__c where id=:platform limit 1];
                platform = oplatform.Name__c;
                
                ogame = [select id,title__c,public_product__c from webtocase_gametitle__c where id=:game limit 1];
                game = ogame.title__c;
                
                omainIssue = [select id,description__c, typemap__c from webtocase_type__c where id=:mainIssue limit 1];
                mainIssue = omainIssue.description__c;
                
                osubIssue = [select id,description__c, subtypemap__c from webtocase_subtype__c where id=:subIssue limit 1];
                subIssue = osubIssue.description__c;
            }
            else{
                ConsolePicklist = new list<selectoption>();
                GamePicklist = new list<selectoption>();
                MainPicklist = new list<selectoption>();
                SubPicklist = new list<selectoption>();
                
                ConsolePicklist.add(new selectOption('','--None--'));
                GamePicklist.add(new selectOption('','--None--'));
                MainPicklist.add(new selectOption('','--None--'));
                SubPicklist.add(new selectOption('','--None--'));
                
                w2c = [select id from webtocase__c where language__c =: userLanguage];
                pf = [select id,name,Name__c,image__c from webtocase_platform__c where webtocase__c=:w2c[0].id AND IsActive__c = true ORDER BY Sort_Order__c, Name];     
                
                //ConsolePicklist.add(new selectOption('','--None--'));
                for(webtocase_platform__c i:pf){
                    ConsolePicklist.add(new selectOption(i.id,i.Name__c));
                }
                
                tempUser = [SELECT id, name, ContactId, Contact.UCDID__c,Contact.email, FederationIdentifier FROM User WHERE Id = :UserInfo.getUserId()];
                //getResults();
            }
        }
        else{
            showWholeForm = false;
        }
    }
    
    public string platformid{get;set;}
    public pageReference getGamePicklist(){
        reset(3);
        gtitle = [select id,title__c from webtocase_gametitle__c where webtoCase_platform__c=:platformid AND IsActive__c = true ORDER BY Priority__c, Title__c];
        for(webtocase_gametitle__c i:gtitle){
            gamePicklist.add(new selectOption(i.id,i.title__c));
        }
        return null;
    }
    
    public string gameTitleid{get;set;}
    public pageReference getMainPicklist(){
        reset(2);
        issueType = [select id,description__c, typemap__c from webtocase_type__c where webtocase_gametitle__c=: gameTitleid AND IsActive__c = true ORDER BY Sort_Position__c, Description__c];
        for(webtocase_type__c i:issueType){
            mainPicklist.add(new selectOption(i.id,i.description__c));
        }
        return null;
    }
    
    public string mainIssueid{get;set;}
    public pageReference getSubPicklist(){
        reset(1);
        subType = [select id,description__c,subtypemap__c from webtocase_subtype__c where webtocase_type__c=: mainIssueid  AND IsActive__c = true ORDER BY Sort_Position__c, Description__c];
        for(webtocase_subtype__c i:subType){
            subPicklist.add(new selectOption(i.id,i.description__c));
        }
        return null;
    }
    public string subIssueid{get;set;}
    public void reset(integer i){
        if(i==3){
            gamePicklist.clear();
            GamePicklist.add(new selectOption('','--None--'));
            mainPickList.clear();
            MainPicklist.add(new selectOption('','--None--'));
            SubPicklist.clear();
            SubPicklist.add(new selectOption('','--None--'));
        }
        else if(i==2){
            mainPickList.clear();
            MainPicklist.add(new selectOption('','--None--'));
            SubPicklist.clear();
            SubPicklist.add(new selectOption('','--None--'));
        }
        else if(i==2){
            SubPicklist.clear();
            SubPicklist.add(new selectOption('','--None--'));
        }
        
    }
    public boolean isBlankOrNull(String s){ return s == '' || s == null; }
    public pagereference submit(){
        try{
            If(!preRender){
                oplatform = [select id,name__c from webtocase_platform__c where id=:platform limit 1];
                platform = oplatform.Name__c;
                
                ogame = [select id,title__c,public_product__c from webtocase_gametitle__c where id=:game limit 1];
                game = ogame.title__c;
                
                omainIssue = [select id,description__c, typemap__c from webtocase_type__c where id=:mainIssue limit 1];
                mainIssue = omainIssue.description__c;
                
                osubIssue = [select id,description__c, subtypemap__c from webtocase_subtype__c where id=:subIssue limit 1];
                subIssue = osubIssue.description__c;
            }
            //validate required fields
            if(isBlankOrNull(gamertag) ||
               isBlankOrNull(platform) ||
               isBlankOrNull(game) ||
               isBlankOrNull(mainIssue) ||
               isBlankOrNull(subIssue) || 
               isBlankOrNull(nature)
            ){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, 'All fields required'));
                return null;
            }
            
            currentCase = new Case();
            RecordType generalSupportType = [SELECT id FROM RecordType WHERE Name = 'General Support' LIMIT 1];
            currentCase.RecordTypeId = generalSupportType.id;
            currentCase.ContactId = tempUser.contactid;
            currentCase.Type = omainIssue.typemap__c; //Derive Type
            currentCase.Sub_Type__c = osubIssue.SubTypeMap__c; //Derive Sub Type
            currentCase.Status = 'Open'; 
            currentCase.Product_Effected__c = ogame.Public_Product__c;//get public product id:selectedTitle.Public_Product__r.id
            currentCase.W2C_Origin_Language__c = 'en_US';//userLanguage; Get Language
            currentCase.W2C_Type__c = mainIssue;
            currentCase.W2C_SubType__c = subIssue;
            currentCase.subject =  game + ' (' + Platform + ')' + ' : ' + mainIssue + ' (' + subIssue + ')';

            currentCase.description = 'Gamertag:'+gamertag+'\nNature:'+nature;
            currentCase.Origin = 'Customer Created Phone Ticket';
            currentCase.Comment_not_required__c = true;
            currentCase.Case_Not_Editable_by_Gamer__c = true;
            currentCase.Medallia_Survey_Not_Required__c = true;
            try{
            currentCase.Estimated_Phone_Wait_Time__c = ApexCodeSetting__c.getValues('InContact_Average_Hold_Time').Number__c;
            }catch(Exception e){}
            currentCase = addDispositionsToCase(
                currentCase, 
                platform,
                game,
                omainIssue.typemap__c,
                osubIssue.SubTypeMap__c
            );
            insert currentCase;
            
            currentCase = [select id,caseNumber from case where id=:currentCase.id limit 1];
            showConfirm = true;
            Casenoconfirm = currentcase.CaseNumber;
        }catch(Exception e){
            //ApexPages.addMessages(e);
            system.debug('ERROR == '+e);
        }
        return null;
    }
    
    public pagereference freeGT(){
        if(gamertag==''){
            showgtform = true;
        }
        return null;
    }
    
    public case addDispositionsToCase(Case pCase, String pPlat, String pProd, String pItyp, String pIStyp){
        pCase.Platform__c = pPlat;
        if(new set<String>{'Wii','Wii U','DS','3DS','64','Gamecube'}.contains(pPlat)){
            pCase.Platform__c = 'Nintendo '+pPlat;
        }    
        pCase.Product_L1__c = pProd;
        pCase.Issue_Type__c = pItyp;
        pCase.Issue_Sub_Type__c = pIStyp;
        return pCase;
    }
    
    public void getResults(){
        if(tempUser.ContactId != null){
            if((tempUser.Contact.UCDID__c == null || tempUser.Contact.UCDID__c == '') && 
               (tempUser.FederationIdentifier != null && tempUser.FederationIdentifier != ''))
            {
                try{
                    update new Contact(Id = tempUser.ContactId, UCDID__c = tempUser.FederationIdentifier);
                }catch(Exception e){
                    //will fail if contact.birthdate is empty, contact.birthdate is empty when TIBCO hasn't updated yet
                }
            }
            Linked_Accounts_Redirect_Controller larc = new Linked_Accounts_Redirect_Controller
                (
                    new ApexPages.standardController(new Contact(Id = tempUser.ContactId))
                );
            if(!test.isRunningTest()){
                larc.updateProfile();
            }
        }
        
        showGTForm = false;
        results = new selectOption[]{};
            if(tempUser.ContactId != null){
                mpaId2mpa = new map<Id, Multiplayer_Account__c>([SELECT Id, Platform__c, Gamertag__c FROM Multiplayer_Account__c WHERE Contact__c = :tempUser.ContactId]);
                system.debug('Multiplayer Account Map:' + this.mpaID2mpa);
                results.add(new selectOption('','--None--'));
                for(Id mpaId : mpaId2mpa.keySet()){
                    //results.add(new selectOption(mpaId, this.mpaId2mpa.get(mpaId).Gamertag__c+' ('+this.mpaId2mpa.get(mpaId).Platform__c+')'));
                    results.add(new selectOption(this.mpaId2mpa.get(mpaId).Gamertag__c, this.mpaId2mpa.get(mpaId).Gamertag__c+' ('+this.mpaId2mpa.get(mpaId).Platform__c+')'));
                }
                results.add(new selectOption('','Other'));
            }
        if(results.size() == 1){
            showGTForm = true;
        }
    }
    public string getArticleLang(){
        return getArticleLang(userLanguage);
    }
    public static string getArticleLang(String uLang){
        string result = 'en_US';
        try{
            if(uLang.containsIgnoreCase('fr')) result = 'fr';
            else if(uLang.containsIgnoreCase('de')) result = 'de';
            else if(uLang.containsIgnoreCase('it')) result = 'it';
            else if(uLang.containsIgnoreCase('pt')) result = 'pt_BR';
            else if(uLang.containsIgnoreCase('sv')) result = 'sv';
            else if(uLang.containsIgnoreCase('nl')) result = 'nl_NL';
            else if(uLang.containsIgnoreCase('es')) result = 'es';
        }catch(Exception e){
            system.debug('error == '+e);
        }
        return result;
    }
}