public class Phone_Utils{
    public Phone_Utils(){}
    public Phone_Utils(ApexPages.standardController cont){}
    public Phone_Utils(Phone pCls){}
    public Phone_Utils(ATVICustomerPortalController acpcCls){}

    String pCustLang = '';
    public void setlCustLang(String s){ //takes the language from the Phone.cls
        this.pCustLang = s; //stores it for use on the page to translate the labels
        this.articleLang = Phone.getArticleLang(s); //convert to article URL format for the link
    }
    public string getlCustLang(){
        return this.pCustLang;
    } 
    string articleLang = 'en_US';
    public string getArtLang(){
        return articleLang;
    }
    public static boolean getIsTicketingSystemActive(){
        Boolean isActive = false;
        try{
            isActive = ApexCodeSetting__c.getValues('PhoneTicketingSystem').IsActive__c;
        }catch(Exception e){
        }
        return isActive;
    }
    public static string getCurrentEstimatedWaitTime(){
        String result = 'Unknown';
        try{
            Decimal waitTimeSeconds = ApexCodeSetting__c.getValues('InContact_Average_Hold_Time').Number__c;
            result = String.valueOf(Math.ceil(waitTimeSeconds / 60.0));
        }catch(Exception e){
        }
        return result;
    }
    public static boolean getIsMobile(){
        String userAgent = '';
        try{
            userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
            return userAgent.containsIgnoreCase('mobi'); //userAgent can be null
        }catch(Exception e){
            return false;
        }
    }
}