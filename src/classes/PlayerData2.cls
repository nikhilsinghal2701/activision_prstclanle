public class PlayerData2{
    
    public string gamertag{get;set;}
    public string console{get;set;}
    public string Endpoint{get;set;}
    public string resp{get;set;}
    public boolean showError{get;set;}
    public ghostsPlayerData player {get;set;}
    public ResetStat.backupRecord[] bRecs {get;set;}
    
   public void getResponse(){
        showError = false;
        string query ;
        if(! isBlankOrNull(gamerTag) && ! isBlankOrNull(console)){
            if(gamertag.contains('STEAM_')){
                gamertag = gamertag.trim();            
                string[] parts = gamertag.split(':');
                long V = 76561197960265728L;
                long Y = long.valueOf( parts[ 1 ] );
                long Z = long.valueOf( parts[ 2 ] );
                long W = Z * 2 + V + Y;
                system.debug('w == '+w);
                gamertag = string.valueof(W);
            }
            DwProxy.service serv = new dwProxy.service();
            serv.timeout_x = 120000;
             if(Endpoint=='Ghosts') query = 'https://contra.infinityward.net/svc/ghosts/'+(UserInfo.getOrganizationId() == '00DU0000000HMgwMAG' ? 'live' : 'dev')+'/'+console.trim()+'/'+gamertag.trim()+'/';
            else query = 'http://shg-gavel.activision.com/svc/blacksmith/'+(UserInfo.getOrganizationId() == '00DU0000000HMgwMAG' ? 'live' : 'dev')+'/'+console.trim()+'/'+gamertag.trim();

            //query += UserInfo.getOrganizationId() == '00DU0000000HMgwMAG' ? 'live' : 'dev';
            //query += '/'+console.trim()+'/'+gamertag.trim();//+type;
            String qSuffix = '@headers-exist@@usecsnetworkcreds#true';
            try{
                 system.debug('>>>>'+query+qSuffix);
                resp = serv.passEncryptedGetData(query+qSuffix);
                system.debug(resp);
                try{
                    player = (ghostsPlayerData) JSON.deserialize(resp, ghostsPlayerData.class);
                    if(player!=null && !player.IsValidGamertag ){
                         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Not a valid gamer tag');
                         ApexPages.addMessage(myMsg);
                    }
                    try{
                        resp = serv.passEncryptedGetData(query+'/getBackupRecords'+qSuffix);
                        bRecs = (ResetStat.backupRecord[]) JSON.deserialize(resp, list<ResetStat.backupRecord>.class);
                    }catch(Exception e){
                        system.debug(showError +'ERROR ==== '+e);
                         bRecs = new ResetStat.backupRecord[]{};
                    }
                }catch(exception e){
                    system.debug('ERROR ==== '+e);
                    player = new ghostsPlayerData();
                }
            }catch(Exception e){system.debug(showError +'ERROR ==== '+e);resp = e.getMessage();showError = true;}
        }else{
            resp = 'A value must be specified for both Gamertag and Console.';
            showError = true;
        }
    }


    public boolean isBlankOrNull(String s){ return s == null || s == ''; }
    
    public static selectOption[] getConsolePicklist(){
        return new selectOption[]{
            new selectOption('','--None--')
            ,new selectOption('ps4','PS4')
            ,new selectOption('xboxone','XB1')
            ,new selectOption('ps3','PS3')
            ,new selectOption('xbox','Xbox360')
            ,new selectOption('pc','PC')
            //,new selectOption('wiiu','Wii U')
        };
    }
    
     public static selectOption[] getEndpointPicklist(){
        return new selectOption[]{
            new selectOption('','--None--'),
            new selectOption('Ghosts','Ghosts'),
            new selectOption('Advanced Warfare','Advanced Warfare')
            //,new selectOption('wiiu','Wii U')
        };
    }
       
}