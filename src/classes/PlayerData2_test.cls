@isTest
public class PlayerData2_test {

    public static testMethod void doTest2(){
        PlayerData2 tstCls = new PlayerData2();
        tstCls.getResponse(); //console & gamertag are blank
        tstCls.gamerTag = 'STEAM_7656119796:7656119796:7656119796';
        tstCls.console = 'asdf';
        
        String testData = '{"Bans":[],"ConnectionType":"wired","Gamertag":"sychophant","GamesPlayed":109,"LastLoginTime":1390454940,';
        testData += '"Prestige":0,"Rank":16,"SquadMembers":[{"Name":"LeFeuvre ber 1","Rank":16},{"Name":"Barnett mber 2","Rank":1},';
        testData += '{"Name":"Carani ember 3","Rank":5},{"Name":"Sanchez mber 5","Rank":1}],"SquadPoints":28,"TimePlayed":"0d 4h 9m ';
        testData += '45s"}';
        
        Test.setMock(WebServiceMock.class, new Gamer_Ban_MockCallout (testData));
        
        test.startTest(); 
        PlayerData2.getConsolePicklist();
       PlayerData2.getEndpointPicklist();
            tstCls.getResponse();
       
        
        testData = '{"Bans":[],"ConnectionType":"wired","IsValidGamertag":false ,"Gamertag":"","GamesPlayed":109,"LastLoginTime":1390454940,';
        testData += '"Prestige":0,"Rank":16,"SquadMembers":[{"Name":"LeFeuvre ber 1","Rank":16},{"Name":"Barnett mber 2","Rank":1},';
        testData += '{"Name":"Carani ember 3","Rank":5},{"Name":"Sanchez mber 5","Rank":1}],"SquadPoints":28,"TimePlayed":"0d 4h 9m ';
        testData += '45s"}';
        
        Test.setMock(WebServiceMock.class, new Gamer_Ban_MockCallout (testData));
        PlayerData2.getConsolePicklist();

            tstCls.getResponse();
        
        
        Test.setMock(WebServiceMock.class, new Gamer_Ban_MockCallout (''));
        PlayerData2.getConsolePicklist();

            tstCls.getResponse();    
            
        test.stopTest();
        
    }
    
}