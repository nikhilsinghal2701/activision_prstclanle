public class PostStreamController{
    public string caseid{get;set;}
    public string currentURL{get;set;}
    public list<socialpost> thePosts{get;set;}
    public list<case> theCases{get;set;}
    public List<case> checkCases{get;set;}
    public string alert{get;set;} 
    public integer count{get;set;}
    public String releaseId{get;set;}
    public case releaseCase{get;set;}
    public String hideId{get;set;}
    public case hideCase{get;set;}
    public String[] openStatus = new String[]{'Open','On Hold','Escalated','New','Awaiting Response'};
            
    
    public PostStreamController(){
        //String[] openStatus = new String[]{'Open','On Hold','Escalated','New'};
        count=0;
        alert='true';
        currentURL = ApexPages.currentPage().getUrl();
        caseid = ApexPages.currentPage().getParameters().get('id');
        //thePosts = [select id, name, Content, PersonaID, Persona.name, ParentID from socialpost order by createddate desc limit 20];
        theCases = [select id, casenumber, subject, lastmodifieddate, (select content, posted from Posts where R6Service__IsOutbound__c=false order by lastmodifieddate desc limit 1) from case where ownerid=:userinfo.getUserId() and has_gamer_commented__c=true and status in :openStatus and social_hide__c=false];
        count = theCases.size();
    }
    
    public pagereference refresh(){
        //thePosts = [select id, name, Content, PersonaID, Persona.name, ParentID from socialpost order by createddate desc limit 20];
        checkCases = [select id, casenumber, subject, lastmodifieddate, (select content, posted from Posts where R6Service__IsOutbound__c=false order by lastmodifieddate desc limit 1) from case where ownerid=:userinfo.getUserId() and has_gamer_commented__c=true and status in :openStatus and social_hide__c=false];
        count=checkCases.size();
        if(checkCases != theCases){
            alert='true';
            theCases=checkCases;
        }
        else{
            alert='false';
        }
        return null;
    }
    
    public pagereference release(){
        system.debug('caseID: ' + caseId);
        releaseCase = [select id from case where id=:releaseId limit 1];
        if (releaseCase != null){
            system.debug('releasecaseID: ' + releaseId);
            list<group> queues = [select id from group where name='Radian6 Social' and type='Queue' limit 1];
            string queueID = queues[0].id;
            releaseCase.ownerid = queueID;
            update releaseCase;
        }
        return null;
    }
    
    public pagereference hide(){
        hideCase = [select id from case where id=:hideId limit 1];
        if (hideCase != null){
            hideCase.social_hide__c = true;
            update hideCase;
        }
        return null;
    }
}