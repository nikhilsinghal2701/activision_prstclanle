public without sharing class RankReset{
    public stat_corruption_report__c scr{get;set;}
    public user curUser{get;set;}
    public Boolean userIsAuthenticated{get;set;}
    map<Id, Multiplayer_Account__c> mpaId2mpa = new map<Id, Multiplayer_Account__c>();
    public Boolean linkedAccountsPresent{get;set;}
    public selectOption[] MultiPlayerAccounts{get;set;}
    public boolean showError{get;set;}
    public string error{get;set;}
    public string checkReason{get;set;}
    public Boolean showResetConfirm{get;set;}
    public Boolean showUnqualified{get;set;}
    public Boolean showConfirm{get;set;}
    public Boolean showInfo{get;set;}
    public Boolean showGTunqualified{get;set;}
    public Boolean showBeenReset{get;set;}
    public string endpoint {get; set;}
    public string console{get;set;}
    public string resp{get; set;}
    public string platform{get;set;}
    public string userLanguage{get;set;}
    public Boolean acceptTos{get;set;}
    public Boolean showSubmit{get;set;}
    public case CurrentCase{get;set;}
    public boolean isProd = UserInfo.getOrganizationId() == '00DU0000000HMgwMAG';
    
    public RankReset(ATVICustomerPortalController acpc){//this constructor necessary to avoid using the desktop only template
        //since Tim R. has dictated that CS's initiatives must be mobile ready
        this(); //call the original constructor, 
    }
    
    public RankReset(){
        
        userLanguage = 'en_US';
        //Get User Language:
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            userLanguage = tempCookie.getValue();            
        }
        currentCase = new case();
        showSubmit = false;
        showInfo = true;
        showResetConfirm = false;
        showUnqualified = false;
        showBeenReset = false;
        showConfirm = false;
        scr = new stat_corruption_report__c();
        error = '';
        checkReason='';
        showError = false;
        linkedAccountsPresent = false;
        //endpoint = 'https://contra.infinityward.net/svc/ghosts/live/';
        endpoint = 'https://contra.infinityward.net/svc/ghosts/dev/';
        if(isProd) endpoint = 'https://contra.infinityward.net/svc/ghosts/live/';
        
        
        curUser = [SELECT Id, Tier__c, CompanyName, Division, Profile.Name, Contact.id, Contact.UCDID__c, FederationIdentifier, name FROM User WHERE Id = :UserInfo.getUserId()];
        userIsAuthenticated = UserInfo.getUserType().equalsIgnoreCase('CSPLitePortal');
        if(userIsAuthenticated){
            if(curUser.contact.id != null){
                MultiPlayerAccounts = new selectOption[]{};
                    MultiplayerAccounts.add(new selectOption('','--None--'));
                this.mpaId2mpa = new map<Id, Multiplayer_Account__c>([SELECT Id, Platform__c, Gamertag__c FROM Multiplayer_Account__c WHERE Contact__c = :curUser.contact.id and is_current__c=true]);
                for(Id mpaId : this.mpaId2mpa.keySet()){
                    MultiPlayerAccounts.add(new selectOption(this.mpaId2mpa.get(mpaId).Gamertag__c, this.mpaId2mpa.get(mpaId).Gamertag__c+' ('+this.mpaId2mpa.get(mpaId).Platform__c+')'));
                    this.linkedAccountsPresent = true;
                }
            }
        }
        
    }
    
    public static void updateMP(){
        user u = [SELECT Id, Contact.Id, Contact.UCDID__c, FederationIdentifier FROM User WHERE Id =:userinfo.getUserId() limit 1];
        system.debug('user:'+u);
        if(UserInfo.getUserType().equalsIgnoreCase('CSPLitePortal')){
            if(u.contact.id != null){
                Linked_Accounts_Redirect_Controller larc = new Linked_Accounts_Redirect_Controller
                    (
                        new ApexPages.standardController(new Contact(Id = u.contact.id))
                    );
                larc.updateProfile();
            }
        }
    }
    
    public pagereference checkFreq(){
        error='';
        showGTunqualified = false;
        system.debug('console'+scr.console__c);
        system.debug('curUser'+curUser.id);
        
        if(console != ''){
            list<stat_corruption_report__c> checkSCR = [select id from stat_corruption_report__c where createddate = last_n_days:30 and console__c=:console and user__c=:curUser.id];
            if(checkSCR.size()>0){
                showGTunqualified = true;
            }
            system.debug('checkSCR:'+string.valueof(checkSCR.size()));            
        }
        return null;
    }
    
    public pagereference insertSCR(){
        error='';
        showerror=false;
        
        if(missingData()){
            showError = true;
            error = 'Error: All fields required or incorrect Gamertag/PSN selection';
            return null;
        }
        else{        
            /*Web Callout*/
            string resp;
            DwProxy.service serv = new dwProxy.service();
            serv.timeout_x = 120000;
            //string query = 'https://contra.infinityward.net/svc/ghosts/live';
            string query = 'https://contra.infinityward.net/svc/ghosts/dev';
            if(isProd) query = 'https://contra.infinityward.net/svc/ghosts/live/';
            query += '/'+console.trim()+'/'+scr.handle__c.trim()+'/selfServiceResetRequest';//+type;
            String qSuffix = '@headers-exist@@usecsnetworkcreds#true';
            system.debug('qSuffix:' + query+qsuffix);
            if(!test.isRunningTest()){
            resp = serv.passEncryptedGetData(query+qSuffix);
                }
            else{
                resp='1';
            }
            if(resp=='0'){
                checkReason = 'Unqualified';
            }
            else if(resp=='1'){
                checkReason = 'Eligible';
            }
            else if(resp=='2'){
                checkReason = 'MaxPrestige';
            }
            else if(resp=='4'){
                checkReason = 'BeenReset';
            }
            else {
                checkReason = 'Error';
            }
            system.debug('checkReason:'+checkReason);
            //case currentCase = new case();
            currentCase.Subject = 'Rank Reset Request';
            RecordType generalSupportType = [SELECT id FROM RecordType WHERE Name = 'General Support' LIMIT 1];
            currentCase.RecordTypeId = generalSupportType.id;
            currentCase.ContactId = curUser.contactid;
            //currentCase.Type = omainIssue.typemap__c; //Derive Type
            //currentCase.Sub_Type__c = osubIssue.SubTypeMap__c; //Derive Sub Type
            currentCase.Status = 'Open'; 
            //currentCase.Product_Effected__c = 'a0UK0000002CuGj';//get public product id:selectedTitle.Public_Product__r.id
            currentCase.Product_L1__c = 'Call of Duty: Ghosts';
            currentCase.Sub_Product_DLC__c = 'Core Game';
            currentCase.Issue_Type__c = 'Game-Multiplayer';
            currentCase.Issue_Sub_Type__c = 'Stats/Rank';
            currentCase.L3__c = 'Player Level Corrupted/Reset';
            system.debug('console:'+console);
            system.debug('scr.console__c:'+scr.Console__c);
            //currentCase.Platform__c = 'Unknown';
            currentCase.Platform__c = convertDispConsole(console);
            currentCase.W2C_Origin_Language__c = 'en_US';//userLanguage; Get Language 
            //currentCase.W2C_Type__c = mainIssue;
            //currentCase.W2C_SubType__c = subIssue;
            currentCase.Origin = 'Rank Reset Self Service';
            currentCase.Comment_not_required__c = true;
            currentCase.Case_Not_Editable_by_Gamer__c = true;
            currentCase.Medallia_Survey_Not_Required__c = true;//Maybe on?
            currentCase.OwnerId = [SELECT Id FROM Group WHERE Name = 'Rank Issues'].Id;
            //currentCase.Status = 'Escalated';
            
            //insert currentCase;*****Move THIS******
            
            scr.mode__c = scr.mode__c.replaceAll( '\\s+', '');
            scr.map__c = scr.map__c.replaceAll( '\\s+', '');
            scr.Status__c = 'OPEN';
            //scr.case__c = currentcase.id;
            scr.contact__c = curUser.contact.id;
            //scr.Issue__c = scr.SCR_Issue__c;
            scr.Comments__c = scr.Comments__c;
            scr.User__c = curUser.id;
            scr.Date__c = date.today();
            scr.selfservice__c = true;
            convertconsole(Console);
            //insert SCR;*****Move THIS******
            showInfo = false;
            if(checkReason=='Eligible'){
                //showResetConfirm = true;
                //sendtoIW();
                //showConfirm = true;
            }
            else if(checkReason=='MaxPrestige' || checkReason=='Unqualified'){
                showUnqualified = true;
                currentCase.Status = 'Resolved';                    
                insert currentCase;
                scr.case__c = currentcase.id;
                scr.status__c = 'Denied';
                scr.Issue__c = 'Your request for reversion has been denied.';
            	insert SCR;
                return null;
            }
            else if(checkReason=='BeenReset'){
                showBeenReset = true;
                currentCase.Status = 'Resolved';
                insert currentCase;
                scr.Status__c = 'IWR';
                scr.case__c = currentcase.id;
                scr.Issue__c = 'Stats reset due to corruption';
            	insert SCR;
                return null;
            }
            //return null;****KEEP AN EYE*****
        }   
    //}*****
    
    //public pagereference sendToIW(){

        //string resetok = scr.reset_ok__c ? '1' : '0';
        if(checkReason=='Eligible'){
        scr.reset_ok__c = true;
        string resetok = '1';
        DwProxy.service serv = new dwProxy.service();
        serv.timeout_x = 120000;
        
        string query = endpoint + scr.console__c + '/';
        query += 'submitResetRequest';
        query += '/'+string.valueof(date.today())+'/';
        query += resetok+'?';
        query += 'gt='+EncodingUtil.URLENCODE(scr.handle__c,'UTF-8')+'&';
        if(! isBlankOrNull(scr.comments__c)){
            query += 'desc='+EncodingUtil.URLENCODE(scr.comments__c, 'UTF-8')+'&';
        }
        query += 'md='+EncodingUtil.URLENCODE(scr.mode__c,'UTF-8')+'&';
        query += 'mp='+EncodingUtil.URLENCODE(scr.map__c,'UTF-8')+'&';
        //query += 'cn='+EncodingUtil.URLENCODE(scr.connection__c,'UTF-8')+'&';
        query += 'cn=wired'+'&';
        query += 'pr='+string.valueof(scr.previous_rank__c)+'&';
        //query += 'cr='+string.valueof(scr.rank__c);
        //query += 'cr='+string.valueof('0');
        system.debug('Query Before IF:'+query);
        if(scr.previous_prestige__c != null){//number field, no URLencoding
            query += '&pp='+scr.previous_prestige__c;
        }
        if(scr.current_prestige__c != null){//number field, no URLencoding
            query += '&cp='+scr.current_prestige__c;
        }
        
        system.debug(query);
        query += '@headers-exist@@usecsnetworkcreds#true';
        
        if(!test.isRunningTest()){
        resp = serv.passEncryptedGetData(query);
            insert currentCase;
            scr.case__c = currentcase.id;
            insert SCR;
            }
        else{
            resp='123';
            insert currentCase;
            scr.case__c = currentcase.id;
            insert SCR;
        }
        
        if (resp.left(1) == '<'){
            showError=true;
            //error = 'Record not found: Please verify platform and gamertags are correct.';
            error = resp;
        }else{
            showError=false;
            scr.dbid__c = integer.valueof(resp);
            scr.user__c = UserInfo.getUserId();
            scr.Submitted_Date_Time__c = datetime.now();
            scr.Status__c = 'Submitted';
            update scr;
            
            list<case_comment__c> cmt = [select id from case_comment__c where case__c=:scr.case__c];
            if(cmt.size()==0){
                insert new case_comment__c(
                    case__c = scr.case__c,
                    comments__c = 'No Rank Reset Comment'
                );
            }
            
            update new Case(
                Id = scr.case__c,
                Status = 'On hold'
            );
            showResetConfirm = false;
            showConfirm = true;
            return null;
            //return new ApexPages.standardController(scr).view();
        }  
        }

        return null;
    }
    
    public pagereference updateGTPicklist(){
        system.debug('updateGTPiclist(console):'+console);
        if(console!=''&&console!=null){
        selectoption[] tempSO = new selectoption[]{};
            if(console.contains('xbox')){
                
                for(multiplayer_account__c mp: mpaId2mpa.values()){
                    if(mp.platform__c=='XBL'){
                        tempSO.add(new selectoption(mp.Gamertag__c,mp.Gamertag__c+' ('+mp.Platform__c+')'));
                    }
                }
                multiplayerAccounts = tempSO;  
            }
        else if(console.contains('ps')){
            for(multiplayer_account__c mp: mpaId2mpa.values()){
                if(mp.platform__c=='PSN'){
                    tempSO.add(new selectoption(mp.Gamertag__c,mp.Gamertag__c+' ('+mp.Platform__c+')'));
                }
            }
            multiplayerAccounts = tempSO;
        }
            }
        else{
            multiplayerAccounts = new selectoption[]{new selectoption('','--None--')};
        }
        return null;
    }
    
    public selectOption[] getConsolePicklist(){
        return new selectOption[]{
            new selectOption('','--None--')
            ,new selectOption('ps4','PS4')
            ,new selectOption('xboxone','XB1')
            ,new selectOption('ps3','PS3')
            ,new selectOption('xbox','Xbox360')
            //,new selectOption('pc','PC')
            //,new selectOption('wiiu','Wii U')
        };
    }
    public void convertConsole(String val){
        if(val == null) val = '';
        val = val.toLowerCase();
        if(val == 'playstation 3'){val='ps3';}
        else if(val == 'playstation 4'){val='ps4';}
        else if(val == 'xbox 360'){val='xbox';}
        else if(val == 'xbox one'){val='xboxone';}
        else if(val == 'nintendo wii u'){val='wiiu';}
        scr.Console__c = val;
    }
    public string convertDispConsole(string val){
        if(val == null) val = '';
        val = val.toLowerCase();
        if(val == 'ps3'){val='Playstation 3';}
        else if(val == 'ps4'){val='Playstation 4';}
        else if(val == 'xbox'){val='Xbox 360';}
        else if(val == 'xboxone'){val='Xbox One';}
        else if(val == 'wiiu'){val='Nintendo Wii U';}
        return val;
    }
    
    public boolean isBlankOrNull(String s){ return s == '' || s == null; }
    
    public boolean missingData(){
        system.debug(scr.Previous_Rank__c);
        system.debug(scr.Previous_Prestige__c);
        system.debug(scr.Map__c);
        system.debug(scr.Mode__c);
        system.debug(scr.Comments__c);
        return isBlankOrNull(scr.handle__c) || 
            //scr.date__c == null || 
            scr.previous_rank__c == null || 
            //scr.rank__c == null || 
            //scr.current_prestige__c == null ||
            scr.previous_prestige__c == null ||
            //isBlankOrNull(scr.connection__c) ||
            isBlankOrNull(scr.map__c) || 
            isBlankOrNull(scr.mode__c) ||
            isBlankOrNull(scr.comments__c) ||
            showGTunqualified == true
            ;
        //isBlankOrNull(scr.comments__c);
    }
    
    public static boolean getIsMobile(){
        String userAgent = '';
        try{
            userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
            return userAgent.containsIgnoreCase('mobi'); //userAgent can be null
        }catch(Exception e){
            return false;
        }
    }
    public string getArticleLang(){
        return getArticleLang(userLanguage);
    }
    public static string getArticleLang(String uLang){
        string result = 'en_US';
        try{
            if(uLang.containsIgnoreCase('fr')) result = 'fr';
            else if(uLang.containsIgnoreCase('de')) result = 'de';
            else if(uLang.containsIgnoreCase('it')) result = 'it';
            else if(uLang.containsIgnoreCase('pt')) result = 'pt_BR';
            else if(uLang.containsIgnoreCase('sv')) result = 'sv';
            else if(uLang.containsIgnoreCase('nl')) result = 'nl_NL';
            else if(uLang.containsIgnoreCase('es')) result = 'es';
        }catch(Exception e){
            system.debug('error == '+e);
        }
        return result;
    }
    
    public pagereference toggle(){
        //system.debug('Prev Rank Lenght:'+string.valueOf(scr.Previous_Rank__c).length());
        //if(string.valueOf(scr.Previous_Rank__c).length()<3 && string.valueOf(scr.Previous_Prestige__c).length()<3){
        //if(scr.Previous_Rank__c<100 && scr.Previous_Prestige__c<100){
        

            showSubmit = !showSubmit;//}
        	//acceptTos = true;
        //}
        //else{
            //acceptTos = false;
        //}
        //else{
                //acceptTos = false;
            //}
        return null;
    }
}