@isTest
public class RankReset_test {
    static testmethod void TrankReset_test(){
        User u = [select Id, Tier__c, CompanyName, Division, Profile.Name, Contact.id, Contact.UCDID__c, FederationIdentifier, name from user where usertype='CspLitePortal' AND IsActive = true limit 1];
        multiplayer_account__c mp = new multiplayer_account__c(contact__c=u.contactid,platform__c='XBL',gamertag__c='Test',dwid__c='123',is_current__c=true);
        insert mp;
            
        system.runAs(u){
        test.startTest();
            ApexPages.currentPage().getHeaders().put('USER-AGENT','mobi');
        	RankReset rr = new RankReset();
            RankReset.getIsMobile();
            RankReset.updateMP();
            rr.updateGTPicklist();
            rr.getConsolePicklist();
            rr.checkFreq();
            rr.insertSCR();
            rr.console = 'xbox';
            rr.scr.previous_rank__c =2; 
			rr.scr.handle__c='test';
            rr.scr.previous_prestige__c =2;
            rr.scr.comments__c='test';

            rr.scr.map__c='test';
            rr.scr.mode__c='test';
            rr.scr.SCR_Issue__c='test';
            rr.updateGTPicklist();
            rr.console= 'ps3';
            rr.updateGTPicklist();
            rr.getArticleLang();
            rr.insertSCR();
            
            //rr.sendToIW();
            //rr.scr.showGTunqualified == true
        test.stopTest();
        }
    }

}