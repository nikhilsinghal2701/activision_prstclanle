public with sharing class RequestNewAmbassador {
    public boolean isProd = UserInfo.getOrganizationId() == '00DU0000000HMgwMAG';
    
        private static String oauthLogin(String loginUri, String clientId, String clientSecret, String username, String password) {
                HttpRequest req = new HttpRequest();
                req.setMethod('POST');
                req.setEndpoint(loginUri+'/services/oauth2/token');
                req.setBody('grant_type=password' +
                            '&client_id=' + clientId +
                            '&client_secret=' + clientSecret +
                            '&username=' + EncodingUtil.urlEncode(username, 'UTF-8') +
                            '&password=' + EncodingUtil.urlEncode(password, 'UTF-8'));

                Http http = new Http();
                HTTPResponse res = http.send(req);
                System.debug('BODY: '+res.getBody());
                System.debug('STATUS:'+res.getStatus());
                System.debug('STATUS_CODE:'+res.getStatusCode());

                return res.getBody();
    } 
    
    public String RequestAmbassador(string firstname, string lastname, string email, string ucdid){
                
                String oauth;
        if(isProd){
                oauth = oauthLogin('https://login.salesforce.com',
                                              '3MVG9A2kN3Bn17hsFEDFK2XkfFX1BR._.BewG.P.nsgys3MBbA9lic5phAKjIF1OH8TiMnco0.ApP4.h7efba',
                                              '7431490711279147600',
                                              'async@activision.com',
                                              '@ccountSync1');
            }
        else{
            oauth = oauthLogin('https://test.salesforce.com',
                                              '3MVG9GiqKapCZBwHfIWAj.pD2u6RGvIL59LQuT.9ir4UWQvooEwD6BzmQnkfiSsJuVBNEfREhICRHIrOntY7p',
                                              '7602007952781140256',
                                              'async@activision.com.ambuat',
                                              '@ccountSync1');
        }

                String accessToken;
                String instanceURl;

                system.debug(oauth);

                JSONParser parser = JSON.createParser(oauth);
                while(parser.nextToken() != Null){
                        If((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')){
                                parser.nextValue();
                                accessToken = parser.getText();
                        }

                        If((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'instance_url')){
                                parser.nextValue();
                                instanceURl = parser.getText();
                        }

                }

                system.debug('Access Token is '+accessToken);
                system.debug('Instance URL is '+instanceURl);                

        HttpRequest req = new HttpRequest();
                req.setEndpoint(instanceURl+'/services/apexrest/NewAmbassador?firstname='+firstname+'&lastname='+lastname+'&email='+email+'&ucdid='+ucdid);
                req.setHeader('Authorization', 'OAuth '+accessToken);
                req.setMethod('GET');
                Http http = new Http();
                HTTPResponse res = http.send(req);
                system.debug(res.getBody());

                return res.getBody();

        //+firstname+'&'+lastname+'&'+email+'&'+ucdid
    }
}