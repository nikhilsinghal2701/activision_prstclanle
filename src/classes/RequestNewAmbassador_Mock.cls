@isTest
global class RequestNewAmbassador_Mock implements httpCalloutMock {
    global HttpResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setStatus('OK');
        res.setStatusCode(200);
        res.setBody('{"id":"https://test.salesforce.com/id/00De0000005PIT9EAO/005e0000001ymDwAAI","issued_at":"1416869181642","token_type":"Bearer","instance_url":"https://cs15.salesforce.com","signature":"B4F5/lVqmnhsCXUJ9eclFa3x2jngBwf4oVNtkFDMOfE=","access_token":"00De0000005PIT9!ARoAQCOZcKSAwvqr8uvnC9IzXBKBNFqREFgQXsAe1SXFc7jm.gydPSEba8z_sUvcQuvnXucFtKFaY1pLGWxVJ5kG6WcPh_Ag"}');
        return res;
    }

}