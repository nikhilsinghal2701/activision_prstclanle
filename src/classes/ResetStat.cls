public without sharing class ResetStat{
    public case theCase{get;set;}
    public stat_corruption_report__c scr{get;set;}
    public string checkURL{get;set;}
    public boolean showButton{get;set;}
    public user curUser{get;set;}
    public boolean showError{get;set;}
    public string error{get;set;}
    public boolean showIWcomments{get;set;}
    public list<stat_corruption_report__c> dupscr{get;set;}
    public boolean isProd = UserInfo.getOrganizationId() == '00DU0000000HMgwMAG';
    public string endpoint {get; set;}

    public string resp{get; set;}

    public ResetStat(ApexPages.StandardController stdController) {
        showError=false;
        //showButton=false; 
        showButton=true;
        showDcomments = false;
        error='';
        dupscr = new stat_corruption_report__c[]{};
        resp = '';
        endpoint = 'https://contra.infinityward.net/svc/ghosts/dev/';
        if(isProd) endpoint = 'https://contra.infinityward.net/svc/ghosts/live/';
        checkURL = ApexPages.currentPage().getParameters().get('id');
        system.debug(checkURL);
        
        curUser = [SELECT Id, Tier__c, CompanyName, Division, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
        system.debug(curUser.profile.name);
        //if((IsBlankOrNull(curUser.Tier__c) == false && curUser.Tier__c.toLowerCase() == 'tier 3') || curUser.profile.name == 'System Administrator'){
            showButton = true;
            showIWcomments = true;
        //}
        
        if (checkURL != null){
            scr = [select id,handle__c,date__c,scr_issue__c,previous_rank__c,rank__c,map__c, mode__c, connection__c, 
                    comments__c, reset_ok__c,case__c,user__c,dbid__c,console__c,schedulekey__c,status__c,
                    iwagentcomment__c, contact__c, CreatedById, current_prestige__c, previous_prestige__c 
                    from stat_corruption_report__c
                    where id=:ApexPages.currentPage().getParameters().get('id')];
            if(scr.dbid__c != null && scr.dbid__c != 0){
                showButton = true;
            }
        }else{
            scr = (stat_corruption_report__c)stdController.getRecord();
            system.debug(scr);
        }
        if(stdController.getId() != null){ 

        }else if(scr.Case__c != null){try{
            convertConsole([SELECT Platform__c FROM Case WHERE Id = :scr.Case__c].Platform__c);
            }catch(Exception e){}
        }
    }
    
    public ResetStat(){
        showDcomments = false;
    }
    
    public pagereference insertSCR(){
        error='';
        showerror=false;
        //scr.case__c = ApexPages.currentPage().getParameters().get('id');
        //scr.user__c = UserInfo.getUserId();
        
        if(missingData()){
            showError = true;
            error = 'All fields required!!!';
            return null;
        }
        else{
            scr.mode__c = scr.mode__c.replaceAll( '\\s+', '');
            scr.map__c = scr.map__c.replaceAll( '\\s+', '');
            scr.Status__c = 'OPEN';
            case tempcase = [select id, contactid from case where id=:scr.case__c];
            scr.contact__c = tempcase.contactid;
            if(scr.Handle__c.contains('STEAM_') && scr.console__c == 'pc'){                
                string[] parts = scr.Handle__c.split(':');
                long V = 76561197960265728L;
                long Y = long.valueOf( parts[ 1 ] );
                long Z = long.valueOf( parts[ 2 ] );
                long W = Z * 2 + V + Y;
                system.debug('w == '+w);
                scr.Handle__c = string.valueof(W);
            }
            upsert SCR;
            tempCase.OwnerId = [SELECT Id FROM Group WHERE Name = 'Rank Issues'].Id;
            tempCase.Status = 'Escalated';
            update tempCase;
            return new ApexPages.StandardController(tempCase).view();
        }   
    }
    
    public pagereference SendtoIW(){
        //test if the gamer tag has played ghosts
        PlayerData2 cToTest = new PlayerData2();
        cToTest.gamertag = scr.handle__c;
        cToTest.console = scr.console__c;
        cToTest.getResponse();
        if(cToTest.player != null && (cToTest.player.IsValidGamertag == null || cToTest.player.IsValidGamertag == false)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, scr.Handle__c+' is being reported as an invalid gamertag'));
            return null;
        }
        if(missingData()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'All fields required.'));
            return null;
        }
    
        string resetok = scr.reset_ok__c ? '1' : '0';
        DwProxy.service serv = new dwProxy.service();
        serv.timeout_x = 120000;
        
        string query = endpoint + scr.console__c + '/'; //console is a text field
        query += 'submitResetRequest';
        query += '/'+string.valueof(scr.date__c)+'/';
        query += resetok+'?';
        query += 'gt='+EncodingUtil.URLENCODE(scr.handle__c,'UTF-8')+'&';
        if(! isBlankOrNull(scr.SCR_Issue__c)){
            query += 'desc='+EncodingUtil.URLENCODE(scr.SCR_Issue__c, 'UTF-8')+'&';
        }
        query += 'md='+EncodingUtil.URLENCODE(scr.mode__c,'UTF-8')+'&';
        query += 'mp='+EncodingUtil.URLENCODE(scr.map__c,'UTF-8')+'&';
        query += 'cn='+EncodingUtil.URLENCODE(scr.connection__c,'UTF-8')+'&';
        query += 'pr='+string.valueof(scr.previous_rank__c)+'&';
        query += 'cr='+string.valueof(scr.rank__c);
        if(scr.previous_prestige__c != null){//number field, no URLencoding
            query += '&pp='+scr.previous_prestige__c;
        }
        if(scr.current_prestige__c != null){//number field, no URLencoding
            query += '&cp='+scr.current_prestige__c;
        }
        
        system.debug(query);
        query += '@headers-exist@@usecsnetworkcreds#true';
        resp = serv.passEncryptedGetData(query);
        
        if (resp.left(1) == '<'){
            showError=true;
            //error = 'Record not found: Please verify platform and gamertags are correct.';
            error = resp;
        }else{
            showError=false;
            scr.dbid__c = integer.valueof(resp);
            scr.user__c = UserInfo.getUserId();
            scr.Submitted_Date_Time__c = datetime.now();
            scr.Status__c = 'Submitted';
            update scr;
            
            list<case_comment__c> cmt = [select id from case_comment__c where case__c=:scr.case__c];
            if(cmt.size()==0){
                insert new case_comment__c(
                    case__c = scr.case__c,
                    comments__c = 'No Rank Reset Comment'
                );
            }

            update new Case(
                Id = scr.case__c,
                Status = 'On hold'
            );
                
            return new ApexPages.standardController(scr).view();
        }   
        return null;
    }
    public string handle {get; set;}
    public pagereference duplicateCheck(){
        dupscr = [SELECT Id, Handle__c, Status__c, CreatedDate 
                 FROM stat_corruption_report__c 
                 WHERE 
                    handle__c=:scr.handle__c AND 
                    console__c=:scr.console__c AND 
                    RecordType.Name = 'Ghosts' AND 
                    Id != :checkURL
                 ORDER BY CreatedDate DESC];
        return null;
    }
    
    public selectOption[] getConsolePicklist(){
        return PlayerData2.getConsolePicklist();
    }
    public void convertConsole(String val){
        if(val == null) val = '';
        val = val.toLowerCase();
        if(val == 'playstation 3'){val='ps3';}
        else if(val == 'playstation 4'){val='ps4';}
        else if(val == 'xbox 360'){val='xbox';}
        else if(val == 'xbox one'){val='xboxone';}
        else if(val == 'nintendo wii u'){val='wiiu';}
        scr.Console__c = val;
    }
    public pageReference queryForBackupRecords(){
        DwProxy.service serv = new dwProxy.service();
        serv.timeout_x = 120000;
        string query = endpoint;
        query += scr.console__c+'/'+scr.handle__c+'/getBackupRecords';
        system.debug(query);
        query += '@headers-exist@@usecsnetworkcreds#true';
        resp = serv.passEncryptedGetData(query);
        try{
            backupRecords = (backupRecord[]) JSON.deserialize(resp, list<backupRecord>.class);
        }catch(Exception e){
            system.debug('e == '+e);
        }
        return null;
    }
    public backupRecord[] backupRecords {get; set;}
    public class backupRecord{
        public String FileName {get; set;}
        public integer MaxRank {get; set;}
        public integer Prestige {get; set;}
        public integer Score {get; set;}
        public String TimePlayed {get; set;}
        public long Timestamp;
        public boolean CorruptionDetected{get; set;}
        public integer Rank{get; set;}
        public void setTimestampe(Long l){Timestamp = l;}
        public String getTimestamp(){
            return DateTime.newInstance(Timestamp*1000).format('MM/dd/yy h:mm a');
        }
        public boolean ViolatesRules {get; set;}
    }
    public boolean showDcomments {get; set;}
    public string dComments {get; set;}
    public string dError {get; set;}
    public string dButtonName {get; set;}
    public pageReference doNothing(){return null;}
    public pageReference denySCR(){
        if(! isBlankOrNull(dComments)){
            insert new Case_Comment__c(
                Case__c = scr.Case__c,
                Comments__c = dComments,
                Origin__c = 'Tier 3'
            );
            Case c = new Case(
                Id = scr.Case__c, 
                Status = 'Closed',
                Closed_By__c = UserInfo.getUserId()
            );
            update c;
            scr.Status__c = 'Denied';
            if(scr.Handle__c.contains('STEAM_') && scr.console__c == 'pc'){                
                string[] parts = scr.Handle__c.split(':');
                long V = 76561197960265728L;
                long Y = long.valueOf( parts[ 1 ] );
                long Z = long.valueOf( parts[ 2 ] );
                long W = Z * 2 + V + Y;
                system.debug('w == '+w);
                scr.Handle__c = string.valueof(W);
            }
            upsert scr;
            return new ApexPages.standardController(new Case(Id = scr.Case__c)).view();
        }else{
            dError = 'Denial comment is required';
        }
        return null;
    }
    public pageReference deescalateSCR(){
        if(! isBlankOrNull(dComments)){
            boolean success = false;
            Group[] gs = [SELECT Id, Name FROM Group WHERE Name LIKE 'Tier 2%' AND Type = 'Queue'];
            User scrCreatedBy = [SELECT CompanyName, Division FROM User WHERE Id = :scr.CreatedById];
            success = checkGroup(gs, scrCreatedBy.Division);
            if(! success){
                success = checkGroup(gs, scrCreatedBy.CompanyName);
                if(! success){
                    gs = [SELECT Id, Name FROM Group WHERE (DeveloperName LIKE 'Tier\_3%' OR DeveloperName = 'Tier_3') AND Type='Queue'];
                    success = checkGroup(gs, 'Tier 3');
                    if(! success){
                        dError = 'Could not de-escalate, queues not found for Company Name: '+scrCreatedBy.CompanyName+' and Division: '+scrCreatedBy.Division+' or Tier 3';
                        return null;
                    }
                }
            }
            return new pageReference('/500/o');
        }else{
            dError = 'De-escalation comment is required';
        }
        return null;
    }
    public pageReference updateSCR(){
        try{
            if(missingData()){
                showError = true;
                error = 'All fields required!!!';
                return null;
            }
            if(scr.Handle__c.contains('STEAM_') && scr.console__c == 'pc'){                
                string[] parts = scr.Handle__c.split(':');
                long V = 76561197960265728L;
                long Y = long.valueOf( parts[ 1 ] );
                long Z = long.valueOf( parts[ 2 ] );
                long W = Z * 2 + V + Y;
                system.debug('w == '+w);
                scr.Handle__c = string.valueof(W);
            }
            update scr;
            return new apexPages.standardController(scr).view();
        }catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;
    }
    public boolean isBlankOrNull(String s){ return s == '' || s == null; }
    public pageReference dCancel(){
        dComments = '';
        dButtonName = '';
        showDcomments = false;
        dError = '';
        return null;
    }
    public boolean checkGroup(Group[] gs, String toMatch){
        if(gs == null) return false;
        if(toMatch == null) return false;      
        for(Group g : gs){
            if(g.Name.containsIgnoreCase(toMatch)){
                update new Case(
                    Id = scr.Case__c, 
                    OwnerId = g.Id,
                    Status = 'De-Escalated',
                    Stat_Reset_Deescalation__c = true
                );
                insert new Case_Comment__c(Case__c=scr.Case__c,Comments__c=dComments,Origin__c='Tier 3');
                scr.Status__c = 'De-Escalated';
                update scr;
                return true;
            }
        }
        return false;
    }
    public boolean missingData(){
        return isBlankOrNull(scr.handle__c) || 
            scr.date__c == null || 
            scr.previous_rank__c == null || 
            scr.rank__c == null || 
            scr.current_prestige__c == null ||
            scr.previous_prestige__c == null ||
            isBlankOrNull(scr.connection__c) ||
            isBlankOrNull(scr.map__c) || 
            isBlankOrNull(scr.mode__c); //|| 
            //isBlankOrNull(scr.comments__c);
    }

}