@isTest
public class SFDC_API_MockWebServiceCallout implements WebServiceMock {
   public void doInvoke(Object stub, Object request, Map<String, Object> response,
           String endpoint, String soapAction, String requestName, String responseNS,
           String responseName, String responseType) 
   {
       system.debug('stub == '+stub);
       system.debug('request == '+request);
       system.debug('endpoint == '+endpoint);
       system.debug('soapAction == '+soapAction); //
       system.debug('requestName == '+requestName); //"login"
       system.debug('responseNS == '+responseNS); // "urn:partner.soap.sforce.com"
       system.debug('responseName == '+responseName); //loginResponse
       system.debug('responseType == '+responseType); //
       //how do we test to determine what type of response to give?
       if(responseType == 'SFDC_API_Partner.loginResponse_element'){ //we are looking for a login response
           SFDC_API_Partner.loginResponse_element responseElement = new SFDC_API_Partner.loginResponse_element();
           SFDC_API_Partner.LoginResult res = new SFDC_API_Partner.LoginResult();
           res.metadataServerUrl = 'this_is_a_test';
           res.sessionId = 'haha_i_am_not_a_real_session';
           responseElement.result = res;
           response.put('response_x', responseElement);
       }else if(responseType == 'SFDC_API_MetaData.readMetadataResponse_element'){
           SFDC_API_MetaData.readMetadataResponse_element responseElement = new SFDC_API_MetaData.readMetadataResponse_element();
           SFDC_API_MetaData.ReadResult res = new SFDC_API_MetaData.ReadResult();
           res.records = new SFDC_API_MetaData.CustomField[]{};
           SFDC_API_MetaData.CustomField f1 = new SFDC_API_MetaData.CustomField();
           f1.fullName = 'key1';
           //the picklist
           SFDC_API_MetaData.Picklist pl1 = new SFDC_API_MetaData.Picklist();
           SFDC_API_MetaData.PicklistValue pl1v_a = new SFDC_API_MetaData.PicklistValue();
           pl1v_a.fullName = 'oldVal1';
           SFDC_API_MetaData.PicklistValue pl1v_b = new SFDC_API_MetaData.PicklistValue();
           pl1v_b.fullName = 'newVal1';
           pl1.picklistValues = new SFDC_API_MetaData.PicklistValue[]{pl1v_a, pl1v_b};
           
           f1.picklist = pl1;
           res.records.add(f1);
           responseElement.result = res;
           response.put('response_x', responseElement);
       }else if(responseType == 'SFDC_API_MetaData.createMetaDataResponse_element'){
           SFDC_API_MetaData.createMetaDataResponse_element responseElement = new SFDC_API_MetaData.createMetaDataResponse_element();
           response.put('response_x', responseElement);
       }else if(responseType == 'SFDC_API_MetaData.updateMetaDataResponse_element'){
           SFDC_API_MetaData.updateMetaDataResponse_element responseElement = new SFDC_API_MetaData.updateMetaDataResponse_element();
           response.put('response_x', responseElement);
       }else if(responseType == 'SFDC_API_MetaData.deleteMetaDataResponse_element'){
            SFDC_API_MetaData.deleteMetaDataResponse_element responseElement = new SFDC_API_MetaData.deleteMetaDataResponse_element();
            response.put('response_x', responseElement);       
       }
   }
}