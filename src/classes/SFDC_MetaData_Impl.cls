public class SFDC_MetaData_Impl{
    //there can only be 10 callouts per apex transaction, which is why there are counts documented in this code
    public SFDC_API_MetaData.MetadataPort metaPort {get; set;}
    public String pSessionId {get; set;}
    public String pMetadataUrl {get; set;}
    public SFDC_MetaData_Impl(){ //initializing can use 0 or 1 (in prod it will be 1)
        metaPort = new SFDC_API_MetaData.MetadataPort();
        for(Profile p : [SELECT Id FROM Profile WHERE PermissionsModifyAllData = true]){
            if(UserInfo.getProfileId() == p.Id){
                pSessionId = UserInfo.getSessionId();
                break;
            }
        }
        pMetadataUrl = 'https://na12.salesforce.com/services/Soap/m/30.0/00DK0000008yTFX';//hardcode fail
    }
    public void login(){ //it is a seperate call in case you need to login again
        SFDC_API_Partner.Soap soapPort = new SFDC_API_Partner.Soap();
        SFDC_API_Partner.LoginResult theLR = soapPort.login('griffin.warburton@activision.com.csuat','an0therP*11');
        metaPort.endpoint_x = theLR.metadataServerUrl;
        pMetadataUrl = metaPort.endpoint_x;
        system.debug('MetaDataServerUrl == '+metaPort.endpoint_x);

        SFDC_API_MetaData.SessionHeader_element hdr = new SFDC_API_MetaData.SessionHeader_element();
        hdr.sessionId = theLR.sessionId;
        metaPort.SessionHeader = hdr;
    }
    public boolean doSync(map<string, string[]> cVals, map<string, string[]> uVals, map<string, string[]> dVals){
        try{//to process everything
            String command = 'create';
            for(map<String, String[]> vals : new List<map<String, String[]>>{cVals,uVals,dVals}){
                if(vals.keySet().size() > 0){
                    if(LIMITS.getFutureCalls() < LIMITS.getLimitFutureCalls()){//we have at least 1 callout remaining
                        String[] pKeys = new String[]{}, pOldVals = new String[]{}, pNewVals = new String[]{};
                        if(vals.keySet().size() <= 10){
                            for(String key : vals.keySet()){
                                pKeys.add(key);
                                pOldVals.add(vals.get(key)[0]);
                                pNewVals.add(vals.get(key)[1]);
                            }
                            if(! test.isRunningTest()) doSync(pSessionId, pMetadataUrl, command, pKeys, pOldVals, pNewVals);
                        }else{
                            Integer count = 0;
                            for(String key : vals.keySet()){
                                if(LIMITS.getFutureCalls() < LIMITS.getLimitFutureCalls()){   
                                    pKeys.add(key);
                                    pOldVals.add(vals.get(key)[0]);
                                    pNewVals.add(vals.get(key)[1]);
                                    count++;
                                    if(count == 10){
                                        count = 0;
                                        if(! test.isRunningTest()) doSync(pSessionId, pMetadataUrl, command, pKeys, pOldVals, pNewVals);
                                        pKeys = new String[]{}; 
                                        pOldVals = new String[]{}; 
                                        pNewVals = new String[]{};
                                    }
                                }
                            }
                        }//end the more than 10 fields being updated in one of the maps block
                    }else return false; //we've hit the callout limit when starting to process the next map
                }
                if(command == 'create') command = 'update';
                else command = 'delete';
            }
            return true;
        }catch(Exception e){
            system.debug('e == '+e);
            return false;
        }
    }

    @future(callout=true)
    public static void doSync(String sessionId, String metadataEndpoint, String command, String[] keys, 
                                String[] oldVals, String[] newVals)
    {
        //this method will use anywhere from 1 to 5 callouts
        //either 0 or 1 to obtain the session Id / API Endpoint 
        // //in prod most likely 1 as the user running the class will not be a sysadmin
        //always 1 for the "readMetaData" call
        //then either
        // if command is delete: 0 or 1 (0 if value not found, 1 if value found when the delete callout is made)
        // if the command is create or update: 0 to 2
        // //0 if the new or updated values are found
        // //1 if none of the new values are found
        // //1 if all of the to be updated values are found
        // //2 if only some of the to be updated values are found
        // // //the not found values will be created and the found will be updated
        if(sessionId == null){//no session passed, get a new admin credential session id
            SFDC_API_Partner.Soap soapPort = new SFDC_API_Partner.Soap();
            SFDC_API_Partner.LoginResult theLR = soapPort.login('griffin.warburton@activision.com.csuat','an0therP*11');
            sessionId = theLR.sessionId;
            metadataEndpoint = theLR.metadataServerUrl;
        }
        //set session id
        SFDC_API_MetaData.SessionHeader_element hdr = new SFDC_API_MetaData.SessionHeader_element();
        hdr.sessionId = sessionId;
        
        SFDC_API_MetaData.MetadataPort metaPort = new SFDC_API_MetaData.MetadataPort();
        metaPort.SessionHeader = hdr;
        metaPort.endpoint_x = metadataEndpoint;
        metaPort.timeout_x = 120000;
        //port all prepped to do the work
        //reconstitute map, because static methods can't accept maps and we need the map to handle
        //the readMetadata response
        map<String, String[]> fullName2vals = new map<String, String[]>();
        for(Integer i = 0; i < keys.size(); i++)
            fullName2vals.put(keys[i],new string[]{oldVals[i], newVals[i]});
        
        SFDC_API_MetaData.Metadata[] mdToCreate = new List<SFDC_API_MetaData.Metadata>();
        SFDC_API_MetaData.Metadata[] mdToUpdate = new List<SFDC_API_MetaData.Metadata>();
        String[] mdToDelete = new String[]{};
        SFDC_API_MetaData.ReadResult rr = metaPort.readMetadata('CustomField', keys);
        
        for(SFDC_API_MetaData.CustomField f : (SFDC_API_MetaData.CustomField[]) rr.records){
            String[] vals = fullName2vals.get(f.fullName);
            boolean newValFound = false, preValFound = false;
            //you don't deploy the new picklist value, you deploy the field with the new values as specified
            for(SFDC_API_MetaData.PicklistValue plv : f.picklist.picklistValues){
                if(plv.FullName == vals[0]){
                    if(command == 'delete') mdToDelete.add(f.fullName+'.'+plv.FullName);
                    preValFound = true;
                }else if(plv.FullName == vals[1])
                    newValFound = true; //new value already exists
            }
            
            if(preValFound){ //the previous value was found
                if(newValFound == false){ //the new value wasn't
                    if(command != 'delete'){ //we are not deleting
                        for(SFDC_API_MetaData.PicklistValue plv : f.picklist.picklistValues){
                            if(plv.FullName == vals[0]){
                                plv.FullName = vals[1];
                                mdToUpdate.add(f); //but the new value wasn't do update
                                break;
                            }
                        }
                    }
                }
                //else if(the new value was found) no need to update
            }else{ //we did not find the previous version
                if(! newValFound){ //nor did we find the new val
                    if(command == 'create' || command == 'update'){//so we need to create it
                        SFDC_API_MetaData.PicklistValue nPLV = new SFDC_API_MetaData.PicklistValue();
                        nPLV.fullName = f.FullName+'.'+vals[1];
                        nPLV.allowEmail = false;
                        nPLV.closed = false;
                        nPLV.color = null; //?assigned dynamically?
                        nPLV.controllingFieldValues = new String[]{};
                        nPLV.converted = false;
                        nPLV.cssExposed = false;
                        nPLV.default_x = false;
                        nPLV.description = '';
                        nPLV.forecastCategory = null;
                        nPLV.highPriority = false;
                        nPLV.probability = 0;
                        nPLV.reverseRole = '';//wth?
                        nPLV.reviewed = false;
                        nPLV.won = false;
                        mdToCreate.add(nPLV);
                    }//else we are deleting and we didn't find either version, so it didn't exist
                }//else{alert('new value already exists');}
            }
        }
                    
        SFDC_API_MetaData.SaveResult[] cResults;
        SFDC_API_MetaData.SaveResult[] uResults;
        SFDC_API_MetaData.DeleteResult[] dResults;
        
        if(! mdToCreate.isEmpty())
            cResults = metaPort.createMetadata(mdToCreate);
        if(! mdToUpdate.isEmpty())
            uResults = metaPort.updateMetadata(mdToUpdate);
        if(! mdToDelete.isEmpty())
            dResults = metaPort.deleteMetadata('PicklistValue', mdToDelete);
        
        system.debug('cResults == '+cResults);
        system.debug('uResults == '+uResults);
        system.debug('dResults == '+dResults);
        
        //TODO: Add in success failure logging against the corresponding disposition 
        
    }
}