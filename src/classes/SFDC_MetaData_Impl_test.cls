@isTest
public class SFDC_MetaData_Impl_test{
    public static testMethod void test_Login(){
        SFDC_MetaData_Impl tstCls = new SFDC_MetaData_Impl();
        test.setMock(WebServiceMock.class, new SFDC_API_MockWebServiceCallout());
        test.startTest();
            tstCls.login();
        test.stopTest();
        system.assertEquals('this_is_a_test',tstCls.pMetadataUrl);
        system.assertEquals(((String) UserInfo.getOrganizationId()).subString(0, 15)+'!ApexTestSession',tstCls.pSessionId);
        system.assertEquals('haha_i_am_not_a_real_session',tstCls.metaPort.SessionHeader.sessionId);
    }
    public static testMethod void test_doSync_notFuture(){
        SFDC_MetaData_Impl tstCls = new SFDC_MetaData_Impl();
        tstCls.doSync(
            new map<String, String[]>{'fullName'=>new String[]{'oldVal','newVal'}}, //create
            new map<String, String[]>{'fullName'=>new String[]{'oldVal','newVal'}}, //update
            new map<String, String[]>{'fullName'=>new String[]{'oldVal','newVal'}}  //delete
        );
        
        tstCls.doSync(
            new map<String, String[]>{
                'fullName0'=>new String[]{'oldVal','newVal'},
                'fullName1'=>new String[]{'oldVal','newVal'},
                'fullName2'=>new String[]{'oldVal','newVal'},
                'fullName3'=>new String[]{'oldVal','newVal'},
                'fullName4'=>new String[]{'oldVal','newVal'},
                'fullName5'=>new String[]{'oldVal','newVal'},
                'fullName6'=>new String[]{'oldVal','newVal'},
                'fullName7'=>new String[]{'oldVal','newVal'},
                'fullName8'=>new String[]{'oldVal','newVal'},
                'fullName9'=>new String[]{'oldVal','newVal'},
                'fullName0'=>new String[]{'oldVal','newVal'},
                'fullNameA'=>new String[]{'oldVal','newVal'}
            }, //create
            new map<String, String[]>{'fullName'=>new String[]{'oldVal','newVal'}}, //update
            new map<String, String[]>{'fullName'=>new String[]{'oldVal','newVal'}}  //delete
        );
    }
    public static testMethod void test_doSync_future_create1(){ //value to create already exists
        test.setMock(WebServiceMock.class, new SFDC_API_MockWebServiceCallout());
        test.startTest();
            SFDC_MetaData_Impl.doSync(
                null,
                'fake_endpoint',
                'create',
                new string[]{'key1','key2'},
                new string[]{'',''},
                new string[]{'newVal1','newVal2'}
            );
        test.stopTest();
    }
    public static testMethod void test_doSync_future_create2(){ //value to create does not yet exist
        test.setMock(WebServiceMock.class, new SFDC_API_MockWebServiceCallout());
        test.startTest();
            SFDC_MetaData_Impl.doSync(
                null,
                'fake_endpoint',
                'create',
                new string[]{'key1','key2'},
                new string[]{'',''},
                new string[]{'newVal3','newVal4'}
            );
        test.stopTest();
    }
    public static testMethod void test_doSync_future_update1(){ //value to update exists
        test.setMock(WebServiceMock.class, new SFDC_API_MockWebServiceCallout());
        test.startTest();
            SFDC_MetaData_Impl.doSync(
                null,
                'fake_endpoint',
                'update',
                new string[]{'key1','key2'},
                new string[]{'oldVal1','oldVal2'},
                new string[]{'newVal1','newVal2'}
            );
        test.stopTest();
    }
    public static testMethod void test_doSync_future_update2(){ //value to update does not exist
        test.setMock(WebServiceMock.class, new SFDC_API_MockWebServiceCallout());
        test.startTest();
            SFDC_MetaData_Impl.doSync(
                null,
                'fake_endpoint',
                'update',
                new string[]{'key1','key2'},
                new string[]{'oldVal3','oldVal4'},
                new string[]{'newVal3','newVal4'}
            );
        test.stopTest();
    }
    public static testMethod void test_doSync_future_delete1(){ //value to delete exists
        test.setMock(WebServiceMock.class, new SFDC_API_MockWebServiceCallout());
        test.startTest();
            SFDC_MetaData_Impl.doSync(
                null,
                'fake_endpoint',
                'delete',
                new string[]{'key1','key2'},
                new string[]{'oldVal1','oldVal2'},
                new string[]{'',''}
            );
        test.stopTest();
    }
    public static testMethod void test_doSync_future_delete2(){ //value to delete does not exist
        test.setMock(WebServiceMock.class, new SFDC_API_MockWebServiceCallout());
        test.startTest();
            SFDC_MetaData_Impl.doSync(
                null,
                'fake_endpoint',
                'delete',
                new string[]{'key1','key2'},
                new string[]{'oldVal3','oldVal4'},
                new string[]{'',''}
            );
        test.stopTest();
    }
    
}