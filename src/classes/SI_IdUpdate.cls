public class SI_IdUpdate{

    public static void updateContact(){
        SI_GoLive__c[] recs = [SELECT Id, UCD__c, UNO__c FROM SI_GoLive__c WHERE ContactProcessed__c = false LIMIT 3300];
        map<String, String> u2u = new map<String, String>();
        Id[] siIds = new Id[]{};
        for(SI_GoLive__c rec : recs){
            u2u.put(rec.UCD__c,rec.UNO__c);
            siIds.add(rec.Id);
        }
        markContactCompleteSIrecs(siIds);
        
        Contact[] ctcs = new Contact[]{};
        Account[] acts = new Account[]{};
        for(Contact c : [SELECT AccountId, UCDId__c FROM Contact WHERE UCDId__c IN :u2u.keyset()]){
            String ucd = u2u.get(c.UCDId__c);
            c.UCDId__c = ucd;
            ctcs.add(c);
            acts.add(new Account(Id = c.AccountId, Name = ucd, AccountNumber = ucd));   
        }
        database.update(ctcs, false);
        update acts;
    }
    @future
    public static void markContactCompleteSIrecs(Id[] siIds){
        SI_GoLive__c[] recs = [SELECT Id FROM SI_GoLive__c WHERE Id IN :siIds];
        for(SI_GoLive__c rec : recs){
            rec.ContactProcessed__c = true;
        }
        update recs;
    }
    @future
    public static void markUserCompleteSIrecs(Id[] siIds){
        SI_GoLive__c[] recs = [SELECT Id FROM SI_GoLive__c WHERE Id IN :siIds];
        for(SI_GoLive__c rec : recs){
            rec.UserProcessed__c = true;
        }
        update recs;
    }
    
    public static void updateUser(){
        SI_GoLive__c[] recs = [SELECT Id, UCD__c, UNO__c FROM SI_GoLive__c WHERE UserProcessed__c = false LIMIT 5000];
        Id[] siIds = new Id[]{};
        map<String, String> u2u = new map<String, String>();
        for(SI_GoLive__c rec : recs){
            u2u.put(rec.UCD__c,rec.UNO__c);
            siIds.add(rec.Id);
        }
        markUserCompleteSIrecs(siIds);
        
        User[] us = [SELECT FederationIdentifier FROM User WHERE FederationIdentifier IN :u2u.keyset()];
        for(User u : us){
            String ucd = u2u.get(u.FederationIdentifier);
            u.FederationIdentifier = ucd;
            u.Username = ucd+'@atvi.support.prod';
        }
        update us;
    }
}