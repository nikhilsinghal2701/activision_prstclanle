global class ScheduledCallouts implements Schedulable, Database.AllowsCallouts {
    
    //Do all the data prep work here, in the scheduled class
    //scheduled class constructor
    global void execute(SchedulableContext sc) {
        doCallout();
    }
    //ATVI scheduled class business rule schedule
    public static void setSchedule(){
        for(integer i = 0; i < 60; i+=5){
            system.schedule('ScheduledCallout-'+i, '0 '+i+' * * * ?', new ScheduledCallouts());
        }
    }
    
    @future(callout=true)
    public static void doCallout(){
        String inContactEndpoint = 'https://home-c8.incontact.com/inContact/Manage/Scripts/Spawn.aspx';
        inContactEndpoint += '?scriptName=ATVI%20Get%20Wait%20Time%20-%20Phone&bus_no=4593162';
        inContactEndpoint += '&scriptId=2897738&skill_no=53365&p1=53367&p2=&p3=&p4=&p5=&Guid=e19ac56f-3e64-47d9-b339-b7592c0fdf74';
        
        httpRequest req = new httpRequest();
        req.setMethod('GET');
        req.setEndpoint(inContactEndpoint);
        http h = new http();
        httpResponse resp = h.send(req);
        system.debug('resp == '+resp);
        if(resp != null) system.debug('resp.getBody() == '+resp.getBody());
    }
}