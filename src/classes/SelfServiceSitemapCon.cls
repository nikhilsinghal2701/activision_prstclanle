public with sharing class SelfServiceSitemapCon {
  // A property to return the proper Sites URL, used to build our full URL later
  public string siteUrl{
    get{
    String surl = site.getcustomwebaddress();
    if (surl != '' && surl != null) {
        return site.getCurrentSiteUrl();
    } else {
        return site.getcurrentsiteurl();
    }
    }
    set;
  }

 // A method to retrieve the most recent 1000 FAQ__kav articles
  public FAQ__kav[] getFAQList(){  
  	 
    FAQ__kav[] f = [select id, knowledgearticleid, URLName, lastpublisheddate, Game_titel__c, title FROM FAQ__kav where PublishStatus = 'Online' AND IsVisibleInPkb = true AND Language='en_US' ORDER BY LastPublishedDate DESC LIMIT 1000];
    return f;   

  }

  // A slightly more complex query based on the product and lang URL params, to return the most recent 1000 articles
  /*public Documentation__kav[] getDocumentationList(){
  	// XXX NOTE FROM KS: DO NOT USE THIS AS-IS, URL PARAMS ARE NOT PROPERLY ESCAPED
  	// NEED TO USE escapeSingleQuotes
    String productcode = ApexPages.currentPage().getParameters().get('product') + '__c';
    String langcode = ApexPages.currentpage().getParameters().get('lang') + '__c';
    String qry = 'select id, knowledgearticleid, lastpublisheddate, localized_title__c, title from Documentation__kav where (PublishStatus = \'Online\' AND IsVisibleInPkb = true) WITH DATA CATEGORY Products__c AT ' + productcode + ' AND Languages__c AT ' + langcode +' ORDER BY LastPublishedDate DESC LIMIT 1000';
    Documentation__kav[] d = database.query(qry);   
    return d;   
  }*/
}