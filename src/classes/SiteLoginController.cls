/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class SiteLoginController {
    global String username {get; set;}
    global String password {get; set;}
    public String branding;
    transient Cookie langCookie;
    public String userCountry;
    public String userLanguage;
    public String getUserLanguage()
    {
       Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
        	system.debug('tempCookie not null');
            userLanguage = tempCookie.getValue();
        }else{
        	system.debug('tempCookie is null');
            userLanguage = 'en_US';
        }
            
        return userLanguage;
    }
     public void setUserLanguage(String userLanguage)
    {
         this.userLanguage = userLanguage;
        system.debug('@@@@@@'+userLanguage);
    } 
   
    public static boolean isloggedIn;
    public string selectedLang;
    public string targetPage{get;set;}
          
    global PageReference login() {
    
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        system.debug('111111111111' + startUrl);
        PageReference PageRef=Site.login(username, password,'/Atvi_authenticated_homepage');
        /*if(PageRef!=null)
        {
            PortalLogin= PageReference('/apex/Atvi_live_support_contact_options');
            return PortalLogin;
        }else
        {
            
            return PageRef;
        }
        */
        //return PageRef;
        //ATVICustomerPortalController.isloggedIn=true;
        ApexPages.currentPage().getParameters().put('hasLoggedIn','true');
        return PageRef;
    }
    public String getBranding(){
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('clickedOn');
        if(tempCookie != null)
            branding = tempCookie.getValue();
        return branding;                
    }
   public PageReference refreshLanguage(){               
        if(selectedLang != null && selectedLang != ''){
                userLanguage    = selectedLang;
        }
        /*else{
                userLanguage    = 'en_us';
        }*/
        system.debug('@@@@@@'+userLanguage);
        langCookie = new Cookie('uLang', userLanguage, null, -1, false);
        ApexPages.currentPage().setCookies(new Cookie[]{langCookie});
        userLanguage = langCookie.getValue();
        return null;
    }
    
    global PageReference checklogin() {
    
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        system.debug('111111111111' + startUrl);
        PageReference PageRef= Site.login(username, password, '/apex/Atvi_authenticated_homepage');

        /*if(PageRef!=null)
        {
            PortalLogin= PageReference('/apex/Atvi_live_support_contact_options');
            return PortalLogin;
        }else
        {
            
            return PageRef;
        }
        */
       
        ApexPages.currentPage().getParameters().put('hasLoggedIn','true');
        //return ApexPages.currentPage();
        return PageRef;
    }
 
     global SiteLoginController() {  
     
        getUserLanguage();
        refreshLanguage();
        setUserLanguage(userLanguage);
        }
    
    @IsTest(SeeAllData=true) global static void testSiteLoginController () {
        // Instantiate a new controller with all parameters in the page
        SiteLoginController controller = new SiteLoginController ();
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
                
        System.assertEquals(controller.login(),null);                           
    }    
}