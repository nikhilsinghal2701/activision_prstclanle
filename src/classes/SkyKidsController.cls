public with sharing class SkyKidsController{
     /*Used to set the language of the Apex page acccording to the user's choice*/    
    public String lang {get;set;}
    public void getUserLanguage()
    {
        Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
        if(tempCookie != null){
            system.debug('tempCookie not null');
            lang = tempCookie.getValue();            
        }else{
            system.debug('tempCookie is null');
            lang='en_US';
        }
    }
    
    /*Checks if user is logged in*/
    public boolean isPortalUser{
        get{
            if(UserInfo.getUserType().equalsIgnoreCase('CSPLitePortal')) return true;
            else return false;
        }
        set;
    }
    
    public SkyKidsController(){
        getUserLanguage();
    }

}