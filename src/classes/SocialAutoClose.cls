global class SocialAutoClose implements Schedulable {
    global static void setSchedule(){
        system.schedule('SocialAutoClose-0', '0 0 * * * ? *' , new SocialAutoClose());
        system.schedule('SocialAutoClose-30', '0 30 * * * ? *' , new SocialAutoClose());
    }
   
    //dt = dt.addHours(-24);
    global void execute(SchedulableContext ctx) {
        processAsynchronously();        
    }   

    @future
    public static void processAsynchronously(){
        datetime dt = system.now().addHours(-24);
        system.debug(LoggingLevel.ERROR, 'dt == '+dt);
        datetime dt72 = system.now().addHours(-72);
        system.debug(LoggingLevel.ERROR, 'dt72 == '+dt72);
        list<case> casestoupdate = new list<case>();
        list<case> theNewCases = new list<case>();
        list<case> theHandledCases = new list<case>();
        string rectypeid = [SELECT id from recordtype where name='Social Media' and sObjectType = 'Case' limit 1].Id;
        set<Id> jiveIntegrationUserIds = new set<Id>();
        Id jiveIntegrationUserId;
        for(User u : [SELECT Id FROM User WHERE Profile.Name = 'Jive Integration']){
            jiveIntegrationUserIds.add(u.Id);
            jiveIntegrationUserId = u.Id;
        }
        
        //New Radian6 Cases
        for(Case c : [select id, social_ishandled__c, CreatedById from case where recordtypeid =:rectypeid and isClosed = false 
                            AND (CreatedById NOT IN :jiveIntegrationUserIds)
                            and createddate < :dt and (status = 'Open' or status='New') LIMIT 200])
        {
            if(c.social_ishandled__c){
                c.status = 'Social Auto-Closed';
                c.close_status__c = 'Handled';
            }else{
                c.status = 'Awaiting Response';
            }
            casestoupdate.add(c);
        }
        
        //Handled Radian6 Cases
        for(Case c : [select id, Social_LastResponse__c, CreatedById from case where recordtypeid =:rectypeid and isClosed = false 
                                AND (CreatedById NOT IN :jiveIntegrationUserIds)
                                and createddate < :dt72 and status = 'Awaiting Response' LIMIT 100])
        {
            c.status = 'Social Auto-Closed';
            if(c.Social_LastResponse__c != NULL){
                c.close_status__c = 'Handled';
            }else{
                c.close_status__c = 'Abandoned';
            }
            casestoupdate.add(c);
        }
        
        //Do this process for JIVE
        DateTime dt48 = system.now().addHours(-48); //48 hours
        DateTime dt168 = system.now().addHours(-168); //168 hours (1 week)
        for(Case c : [SELECT Id, LastModifiedDate, (SELECT Id FROM CaseComments) FROM Case WHERE CreatedById IN :jiveIntegrationUserIds 
                            AND CreatedDate <= :dt48 AND IsClosed = false LIMIT 100])
        {
            //If customer doesn't respond to request for more information in a week, close the case as abandoned by customer
            if(c.CaseComments != null && c.CaseComments.size() > 0){
                //pending user response, how long has it been pending?
                if(c.lastModifiedDate <= dt168 || Test.isRunningTest()){
                    c.Status = 'Closed';
                    c.Resolution_Type__c = 'Handled';
                    if(jiveIntegrationUserId != null) c.Closed_By__c = jiveIntegrationUserId;
                    else c.Closed_By__c = UserInfo.getUserId();
                    casestoupdate.add(c);
                }
            }else{
                c.Status = 'Closed';
                c.Resolution_Type__c = 'Abandoned';
                if(jiveIntegrationUserId != null) c.Closed_By__c = jiveIntegrationUserId;
                else c.Closed_By__c = UserInfo.getUserId();
                casestoupdate.add(c);
            }
        }
        system.debug(LoggingLevel.ERROR, 'casesToUpdate.size() == '+casesToUpdate.size());
        update casestoupdate;
        
    }
}