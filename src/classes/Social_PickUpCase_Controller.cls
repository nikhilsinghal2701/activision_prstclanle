public class Social_PickUpCase_Controller{
	Case theCase;
    public Social_PickUpCase_Controller(ApexPages.StandardController cont){
        this.theCase = (Case) cont.getRecord();
    }
    
    public pageReference doPickup(){
        this.theCase.OwnerId = UserInfo.getUserId();
        //this.theCase.Status = 'Work in Progress';
        //this.theCase.CM_Case_Owner_TEXT__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
        //this.theCase.Support_User__c = this.theCase.CM_Case_Owner_TEXT__c;
        update this.theCase;
        return new pageReference('/'+this.theCase.id);
    }
}