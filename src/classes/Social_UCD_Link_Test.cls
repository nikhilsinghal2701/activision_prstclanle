@isTest
public class Social_UCD_Link_Test{

    public static testMethod void createData(){
        insert new R6_Customizations__c(
            User_excluded_from_data_requirement__c = true, 
            SetupOwnerId = UserInfo.getUserId()
        );
        insert new Contact[]{
            new Contact(FirstName = 'test',LastName = '1',Email='1@1.1',UCDID__c = '1',Birthdate = system.today()),
            new Contact(FirstName = 'test',LastName = '2',Email='2@1.1',UCDID__c = '2',Birthdate = system.today()),
            new Contact(FirstName = 'test',LastName = '3',Email='3@1.1',UCDID__c = '3',Birthdate = system.today()),
            new Contact(FirstName = 'test',LastName = '4',Email='4@1.1',UCDID__c = '4',Birthdate = system.today()),
            new Contact(FirstName = 'test',LastName = '5',Email='5@1.1',UCDID__c = '5',Birthdate = system.today()),
            new Contact(FirstName = 'test',LastName = '6')
        };
    }
    public static testMethod void test1_onSPcreateNoMatch(){
        createData();
        system.assert([SELECT count() FROM UCD_Social_Link__c] == 0);
        insert new SocialPersona(
            ParentId = [SELECT Id FROM Contact Limit 1].id,
            ExternalId = 'zzz',
            Name = 'zzz',
            Provider = 'Twitter'
        );
        system.assert([SELECT count() FROM UCD_Social_Link__c] == 1);
        //the outbound message has sent the query to Tibco, and lack of response indicates no match
    }
    public static testMethod void test2_onSPcreateMatch(){
        createData();system.assert([SELECT count() FROM UCD_Social_Link__c] == 0);
        insert new SocialPersona(
            ParentId = [SELECT Id FROM Contact WHERE LastName = '6' Limit 1].id,
            ExternalId = 'zzz',
            Name = 'zzz',
            Provider = 'Twitter'
        );
        system.assert([SELECT count() FROM UCD_Social_Link__c] == 1);
        UCD_Social_Link__c theLinker = [SELECT Id FROM UCD_Social_Link__c];
        theLinker.Ucdid__c = '5';
        update theLinker; //this is the simulation of a match
        system.assert([SELECT count() FROM Contact] == 5);
        system.assert([SELECT count() FROM Contact WHERE LastName = '6'] == 0);
    }
    public static testMethod void test3_onPostCreateNoMatch(){
        createData();
        system.assert([SELECT count() FROM UCD_Social_Link__c] == 0);
        SocialPersona sp = new SocialPersona(
            ParentId = [SELECT Id FROM Contact Limit 1].id,
            ExternalId = 'zzz',
            Name = 'zzz',
            Provider = 'Twitter'
        );
        insert sp;
        UCD_Social_Link__c theLink1 = [SELECT Id, LastModifiedDate FROM UCD_Social_link__c];
        SocialPost spo = new SocialPost(
            PersonaId = sp.Id,
            WhoId = sp.ParentId,
            Name = 'test',
            RecordTypeId = [SELECT Id FROM RecordType WHERE IsActive = TRUE AND sObjectType = 'SocialPost' LIMIT 1].Id
        );
        insert spo;
        system.assert([SELECT count() FROM UCD_Social_Link__c] == 1); //this verifies that the record was updated.
    }
    
}