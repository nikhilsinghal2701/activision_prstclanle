public with sharing class Survey_Result_Page_Controller {
    private CSAT_Survey_Response__c[] surveys = [Select Id,OwnerId From CSAT_Survey_Response__c];
    private Group queue = [Select Id From Group Where Name = 'CSAT Survey Response Queue'];
    private User[] cleanlist{get;set;}
    private Boolean firstrun = True;
    public integer totalPages{get;set;}
    public string userId{get;set;}
    public Id detailId{get;set;}
    public string email{get;set;}
    public string contactedstatus{get;set;}
    public ApexPages.StandardSetController ssc{get;set;}
    public List<CSAT_Survey_Response__c> records {
        get{
            return (List<CSAT_Survey_Response__c>) ssc.getRecords();
        }
    }   
    
       
    public Survey_Result_Page_Controller(){
        Set<ID> userIDs = new Set<ID>();
        for(CSAT_Survey_Response__c u : surveys){
          userIDs.add(u.OwnerId);
        }
        cleanlist = [Select Id, Name From User Where Id IN :userIDs Order By Name ASC];
           
        referencerecord = new CSAT_Survey_Response__c();
        this.Refresh();
    }
    
    public CSAT_Survey_Response__c referencerecord {get;set;}
        
    public PageReference Refresh(){
      
        Database.Querylocator responses;
        decimal dtotalPages;
        integer recordperpage=20;
        string query = 'Select Id From CSAT_Survey_Response__c Where Name = Null';
        system.debug('Email='+email+' userID='+userId+' referencerecord.Status__c='+referencerecord.Status__c+' referencerecord.Reason__c='+referencerecord.Reason__c+' contactedstatus='+contactedstatus);
   
      if(firstrun == False){
        query = 'Select Id, Name, Gamer__r.Name, Case__r.CaseNumber, Agent_Full_Name__c, Email_Address__c, Status__c, Reason__c, Contacted__c From CSAT_Survey_Response__c Where Name != Null';

        if(userId != Null && userId != 'None'){
            query+=' And OwnerId = '+'\''+userID+'\'';
        }
        
        if(referencerecord.Status__c != Null && referencerecord.Status__c != ''){
            query+= ' And Status__c = '+'\''+referencerecord.Status__c+'\'';
        }
        
        if(referencerecord.Reason__c != Null && referencerecord.Reason__c != ''){
            query+= ' And Reason__c = '+'\''+referencerecord.Reason__c+'\''; 
        }
        
        if(contactedstatus != Null && contactedstatus != 'None'){
            query+= ' And Contacted__c= '+contactedstatus;
        }
        if(email != Null && email != ''){
            query+= ' And Email_Address__c= '+'\''+string.escapeSingleQuotes(email)+'\'';
        }
        
        query+=' Order By Name ASC';
        system.debug('Query String= '+query);
      }  

        responses=database.getQueryLocator(query);
        ssc = new ApexPages.StandardSetController(responses);
        ssc.setPageSize(recordperpage);
        dtotalPages = (ssc.getResultSize() / ssc.getPageSize());
        dtotalPages = Math.floor(dtotalPages) + ((Math.mod(ssc.getResultSize(), recordperpage)>0) ? 1 : 0);
        totalPages = Integer.valueOf(dtotalPages);
        firstrun = False;
        return Null;
            
    }
    
    public PageReference populateDetailId(){
        system.debug(ApexPages.currentPage().getParameters().get('dId'));
        if(ApexPages.currentPage().getParameters().get('dId') != null && 
            ApexPages.currentPage().getParameters().get('dId') != '' && 
            ApexPages.currentPage().getParameters().get('dId') != 'null')
        {
            this.detailId = ApexPages.currentPage().getParameters().get('dId');
            system.debug(detailId);
        }
      return null;
    }
    public pageReference fixAsyncRefresh(){
        //http://salesforce.stackexchange.com/questions/8793/rerender-apexdetails-after-dml-async-method-call
        return null;
    }
    
    public List<SelectOption> getAgents(){
        List<SelectOption> agentNames = new List<SelectOption>();
        agentNames.add(new SelectOption('None','--None--'));
        agentNames.add(new SelectOption(queue.Id,'Survey Response Queue'));
        Set<String> noDupes = new Set<String>();
        for (User u : cleanlist ){
            if(cleanlist != NULL){
                if(!(noDupes.contains(u.Id))){
                    agentNames.add(new SelectOption(u.Id,u.Name));
                    noDupes.add(u.Id);
                }
            }   
        }
        
        return agentNames;
    }


}