@isTest
private class Survey_Result_Page_Controller_Test {
	
	@isTest static void test_method_one() {
		Group queue = [Select Id From Group Where Name = 'CSAT Survey Response Queue' Limit 1];
		User u = UnitTestHelper.generateTestUser();
		u.ProfileId=[select Id from Profile where Name = 'System Administrator' Limit 1].Id;
		insert u;
		Contact c = UnitTestHelper.generateTestContact();
		insert c;
		Case ca = UnitTestHelper.generateTestCase();
		ca.Comments__c = 'Test';
		ca.ContactId = c.Id;
		insert ca;
		system.debug('Contact ID for Case= '+ca.ContactId);
		Case cawithnumber = [Select Id, CaseNumber From Case Where Id =:ca.Id];
		string casenumber = cawithnumber.CaseNumber;
		string casenumbernozero = casenumber.subString(1);
		CSAT_Survey_Response__c csr = new CSAT_Survey_Response__c(Gamer__c = c.Id, Case_Number__c = casenumbernozero, Status__c = 'Open', Reason__c ='Product', Contacted__c = True);
		insert csr;
		csr.OwnerId = u.Id;
		update csr;
		List<SelectOption> testvalues = new List<SelectOption>();
		testvalues.add(new SelectOption('None','--None--'));
		testvalues.add(new SelectOption(queue.Id,'Survey Response Queue'));
		testvalues.add(new SelectOption(u.Id,[Select Name From User Where Id=:u.Id].Name)); 
		pagereference currentpage = Page.Survey_Result_Page;
		Test.setCurrentPage(currentpage);
		ApexPages.StandardSetController tssc = new ApexPages.StandardSetController([Select Id, Name, Gamer__r.Name, Case__r.CaseNumber, Agent_Full_Name__c, Email_Address__c, Status__c, Reason__c, Contacted__c From CSAT_Survey_Response__c Where Name!= Null]);
		Survey_Result_Page_Controller srpc = new Survey_Result_Page_Controller();
		srpc.userId = u.Id;
		srpc.referencerecord.Status__c = 'Open';
		srpc.referencerecord.Reason__c = 'Product';
		srpc.contactedstatus = 'True';
		srpc.email = 'abc@atvi.com';
		PageReference p1 = srpc.Refresh();
		CSAT_Survey_Response__c queryobj = [Select Id, Email_Address__c From CSAT_Survey_Response__c];
		currentpage.getParameters().put('dId',queryobj.Id);
		PageReference p2 = srpc.populateDetailId();
		PageReference p3 = srpc.fixAsyncRefresh();
		List<SelectOption> l = srpc.getAgents();
        system.assertEquals(testvalues,l);
		system.assertEquals(tssc.getRecords(),srpc.records);

		
	}
	
	
	
}