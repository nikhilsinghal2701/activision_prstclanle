/*******************************************************************************
 * Name         -   TestCampMember
/**
* @author           sumankrishnasaha
* @date             Nov-2011
* @description      Test class for campMember class
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* sumankrishnasaha          Nov-2011                Created
* ---------------------------------------------------------------------------------------------------
* Review Log  :
*---------------------------------------------------------------------------------------------------
* Reviewer                      Review Date            Review Comments
* --------                      ------------          --------------- 
*---------------------------------------------------------------------------------------------------
*/
@isTest
private class TestCampMember {

    static testMethod void myUnitTest() {
        
        Contact contactRecord = new Contact(lastName = 'testgamer');
        insert contactrecord;
        system.debug(contactrecord.id + '@@@');
        //contactRecord = [select id from contact where id = '003Z0000001vEQW'];
        PageReference pageRef = Page.Gamers1;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', contactrecord.id);
        
        system.debug(ApexPages.currentPage().getParameters().get('id'));
        Apexpages.StandardController cnt = new Apexpages.StandardController(contactrecord);
        system.debug(cnt + 'this is cnt');
        campMember campMemberObj = new campMember(cnt);

        campMemberObj.getcontCmpgns();
        campMemberObj.getCases();
        campMemberObj.getAssets();
        campMemberObj.getGameplay();
        campMemberObj.getTweets();
        campMemberObj.getFbPosts();
    }
}