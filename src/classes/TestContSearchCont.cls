/*******************************************************************************
* Name              TestContSearchCont
* @author           sumankrishnasaha
* @date             Nov-2011
* @description      Test class for contSearchCont class
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* sumankrishnasaha          Nov-2011                Created
* ---------------------------------------------------------------------------------------------------
* Review Log  :
*---------------------------------------------------------------------------------------------------
* Reviewer                      Review Date            Review Comments
* --------                      ------------          --------------- 
*---------------------------------------------------------------------------------------------------
*/
@isTest
private class TestContSearchCont  {
    static testMethod void myUnitTest() {
        contSearchCont contSearchContObj = new contSearchCont();
        contSearchContObj.searchText = 'Thomas, Ruben';
        contSearchContObj.searchMeth();
    }
}