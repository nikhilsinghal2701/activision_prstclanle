/**
* @author           Aishwarya
* @date             12/16/2011
* @description      Test class for the trigger - Event_to_Winner					
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Aishwarya					16-Dec-2011				Created           
*---------------------------------------------------------------------------------------------------
* Review Log	:
*---------------------------------------------------------------------------------------------------
* Reviewer											Review Date						Review Comments
* --------											------------					--------------- 
* Karthik (Deloitte Consultant)						19-Apr-2012						Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
@isTest
public class TestEventtoWinnerTrigger {
    public static testMethod void testTrigger() {
		List<Winner__c> testWinners = new List<Winner__c>();
		//Create a single Test Event record
		Event__c ev = new Event__c(Name = 'Test Event');
        insert ev;
        //Create test Winner records
        for(Integer i=0; i<200; i++) {
        	Winner__c win = new Winner__c(Event__c = ev.Id);
        	testWinners.add(win);
        }
        insert testWinners;
        //Start of test        
        Test.startTest();
        ev.Status__c = Label.Event_Status;
        update ev;   
        //Assert Approved field of updated Winner records to be True
        for(Winner__c w : [SELECT Approved__c from Winner__c
        				   WHERE Event__c =: ev.Id])
        	System.assert(w.Approved__c == True);
        Test.stopTest();
        //End of test
    }    
}