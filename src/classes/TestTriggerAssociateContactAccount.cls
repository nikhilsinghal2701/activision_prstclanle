/**
* @author           Aishwarya
* @date             12/16/2011
* @description      Test class for the triggers - associateContactAccount and
					PopulateLastNameWithNA				
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Aishwarya					16-Dec-2011				Created           
*---------------------------------------------------------------------------------------------------
* Review Log	:
*---------------------------------------------------------------------------------------------------
* Reviewer											Review Date						Review Comments
* --------											------------					--------------- 
* Karthik (Deloitte Consultant)						19-Apr-2012						Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
@isTest
private class TestTriggerAssociateContactAccount {
    static testMethod void myUnitTest() {
    	List<Contact> testCons = new List<Contact>();
    	//Create 200 test contacts
        for(Integer i=0; i<200; i++) {
        	Contact con = UnitTestHelper.generateTestContact();
        	con.LastName = 'Test' + i;
        	testCons.add(con);
        }
        //Start of test
        Test.startTest();
        insert testCons;
        //Assert Account Ids of inserted contacts that shouldn't be null
        for(Contact c : [SELECT AccountId from Contact where Id IN: testCons])
        	System.assert(c.AccountId != NULL);
        Test.stopTest();
        //End of test        
    }
}