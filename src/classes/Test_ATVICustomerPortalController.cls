@isTest
/**
* @author      Mohith Kumar 
* @date        27/02/2012
* @description Test coverage class for  ATVICustomerPortalController class

* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                   Date                    Description
* ---------------             -----------             ----------------------------------------------
* Mohith Kumar                14/03/2012              Original Version
*---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                    Review Comments
* --------                                          ------------                   ---------------
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                    Final Inspection before go-live

*******************************************************************************************************/

public with sharing class Test_ATVICustomerPortalController {

@future
private static void createUserwRole(Id roleId){
    User testuser=UnitTestHelper.generateTestUser();
    testuser.UserRoleId=roleId;
    insert testuser;
}


public static testmethod void Customerportaltest()

{  
    test.startTest();
    UserRole testRole;
    User testUser;
    List<User> users=[select id, name from User where UserRoleId<>null And Profile.name='System Administrator' AND isActive=true limit 1];
    if(users.size()>0) testUser=users.get(0);
    
        account testaccount=UnitTestHelper.generateTestAccount();
        testaccount.OwnerId=testUser.Id;
        insert testaccount;
        
        contact testcontact=UnitTestHelper.generateTestContact();
        testcontact.AccountId=testaccount.Id;       
        insert testcontact;
        
        Public_Product__c testproduct=UnitTestHelper.generatePublicProduct();
        insert testproduct;     
        
        User testportaluser=UnitTestHelper.getCustomerPortalUser('abc', testcontact.Id);
        insert testportaluser;
        
         System.runAs(testportaluser)
         
         {
        
        ATVICustomerPortalController atviclass=new ATVICustomerPortalController ();
        atviclass.getUserLanguage();
        atviclass.setUserLanguage('English');
        atviclass.getSelectedLang();
        atviclass.setSelectedLang('English');
        atviclass.doUserNameLoad();
        atviclass.refreshLanguage();
        atviclass.getIsloggedIn();
        atviclass.getproduct();
        atviclass.getMypopgames();
        atviclass.doUserNameLoad();
        atviclass.doProductListLoad();
       
        
        AtviUtility utility=new AtviUtility();
        atviclass.cod_mw3();
        atviclass.getSelected_cod_mw3();
        atviclass.getSelected_cod_blackops();
        atviclass.getSelected_goldeneye();
        atviclass.getIs_goldeneye();
        atviclass.getSelected_goldeneye();
        atviclass.transformers();
        atviclass.getIs_transformers();
        atviclass.getSelected_transformers(); 
        atviclass.spiderman();
        atviclass.getIs_spiderman(); 
        atviclass.getSelected_spiderman(); 
        atviclass.xmen() ;
        atviclass.getIs_xmen(); 
        atviclass.getSelected_xmen () ;
        atviclass.prototype(); 
        atviclass.getIs_prototype(); 
        atviclass.getSelected_prototype(); 
        atviclass.getBranding();
        atviclass.getisloggedin();
        atviclass.getIs_cod_mw3();
        atviclass.cod_blackops(); 
        atviclass.getIs_cod_blackops();
        atviclass.goldeneye(); 
        
         }
        test.stopTest();
   
}

}