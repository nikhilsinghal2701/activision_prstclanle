@isTest
private class Test_ATVI_Case_Pickup {
  public static testmethod void test() {
        Account testAcc=UnitTestHelper.generateTestAccount();
        insert testAcc;

        Group grp = new Group(Name='Queue',Type='Queue');
        insert grp;
        
        QueueSobject mappingObject = new QueueSobject(QueueId = grp.Id, SobjectType = 'Case');
        System.runAs(new User(Id = UserInfo.getUserId()))
        {insert mappingObject;}
        
        Case testCase=UnitTestHelper.generateTestCase();
        testCase.AccountId=testAcc.Id;
        testCase.Status='Open';
        testCase.Return_Code__c='test';
        testCase.Comments__c='Test';
        testCase.ownerid=grp.id;
        insert testCase;
        
        List<case> lstCase= new List<case>();
        lstCase.add( [ Select Id From case ] );
        ATVI_Case_Pickup sSetContACP = new ATVI_Case_Pickup(new ApexPages.StandardSetController( lstCase));
        sSetContACP.pickupCases();
  }
}