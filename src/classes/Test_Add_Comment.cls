@isTest
private class Test_Add_Comment {

    static testMethod void myUnitTest() {
    	
    	//Create an Agent
    	User Agent = new User(FirstName='Test',LastName='Agent',Alias='testag',Email='testagent@activision.com',Username='testagent@activision.com',CommunityNickname='testagent@atvi',
    	UserRoleid =[Select Id From UserRole Where Name = 'Agent'].id,ProfileId=[Select Id From Profile Where Name = 'CS - Agent'].Id,EmailEncodingKey='ISO-8859-1',TimeZoneSidKey='America/Los_Angeles',LocaleSidKey='en_US',LanguageLocaleKey='en_US');
    	                      
    	insert Agent;
    	
    	//Create and Account
    	Account newAccount = new Account(Name='Test Account');
    	system.runAs(Agent){
    		insert newAccount;
    	}
    	
    	//Create a Contact
    	Contact newContact = new Contact(AccountId=newAccount.id,FirstName='Test', LastName='Person',Email='testperson@test.com' );
    	system.runAs(Agent){
    		insert newContact;
    	}
    	
    	//Create new Case
    	Case newCase = new Case(RecordTypeId=[Select Id From RecordType Where Name = 'General Support'].Id,
    	                                      ContactId=newContact.Id,Origin='Web',Status='New',Subject='Test Case ',Description='Test issue',Comments__c='New Comment');
        system.runAs(Agent){
        	insert newCase;
        }
        
        //Create a Comment
        Case_Comment__c newCaseComment = new Case_Comment__c(Case__c=newCase.Id,Origin__c='Phone',Comments__c='This is a case comment');
        
        system.runAs(Agent){
        	insert newCaseComment;
        }
        
        CaseComment newStandardComment = new CaseComment(CommentBody='This is case comment',ParentId=newCase.Id);
        
        system.runAs(Agent){
        	insert newStandardComment;
        }
        
        Case_Comment__c newCaseComment2 = new Case_Comment__c(Case__c=newCase.Id,Origin__c='Phone',Comments__c='This is a case comment',Case_Comment_Id__c=String.valueOf(newStandardComment.id));
        
        
        system.runAs(Agent){
        	insert newCaseComment2;
        }
    }   
    
}