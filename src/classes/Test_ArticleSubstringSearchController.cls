/**
**Test_ArticleSubstringSearchController - test class for AtviArticleSubstringSearchController
**Created 18.03.12 - abpal
**/
@isTest
public class Test_ArticleSubstringSearchController{
    public static testMethod void testConstructors(){
        Pkb_controller pkbconTest=new Pkb_controller();
        PageReference testPage=Page.AtviArticleSubstringSearch;
        KnowledgeArticleVersion testKav=new KnowledgeArticleVersion();
        Test.setCurrentPage(testPage);
        
        Test.startTest();
        ApexPages.StandardController scon=new ApexPages.StandardController(testKav);
        AtviArticleSubstringSearchController testConPage=new AtviArticleSubstringSearchController(scon);
        
        AtviArticleSubstringSearchController testCon=new AtviArticleSubstringSearchController();
        AtviArticleSubstringSearchController testConPkb=new AtviArticleSubstringSearchController(pkbconTest);
        
        Test.stopTest();
        
    
    }

}