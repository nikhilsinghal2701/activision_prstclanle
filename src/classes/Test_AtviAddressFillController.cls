/**
* @author      A. Pal(Deloitte Senior Consultant)   
* @date        20/03/2012  
* @description Test coverage class for  AtviAddressFillController class 

* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                               Date                      Description
* ---------------                        -----------                ----------------------------------------------
* A. Pal(Deloitte Senior Consultant)      20/03/2012                Original Version
*---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                    Review Comments
* --------                                          ------------                   --------------- 
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                    Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
@isTest
private class Test_AtviAddressFillController {

    static testMethod void myUnitTest() {
        Contact testContact = new Contact(lastname = 'testContact');
        insert testContact;
        Test.startTest();
        PageReference pageRef = Page.ATVI_RMA_Page;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('def_contact_id', testContact.id);
        AtviRmaAddressFillController controller = new AtviRmaAddressFillController(null);
        AtviRmaAddressFillController.populateRmaVariables();
        AtviRmaAddressFillController.getBeforeQString();
        Test.stopTest();
    }
    
    static testMethod void myUnitTestNegative() {
    	Test.startTest();
        PageReference pageRef = Page.ATVI_RMA_Page;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('def_contact_id', '');
        AtviRmaAddressFillController controller = new AtviRmaAddressFillController(null);
        AtviRmaAddressFillController.populateRmaVariables();
        AtviRmaAddressFillController.getBeforeQString();
        Test.stopTest();
    }
}