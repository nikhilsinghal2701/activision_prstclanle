@isTest
/*******************************************************************************
 * Name         -   Test_AtviArticleToCaseController
 * Description  -   Test Class for AtviArticleToCaseController. 
 
 * Modification Log :
 * ---------------------------------------------------------------------------
 * Developer                  Date                      Description
 * ---------------------------------------------------------------------------
 * A.Pal(Deloitte Senior Consultant)          08-Feb-12                 Created
 *---------------------------------------------------------------------------------------------------
* Review Log	:
*---------------------------------------------------------------------------------------------------
* Reviewer											Review Date						Review Comments
* --------											------------					--------------- 
* A. Pal(Deloitte Senior Consultant)				19-APR-2012						Final Inspection before go-live
---------------------------------------------------------------------------------------------------**/
public with sharing class Test_AtviArticleToCaseController {
    static SObject kavObj;
    public static testMethod void testSaveArticleTitle()
    {
    FAQ__kav testFaq=new FAQ__kav();
    testFaq.Title='Test Faq';
    testFaq.UrlName='Test-Faq';
    testFaq.Summary='Test Summary';
    testFaq.Language='en_US';
    insert testFaq;
    
    PageReference pref=Page.Atvi_faqKav_Override;
    
    Test.setCurrentPage(pref);   
    
    
    Test.startTest();
    
    //KnowledgeArticleVersion TestkavObj = UnitTestHelper.setupKavData();
    Cookie aCookie=new Cookie(UserInfo.getSessionId(),'VisistedArticle',null,-1,false);
    
    ApexPages.currentPage().getParameters().put('id',testFaq.KnowledgeArticleId);
    ApexPages.currentPage().setCookies(new Cookie[]{aCookie}); 
    
    ApexPages.StandardController sc = new ApexPages.standardController(testFaq);
    // create an instance of the controller
    AtviArticleToCaseController myPageCon = new AtviArticleToCaseController(sc);
    
    pref=AtviArticleToCaseController.saveArticleTitle();
    
    Test.stopTest();   
    
    }
    
     public static testMethod void testRateAnArticle(){
     	
     	FAQ__kav testFaq=new FAQ__kav();
	    testFaq.Title='Test Faq';
	    testFaq.UrlName='Test-Faq';
	    testFaq.Summary='Test Summary';
	    testFaq.Language='en_US';
	    insert testFaq;
	    
	    ApexPages.Standardcontroller sCon=new ApexPages.Standardcontroller(testFaq);
	    AtviArticleToCaseController acon=new AtviArticleToCaseController(scon);
	    
	    UserRole testRole;    
	   	User testUser;
	    
	    List<User> users=[select id, name from User where UserRoleId<>null And Profile.name='System Administrator' AND isActive=true limit 1];
	    if(users.size()>0) testUser=users.get(0);    
        
        account testaccount=UnitTestHelper.generateTestAccount();
        testaccount.OwnerId=testUser.Id;
        insert testaccount;
        
        contact testcontact=UnitTestHelper.generateTestContact();
        testcontact.AccountId=testaccount.Id;       
        insert testcontact;        
             
        User testportaluser=UnitTestHelper.getCustomerPortalUser('abc', testcontact.Id);
        insert testportaluser;
        
        System.runAs(testportaluser){
        	Test.startTest();
        		if(acon.isPortalUser){
        			acon.kaIdforRating=testFaq.KnowledgeArticleId;
        			acon.voteType='5';
        			system.debug('Calling rateAnArticle()');
        			PageReference testpage=acon.rateAnArticle();
        			acon.kaIdforRating=testFaq.KnowledgeArticleId;
        			acon.voteType='1';
        			system.debug('Calling rateAnArticle() again');
        			PageReference testpage2=acon.rateAnArticle();
        		}
        	Test.stopTest();
        }
	     	
     }
    
    
}