/**
* @author      Manas Jain  
* @date        20/03/2012  
* @description Test coverage class for  AtviArticletoCaseForm class and hence test that successful case creation happens when customer portal user logs a case through the form

* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                   Date                    Description
* ---------------             -----------             ----------------------------------------------
* Manas Jain                  20/03/2012              Original Version
* Mohith Kumar                22/04/2012              Improved to increase test code coverage 
*---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                    Review Comments
* --------                                          ------------                   ---------------
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                    Final Inspection before go-live

*******************************************************************************************************/

@isTest
public with sharing class Test_AtviArticletoCaseForm {
    public static testMethod void setupTest() {
    
        CheckAddError.addError = false;     
        
        Account testaccount=new Account();
        testaccount.Name='Test account';
        testaccount.Type__c='Agency';
        insert testaccount;
        
        Contact testContact=new Contact();
        testContact.LastName='Test User';
        testContact.Languages__c='English';
        testContact.MailingCountry='US';
        testContact.AccountId=testaccount.id;
        insert testContact;
        
        User userObject = UnitTestHelper.getCustomerPortalUser('foo',testContact.id);
        insert userObject;
        
        
        
        Test.startTest();
        
     
    System.runAs(new User(Id=UserInfo.getUserId())){
      User testuser=[Select Subscriptions__c, Street, State,Id,
                              PostalCode, Phone, MobilePhone, LastName,
                              Gender__c, FirstName, Email, Country, ContactId, IsPortalEnabled,
                              City, Birthdate__c, LanguageLocaleKey From User where id=:userObject.id LIMIT 1];
                              
        testuser.LanguageLocaleKey='fr';
        testuser.MobilePhone='1234'; 
        testuser.Subscriptions__c='Activision Value';
        testuser.Country='India';
        testuser.Birthdate__c=System.today();
        testuser.Gender__c='M';
        testuser.Phone='1234';
        testuser.Street='teststreet';
        testuser.City='testcity';
        update testuser;
       } 
       
       Case testRMACase=UnitTestHelper.generateTestCase();
        testRMACase.AccountId=testaccount.Id;
        testRMACase.ContactId=testContact.Id;
        testRMACase.Type='RMA';
        testRMACase.Status='Open';
        insert testRMACase;
        ApexPages.StandardController controller = new ApexPages.StandardController(testRMACase);
        
       
       string EmailAddress='test@force.conm';
       
        System.runAs(userObject){
            
            AtviArticletoCaseForm obj = new AtviArticletoCaseForm(controller);
            
            obj.setsubject('missing token');
            obj.setdescription('testing....');
            obj.setUserLanguage('en_US');
            obj.casecreate();
            obj.getdescription();
            obj.getsubject();
            obj.getUserLanguage();
            
            Case cs = new Case();
            insert cs;
        
        }
        
            
            Test.stopTest();
    
    }
    

    
}