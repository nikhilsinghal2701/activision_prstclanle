@istest
public with sharing class Test_AtviArticletoCaseForm_SpecOps
{
    public static testMethod void setupTest()
    {
        checkAddError.addError = false;
        Account testAccount= new Account();
        testAccount.Name = 'Test account';
        testAccount.Type__c = 'Test User';
        insert testAccount;
    
        Contact testContact = new Contact();
        testContact.LastName = 'Test User';
        testContact.Languages__c = 'English';
        testContact.MailingCountry = 'US';
        testContact.AccountId = testAccount.id;
        insert testContact;
        
        User userObject = UnitTestHelper.getCustomerPortalUser('foo', testContact.id);
        insert userObject;
        
        Test.startTest();
    
        User testUser = 
            [SELECT Subscriptions__c, Street, State, Id,
                PostalCode, Phone, MobilePhone, LastName,
                Gender__c, FirstName, Email, Country, ContactId,
                IsPortalEnabled, City, Birthdate__c, LanguageLocaleKey
             FROM User
             WHERE Id = :userObject.id LIMIT 1];
           
        testUser.LanguageLocaleKey = 'fr';
        testUser.MobilePhone = '1234';
        testuser.Subscriptions__c='Activision Value';
        testuser.Country='India';
        testuser.Birthdate__c=System.today();
        testuser.Gender__c='M';
        testuser.Phone='1234';
        testuser.Street='teststreet';
        testuser.City='testcity';
        update testUser;
           
        Case testSpecOpsCase = UnitTestHelper.generateTestCase();

        testSpecOpsCase.AccountId = testAccount.Id;
        testSpecOpsCase.ContactId = testContact.Id;
        
        testSpecOpsCase.Description = 'This is a Description';
        testSpecOpsCase.Gamertag__c = 'Gamertag';
        testSpecOpsCase.SEN_ID__c = 'SEN ID';
        testSpecOpsCase.Error_Reason__c = 'This is an Error Reason';
        testSpecOpsCase.Subject = 'This is Subject';
        testSpecOpsCase.RecordTypeId = '012U0000000Q6zg';
        testSpecOpsCase.W2C_Origin_Language__c = 'en_US';
        
        testSpecOpsCase.Type = 'RMA';   
        testSpecOpsCase.Status = 'Open';
        insert testSpecOpsCase ;
        
        ApexPages.StandardController controller = new Apexpages.StandardController(testSpecOpsCase);
        
        string EmailAddress = 'test@force.com';
        
        System.runAs(userObject)
        {
            AtviArticleToCaseForm_SpecOps passObj = new AtviArticleToCaseForm_SpecOps(controller);
            
            passobj.EmailAddress = 'test@force.com';
            passObj.Gamertag = 'g_Tag';
            passObj.description = 'Description';
            passObj.userLanguage = 'en_US';
            passObj.subject = 'Subject';
            passObj.rank = '12';
            passObj.setdescription('testing...');
            passObj.getdescription();
            passObj.setsubject('missing token');
            passObj.getsubject();
            passObj.setUserLanguage('en_US');
            passObj.getUserLanguage();
            passObj.casecreate();
            
            AtviArticleToCaseForm_SpecOps failObj_1 = new AtviArticleToCaseForm_SpecOps(controller); //FAIL Blank Rank
            
            failObj_1.EmailAddress = 'test@force.com';
            failObj_1.Gamertag = 'g_Tag';
            failObj_1.description = 'Description';
            failObj_1.userLanguage = 'en_US';
            failObj_1.subject = 'Subject';
            failObj_1.rank = '';
            
            try{ failObj_1.casecreate(); } 
            catch(Exception ex) { }
            
            AtviArticleToCaseForm_SpecOps failObj_2 = new AtviArticleToCaseForm_SpecOps(controller); //FAIL Blank Gtag
            
            failObj_2.EmailAddress = 'test@force.com';
            failObj_2.Gamertag = '';
            failObj_2.description = 'Description';
            failObj_2.userLanguage = 'en_US';
            failObj_2.subject = 'Subject';
            failObj_2.rank = '54';
            
            try{ failObj_2.casecreate(); } 
            catch(Exception ex) { }
                  
            Case cs = new Case();
            insert cs;
        }           
        Test.stopTest();
    }
}