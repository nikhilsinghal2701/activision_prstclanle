@istest
public with sharing class Test_AtviArticletoCaseForm_Token
{
    public static testMethod void setupTest()
    {
        CheckAddError.addError = false;
        
        Account testAccount= new Account();
        testAccount.Name = 'Test account';
        testAccount.Type__c = 'Test User';
        insert testAccount;
    
        Contact testContact = new Contact();
        testContact.LastName = 'Test User';
        testContact.Languages__c = 'English';
        testContact.MailingCountry = 'US';
        testContact.AccountId = testAccount.id;
        insert testContact;
        
        User userObject = UnitTestHelper.getCustomerPortalUser('foo', testContact.id);
        insert userObject;
        
        Test.startTest();
    
        User testUser = 
            [SELECT Subscriptions__c, Street, State, Id,
                PostalCode, Phone, MobilePhone, LastName,
                Gender__c, FirstName, Email, Country, ContactId,
                IsPortalEnabled, City, Birthdate__c, LanguageLocaleKey
             FROM User
             WHERE Id = :userObject.id LIMIT 1];
           
        testUser.LanguageLocaleKey = 'fr';
        testUser.MobilePhone = '1234';
        testuser.Subscriptions__c='Activision Value';
        testuser.Country='India';
        testuser.Birthdate__c=System.today();
        testuser.Gender__c='M';
        testuser.Phone='1234';
        testuser.Street='teststreet';
        testuser.City='testcity';
        update testUser;
           
        Case testTokenCase = UnitTestHelper.generateTestCase();

        testTokenCase.AccountId = testAccount.Id;
        testTokenCase.ContactId = testContact.Id;
        
        testTokenCase.Description = 'This is a Description';
        testTokenCase.Gamertag__c = 'Gamertag';
        testTokenCase.SEN_ID__c = 'SEN ID';
        testTokenCase.Token_Code__c = '1234567890123';
        testTokenCase.Error_Reason__c = 'This is an Error Reason';
        testTokenCase.Subject = 'This is Subject';
        testTokenCase.RecordTypeId = '012U0000000Q6m2';
        testTokenCase.W2C_Origin_Language__c = 'en_US';
        
        testTokenCase.Type = 'RMA';   
        testTokenCase.Status = 'Open';
        insert testTokenCase;
        
        ApexPages.StandardController controller = new Apexpages.StandardController(testTokenCase);
        
        string EmailAddress = 'test@force.com';
        
        System.runAs(userObject)
        {
            AtviArticleToCaseForm_Token passObj = new AtviArticleToCaseForm_Token(controller);
            
            passobj.EmailAddress = 'test@force.com';
            passObj.Gamertag = 'g_Tag';
            passObj.description = 'Description';
            passObj.userLanguage = 'en_US';
            passObj.subject = 'Subject';
            passObj.SENID = 'sen_ID';
            passObj.Token = '1234567890123';
            passObj.error = 'Error Message';
            passObj.setdescription('testing...');
            passObj.getdescription();
            passObj.setsubject('missing token');
            passObj.getsubject();
            passObj.setUserLanguage('en_US');
            passObj.getUserLanguage();
            passObj.casecreate();
            
            AtviArticleToCaseForm_Token failObj_1 = new AtviArticleToCaseForm_Token(controller); //FAIL Dashes
            
            failObj_1.EmailAddress = 'test@force.com';
            failObj_1.Gamertag = 'g_Tag';
            failObj_1.description = 'Description';
            failObj_1.userLanguage = 'en_US';
            failObj_1.subject = 'Subject';
            failObj_1.SENID = 'sen_ID';
            failObj_1.Token = '123-123-123';
            failObj_1.error = 'Error Message';
            
            try{ failObj_1.casecreate(); } 
            catch(Exception ex) { }
            
            AtviArticleToCaseForm_Token failObj_2 = new AtviArticleToCaseForm_Token(controller); //FAIL SPEC CHRS
            
            failObj_2.EmailAddress = 'test@force.com';
            failObj_2.Gamertag = 'g_Tag';
            failObj_2.description = 'Description';
            failObj_2.userLanguage = 'en_US';
            failObj_2.subject = 'Subject';
            failObj_2.SENID = 'sen_ID';
            failObj_2.Token = '123*123@123';
            failObj_2.error = 'Error Message';
            
            try{ failObj_2.casecreate(); } 
            catch(Exception ex) { }
            
            AtviArticleToCaseForm_Token failObj_3 = new AtviArticleToCaseForm_Token(controller); //FAILS BLANK GT
            
            failObj_3.EmailAddress = 'test@force.com';
            failObj_3.Gamertag = '';
            failObj_3.description = 'Description';
            failObj_3.userLanguage = 'en_US';
            failObj_3.subject = 'Subject';
            failObj_3.SENID = '';
            failObj_3.Token = '1234567890123';
            failObj_3.error = 'Error Message';

            try{ failObj_3.casecreate(); } 
            catch(Exception ex) { }
            
            AtviArticleToCaseForm_Token failObj_4 = new AtviArticleToCaseForm_Token(controller); //FAILS SHORT CODE
            
            failObj_4.EmailAddress = 'test@force.com';
            failObj_4.Gamertag = 'test';
            failObj_4.description = 'Description';
            failObj_4.userLanguage = 'en_US';
            failObj_4.subject = 'Subject';
            failObj_4.SENID = '';
            failObj_4.Token = '123';
            failObj_4.error = 'Error Message';

            try{ failObj_4.casecreate(); } 
            catch(Exception ex) { }
            
                  
            Case cs = new Case();
            insert cs;
        }           
        Test.stopTest();
    }
}