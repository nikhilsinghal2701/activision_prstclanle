/*************************************************************************************************
* @Name             Test_AtviCaseDetailsController 
* @author           Mohith Kumar 
* @date             24-MAR-2012
* @description      Test Class to increase the code coverage of AtviCaseDetailsController Class
* Modification Log :
* ------------------------------------------------------------------------------------------------
* Developer                   Date                     Description
* ---------------             -----------              ----------------------------------------------
*  Mohith Kumar               26-Mar-12               Test Class for AtviCaseDetailsController
*  Sam Akiki                  01-May-14               Updated Class for more coverage 
*---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                     Review Comments
* --------                                          ------------                    ---------------- 
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                     Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
@isTest
private class Test_AtviCaseDetailsController {
    

    static testMethod void myUnitTest() { 
     
        CheckAddError.addError = false;     
        
        Account testAcc=UnitTestHelper.generateTestAccount();
        insert testAcc;
        
        Contact testContact=UnitTestHelper.generateTestContact();
        testContact.AccountId=testAcc.Id;
        insert testContact;
        
        User testPortalUser=UnitTestHelper.getCustomerPortalUser('tUser', testContact.Id);
        insert testPortalUser;
        system.debug(testPortalUser);
        
        Test.startTest();
        
        System.runAs(testPortalUser){
        	
            Case testRMACase=UnitTestHelper.generateTestCase();
            testRMACase.AccountId=testAcc.Id;
            testRMACase.ContactId=testContact.Id;
            testRMACase.Type='RMA';
            testRMACase.Status='Open';
            testRMACase.Return_Code__c='test';
            testRMACase.Gamer__c=testPortalUser.Id;
            insert testRMACase;
            system.debug('Test Case Contact ID is ' + testRMACase.ContactId);
            system.debug('Test Case User ID is ' + testRMACase.Gamer__c);
        
            Case_Comments_Log__c rmaCaseCommentLog = new Case_Comments_Log__c(Case__c=testRMACase.id,Comment_Body__c='Test Comment');
            insert rmaCaseCommentLog;
        
            Case testnonRMACase=UnitTestHelper.generateTestCase();
            testnonRMACase.AccountId=testAcc.Id;
            testnonRMACase.ContactId=testContact.Id;
            testnonRMACase.Gamer__c=testPortalUser.Id;
            testnonRMACase.Type='General Support';
            testnonRMACase.Status='Open';
            insert testnonRMACase;
        
            CaseComment testnonRMACaseComment = new CaseComment(ParentId=testnonRMACase.id,CommentBody='Test Comment');
            insert testnonRMACaseComment;
        
            blob testBlob = blob.toPdf('Test Attachment');
            Attachment testnonRMAAttachment = new Attachment(ParentId=testnonRMACase.id,Description='Test Attachment',Name='Test Attachment',Body=testBlob);
            insert testnonRMAAttachment;
                
            Case testClosedCase=UnitTestHelper.generateTestCase();
            testClosedCase.AccountId=testAcc.Id;
            testClosedCase.ContactId=testContact.Id;
            testClosedCase.Gamer__c=testPortalUser.Id;
            testClosedCase.Type='General Support';
            testClosedCase.Status='Closed';
            testClosedCase.Closed_By__c=testPortalUser.Id;
            insert testClosedCase;
        
            Case_Comments_Log__c testClosedCaseCommentLog = new Case_Comments_Log__c(Case__c=testClosedCase.id,Comment_Body__c='Test Comment');
            insert testClosedCaseCommentLog;
        	
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(testnonRMACase);
            system.debug('Starting test run as test portal user');
            AtviCaseDetailsController con=new AtviCaseDetailsController(sc);
            AtviCaseDetailsController.CaseDetails caseDetails=new AtviCaseDetailsController.CaseDetails();
            caseDetails.getCaseCommentsLogList();
            caseDetails.getCaseHistoryList();
            caseDetails.getAttachmentList();
            ApexPages.currentPage().getParameters().put('id',testRMACase.Id);
            List<AtviCaseDetailsController.CaseDetails> testRMACases=con.getRmaCaseDetails();
                        
            ApexPages.currentPage().getParameters().put('id',testnonRMACase.Id);
            PageReference nonRMACasesRef=con.loadCaseDetails();
                        
            con.setUserLanguage('en_US');
            con.getUserLanguage();
            
            con.commentByGamer='Test comments by Gamer';
            PageReference commentsRef=con.saveCaseComment();
            
            PageReference attRef=con.upload();
                        
            Case testCase=new case();
        	testCase.AccountId=testAcc.Id;
        	testCase.ContactId=testContact.Id;
        	testCase.Gamer__c=testPortalUser.Id;
        	testCase.Type='RMA';
        	testCase.Status='Closed';
        	testCase.Closed_By__c=testPortalUser.Id;
        	insert testCase;
        	ApexPages.currentPage().getParameters().put('id',testCase.id); 
            PageReference reopenRef=con.reopenCase();
           	AtviCaseDetailsController.CaseDetails caseDetailObj = new AtviCaseDetailsController.CaseDetails();
           	list<Case_Comments_Log__c> caseCommsLogList=new list<Case_Comments_Log__c>();
           	caseDetailObj.setCaseCommentsLogList(caseCommsLogList);
           	list<CaseHistory> caseHistList=new list<CaseHistory>();
           	caseDetailObj.setCaseHistoryList(caseHistList);
           	list<Attachment> attList = new list<Attachment>();
           	caseDetailObj.setAttachmentList(attList);
           	           	           
            system.debug('End of test run as test portal user'+Userinfo.getUserId()+'===testPortalUser'+testPortalUser);
        }
        
        Test.stopTest();
    }
}