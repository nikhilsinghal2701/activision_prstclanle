@isTest
/**
* @author      A. Pal
* @date        27/02/2012
* @description Test coverage class for  AtviDumpArticleStatsBatch class

* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                                     Date                  Description
* ---------------                            ----------               ---------------------------------------------
* A. Pal(Deloitte Senior Consultant)          14/03/2012              Original Version
*---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                    Review Comments
* --------                                          ------------                   ---------------
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                    Final Inspection before go-live

*******************************************************************************************************/

public class Test_AtviDumpArticleStatsBatch{
    public static testMethod void testBatch() {
      	
      	Test.StartTest();
      	String objType = 'FAQ__kav';
    
    	System.debug('***** CUSTOM ARTICLE TYPE: ' +objType+ ' *****');

	    sObject kavObj = Schema.getGlobalDescribe().get(objType).newSObject();
	    kavObj.put('Title','Foo Foo Foo!!!');
	    kavObj.put('UrlName', 'foo-foo-foo');
	    kavObj.put('Summary', 'This is a summary!!! Foo. Foo. Foo.');
	    kavObj.put('Language', 'en_US');
	    insert kavObj;
       
       
       AtviDumpArticleStatsBatch testDumper=new AtviDumpArticleStatsBatch();
       String channelFlag='All';
       String testquery='Select id, ArticleNumber';
	        testquery+=', (Select id, NormalizedScore from VoteStats where Channel=:channelFlag)';
	        testquery+=', (Select id, NormalizedScore from ViewStats where Channel=:channelFlag)';
	        testquery+=' from KnowledgeArticle where archivedDate=null and lastPublishedDate!=null';
	        testquery+=' limit 1';
	   system.debug('testquery='+testquery);
	   testDumper.query=testquery;
	   Id processId=Database.executeBatch(testDumper);
       Test.StopTest();     
    }
}