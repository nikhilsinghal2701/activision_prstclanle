/**
* @author           Suman
* @date             16-FEB-2012
* @description      Test class for AtviMyCasesController
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Suman	                    16-FEB-2012             Created
* Mohith                    16-FEB-2012             Increased code coverage
* Suman	                    17-FEB-2012             Modified 
* ---------------------------------------------------------------------------------------------------
* Review Log  :
*---------------------------------------------------------------------------------------------------
* Reviewer                      			Review Date            Review Comments
* --------                      			------------           --------------- 
* A. Pal(Deloitte Senior Consultant)        19-APR-2012            Final Inspection before go-live
* --------------------------------------------------------------------------------------------------
*/
@isTest
public class Test_AtviMyCasesController {
    
    Case caseObject;
    List<Case> myCases;
    List<Case> myRmaCases;
    AtviMyCasesController atviMyCasesControllerObject;
    AtviMyCasesController atviMyCasesControllerObject2;
    
    Test_AtviMyCasesController() {
        
        test.startTest();
        
        for(integer i=0;i<=12;i++){
            myCases.add(new Case(status = 'New', origin = 'Phone', Type='General'));
        }
        insert myCases;
        
        for(integer i=0;i<=12;i++){
            myCases.add(new Case(status = 'New', origin = 'Phone', Type='RMA'));
        }
        insert myRmaCases;
        
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(myCases);
        
        ApexPages.currentPage().getParameters().put('rma','1');
        atviMyCasesControllerObject = new AtviMyCasesController(setCon);
        PageReference pref1=atviMyCasesControllerObject.nextBtnClick();
        PageReference pref2=atviMyCasesControllerObject.previousBtnClick();
        
        ApexPages.StandardSetController setCon2 = new ApexPages.StandardSetController(myRmaCases);
        ApexPages.currentPage().getParameters().put('rma','0');
        atviMyCasesControllerObject2 = new AtviMyCasesController(setCon2);
        PageReference pref3=atviMyCasesControllerObject2.nextBtnClick();
        PageReference pref4=atviMyCasesControllerObject2.previousBtnClick();
        
        integer i=atviMyCasesControllerObject2.getPageNumber();
        i=atviMyCasesControllerObject2.getPageSize();
        i=atviMyCasesControllerObject2.getTotalPageNumber();
        boolean b=atviMyCasesControllerObject2.getNextButtonDisabled();
        b=atviMyCasesControllerObject2.getPreviousButtonEnabled();
        List<Case> tcases=atviMyCasesControllerObject2.getMyCases();
        
        test.stopTest();
    }
   
    static testMethod void myNegativeTest() {
        
        Account testaccount=new Account();
        testaccount.Name='Test account';
        testaccount.Type__c='Agency';
        insert testaccount;
        
        Contact testContact=new Contact();
        testContact.LastName='Test User';
        testContact.Languages__c='English';
        testContact.MailingCountry='US';
        testContact.AccountId=testaccount.id;
        insert testContact;
        
        User userObject = UnitTestHelper.getCustomerPortalUser('foo',testContact.id);
        insert userObject;
        System.runAs(userObject) {
        test.startTest();
        Case testcase=new Case();
        testcase.ContactId=testContact.Id;
        testcase.Status='New';
        testcase.Origin='Web';
        insert testcase;
        List<Case> myCases = [SELECT id FROM Case WHERE id = :testcase.Id order by CreatedDate asc];
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(
                   myCases);
        AtviMyCasesController atviMyCasesControllerObject = new AtviMyCasesController(setCon);
        AtviMyCasesControllerObject.previousBtnClick();
        AtviMyCasesControllerObject.getMyCases();
        AtviMyCasesControllerObject.getPageNumber();
        AtviMyCasesControllerObject.getMyCases();
        AtviMyCasesControllerObject.getPageSize();
        AtviMyCasesControllerObject.getPreviousButtonEnabled();
        AtviMyCasesControllerObject.getNextButtonDisabled();
        AtviMyCasesControllerObject.getPreviousButtonEnabled();
        AtviMyCasesControllerObject.getTotalPageNumber();
        AtviMyCasesControllerObject.nextBtnClick();
       
        test.stopTest();
    }
    }
}