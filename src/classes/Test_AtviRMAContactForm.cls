@istest(seealldata=true)
public with sharing class Test_AtviRMAContactForm
{
    public static testMethod void setupTest()
    {
        CheckAddError.addError = false;
        
        Account testAccount= new Account();
        testAccount.Name = 'Test account';
        testAccount.Type__c = 'Test User';
        insert testAccount;
    
        Contact testContact = new Contact();
        testContact.LastName = 'Test User';
        testContact.Languages__c = 'English';
        testContact.MailingCountry = 'US';
        testContact.AccountId = testAccount.id;
        insert testContact;
        
        User userObject = UnitTestHelper.getCustomerPortalUser('foo', testContact.id);
        insert userObject;
        
        Test.startTest();
    
        User testUser = 
            [SELECT Subscriptions__c, Street, State, Id,
                PostalCode, Phone, MobilePhone, LastName,
                Gender__c, FirstName, Email, Country, ContactId,
                IsPortalEnabled, City, Birthdate__c, LanguageLocaleKey
             FROM User
             WHERE Id = :userObject.id LIMIT 1];
           
        testUser.LanguageLocaleKey = 'fr';
        testUser.MobilePhone = '1234';
        testuser.Subscriptions__c='Activision Value';
        testuser.Country='India';
        testuser.Birthdate__c=System.today();
        testuser.Gender__c='M';
        testuser.Phone='1234';
        testuser.Street='teststreet';
        testuser.City='testcity';
        
        system.runAs(userObject){
        	update testUser;
        }
        
           
        Case testTokenCase = UnitTestHelper.generateTestCase();

        testTokenCase.AccountId = testAccount.Id;
        testTokenCase.ContactId = testContact.Id;
        
        testTokenCase.Description = 'This is a Description';
        testTokenCase.Gamertag__c = 'Gamertag';
        testTokenCase.SEN_ID__c = 'SEN ID';
        testTokenCase.Token_Code__c = '1234567890123';
        testTokenCase.Error_Reason__c = 'This is an Error Reason';
        testTokenCase.Subject = 'This is Subject';
        testTokenCase.RecordTypeId = '012U0000000Q6m2';
        testTokenCase.W2C_Origin_Language__c = 'en_US';
        
        testTokenCase.Type = 'RMA';   
        testTokenCase.Status = 'Open';
        
        system.runAs(testUser){
        	insert testTokenCase;
        }
               
        ApexPages.StandardController controller = new Apexpages.StandardController(testTokenCase);
        
        string EmailAddress = 'test@force.com';
        
        ApexPages.currentPage().getParameters().put('prod', 'a0UU0000001b9tA');
        
        System.runAs(userObject)
        {
            Test.setCurrentPageReference(new PageReference('Page.Atvi_RMA_Contact_Us')); 
            System.currentPageReference().getParameters().put('prod', 'a0UU0000001b9tA');
            Cookie counter = new Cookie('uLang','en_US',null,-1,false);
            ApexPages.currentPage().setCookies(new Cookie[]{counter});
            
            AtviRMAContactForm passObj = new AtviRMAContactForm(controller);
            
            passobj.getUserLanguage();
            passobj.setUserLanguage('');
            passobj.casecreate();
            passobj.getprod();
            passobj.getdescription();
            passobj.setdescription('test');
            passobj.getsubject();
            passobj.setsubject('test');
            
            AtviRMAContactForm failObj = new AtviRMAContactForm(controller);
            failobj.phone='';
            failobj.description='';
            failobj.casecreate();
                       
            
            
            
        }           
        Test.stopTest();
    }
}