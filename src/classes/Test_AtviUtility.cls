/*
* @author      Abhishek Pal 
* @date        3/8/2012
* @description Test coverage class for AtviUtility

* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                   Date                    Description
* ---------------             -----------             ----------------------------------------------
* Manas Jain                19/03/2012              Original Version
*---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                    Review Comments
* --------                                          ------------                   ---------------* 
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                    Final Inspection before go-live

*******************************************************************************************************/
@isTest
public with sharing class Test_AtviUtility {
public static String KAV_EXTENSION = '__kav';
    static testMethod void testUtility(){
   
       Set<String> test =  AtviUtility.kavNames;
     AtviUtility.SearchLabelfromKey('en_US',true);
     AtviUtility.Searchkeyfromlabel('en_US');
     AtviUtility.checkNull('en_US');
    } 
    static testMethod void testUtility1()
     {
       
     AtviUtility.SearchLabelfromKey('en_US',false);
     AtviUtility.Searchkeyfromlabel('en_US');
     }
     
}