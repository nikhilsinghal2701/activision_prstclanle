/*******************************************************************************
 * Name         -   Test_Atvi_chat_controller
 * Description  -   This class provides test to the  Test_Atvi_chat_controller
 
 * Modification Log :
 * -----------------------------------------------------------------------------
 * Developer            Date        Description
 * -----------------------------------------------------------------------------
 * abpal                13.04.12       Created
 * Mohith Kumar         22.04.12      Modified to include System asserts and increased code coverage
 *---------------------------------------------------------------------------------------------------
* Review Log	:
*---------------------------------------------------------------------------------------------------
* Reviewer											Review Date						Review Comments
* --------											------------					--------------- 
* Abhishek (Deloitte Senior Consultant)				19-Apr-2012						Final Inspection before go-live
********************************************************************************************************/


@isTest
public class Test_Atvi_chat_controller {
    
    public static final string Elitestatus='Premium';
    public static testMethod void myUnitTest() {
       
       //Creation and invoking of test data from the UnitTestHelper Class 
       Account testaccount= UnitTestHelper.generateTestAccount();
       insert testaccount;
       Contact testcontact=UnitTestHelper.generateTestContact();
       testcontact.AccountId=testaccount.Id;       
       insert testcontact;
       User testportaluser=UnitTestHelper.getCustomerPortalUser('abc', testcontact.Id);
       insert testportaluser;//Create a Test Customer Portal User having Contact ID as inserted contact ID
       
        Contact con = new Contact();
        con.FirstName='test';
        con.Country__c='United States';
        con.Salutation='';
        con.LastName='test2';
        con.Email='abc@atvi.com';
        con.MailingCountry='US';
        con.Gamertag__c='testtag';
        insert con;
        
        User testportaluser2=UnitTestHelper.getCustomerPortalUser('cdf', con.Id);
        insert testportaluser2;
        
        Contact nonUScontact = new Contact();
        nonUScontact.FirstName='test2';
        nonUScontact.Country__c='France';
        nonUScontact.Salutation='';
        nonUScontact.LastName='test3';
        nonUScontact.Email='abcde@atvi.com';
        nonUScontact.MailingCountry='France';
        nonUScontact.Elite_Status__c='';
        insert nonUScontact;
        
        User testportaluser3=UnitTestHelper.getCustomerPortalUser('cdf', nonUScontact.Id);
        insert testportaluser3;
       
        test.starttest();
       
       
       //Run as a customer portal User 
       System.runas(testportaluser)
       {
       
       Atvi_chat_controller connuser=new Atvi_chat_controller();
       connuser.gamerEliteStatus=Elitestatus;
       System.assert(connuser.gamerUserName==testcontact.Gamertag__c);//Assert that gamerUserName is equal to gamer Tag
       }
       
       //Run as a Customer portal User who has no Elite_Status__c
       
       System.runas(testportaluser2)
       {
       Atvi_chat_controller connuser=new Atvi_chat_controller();
       System.assert(connuser.gamerUserName==testcontact.firstname);//Assert that gamerUserName is equal to gamer FirstName if Elite Status is absent
       }
       
       //Run as a Customer portal User who has no Elite_Status__c and Country<>US 
       
       System.runas(testportaluser3)
       {
       Atvi_chat_controller connuser=new Atvi_chat_controller();
      
       }
       
       //Run the class as a normal user  
       
       Atvi_chat_controller connuser=new Atvi_chat_controller();
       System.assert(connuser.gamerUserName=='Gamer!');//Assert that gamerUserName is Gamer once he is not a portal User
       
       test.stoptest();                
       
    }   
}