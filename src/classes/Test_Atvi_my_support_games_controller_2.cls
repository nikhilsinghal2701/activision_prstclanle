/**
* @author      Mohith Kumar 
* @date        27/02/2012
* @description Test coverage class for  Atvi_Games_Controller class

* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                   Date                    Description
* ---------------             -----------             ----------------------------------------------
* Mohith Kumar                14/03/2012              Original Version
* ------------------------------------------------------------------------------------------------
* Review Log	:
*---------------------------------------------------------------------------------------------------
* Reviewer											Review Date						Review Comments
* --------											------------					--------------- 
* Abhishek (Deloitte Senior Consultant)				19-Apr-2012						Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
@isTest
private class Test_Atvi_my_support_games_controller_2 {

    static testMethod void myUnitTest() {
        Public_Product__c prod = new Public_Product__c(Name='Test Product');
        insert prod;
        UserRole testRole;    
       User testUser;
      
      List<User> users=[select id, name from User where UserRoleId<>null And Profile.name='System Administrator' AND isActive=true limit 1];
      if(users.size()>0) testUser=users.get(0);
    
        
        account testaccount=UnitTestHelper.generateTestAccount();
        testaccount.OwnerId=testUser.Id;
        insert testaccount;
        
        contact testcontact=UnitTestHelper.generateTestContact();
        testcontact.AccountId=testaccount.Id;       
        insert testcontact;
        
             
        User testportaluser=UnitTestHelper.getCustomerPortalUser('abc', testcontact.Id);
        insert testportaluser;
        
        Asset_Custom__c ast = new Asset_Custom__c(Gamer__c = testportaluser.Id, My_Product__c = prod.Id);
        
        Test.startTest();
        System.runAs(testportaluser) {
            Atvi_my_support_games_controller_2 obj = new Atvi_my_support_games_controller_2();
            obj.addToMyGames();
            obj.getMapGamesByPlatform();
            obj.getProductNames();
            obj.getPlatformValues();
         Test.stopTest();
         
        }
    }
}