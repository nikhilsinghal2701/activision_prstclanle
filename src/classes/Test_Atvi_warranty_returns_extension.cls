/**
* @author           Karthik
* @date             27-FEB-2012
* @description      Test class for Atvi_warrant_returns_extension class
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Karthik                   27-FEB-2012             Created
* ---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                     Review Comments
* --------                                          ------------                    --------------- 
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                     Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
@isTest(seealldata=true)
private class Test_Atvi_warranty_returns_extension {

    static testMethod void myUnitTest() {
        UserRole testRole;   
        User testUser;
        List<User> users = [Select Id, Name from User 
                            where UserRoleId <> null and isActive=true limit 1];
        if(users.size()>0) testUser = users.get(0);
        
        Account testaccount = UnitTestHelper.generateTestAccount();
        testaccount.OwnerId = testUser.Id;
        insert testaccount;
        
        Contact testcontact = UnitTestHelper.generateTestContact();
        testcontact.AccountId = testaccount.Id;       
        insert testcontact;
        
        Public_Product__c testproduct = UnitTestHelper.generatePublicProduct();
        insert testproduct;     
        
        User testportaluser = UnitTestHelper.getCustomerPortalUser('abc', testcontact.Id);
        insert testportaluser;
        
        Asset_Custom__c ast = new Asset_Custom__c(Gamer__c = testcontact.Id, My_Product__c = testproduct.Id);
        
        Test.startTest();
            System.runAs(testportaluser) {
                Atvi_warranty_returns_extension obj = new Atvi_warranty_returns_extension();
                obj.getTopSixGames();
                //obj.selectGameController();
                obj.rendermodelcheck = true;
                obj.modelno = '12345689';
                obj.getrenderModelCheck();
                obj.getrenderProdError();
                obj.getrenderProdError2();
                obj.getRenderHome();
                obj.getRenderProdInfo();
                obj.getRenderConInfo();
                obj.getRenderReview();
                obj.getRenderConfirmation();
                obj.ProdId = testproduct.Id;  
                obj.selectProd();              
                obj.getSelPubProd();                
                obj.submitProdInfo();
                obj.submitConInfo();
                obj.submitReview();
                obj.getRemGames();
                obj.getIsViewAll();
                obj.getUserLang();
                PageReference pg2 = obj.prevProdInfo();
                PageReference pg3 = obj.prevConInfo();
                obj.getisAustralia();
                obj.goToShowAll();
                obj.getRenderMultiProd();
                obj.gamename = testproduct.name;
                obj.gameplatform = testproduct.platform__c;
                obj.selectGameController();
                obj.ProdInfoContinue();
                obj.assignParam1();
                //obj.refreshPage();
                obj.doQuery();
                obj.param3 = testproduct.Id;
                obj.selectMulti();
                obj.modelNo = testproduct.Model_Number__c;
                obj.submitprodinfo();
            }        
        Test.stopTest();
    }
}