@isTest 
private class Test_CRR_CodeDistributionRequest_Helper {

    static testMethod void myUnitTest() {
        User u = UnitTestHelper.generateTestUser();
        u.ProfileId = [Select Id From Profile Where Name = 'System Administrator'].Id;
        insert u;
        
        CRR_Code_Distribution_Recipient__c cdr = new CRR_Code_Distribution_Recipient__c(Recipient__c = u.Id);
        insert cdr;
        
        Date d = Date.today();
        
        CRR_Code_Franchise__c franchise = new CRR_Code_Franchise__c(Name = 'Call of Duty', Franchise__c = 'Call of Duty', Product_Type__c = 'Game');
        insert franchise;
        
        CRR_Code_Product__c codeproduct = new CRR_Code_Product__c(Name = 'Call of Duty:Ghosts',Clarity_Project_name_SKU__c = '12345',Code_Franchise__c = franchise.Id);
        insert codeproduct;
        
        CRR_Code_Type__c promocode = new CRR_Code_Type__c (Name = 'Pre-Order',Filename__c = 'COD Test 1',Content_Type__c = 'Test 1',Content_Name__c ='Test 1',Promotion_Name__c = 'COD Test 1',
        Description__c = 'Test',First_Party_Picklist__c = 'Microsoft',Platform__c = 'XBOX 360',Activation_Date__c = d,Expiration_Date__c = d.addDays(365),
        Number_of_Codes__c = 3, Number_of_codes_distributed__c = 0,Code_Product__c = codeproduct.Id,Loading_complete__c = True);
        insert promocode;

        ContentWorkspace library = [Select Id From ContentWorkspace Where Name='Code Repository File Upload'];
        Id recordId = [Select Id From RecordType Where Name = 'Code repository codes' Limit 1].Id;
        system.debug(library.id);
        system.debug(recordId);

        ContentVersion cv = new ContentVersion(Title = 'Test', ContentURL = 'http://support.activision.com', Content_Name__c = 'Test 1', Content_Type__c = 'Full Game', Game_Title__c = 'Call of Duty:Ghosts', First_Party__c = 'Microsoft NA', 
                                               Code_Type__c = 'Pre-Order', Platform__c = 'Xbox 360', Activation_Year__c = '2014', Activation_Day__c = '1',
                                               Activation_Month__c = 'July', Expiration_Year__c = '2015', Expiration_Month__c = 'July', Expiration_Day__c = '1',
                                               Language_Choices__c = 'EN (US)', Promotion_Code__c = promocode.Id, FirstPublishLocationId = library.Id, RecordTypeId = recordId);

        insert cv;
        system.debug('Promodode ID is '+promocode.Id);
        system.debug('Promocode ID in ContentVersion is '+cv.Promotion_Code__c);
        CRR_Code_Type__c ctype = [Select Id, Name, (Select Game_Title__c From Content__r) From CRR_Code_Type__c];
        system.debug('Content_r for promocode is'+ctype.Content__r);

        
        
        
        //All Selections Made
        	
        	PageReference currentpage = Page.CRR_CodeDistributionRequest_Edit;
        	Test.setCurrentPage(currentpage);
        	ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(new CRR_Code_Distribution_Request__c());
        	CRR_CodeDistributionRequest_Helper cdrh = new CRR_CodeDistributionRequest_Helper(sc);
        	List<SelectOption> games = cdrh.getGames();
        	system.assertEquals(2,games.size());
        	if(games.size()== 2){
        		system.assertEquals(new SelectOption('None','None',false),games[0]);
        		system.assertEquals(new SelectOption('Call of Duty:Ghosts','Call of Duty:Ghosts',false),games[1]);
        	}
        	cdrh.gameselection = 'Call of Duty:Ghosts';
        	List<SelectOption> platforms = cdrh.getPlatforms();
        	system.assertEquals(2,platforms.size());
        	if(platforms.size()==2){
        		system.assertEquals(new SelectOption('None','None',false),platforms[0]);
        		system.assertEquals(new SelectOption('Xbox 360','Xbox 360',false),platforms[1]);
        	}
            
        	cdrh.platformselection = 'Xbox 360';
        	List<SelectOption> codetypes = cdrh.getCodeType();
        	system.assertEquals(2,codetypes.size());
        	if(codetypes.size()==2){
        		system.assertEquals(new SelectOption('None','None',false),codetypes[0]);
        		system.assertEquals(new SelectOption('Pre-Order','Pre-Order',false),codetypes[1]);
        	}
            
        	cdrh.codetypeselection = 'Pre-Order';
        	List<SelectOption> contenttypes = cdrh.getContentType();
        	system.assertEquals(2,contenttypes.size());
        	if(contenttypes.size()==2){
        		system.assertEquals(new SelectOption('None','None',false),contenttypes[0]);
        		system.assertEquals(new SelectOption('Full Game','Full Game',false),contenttypes[1]);
        	}
            
        	cdrh.contenttypeselection = 'Full Game';
        	List<SelectOption> contentnames = cdrh.getContentName();
        	system.assertEquals(2,contentnames.size());
        	if(contentnames.size()==2){
        		system.assertEquals(new SelectOption('None','None',false),contentnames[0]);
        		system.assertEquals(new SelectOption('Test 1','Test 1',false),contentnames[1]);
        	}
            
        	cdrh.contentnameselection = 'Test 1';
        	List<SelectOption> codelist = cdrh.getPromoCodes();
        	system.assertEquals(2,codelist.size());
        	if(codelist.size()==2){
        		system.assertEquals(new SelectOption('None','None',false),codelist[0]);
        		system.assertEquals(new SelectOption(promocode.Id,'Test Microsoft NA',false),codelist[1]);
        	}
            
        	cdrh.promocodeselection = promocode.Id;
        	PageReference p = cdrh.doNothing();
        	cdrh.codedistrequest.Code_Distribution_Recipient__c=cdr.Id;
        	cdrh.codedistrequest.Number_of_codes_to_deliver__c=1;
        	PageReference p2 = cdrh.Save();


        	
        	
        
    }

    static testMethod void myUnitTest2() {
        User u = UnitTestHelper.generateTestUser();
        u.ProfileId = [Select Id From Profile Where Name = 'System Administrator'].Id;
        insert u;
        
        CRR_Code_Distribution_Recipient__c cdr = new CRR_Code_Distribution_Recipient__c(Recipient__c = u.Id);
        insert cdr;
        
        Date d = Date.today();
        
        CRR_Code_Franchise__c franchise = new CRR_Code_Franchise__c(Name = 'Call of Duty', Franchise__c = 'Call of Duty', Product_Type__c = 'Game');
        insert franchise;
        
        CRR_Code_Product__c codeproduct = new CRR_Code_Product__c(Name = 'Call of Duty:Ghosts',Clarity_Project_name_SKU__c = '12345',Code_Franchise__c = franchise.Id);
        insert codeproduct;
        
        CRR_Code_Type__c promocode = new CRR_Code_Type__c (Name = 'Pre-Order',Filename__c = 'COD Test 1',Content_Type__c = 'Test 1',Content_Name__c ='Test 1',
        Description__c = 'Test',First_Party_Picklist__c = 'Microsoft',Platform__c = 'XBOX 360',Activation_Date__c = d,Expiration_Date__c = d.addDays(365),
        Number_of_Codes__c = 3, Number_of_codes_distributed__c = 0,Code_Product__c = codeproduct.Id,Loading_complete__c = True);
        insert promocode;

        ContentWorkspace library = [Select Id From ContentWorkspace Where Name='Code Repository File Upload'];
        Id recordId = [Select Id From RecordType Where Name = 'Code repository codes' Limit 1].Id;
        system.debug(library.id);
        system.debug(recordId);

        ContentVersion cv = new ContentVersion(Title = 'Test', ContentURL = 'http://support.activision.com', Content_Name__c = 'Test 1', Content_Type__c = 'Full Game', Game_Title__c = 'Call of Duty:Ghosts', First_Party__c = 'Microsoft NA', 
                                               Code_Type__c = 'Pre-Order', Platform__c = 'Xbox 360', Activation_Year__c = '2014', Activation_Day__c = '1',
                                               Activation_Month__c = 'July', Expiration_Year__c = '2015', Expiration_Month__c = 'July', Expiration_Day__c = '1',
                                               Language_Choices__c = 'EN (US)', Promotion_Code__c = promocode.Id, FirstPublishLocationId = library.Id, RecordTypeId = recordId);

        insert cv;
        system.debug('Promodode ID is '+promocode.Id);
        system.debug('Promocode ID in ContentVersion is '+cv.Promotion_Code__c);
        CRR_Code_Type__c ctype = [Select Id, Name, (Select Game_Title__c From Content__r) From CRR_Code_Type__c];
        system.debug('Content_r for promocode is'+ctype.Content__r);

        
        
        
        //No Selections Made
            
            PageReference currentpage = Page.CRR_CodeDistributionRequest_Edit;
            Test.setCurrentPage(currentpage);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(new CRR_Code_Distribution_Request__c());
            CRR_CodeDistributionRequest_Helper cdrh = new CRR_CodeDistributionRequest_Helper(sc);
            List<SelectOption> games = cdrh.getGames();
            system.assertEquals(2,games.size());
            if(games.size()== 2){
                system.assertEquals(new SelectOption('None','None',false),games[0]);
                system.assertEquals(new SelectOption('Call of Duty:Ghosts','Call of Duty:Ghosts',false),games[1]);
            }
            
            cdrh.gameselection = 'None';
            List<SelectOption> platforms2 = cdrh.getPlatforms();
            system.assertEquals(1,platforms2.size());
            if(platforms2.size()==1){
                system.assertEquals(new SelectOption('None','None',false),platforms2[0]);
            }
            
            cdrh.platformselection = 'None';
            List<SelectOption> codetypes2 = cdrh.getCodeType();
            system.assertEquals(1,codetypes2.size());
            if(codetypes2.size()==1){
                system.assertEquals(new SelectOption('None','None',false),codetypes2[0]);
            }
            
            cdrh.codetypeselection = 'None';
            List<SelectOption> contenttypes2 = cdrh.getContentType();
            system.assertEquals(1,contenttypes2.size());
            if(contenttypes2.size()==1){
                system.assertEquals(new SelectOption('None','None',false),contenttypes2[0]);
            }
            
            cdrh.contenttypeselection = 'None';
            List<SelectOption> contentnames2 = cdrh.getContentName();
            system.assertEquals(1,contentnames2.size());
            if(contentnames2.size()==1){
                system.assertEquals(new SelectOption('None','None',false),contentnames2[0]);
            }
            
            cdrh.contentnameselection = 'None';
            List<SelectOption> codelist2 = cdrh.getPromoCodes();
            system.assertEquals(1,codelist2.size());
            if(codelist2.size()==1){
                system.assertEquals(new SelectOption('None','None',false),codelist2[0]);
            }
            cdrh.promocodeselection = promocode.Id;
            Decimal codes = cdrh.getAvailableCodes();
            system.assertEquals(codes,3);
            PageReference p = cdrh.doNothing();
            cdrh.codedistrequest.Code_Distribution_Recipient__c=cdr.Id;
            cdrh.codedistrequest.Number_of_codes_to_deliver__c=1;
            PageReference p2 = cdrh.Save();


            
            
        
    }

    static testMethod void myUnitTest3() {
        User u = UnitTestHelper.generateTestUser();
        u.ProfileId = [Select Id From Profile Where Name = 'System Administrator'].Id;
        insert u;
        
        CRR_Code_Distribution_Recipient__c cdr = new CRR_Code_Distribution_Recipient__c(Recipient__c = u.Id);
        insert cdr;
        
        Date d = Date.today();
        
        CRR_Code_Franchise__c franchise = new CRR_Code_Franchise__c(Name = 'Call of Duty', Franchise__c = 'Call of Duty', Product_Type__c = 'Game');
        insert franchise;
        
        CRR_Code_Product__c codeproduct = new CRR_Code_Product__c(Name = 'Call of Duty:Ghosts',Clarity_Project_name_SKU__c = '12345',Code_Franchise__c = franchise.Id);
        insert codeproduct;
        
        CRR_Code_Type__c promocode = new CRR_Code_Type__c (Name = 'Pre-Order',Filename__c = 'COD Test 1',Content_Type__c = 'Test 1',Content_Name__c ='Test 1',
        Description__c = 'Test',First_Party_Picklist__c = 'Microsoft',Platform__c = 'XBOX 360',Activation_Date__c = d,Expiration_Date__c = d.addDays(365),
        Number_of_Codes__c = 3, Number_of_codes_distributed__c = 0,Code_Product__c = codeproduct.Id,Loading_complete__c = True);
        insert promocode;

        ContentWorkspace library = [Select Id From ContentWorkspace Where Name='Code Repository File Upload'];
        Id recordId = [Select Id From RecordType Where Name = 'Code repository codes' Limit 1].Id;
        system.debug(library.id);
        system.debug(recordId);

        ContentVersion cv = new ContentVersion(Title = 'Test', ContentURL = 'http://support.activision.com', Content_Name__c = 'Test 1', Content_Type__c = 'Full Game', Game_Title__c = 'Call of Duty:Ghosts', First_Party__c = 'Microsoft NA', 
                                               Code_Type__c = 'Pre-Order', Platform__c = 'Xbox 360', Activation_Year__c = '2014', Activation_Day__c = '1',
                                               Activation_Month__c = 'July', Expiration_Year__c = '2015', Expiration_Month__c = 'July', Expiration_Day__c = '1',
                                               Language_Choices__c = 'EN (US)', Promotion_Code__c = promocode.Id, FirstPublishLocationId = library.Id, RecordTypeId = recordId);

        insert cv;
        system.debug('Promodode ID is '+promocode.Id);
        system.debug('Promocode ID in ContentVersion is '+cv.Promotion_Code__c);
        CRR_Code_Type__c ctype = [Select Id, Name, (Select Game_Title__c From Content__r) From CRR_Code_Type__c];
        system.debug('Content_r for promocode is'+ctype.Content__r);

        
        
        
        //All Selections Made but PromoCode name is null
            
            PageReference currentpage = Page.CRR_CodeDistributionRequest_Edit;
            Test.setCurrentPage(currentpage);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(new CRR_Code_Distribution_Request__c());
            CRR_CodeDistributionRequest_Helper cdrh = new CRR_CodeDistributionRequest_Helper(sc);
            List<SelectOption> games = cdrh.getGames();
            system.assertEquals(2,games.size());
            if(games.size()== 2){
                system.assertEquals(new SelectOption('None','None',false),games[0]);
                system.assertEquals(new SelectOption('Call of Duty:Ghosts','Call of Duty:Ghosts',false),games[1]);
            }
            cdrh.gameselection = 'Call of Duty:Ghosts';
            List<SelectOption> platforms = cdrh.getPlatforms();
            system.assertEquals(2,platforms.size());
            if(platforms.size()==2){
                system.assertEquals(new SelectOption('None','None',false),platforms[0]);
                system.assertEquals(new SelectOption('Xbox 360','Xbox 360',false),platforms[1]);
            }
            
            cdrh.platformselection = 'Xbox 360';
            List<SelectOption> codetypes = cdrh.getCodeType();
            system.assertEquals(2,codetypes.size());
            if(codetypes.size()==2){
                system.assertEquals(new SelectOption('None','None',false),codetypes[0]);
                system.assertEquals(new SelectOption('Pre-Order','Pre-Order',false),codetypes[1]);
            }
            
            cdrh.codetypeselection = 'Pre-Order';
            List<SelectOption> contenttypes = cdrh.getContentType();
            system.assertEquals(2,contenttypes.size());
            if(contenttypes.size()==2){
                system.assertEquals(new SelectOption('None','None',false),contenttypes[0]);
                system.assertEquals(new SelectOption('Full Game','Full Game',false),contenttypes[1]);
            }
            
            cdrh.contenttypeselection = 'Full Game';
            List<SelectOption> contentnames = cdrh.getContentName();
            system.assertEquals(2,contentnames.size());
            if(contentnames.size()==2){
                system.assertEquals(new SelectOption('None','None',false),contentnames[0]);
                system.assertEquals(new SelectOption('Test 1','Test 1',false),contentnames[1]);
            }
            
            cdrh.contentnameselection = 'Test 1';
            List<SelectOption> codelist = cdrh.getPromoCodes();
            system.assertEquals(2,codelist.size());
            if(codelist.size()==2){
                system.assertEquals(new SelectOption('None','None',false),codelist[0]);
                system.assertEquals(new SelectOption(promocode.Id,'Test Microsoft NA',false),codelist[1]);
            }
            
            cdrh.promocodeselection = promocode.Id;
            Decimal codes = cdrh.getAvailableCodes();
            system.assertEquals(codes,3);
            PageReference p = cdrh.doNothing();
            cdrh.codedistrequest.Code_Distribution_Recipient__c=cdr.Id;
            cdrh.codedistrequest.Number_of_codes_to_deliver__c=5;
            PageReference p2 = cdrh.Save();
            

            
            
        
    }

    static testMethod void myUnitTest4() {
        User u = UnitTestHelper.generateTestUser();
        u.ProfileId = [Select Id From Profile Where Name = 'System Administrator'].Id;
        insert u;
        
        CRR_Code_Distribution_Recipient__c cdr = new CRR_Code_Distribution_Recipient__c(Recipient__c = u.Id);
        insert cdr;
        
        Date d = Date.today();
        
        CRR_Code_Franchise__c franchise = new CRR_Code_Franchise__c(Name = 'Call of Duty', Franchise__c = 'Call of Duty', Product_Type__c = 'Game');
        insert franchise;
        
        CRR_Code_Product__c codeproduct = new CRR_Code_Product__c(Name = 'Call of Duty:Ghosts',Clarity_Project_name_SKU__c = '12345',Code_Franchise__c = franchise.Id);
        insert codeproduct;
        
        CRR_Code_Type__c promocode = new CRR_Code_Type__c (Name = 'Pre-Order',Filename__c = 'COD Test 1',Content_Type__c = 'Test 1',Content_Name__c ='Test 1',
        Description__c = 'Test',First_Party_Picklist__c = 'Microsoft',Platform__c = 'XBOX 360',Activation_Date__c = d,Expiration_Date__c = d.addDays(365),
        Number_of_Codes__c = 3, Number_of_codes_distributed__c = 0,Code_Product__c = codeproduct.Id,Loading_complete__c = True);
        insert promocode;

        ContentWorkspace library = [Select Id From ContentWorkspace Where Name='Code Repository File Upload'];
        Id recordId = [Select Id From RecordType Where Name = 'Code repository codes' Limit 1].Id;
        system.debug(library.id);
        system.debug(recordId);

        ContentVersion cv = new ContentVersion(Title = 'Test', ContentURL = 'http://support.activision.com', Content_Name__c = 'Test 1', Content_Type__c = 'Full Game', Game_Title__c = 'Call of Duty:Ghosts', First_Party__c = 'Microsoft NA', 
                                               Code_Type__c = 'Pre-Order', Platform__c = 'Xbox 360', Activation_Year__c = '2014', Activation_Day__c = '1',
                                               Activation_Month__c = 'July', Expiration_Year__c = '2015', Expiration_Month__c = 'July', Expiration_Day__c = '1',
                                               Language_Choices__c = 'EN (US)', Promotion_Code__c = promocode.Id, FirstPublishLocationId = library.Id, RecordTypeId = recordId);

        insert cv;
        system.debug('Promodode ID is '+promocode.Id);
        system.debug('Promocode ID in ContentVersion is '+cv.Promotion_Code__c);
        CRR_Code_Type__c ctype = [Select Id, Name, (Select Game_Title__c From Content__r) From CRR_Code_Type__c];
        system.debug('Content_r for promocode is'+ctype.Content__r);

        
        
        
        //All Selections Made but PromoCode name is null
            
            PageReference currentpage = Page.CRR_CodeDistributionRequest_Edit;
            Test.setCurrentPage(currentpage);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(new CRR_Code_Distribution_Request__c());
            CRR_CodeDistributionRequest_Helper cdrh = new CRR_CodeDistributionRequest_Helper(sc);
            List<SelectOption> games = cdrh.getGames();
            system.assertEquals(2,games.size());
            if(games.size()== 2){
                system.assertEquals(new SelectOption('None','None',false),games[0]);
                system.assertEquals(new SelectOption('Call of Duty:Ghosts','Call of Duty:Ghosts',false),games[1]);
            }
            cdrh.gameselection = 'Call of Duty:Ghosts';
            List<SelectOption> platforms = cdrh.getPlatforms();
            system.assertEquals(2,platforms.size());
            if(platforms.size()==2){
                system.assertEquals(new SelectOption('None','None',false),platforms[0]);
                system.assertEquals(new SelectOption('Xbox 360','Xbox 360',false),platforms[1]);
            }
            
            cdrh.platformselection = 'Xbox 360';
            List<SelectOption> codetypes = cdrh.getCodeType();
            system.assertEquals(2,codetypes.size());
            if(codetypes.size()==2){
                system.assertEquals(new SelectOption('None','None',false),codetypes[0]);
                system.assertEquals(new SelectOption('Pre-Order','Pre-Order',false),codetypes[1]);
            }
            
            cdrh.codetypeselection = 'Pre-Order';
            List<SelectOption> contenttypes = cdrh.getContentType();
            system.assertEquals(2,contenttypes.size());
            if(contenttypes.size()==2){
                system.assertEquals(new SelectOption('None','None',false),contenttypes[0]);
                system.assertEquals(new SelectOption('Full Game','Full Game',false),contenttypes[1]);
            }
            
            cdrh.contenttypeselection = 'Full Game';
            List<SelectOption> contentnames = cdrh.getContentName();
            system.assertEquals(2,contentnames.size());
            if(contentnames.size()==2){
                system.assertEquals(new SelectOption('None','None',false),contentnames[0]);
                system.assertEquals(new SelectOption('Test 1','Test 1',false),contentnames[1]);
            }
            
            cdrh.contentnameselection = 'Test 1';
            List<SelectOption> codelist = cdrh.getPromoCodes();
            system.assertEquals(2,codelist.size());
            if(codelist.size()==2){
                system.assertEquals(new SelectOption('None','None',false),codelist[0]);
                system.assertEquals(new SelectOption(promocode.Id,'Test Microsoft NA',false),codelist[1]);
            }
            
            PageReference p = cdrh.doNothing();
            cdrh.codedistrequest.Code_Distribution_Recipient__c=cdr.Id;
            cdrh.codedistrequest.Number_of_codes_to_deliver__c=3;
            PageReference p2 = cdrh.Save();

            
        
    }
}