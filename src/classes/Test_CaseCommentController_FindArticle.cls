/**
**Test_Atvi_NewsflashController - test class for the class CaseCommentController only the find artile logic is covered
**Created 18.03.12 - 
**/
@isTest

public class Test_CaseCommentController_FindArticle{
 
 
 public static testMethod void test1(){
        Test.startTest();
        Case cc = new Case(Comment_not_Required__c = true);
        contact con = new contact(lastname = 'test');
        insert con;
        cc.contactid = con.id;
        cc.Type      = 'Registration';
        cc.Sub_Type__c = 'Wrong Email';
        cc.status = 'Open';
        cc.Origin = 'web';
        Public_Product__c pp = new  Public_Product__c(name = 'testgame');
        insert pp;
        cc.Product_Effected__c= pp.id;    
        cc.Priority = 'Medium';
        cc.subject = 'Test';
        cc.Description = 'Troubleshooting';
        insert cc;
        
        Case cc2 = new Case(Comment_not_Required__c = true);
       
        cc2.subject = 'Test2';
        cc2.Description = 'Troubleshooting';
        insert cc2;
        
        ApexPages.StandardController sc = new ApexPages.standardController(cc);
        ApexPages.StandardController sc2 = new ApexPages.standardController(cc2);

        ApexPages.currentPage().getParameters().put('Caseid',cc.id) ;
        ApexPages.currentPage().getParameters().put('caseId',cc2.id) ;
        CaseCommentController controller = new CaseCommentController(sc);
        CaseCommentController2 controller2 = new CaseCommentController2 (sc2);
        
        SObject kavObj = Schema.getGlobalDescribe().get('FAQ__kav').newSObject();
        kavObj.put('Title','Foo Foo Foo!!!');
        kavObj.put('UrlName', 'foo-foo-foo');
        kavObj.put('Summary', 'This is a summary!!! Foo. Foo. Foo.');
        kavObj.put('Language', 'en_US');
        insert kavObj;

        // requery the kavObj to get the KnowledgeArticleId on it that is created automatically
        String q = 'select id, KnowledgeArticleId from FAQ__kav where Id = \'' +kavObj.get('Id')+  '\' and PublishStatus = \'Draft\' and Language = \'en_US\'';
        List<SObject> kavs = Database.query(q);
       
        kavObj = kavs[0];
        KbManagement.PublishingService.publishArticle((ID)(kavObj.get('KnowledgeArticleId')),true);
            KnowledgeArticleVersion kav = [select id,KnowledgeArticleId from KnowledgeArticleVersion where PublishStatus = 'online' and Language ='en_US' limit 1];
            CaseArticle ca = new CaseArticle(CaseId=cc.id,knowledgeArticleId=(ID)(kavObj.get('KnowledgeArticleId')) );
            insert ca;
            CaseArticle ca2 = new CaseArticle(CaseId=cc2.id,knowledgeArticleId=(ID)(kavObj.get('KnowledgeArticleId')) );
            insert ca2;
            
            CaseCommentController.ArticleClass innerCls = new CaseCommentController.ArticleClass(
                kav , ca
            ); 
            controller.retrieveKnowledgeArticles();
            innerCls.Detach(); 
            
            CaseCommentController2.ArticleClass innerCls2 = new CaseCommentController2.ArticleClass(
                kav , ca2
            );      
            controller2.retrieveKnowledgeArticles();
            innerCls2.Detach();                
            Test.stopTest();
      
    }
    
 }