@isTest(seealldata=true)
public Class Test_CaseMassArticle{
    static testMethod void testSelf(){
        case[] testCases = new case[]{};
    
        Account a = new Account(
            name = 'test account',
            Type = 'fake'
        );
        insert a;
        Contact c = new Contact(
            AccountId = a.id,
            FirstName = 'test',
            LastName = 'contact'
        );
        
        Public_Product__c prod = new Public_Product__c(name = 'test product');
        insert new sObject[]{c, prod};
        case baseCase = new case(
            ContactId = c.Id,
            Subject = 'test case',
            Comments__c = 'test comment',
            Origin = 'fake',
            Product_Effected__c = prod.Id,
            Description = 'another required field',
            Type = 'prizing',
            Sub_Type__c = 'b',
            Status = 'Open'
        );
        testcases.add(baseCase);
        insert testcases;
        user u = [SELECT Id FROM User WHERE IsActive = true AND ContactId = null AND Profile.Name LIKE '%agent%' LIMIT 1];
        //insert new ApexCodeSetting__c(Name = 'CaseMassCommentLimit', Number__c = 500, Description__c = 'max # of comments per iteration');
        
        
        system.runAs(u){
            ApexPages.standardSetController tstSetCont = new ApexPages.standardSetController(testCases); 
            CaseMassArticle tstCls = new CaseMassArticle(tstSetCont);
            tstSetCont.setSelected(testCases);
            tstCls.goToMassArticlePage();
            test.setCurrentPage(Page.CaseMassAttach);
            ApexPages.currentPage().getParameters().put('selectCount','1');
            tstCls = new CaseMassArticle();
            KnowledgeArticle faq = [Select k.Id, k.ArticleNumber From KnowledgeArticle k limit 1];
            //system.debug('faq.id' + faq.id);
            tstCls.articleid = faq.id;
            //tstCls.articleid = 'kA0J0000000Cdzo';
            //tstCls.articleid = 'ka0J0000000CeUhIAK';
            tstCls.attachArticles();
            tstCls.undo();
            tstCls.cancel();
            tstCls.submitSearchText();
            tstCls.Debug();
            ApexPages.currentPage().getParameters().put('selectCount','0');
            tstCls = new CaseMassArticle();
        }
        //case_mass_article__c ma = new case_mass_article__c(case_ids__c=basecase.id);
        //insert ma;
        
        //knowledgearticle ka = new knowledgearticle();
        
        
        //ApexPages.currentPage().getParameters().put('selectCount', '1');
        
        //caseMassArticle testcls = new caseMassArticle();
        //testcls.articleid = ka.id;
        //testcls.goToMassArticlePage();
        //testcls.getCaseAttachLimit();
        //testcls.submitSearchText();
        //testcls.debug();
        //testcls.attacharticles();
        //testcls.undo();
        
        
    }
}