@isTest 
public class Test_CaseMassComment{
/*
 * Test methods
 * //comment added to test whether sf4twitter tests are run during an eclipse deployment
*/
    static testMethod void testSelf(){
        //create data
        case[] testCases = new case[]{};
        Account a = new Account(
            name = 'test account',
            Type = 'fake'
        );
        insert a;
        Contact c = new Contact(
            AccountId = a.id,
            FirstName = 'test',
            LastName = 'contact'
        );
        Public_Product__c prod = new Public_Product__c(name = 'test product');
        insert new sObject[]{c, prod};
        case baseCase = new case(
            ContactId = c.Id,
            Subject = 'test case',
            Comments__c = 'test comment',
            Origin = 'fake',
            Product_Effected__c = prod.Id,
            Description = 'another required field',
            Type = 'prizing',
            Sub_Type__c = 'b',
            Status = 'Open'
        );
        do{
            testCases.add(baseCase.clone());
        }while(testCases.size() < 600);
        insert testCases;
        user u = [SELECT Id FROM User WHERE IsActive = true AND ContactId = null AND Profile.Name LIKE '%agent%' LIMIT 1];
        insert new ApexCodeSetting__c(Name = 'CaseMassCommentLimit', Number__c = 500, Description__c = 'max # of comments per iteration');
        
        //begin testing
        system.runAs(u){
        if(utility.CreatedFromCust == false){
            //test, ensure that the limitation is enforced, limit at this point is is 500 cases
            //simulate the user selecting more cases than allowed
            ApexPages.standardSetController tstSetCont = new ApexPages.standardSetController(testCases); //load 600 cases
            CaseMassComment tstCls = new CaseMassComment(tstSetCont); //init from list view
            tstSetCont.setSelected(testCases); //select all 600 cases
            tstCls.goToMassCommentPage();
            test.setCurrentPage(Page.CaseMassComment);
            tstCls = new CaseMassComment(); //init from VF page
            system.assertEquals(tstCls.cases.size(), tstCls.getCaseCommentLimit()); //assert that only 500 cases were loaded
            
            //test, click addMassComment without typing a comment
            tstCls.addMassComment();
            tstCls.baseComment.CommentBody = 'test class comment';
            
            //test, try/catch
            tstCls.addMassComment();
            tstCls.changeStatus = false;
            
            //test, all comments added
            tstCls.addMassComment();
            Integer addedComments = [SELECT count() FROM CaseComment WHERE CreatedById = :UserInfo.getUserId()];
            system.assertEquals(addedComments, tstCls.getCaseCommentLimit());
            
            //test, change status
            tstCls.changeStatus = true;
            tstCls.addMassComment(); //page error added stating that a new status has to be chosen
            
            //test, change status success
            tstCls.ChosenStatus = 'Closed';
            tstCls.addMassComment();
            
            //test, helper methods
            tstCls.doNothing();
            tstCls.returnToTab();
            tstCls.getAllPosslStatusOptions();
        }
        }
    }
 
}