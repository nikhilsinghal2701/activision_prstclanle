@isTest
Private Class Test_CaseTriggerHandler{
    static Account acc = new Account();    
    static Contact con = new Contact();    
    static Case cases = new Case(); 
    static Case newCase = new Case();
    static Public_Product__c PublicProduct = new Public_Product__c();
    static CaseComment casecomments = new CaseComment();
    static CaseComment newCaseComment = new CaseComment();
    static User TestUserNew12 = new User();
    static RecordType[] RT = [select id from RecordType where Name =: 'General Support' and sObjectType =: 'Case']; 
    static RecordType[] RcdTypeRMA = [select id from RecordType where Name =: 'RMA' and sObjectType =: 'Case'];  
    static Profile[] profiles = [Select name, id From Profile where Name =: 'CS - Agent']; 
    
    static testmethod void TestOne(){
        
        CheckAddError.AddError = false;
       
        PublicProduct.Name = 'Call of Duty';
        Insert PublicProduct;
  
        cases.RecordTypeId = RT[0].id;
        cases.Type = 'Hardware';
        cases.Sub_Type__c = 'Instruments';
        cases.Status = 'New';
        cases.origin = 'Phone Call';
        cases.Product_Effected__c = PublicProduct.id;
        try{
            insert cases;
        }
        catch(dmlException e){
        }        
        CheckAddError.AddError = true;
                   
        casecomments.commentBody = 'test';
        casecomments.parentId = cases.id;
        casecomments.isPublished = true;
        insert casecomments;
        
        newCaseComment.commentBody = 'test comment';
        newCaseComment.parentId = cases.id;
        newCaseComment.isPublished = true;
        insert newCaseComment;
        
        cases.Status = 'Open';
        try{
            update cases;
        }
        catch (dmlexception e) {
        }          
    }
    static testmethod void TestTwo(){
        
        TestUserNew12.Username ='testUser12@activision.com';        
        TestUserNew12.FirstName = 'Test';        
        TestUserNew12.LastName = 'User12';        
        TestUserNew12.Email = 'Testmail12@activision.com';        
        TestUserNew12.alias = 'testAl12';        
        TestUserNew12.TimeZoneSidKey = 'America/New_York';        
        TestUserNew12.LocaleSidKey = 'en_US';        
        TestUserNew12.EmailEncodingKey = 'ISO-8859-1';       
        TestUserNew12.ProfileId = profiles[0].id;                 
        TestUserNew12.LanguageLocaleKey = 'en_US';        
        TestUserNew12.IsActive = true;                
        insert TestUserNew12;
        
        System.runAs(TestUserNew12) {

            newCase.RecordTypeId = RT[0].id;
            newCase.Type = 'Hardware';
            newCase.Sub_Type__c = 'Instruments';
            newCase.Status = 'New';
            newCase.origin = 'Phone Call';
            newCase.Product_Effected__c = PublicProduct.id;
            try{
                insert newCase;
            }
            catch(dmlException e){
            }
        }          
    }
    static TestMethod void TestThree(){
        PublicProduct.Name = 'Call of Duty';
        PublicProduct.RMA_Available__c = true;
        Insert PublicProduct;
  
        cases.RecordTypeId = RT[0].id;
        cases.Type = 'Hardware';
        cases.Sub_Type__c = 'Instruments';
        cases.Status = 'New';
        cases.origin = 'Phone Call';
        cases.Product_Effected__c = PublicProduct.id;
        cases.Comments__c = 'New Comment';
        try{
            insert cases;
        }
        catch(dmlException e){
        }
        
        CaseTriggerHandler.updatedCase = false;
        newCase.RecordTypeId = RcdTypeRMA[0].id;
        newCase.Associated_General_Support_Case__c = cases.id;
        newCase.Status = 'Open';
        newCase.origin = 'Phone Call';
        newCase.Product_Effected__c = PublicProduct.id;
        newCase.Return_Code__c = 'NEW ORDER';
        //try{
        insert newCase;

        cases.status = 'Resolved';
        try{
            update cases;
        }
        catch (dmlException e){
        }
        
        newCase.Return_Code__c = 'REPAIRED UNIT DELIVERED';
        update newCase;      
    }
}