@isTest
public class Test_ConsoleTabLeftController{
    static testmethod void Test_ConsoleTabLeftController(){
    	public_product__c product = new public_product__c();
        product.name = 'test';
        insert product;
        
        case theCase = new case();
        theCase.type = 'Registration';
        theCase.Status = 'New';
        theCase.origin = 'Phone Call';
        theCase.Product_Effected__c = product.id;
        theCase.subject = 'test';
        theCase.Description = 'test'; 
        theCase.comments__c = 'test';
        insert theCase;
        
        test.startTest();
        ApexPages.currentPage().getParameters().put('id', theCase.id);
        ConsoleTabLeftController it = new ConsoleTabLeftController();
        test.stopTest();
    }
}