@isTest 
private class Test_CustomCaseArticleReportController {       

    static Case cases = new Case(); 
    static Case newCase = new Case();
    static Public_Product__c PublicProduct = new Public_Product__c();
    static CaseComment casecomments = new CaseComment();
    static CaseComment newCaseComment = new CaseComment();
    static User TestUserNew12 = new User();
    static RecordType[] RT = [select id from RecordType where Name =: 'General Support' and sObjectType =: 'Case'];    
        
    static testMethod void testmethod_GeneralSupportCasesCustomReport() {                 
         
        Profile p = [SELECT Id FROM Profile WHERE Name='CS - Agent']; 
        User u = new User(Alias = 'tagent', Email='testagentuser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='TestAgent', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='testagentuser@testorg.com',companyname='Sutherland');
       
        System.runAs(u) {
        CheckAddError.AddError = false;
       
        PublicProduct.Name = 'Call of Duty';
        PublicProduct.SubType__c = 'Main';
        Insert PublicProduct;
  
        cases.RecordTypeId = RT[0].id;
        cases.Type = 'Hardware';
        cases.Sub_Type__c = 'Instruments';
        cases.Status = 'New';
        cases.origin = 'Phone Call';
        cases.Sub_Type__c = 'Wrong Email';
        cases.Product_Effected__c = PublicProduct.id;
        insert cases;
       
        CheckAddError.AddError = true;
                   
        casecomments.commentBody = 'test';
        casecomments.parentId = cases.id;
        casecomments.isPublished = true;
        insert casecomments;
        
        newCaseComment.commentBody = 'test comment';
        newCaseComment.parentId = cases.id;
        newCaseComment.isPublished = true;
        insert newCaseComment;
        
        cases.Status = 'Open';
        //update cases;
        
        test.starttest();
        CustomCaseArticleReportController controllervar = new CustomCaseArticleReportController ();  
        CustomCaseArticleReportController.CasesWraperClass wrapervar = new  CustomCaseArticleReportController.CasesWraperClass();        
        controllervar.getCasesToDisplay();               
        controllervar.GotoExportPage() ;        
        controllervar.GenerateReport() ;          
        controllervar.getcompanynamesset();             
        test.stoptest();
        }
    }

}