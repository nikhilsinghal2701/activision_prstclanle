@isTest
public Class Test_FeedBackController{
    static testmethod void mytest(){
        
        Account testaccount=new Account();
        testaccount.Name='Test account';
        testaccount.Type__c='Agency';
        insert testaccount;
        
        Contact testContact=new Contact();
        testContact.LastName='Test User';
        testContact.Languages__c='English';
        testContact.MailingCountry='US';
        testContact.AccountId=testaccount.id;
        insert testContact;
        
        User userObject = UnitTestHelper.getCustomerPortalUser('foo',testContact.id);
        insert userObject;
        System.runAs(userObject) {
        test.startTest();
            FeedBackController sub = new FeedBackController();
            sub.checkSubmitted();
            sub.game='mw3';
            sub.CreateFB();
            sub.CreateFBComment();
            //sub.createSubscription();
            
            sub.Recorded=false;
            
            //sub.checkSubmitted();
            sub.CreateFBComment();
            
            
        test.stopTest();
        }
    }
}