/*************************************************************************************************
* @Name             Test_GamerSearchController
* @author           Abhishek Pal
* @date             24-MAR-2012
* @description      Test Class to increase the code coverage of GamerSearchController Class*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
*  Manas Jain               26-Mar-12               Test Class to increase the code coverage of GamerSearchController Class*
*
*---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                    Review Comments
* --------                                          ------------                   ---------------* 
* A. Pal(Deloitte Senior Consultant)                19-APR-2012                    Final Inspection before go-live

*******************************************************************************************************/
 
@isTest
public with sharing class Test_GamerSearchController {
    
    public static testMethod void GamerSearchController () 
    
    {       
     CheckAddError.addError = false;
        
     PageReference pageRef = Page.GamerSearchPage;

     Test.setCurrentPage(pageRef);
     
     Account testaccount=new Account();
     testaccount.Name='testname';
     insert testaccount;
     
     Contact testcontact=new Contact();
     testcontact.LastName='LName1';
     testcontact.Email='test@atvi.com';
     testcontact.AccountId=testaccount.Id;
     testcontact.Phone='122333';
     insert testcontact;
     
     case testcase=new case();
     testcase.ContactId=testcontact.Id;
     insert testcase;
     
     test.startTest();
     GamerSearchController.testMode=true;
 
     GamerSearchController searchgame=new GamerSearchController ();
     searchgame.caseNum=testcase.CaseNumber;
     searchgame.email='test@atvi.com';
     searchgame.phoneNum='122333';
     searchgame.getCountry_list();
     searchgame.ValidateEmailAddress('test@atvi.com');
     searchgame.CheckValid();
     searchgame.searchClick();
     searchgame.getContacts();
     searchgame.first();
     searchgame.last();
     searchgame.previous();
     searchgame.next();
     searchgame.cancel();
    
     test.stopTest();
        
    }
    
    public static testMethod void GamerSearchController_e () 
    
    {
     CheckAddError.addError = false;
    
     PageReference pageRefe = Page.GamerSearchPage;

     Test.setCurrentPage(pageRefe);
     
     Account testaccount_e=new Account();
     testaccount_e.Name='testnamee';
     insert testaccount_e;
     
     Contact testcontact=new Contact();
     testcontact.LastName='LName2';
     testcontact.Email='';
     testcontact.AccountId=testaccount_e.Id;
     testcontact.Phone='122333';
     insert testcontact;
     
     case testcase=new case();
     testcase.ContactId=testcontact.Id;
     insert testcase;
     
     test.startTest();
        GamerSearchController.testMode=true;
        GamerSearchController searchgame=new GamerSearchController ();
        searchgame.country='USA';
        searchgame.caseNum='3234';
        searchgame.email='@.com';
        searchgame.phoneNum='122333';
        searchgame.getCountry_list();
        searchgame.ValidateEmailAddress('@.com');
        searchgame.CheckValid();
        searchgame.searchClick();
        searchgame.getContacts();
        searchgame.first();
        searchgame.last();
        searchgame.previous();
        searchgame.next();
        searchgame.cancel();
     test.stopTest();
        
    }
    
     public static testMethod void GamerSearchController1 () 
    
    {
     CheckAddError.addError = false;
    
     PageReference pageRefe = Page.GamerSearchPage;

     Test.setCurrentPage(pageRefe);
     
     Account testaccount_e=new Account();
     testaccount_e.Name='testnamee';
     insert testaccount_e;
     
     Contact testcontact=new Contact();
     testcontact.LastName='LName3';     
     testcontact.AccountId=testaccount_e.Id;
     testcontact.Phone='122333';
     insert testcontact;
     
     case testcase=new case();
     testcase.ContactId=testcontact.Id;
     insert testcase;
     
   test.startTest();
     GamerSearchController.testMode=true;
     GamerSearchController searchgame=new GamerSearchController ();
     searchgame.country='USA';
     searchgame.caseNum='';
     searchgame.email='';
     searchgame.phoneNum='';
     searchgame.getCountry_list();
     searchgame.ValidateEmailAddress('');
     searchgame.CheckValid();
     searchgame.searchClick();
     searchgame.getContacts();
     searchgame.first();
     searchgame.last();
     searchgame.previous();
     searchgame.next();
     searchgame.cancel();
   test.stopTest();
        
    }
public static testMethod void GamerSearchController2 () 
    
    {
     CheckAddError.addError = false;
         
     PageReference pageRefe = Page.GamerSearchPage;

     Test.setCurrentPage(pageRefe);
     
     Account testaccount_e=new Account();
     testaccount_e.Name='testnamee';
     insert testaccount_e;
     
     Contact testcontact=new Contact();
     testcontact.LastName='LName4';     
     testcontact.AccountId=testaccount_e.Id;
     testcontact.Phone='122333';
     insert testcontact;
     
     case testcase=new case();
     testcase.ContactId=testcontact.Id;
     insert testcase;
     
  test.startTest();
     GamerSearchController.testMode=true;
     GamerSearchController searchgame=new GamerSearchController ();
     searchgame.country='USA';
     searchgame.caseNum='2431';
     searchgame.email='';
     searchgame.phoneNum='';
     searchgame.getCountry_list();
     searchgame.ValidateEmailAddress('');
     searchgame.CheckValid();
     searchgame.searchClick();
     searchgame.getContacts();
     searchgame.first();
     searchgame.last();
     searchgame.previous();
     searchgame.next();
     searchgame.cancel();
  test.stopTest();
        
    }

}