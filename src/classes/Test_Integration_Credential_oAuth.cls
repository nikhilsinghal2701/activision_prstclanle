@isTest
public class Test_Integration_Credential_oAuth{
    static testmethod void Test_Integration_Credential_oAuth(){
        integration_credential__c ic = new integration_credential__c();
        ic.Application_Name__c = 'Test';
        ic.access_token__c = '123';
        ic.refresh_token__c = '123';
        ic.api_key__c = '123';
        ic.api_secret__c = '123';
        ic.Twitter_Enabled__c = true;
        insert ic;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ic);
        
        test.startTest();
        integration_credential_oauth_v2 ico = new integration_credential_oauth_v2();
        ico.getoAuthToken();
        ico.startOauth();
        
        integration_credential_oauth_v2 ico2 = new integration_credential_oauth_v2(sc);
        test.stopTest();
    }

}