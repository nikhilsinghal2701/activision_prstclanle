@isTest
public class Test_MedalliaSettingController{
    public static testMethod void createData(){
        insert new Medallia_setting__c[]{
            new Medallia_Setting__c(
                name='Atvi_faqKav_override', delay__c=0, mobile_delay__c=1, skylanders_sampling_rate__c=2, sampling__c=3
            ),new Medallia_Setting__c(
                name='pkb_Home', delay__c=10, mobile_delay__c=11, skylanders_sampling_rate__c=12, sampling__c=13
            ),new Medallia_Setting__c(
                name='All', delay__c=20, mobile_delay__c=21, skylanders_sampling_rate__c=22, sampling__c=23
            )
        };
    }

    static testmethod void Test_MedalliaSettingController(){
        createData();
        PageReference pageRef = page.Atvi_faqKav_override;
        system.test.setCurrentPage(pageRef);
       
        MedalliaSettingController msc = new MedalliaSettingController();
        system.assert(! msc.getIsMobile());
        system.assertEquals(msc.getDelay(), 0);
        
        //switch to mobile
        ApexPages.currentPage().getHeaders().put('USER-AGENT','Another Mobile Device');
        system.assert(msc.getIsMobile());
        system.assertEquals(msc.getDelay(), 1);
        
        map<String, String> qs2name = new map<String, String>{
            'de'=>'German',
            'fr'=>'French',
            'it'=>'Italian',
            'es'=>'Spanish',
            'nl'=>'Dutch',
            'sv'=>'Swedish',
            'pt'=>'Portuguese (Brazilian)',
            'en'=>'English'
        };
        system.assertEquals(msc.getSurvLang(), 'English'); //default
        for(String locName : new string[]{'uil','lang','ulang'}){
            for(String langVal : new string[]{'de','fr','it','es','nl','sv','pt','en'}){
                //set querystring
                ApexPages.currentPage().getParameters().put(locName, langVal);
                system.assertEquals(qs2name.get(langVal), msc.getSurvLang());
                
                //clear settings
                ApexPages.currentPage().getParameters().remove(locName);
                
            }
        }
        for(String locName : new string[]{'ulang','lang','uil'}){
            for(String langVal : new string[]{'de','fr','it','es','nl','sv','pt','en'}){  
                //msc = new MedalliaSettingController();
                ApexPages.currentPage().setCookies(new Cookie[]{new Cookie(locName, langVal, null, -1, false)});
                system.assertEquals(qs2name.get(langVal),msc.getSurvLang());
            }
        }
    }
    
    
}