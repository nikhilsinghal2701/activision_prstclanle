/**
**Test_Atvi_NewsflashController - test class for the trigger Atvi_MultiplayerAccount_HistoryTracking
**Created 18.03.12 - 
**/
@isTest

public class Test_MultiplayerAccount_trigger{
     public static testMethod void testtrigger(){
        Test.startTest();
        contact c1 = new contact(lastname='Test');
        contact c2 = new contact(lastname='Test2');
        insert c1;
        insert c2;
        Multiplayer_Account__c mpa = new Multiplayer_Account__c(Contact__c=c1.id,Gamertag__c='xyz');
        insert mpa;
        mpa.contact__c =c2.id;
        mpa.Gamertag__c='abc';
        update mpa;
       
        Test.stopTest();
     }
}