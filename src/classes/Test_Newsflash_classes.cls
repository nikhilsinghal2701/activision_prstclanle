/**
**Test_Atvi_NewsflashController - test class for Atvi_NewsflashController
**Created 18.03.12 - 
**/
@isTest

public class Test_Newsflash_classes{
     public static List<Newsflash__c> Tl = new List<Newsflash__c> ();
     public static id nid;
     public static testMethod void testConstructors(){
        Test.startTest();
        Newsflash__c nfObj = new Newsflash__c();
        nfobj.recordtypeid=newsflash__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Product Newsflash').getRecordTypeId();
        insert nfObj;
        ApexPages.currentPage().getParameters().put('RecordType', newsflash__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Product Newsflash').getRecordTypeId());
        ApexPages.currentPage().getParameters().put('clone','1');
        ApexPages.StandardController controller =new ApexPages.StandardController(nfObj);
        CreateNewsFlashController nfEXT = new CreateNewsFlashController(controller);
        nfEXT.Save();
        nfObj.Product_L1__c='Test elite';
        nfObj.Product_L2__c='Test elite';
        nfObj.L3__c='Needs L3';
        controller =new ApexPages.StandardController(nfObj);
        nfEXT = new CreateNewsFlashController(controller);
        nfEXT.thecase.Product_L1__c='Test elite';
        nfEXT.Save();
        Atvi_NewsflashController  anfObj = new Atvi_NewsflashController();
        anfObj.reQuery();
        nfObj.Approval_Status__c='Approved';
        nfObj.Requested_Date_Time__c =system.now();
        update nfObj;
        Atvi_NewsflashController.SearchAlerts(Tl);
        Atvi_NewsflashController.NewsFlashDelivered(nid);
        ApexPages.currentPage().getParameters().put('id', nfObj.Id);
        Atvi_NewsflashViewed nfvEXT = new Atvi_NewsflashViewed(controller);
        nfvEXT.param='1';
        nfvEXT.CreateNewsflashViewedRec();
        nfvEXT.CreateNewsflashViewedRec();
        Test.stopTest();
     }
}