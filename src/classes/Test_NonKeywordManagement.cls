@isTest
Private Class Test_NonKeywordManagement{
    static Non_Keywords__c NonKeyWordObj = new Non_Keywords__c ();
    static Non_Keywords__c NewNonKeyWordObj = new Non_Keywords__c ();
    static Public_Product__c PublicProduct = new Public_Product__c();
    static Case cases = new Case();
    static RecordType[] RT = [select id from RecordType where Name =: 'General Support' and sObjectType =: 'Case']; 

    static TestMethod void TestOne(){
        NonKeyWordObj.Language_code__c = 'en_US'; 
        NonKeyWordObj.Language_Name__c = 'English (United States)'; 
        NonKeyWordObj.Object_Name__c = 'Case'; 
        NonKeyWordObj.Object_Prefix__c = '500'; 
        NonKeyWordObj.Non_Keywords__c = 'One;Two;Three;Four';
        try{
            insert NonKeyWordObj ;
        }
        catch(dmlException e){
        }
        
        PublicProduct.Name = 'Call of Duty';
        Insert PublicProduct;
        
        cases.RecordTypeId = RT[0].id;
        cases.Type = 'Hardware';
        cases.Sub_Type__c = 'Instruments';
        cases.Status = 'New';
        cases.origin = 'Phone Call';
        cases.Product_Effected__c = PublicProduct.id;
        cases.Comment_Not_Required__c = true;
        cases.description = 'One is less than Two';
        cases.W2C_Origin_Language__c = 'en_US';
        insert cases;
  
        cases.description = ' This is a test description to test the functionality when the number of characters in the description field of the case object is more than 255. I hope I have entered enough amount of characters in this description so that the total number of charaters exceed 255';
        update cases;
               
        cases.description = '';
        update cases;
          
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(NonKeyWordObj);
        ApexPages.currentPage().getParameters().put('id', NonKeyWordObj.id);
        NonKeywordManagement NKMObj = new NonKeywordManagement(sc);
        NKMObj.NonKeyWordsObj = NonKeyWordObj;
        NKMObj.getItems();
        NKMObj.getName();
        NKMObj.getLanguages();
        NKMObj.getkeyprefix('Case');
        NKMObj.saveRecord();
        NKMObj.CancelAction();
        
        Test.stopTest();
        
    }
}