@isTest
public class Test_PickUpCase{
    static testmethod void Test_PickUpCase(){
		public_product__c product = new public_product__c();
        product.name = 'test';
        insert product;
               
        case theCase = new case();
        theCase.type = 'Registration';
        theCase.Status = 'New';
        theCase.origin = 'Phone Call';
        theCase.Product_Effected__c = product.id;
        theCase.subject = 'test';
        theCase.Description = 'test'; 
        theCase.comments__c = 'test';
        insert theCase;
    
        ApexPages.StandardController sc = new ApexPages.StandardController(theCase);
        
        test.startTest();
        	Social_PickUpCase_Controller puc = new Social_PickUpCase_Controller(sc);
        	
        	puc.doPickup();
        
        test.stopTest();
    }
}