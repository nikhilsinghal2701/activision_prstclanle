@isTest
public class Test_PostStreamController{
    static testmethod void Test_PostStreamController(){
        public_product__c product = new public_product__c();
        product.name = 'test';
        insert product;
        
        case theCase = new case();
        theCase.type = 'Registration';
        theCase.Status = 'New';
        theCase.origin = 'Phone Call';
        theCase.Product_Effected__c = product.id;
        theCase.subject = 'test';
        theCase.Description = 'test'; 
        theCase.comments__c = 'test';
        insert theCase;
        
        test.starttest();
        PostStreamController psm = new PostStreamController();
        psm.releaseid = theCase.id;
        psm.hideid = theCase.id;
        psm.refresh();
        psm.hide();
        psm.release();
        test.stoptest();
    }
}