@istest
class Test_SocialAutoClose{

    static testmethod void test(){
        List<Case> cases = Test.loadData(Case.sObjectType,'TestData_SocialAutoClose_Cases');
        for(Case c : cases){
            system.debug('c.Status == '+c.Status);
            system.debug('c.CreatedDate == '+c.CreatedDate);
            system.debug('c.CreatedById == '+c.CreatedById);
            system.debug('c.Social_IsHandled__c == '+c.Social_IsHandled__c);
            system.debug('c.Social_LastResponse__c == '+c.Social_LastResponse__c);
            system.debug('c.LastModifiedDate == '+c.LastModifiedDate);
        }
        List<CaseComment> comments = Test.loadData(CaseComment.sObjectType,'TestData_SocialAutoClose_CaseComments');     
        //the load of the comments file updates the LastModifiedDate of the cases previously loaded to "Now" fail
        
        Test.startTest();
            string jobid = system.schedule('SocialAutoCloseTest','0 0 * * * ? *', new SocialAutoClose());
            //CronTrigger ct = [select id, cronexpression, timestriggered, nextfiretime from crontrigger where id=:jobid];
            //system.assertEquals(CRON_EXP, ct.CronExpression);
            //system.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}