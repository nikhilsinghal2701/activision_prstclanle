@isTest
public class Test_Social_LastResponse_Trigger{
    static testmethod void Test_Social_LastResponse_Trigger(){
        public_product__c product = new public_product__c();
        product.name = 'test';
        insert product;
               
        case theCase = new case();
        theCase.type = 'Registration';
        theCase.Status = 'New';
        theCase.origin = 'Phone Call';
        theCase.Product_Effected__c = product.id;
        theCase.subject = 'test';
        theCase.Description = 'test'; 
        theCase.comments__c = 'test';
        insert theCase;
        
        socialPost thePost = new socialPost();
        thePost.Posted = datetime.now();
        thePost.Headline = 'test';
        thePost.name = 'test';
        thePost.parentid = theCase.id;
        thePost.R6Service__IsOutbound__c = true;
        //Social Post Record Type ID Twitter May need to change
        thePost.RecordTypeId = '012U0000000Q7zL';
        
        test.starttest();
            insert thePost;
        test.stoptest();
    }
}