@isTest
Private Class Test_StoreImageAndEmblemValue {
    static Public_Product__c PPdt = new Public_Product__c(); 
    
    static testmethod void testOne (){
        PPdt.Name = 'Test Product';
        insert PPdt;
        PPdt.Redesign_Image__c = 'Redesign Image';
        PPdt.Game_Emblem__c = 'Game Emblem';
        update PPdt;
        PPdt.Redesign_Image__c = 'Redesign Image Redesign Image Redesign Image Redesign Image Redesign Image'+
                                 'Redesign Image Redesign Image Redesign Image Redesign Image Redesign Image'+
                                 'Redesign Image Redesign Image Redesign Image Redesign Image Redesign Image'+                                 
                                 'Redesign Image Redesign Image Redesign Image Redesign Image Redesign Image';
        PPdt.Game_Emblem__c = 'Game Emblem Game Emblem Game Emblem Game Emblem Game Emblem Game Emblem'+
                              'Game Emblem Game Emblem Game Emblem Game Emblem Game Emblem Game Emblem'+
                              'Game Emblem Game Emblem Game Emblem Game Emblem Game Emblem Game Emblem'+                              
                              'Game Emblem Game Emblem Game Emblem Game Emblem Game Emblem Game Emblem';
        update PPdt;
    }
}