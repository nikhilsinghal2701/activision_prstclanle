@isTest
public class Test_Twitter{
    static testmethod void Test_Twitter(){
        string postid;
        integration_credential__c ic = new integration_credential__c();
        ic.Application_Name__c = 'Test';
        ic.access_token__c = '123';
        ic.refresh_token__c = '123';
        ic.api_key__c = '123';
        ic.api_secret__c = '123';
        ic.Twitter_Enabled__c = true;
        insert ic;
        
        test.startTest();
        
        twitter twit = new twitter();
        postid = twit.sendMessage('test');
        twit.destroy(postid);
        
        test.stopTest();
    }
    
}