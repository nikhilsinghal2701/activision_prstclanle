@istest

class Test_UCD_Social_Linker{
    public static string CRON_EXP = '0 0 * * * ?';
    static testmethod void test(){
            UCD_Social_Link__c usl = new UCD_Social_Link__c();
            usl.externalid__c = '1111';
            usl.provider__c = 'twitter';
            usl.Social_Persona_Id__c = '2222';
            insert usl;
            
            Test.startTest();
        
            string jobid = system.schedule('TestUCDLinker',CRON_EXP, new UCD_Social_Linker());
            
            //CronTrigger ct = [select id, cronexpression, timestriggered, nextfiretime from crontrigger where id=:jobid];
            
            //system.assertEquals(CRON_EXP, ct.CronExpression);
            
            //system.assertEquals(0, ct.TimesTriggered);
            
            
        
        Test.stopTest();
    }
}