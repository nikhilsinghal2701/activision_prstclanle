/*******************************************************************************
 * Name         -   Test_welcome_message_controller
 * Description  -   Test Class for Atvi_welcome_message_controller. 
 
 * Modification Log :
 * ---------------------------------------------------------------------------
 * Developer                  Date                      Description
 * ---------------------------------------------------------------------------
 * Mohith Shrivastava         19-APR-12                 Created
 *----------------------------------------------------------------------------
 * Review Log :
 *----------------------------------------------------------------------------
 * Developer                                    Date              Description
 * ---------------------------------------------------------------------------
 * A. Pal(Deloitte Senior Consultant)       20-APR-2012         Added system.debug messages
 *
 ******************************************************************************/
@isTest
public with sharing class Test_welcome_message_controller {    
    
    public static testMethod void testConstructor(){
        UserRole testRole;    
        User testUser;
        
        List<User> users=[select id, name from User where UserRoleId<>null AND IsActive = true AND Profile.Name = 'System Administrator' limit 1];
        if(users.size()>0) testUser=users.get(0);
    
        
        account testaccount=UnitTestHelper.generateTestAccount();
        testaccount.OwnerId=testUser.Id;
        insert testaccount;
        
        contact testcontact=UnitTestHelper.generateTestContact();
        testcontact.AccountId=testaccount.Id;       
        insert testcontact;      
        
         
        User testportaluser=UnitTestHelper.getCustomerPortalUser('abc', testcontact.Id);
        insert testportaluser;
        
         System.runAs(testportaluser)
         
         {
            Test.startTest();
            system.debug('Start of test');
            Atvi_welcome_message_controller testWelcome=new Atvi_welcome_message_controller();
            Test.stopTest();
            system.debug('End of test');
         }        
    }  
}