global class Twitter{
    public integration_credential__c ic{get;set;}
    
    public string theTweet{get; set;}
    public string encodedChirp{get; set;}
    
    public string sigBaseString{get; set;}
    public string derivedAuthHeader{get; set;}
    public string request{get; set;}
    public string response{get; set;}
    
    string conKey,conSec,accTok,accTokSec;
    
    
    public Twitter(){
        ic = [select id, Access_token__c, Refresh_token__c ,API_Key__c,API_Secret__c from integration_credential__c where twitter_enabled__c=true limit 1];
        
        conKey=ic.API_Key__c;
        conSec=ic.API_Secret__c;
        accTok=ic.Access_token__c;
        accTokSec=ic.Refresh_token__c;
        
        /*conKey='KPqU9xEgfor3nCjPTWlMkw';
        conSec='f3gb0JdxPSceiuFRuz4dHxedE15waW7WFmHZwWLrhug';
        accTok='771842018-r5yLNUY6hepgG1XHRSBY50oPj4XOyQoYe6JsZe2G';
        accTokSec='WIGSOmG5P35KU8OWostOps8rJQnqvRmKgMGxeCQswkZ2l';*/
        
    }
    
    
    public string sendDirectMessage(string msg, string screenName){
        theTweet= msg;
        
        //Generate Nonce
        string oAuth_nonce = EncodingUtil.base64Encode(blob.valueOf(string.valueOf(Crypto.getRandomInteger()+system.now().getTime())+string.valueOf(Crypto.getRandomInteger()))).replaceAll('[^a-z^A-Z^0-9]','');
        
        httpRequest newReq = new httpRequest();
        encodedChirp = EncodingUtil.urlEncode(theTweet,'UTF-8');
        system.debug('body == '+'text='+encodedChirp+'&screen_name='+screenName);
        newReq.setBody('screen_name='+screenName+'&text='+encodedChirp);
        newReq.setMethod('POST');
        newReq.setEndpoint('https://api.twitter.com/1.1/direct_messages/new.json');
        
        map<String, String> heads = new map<String, String>{
            'oauth_token'=>accTok,
            'oauth_version'=>'1.0',
            'oauth_nonce'=>oAuth_nonce,
            'oauth_consumer_key'=>conKey,
            'oauth_signature_method'=>'HMAC-SHA1',
            'oauth_timestamp'=>string.valueOf(system.now().getTime()/1000)
        };
                    
        //Alphabetize 
        string[] paramHeads = new string[]{};
        paramHeads.addAll(heads.keySet());
        paramHeads.sort();
        string params = '';
        for(String encodedKey : paramHeads){
            params+=encodedKey+'%3D'+heads.get(encodedKey)+'%26';
        }
        
        //params+='status'+percentEncode('='+percentEncode(theTweet));
        params+='screen_name%3D'+screenName+'%26text%3D'+percentEncode(percentEncode(theTweet));
        
        //Build the base string
        sigBaseString = newReq.getMethod().toUpperCase()+'&'+EncodingUtil.urlEncode(newReq.getEndpoint(),'UTF-8')+'&'+params;
        system.debug('signatureBaseString == '+sigBaseString);
        
        //calculate signature
        string sigKey = EncodingUtil.urlEncode(conSec,'UTF-8')+'&'+EncodingUtil.urlEncode(accTokSec,'UTF-8');
        blob mac = crypto.generateMac('hmacSHA1', blob.valueOf(sigBaseString), blob.valueOf(sigKey));
        string oauth_signature = EncodingUtil.base64Encode(mac);
        heads.put(EncodingUtil.urlEncode('oauth_signature','UTF-8'), EncodingUtil.urlEncode(oauth_signature,'UTF-8'));
        
        //build the authorization header
        paramHeads.clear();
        paramHeads.addAll(heads.keySet());
        paramHeads.sort();
        string oAuth_Body = 'OAuth ';
        for(String key : paramHeads){
            oAuth_Body += key+'="'+heads.get(key)+'", ';
        }
        oAuth_Body = oAuth_Body.subString(0, (oAuth_Body.length() - 2));
        newReq.setHeader('Authorization', oAuth_Body);
        system.debug('Authroization Header == '+oAuth_Body);
        newReq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        
        derivedAuthHeader = newReq.getHeader('Authorization');
        request = newReq.getBody();
        
        if(!test.isRunningTest()){
            httpResponse httpRes = new http().send(newReq);
            response = httpRes.getBody();
        }
        else{
            response = '{"id":"123"}';
        }
            
        system.debug(response);
        string jsonid = 'error';
        JSONParser parser = JSON.createParser(response);
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                String fieldName = parser.getText();
                parser.nextToken();
                if(fieldName == 'id') {
                    jsonid = parser.getText();
                    break;
                } 
            }
        }
        return jsonid;        
    }
    
    public string sendMessage(string msg){
        theTweet= msg;
        
        //Generate Nonce
        string oAuth_nonce = EncodingUtil.base64Encode(blob.valueOf(string.valueOf(Crypto.getRandomInteger()+system.now().getTime())+string.valueOf(Crypto.getRandomInteger()))).replaceAll('[^a-z^A-Z^0-9]','');
        
        httpRequest newReq = new httpRequest();
        encodedChirp = EncodingUtil.urlEncode(theTweet,'UTF-8');
        newReq.setBody('status='+encodedChirp);
        newReq.setMethod('POST');
        newReq.setEndpoint('https://api.twitter.com/1.1/statuses/update.json');
        
        map<String, String> heads = new map<String, String>{
            'oauth_token'=>accTok,
                'oauth_version'=>'1.0',
                'oauth_nonce'=>oAuth_nonce,
                'oauth_consumer_key'=>conKey,
                'oauth_signature_method'=>'HMAC-SHA1',
                'oauth_timestamp'=>string.valueOf(system.now().getTime()/1000)
                };
                    
                    //Alphabetize 
                    string[] paramHeads = new string[]{};
                        paramHeads.addAll(heads.keySet());
        paramHeads.sort();
        string params = '';
        for(String encodedKey : paramHeads){
            params+=encodedKey+'%3D'+heads.get(encodedKey)+'%26';
        }
        
        params+='status'+percentEncode('='+percentEncode(theTweet));
        
        //Build the base string
        sigBaseString = newReq.getMethod().toUpperCase()+'&'+EncodingUtil.urlEncode(newReq.getEndpoint(),'UTF-8')+'&'+params;
        
        //calculate signature
        string sigKey = EncodingUtil.urlEncode(conSec,'UTF-8')+'&'+EncodingUtil.urlEncode(accTokSec,'UTF-8');
        blob mac = crypto.generateMac('hmacSHA1', blob.valueOf(sigBaseString), blob.valueOf(sigKey));
        string oauth_signature = EncodingUtil.base64Encode(mac);
        heads.put(EncodingUtil.urlEncode('oauth_signature','UTF-8'), EncodingUtil.urlEncode(oauth_signature,'UTF-8'));
        
        //build the authorization header
        paramHeads.clear();
        paramHeads.addAll(heads.keySet());
        paramHeads.sort();
        string oAuth_Body = 'OAuth ';
        for(String key : paramHeads){
            oAuth_Body += key+'="'+heads.get(key)+'", ';
        }
        oAuth_Body = oAuth_Body.subString(0, (oAuth_Body.length() - 2));
        newReq.setHeader('Authorization', oAuth_Body);
        
        derivedAuthHeader = newReq.getHeader('Authorization');
        request = newReq.getBody();
        
        if(!test.isRunningTest()){
            httpResponse httpRes = new http().send(newReq);
            response = httpRes.getBody();
        }
        else{
            response = '{"id":"123"}';
        }
            system.debug(response);
            string jsonid = 'error';
            JSONParser parser = JSON.createParser(response);
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if(fieldName == 'id') {
                        jsonid = parser.getText();
                        break;
                    } 
                }
            }
            return jsonid;
    }
    
    public void destroy(string twitid){
        //Generate Nonce
        string oAuth_nonce = EncodingUtil.base64Encode(blob.valueOf(string.valueOf(Crypto.getRandomInteger()+system.now().getTime())+string.valueOf(Crypto.getRandomInteger()))).replaceAll('[^a-z^A-Z^0-9]','');
        httpRequest newReq = new httpRequest();
        
        newReq.setMethod('POST');
        newReq.setEndpoint('https://api.twitter.com/1.1/statuses/destroy/'+twitid+'.json');
        
        map<String, String> heads = new map<String, String>{
            'oauth_token'=>accTok,
                'oauth_version'=>'1.0',
                'oauth_nonce'=>oAuth_nonce,
                'oauth_consumer_key'=>conKey,
                'oauth_signature_method'=>'HMAC-SHA1',
                'oauth_timestamp'=>string.valueOf(system.now().getTime()/1000)
                };
                    
                    //Step 3 - these base parts must be sorted alphabetically, the map above doesn't maintain order, so sort again
                    string[] paramHeads = new string[]{};
                        paramHeads.addAll(heads.keySet());
        paramHeads.sort();
        string params = '';
        for(String encodedKey : paramHeads){
            params+=encodedKey+'%3D'+heads.get(encodedKey)+'%26';
        }
        params = params.subString(0, (params.length() - 3));
        
        //Step 4 - actually build the base string
        sigBaseString = newReq.getMethod().toUpperCase()+'&'+EncodingUtil.urlEncode(newReq.getEndpoint(),'UTF-8')+'&'+params;
        
        //calculate the signature for this request
        string sigKey = EncodingUtil.urlEncode(conSec,'UTF-8')+'&'+EncodingUtil.urlEncode(accTokSec,'UTF-8');
        blob mac = crypto.generateMac('hmacSHA1', blob.valueOf(sigBaseString), blob.valueOf(sigKey));
        string oauth_signature = EncodingUtil.base64Encode(mac);
        heads.put(EncodingUtil.urlEncode('oauth_signature','UTF-8'), EncodingUtil.urlEncode(oauth_signature,'UTF-8'));
        
        //build the authorization header
        paramHeads.clear();
        paramHeads.addAll(heads.keySet());
        paramHeads.sort();
        string oAuth_Body = 'OAuth ';
        for(String key : paramHeads){
            oAuth_Body += key+'="'+heads.get(key)+'", ';
        }
        oAuth_Body = oAuth_Body.subString(0, (oAuth_Body.length() - 2));
        newReq.setHeader('Authorization', oAuth_Body);
        
        derivedAuthHeader = newReq.getHeader('Authorization');
        request = newReq.getBody();
        
        if(!test.isRunningTest()){
            httpResponse httpRes = new http().send(newReq);
            response = httpRes.getBody();
        }
        
    }
    
    public string percentEncode(String toEncode){
        string encoded = '';
        if(toEncode != null){
            for(Integer i = 0; i < toEncode.length(); i++){
                String toEncodeChar = toEncode.subString(i, i+1);
                if(allowedChars.contains(toEncodeChar)){
                    encoded += toEncodeChar;
                }else{
                    String hexed = encodingUtil.convertToHex(blob.valueOf(toEncodeChar));
                    for(integer x = 0; x != hexed.length(); x+=2){
                        encoded += '%'+hexed.subString(x, x+2).toUpperCase();
                    }
                }
            }
        }
        return encoded;
    }
    
    public set<String> allowedChars = new set<String>{
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '.', '_', '~',
        'A', 'B', 'C', 'D', 'E', 'F','G', 'H', 'I', 'J', 'K', 'L','M', 'N', 'O', 'P', 'Q', 'R','S', 'T', 'U', 'V', 'W', 'X','Y', 'Z',
        'a', 'b', 'c', 'd', 'e', 'f','g', 'h', 'i', 'j', 'k', 'l','m', 'n', 'o', 'p', 'q', 'r','s', 't', 'u', 'v', 'w', 'x','y', 'z'
    };               
}