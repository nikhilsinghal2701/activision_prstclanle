public class TwitterOAuthRedirect{
    public map<string,string> header{get;set;}
    public map<string,string> dump{get;set;}
    public string oauthToken{get;set;}
    public string oauthVerifier{get;set;}
    public integration_credential__c ic{get;set;}
    
    public string conKey{get;set;}
    public string conSec{get;set;}
    public string accTok{get;set;}
    public string accTokSec{get;set;}
    public string sigBaseString{get;set;}
    public string response{get;set;}
    
    map<String, String> heads{get;set;}
    
    public string oAuth_nonce = EncodingUtil.base64Encode(blob.valueOf(string.valueOf(Crypto.getRandomInteger()+system.now().getTime())+string.valueOf(Crypto.getRandomInteger()))).replaceAll('[^a-z^A-Z^0-9]','');
    
    public TwitterOAuthRedirect(){
        header = ApexPages.currentpage().getHeaders();
        dump = ApexPages.currentpage().getParameters();
        oauthToken = ApexPages.currentPage().getparameters().get('oauth_token');
        oauthVerifier = ApexPages.currentPage().getparameters().get('oauth_verifier');
        ic = [select id, oAuth_Token__c, oAuth_Secret__c from integration_credential__c where oAuth_Token__c =:oauthToken];
        
        conKey = 'KPqU9xEgfor3nCjPTWlMkw';
        conSec = 'f3gb0JdxPSceiuFRuz4dHxedE15waW7WFmHZwWLrhug';
        heads = new map<String, String>{
            'oauth_token'=>ic.oAuth_Token__c,
                'oauth_version'=>'1.0', //yes
                'oauth_nonce'=>oAuth_nonce, //yes
                'oauth_consumer_key'=>conKey, //yes
                'oauth_signature_method'=>'HMAC-SHA1', //yes
                'oauth_timestamp'=>string.valueOf(system.now().getTime()/1000), //yes,
                'oauth_verifier'=>oauthVerifier
                };
                    
                    httpRequest newReq = new httpRequest();
        newReq.setMethod('POST');
        newReq.setEndpoint('https://api.twitter.com/oauth/access_token');
        
        map<String, String> encodedHeadMap = new map<String, String>();
        for(String key : heads.keySet()){
            encodedHeadMap.put(percentEncode(key), percentEncode(heads.get(key)));
        }
        
        string[] paramHeads = new string[]{};
            paramHeads.addAll(heads.keySet());
        paramHeads.sort();
        string params = '';
        for(String encodedKey : paramHeads){
            params+=encodedKey+'%3D'+heads.get(encodedKey)+'%26';
        }
        params = params.substring(0,(params.length() - 3));
        
        sigBaseString = newReq.getMethod().toUpperCase()+'&'+percentEncode(newReq.getEndpoint())+'&'+params;
        
        string sigKey = percentEncode(conSec)+'&'+percentEncode(ic.oAuth_Secret__c);
        blob mac = crypto.generateMac('hmacSHA1', blob.valueOf(sigBaseString), blob.valueOf(sigKey));
        string oauth_signature = EncodingUtil.base64Encode(mac);
        heads.put(percentEncode('oauth_signature'), percentEncode(oauth_signature));
        
        paramHeads.clear();
        paramHeads.addAll(heads.keySet());
        paramHeads.sort();
        string oAuth_Body = 'OAuth ';
        for(String key : paramHeads){
            oAuth_Body += key+'="'+heads.get(key)+'", ';
        }
        oAuth_Body = oAuth_Body.subString(0, (oAuth_Body.length() - 2));
        newReq.setHeader('Authorization', oAuth_Body);
        
        httpResponse httpRes = new http().send(newReq);
        response = httpRes.getBody();
        
        ic.Access_token__c = response.substring(response.indexof('oauth_token=')+'oauth_token='.length(),response.indexof('&oauth_token_secret='));
        ic.Refresh_token__c = response.substring(response.indexof('&oauth_token_secret=')+'&oauth_token_secret='.length(),response.indexof('&user_id='));
        //update ic;
        
		system.debug('newreq.getheader(\'Authorization\') == '+newreq.getheader('Authorization'));

    }
    
    public pagereference redirect(){
        update ic;
        return new pagereference('/'+(string)ic.Id);
    }
    
    public string percentEncode(String toEncode){
        set<String> allowedChars = new set<String>{
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '.', '_', '~',
                'A', 'B', 'C', 'D', 'E', 'F','G', 'H', 'I', 'J', 'K', 'L','M', 'N', 'O', 'P', 'Q', 'R','S', 'T', 'U', 'V', 'W', 'X','Y', 'Z',
                'a', 'b', 'c', 'd', 'e', 'f','g', 'h', 'i', 'j', 'k', 'l','m', 'n', 'o', 'p', 'q', 'r','s', 't', 'u', 'v', 'w', 'x','y', 'z'
                };
                    string encoded = '';
        if(toEncode != null){
            for(Integer i = 0; i < toEncode.length(); i++){
                String toEncodeChar = toEncode.subString(i, i+1);
                if(allowedChars.contains(toEncodeChar)){
                    encoded += toEncodeChar;
                }else{
                    String hexed = encodingUtil.convertToHex(blob.valueOf(toEncodeChar));
                    for(integer x = 0; x != hexed.length(); x+=2){
                        encoded += '%'+hexed.subString(x, x+2).toUpperCase();
                    }
                }
            }
        }
        return encoded;
    }
    
}