global class UCD_Social_Linker implements Schedulable {
   global void execute(SchedulableContext SC) {
      doLink();
   }
   //cron expression = "0 5 * * * ?" every day, at 5 minutes past every hour
   public static void doLink(){
       update [SELECT Id FROM UCD_Social_Link__c WHERE LastModifiedDate > :system.now().addMinutes(-6) AND (NOT LastModifiedBy.Name LIKE 'tibco%')];
   }
}