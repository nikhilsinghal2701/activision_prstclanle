public class URLShortener {
    public string longURL{get;set;}
    public string shortURL{get;set;}
    public string endpoint{get;set;}
    public string ACCESS_TOKEN{get;set;}
    
    public URLShortener(ApexPages.StandardController stdController){
        longURL = 'Paste HERE!';
        shortURL = 'Comes Out Here!';
        endpoint = 'https://api-ssl.bitly.com';
        ACCESS_TOKEN = '1269d6b298831ca6fa5956b2df83b6c28bc9a1ff';
    }
    
    public URLShortener(){
        longURL = 'Paste HERE!';
        shortURL = 'Comes Out Here!';
        endpoint = 'https://api-ssl.bitly.com';
        ACCESS_TOKEN = '1269d6b298831ca6fa5956b2df83b6c28bc9a1ff';
    }
    
    public pagereference shorten(){

        if(longURL.containsignorecase('Paste HERE!')){
            longURL = 'Paste HERE!';
            return null;
        }
        else{
        //endpoint = percentEncode(endpoint);
        try{
        httpRequest req = new httpRequest();
        req.setEndpoint(endpoint+'/v3/shorten?access_token='+ACCESS_TOKEN+'&longUrl='+longURL);
        req.setMethod('GET');
        
        Http http = new Http();
     	HTTPResponse res = http.send(req);
        
        shortURL = res.getBody();
        
        JSONParser parser = JSON.createParser(shortURL);
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if(fieldName == 'url') {
                        shortURL = parser.getText();
                        break;
                    } 
                }
                shortURL = 'ERROR';
            }
        }
        catch(exception e){
            shortURL = 'ERROR';
            return null;
        }
        return null;
        }
    }
    
    public string percentEncode(String toEncode){
        string encoded = '';
        if(toEncode != null){
            for(Integer i = 0; i < toEncode.length(); i++){
                String toEncodeChar = toEncode.subString(i, i+1);
                if(allowedChars.contains(toEncodeChar)){
                    encoded += toEncodeChar;
                }else{
                    String hexed = encodingUtil.convertToHex(blob.valueOf(toEncodeChar));
                    for(integer x = 0; x != hexed.length(); x+=2){
                        encoded += '%'+hexed.subString(x, x+2).toUpperCase();
                    }
                }
            }
        }
        return encoded;
    }
    
    public set<String> allowedChars = new set<String>{
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '.', '_', '~',
        'A', 'B', 'C', 'D', 'E', 'F','G', 'H', 'I', 'J', 'K', 'L','M', 'N', 'O', 'P', 'Q', 'R','S', 'T', 'U', 'V', 'W', 'X','Y', 'Z',
        'a', 'b', 'c', 'd', 'e', 'f','g', 'h', 'i', 'j', 'k', 'l','m', 'n', 'o', 'p', 'q', 'r','s', 't', 'u', 'v', 'w', 'x','y', 'z'
    };               
}