/**
* @author           Karthik
* @date             07-FEB-2012
* @description      This class provides reusable methods for unit test methods.
                    Provides static methods for instantiating SObjects
                    and other objects required by testing. Allows re-use and
                    allows default values required by field validation or other
                    rules to be pre-filled and updated easily.
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Karthik              		07-FEB-2012  			Created
* A. Pal                	07-FEB-2012  			Added new method - generateKnowledgeArticle()
* Karthik              		10-FEB-2012 			Added methods to create test Account, Contact
                                    				and isEmail, isBlank utility methods
* Suman			     		17-FEB-2012   			Added customerPortalUser(String, String)
* Suman			     		23-FEB-2012   			Copied kavNames method from pkb_Controller
* Mohith               		14-MAR-2012 			Added new method -generatePublicProduct() which hold public products
* ---------------------------------------------------------------------------------------------------
* Review Log	:
*---------------------------------------------------------------------------------------------------
* Reviewer											Review Date						Review Comments
* --------											------------					--------------- 
* A. Pal(Deloitte Senior Consultant)				22-MAR-2012						Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
*/
public with sharing class UnitTestHelper {
     static String objType;
     static SObject kavObj;
	//Generate test customer portal user
    public static  User getCustomerPortalUser(String uniqueKey, String contactId) {     
        Id portalLicenseId=[select id, name from UserLicense where name like '%Customer Portal%' limit 1].Id;        
    	return new User(
            Alias = uniqueKey,
            Email = uniqueKey + '@testorg.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'Test' + uniqueKey,
            FirstName = uniqueKey,
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ProfileId = [select Id from Profile where UserLicenseId=:portalLicenseId limit 1].Id,
            TimeZoneSidKey = 'America/Chicago',
            Username = uniqueKey + '@test' + Math.round(Math.random() * 10000) + '.com',
            Country = 'Great Britain',
            IsActive = true,
            ContactId = contactId
        );
    }
    //Generate test account
    public static Account generateTestAccount() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Type__c = 'Agency';
        return acc;
    }
    //Generate test contact
    public static Contact generateTestContact() {
        Contact con = new Contact();
        con.FirstName='test';
        con.Country__c='United States';
        con.Salutation='';
        con.LastName='test1';
        con.Email='abc@atvi.com';
        con.Elite_Status__c='Standard';
        con.MailingCountry='US';
        con.Gamertag__c='testtag';
        return con;
    }
    //Generate test case
    public static Case generateTestCase() {
        Case cs = new Case();
        cs.Status = 'New';
        cs.Origin = 'Email';
        cs.Priority = 'Medium';
        cs.Subject = 'Test Case';
        return cs;  
    }   
    //Generate test case comment
    public static CaseComment generateTestCaseComment() {
        CaseComment cscom = new CaseComment();
        cscom.CommentBody = 'Test Comment Body';
        return cscom;
    }
    //Generate test user
    public static User generateTestUser() {
        User u = new User();
        u.Alias = 'Test';
        u.Email = 'testuser@activision.com';
        u.EmailEncodingKey = 'UTF-8';
        u.LastName = 'Test User';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.Username = 'testuser'+System.now().getTime()+'@activision.com';
        u.ProfileId=[select Id from Profile where Name = 'Standard User' limit 1].Id;
        return u;
    }
    //Generate test Knowledge Article
    public static KnowledgeArticle generateKnowledgeArticle() {
        KnowledgeArticle testKa=new KnowledgeArticle();         
        return testKa;  
    }
    //Email validation method
    public static Boolean isEmail(String s) {
        if(isBlank(s)) 
            return false;
        String p = '\\w+([-+.\']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*';
        Pattern pat = Pattern.compile(p);
        Matcher mat = pat.matcher(s);
        return mat.matches();
    }    
    //Null check method
    public static Boolean isBlank(String s) {
        return (s == null || s == '');
    } 
    //Generate test Public Product
    public static Public_Product__c generatePublicProduct() {
        Public_Product__c p=new Public_Product__c();
        p.Image__c='test';
        p.Name='test_product';
        p.Type__c='Software';
        return p;     
    } 
    //Generate test Asset
     public static Asset_Custom__c generateAsset(Contact C,Public_Product__c prodId) {
        Asset_Custom__c A= new Asset_Custom__c() ;
        A.Gamer__c=C.Id;
        A.Image__c='test';
        A.My_Product__c = prodId.Id;        
        return A;
     }     
}