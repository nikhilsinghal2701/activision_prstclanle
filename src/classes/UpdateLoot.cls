@RestResource(urlMapping = '/UpdateLoot/*')
global with sharing class UpdateLoot {

    @HttpPost
    Global Static String UpdateLoot(list<loot_recovery__c> lootlist) {
    	upsert lootlist gid__c;

        return 'Success';
    }
}