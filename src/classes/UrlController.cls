// Jonathan Hersh - jhersh@salesforce.com - 8/5/2010
public with sharing class UrlController {  
    public string welcomemsg{get;set;}
    public string url{get;set;}
    public string spid{get;set;}
    public string userAgent{get;set;}
    public boolean surveyExpired{get;set;}
    public boolean shortLinkIsASurvey{get;set;}
    public UrlController(){
        try{
            url = Apexpages.currentpage().getparameters().get('url');
        }catch(Exception e){
            url = '';
        }

        try{
            spid = Apexpages.currentpage().getparameters().get('psid');
        }catch(Exception e){
            spid = '';
        }

        try{
            userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
        }catch(Exception e){
            userAgent = '';
        }
        surveyExpired = false;
        shortLinkIsASurvey = false;

        //welcomemsg = 'Click Continue to Start Survey'; //replace with Label
    }
    public pageReference isVisitSurvey(){        
        // Find our destination
        if( url == null || !urlHomeController.isSetup )
            return null;
        
        url = string.escapeSingleQuotes(url.trim());
            
        if( url.startsWith('/') )
            url = url.substring( 1, url.length() );
        
        if( url == '' )
            return null;
        
        Short_URL__c[] urls;
        try {
            urls = [select Id, URL__c, Long_URL__c, Twitter_Post_ID__c, CreatedDate, Clicked__c
                    from Short_URL__c
                    where short_url__c = :url
                    order by createddate asc limit 1];
        } catch( QueryException e ) {}

        if( urls == null || urls.isEmpty() ) //moved above the click log because if there's no URL the insert will fail
            return null;
            
        string redirect = urls[0].url__c;
        if(urls[0].Long_URL__c != '' && urls[0].Long_URL__c != null)
            redirect = urls[0].Long_URL__c;
            
        //Social Surveys expire after x days
        if(redirect.containsIgnoreCase('activision.allegiancetech.com/cgi-bin/qwebcorporate.dll?idx=DHSMQQ')){
            shortLinkIsASurvey = true;
            if(urls[0].Clicked__c == true){
                surveyExpired = true;
            }else{
                try{
                    //default to twitter, because that's what Allegiance does if incident_creation_channel isn't specified
                    String settingName = 'Twitter';
                    if(redirect.contains('incident_creation_channel='))
                        settingName = redirect.split('incident_creation_channel=')[1].split('&')[0];
                    Integer expiryDays = (Integer) ApexCodeSetting__c.getValues(settingName+'_Survey_Expiration_Days').Number__c;
                    surveyExpired = system.today() > urls[0].createdDate.addDays(expiryDays);
                }catch(Exception e){
                    system.debug('ERROR ======== '+e);
                    surveyExpired = true;
                }
            }
            if(surveyExpired) return null;
        }
        if(shortLinkIsASurvey == false) return doRedirect();
        return null; 
    }
    public pageReference doRedirect() {
        PageReference pr = new PageReference( urlHomeController.DEFAULT_URL );
        pr.setRedirect( true );
        
        // Find our destination
        system.debug('spid == '+spid);
        system.debug('url == '+url);
        system.debug('urlHomeController.isSetup == '+urlHomeController.isSetup);
        if( url == null || !urlHomeController.isSetup )
            return pr;
        
        url = string.escapeSingleQuotes(url.trim());
        system.debug('url == '+url);
            
        if( url.startsWith('/') )
            url = url.substring( 1, url.length() );
        system.debug('url == '+url);
        
        if( url == '' )
            return pr;
        
        Short_URL__c[] urls;
        try {
            urls = [select Id, URL__c, Long_URL__c, Twitter_Post_ID__c, CreatedDate, Clicked__c
                    from Short_URL__c
                    where short_url__c = :url
                    order by createddate asc limit 1];
        } catch( QueryException e ) {}

        if( urls == null || urls.isEmpty() ) //moved above the click log because if there's no URL the insert will fail
            return pr;
        
        // Save this click
        Map<string,string> headers = Apexpages.currentpage().getHeaders();
        
        Short_URL_Click__c click = new Short_URL_Click__c();
        click.Short_URL__c = urls[0].id;
        if(headers.containsKey('User-Agent'))
            click.User_Agent__c = headers.get('User-Agent');
        if(headers.containsKey('X-Salesforce-SIP'))
            click.IP_Address__c = headers.get('X-Salesforce-SIP');
        if(headers.containsKey('Referer'))
            click.Referrer__c = headers.get('Referer');
        insert click;
            
        string redirect = urls[0].url__c;
        if(urls[0].Long_URL__c != '' && urls[0].Long_URL__c != null)
            redirect = urls[0].Long_URL__c;
        
        if( !redirect.tolowercase().startsWith('http://') &&
            !redirect.tolowercase().startsWith('https://') &&
            !redirect.tolowercase().startsWith('mailto:') )
            redirect = 'http://' + redirect;
            
        if( redirect.endsWith( '/' ) )
            redirect = redirect.substring( 0, redirect.length() - 1 );
            
        pr = new PageReference( redirect );
        pr.setRedirect( true );
        
        //update short_url__c to clicked__c true
        if(shortLinkIsASurvey && ! userAgent.toLowercase().contains('bot') && ! userAgent.contains('Slurp')){
            try{
                urls[0].clicked__c = true;
                update urls;
            }catch(Exception e){
                system.debug('ERROR ========= '+e);
            }
            try{
                destroyTweet(urls[0].Twitter_Post_ID__c);
            }catch(Exception e){
                system.debug('ERROR ========= '+e);
            }
        }
        
        return pr;
    }
    
    public pagereference OptOut(){
        try{
        PageReference pr = new PageReference( urlHomeController.DEFAULT_URL );
        pr.setRedirect( true );
        
        //Find Destination
        if( url == null || !urlHomeController.isSetup )
            return pr;
            
        url = string.escapeSingleQuotes(url.trim());
            
        if( url.startsWith('/') )
            url = url.substring( 1, url.length() );
            
        if( url == '' )
            return pr;
        
        //Get Short to add click
        Short_URL__c[] urls;
        try {
            urls = [select id, URL__c, Twitter_Post_ID__c,social_name__c
            from Short_URL__c
            where short_url__c = :url
            order by createddate asc limit 1];
        } catch( QueryException e ) {}
        
        if( urls == null || urls.isEmpty() )
            return pr;
        
        // Save this click
        Map<string,string> headers = Apexpages.currentpage().getHeaders();
        Short_URL_Click__c click = new Short_URL_Click__c();
        click.Short_URL__c = urls[0].id;
        click.User_Agent__c = headers.get('User-Agent');
        click.IP_Address__c = headers.get('X-Salesforce-SIP');
        click.Referrer__c = headers.get('Referer');
        insert click;

        urls[0].clicked__c = true;
        
        try { 
            update urls;
        } catch( Exception e ) {}
        
        socialpersona sp = [SELECT id from socialpersona where id=:urls[0].social_name__c limit 1];
        sp.Allegiance_Opt_Out__c = true;
        update sp;
        pr = page.SurveyOptOut;
        pr.setRedirect(true);
        
        //Destroy Post
        destroyTweet(urls[0].Twitter_Post_ID__c);
        
        return pr;
        }catch(Exception e){
            system.debug('ERROR === '+e);
        }
        return null;
    }
    
    @future(callout=true)
    public static void destroyTweet(String tweetId){
        Twitter twit = new Twitter();
        twit.destroy(tweetId);
    }
    
    
}