@isTest
public class UrlController_Test{
	public static testmethod void testRedirect() {
        
        Account testAccount= new Account();
        testAccount.Name = 'Test account';
        testAccount.Type__c = 'Test User';
        insert testAccount;
        
        User testUser = [SELECT Subscriptions__c, Street, State, Id,
                PostalCode, Phone, MobilePhone, LastName,
                Gender__c, FirstName, Email, Country, ContactId,
                IsPortalEnabled, City, Birthdate__c, LanguageLocaleKey
             FROM User LIMIT 1];

    
        Contact testContact = new Contact();
        testContact.LastName = 'Test User';
        testContact.Languages__c = 'English';
        testContact.MailingCountry = 'US';
        testContact.AccountId = testAccount.id;
        insert testContact;
        
        SocialPersona sp = new SocialPersona();
        sp.ParentId = testContact.id;
        sp.Name = 'test';
        sp.Provider = 'Twitter';
        insert sp;
    	
        string postid;
        integration_credential__c ic = new integration_credential__c();
        ic.Application_Name__c = 'Test';
        ic.access_token__c = '123';
        ic.refresh_token__c = '123';
        ic.api_key__c = '123';
        ic.api_secret__c = '123';
        ic.Twitter_Enabled__c = true;
        insert ic;
        
    	PageReference notfound = new PageReference( urlHomeController.DEFAULT_URL );
    	PageReference redirect = new PageReference('http://www.salesforce.com');
        
        linkforce_settings__c ls = new linkforce_settings__c();
        ls.base_url__c = 'http://csuat-activisionsupport.cs9.force.com/lf/';
        ls.Default_URL__c = 'http://rewards.activision.com';
        insert ls;
        
        Short_URL__c url = new Short_URL__c();
    	url.url__c = redirect.getURL();
    	url.Custom_URL__c = 'activision.allegiancetech.com/cgi-bin/qwebcorporate.dll?idx=DHSMQQ';
        url.Long_URL__c = 'activision.allegiancetech.com/cgi-bin/qwebcorporate.dll?idx=DHSMQQ';
        insert url;
        
        Short_URL__c url1 = new Short_URL__c();
    	url1.url__c = redirect.getURL();
    	url1.Custom_URL__c = 'lalala';
        url1.Long_URL__c = 'lalala';
        url1.Social_Name__c = sp.id;
        insert url1;
        
    	test.startTest();
        
        
        // Test a null URL
    	Apexpages.currentpage().getparameters().put('url','activision.allegiancetech.com/cgi-bin/qwebcorporate.dll?idx=DHSMQQ');
        apexpages.currentPage().getheaders().put('USER-AGENT','bot');
        apexpages.currentPage().getheaders().put('X-Salesforce-SIP','bot');
        apexpages.currentPage().getheaders().put('Referer','bot');
    	urlController uc = new urlController();
        uc.isVisitSurvey();
        
        Apexpages.currentpage().getparameters().put('url','lalala');
        apexpages.currentPage().getheaders().put('USER-AGENT','bot');
        urlcontroller uc1 = new urlcontroller();
        uc1.isVisitSurvey();
        
        uc.OptOut();
    	
        Apexpages.currentpage().getparameters().put('url','/lalala');
        urlcontroller uc2 = new urlcontroller();
        uc2.isVisitSurvey();
 
        Apexpages.currentpage().getparameters().put('url','');
        urlcontroller uc3 = new urlcontroller();
        uc3.isVisitSurvey();
        
        Apexpages.currentpage().getparameters().put('url','/incident_creation_channel=Facebook');
        urlcontroller uc4 = new urlcontroller();
        uc4.shortLinkIsASurvey = true;
        uc4.isVisitSurvey();
        urlcontroller.destroyTweet('123');
    	//PageReference p = uc.doRedirect();
    	//PageReference p = uc.isvisitsurvey();
        
    	//system.assertEquals( notfound.getURL(), p.getURL() );
    	
    	// Test a URL not in the system
    	
    	//Apexpages.currentpage().getparameters().put('url','lalalalalalalalalalalalalalala');
    	
        //uc.url = 'http://www.google.com';
    	/*p = uc.doRedirect();*/
    	//p = uc1.isvisitsurvey();
        
    	//system.assertEquals( notfound.getURL(), p.getURL() );
    	
    	// Add a URL to the system
    	/*Short_URL__c url = new Short_URL__c();
    	url.url__c = redirect.getURL();
    	url.Custom_URL__c = 'lalalala';
    	
    	insert url;
    	
    	Apexpages.currentpage().getparameters().put('url','lalalala');
    	
    	p = uc.doRedirect();
    	
    	system.assertEquals( redirect.getURL(), p.getURL() );   */
        
        
        test.stopTest();
    }
}