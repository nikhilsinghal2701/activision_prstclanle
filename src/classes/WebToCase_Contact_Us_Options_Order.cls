public class WebToCase_Contact_Us_Options_Order{
    sObject pTheRecord;
    public sObject getTheRecord(){
        return pTheRecord;
    }
    public void setTheRecord(Sobject so){
        if(pTheRecord == null){
            String apiName = so.getSObjectType().getDescribe().getLocalName();
            leftOptions = new selectOption[]{};
            rightOptions = new selectOption[]{};
            String recId = (Id) so.get('Id');
            pTheRecord = database.query('SELECT Contact_Options_Order__c FROM '+apiName+' WHERE Id = \''+recId+'\'');
            
            set<string> theOptionsSet = new set<string>();
            for (Schema.PicklistEntry a : WebToCase__c.Contact_Options_Order_Master__c.getDescribe().getPickListValues()) { //for all values in the picklist list         
                theOptionsSet.add(a.getValue().toLowerCase());
            }
    
            string rawVal = (string) pTheRecord.get(theFieldName);
            if(rawVal != '' && rawVal != null){
                for(String val : rawVal.split(';')){
                    String lCaseVal = val.toLowerCase();
                    rightOptions.add(new selectOption(lCaseVal, lCaseVal));
                    theOptionsSet.remove(lCaseVal);
                }
            }else{
                //this is a hardcoded list because it is the default order requested by CS
                for(String opt : new string[]{'chat','twitter','facebook','forum','phone','email','form','ambassadorchat','scheduleacall'}){
                    rightOptions.add(new selectOption(opt, opt));
                }
                theOptionsSet = new set<String>();
            }
            for(String val : theOptionsSet){
                leftOptions.add(new selectOption(val, val));
            }
        }
        
    }
    
    public selectOption[] leftOptions {get; set;}
    public selectOption[] rightOptions {get; set;}
    
    string theFieldName = 'Contact_Options_Order__c';
    
    public pageReference save(){
        try{
            if(pTheRecord !=  null){            
                String theNewVal = '';
                for(SelectOption so : rightOptions){
                    theNewVal += so.getValue()+';';
                }
                if(theNewVal != ''){
                    theNewVal = theNewVal.substring(0, (theNewVal.length() - 1));
                }
                pTheRecord.put(theFieldName, theNewVal);
                update pTheRecord;
            }else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.SEVERITY.FATAL, 'Something went wrong'));
            }
        }catch(Exception e){
            apexPages.addMessages(e);
        }
        return null;
    }
}