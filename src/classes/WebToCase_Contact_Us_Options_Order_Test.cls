@isTest
public class WebToCase_Contact_Us_Options_Order_Test{
    public static testMethod void createData(){
        Public_Product__c pp = new Public_Product__c();
        insert pp;
        
        WebToCase__c w2c = new WebToCase__c();
        insert w2c;
        
        WebToCase_Platform__c w2cp = new WebToCase_Platform__c(
            Name__c = 'test',
            WebToCase__c = w2c.Id
        );
        insert w2cp;
        
        WebToCase_GameTitle__c w2cg = new WebToCase_GameTitle__c(
            WebToCase_Platform__c = w2cp.Id,
            Public_Product__c = pp.Id,
            Title__c = 'test'
        );
        insert w2cg;
        
        WebToCase_Type__c w2ct = new WebToCase_Type__c(
            WebToCase_GameTitle__c = w2cg.id,
            Description__c = 'test'
        );
        insert w2ct;
        
        WebToCase_SubType__c w2cst = new WebToCase_SubType__c(
            WebToCase_Type__c = w2ct.Id
        );
        insert w2cst;
    }
    public static testMethod void test1(){
        createData();
        WebToCase_Contact_Us_Options_Order tstCls = new WebToCase_Contact_Us_Options_Order();
        sObject theObj = tstCls.getTheRecord();
        system.assertEquals(null, theObj);
        
        WebToCase__c w2c = [SELECT Id FROM WebToCase__c];
        tstCls.setTheRecord(w2c);
        WebToCase__c w2c2 = (WebToCase__c) tstCls.getTheRecord();
        system.assertEquals(w2c2.Id, w2c.Id);
        
        tstCls.rightOptions = new SelectOption[]{new SelectOption('right','right')};
        tstCls.save();
        system.assertEquals('right',(String) tstCls.getTheRecord().get('Contact_Options_Order__c'));
    }
    
    public static testMethod void test2(){
        createData();
        WebToCase_Contact_Us_Options_Order tstCls = new WebToCase_Contact_Us_Options_Order();
        tstCls.save();
        WebToCase__c w2c = [SELECT Id FROM WebToCase__c];
        w2c.Contact_Options_Order__c = '123;456;';
        update w2c;
        tstCls.setTheRecord(w2c);
        WebToCase__c w2c2 = (WebToCase__c) tstCls.getTheRecord();
    }
}