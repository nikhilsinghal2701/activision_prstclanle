public class WebToCase_FormWizard_Controller {

	public static string W2C_LANG = 'en_US';
	public static string W2C_DEFAULT = 'en_US';

	//WebTo Case Properties:
	public WebToCase__c web2CaseMain { get; set; }
	public List<WebToCase_SubType__c> subTypeList { get; set; }
	public List<SelectOption> subTypeSelections { get; set; }
	public List<string> selectedSubTypes { get; set; }

	//Form Properties:
	public WebToCase_Form__c Form { get; set; }
	public List<WebToCase_FormField__c> FormFields { get; set; }  
	public List<SelectOption> formFieldTypes { get; set; }
	public string selectedFormFieldLabel { get; set; }
	public string selectedFormFieldType { get; set; }
	public string picklistOrRichText { get; set; }
	
	//Constructor:
	public WebToCase_FormWizard_Controller(){
		
		List<WebToCase__c> w2cList = [SELECT id, W2CBuild__c, Language__c FROM WebToCase__c WHERE Language__c = :W2C_LANG LIMIT 1];
		if(w2cList.size() > 0){
			web2CaseMain = w2cList[0];
		}
		else{
			web2CaseMain = [SELECT id, W2CBuild__c, Language__c FROM WebToCase__c WHERE Language__c = :W2C_DEFAULT LIMIT 1];	
		}
		
		subTypeList = [SELECT id, Description__c, SubTypeMap__c, WebToCase_Type__c, WebToCase_Type__r.Description__c FROM WebToCase_SubType__c WHERE isActive__c = true AND Description__c = 'Click Here to report a cheater' ORDER BY Description__c DESC LIMIT 999];
		subTypeSelections = new List<SelectOption>();
		for(WebToCase_SubType__c subType : subTypeList){
			subTypeSelections.add( new SelectOption( subType.Id, subType.Description__c + '_' + subType.WebToCase_Type__r.Description__c));
		}
		
		//Init Form:
		Form = new WebToCase_Form__c();
		FormFields = new List<WebToCase_FormField__c>();
		
		//Retrieve All Field Types:
		formFieldTypes = new List<SelectOption>();
		Schema.sObjectType sObj = WebToCase_FormField__c.getSObjectType();
		Schema.DescribeSObjectResult sobjectDescribe = sObj.getDescribe();
		Map<String, Schema.SObjectField> fieldMap = sobjectDescribe.fields.getMap();
		List<Schema.PicklistEntry> picklistValues = fieldMap.get('FieldType__c').getDescribe().getPickListValues();
		for(Schema.PicklistEntry pVal : picklistValues){
			formFieldTypes.add(new SelectOption(pVal.getLabel(), pVal.getValue()));
		}
	}
	
	public PageReference updateForm(){
		
		if(selectedFormFieldLabel != null && selectedFormFieldLabel != ''){
			WebToCase_FormField__c field = new WebToCase_FormField__c();
			field.FieldType__c = selectedFormFieldType;
			field.Label__c = selectedFormFieldLabel;
			
			if(selectedFormFieldType == 'RichText'){
				field.TextAreaLong__c = picklistOrRichText;
			}
			
			if(selectedFormFieldType == 'Picklist'){
				
			}
			
			FormFields.add(field);
		}
		
		return null;
	}
	
	public void submitForm(){
		List<WebToCase_Form__c> formsToInsert = new List<WebToCase_Form__c>();
		//Insert A Form for each Selected SubType:
		for(WebToCase_SubType__c sType : [SELECT (SELECT Id, Name FROM WebToCase_Forms__r) FROM WebToCase_SubType__c WHERE id IN :selectedSubTypes ]){
			//Check for Multiple Forms:
			if(sType.WebToCase_Forms__r.size() == 0){
				WebToCase_Form__c tmpForm = new WebToCase_Form__c();
				tmpForm.WebToCase_SubType__c = sType.id;
				tmpForm.Name__c = Form.Name__c;
				formsToInsert.add(tmpForm);
			}
			else{
				//Override existing Form:
				sType.WebToCase_Forms__r[0] = new WebToCase_Form__c();
				sType.WebToCase_Forms__r[0].WebToCase_SubType__c = sType.id;
				sType.WebToCase_Forms__r[0].Name__c = Form.Name__c;
				formsToInsert.add(sType.WebToCase_Forms__r[0]);
			}
		}
		upsert formsToInsert;
		
		//Add each FormField to the inserted Fields:
		List<WebToCase_FormField__c> formFieldsToInsert = new List<WebToCase_FormField__c>();
		for(WebToCase_Form__c tmpForm : formsToInsert){
			for(WebToCase_FormField__c formField : FormFields){
				WebToCase_FormField__c tmpField = new WebToCase_FormField__c();
				tmpField.FieldType__c = formField.FieldType__c;
				tmpField.Label__c = formField.Label__c;
				tmpField.WebToCase_Form__c = tmpForm.id;
				formFieldsToInsert.add(tmpField);
			}
		}
		insert formFieldsToInsert;
	}
}