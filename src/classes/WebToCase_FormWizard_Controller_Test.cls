/////////////////////////////////////////////////////////////////////////////
// @Name             WebToCase_FormWizard_Controller_Test
// @author           Jon Albaugh
// @date             08-NOV-2042
// @description      Test For (Controller for: WebToCase_FormWizard_Controller)
/////////////////////////////////////////////////////////////////////////////

@istest
public with sharing class WebToCase_FormWizard_Controller_Test{

    public static testMethod void test(){
    
    	WebToCase__c w2c = new WebToCase__c();
    	w2c.Language__c = 'en_US';
    	w2c.chatEnabled__c = true;
    	w2c.chatEnabledMessage__c = 'tstmsg';
    	w2c.chatDisabledMessage__c = 'chatDisMsg';
    	w2c.phoneEnabled__c = true;
    	w2c.chatOfflineMessage__c = 'offlineMsg';
    	w2c.phoneDisabledMessage__c = 'pDisabledMsg';
    	w2c.phone_Hours__c = '01:00 GMT';
    	w2c.Phone_Number__c = '100';
    	w2c.Chat_Weekday_Start__c = '01:00GMT';
    	w2c.Chat_Weekday_End__c = '19:00GMT';
    	w2c.Chat_Weekend_Start__c = '01:00GMT';
    	w2c.Chat_Weekend_End__c = '19:00GMT';
    	insert w2c;
    	
    	//Insert a SubType:
    	WebToCase_Platform__c platform1 = new WebToCase_Platform__c();
    	platform1.Name__c = 'Xbox';
    	platform1.Image__c = 'xbox';
    	platform1.isActive__c = true;
    	platform1.disableChat__c = false;
    	platform1.disablePhone__c = false;
    	platform1.disableEmail__c = false;
    	platform1.WebToCase__c = w2c.id;
    	insert platform1;
    	
    	Public_Product__c pubProduct = new Public_Product__c();
    	insert pubProduct;
    	
    	WebToCase_GameTitle__c testTitle1 = new WebToCase_GameTitle__c();
    	testTitle1.Title__c = 'title';
    	testTitle1.Image__c = 'titleImg';
    	testTitle1.isActive__c = true;
    	testTitle1.disableChat__c = false;
    	testTitle1.disablePhone__c = false;
    	testTitle1.disableEmail__c = false;
    	testTitle1.isOther__c = false;
    	testTitle1.WebToCase_Platform__c = platform1.id;
    	testTitle1.Language__c = w2c.Language__c;
    	testTitle1.Public_Product__c = pubProduct.id;
    	insert testTitle1;
    	
    	WebToCase_Type__c testType1 = new WebToCase_Type__c();
    	testType1.Description__c = 'type1';
    	testType1.isActive__c = true;
    	testType1.disableChat__c = false;
    	testType1.disablePhone__c = false;
    	testType1.disableEmail__c = false;
    	testType1.typeMap__c = 'testTypeMap';
    	testType1.WebToCase_GameTitle__c = testTitle1.id;
    	insert testType1;
    	
    	WebToCase_SubType__c testSubType1 = new WebToCase_SubType__c();
    	testSubType1.Description__c = 'subType1';
    	testSubType1.isActive__c = true;
    	testSubType1.disableChat__c = false;
    	testSubType1.disablePhone__c = false;
    	testSubType1.disableEmail__c = false;
    	testSubType1.SubTypeMap__c = 'testSubTypeMap';
    	testSubType1.WebToCase_Type__c = testType1.id;
    	testSubType1.Article_ID__C = '';
    	insert testSubType1;
    
    	WebToCase_FormWizard_Controller wizardController = new WebToCase_FormWizard_Controller();
    	
    	wizardController.Form.Name__c = 'testForm';
    	wizardController.selectedFormFieldLabel = 'Abc';
    	wizardController.picklistOrRichText = 'richtext';
    	wizardController.selectedFormFieldType = 'RichText';
    	wizardController.updateForm();
    	
    	wizardController.selectedSubTypes = new List<String>();
    	wizardController.selectedSubTypes.add(testSubType1.id);
    	
    	wizardController.submitForm();
    	
    	//To test Override:
    	wizardController.submitForm();
    }
}