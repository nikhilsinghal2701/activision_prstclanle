public with sharing class WebToCase_Wizard_Controller {
	
	//WebToCase Properties:
	public WebToCase__c web2CaseMain { get; set; }
	public static string W2C_LANG = 'en_US';
	public static string W2C_DEFAULT = 'en_US';
	
	//Lists:	
	public List<Public_Product__c> publicProducts { get; set; }
	public List<WebToCase_Platform__c> platformList { get; set; }
	public List<WebToCase_GameTitle__c> titleList { get; set; }
	
	public List<WebToCase_Type__c> typeList { get; set; }
	public List<WebToCase_SubType__c> subTypeList { get; set; }
	
	//SelectLists:
	public List<SelectOption> languageSelections { get; set; }
	public List<SelectOption> platformSelections { get; set; }
	public List<SelectOption> titleSelections { get; set; }
	public List<String> selectedTitleIDs { get; set; }
	public List<SelectOption> typeMapSelections { get; set; }
	public List<String> selectedTypeIDs { get; set; }
	public List<SelectOption> subTypeSelections { get; set; }
	public List<SelectOption> subTypeMapSelections { get; set; }
	
	//New Field Properties:
	public WebToCase_Platform__c newPlatform { get; set; }
	public WebToCase_GameTitle__c newTitle { get; set; }
	public WebToCase_Type__c newType { get; set; }
	public WebToCase_SubType__c newSubType { get; set; }
	
	//Predictive Titles:
	public string predictiveTitles { get; set; }
	
	//Constructor:
	public WebToCase_Wizard_Controller(){
		
		//Get WebToCase Object:
		List<WebToCase__c> webToCaseList = [SELECT id, Language__c, W2CBuild__c FROM WebToCase__c WHERE Language__c = :W2C_LANG]; 
		
		if(webToCaseList.size() > 0){
			web2CaseMain = webToCaseList[0];
		}
		else{
			web2CaseMain = [SELECT id, Language__c, W2CBuild__c FROM WebToCase__c WHERE Language__c = :W2C_DEFAULT];
		} 
		
		//Create Predictive Title Search
		predictiveTitles = '';
		for(Public_Product__c product : [SELECT Id, Name, Platform__c FROM Public_Product__c]){
			predictiveTitles += product.Name + '---' + product.Platform__c + '##';
		}
		
		//Retrieve List Of Current Platforms:
		platformList = [SELECT id, Name__c, isActive__c, image__c, disableChat__c, disableEmail__c, disablePhone__c, WebToCase__c FROM WebToCase_Platform__c WHERE WebToCase__c = :web2CaseMain.id];
		platformSelections = new List<SelectOption>();
		for(WebToCase_Platform__c platform : platformList){
			platformSelections.add(new SelectOption(platform.Id, platform.Name__c));
		}

		//Retrieve List Of Current Titles:
		titleList = [SELECT id, Language__c, Title__c, isActive__c, isOther__c, disableChat__c, disableEmail__c, disablePhone__c, WebToCase_Platform__c, WebToCase_Platform__r.Name__c, Public_Product__c, Public_Product__r.Name FROM WebToCase_GameTitle__c WHERE Language__c = :W2C_LANG  ORDER BY Title__c];
		titleSelections = new List<SelectOption>();
		for(WebToCase_GameTitle__c gameTitle : titleList){
			titleSelections.add( new SelectOption( gameTitle.Id, gameTitle.Title__c + '_' + gameTitle.WebToCase_Platform__r.Name__c + '_' + gameTitle.Language__c ));
		}
		
		//Retrieve List Of Current Types:
		typeList = [SELECT id, Description__c, isActive__c, disableChat__c, disableEmail__c, disablePhone__c, TypeMap__c,  WebToCase_GameTitle__c, WebToCase_GameTitle__r.Title__c, WebToCase_GameTitle__r.Language__c FROM WebToCase_Type__c WHERE WebToCase_GameTitle__c IN :titleList ORDER BY CreatedDate DESC LIMIT 500];
		subTypeSelections = new List<SelectOption>();
		for(WebToCase_Type__c wType : typeList){
			subTypeSelections.add( new SelectOption ( wType.id, wType.Description__c + '_' + wType.WebToCase_GameTitle__r.Title__c ));
		}
		
		//Retrieve All Type Mapping options:
		typeMapSelections = new List<SelectOption>();
		Schema.sObjectType sobjectType = WebToCase_Type__c.getSObjectType();
		Schema.DescribeSObjectResult sobjectDescribe = sobjectType.getDescribe();
		Map<String, Schema.SObjectField> fieldMap = sobjectDescribe.fields.getMap();
		List<Schema.PicklistEntry> picklistValues = fieldMap.get('TypeMap__c').getDescribe().getPickLIstValues();
		for(Schema.PicklistEntry pVal : picklistValues){
			typeMapSelections.add(new SelectOption(pVal.getLabel(), pVal.getValue()));
		}
		
		//Retrieve List Of Current SubTypes:
		subTypeList = [SELECT id, Description__c, isActive__c, disableChat__c, disableEmail__c, disablePhone__c, SubTypeMap__c, Article_ID__c, WebToCase_Type__c, WebToCase_Type__r.Description__c FROM WebToCase_SubType__c WHERE WebToCase_Type__c IN :typeList ORDER BY CreatedDate DESC LIMIT 500];
		
		//Retrieve All SubType Mapping Options:
		subTypeMapSelections = new List<SelectOption>();
		Schema.sObjectType sObjectSubType = WebToCase_SubType__c.getSObjectType();
		Schema.DescribeSObjectResult sObjectDescribeSubType = sObjectSubType.getDescribe();
		Map<String, Schema.SObjectField> subTypeFieldMap = sObjectDescribeSubType.fields.getMap();
		List<Schema.PicklistEntry> subTypePicklistValues = subTypeFieldMap.get('SubTypeMap__c').getDescribe().getPickListValues();
		for(Schema.PicklistEntry pVal : subTypePicklistValues){
			subTypeMapSelections.add(new SelectOption(pVal.getLabel(), pVal.getValue()));
		}
		
		//Init all new variables:
		newPlatform = new WebToCase_Platform__c();
		newPlatform.isActive__c = true;
		newTitle = new WebToCase_GameTitle__c();
		newTitle.isActive__c = true;
		newType = new WebToCase_Type__c();
		newType.isActive__c = true;
		newSubType = new WebToCase_SubType__c();
		newSubType.isActive__c = true;
	} 
	
	public pageReference createNewPlatform(){
		newPlatform.WebToCase__c = web2CaseMain.id;
		insert newPlatform;
		
		//Update PlatformList:
		platformList.add(newPlatform);
		//platformSelections.add(new SelectOption(newPlatform.Id, newPlatform.Name__c));
		
		//Init new Platform:
		newPlatform = new WebToCase_Platform__c();
		return null;
	}
	
	public pageReference createNewTitle(){
		newTitle.Language__c = W2C_LANG;
		insert newTitle;
		system.debug('title inserted');
		system.debug(newTitle);
		//Update TitleList: 
		titleList = [SELECT id, Language__c, Title__c, isActive__c, isOther__c, disableChat__c, disableEmail__c, disablePhone__c, WebToCase_Platform__c, WebToCase_Platform__r.Name__c, Public_Product__c, Public_Product__r.Name FROM WebToCase_GameTitle__c WHERE Language__c = :W2C_LANG];
		system.debug('titlelist retrieved');
		//titleSelections.add( new SelectOption( newTitle.Id, newTitle.Title__c + '_' + newTitle.WebToCase_Platform__r.Name__c + '_' + newTitle.Language__c));

		//Init new GameTitle:
		newTitle = new WebToCase_GameTitle__c();
		return null;	
	}
	
	public pageReference createNewType(){
		List<WebToCase_Type__c> typesToInsert = new List<WebToCase_Type__c>();
		//Find Selected Titles:
		for(String selectedTitle : selectedTitleIDs){
			for(WebToCase_GameTitle__c eachTitle : titleList ){
				if(selectedTitle == eachTitle.id){
					//Create Type:
					WebToCase_Type__c tempType = new WebToCase_Type__c();
					tempType.Description__c = newType.Description__c;
					tempType.TypeMap__c = newType.typeMap__c;
					tempType.isActive__c = newType.isActive__c;
					tempType.disableChat__c = newType.disableChat__c;
					tempType.disableEmail__c = newType.disableEmail__c;
					tempType.disablePhone__c = newType.disablePhone__c;
					tempType.WebToCase_GameTitle__c = eachTitle.id;
					typesToInsert.add(tempType);
				}
			}	
		}
		
		if(typesToInsert.size() > 0){
			insert typesToInsert;
		}
		
		//Update TypeList:
		typeList = [SELECT id, Description__c, TypeMap__c, isActive__c, disableChat__c, disableEmail__c, disablePhone__c,  WebToCase_GameTitle__c, WebToCase_GameTitle__r.Title__c, WebToCase_GameTitle__r.Language__c FROM WebToCase_Type__c WHERE WebToCase_GameTitle__c IN :titleList ORDER BY CreatedDate DESC LIMIT 999];
		
		//Clear newType
		newType = new WebToCase_Type__c();
		
		return null;
	}
	
	public pageReference createNewSubType(){
		List<WebToCase_SubType__c> subTypesToInsert = new List<WebToCase_SubType__c>();
		//Find Selected Types:
		
		
		//Create Type Map:
		Map<String, String> typeMap = new Map<String, String>();
		for(WebToCase_Type__c eachType : typeList){
			typeMap.put(eachType.id, eachType.id);
		}
		
		
		
		for(string selectedTypeID : selectedTypeIDs){
			//for(WebToCase_Type__c eachType : typeList){
				//if(selectedType == eachType.id){
					//Create SubType:
					//tempSubType.WebToCase_Type__c = eachType.id;
					WebToCase_SubType__c tempSubType = new WebToCase_SubType__c();
					tempSubType.Description__c = newSubType.Description__c;
					tempSubType.Article_ID__c = newSubType.Article_ID__c;
					tempSubType.disableChat__c = newSubType.disableChat__c;
					tempSubType.disablePhone__c = newSubType.disablePhone__c;
					tempSubType.disableEmail__c = newSubType.disableEmail__c;
					tempSubType.isActive__c = newSubType.isActive__c;
					tempSubType.SubTypeMap__c = newSubType.SubTypeMap__c;
					tempSubType.WebToCase_Type__c = typeMap.get(selectedTypeID);
					subTypesToInsert.add(tempSubType);
				//}
			//}
		}
		
		if(subTypesToInsert.size() > 0){
			insert subTypesToInsert;
		}
		
		//Update SubType List
		subTypeList = [SELECT id, Description__c, isActive__c, disableChat__c, disableEmail__c, disablePhone__c, SubTypeMap__c, Article_ID__c, WebToCase_Type__c, WebToCase_Type__r.Description__c FROM WebToCase_SubType__c WHERE WebToCase_Type__c IN :typeList ORDER BY CreatedDate DESC LIMIT 500];
		
		//Clear newSubType:
		newSubType = new WebToCase_SubType__c();
	
		return null;
	}	
}