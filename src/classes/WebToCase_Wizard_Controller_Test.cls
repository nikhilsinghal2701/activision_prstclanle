/////////////////////////////////////////////////////////////////////////////
// @Name             WebToCase_Wizard_Controller_Test
// @author           Jon Albaugh
// @date             07-NOV-2042
// @description      Test For (Controller for: WebToCase_Wizard_Controller)
/////////////////////////////////////////////////////////////////////////////

@istest
public with sharing class WebToCase_Wizard_Controller_Test{

    public static testMethod void test(){
    	
    	WebToCase__c w2c = new WebToCase__c();
    	w2c.Language__c = 'en_US';
    	w2c.chatEnabled__c = true;
    	w2c.chatEnabledMessage__c = 'tstmsg';
    	w2c.chatDisabledMessage__c = 'chatDisMsg';
    	w2c.phoneEnabled__c = true;
    	w2c.chatOfflineMessage__c = 'offlineMsg';
    	w2c.phoneDisabledMessage__c = 'pDisabledMsg';
    	w2c.phone_Hours__c = '01:00 GMT';
    	w2c.Phone_Number__c = '100';
    	w2c.Chat_Weekday_Start__c = '01:00GMT';
    	w2c.Chat_Weekday_End__c = '19:00GMT';
    	w2c.Chat_Weekend_Start__c = '01:00GMT';
    	w2c.Chat_Weekend_End__c = '19:00GMT';
    	
    	insert w2c;
    	
    	Public_Product__c pubProduct = new Public_Product__c();
    	pubProduct.Name = 'testName';
    	pubProduct.Platform__c = 'Xbox';
        insert pubProduct;
    	
    	//Init with No Platforms:
    	WebToCase_Wizard_Controller wizardController = new WebToCase_Wizard_Controller();
    	
    	//Submit New Platform:
    	wizardController.newPlatform.Name__c = 'testPlatform';
    	wizardController.createNewPlatform();
    	
    	//Assign Platform for GameTitle:
    	WebToCase_Platform__c platform1 = [SELECT id FROM WebToCase_Platform__c LIMIT 1];
    	
    	//Page Refresh Simulation:
    	wizardController = new WebToCase_Wizard_Controller();
    	
    	//Submit New Title:
    	wizardController.newTitle.Title__c = 'testTitle';
    	wizardController.newTitle.WebToCase_Platform__c = platform1.id;
    	wizardController.newTitle.Public_Product__c = pubProduct.id;
    	wizardController.createNewTitle();
    	
    	//Assign GameTitle for Type:
    	WebToCase_GameTitle__c title1 = [SELECT id FROM WebToCase_GameTitle__c LIMIT 1];
    	
    	//Page Refresh Simulation:
    	wizardController = new WebToCase_Wizard_Controller();
    	
    	//Submit New Type:
    	wizardController.newType.Description__c = 'testType';
    	wizardController.newType.WebToCase_GameTitle__c = title1.id;
    	wizardController.selectedTitleIDs = new List<String>();
    	wizardController.selectedTitleIDs.add(title1.id);
    	wizardController.createNewType();
    	
    	//Assign Type for SubType:
    	WebToCase_Type__c type1 = [SELECT id FROM WebToCase_Type__c LIMIT 1];
    	
    	//Page Refresh Simulation:
    	wizardController = new WebToCase_Wizard_Controller();
    	
    	//Submit New SubType:
    	wizardController.newSubType.Description__c = 'testSubType';
    	wizardController.newSubType.WebToCase_Type__c = type1.id;
    	wizardController.selectedTypeIDs = new List<String>();
    	wizardController.selectedTypeIDs.add(type1.id);
    	wizardController.createNewSubType();
    	
    	//Page Refresh Simulation:
    	wizardController = new WebToCase_Wizard_Controller();
    }
}