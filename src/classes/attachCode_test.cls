@isTest
public with sharing class attachCode_test{
    @isTest static void myTest(){
        
        crr_code_type__c ct = new crr_code_type__c();
        ct.name = 'test';
        ct.description__c = 'test';
        ct.Filename__c = 'test.txt';
        ct.Activation_date__c = date.today();
        ct.First_party_picklist__c = 'Demonware';
        ct.Expiration_date__c = date.today();
        ct.Platform__c = 'Xbox 360';
        ct.Billable__c = true;
//        ct.Title__c = 'test';
        ct.First_party_reference_number__c = 'test';
//        ct.US_CA__c = true;
        insert ct;
        
        crr_code__c code = new crr_code__c();
        code.code__c = 'test';
        code.First_Party__c = 'NOA';
        //code.Code_Type__c = ct.id;
        insert code;
        
        phantom_request__c pr = new phantom_request__c();
        pr.code__c = code.id;
        insert pr;
        
        pr.verification__c = 'Denied';
        test.startTest();
        
        update pr;       
        
        test.stopTest();
    
    }

}