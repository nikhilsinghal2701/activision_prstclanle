/*******************************************************************************
 * Name         -   campMember
/**
* @author           sumankrishnasaha
* @date             Nov-2011
* @description      This controller returns details of a Gamer from 6 viewpoints:
                    Demographics, Gameplay Stats, Social Stream, Campaign Touches,
                    Customer Support Interactions, and Purchases
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* sumankrishnasaha          Nov-2011                Created
* sumankrishnasaha          26-Apr-2012             Added documentation / code cleanup
* ---------------------------------------------------------------------------------------------------
* Review Log  :
*---------------------------------------------------------------------------------------------------
* Reviewer                      Review Date            Review Comments
* --------                      ------------          --------------- 
*---------------------------------------------------------------------------------------------------
*/
public with sharing class campMember {
        Id contId = ApexPages.currentPage().getParameters().get('id');
        public String trimContId {get;set;}
        List<CampaignMember> cMList = new List<CampaignMember>();
        List<Case> caseList = new List<Case>();
        List<Asset> assetList = new List<Asset>();
        List<Asset> gameplayList = new List<Asset>();
        List<sf4twitter__Twitter_Conversation__c> tweetList = new List<sf4twitter__Twitter_Conversation__c>();
        List<sf4twitter__Twitter_Conversation__c> fbpostList = new List<sf4twitter__Twitter_Conversation__c>();
        public campMember(ApexPages.StandardController controller) {
        
        String contIdString = contId ;
        
        trimContId = contIdString .substring(0,15);
    }
    
    public List<CampaignMember> getcontCmpgns() {
        for(CampaignMember cM : [Select CampaignId, Status, HasResponded, LastModifiedDate from CampaignMember where ContactId =:contId Order by LastModifiedDate Desc limit 10])
        cMList.add(cM);
        return cMList ;
    }
    
    public List<Case> getCases() {
        for(Case c : [Select Id, CaseNumber, RecordType.Name, CreatedDate, LastModifiedDate, Status from Case where ContactId =:contId AND CreatedDate = LAST_N_DAYS:180 limit 100])
        caseList.add(c);
        return caseList;
    }
    
    public List<Asset> getAssets() {
        for(Asset a : [Select Id, Name, Product2Id, Product2.Name, Product2.Type__c, CreatedDate from Asset where ContactId =:contId Order by createdDate Desc limit 8])
        assetList.add(a);
        return assetList;
    }
    
    public List<Asset> getGameplay() {
        for(Asset b : [Select Id, Name, Product2Id, Product2.Name, Current_Rank__c, Most_Recent_Session__c, Prestige_Tenure__c, Kill_Death_Ratio__c, Total_Duration__c, Product2.Type__c, CreatedDate from Asset where ContactId =:contId AND Product2.Type__c=:'Physical' Order by createdDate Desc limit 4])
        gameplayList.add(b);
        return gameplayList;
    }
      
    public List<sf4twitter__Twitter_Conversation__c> getTweets() {
       for(sf4twitter__Twitter_Conversation__c twtr :
                [Select Id, Name, sf4twitter__Message__c, sf4twitter__Published_Date__c, sf4twitter__Image__c, sf4twitter__Status__c
                FROM sf4twitter__Twitter_Conversation__c
                WHERE sf4twitter__Contact__c =: contId and sf4twitter__Origin__c = 'Twitter' Order by sf4twitter__Published_Date__c Desc limit 3]) {
            tweetList.add(twtr);
        }
        return tweetList;
    }
    
    public List<sf4twitter__Twitter_Conversation__c> getFbPosts() {
        for(sf4twitter__Twitter_Conversation__c fbp :
                [Select Id, Name, sf4twitter__Message__c, sf4twitter__Published_Date__c, sf4twitter__Image__c, sf4twitter__Status__c
                FROM sf4twitter__Twitter_Conversation__c
                WHERE sf4twitter__Contact__c =: contId and sf4twitter__Origin__c = 'Facebook' Order by sf4twitter__Published_Date__c Desc limit 3]) {
            fbpostList.add(fbp);
        }
        return fbpostList;
    }
}