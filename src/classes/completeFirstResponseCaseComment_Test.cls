@isTest
private class completeFirstResponseCaseComment_Test {
    static Account acc = new Account();    
    static Contact con = new Contact();
    static User TestUser = new User();
    static Case csObj = new Case();
    static Profile[] profiles = [Select name, id From Profile where Name =: 'CS - Agent'];
    static Public_Product__c products = new Public_Product__c(name = 'TEST');
    static Entitlement entitlements = new Entitlement();
    static CaseComment Ccobj = new caseComment();
    
    static testMethod void myUnitTest() {
        
        acc.Name = 'TestAccount';        
        insert acc;
        
        entitlements.name ='TEST';
        entitlements.AccountId = acc.id;
        insert entitlements ;
        
        con.FirstName = 'Test';        
        con.LastName = 'contact';        
        con.AccountId = acc.id; 
        con.Gamertag__c = 'Enter Your Gamertag';
        con.Email = 'abcd@activision.com'; 
        con.MailingCountry = 'US';
        insert con;
        
        TestUser.Username ='testUser1@activision.com';
        TestUser.FirstName = 'Test';
        TestUser.LastName = 'User';
        TestUser.Email = 'Testmail@activision.com';
        TestUser.alias = 'testAl';
        TestUser.TimeZoneSidKey = 'America/New_York';
        TestUser.LocaleSidKey = 'en_US';
        TestUser.EmailEncodingKey = 'ISO-8859-1';
        TestUser.ProfileId = profiles[0].id;         
        TestUser.LanguageLocaleKey = 'en_US';
        TestUser.IsActive = true;
        insert TestUser;

        System.runAs(TestUser) {

            csObj.ContactId = con.id ;
            csObj.Type = 'Administration';
            csObj.Sub_Type__c = 'Feedback';
            csObj.Status = 'Open';
            csObj.Product_Effected__c = products.id;
            csObj.SlaStartDate = System.NOW();
            csObj.EntitlementId = entitlements.id;
            csObj.Comment_Not_Required__c = True;
            Insert csObj;
        
            ccObj.ParentId = csObj.id ;
            ccObj.IsPublished = True ;
            ccObj.CommentBody = 'TEST CLASS COMMENT' ;        
            Insert ccObj ;
        }         
    }
}