/*******************************************************************************
* Name              contSearchCont
* @author           sumankrishnasaha
* @date             Nov-2011
* @description      This controller returns a list of contact whose names start
                    with entered letter
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* sumankrishnasaha          Nov-2011                Created
* sumankrishnasaha          26-Apr-2012             Added documentation / Code Cleanup
* ---------------------------------------------------------------------------------------------------
* Review Log  :
*---------------------------------------------------------------------------------------------------
* Reviewer                      Review Date            Review Comments
* --------                      ------------          --------------- 
*---------------------------------------------------------------------------------------------------
*/
public with sharing class contSearchCont {

    public List<Contact> contList { get; set;}
    public String searchText { get; set; }

    public PageReference searchMeth() {
        contList = [ Select Id
                          , Name
                          , FirstName
                          , LastName
                          , Email
                          , Phone
                       from Contact
                      where Name like : (searchText+'%')
                   ];
        return null;
    }  
}