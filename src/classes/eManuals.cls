public without sharing class eManuals{
    //eManual variables
    public map<String, map<String, eDocument__c[]>> titleName2docList {get; set;}
    public String[] titleList {get; set;}

    //shared variables
    public map<Id, String> docId2attId {get; set;}

    //EULA variables    
    public map<String, map<String, eDocument__c[]>> titleName2eula {get; set;}
    public String[] eulaTitleList {get; set;}

    public eManuals(){
        //instantiate
        this.titleName2docList = new map<String, map<String, eDocument__c[]>>();
        this.titleList = new String[]{};

        this.titleName2eula = new map<String, map<String, eDocument__c[]>>();
        this.eulaTitleList = new String[]{};
        
        this.docId2attId = new map<Id, String>();
        
        //get data
        for(eDocument__c eDoc : [SELECT Id, Name, RecordType.Name, Platform__c, Language__c, Override_Link__c, (SELECT Id FROM Attachments ORDER BY CreatedDate DESC LIMIT 1) FROM eDocument__c ORDER BY Name]){
            map<String, eDocument__c[]> platformMap = new map<String, eDocument__c[]>();
            eDocument__c[] languageEdocs = new eDocument__c[]{};
            if(eDoc.RecordType.Name != 'EULA'){
                if(titleName2docList.containsKey(eDoc.name)){ //we have processed this game title before
                    platformMap = titleName2docList.get(eDoc.name);
                    if(platformMap.containsKey(eDoc.Platform__c)){ //we have this title & platform
                        languageEdocs = platformMap.get(eDoc.Platform__c);
                    }
                }
                languageEdocs.add(eDoc);
                platformMap.put(eDoc.Platform__c, languageEdocs);
                titleName2docList.put(eDoc.Name, platformMap);
            }else{
                if(titleName2eula.containsKey(eDoc.name)){
                    platformMap = titleName2eula.get(eDoc.name);
                    if(platformMap.containsKey(eDoc.Platform__c)){ //we have this title & platform
                        languageEdocs = platformMap.get(eDoc.Platform__c);
                    }
                }
                languageEdocs.add(eDoc);
                platformMap.put(eDoc.Platform__c, languageEdocs);         
                titleName2eula.put(eDoc.Name, platformMap);
                
            }
            docId2attId.put(eDoc.Id, '');
            if(eDoc.Attachments != null && eDoc.Attachments.size() > 0) docId2attId.put(eDoc.Id, eDoc.Attachments[0].Id);
        }

        //sort for presentation
        this.titleList.addAll(this.titleName2docList.keySet());
        this.titleList.sort();
        this.eulaTitleList.addAll(this.titleName2eula.keySet());
        this.eulaTitleList.sort();
    }
}