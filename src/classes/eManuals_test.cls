@isTest
public class eManuals_test{
    static testMethod void testEmanuals(){
        eManuals tstCls = new eManuals(); //test no data
        //prep data
        RecordType manual = [SELECT Id FROM RecordType WHERE sObjectType = 'eDocument__c' AND Name = 'eManuals'];
        RecordType eula = [SELECT Id FROM RecordType WHERE sObjectType = 'eDocument__c' AND Name = 'eula'];
        eDocument__c[] docs = new eDocument__c[]{
            new eDocument__c(
                Name = 'test1',
                Platform__c = 'Wii',
                Language__c = 'English',
                RecordTypeId = manual.Id
            ),
            new eDocument__c(
                Name = 'test2',
                Platform__c = 'PS3',
                Language__c = 'French',
                RecordTypeId = manual.Id
            ),
            new eDocument__c(
                Name = 'test1',
                Platform__c = 'Wii',
                Language__c = 'English',
                RecordTypeId = eula.Id
            ),
            new eDocument__c(
                Name = 'test2',
                Platform__c = 'PS3',
                Language__c = 'French',
                RecordTypeId = eula.Id
            ),
            new eDocument__c(
                Name = 'test1',
                Platform__c = 'Wii',
                Language__c = 'Spanish',
                RecordTypeId = manual.Id
            ),
            new eDocument__c(
                Name = 'test1',
                Platform__c = 'Wii',
                Language__c = 'Spanish',
                RecordTypeId = eula.Id
            )
        };
        insert docs;
        insert new Attachment[]{
            new Attachment(
                Name = 'test.csv',
                Body = Blob.valueOf('dummyData'),
                ParentId = docs[0].Id
            ),
            new Attachment(
                Name = 'test.csv',
                Body = Blob.valueOf('dummyData'),
                ParentId = docs[2].Id
            )
        };
        tstCls = new eManuals();
        
    }
}