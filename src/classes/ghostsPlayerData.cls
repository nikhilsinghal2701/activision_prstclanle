public class ghostsPlayerData{
    public String ConnectionType {get; set;}
    public String Gamertag {get; set;}
    public Integer GamesPlayed {get; set;}
    public Long LastLoginTime;
    public Boolean CorruptionDetected {get; set;}
    public Boolean IsStatsBlobMissing {get; set;}
    public Integer Score {get; set;}

    public void setLastLoginTime(Long l){
        LastLoginTime = l;
    }
    public String getLastLoginTime(){
        return DateTime.newInstance(LastLoginTime*1000).format('MM/dd/yy h:mm a');
    }
    public Integer Prestige {get; set;}
    public Integer Rank {get; set;}
    public Integer RevertCount {get; set;}
    public String TimePlayed {get; set;}
    public ban[] Bans {get; set;}
    public InventoryItem[] InventoryItems{get;set;}
    public SquadMember[] SquadMembers {get; set;}
    public integer SquadPoints {get; set;}
    public Boolean ScrubbedFromLeaderboards {get; set;}
    public Boolean SuspectedBooster {get; set;}
    public Boolean SuspectedHacker {get; set;}
    public Integer ResetCount {get; set;}
    public Boolean IsValidGamertag {get; set;}
    public Boolean ViolatesRules {get; set;}
    public string LastCaseComment {get; set;}
    public long LastCaseTimestamp ;
    public string getLastCaseTimestamp() { 
        if(LastCaseTimestamp  ==null)LastCaseTimestamp  =0;
        return DateTime.newInstance((long)LastCaseTimestamp  * 1000).format('MM/dd/yyyy h:mm a');
    }
    public void setLastCaseTimestamp(Long l){
        LastCaseTimestamp= l;
    }
    public class ban{
        public string Category{get;set;}
        public integer EndDate;
        public integer StartDate;
        public string  Notes{get;set;}
        public Integer BanCode{get;set;}
        
        public string getEndDate(){
            return DateTime.newInstance((Long) EndDate * 1000).format('MM/dd/yyyy h:mm a');
            //return DateTime.newInstance(EndDate*1000).format('MM/dd/yyyy h:mm a'); //integer * integer = integer
        }
        public string getStartDate(){
            return DateTime.newInstance((Long) StartDate*1000).format('MM/dd/yy h:mm a');
            //return DateTime.newInstance(StartDate*1000).format('MM/dd/yy h:mm a'); //1386177720 * 1000 = outside integer range
        }
    }
    
    public class InventoryItem{
        public string EnglishName{get;set;}
        public boolean HasStats{get;set;}
        public boolean IsExpired{get;set;}
        public string ItemGroup{get;set;}
        public string ItemSource{get;set;}
        public string Rarity{get;set;}
        public WeaponStat WeaponStats{get;set;}  
    }
    
    public class WeaponStat{

        public integer Accuracy{get;set;}
        public integer AccuracyDelta{get;set;}
        public integer Ammo{get;set;}
        public integer AmmoDelta{get;set;}
        public integer Damage{get;set;}
        public integer DamageDelta{get;set;}
        public integer FireRate{get;set;}
        public integer FireRateDelta{get;set;}
        public integer Handling{get;set;}
        public integer HandlingDelta{get;set;}
        public integer Magazine{get;set;}
        public integer MagazineDelta{get;set;}
        public integer Mobility{get;set;}
        public integer MobilityDelta{get;set;}
        public integer Range{get;set;}
        public integer RangeDelta{get;set;}
    }
    public class SquadMember{
        public String name{get;set;}
        public String rank{get;set;}
    }
    
}