@IsTest
public class ghostsPlayerData_test{
    public static testMethod void doTest(){
        String testData = '{"Bans":[],"ConnectionType":"wired","Gamertag":"sychophant","GamesPlayed":109,"LastLoginTime":1390454940,';
        testData += '"Prestige":0,"Rank":16,"SquadMembers":[{"Name":"LeFeuvre ber 1","Rank":16},{"Name":"Barnett mber 2","Rank":1},';
        testData += '{"Name":"Carani ember 3","Rank":5},{"Name":"Sanchez mber 5","Rank":1}],"SquadPoints":28,"TimePlayed":"0d 4h 9m ';
        testData += '45s"}';
        ghostsPlayerData tstCls = (ghostsPlayerData) JSON.deserialize(testData, ghostsPlayerData.class); 
        system.assertEquals(tstCls.ConnectionType, 'wired');
        system.assertEquals(tstCls.GamerTag, 'sychophant');
        system.assertEquals(tstCls.GamesPlayed, 109);
        system.assertEquals(tstCls.getLastLoginTime(), '01/22/14 9:29 PM');
        system.assertEquals(tstCls.Prestige, 0);
        system.assertEquals(tstCls.Rank, 16);
        system.assertEquals(tstCls.TimePlayed, '0d 4h 9m 45s');
        system.assertEquals(tstCls.Bans, new ghostsPlayerData.ban[]{});
        system.assertEquals(tstCls.SquadPoints, 28);
        system.assertEquals(tstCls.SquadMembers.size(), 4);
        tstCls.setLastLoginTime(1390454940L);
        ghostsPlayerData.ban tstBans = new ghostsPlayerData.ban();
        ghostsPlayerData.squadMember tstSQM = new ghostsPlayerData.squadMember();
        try{
            tstBans.getEndDate();
        }catch(Exception e){
            system.assertEquals(e.getMessage(), 'Attempt to de-reference a null object');
        }
        try{
            tstBans.getStartDate();
        }catch(Exception e){
            system.assertEquals(e.getMessage(), 'Attempt to de-reference a null object');
        }
    }
}