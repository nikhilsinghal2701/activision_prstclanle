public class history {
   
      public datetime historydt { get; private set; }
      public boolean ispublic   { get; private set; }
      public string actorname   { get; private set; }
      public string historyType { get; private set; }
      public string to          { get; private set; }
      public string fr          { get; private set; }
      public id newid           { get; private set; }
       
      /* Class constructor */
      public History(Datetime d, boolean p, String actor, String ht, String f, String t, id Nid) {
          historydt   = d;
          historydate = d.format();
          ispublic    = p;
          actorname   = actor;
          historyType = ht;
          fr          = f;
          to          = t;
          newid       = Nid;
      }
       
      public string historydate { get; set; }
      public string dtmonthyr   { get { return historydt.format('MMMMM yyyy'); } }
      public string dttime      { get { return historydt.format('h:mm a');} }
      public string dtdayfmt    { get { return historydt.format('d - EEEE'); } }
      public integer dtmonth    { get { return historydt.month();} }
      public integer dtyear     { get { return historydt.year();} }
      public integer dtday      { get { return historydt.day();} }
       
  }