public class integration_credential_oauth_v2{
    sObject pObj;
    public string conKey{get;set;}
    public string conSec{get;set;}
    public string accTok{get;set;}
    public string accTokSec{get;set;}
    public string sigBaseString{get;set;}
    public string response{get;set;}
    public Integration_Credential__c ic{get;set;}
    
    map<String, String> heads{get;set;}
    
    public string oAuth_nonce = EncodingUtil.base64Encode(blob.valueOf(string.valueOf(Crypto.getRandomInteger()+system.now().getTime())+string.valueOf(Crypto.getRandomInteger()))).replaceAll('[^a-z^A-Z^0-9]','');
    
    public integration_credential_oauth_v2(ApexPages.standardController cont){
        integration_credential__c ic = [select id, API_Key__c, API_Secret__c from integration_credential__c where twitter_enabled__c=true limit 1];
        conKey = ic.API_Key__c;
        conSec = ic.API_Secret__c;
        heads = new map<String, String>{
        'oauth_version'=>'1.0', //yes
            'oauth_nonce'=>oAuth_nonce, //yes
            'oauth_consumer_key'=>conKey, //yes
            'oauth_signature_method'=>'HMAC-SHA1', //yes
            'oauth_timestamp'=>string.valueOf(system.now().getTime()/1000) //yes
            //,'oauth_callback'=>percentEncode('https://fgw-developer-edition--c.na10.visual.force.com/apex/tweeter')
            };
                if(!test.isRunningTest()){        
        cont.addFields(new string[]{'oAuth_Endpoint__c'});
                }
        pObj = cont.getRecord();
    }
    public integration_credential_oauth_v2(){
        integration_credential__c ic = [select id, API_Key__c, API_Secret__c from integration_credential__c where twitter_enabled__c=true limit 1];
        conKey = ic.API_Key__c;
        conSec = ic.API_Secret__c;
        heads = new map<String, String>{
        'oauth_version'=>'1.0', //yes
            'oauth_nonce'=>oAuth_nonce, //yes
            'oauth_consumer_key'=>conKey, //yes
            'oauth_signature_method'=>'HMAC-SHA1', //yes
            'oauth_timestamp'=>string.valueOf(system.now().getTime()/1000) //yes
            //,'oauth_callback'=>percentEncode('https://fgw-developer-edition--c.na10.visual.force.com/apex/tweeter')
            };
    }
    public pageReference startOauth(){
        try{
            return new pageReference((String) pObj.get('oAuth_Endpoint__c')+'?oauth_token='+ic.oAuth_Token__c);
        }catch(Exception e){
            system.debug(e);
            try{
                ApexPages.addMessages(e);
            }catch(Exception ex){
                system.debug(ex);
            }
        }
        return null;
    }
           
                public pagereference getoAuthToken(){
                    
                    httpRequest newReq = new httpRequest();
                    newReq.setMethod('POST');
                    newReq.setEndpoint('https://api.twitter.com/oauth/request_token');
                    
                    map<String, String> encodedHeadMap = new map<String, String>();
                    for(String key : heads.keySet()){
                        encodedHeadMap.put(percentEncode(key), percentEncode(heads.get(key)));
                    }
                    
                    string[] paramHeads = new string[]{};
                        paramHeads.addAll(heads.keySet());
                    paramHeads.sort();
                    string params = '';
                    for(String encodedKey : paramHeads){
                        params+=encodedKey+'%3D'+heads.get(encodedKey)+'%26';
                    }
                    params = params.substring(0,(params.length() - 3));
                    
                    sigBaseString = newReq.getMethod().toUpperCase()+'&'+percentEncode(newReq.getEndpoint())+'&'+params;
                    
                    string sigKey = percentEncode(conSec)+'&';
                    blob mac = crypto.generateMac('hmacSHA1', blob.valueOf(sigBaseString), blob.valueOf(sigKey));
                    string oauth_signature = EncodingUtil.base64Encode(mac);
                    heads.put(percentEncode('oauth_signature'), percentEncode(oauth_signature));
                    
                    paramHeads.clear();
                    paramHeads.addAll(heads.keySet());
                    paramHeads.sort();
                    string oAuth_Body = 'OAuth ';
                    for(String key : paramHeads){
                        oAuth_Body += key+'="'+heads.get(key)+'", ';
                    }
                    oAuth_Body = oAuth_Body.subString(0, (oAuth_Body.length() - 2));
                    newReq.setHeader('Authorization', oAuth_Body);
                    
                    if(!test.isRunningTest()){
                        httpResponse httpRes = new http().send(newReq);
                        response = httpRes.getBody();
                        
                        ic = (Integration_Credential__c)pObj;
                    
                    //ic.access_Token__c = response;
                    ic.oAuth_Token__c = response.substring(response.indexof('oauth_token=')+'oauth_token='.length(),response.indexof('&oauth_token_secret='));
                    ic.oAuth_Secret__c = response.substring(response.indexof('&oauth_token_secret=')+'&oauth_token_secret='.length(),response.indexof('&oauth_callback_confirmed'));
                    update ic;
                        return new pageReference('https://api.twitter.com/oauth/authenticate?oauth_token='+ic.oAuth_Token__c);
                    }
                    else{
                        response = '345345oauth_token=123&oauth_token_secret=123&oauth_callback_confirmed=1';
                        return null;
                    }
                    
                    
                    
                    
                }
    public string percentEncode(String toEncode){
        set<String> allowedChars = new set<String>{
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '.', '_', '~',
                'A', 'B', 'C', 'D', 'E', 'F','G', 'H', 'I', 'J', 'K', 'L','M', 'N', 'O', 'P', 'Q', 'R','S', 'T', 'U', 'V', 'W', 'X','Y', 'Z',
                'a', 'b', 'c', 'd', 'e', 'f','g', 'h', 'i', 'j', 'k', 'l','m', 'n', 'o', 'p', 'q', 'r','s', 't', 'u', 'v', 'w', 'x','y', 'z'
                };
                    string encoded = '';
        if(toEncode != null){
            for(Integer i = 0; i < toEncode.length(); i++){
                String toEncodeChar = toEncode.subString(i, i+1);
                if(allowedChars.contains(toEncodeChar)){
                    encoded += toEncodeChar;
                }else{
                    String hexed = encodingUtil.convertToHex(blob.valueOf(toEncodeChar));
                    for(integer x = 0; x != hexed.length(); x+=2){
                        encoded += '%'+hexed.subString(x, x+2).toUpperCase();
                    }
                }
            }
        }
        return encoded;
    }
}