public class reCAPTCHA_controller{

    public reCAPTCHA_controller(ATVICustomerPortalController controller) {}

    /* Configuration */
    static final boolean isProd = UserInfo.getOrganizationId() == '00DU0000000HMgwMAG';

    // The API endpoint for the reCAPTCHA service
    private static String baseUrl = 'https://www.google.com/recaptcha/api/verify'; 

    // The keys you get by signing up for reCAPTCHA for your domain
    private static String privateKey = isProd ? '6Lc5CegSAAAAAElkhHkjwsOIge2HRFaMw379Cldz' : '6LevMfYSAAAAABpCRBvcz4nabAdDgtE31DjSwF4s';
    public String publicKey { 
        get { return isProd ? '6Lc5CegSAAAAAMaaHIMHdcAwMFiT5ANTgDjeKmfA' : '6LevMfYSAAAAAGoxKfJl6xtCCWKoZX5pHjr9qFIt'; }
    } 

    /* Implementation */
    
    // Create properties for the non-VF component input fields generated
    // by the reCAPTCHA JavaScript.
    public String challenge { 
        get {
            return ApexPages.currentPage().getParameters().get('recaptcha_challenge_field');
        }
    }
    public String response  { 
        get {
            return ApexPages.currentPage().getParameters().get('recaptcha_response_field');
        }
    }
    
    // Whether the submission has passed reCAPTCHA validation or not
    public Boolean verified { get; private set; }
    
    public reCAPTCHA_controller() {
        this.verified = false;
    }
    public reCAPTCHA_controller(ApexPages.standardController cont){
        this.verified = false;
    }
    
    public String pPageToGoToAfterVerify {get; set;}
    public String pSobjectTypeToCreateAfterVerify {get; set;}
    public boolean pGenerateNonce {get; set;}
    
    public PageReference verify() {
        System.debug('reCAPTCHA verification attempt');
        // On first page load, form is empty, so no request to make yet
        if ( challenge == null || response == null ) { 
            System.debug('reCAPTCHA verification attempt with empty form');
            return null; 
        }
                    
        HttpResponse r = makeRequest(baseUrl,
            'privatekey=' + privateKey + 
            '&remoteip='  + remoteHost + 
            '&challenge=' + challenge +
            '&response='  + response
        );
        
        if ( r!= null ) {
            this.verified = (r.getBody().startsWithIgnoreCase('true'));
        }
        
        if(this.verified) {
            return verificationSuccess();
        }
        return null;
    }
    
    public pageReference verificationSuccess(){
        PageReference pr;
        if(pPageToGoToAfterVerify != null){
            pr = new PageReference('/'+pPageToGoToAfterVerify);
            pr.setRedirect(true);
            if(pSobjectTypeToCreateAfterVerify != null && pSobjectTypeToCreateAfterVerify != ''){
                try{
                    Schema.SObjectType targetType = Schema.getGlobalDescribe().get(pSobjectTypeToCreateAfterVerify);
                    if(targetType != null){
                        sObject rec = targetType.newSobject();
                        insert rec;
                        pr.getParameters().put('id',(String) rec.get('id'));
                    }
                }catch(Exception e){
                    ApexPages.addMessages(e);
                    system.debug(e);
                    return null;
                }
            }
            if(pGenerateNonce == true){
                pr.getParameters().put('n',generateSpamPreventionNonce());
            }
        }
        return pr;
    }
    public string generateSpamPreventionNonce(){
        //if we don't need to create a record, but still want to use captcha this method
        //generates a "random" "unique" verifiable string that can be passed to the next page
        //the next page can then decrypts it and validate as necessary.  
        //current Timestamp & current user id are being passed
        String rawNonce = UserInfo.getuserId()+':::'+string.valueOf(system.now().getTime());
        Blob encryptedNonce = Crypto.encryptWithmanagedIV('AES256',blob.valueOf('asdfqwerzxcvpoiu;lkj.,mn12345678'), blob.valueOf(rawNonce));
        String b64nonce = EncodingUtil.base64encode(encryptedNonce);
        String urlSafeNonce = EncodingUtil.urlEncode(b64nonce,'UTF-8');
        system.debug(urlSafeNonce);
        return urlSafeNonce;
    }
    //localization
    public String getLanguage(){
        String lang = 'en_US', langFromUrl = '';
        Boolean langFound = false;
        if(ApexPages.currentPage() != null){ //get the value
            map<String, String> params = ApexPages.currentPage().getParameters();
            for(String key : params.keySet()){
                if( key.equalsIgnoreCase('uil') ||
                    key.equalsIgnoreCase('lang') ||
                    key.equalsIgnoreCase('ulang') 
                ){
                    langFromUrl = String.escapeSingleQuotes(params.get(key));
                    langFound = true;
                    break;
                }
            }
        }
        if(langFound == false){
            Cookie tempCookie = ApexPages.currentPage().getCookies().get('uLang');
            if(tempCookie != null){
                langFromUrl = tempCookie.getValue();            
            }else{
                tempCookie = ApexPages.currentPage().getCookies().get('apex__uLang');
                if(tempCookie != null){
                    langFromUrl = tempCookie.getValue();
                }
            }
            if(langFromUrl == null) lang = 'en_US';
        }
        if(langFromUrl.contains('nl')){lang = 'nl_NL';}
        else if(langFromUrl.contains('fr')){lang = 'fr_FR';}
        else if(langFromUrl.contains('de')){lang = 'de_DE';}
        else if(langFromUrl.contains('es')){lang = 'es_ES';}
        else if(langFromUrl.contains('it')){lang = 'it_IT';}
        return lang;
    }

    public PageReference reset() {
        return null; 
    }
    
    /* Private helper methods */
    
    private static HttpResponse makeRequest(string url, string body)  {
        HttpResponse response = null;
        HttpRequest req = new HttpRequest();   
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody (body);
        try {
            Http http = new Http();
            response = http.send(req);
            System.debug('reCAPTCHA response: ' + response);
            System.debug('reCAPTCHA body: ' + response.getBody());
        } catch(System.Exception e) {
            System.debug('ERROR: ' + e);
        }
        return response;
    }   
        
    private String remoteHost { 
        get { 
            String ret = '127.0.0.1';
            // also could use x-original-remote-host 
            Map<String, String> hdrs = ApexPages.currentPage().getHeaders();
            if (hdrs.get('x-original-remote-addr')!= null)
                ret =  hdrs.get('x-original-remote-addr');
            else if (hdrs.get('X-Salesforce-SIP')!= null)
                ret =  hdrs.get('X-Salesforce-SIP');
            return ret;
        }
    }
}