@isTest
public class reCAPTCHA_controller_test{
    public static testMethod void test1(){
        reCAPTCHA_controller tstCls = new reCAPTCHA_controller(
            new ATVICustomerPortalController()
        );
        tstCls = new reCAPTCHA_controller();
        String tstStr = tstCls.publicKey;
        tstStr = tstCls.challenge;
        tstStr = tstCls.response;
        Boolean verified = tstCls.verified;
        
        tstCls = new reCAPTCHA_controller(
            new ApexPages.standardController(
                new Account()
            )
        );
        
        pageReference pr;
        tstStr = tstCls.pPageToGoToAfterVerify;
        tstStr = tstCls.pSobjectTypeToCreateAfterVerify; 
        pr = tstCls.verificationSuccess();
        test.setCurrentPage(page.DestinyPreOrderSupport);
        
        for(String param : new string[]{'lang','ulang','uil'}){
            ApexPages.currentPage().getParameters().put(param,'fr');
            tstCls.getLanguage();
        }
        for(String lan : new string[]{'nl','fr','de','es','it'}){
            ApexPages.currentPage().getParameters().put('uil',lan);
            tstCls.getLanguage();
        }
        tstCls.reset();
        tstCls.pPageToGoToAfterVerify = 'asdf';
        tstCls.pSobjectTypeToCreateAfterVerify = 'Contact';
        tstCls.verificationSuccess(); //trip try catch because we're missing the required field LastName
        tstCls.pSobjectTypeToCreateAfterVerify = 'Destiny_Support_Issue__c';
        tstCls.verificationSuccess(); //succeed
        
        Test.setMock(WebServiceMock.class, new reCAPTCHA_controller_test_mockCallout());
        test.startTest();
            pr = tstCls.verify();
            ApexPages.currentPage().getParameters().put('recaptcha_response_field', 'resp');
            ApexPages.currentPage().getParameters().put('recaptcha_challenge_field', 'chal');
            pr = tstCls.verify();
        test.stopTest();
    }
}