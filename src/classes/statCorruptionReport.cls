public with sharing class statCorruptionReport{
/*
*
* See SD#00062299 and/or CS#02467845 for verbose details
*
*/
    /*public variables*/
    public Stat_Corruption_Report__c[] previousReports {get; private set;}
    public Stat_Corruption_Report__c newReport {get; set;}
    public boolean mpaRefreshComplete {get; set;}
    public Integer errorCount {get; set;}
    public string errorMessage {get; set;}
    public User gamerUser {get; set;}
    
    /*private variables, private is default, no need to redeclare*/
    static final integer MAX_ERRORS = 2;
    map<Id, Multiplayer_Account__c> mpaId2mpa = new map<Id, Multiplayer_Account__c>();
    boolean linkedAccountsPresent = false;
    
    /*Constructor*/
    public statCorruptionReport(){
        this.errorCount = 0;
        this.errorMessage = '';
        this.mpaRefreshComplete= false;
        this.newReport = new Stat_Corruption_Report__c();
        this.gamerUser = [SELECT ContactId, Contact.UCDID__c, FederationIdentifier FROM User WHERE Id = :UserInfo.getUserId()];
        if(this.gamerUser.contactId != null){
            this.previousReports = [SELECT Id, Name, Status__c FROM Stat_Corruption_Report__c WHERE Multiplayer_Account__r.Contact__c = :this.gamerUser.ContactId ORDER BY Name DESC];
        }
    }
    
    /*page init method*/
    public pageReference pageInitAction(){
        //after the page finishes loading, begin refresh the linked account data
        if(this.gamerUser.ContactId != null){
            if((this.gamerUser.Contact.UCDID__c == null || this.gamerUser.Contact.UCDID__c == '') && 
               (this.gamerUser.FederationIdentifier != null && this.gamerUser.FederationIdentifier != ''))
            {
                try{
                update new Contact(Id = this.gamerUser.ContactId, UCDID__c = this.gamerUser.FederationIdentifier);
                }catch(Exception e){
                //will fail if contact.birthdate is empty, contact.birthdate is empty when TIBCO hasn't updated yet
                }
            }
            Linked_Accounts_Redirect_Controller larc = new Linked_Accounts_Redirect_Controller
            (
                new ApexPages.standardController(new Contact(Id = this.gamerUser.ContactId))
            );
            larc.updateProfile();
        }
        this.mpaRefreshComplete = true;
        return null;
    }
    
    /*build the multiplayer picklist*/
    public selectOption[] getMultiPlayerAccounts(){
        selectOption[] results = new selectOption[]{};
        if(this.gamerUser.ContactId != null){
            this.mpaId2mpa = new map<Id, Multiplayer_Account__c>([SELECT Id, Platform__c, Gamertag__c FROM Multiplayer_Account__c WHERE Contact__c = :this.gamerUser.ContactId]);
            for(Id mpaId : this.mpaId2mpa.keySet()){
                results.add(new selectOption(mpaId, this.mpaId2mpa.get(mpaId).Gamertag__c+' ('+this.mpaId2mpa.get(mpaId).Platform__c+')'));
                this.linkedAccountsPresent = true;
            }
        }
        return results;
    }
    
    /*get primary issue types from object, iterating over schema */
    public selectOption[] getPrimaryIssueType(){
        selectOption[] results = new selectOption[]{};
        for(Schema.PicklistEntry ple : Stat_Corruption_Report__c.Issue__c.getDescribe().getPicklistValues()){
            results.add(new selectOption(ple.getValue(), ple.getLabel()));
        }
        return results;
    }
    
    /*exit method from the page, either success or failure*/
    public pageReference exit(){
        return new pageReference('/');
    }
    
    /*clear the error message to hide the pop-over*/
    public pageReference acknowledgeError(){
        this.errorMessage = '';
        return null;
    }
    
    /*the work method*/
    public pageReference submit(){
        try{
            
            //validation 1, do they have accounts linked?
            if(this.linkedAccountsPresent == false){
                this.errorMessage = 'No associated gamertags, please link your accounts to submit a report';
                return null;
            }
            
            //validation 2, does this gamertag have an open request?
            String platform = this.mpaId2mpa.get(this.newReport.Multiplayer_Account__c).Platform__c;
            if([SELECT count() FROM Stat_Corruption_Report__c WHERE Multiplayer_Account__c = :this.newReport.Multiplayer_Account__c AND Status__c IN ('OPEN','REVIEWED_ESCALATED')] > 0){
                this.errorMessage = this.mpaId2mpa.get(this.newReport.Multiplayer_Account__c).Gamertag__c+' ('+platform+') is already being researched, we cannot start another investigation until the current one is finished.';
                return null;
            }
            
            //data prep 1, calc platform enum required by LAS
            if(platform == 'PSN'){
                platform = 'PLAYSTATION_3';
            }else{
                platform = 'XBOX_360';
            }
            
            //data prep 2, calc booleans required by LAS
            Boolean mpReset = false, mpRollback = false, zmReset = false, zmRollback = false;
            String issue = this.newReport.Issue__c == null ? '' : this.newReport.Issue__c;
            if(issue.containsIgnoreCase('multi')){
                if(issue.containsIgnoreCase('roll')) mpRollback = true;
                else mpReset = true;
            }else{
                if(issue.containsIgnoreCase('roll')) zmRollback = true;
                else zmReset = true;
            }
            
            //send information to LAS
            LASService.BasicHttpBinding_ILinkedAccounts service = new LASService.BasicHttpBinding_ILinkedAccounts();
            
            Integer lasGUID = 0;
            if(test.isRunningTest()){
                if(this.gamerUser.FederationIdentifier == '3'){
                    lasGUID = 3;
                }
            }else{
                lasGUID = service.CreateStatRequest(
                    platform, 
                    this.mpaId2mpa.get(this.newReport.Multiplayer_Account__c).Gamertag__c,
                    mpReset, 
                    mpRollback, 
                    zmReset, 
                    zmRollback, 
                    this.newReport.General_Multiplayer_Stats__c, 
                    this.newReport.General_Zombie_Stats__c, 
                    this.newReport.Weapon_Stats__c, 
                    this.newReport.Weapon_Unlocks__c, 
                    this.newReport.Perks__c, 
                    this.newReport.Scorestreaks__c, 
                    this.newReport.Player_Card_Backings__c, 
                    this.newReport.Emblems__c, 
                    this.newReport.Unlock_Tokens__c, 
                    this.newReport.Prestige_Awards__c, 
                    this.newReport.Other_please_specify__c, 
                    this.newReport.Other_details__c,
                    this.newReport.Played_offline_match_recently__c, 
                    this.newReport.Received_Corrupt_Stats_Message__c, 
                    this.newReport.Current_Previous_ban__c, 
                    this.newReport.Comments__c
                 );
             }

            if(lasGUID > 0){
                this.newReport.LAS_GUID__c = lasGUID;
                this.newReport.Status__c = 'OPEN';
                insert this.newReport;
            }else{//Error with LAS, tell user to try again, unless they've already tried MAX_ERRORS times
                this.errorCount++;
                if(this.errorCount < MAX_ERRORS){
                    this.errorMessage = 'There has been an error, please try again.';                    
                }else{
                    //they've already tried MAX_ERRORS times, dont tell them to do it again
                    insert this.newReport;
                    this.newReport = [SELECT Id, Name FROM Stat_Corruption_Report__c WHERE Id = :this.newReport.Id];
                    this.newReport.LAS_GUID__c = integer.valueOf(this.newReport.Name) * -1;
                    update this.newReport;
                    this.errorMessage = 'We are currently unable to submit your request; the incident has been logged for investigation.';
                }
            }
        }catch(Exception e){
            system.debug('error == '+e);
            //ApexPages.addMessages(e);
            //this.errorMessage = e.getMessage();
        }
        return null;
    }
}