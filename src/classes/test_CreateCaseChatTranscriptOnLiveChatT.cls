@isTest
public class test_CreateCaseChatTranscriptOnLiveChatT {
	public static testMethod void testTrigger() {
		
	CheckAddError.addError = false;
	Account testAcc=UnitTestHelper.generateTestAccount();
    insert testAcc;
    
    Contact testContact=UnitTestHelper.generateTestContact();
    testContact.AccountId=testAcc.Id;
    insert testContact;
    
    Case c=UnitTestHelper.generateTestCase();
    c.AccountId=testAcc.Id;
    c.ContactId=testContact.Id;
    c.Type='RMA';
    c.Status='Open';
    insert c;
		
		LiveChatVisitor lcv = new LiveChatVisitor();
		insert lcv;
		
		LiveChatTranscript lct = new LiveChatTranscript();
		lct.Body='TEST';
		lct.LiveChatVisitorId = lcv.id;
		insert lct;
		
		lct.Body = 'teste';
		update lct;
		
	}
}