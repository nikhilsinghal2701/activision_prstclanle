@isTest
public class test_NewsController {


    static testmethod void mytest(){
        Account testaccount=new Account();
        testaccount.Name='Test account';
        testaccount.Type__c='Agency';
        insert testaccount;
        
        Contact testContact=new Contact();
        testContact.LastName='Test User';
        testContact.Languages__c='English';
        testContact.MailingCountry='US';
        testContact.AccountId=testaccount.id;
        insert testContact;
        
        User userObject = UnitTestHelper.getCustomerPortalUser('foo',testContact.id);
        insert userObject;
        
        System.runAs(userObject) {
            test.startTest();
            NewsController nws = new NewsController();
            nws.getPage(1);
            nws.nextPage();
            nws.next2Page();
            nws.next3Page();
            nws.next4Page();
            nws.prevPage();
            nws.prev2Page();
            nws.prev3Page();
            nws.prev4Page();
            test.stopTest();
        }
    }
    
    

}