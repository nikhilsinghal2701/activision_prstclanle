@isTest
public class test_skykidscontroller {
    static testmethod void mytest(){
        Account testaccount=new Account();
        testaccount.Name='Test account';
        testaccount.Type__c='Agency';
        insert testaccount;
        
        Contact testContact=new Contact();
        testContact.LastName='Test User';
        testContact.Languages__c='English';
        testContact.MailingCountry='US';
        testContact.AccountId=testaccount.id;
        insert testContact;
        
        User userObject = UnitTestHelper.getCustomerPortalUser('foo',testContact.id);
        insert userObject;
        
        cookie cook = new cookie('uLang',null,null,-1,false);
        ApexPages.currentPage().setCookies(New Cookie[]{cook});
        
        System.runAs(userObject) {
        test.startTest();
            Skykidscontroller con = new Skykidscontroller();
            
        test.stopTest();
        }
    }
    
    
}