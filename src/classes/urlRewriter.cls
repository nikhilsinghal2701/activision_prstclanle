// Jonathan Hersh - jhersh@salesforce.com - 8/5/2010
global with sharing class urlRewriter implements Site.UrlRewriter {
    global PageReference mapRequestUrl(PageReference myFriendlyUrl){
        try{
            String url = myFriendlyUrl.getUrl();
            
            if( url == null || url == '' || url == '/' )
                return new pageReference( urlHomeController.DEFAULT_URL );  
                            
            if( url.startsWith('/') )
                url = url.substring(1, url.length());
            
            if( url.length() > 100 )
                url = url.substring( 0, 100 );
            
            if( url.contains('?') )
                url = url.split('\\?')[0];
                
           // Does this URL exist in the database?
           Short_URL__c[] links = [select id from Short_URL__c where short_url__c = :url limit 1];
           
           if( links.isEmpty() )
            return myFriendlyURL;
                
           PageReference p = Page.urlRedirect;
           p.setRedirect( true );
           p.getparameters().put('url',url);
           return p;
       }catch(Exception e){
           system.debug('ERROR == '+e);
           return myFriendlyURL;
       }
    }
    
    global List<PageReference> generateUrlFor(List<PageReference> 
            mySalesforceUrls){    
        return null;
    }
}