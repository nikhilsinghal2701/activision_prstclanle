@isTest
public class urlRewriter_test{
    public static testmethod void testRewriter() {
        
        PageReference notfound = new PageReference( urlHomeController.DEFAULT_URL );
    	PageReference redirect = new PageReference('http://www.salesforce.com');
        
        string url = '/shortURL';
        urlRewriter ur = new urlRewriter();
        PageReference p = ur.mapRequestUrl( new pageReference( url ) );
        system.assertEquals( true, p.getRedirect() );
        system.assertEquals( url, p.getUrl() );
        system.assertEquals( null, ur.generateUrlFor( new PageReference[] {} ) );
        
        linkforce_settings__c ls = new linkforce_settings__c();
        ls.base_url__c = 'http://csuat-activisionsupport.cs9.force.com/lf/';
        ls.Default_URL__c = 'http://rewards.activision.com';
        insert ls;
        
        Short_URL__c shurl = new Short_URL__c();
    	shurl.url__c = redirect.getURL();
    	shurl.Custom_URL__c = 'test';
        shurl.Long_URL__c = 'activision.allegiancetech.com/cgi-bin/qwebcorporate.dll?idx=DHSMQQ';
        insert shurl;
        
        short_url__c check = [select short_url__c from short_URL__c where id =: shurl.id];
        system.debug(check.Short_URL__c);
        
        test.startTest();
        
        urlRewriter ur1 = new urlRewriter();
        PageReference p1 = ur1.mapRequestUrl( new pageReference( 'test' ) );
        
        PageReference p2 = ur1.mapRequestUrl( new pageReference('1234567890123456789012345678901234567?8901234567890123456789012345678901234567890123456789012345678901234567890'));
        
        
        test.stopTest();
    }
}