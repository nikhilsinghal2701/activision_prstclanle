/*
    NAME: 			utils
	CALLED FROM:	Triggers.
	DESCRIPTION: 	All the variables are static, 
					hence are used to hold values between 
					several callouts in a single invocation.
	TEST CLASS: 	utils.
	NOTE: 			None.
*/
public without sharing class utils {
	
	/*
	public static Map<Id, RecordType> recordTypeStaticMap; 	
  	
  	//Static map for getting record types for a given id
  	public static Map<Id, RecordType> recordTypeMap(){
		if (recordTypeStaticMap == null){
			recordTypeStaticMap = new Map<Id, RecordType>([Select sObjectType, DeveloperName, Id, Name
                        From RecordType ]);
		} 
		
		return recordTypeStaticMap;
	}
	*/
	
	public static Account defaultHouseholdStatic; 	
	
	public static Account defaultHousehold(){
		if (defaultHouseholdStatic == null){
			try{
				defaultHouseholdStatic = [SELECT Id from Account WHERE Name =: Label.Default_Account LIMIT 1];
			} catch (Exception e){
            //do nothing
        	}
		} 
		
		return defaultHouseholdStatic;
	}
	
	public static List<User> tibcoUsersStatic;	
	
	public static List<User> tibcoUsers(){
		if (tibcoUsersStatic == null){
			try{
				tibcoUsersStatic = [Select Id, UserName from User where UserName LIKE 'Tibco%' and IsActive = true];
			} catch (Exception e){
            //do nothing
        	}
		} 
		
		return tibcoUsersStatic;
	}
}