<!-- 
* @Name             Atvi_pkb_js.component
* @author           sumankrishnasaha
* @date             08-FEB-12
* @description      Cloned from the pkb_js component in the PKB2 application
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* sumankrishnasaha          08-FEB-12               Created
* A. Pal                    20-APR-12               Fixed the narrow filter data category select issue in buildSearchHref2() method 
*---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                     Review Comments
* --------                                          ------------                    --------------- 
* A. Pal(Deloitte Senior Consultant)                20-APR-2012                     Final Inspection before P2 go-live
*---------------------------------------------------------------------------------------------------
-->
<apex:component allowDML="true">
    <apex:attribute name="pkbCon" type="pkb_Controller" required="true" description="handle to the pkb controller" />

    <script type="text/javascript">
        // capture some data from the Apex controller and store in js vars
        var searchPrompt = "";//"{!JSENCODE(HTMLENCODE(pkbCon.searchPrompt))}";
        var baseURL = "{!JSENCODE(HTMLENCODE(pkbCon.currentSiteUrl))}";
        var currSearch1 = jQuery.trim("{!JSENCODE(HTMLENCODE(pkbCon.urlSearchQuery))}");
        var currLanguage = jQuery.trim("{!JSENCODE(HTMLENCODE(pkbCon.selectedLanguage))}");
        var currCategories = jQuery.trim("{!JSENCODE(HTMLENCODE(pkbCon.selectedCategory))}");
        
        var contactUs = {!pkbCon.displayContactUs};
        var viewingArticle = {!pkbCon.viewingArticle};
        var pkbHome = "{!$Page.pkb_Home}";
        var openDialogWhenReady = false;
        
        // constants pointing to element ids used in the page
        var CATEGORY_SELECT_ID_PREFIX = 'categorySelect';
        var SEARCH_LINK_ID = 'searchAgainButton';
        var ASK_LINK_ID = 'searchAskButton2';
        var NEXT_LINK_ID = 'nextLink';
        var NEXT_DIS = 'nextLinkDisabled';
        var SEARCH_TEXT_ID = 'searchAskInput2';
        var RSS_RESULTS_LINK_ID = 'resultsRssLink';
        var RIGHT_COLUMN_ID = 'right_column';
        var MIN_CONTENT_HEIGHT = 650;
        var FEEDBACK_YESNO_FORM_ID = 'feedbackYesNoForm';
        var FEEDBACK_FORM_ID = 'feedbackForm';
        var FEEDBACK_YES_BUTTON = 'feedbackYesButton';
        var FEEDBACK_NO_BUTTON = 'feedbackNoButton';
        var FEEDBACK_DIALOG_ID = 'feedbackDialog';
        var FEEDBACK_COMMENTS_ID = 'feedbackComments';
        var FEEDBACK_TEXT_ID = 'feedbackTextArea';
        var CHARS_REMAINING_ID = 'charsRemaining';
        var FEATURED_ARTICLES_ID = 'featuredArticles';
        var IMMEDIATE_HELP_ID = 'immediateHelp';
        var CASE_ERROR_MSG_ID = 'createCaseErrorMsg';
        
        var CONTACT_NO_BUTTON_ID = 'contactUsNoButton';
        var CONTACT_YES_BUTTON_ID = 'contactUsYesButton';
        var CONTACT_US_FORM_ID = 'contactUsForm';
        var CONTACT_YES_NO_FORM_ID = 'contactYesNoForm';
        
        // will store each selected category string for later processing
        var selectedCategories = new Array();
        
        var GameTitle;
        var GamePlatform;
        
        function setLanguage(e) {
          currLanguage = e.options[e.selectedIndex].value;
          buildSearchHref();
          doSearch();
          buildSearchHref3();
          doSearch3();  
        }
        
        
        
            var collapsed = 1;
            
            jQuery(window).load(function(e) {
            jQuery('.knowledgeBaseSearch .filterDropdown .handle a').click(function(e){
                e.preventDefault();
                if(collapsed){
                    collapsed = 0;
                    jQuery(this).text('Hide Filters');
                    jQuery(this).css('background-position','-3px -6px');
                    jQuery('.knowledgeBaseSearch .filterDropdown .filterContainer').show();
                }else{
                    collapsed = 1;
                    jQuery(this).text('Show Filters');
                    jQuery(this).css('background-position','-3px 6px');
                    jQuery('.knowledgeBaseSearch .filterDropdown .filterContainer').hide();
                }
                
            });
            
            jQuery('#' +FEEDBACK_DIALOG_ID).ready(function() {
          if (openDialogWhenReady) showFeedbackDialog();
        });
        
        jQuery('#' +SEARCH_TEXT_ID).ready(function() {
          var oppti = jQuery('#' +SEARCH_TEXT_ID)[0];
          if (currSearch1 == "") {
            oppti.value = searchPrompt;
            oppti.className = "default";
          }
        });
        
        jQuery("#question").keypress(function(event) {
          if ( event.which == 13 ) {
             event.preventDefault();
             // currSearch1 = jQuery.trim(o.value);
             currSearch1 = jQuery.trim(jQuery("#question").val());
            var xpp = buildSearchHref2();
            /*alert("x::-"+x);*/
             window.location.href=xpp;
           }
         
        });
        
        
        });
        
        
        // called when page is rendered, uses the category data passed from the Apex controller to build the selectedCategories array 
        // as well as to set the category selects to any previously-selected categories
        function setCurrCategories() {
        if(currCategories != null && currCategories != '') {
        collapsed = 0;
               }
          var i, j, selects, catObjs;
        
          if (currCategories != null && currCategories != '') {
            selectedCategories = currCategories.split(',');
        
            // build a hash of the categories for easy searching later
            catObjs = new Object();
            for (i = 0; i < selectedCategories.length; i++) {
              catObjs[selectedCategories[i]] = true;
            }
        
            // go through each option in each select and select the option if it is in the hash
            selects = document.getElementsByTagName('select');
            for (i = 0; i < selects.length; i++) {
              oppti = selects[i];
              if (oppti.id.indexOf(CATEGORY_SELECT_ID_PREFIX) > -1) {
                for (j = 0; j < oppti.options.length; j++) {
                  if (catObjs[oppti.options[j].value]) {
                    oppti.selectedIndex = j;
                  }
                }
              }
            }  
          }
        }
        
        // called from the onchange handler in each of the category select picklists, this will rebuild the array of selected
        // categories, rebuild the search href,  and then call doSearch()
        function setCategory(e) {
          var i;
          var o;
          var selects = document.getElementsByTagName('select');  
          selectedCategories = new Array();
        
          // check all of the select lists in the narrow search section and pull out the selected values
          for (i = 0; i < selects.length; i++) {
            oppti = selects[i];
            if (oppti.id.indexOf(CATEGORY_SELECT_ID_PREFIX) > -1 && o.selectedIndex > 0) {
              selectedCategories.push(o.options[o.selectedIndex].value);      
            }
          }
          buildSearchHref();
          buildSearchHref3();
          /* commented by sumankrishnasaha
          doSearch();*/
        }
        
        // grabs all of the relevant search details (query, category selections) and uses those to build the href of the search link
        // continuously updating the href property of the search link this way allows the "right-click and open in new tab/window..."
        // options to always work
        function buildSearchHref3() {
            //alert('++H++');
            var e = document.getElementById(SEARCH_LINK_ID);
          if (!e) e = document.getElementById(ASK_LINK_ID);
        
        //  var url = baseURL + '?';
          var url = pkbHome + '?';
        
          var params = new Array();
        
          if (validSearchQuery()) {
                params.push(/*"{!pkbCon.queryParam}="*/ "q=" +encodeURIComponent(currSearch1));
            } 
          params.push("{!pkbCon.languageParam}=" +encodeURIComponent(currLanguage));
          if (selectedCategories.length) params.push("{!pkbCon.categoryParam}=" +encodeURIComponent(selectedCategories.join(',')));
          if (contactUs) params.push("{!pkbCon.contactUsParam}={!pkbCon.contactUsValue}");
        
          url += params.join('&');
        
        if(selectedCategories.length==1)
          {
            //alert(selectedCategories[0]);
            if(selectedCategories[0][0]=='G'){GameTitle=selectedCategories[0];}
            
            
            if(selectedCategories[0][0]=='P'){GamePlatform=selectedCategories[0];}
            
            //alert("GameTitle:"+GameTitle);
            //alert("GamePlatform:"+GamePlatform);
          }
          if(selectedCategories.length==2)
          {
            //alert(selectedCategories[0] + '///' + selectedCategories[1]);
            GameTitle=selectedCategories[0];
            GamePlatform=selectedCategories[1];
          }
        
          e.href = url;
        }
        
        // detects the pressing of the enter key while the search box is in focus and performs the search
        function checkForEnter3(e, o) {
          if (e.keyCode == 13) {
            currSearch1 = jQuery.trim(o.value);
            buildSearchHref3();
            searchButtonClicked3();
          }
         
        }
        
        function searchButtonClicked3() {
            currSearch1 = jQuery.trim(jQuery('#searchAskInput2').val());
            //alert('######');
            buildSearchHref3();
          if (!validSearchQuery()) return;
          doSearch3();
        }
        
        
        function buildSearchHref2() {
          var e = document.getElementById(SEARCH_LINK_ID);
          if (!e) e = document.getElementById(ASK_LINK_ID);
        
        //  var url = baseURL + '?';
          var url = pkbHome + '?';
        
          var params = new Array();
        
          if (validSearchQuery()) {
        //         params.push(/*"{!pkbCon.queryParam}="*/ "q=" +encodeURIComponent(currSearch));
            } 
          params.push("l=" +encodeURIComponent(currLanguage));
          
          /* var currSearch1 = currSearch.split(", ");
          var filterString = "";
        //   currSearch1[0] = currSearch1[0].replace(/[_\W]+/g,"_"));
        //   currSearch1[1] = currSearch1[1].replace(/[_\W]+/g,"_"));
          
          if (jQuery.inArray(currSearch1[0].replace(/[_\W]+/g,"_"), categoryGroup1) > -1) {
            filterString = "Game_Title:" + currSearch1[0].replace(/[_\W]+/g,"_");
            if (jQuery.inArray(currSearch1[1].replace(/[_\W]+/g,"_"), categoryGroup2) > -1) {
            filterString += ",";
            }
          }
          if (jQuery.inArray(currSearch1[1].replace(/[_\W]+/g,"_"), categoryGroup2) > -1) {
            filterString += "Platform:" + currSearch1[1].replace(/[_\W]+/g,"_");
          } */
          
        //   params.push(/*"{!pkbCon.categoryParam}="*/ "c=" +encodeURIComponent(filterString));
          /* if (contactUs) params.push("{!pkbCon.contactUsParam}={!pkbCon.contactUsValue}");
        
          url += params.join('&');
          /*alert(url);*/
          e.href = url;
          
          
          var filterString = "";
          
          if (jQuery.inArray(currSearch.replace(/[_\W]+/g,"_"), categoryGroup1) > -1) {
            filterString = "Game_Title:" + currSearch.replace(/[_\W]+/g,"_");
          }
          
          params.push(/*"{!pkbCon.categoryParam}="*/ "c=" +encodeURIComponent(filterString));
          if (contactUs) params.push("{!pkbCon.contactUsParam}={!pkbCon.contactUsValue}");
        
          url += params.join('&');
        
          e.href = url;
          return url;
        }
        
        function checkForEnter2(e, o) {
         
          if (e.keyCode == 13) {
            currSearch = jQuery.trim(o.value);
            var x = buildSearchHref2();
            /*alert("x::-"+x);*/
            
             window.location.href="http://atvisupport.gsdev2.cs9.force.com/pkb_Home?l=&c=Game_Title%3ABlack_Ops";
          }
          return false;
        }
        
        function searchButtonClicked2() {
            currSearch = jQuery.trim(jQuery('#question').val());
            buildSearchHref2();
          if (!validSearchQuery()) return;
          doSearch3();
        }
        
        // udpates the search link url and then performs the search as long as a query has been entered
        function doSearch3() {
            //alert('********');
            freezeInputs3();
          var e = document.getElementById(SEARCH_LINK_ID);
          if (!e) e = document.getElementById(ASK_LINK_ID);
          window.location.href = e.href;
          setTimeout(function(){document.location.href = e.href},500);          
        }
        
        // freeze the category select options, if any, and search text box so that they cannot be changed while the search is loading
        function freezeInputs3() {
          var selects = document.getElementsByTagName('select');
          var textarea = document.getElementById(SEARCH_TEXT_ID);
          var i;
          
          for (i = 0; i < selects.length; i++) {
            if (selects[i].id.indexOf(CATEGORY_SELECT_ID_PREFIX) > -1) { 
              selects[i].disabled = true;
            }
          }
          textarea.disabled = true;
        }
        
        // restores the search prompt if the search box is empty, otherwise copies the search query to the global var
        function restorePrompt(e) {
          if (e.value == "" || e.value == null) {
            e.value = searchPrompt;
            currSearch = searchPrompt;
            e.className = "default";
          } else {
            currSearch = jQuery.trim(e.value);
          }
        }
        
        // clears the search box of the prompt text when a user clicks on it unless the prompt is being displayed
        function clearPrompt(e) {
          if (e.value == searchPrompt) {
            e.value = "";
            e.className = "";
          }
        }
        
        // clears the search box regardless of what text is in there
        function resetPrompt(e) {
          currSearch = "";
          e.value = "";
          e.className = "";
        }
        
        function validSearchQuery() {
        /* commented by sumankrishnasaha 20-Mar-12
        var val = currSearch != '' && currSearch != searchPrompt;
          return val;*/
          return true;
        }
        
        // hides the "next" article link if there are no more articles
        // the more var is set in the VF "articles" component each time the articleList tag iterates
        var moreResults = false;
        function hideNext() {
          e = document.getElementById(NEXT_LINK_ID);
          Ed =  document.getElementById(NEXT_DIS);
          if (!e) return;
          if (!moreResults) {
              e.style.display = 'none';
              Ed.style.display = 'block';
          }
        }
        
        var noResultsFound = true;
        function hideResultsRss() { 
          e = document.getElementById(RSS_RESULTS_LINK_ID);
          if (!e) return;
          if (noResultsFound) e.style.display = 'none';
        }
        
        function hideImmediateHelp(noResults) {
          if (noResults) {
            var o = jQuery('#' +IMMEDIATE_HELP_ID);
            if (o.length) o[0].style.display = 'none';
          }
        }
        
        function showFeedbackDialog() {
          jQuery('#' +FEEDBACK_DIALOG_ID).dialog();
        }
        
        function copyComments() {
          var o = jQuery('[id$=' +FEEDBACK_COMMENTS_ID+ ']');
          var p = jQuery('#' +FEEDBACK_TEXT_ID);
          
          if (o.length && p.length) o[0].value = p[0].value;
        }
        
        function toggleFeedbackYesNoButtons(isEnabled) {
          var yes = jQuery('[id$=' +FEEDBACK_YES_BUTTON+ ']')[0];
          var no =jQuery('[id$=' +FEEDBACK_NO_BUTTON+ ']')[0];
        
          yes.disabled = !isEnabled;
          no.disabled = !isEnabled;
        }
        
        function toggleContactYesNoButtons(isEnabled) {
          var yes = jQuery('[id$=' +CONTACT_YES_BUTTON_ID+ ']')[0];
          var no = jQuery('[id$=' +CONTACT_NO_BUTTON_ID+ ']')[0];
        
          yes.disabled = !isEnabled;
          no.disabled = !isEnabled;
        }
        
        function closeModal() {
          jQuery('#' +FEEDBACK_DIALOG_ID).dialog("close");
        }
        
        function countChars(event, o) {
          var maxChars = 255;
          var chars = document.getElementById(CHARS_REMAINING_ID);
          var charsLeft = maxChars - o.value.length;
        
          if (charsLeft <= 0) {
            o.value = o.value.substring(0, maxChars-1);
            charsLeft = 0;
            return (event.keyCode == 8);
            chars.innerHTML = charsLeft;
          } else {
            chars.innerHTML = charsLeft;
            return true;
          }
        }
        
        var atLeastOneFeatured = false;
        function hideFeatured() {
          var e = document.getElementById(FEATURED_ARTICLES_ID);
          if (!e) return;
          if (!atLeastOneFeatured) e.style.display = 'none';
        }
        
        function adjustHeight() {
          var o = jQuery('#' +RIGHT_COLUMN_ID);
        
          if (o.height() < MIN_CONTENT_HEIGHT) o.height(MIN_CONTENT_HEIGHT);
        }
        
        function rewriteContactUsFormAction() {
          if (!contactUs) return;
          
          var actionURL = pkbHome;
          if (viewingArticle) actionURL += '?id={!pkbCon.theKad.id}';
        
          var o = jQuery('[id$=' +CONTACT_US_FORM_ID+ ']');
          var p = jQuery('[id$=' +CONTACT_YES_NO_FORM_ID+ ']');
          if (o.length) o[0].action = actionURL;
          if (p.length) p[0].action = actionURL;
        }
        
        function rewriteFeedbackFormAction() {
          if (contactUs || !viewingArticle) return;
        
          var actionURL = pkbHome + '?id={!pkbCon.theKad.id}';
        
          var o = jQuery('[id$=' +FEEDBACK_YESNO_FORM_ID+ ']');
          var p = jQuery('[id$=' +FEEDBACK_FORM_ID+ ']');  
        
          if (o.length) o[0].action = actionURL;
          if (p.length) p[0].action = actionURL;
        
        }
        
        function clearCaseErrorMsg() {
          var o = jQuery('[id$=' +CASE_ERROR_MSG_ID+ ']');
          if (o.length) o[0].style.display = 'none';
        }
        
        function prepareToRenderOverlay() {
          openDialogWhenReady = true; 
        }
        
        
        jQuery(window).load(function() {
          if (viewingArticle) adjustHeight();
          rewriteContactUsFormAction();
          rewriteFeedbackFormAction();
        });
    </script>
</apex:component>