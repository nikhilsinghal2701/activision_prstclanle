<!-- 
* @Name             ATVI_pkb_Home.component
* @author           Abhishek Pal
* @date             09-MAR-12
* @description      This component is used in knowledge base home page.
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* A. Pal                    09-MAR-12               Created
*---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                     Review Comments
* --------                                          ------------                    --------------- 
* K. Sankar(Deloitte Consultant)                    22-MAR-2012                     Final Inspection before P1 go-live
*---------------------------------------------------------------------------------------------------
-->
<apex:component allowDML="true" controller="Atvi_pkb_page_navigation">
    <apex:attribute name="showheaderinfo" type="String" description="TODO: Describe me"/>
    <apex:attribute name="pkbCon" type="pkb_Controller" required="true"
        description="handle to the pkb controller" />
    <apex:attribute name="sortBy" type="String" required="false" description="" />
    <apex:attribute name="displayType" type="String" required="false" default="Featured" 
         description="Why the article was displayed to the user, Featured or Search"  />
     <apex:variable var="category"
                value="{!IF(ISBLANK(pkbCon.selectedCategory), pkbCon.categoryGroup1+" :"+pkbCon.rootCategory1, pkbCon.selectedCategory)}" />
	<apex:variable var="currPage" value="{!IF(pkbCon.displayContactUs, 1, pkbCon.currPage)}" />
    <apex:variable var="noResultsFound" value="{!IF(pkbCon.foundRecommended, 'false', 'true')}" />
    <apex:variable var="more" value="true" />

    <script type="text/javascript">
        /*
        POPULAR PAGE SIZE: {!pkbCon.popularArticles}
        RESULTS PAGE SIZE: {!pkbCon.articlesPerPage}
        CURRENT PAGE: {!pkbCon.currPage}
        LANG: {!pkbCon.selectedLanguage}
        CATEGORY USED: {!pkbCon.selectedCategory}
        */
        var csURL = "";
    </script>
    
    <ul class="landing-results cf">
		<apex:outputPanel layout="none" id="articlePage">
	        <knowledge:articleList articleVar="a" 
                pageSize="7"
                categories="{!pkbCon.selectedCategory}"
                hasMoreVar="more"
                keyword="{!pkbCon.urlSearchQuery}"
                language="{!pkbCon.selectedLanguage}"
                pageNumber="{!currPage}"
                sortBy="{!sortBy}"
            >
            	<c:Atvi_pkb_article_responsive pkbCon="{!pkbCon}" 
            	      aid="{!a.id}"
                      title="{!a.title}"
                      summary="{!a.summary}"
                      urlName="{!a.urlName}"
                      articleTypeName="{!a.articleTypeName}"
                      articleTypeLabel="{!a.articleTypeLabel}"
                      lastModifiedDate="{!a.lastModifiedDate}"
                      firstPublishedDate="{!a.firstPublishedDate}"
                      lastPublishedDate="{!a.lastPublishedDate}"
                      displayType="{!displayType}"
                      anotherArtId="{!a.Id}"
                />
            	<script type="text/javascript">moreResults = {!more};</script>
                <apex:variable var="noResultsFound" value="false" />
             	<script type="text/javascript">noResultsFound = {!noResultsFound}</script>  
            </knowledge:articleList>        
            <script type="text/javascript">
                if("{!pkbcon.currpage}" == "1"){
                    if(csURL != ""){
                        csURL = csURL.substr(2,csURL.length-1);
                        cmsrEvent("Search Term","{!pkbcon.urlsearchquery}",csURL);
                    }
                    else{
                        cmsrEvent("Search Term","{!pkbcon.urlsearchquery}","None");
                    }
                }
            </script>
            <apex:outputPanel layout="block" rendered="{!(noResultsFound == 'true') && (NOT(pkbCon.displayContactUs))}" style="margin-top: 10px;">
                {!$Label.PKB2_No_Results}
            </apex:outputPanel>
        </apex:outputPanel>
    </ul>
    <apex:actionStatus id="nextStatus">    
        <apex:facet name="start"><apex:image value="{!$Resource.pkb_loadingdots}" /></apex:facet>
        <apex:facet name="stop"></apex:facet>
    </apex:actionStatus>

	<apex:outputPanel rendered="{!noResultsFound = 'false'}" layout="block" id="navPanel">
		<ul class="landing-pagination cf">
			<apex:outputPanel layout="none" id="prevPanel" rendered="{!pkbCon.currPage > 1}">
				<li class="previous"><a href="javascript:void(0);" onclick="JS_GoToResultsPage({!currPage - 1});">&lt;</a></li>
            </apex:outputPanel>
            <!-- excluded because a reliable way to determine the number of results was unknown 
            <c:Atvi_pkb_page_navigation
    			lang="{!pkbCon.selectedLanguage}" 
    			pageSize="1" 
    			categories="{!category}" 
    			currentPageNumber="{!currPage}"
    		/>
    		-->
        	<apex:outputPanel layout="none" id="nextPanel" rendered="{!more}">
        		<li class="next"><a href="javascript:void(0);" onclick="JS_GoToResultsPage({!currPage + 1});">&gt;</a></li>
        	</apex:outputPanel>
    	</ul>
    </apex:outputPanel>
<!-- Orig pagnagion
                <apex:outputPanel layout="block" style="height: 20px; margin-top: 10px;" styleClass="resultsPaginationLinks" >
                    <div style="float: right; margin-bottom: 10px;">
                        <span id="nextLink">&nbsp;
                            <apex:commandLink action="{!pkbCon.nextPage}" rerender="results" status="nextStatus">
                                <apex:image url="{!URLFOR($Resource.V2_resources, 'assets/Pager-Next-icon.png')}" alt="" />
                            </apex:commandLink>
                        </span>
                        <span id="nextLinkDisabled" style="display:none;">
                            <apex:image url="{!URLFOR($Resource.V2_resources, 'assets/Pager-Next-icon2.jpg')}" alt=""  /> 
                        </span>
                    </div>
                    <div style="float: right; margin-bottom: 10px;">
                        <apex:commandLink action="{!pkbCon.prevPage}" rerender="results" rendered="{!(pkbCon.currPage > 1)}" status="nextStatus">
                            <apex:image url="{!URLFOR($Resource.V2_resources, 'assets/Pager-Previous-icon.png')}" alt="Previous" rendered="{!(pkbCon.currPage > 1)}" />
                        </apex:commandLink>
                        <apex:image url="{!URLFOR($Resource.V2_resources, 'assets/Pager-Previous-icon2.jpg')}" alt="Previous" rendered="{!(pkbCon.currPage == 1)}" />
                        &nbsp;&nbsp;
                    </div>
                </apex:outputPanel>
                <apex:outputText rendered="{!pkbCon.displayContactUs && pkbCon.searchExecuted}">
                    <script type="text/javascript">if ({!noResultsFound}) prepareToRenderOverlay();</script>
                </apex:outputText>
                <script type="text/javascript">hideNext(); hideResultsRss(); hideImmediateHelp({!noResultsFound});</script>
            </div>
        </apex:outputpanel>
        -->
    
</apex:component>