<apex:component controller="MedalliaSettingController">
    <apex:attribute name="auth" description="This is the value for the component." type="String" required="true"/>
    <style type="text/css">    
    .overlay 
    {
        z-index:999990;
        position:absolute;
        width:100%;
        height:100%;
        background:#000;
        opacity:0.33;
    }
    .allegianceModal 
    {
        z-index:999991;
        position:absolute;
        width:300px;
        background:#fff;
        padding:20px;
        left:0;
        right:0;
        top:25%;
        margin-top:auto;
        margin-bottom:auto;
        margin-left:auto;
        margin-right:auto;
    }
    .buttons{
        display: block;
        float: left;
        margin: 0 7px 0 0;
        padding: 10px 15px 10px 15px;
        background: #2abcff url(../assets/module-account-links-arrow.png) no-repeat 15px 12px;
        font: 700 13px/16px "Open Sans Bold", sans-serif;
        color: #fff;
        text-decoration: none;
        text-transform: uppercase;
    }
    .buttons-outer{
        margin: 0 7px 0 0;
        padding: 10px 15px 10px 15px;
    }
    .modal p{
        padding: 10px 15px 10px 15px;
    }
    </style>
    <div id="askForSurvey" style="display: none;">
        <div class="overlay"></div>
        <div class="allegianceModal">
            <img src="{!URLFOR($Resource.V2_resources, '/assets/logo-activision.png')}" />
            <p>Activision is here to help you! Fill out our survey {!IF(isMobile, '','at the end of your visit ')}and tell us what we can do better.</p>
            <p>Your opinion matters to us!</p>
            <div class="buttons-outer">
            <a href="javascript:void(0);" id="surveyAccept" class="buttons" onclick="try{cmsrEvent('survey_modal','survey_modal','modal_accept');}catch(e){}{!IF(isMobile, 'AllegianceSiteInterceptOnExit.launchSurvey();','AllegianceSiteInterceptOnExit.popUp();AllegianceSiteInterceptOnExit.hideModal();')}">Yes</a>
            <a href="javascript:void(0);" id="surveyDecline" class="buttons" onclick="AllegianceSiteInterceptOnExit.declineSurvey();try{cmsrEvent('survey_modal','survey_modal','modal_decline');}catch(e){}">No</a>
            </div>

        </div>
    </div>
    <script type="text/javascript">
        function getCookie(cName){
            var i,x,y,ARRcookies=document.cookie.split(";");
            for (i=0;i<ARRcookies.length;i++){
                x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
                y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
                x=x.replace(/^\s+|\s+$/g,"");
                if (x==cName){
                  return unescape(y);
                  break;
                }
            }
            return '';
        }
        function getProbability(){
            if('{!survLang}' != 'English') return 0;
            var cVal = getCookie('apex__clickedOn');
            if(cVal == '') cVal = getCookie('clickedOn');
            if(cVal != '' && cVal.toLowerCase().indexOf('skylander') >= 0) return {!settings.Skylanders_Sampling_Rate__c};
            return {!settings.Sampling__c};
        }
    </script>
    <script type="text/javascript">
        /*#######################################################################################
            surveyURL           The URL of the target survey.
            placeholderLink     This link will be loaded in the background window prior to the survey.
            invitationID        The CSS ID of the DOM element that will prompt the user to take the survey.
            probability         The probability determines the likelihood that the user will be prompted to take the survey.
            width               The width of the popup.
            height              The height of the popup.
            expireDaysIfYes     If the user selects to take the survey -- the amount of days that must pass before the user is eligible to take the survey again.
            expireDaysIfNo      If the user selects NOT to take the survey -- the amount of days that must pass before the user is asked to take the survey again.
            delay               How long the page will wait before asking to the user to take the survey.
            cleaseCookie        Setting this to true will clear out any saved cookies relating to the display of the survey. Useful for testing.
        #######################################################################################*/
        var AllegianceSiteInterceptOnExit = {
            parameters: {
                surveyURL: 'https://activision.allegiancetech.com/cgi-bin/qwebcorporate.dll?idx=6EJTJR&language={!survLang}', //&preview=1,
                placeholderLink: "siteIntercept", //
                invitationID: 'askForSurvey',
                probability: getProbability(), //0 - 100%
                width: 780, //px
                height: 520, //px
                expireDaysIfYes: 30, //days
                expireDaysIfNo: 30, //days
                delay: ({!delay}*1000), //ms
                cleanseCookie: false
            },
        
            takeSurvey: false, //Script-level variable, leave false.
            surveyWindow: undefined, //Survey window, leave undefined.
            surveyInfoCookie: undefined,
            surveyURLParams: undefined,
        
            //Called when the page is loaded. Executes logic to determine whether to show the invitation or not.
            onPageLoad: function () {
                AllegianceSiteInterceptOnExit.surveyInfoCookie = AllegianceSiteInterceptOnExit.getCookieByName('allegSurveyInfo');
                if (!AllegianceSiteInterceptOnExit.surveyInfoCookie) AllegianceSiteInterceptOnExit.createCookie('allegSurveyInfo', '[{}]', 1);
                window.setTimeout(function () {
                    var rand = Math.floor(Math.random() * 100);
                    if (rand < AllegianceSiteInterceptOnExit.parameters.probability) {
                        var cookie = AllegianceSiteInterceptOnExit.getCookie('allegSurveyQuarantine', 'allegSurveyQuarantine');
                        if (!cookie) {
                            AllegianceSiteInterceptOnExit.showModal();
                        }
                    }
                }, AllegianceSiteInterceptOnExit.parameters.delay);
            },
        
            //Used by mobile pages where the behavior is to send users directly to the survey instead of "onExit"
            launchSurvey: function() {
                AllegianceSiteInterceptOnExit.createCookie('allegSurveyQuarantine', 'allegSurveyQuarantine', AllegianceSiteInterceptOnExit.parameters.expireDaysIfNo);
                window.location.href = AllegianceSiteInterceptOnExit.parameters.surveyURL;
            },
        
            addUrlParameter: function (value, name) {
                AllegianceSiteInterceptOnExit.surveyInfoCookie = AllegianceSiteInterceptOnExit.getCookieByName('allegSurveyInfo');
                var object = JSON.parse(AllegianceSiteInterceptOnExit.surveyInfoCookie);
                object[0][value] = name;
                AllegianceSiteInterceptOnExit.createCookie('allegSurveyInfo', JSON.stringify(object), 1);
                AllegianceSiteInterceptOnExit.surveyInfoCookie = AllegianceSiteInterceptOnExit.getCookieByName('allegSurveyInfo');
            },
        
            removeUrlParameter: function (value) {
                var object = JSON.parse(AllegianceSiteInterceptOnExit.surveyInfoCookie);
                delete object[0][value];
                AllegianceSiteInterceptOnExit.createCookie('allegSurveyInfo', JSON.stringify(object), 1);
                AllegianceSiteInterceptOnExit.surveyInfoCookie = AllegianceSiteInterceptOnExit.getCookieByName('allegSurveyInfo');
            },
        
            //Checks for a cookie\s existence based on the name and value
            getCookie: function (name, value) {
                if (AllegianceSiteInterceptOnExit.parameters.cleanseCookie) AllegianceSiteInterceptOnExit.createCookie(name, "", -1); //Removes cookie
                if (document.cookie.indexOf(name) == 0)
                    return -1 < document.cookie.indexOf(value ? name + "=" + value + ";" : name + "=")
                else if (value && document.cookie.indexOf("; " + name + "=" + value) + name.length + value.length + 3 == document.cookie.length)
                    return true
                else {
                    return -1 < document.cookie.indexOf("; " + (value ? name + "=" + value + ";" : name + "="))
                }
            },
        
            getCookieByName: function (name) {
                var value = "; " + document.cookie;
                var parts = value.split("; " + name + "=");
                if (parts.length == 2) return parts.pop().split(";").shift();
            },
        
            //Creates a cookie with the given parameters
            createCookie: function (name, value, days) {
                days = parseInt(days, 10);
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    var expires = "; expires=" + date.toGMTString();
                }
                else var expires = "";
                document.cookie = name + "=" + value + expires + "; path=/";
            },
        
            //Called when page is closed or domain is changed.
            onPageClose: function () {
                AllegianceSiteInterceptOnExit.addUrlParameter("lastVisitedURL", window.location.href);
                if (AllegianceSiteInterceptOnExit.takeSurvey && AllegianceSiteInterceptOnExit.surveyWindow.opener && !AllegianceSiteInterceptOnExit.surveyWindow.opener.closed) {
                    AllegianceSiteInterceptOnExit.surveyWindow.AllegianceSurveyWindow.surveyUrl = AllegianceSiteInterceptOnExit.parameters.surveyURL;
                } else return;
            },
        
            //Brings up the window that will contain the survey.
            popUp: function () {
                var strWindowFeatures = "menubar=0,location=yes,resizable=yes,scrollbars=yes,toolbar=0,status=yes,modal=yes,width=" + parseInt(AllegianceSiteInterceptOnExit.parameters.width, 10) + ",height=" + parseInt(AllegianceSiteInterceptOnExit.parameters.height, 10);
                AllegianceSiteInterceptOnExit.takeSurvey = true;
                AllegianceSiteInterceptOnExit.createCookie('allegSurveyQuarantine', 'allegSurveyQuarantine', AllegianceSiteInterceptOnExit.parameters.expireDaysIfYes);
                document.getElementById(AllegianceSiteInterceptOnExit.parameters.invitationID).style.display = 'none';
                AllegianceSiteInterceptOnExit.surveyWindow = window.open(AllegianceSiteInterceptOnExit.parameters.placeholderLink, "_blank", strWindowFeatures);
                window.focus();
            },
        
            //Creates a quarantine cookie and hides the invitation.
            declineSurvey: function () {
                AllegianceSiteInterceptOnExit.createCookie('allegSurveyQuarantine', 'allegSurveyQuarantine', AllegianceSiteInterceptOnExit.parameters.expireDaysIfNo);
                document.getElementById(AllegianceSiteInterceptOnExit.parameters.invitationID).style.display = 'none';
            },
            //Shows the invitation
            showModal: function () {
                try{cmsrEvent('survey_modal','survey_modal','modal_display');}catch(e){}
                document.getElementById(AllegianceSiteInterceptOnExit.parameters.invitationID).style.display = 'block';
            }
        }
        
        function addOnLoadEvent(func) {
            var oldonload = window.onload;
            if (typeof window.onload != 'function') {
                window.onload = func;
            } else {
                window.onload = function () {
                    if (oldonload) { oldonload(); }
                    func();
                }
            }
        }
        function addBeforeUnLoadEvent(func) {
            var oldonload = window.onbeforeunload;
            if (typeof window.onbeforeunload != 'function') {
                window.onbeforeunload = func;
            } else {
                window.onbeforeunload = function () {
                    if (oldonload) { oldonload(); }
                    func();
                }
            }
        }
        function addOnUnLoadEvent(func) {
            var oldonload = window.onunload;
            if (typeof window.onunload != 'function') {
                window.onunload = func;
            } else {
                window.onunload = function () {
                    if (oldonload) { oldonload(); }
                    func();
                }
            }
        }
        
        addOnUnLoadEvent(AllegianceSiteInterceptOnExit.onPageClose); //ensures that other events are retained
        addBeforeUnLoadEvent(AllegianceSiteInterceptOnExit.onPageClose); //ensures that other events are retained
        addOnLoadEvent(AllegianceSiteInterceptOnExit.onPageLoad); //ensures that other events are retained
    </script>
    
</apex:component>