<!-- 
* @Name             Atvi_rma_confirmation
* @author           Abhishek Pal
* @date             02-MAY-2012
* @description      Final confirmation page after submitting an RMA from Portal
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                   Date                    Description
* ---------------           -----------             ----------------------------------------------
* A. Pal                    02-MAY-2012             Created 
*---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
*  Reviewer                                          Review Date                     Review Comments
* --------                                          ------------                    ---------------* 
* Karthik(Deloitte Consultant)                      03-MAY-2012                     Final Inspection
*---------------------------------------------------------------------------------------------------
-->

<apex:page controller="Atvi_warranty_returns_confirm"
    showHeader="false" sidebar="false" >
    <html>
        <head>
        <title>Activision.com Customer Support</title>
        <link rel="shortcut icon" type="image/ico" href="{!URLFOR($Resource.Atvi_Brandings, 'Atvi_Brandings/Generic/favicon.ico')}" />
        <!--<script type="text/javascript">
                var jslang = getCookie('apex__uLang');
                if(jslang.toLowerCase().indexOf("en") >= 0){
                $(document).ready(function() {
                    MEDALLIA.Intercept({cat: 'All', auth: 'Yes',  lang: 'English', lng: 'EN'});
                });
                }
            </script>--->
            <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-6621671-50']);
            
            _gaq.push(['_trackPageview', '/warranty_returns/4-confirmation']);
            
            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            </script>
        <style type="text/css">
        .pageContent { /* padding: 10px 30px 5px 0; commented by durga */
            padding: 0 0 35px;
            width: 100%;
        }
        
        /* added by durga to fix the issues on the page */
        #globalHeader .mainNavigation ul li {
            margin-left: 0;
        }
        
        .support-menu h2,.liveSupport h1 {
            display: block;
        }
        
        .answersSearch .inputText,.gameSearch .inputText,.knowledgeBaseSearch .inputText
            {
            color: black;
        }
        
        .support-menu li {
            margin: 0;
        }
        
        h1,h2,h3,h4,h5,h6 {
            display: block;
        }
        
        .btn,.button,.formulaButton,.btnWhatsNew {
            font-family: "Open Sans Light", Helvetica, Arial, sans-serif;
        }
        
        body .wrap-buttons input.btn.btnSaveChanges {
            background: url("../resource/1330756342000/Xi_agency/img/bgButton.gif")
                repeat-x scroll 0 bottom #555656;
            color: #A6D103;
            display: block;
            font-size: 13px;
            font-weight: 100;
            padding: 7px 25px;
            text-align: left;
            text-decoration: none;
            border: none medium;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            -o-border-radius: 0;
            border-radius: 0;
        }
        
        /* added by Priyankshu to fix the issues on the page */
        .wrap-step select {
            width: 300px;
            padding: 0px;
            border-bottom: 1px solid #E0DFDF;
        }
        
        .platformFilter a.button {
            float: right;
            margin-left: 20px;
        }
        
        .liveSupport .wrap-step .games-list li {
            margin-left: 0;
        }
        
        .liveSupport .gameSearch {
            border: none;
        }
        
        .liveSupport .games-list li {
            list-style-type: none;
        }
        
        .gameSearch {
            background: none;
        }
        
        .answersSearch .inputText,.gameSearch .inputText,.knowledgeBaseSearch .inputText
            {
            margin-left: -8px;
        }
        
        .liveSupport .wrap-buttons {
            margin-left: -90px;
        }
        
        .liveSupport .games-list img {
            margin-bottom: 0px;
        }
        
        .gameItem {
            width: 300px;
            margin-bottom: 39px;
        }
            
        .gameItem span {
            font: 700 14px/20px "Open Sans Bold", sans-serif;
        }
        
        .gameTitle {
            float: left;
            margin-top: -110px;
            margin-left: 150px;
        }
        
        .liveSupport .one-column {
            background: #F0F0F0;
        }
        
        .bPageBlock .detailList tr td,.bPageBlock .detailList tr th,.hoverDetail .bPageBlock .detailList tr td,.hoverDetail .bPageBlock .detailList tr th
            {
            border-bottom: none;
        }
        
        .bPageBlock,.individualPalette .bPageBlock {
            border: none;
        }
        
        table {
            border: none;
        }
        
        .registration-tabs li a {
            padding-top: 15px;
        }
        
        .submitClaimContainer ul {
            margin: 0 0 15px 0;
        }
            
        .bPageBlock .detailList .labelCol {
            width: 58%;
            font: 700 14px/14px "Open Sans Bold", sans-serif;
            vertical-align: middle;
        }
            
        .bPageBlock .detailList .data2Col {
            font: 300 14px/20px "Open Sans Light", sans-serif;
            vertical-align: middle;
        }
        
        .gameSelector .viewAllGamesContainer .allGamesList .gameItem .gameTitle
            {
            top: 125px;
            right: 145px;
        }
        
        .gameSelector .viewAllGamesContainer .allGamesList .gameItem {
            padding-bottom: 40px;
        }
        
        /**
         * sumankrishnasaha
         */
        .ui-autocomplete {
            max-height: 100px;
            overflow-y: auto;
            overflow-x: hidden;
            padding-right: 20px;
        }
        
        * html .ui-autocomplete {
            height: 100px;
        }
        
        #globalHeader .mainNavigation {
            text-align: left;
        }
        
        #globalHeader .mainNavigation ul {
            text-transform: uppercase;
            width: 1046px;
        }
        
        #globalHeader .mainNavigation ul li {
            margin-left: 0px;
            width: 261px;
        }
        
        #globalHeader .mainNavigation ul li a {
            padding: 17px 20px 12px;
            font-size: 15px;
            text-align: center;
        }
        
        #globalHeader .mainNavigation ul li.first a {
            padding: 17px 20px 12px;
            text-align: center;
        }
        
        #globalHeader .mainNavigation ul li.last a {
            padding: 17px 20px 12px;
            text-align: center;
        }
        
        .registration-tabs li {
            margin-left: 0;
        }
        
        .leftnew {
            width: 400px;
        }
        
        .rightnew {
            width: 500px;
        }
        
        .nextBtn{
            background-color: #2abcff;
            color: #fff !important;
            display: block;
            font: 300 14px/14px "Open Sans Light", sans-serif;
            margin: 0 3px 0 0;
            padding: 8px 0 6px;
            text-align: center;
            text-decoration: none !Important;
            width: 148px;
            position: absolute;
            right: 80px;
        }
        
        .submitClaimContainer .actionButtons{
            padding: 10px 0;
        }
        
        .warranty-confirm-case {font: 700 14px/18px "Open Sans Bold", sans-serif;}
        .warranty-confirm-blurb {display: block; margin: 15px 0 0 0;}
        .warranty-confirm-blurb p {font: 300 18px/24px "Open Sans Light", sans-serif;}
        .warranty-confirm-return-button {display: inline-block; min-width: 300px; margin: 15px 0 30px 0; padding: 10px 40px; background: #2abcff; font: 300 22px/28px "Open Sans Light", sans-serif; color: #fff; text-align: center; text-decoration: none;}
        a:hover.warranty-confirm-return-button {color: #fff; text-decoration: none;}
        </style>
        </head>
                  
        <apex:composition template="Atvi_site_template">
            <apex:define name="bodyHtml">
               <div id="all" class="pagewrapper" style="border:0px solid black;width:100%;">
                         <apex:outputpanel rendered="true">
<!-- start global header -->
            <div id="header">
                    <div id="header-inner" class="clearfix">
                        <h1 id="logo">
                            <a href="{!$page.ATVI_authenticated_homepage}" title="Activision Support Home">Activision <span>{!$Label.ATVI_Support}</span></a>
                        </h1>
                        <div id="navigation">
                            <ul>
                                                          
                                <li style="border:0px solid grey;">
                                <apex:outputLink value="{!$Page.Atvi_my_support_games}?clickedOn=" onclick="_gaq.push(['_trackEvent', 'Global Navigation', 'Navigation Selection', 'My Support']);"><span>{!$Label.Atvi_MySupport}</span></apex:outputLink>                                           
                                </li>
                                <li>
                                <apex:outputLink value="{!$Page.Atvi_warranty_returns}?clickedOn=" onclick="_gaq.push(['_trackEvent', 'Global Navigation', 'Navigation Selection', 'Warranty and Returns']);"><span>{!$Label.Atvi_WarrantyReturns}</span></apex:outputLink>
                                </li>                             
                                <li><apex:outputLink value="{!$Page.Contact_Us}" ><span>{!$Label.Atvi_CONTACTUS}</span></apex:outputLink></li>
                            </ul>
                        </div>
                        <div id="search">
                            
                                <div id="search-box">                        
                                    <c:Global_search />
                                </div>
                        </div>
                    </div>
                </div>           
            <!-- end global header -->

            <div id="content">
                    <div id="content-inner">
                            <apex:form id="form_element">
                                <apex:outputPanel id="warranty_returns_confirmation">
                            
                                    <div class="submitClaimContainer">
                                        <h2 class="warranty-title">{!$Label.Atvi_WarrentyClaim}</h2>
                                        <ul class="registration-tabs clearfix">
                                            <li class="first"><a class="selected">1.
                                                    {!$Label.Atvi_ProductInformation}</a></li>
                                            <li><a class="selected">2.
                                                    {!$Label.Atvi_ContactInformation}</a></li>
                                            <li><a class="selected">3. {!$Label.Atvi_Review}</a></li>
                                            <li class="last"><a class="selected">4.
                                                    {!$Label.Atvi_Confirmation}</a></li>
                                        </ul>
                                        
                                        
                                                    <apex:outputLabel styleClass="warranty-confirm-case">{!$Label.Atvi_CaseNumber}&nbsp;</apex:outputLabel>
                                                    <apex:outputText value="{!rmaCase.CaseNumber}" styleClass="warranty-confirm-case"></apex:outputText>
                                                
                                                    <apex:outputPanel styleClass="warranty-confirm-blurb">
                                                    <p>{!$Label.rma_YourClaimSubmitted}</p>
                                                        <br/>
                                                    <p>{!$Label.rma_pleaseclick}</p>
                                                    </apex:outputPanel>
                                                
                                                    <apex:outputLabel />
                                                    <apex:outputLink onclick="_gaq.push(['_trackEvent', 'Warranty and Returns', 'CTA Click', 'Return Instructions']);" styleClass="warranty-confirm-return-button" target="_blank" value="/articles/{!userLang}/FAQ/Download-Disc-Replacement-Form">{!$Label.Atvi_ReturnInstructions}</apex:outputLink>
                                                
                                        
                                        <table>
                                            <tr>
                                                <td style="padding-bottom: 140px;">
                                                    <div class="gameItem clearfix">
                                                        <apex:outputField value="{!rmaCase.Product_Effected__r.Image__c}" />
                                                        <span style="color: #7A7A7A; font-size: 14px;">{!rmaCase.Product_Effected__r.Name}</span><br />
                                                        <br /> <span style="color: #7A7A7A; font-size: 14px;">{!rmaCase.Product_Effected__r.Platform__c}</span>
                                                    </div>
                                                </td>
                                                <td style="width: 43%; vertical-align: top;"><apex:pageBlock >
                                                        <apex:pageBlockSection columns="1">
                                                            <apex:pageBlockSectionItem >
                                                                <apex:outputLabel >{!$Label.Atvi_Country}</apex:outputLabel>
                                                                <apex:outputText value="{!rmaCase.Country__c}"></apex:outputText>
                                                            </apex:pageBlockSectionItem>
                                                            <apex:pageBlockSectionItem >
                                                                <apex:outputLabel >{!$Label.Atvi_Attention}</apex:outputLabel>
                                                                <apex:outputText value="{!rmaCase.Attention__c}"></apex:outputText>
                                                            </apex:pageBlockSectionItem>
                                                            <apex:pageBlockSectionItem >                                        
                                                                <apex:outputLabel >{!$Label.Atvi_ShipTo}:</apex:outputLabel>
                                                                <apex:outputText value="{!rmaCase.Ship_To__c}"></apex:outputText>
                                                            </apex:pageBlockSectionItem>
                                                            <apex:pageBlockSectionItem >                                        
                                                                <apex:outputLabel >{!$Label.Atvi_Phone}:</apex:outputLabel>
                                                                <apex:outputText value="{!rmaCase.Phone__c}"></apex:outputText>
                                                            </apex:pageBlockSectionItem>
                                                            <apex:pageBlockSectionItem >
                                                                <apex:outputLabel >{!$Label.Atvi_ProblemDescription}</apex:outputLabel>
                                                                <apex:outputText value="{!rmaCase.Problem_Description__c}"></apex:outputText>
                                                            </apex:pageBlockSectionItem>
                                                            <apex:pageBlockSectionItem >
                                                                <apex:outputLabel >{!$Label.Atvi_Street}</apex:outputLabel>
                                                                <apex:outputText value="{!rmaCase.Address_1__c}"></apex:outputText>
                                                            </apex:pageBlockSectionItem>
                                                            <apex:pageBlockSectionItem >
                                                                <apex:outputLabel >{!$Label.Atvi_Street2}</apex:outputLabel>
                                                                <apex:outputText value="{!rmaCase.Address_2__c}"></apex:outputText>
                                                            </apex:pageBlockSectionItem>
                                                            <apex:pageBlockSectionItem >
                                                                <apex:outputLabel >{!$Label.Atvi_City}</apex:outputLabel>
                                                                <apex:outputText value="{!rmaCase.City__c}"></apex:outputText>
                                                            </apex:pageBlockSectionItem>
                                                            <apex:pageBlockSectionItem >
                                                                <apex:outputLabel >{!$Label.Atvi_StateProvince}</apex:outputLabel>
                                                                <apex:outputText value="{!rmaCase.State__c}"></apex:outputText>
                                                            </apex:pageBlockSectionItem>
                                                            <apex:pageBlockSectionItem >
                                                                <apex:outputLabel >{!$Label.Atvi_ZipPostal_Code}</apex:outputLabel>
                                                                <apex:outputText value="{!rmaCase.Zipcode__c}"></apex:outputText>
                                                            </apex:pageBlockSectionItem>                                                   
                                                        </apex:pageBlockSection>
                                                    </apex:pageBlock></td>
                                                <td style="width: 47%; vertical-align: top;">
                                                
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="actionButtons">
                                            <apex:outputLink value="{!$Page.AtviMyCasesPage}?rma=1"
                                                styleClass="nextBtn">{!$Label.Atvi_BacktoMyReturns}</apex:outputLink>
                                        </div>
                                    </div>
                                </apex:outputPanel>
                            </apex:form>
                            </div>
                        </div>
                    </apex:outputPanel>
                </div>
            </apex:define>
        </apex:composition>
    </html>
</apex:page>