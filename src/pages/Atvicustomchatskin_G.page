<!-- 
* @Name             Atvicustomchatskin
* @author           Karthik Sankar
* @date             25-MAR-2012
* @description      The page to connect to chat
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------
* Developer                 Date                    Description
* ---------------           -----------             ----------------------------------------------
* Karthik Sankar            25-MAR-2012             Initial draft
* Karthik Sankar            29-MAR-2012             Added JS for LiveAgent chat
* Abhinandan Panda          29-MAR-2012             Optimized the JavaScripts
* Priyankshu M              19-APR-2012             Optimized the HTML  
* Manas jain                23-APR-2012             Code Cleanup
* 
*---------------------------------------------------------------------------------------------------
* Review Log    :
*---------------------------------------------------------------------------------------------------
* Reviewer                                          Review Date                     Review Comments
* --------                                          ------------                    ---------------* 
*  A. Pal(Deloitte Senior Consultant)               19-APR-2012                     Final Inspection before go-live
*---------------------------------------------------------------------------------------------------
-->
<apex:page showHeader="false" 
           controller="ATVICustomerPortalController" 
           action="{!doUserNameLoad}" 
           language="{!userLanguage}" 
           cache="false">
    <link rel="shortcut icon" type="image/ico" href="{!URLFOR($Resource.Atvi_Brandings, 'Atvi_Brandings/Generic/favicon.ico')}" />
    <apex:outputpanel rendered="{!isMobile}" layout="none">
        <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0"/>
    </apex:outputpanel>
    <style type="text/css">
        html{
            background: #DADADB;
            resize: none;
        }
        
        body {
            margin: 0;
            padding: 12px;
            background: #DADADB;
            font-size: 12px;
            font-family: Arial, Helvetica, sans-serif;
        }
        
        a,a:hover {
            text-decoration: none;
        }
        
        <apex:outputpanel rendered="{!Not(isMobile)}" layout="none">
            .pod,#liveAgentChatLog {
                position: relative;
                margin: 0 0 7px;
                border: 1px solid #ccc;
                padding: 12px;
                background: #fff;
                -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25);
                -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25);
                -o-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25);
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25);
            }
            #liveAgentClientChat {
                float: left;
                margin-top: -10px;
                margin-left: -10px;
            }
            #liveAgentChatLog {
                background: #fff;
                overflow: auto;
                width: 395px;
                margin: 0 1px;
                height: 195px;
                padding: 13px 16px;
                word-wrap: break-word;
            }
            .endChatButton{
                float:right;
                width:150px;
            }
            .mainChatWindow {   
                background: url("{!
                IF(CONTAINS(LOWER(branding),'modern_warfare_3'), URLFOR($Resource.Atvi_chatsupportbg, 'mw3-web-chatWindowBg.jpg'), 
                    IF(CONTAINS(LOWER(branding), 'black_ops_ii'), URLFOR($Resource.Atvi_chatsupportbg, 'blk-ops-2-web-chatWindowBg.jpg'),
                        IF(CONTAINS(LOWER(branding), 'black_ops'), URLFOR($Resource.Atvi_chatsupportbg, 'blk-ops-web-chatWindowBg.jpg'),
                            IF(CONTAINS(LOWER(branding), 'ghosts'), URLFOR($Resource.Atvi_chatsupportbg, 'ghosts-web-chatWindowBg.jpg'),
                                IF(CONTAINS(LOWER(branding), 'advanced_war'), URLFOR($Resource.Atvi_chatsupportbg, 'aw-web-chatWindowBg.jpg'),
                                    IF(CONTAINS(LOWER(branding), 'swap_force'), URLFOR($Resource.Atvi_chatsupportbg, 'swapforce-web-chatWindowBg.png'),
                                        IF(CONTAINS(LOWER(branding), 'trap_team'), URLFOR($Resource.Atvi_chatsupportbg, 'trapteam-web-chatWindowBg.png'),
                                            URLFOR($Resource.Atvi_chatsupportbg, 'generic-web-chatWindowBg.jpg')
                                        )
                                    )
                                )
                            )
                        )
                    )
                )}") 100% 100% transparent;
            }
            #liveAgentChatInput {
                width: 425px;
                height: 30px;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 13px;
            }
        </apex:outputpanel>

        <apex:outputPanel layout="none" rendered="{!OR(contains(LOWER(branding), 'swap_force'),contains(LOWER(branding), 'trap_team'))}">
            .intersitialContainer p {
                background: no-repeat scroll 0 0 transparent!important;
                padding: 0px 0px 0px 0px!important;
            }
            .liveAgentSendButton{
                color:#2599CF!important;
            }
            .liveAgentEndButton, .closeWaitingButton{
                color:#2599CF!important;
            }
        </apex:outputPanel>

        <apex:outputpanel rendered="{!isMobile}" layout="none">
            .mainChatWindowMobile {   
                background: 100% 100% transparent;
            }
            .pod,#liveAgentChatLog {
                position: relative;
                margin: 0;
                width: 100%;
                border: 1px solid #ccc;
                padding: 0;
                background: #fff;
                -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25);
                -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25);
                -o-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25);
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25);
            }
            .mainChatWindow {height: auto; position: relative; width: 100%;}
            
            .livehelp {
                background-image: none;
                border-radius: 0 0 0 0;
                display: block;
                float: left;
                font-size: 25px;
                line-height: 50px;
                font-weight: normal;
                margin: 0 0 20px 0;
                padding: 0;
                text-align: center;
                text-decoration: none;
                width: 50%;
                height: ;
            }
            
            .liveAgentEndButton, .closeWaitingButton {
                background-color: #454646;
                background-image: none;
                border-radius: 0 0 0 0;
                color: #A6D103;
                display: block;
                float: left;
                font-size: 50px;
                font-weight: normal;
                margin: -5px 6px 0 270px;
                padding: 6px 0 4px;
                text-align: center;
                text-decoration: none;
                width: 500px;
                height: 100px;
            }
            
            .liveAgentSendButton {
                background-color: #454646;
                background-image: none;
                border-radius: 0 0 0 0;
                color: #A6D103;
                display: block;
                float: none;
                font-size: 60px;
                font-weight: normal;
                margin: auto;
                padding: 0;
                text-align: center;
                text-decoration: none;
                width: 100%;
                height: 100px;
            }
            
            .secureConnection p {
                color: #000000;
                font-size: 22px;
                margin-top: 23px;
                margin-bottom: 20px;
                padding-left: 0;
            }

            #liveAgentChatLog {
                background: #fff;
                overflow: auto;
                margin: 10px 1px 0px 1px;
                height: 195px;
                padding: 13px 16px;
                word-wrap: break-word;
                box-sizing: border-box;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            .liveAgentEndButton,.liveAgentSendButton { 
                width: 50%;
                margin: auto;
                padding: 15px 10px;
                height: auto;
                font-size: 20px;
            }
            #liveAgentChatInput {
                width: 100%;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 18px;
                padding: 10px;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
            }
            .actionButtons {width: 100%;}
        </apex:outputpanel>
        
        
        #liveAgentClientChat,.modal {
            height: 350px;
        }
        
        .modal {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            text-align: center;
            vertical-align: middle;
        }
        
        .modal em {
            display: inline-block;
        }
        
        #chatControls {
            height: 25px;
            background: #E9E9E9 -223px -79px no-repeat;
        }
        #queuePOS img {margin: 0 auto;}
        .intersitialContainer p {padding: 20px 0 0 65px;}
        
        #liveAgentChatLog .operator,#liveAgentChatLog .client,#liveAgentChatLog .system
        {
            display: block;
            margin: 0 0 7px;
            border: 1px solid #ccc;
            padding: 8px;
            background-color: #f7f7f7;
            line-height: 18px;
            color: #3A3738;
            -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);
            -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);
            -o-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);
        }
        
        #liveAgentChatLog .operator,#liveAgentChatLog .system {
            padding-right: 30px;
            background: #fff 98% 9px no-repeat;
        }
        
        #liveAgentChatLog .operator strong {
            color: #E97938;
        }
        
        #liveAgentChatLog .client {
            background: #fafafa;
        }
        
        #liveAgentChatLog .client strong {
            color: #2599CF;
        }
        
        #liveAgentChatLog .system {
            background-color: #f8f2b5;
        }
        
        #liveAgentChatLog #liveAgentChatLogTyping {
            background: none;
            color: #AAAAAA;
            padding-left: 25px;
            text-transform: lowercase;
        }
        
        .alert {
            position: relative;
            width: 245px;
            margin: 0 auto;
            padding: 10px 14px;
            background: #f6f1bf;
            line-height: 15px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -moz-box-shadow: 0 1px 3px #323232;
            -webkit-box-shadow: 0 1px 3px #323232;
            box-shadow: 0px 1px 3px #323232;
        }
        
        #chatInput {
            height: 25px;
        }
        
        #chatInput form {
            position: absolute;
            top: 12px;
            right: 70px;
            left: 12px;
            height: 26px;
            border: 1px solid #cacaca;
            background: #EEEEEE;
        }
        
        #chatInput #liveAgentChatInput {
            width: 97%;
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            right: 0;
            border: none;
            padding: 5px;
            background: none;
        }
        
        button {
            display: block;
            border: none;
            padding: 5px;
            background: #2599CF;
            color: #fff;
            font-weight: 700;
            text-transform: uppercase;
            letter-spacing: -.5px;
            cursor: pointer;
        }
        
        button:hover {
            background:#000000;
        }
        
        /* START State Specific */
        #waitingMessage {
            display: none;
            position: absolute;
            left: 0;
            z-index: 1;
            height: auto;
        }
        
        #liveAgentClientChat.liveAgentStateWaiting #waitingMessage {
            display: block;
            width: 100%;
            height: 350px;
        }
        
        .intersitialContainer {width: 100%}
        
        #liveAgentClientChat.liveAgentStateWaiting .liveAgentSaveButton {
            display: none;
        }
        
        #liveAgentClientChat.liveAgentStateWaiting .mainChatWindow {
            display: none !important;
        }
        
        #liveAgentClientChat.liveAgentStateWaiting  #chatInput {
            display: none !important;
        }
        
        #liveAgentClientChat.liveAgentStateEnded  #liveAgentChatInput {
            display: none !important;
        }
        /* END State Specific */
        
        .closeWaitingButton{
            margin-bottom:3px;
        }
        
        .mainChatWindow .left p {
            padding-right: 26px;
            padding-left: 20px;
        }
        
        .mainChatWindow .left {
            padding-top: 2px;
        }
        
        .mainChatWindow .left h2 {
            border: none;
        }
        
        .mainChatWindow .right {
            padding-top: 20px;
        }
        
        .mainChatWindow .right .welcome {
            margin-top: 25px;
            padding: 0px;
            border: none;
        }
        
        .intersitialContainer span { *
            float: left;
        }
        
        /* START Alert Styles */
        .modal td {
            vertical-align: middle;
            background: url(../images/alert_xy.png);
        }
        
        .liveAgentStateEnded #liveAgentMessageStatus {
            display: none;
        }
        
        .liveAgentStateEnded .secureConnection {
            display: none;
        }
        
        #liveAgentMessageContainer {
            display: none;
        }
        
        .liveAgentStateEnded #liveAgentMessageContainer {
            display: block;
            position: absolute;
            z-index: 500;
            border: none;
            background: none;
        }
        
        .liveAgentStateEnded #liveAgentChatLog {
            height: 275px;
        }
        
        #liveAgentMessageContainer div {
            position: relative;
            width: 245px;
            margin: 0 auto;
            padding: 10px 14px;
            background: #f6f1bf;
            line-height: 15px;
            font-size: 1.333em;
            margin-top: 150px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -moz-box-shadow: 0 1px 3px #323232;
            -webkit-box-shadow: 0 1px 3px #323232;
            box-shadow: 0px 1px 3px #323232;
        }
        #newsTextContainer{
            height: 225px;
            width: 293px;
            position: absolute;
            left: 5px;
            top: 68px;
            overflow-x: hidden;
            overflow-y: hidden;
        }
        #newsTextContainer .sfdc_richtext{ 
            height: 225px;
            width: 280px;
            overflow-x: hidden;
            overflow-y: hidden;
            padding: 10px 15px 10px 20px;
        }

        #newsTextContainer table{
            border:none;
        }

        #newsTextContainer table img{
            position: relative;
            bottom: 0;
            left: 0;
        }
        
        /* This is for ie7 */
        #liveAgentMessageContainer div {
            border: 1px solid #d4cb63;
        }
        
        /* ie8 does not get this, this is for all others */
        #liveAgentMessageContainer div,#nonie8#fix {
            border: none;
        }
        
        #liveAgentMessageContainer div strong {
            font-weight: normal;
            color: #1d1d1d;
        }
        
        #liveAgentMessageContainer div strong strong {
            font-size: 1.167em;
            color: #C00;
            vertical-align: middle;
        }
        
        #liveAgentMessageContainer div a {
            color: #0561c8;
        }
        
        #liveAgentMessageContainer div a:hover {
            text-decoration: underline;
        }
        
        #liveAgentMessageContainer  #alert_close {
            display: block;
            margin: 2px 0 0;
            text-align: right;
        }
        /* END Alert Styles */

        /*Queue Pos info */
        #queuePOS p{
            font-weight: bold;
            color: #6B6B6B;
            font-size: 14px;
            line-height: 20px;
            padding: 5px 0 0 0;
            height: 25px;
            background: none;
        }
    </style>
    
    <liveAgent:clientchat >
        <apex:stylesheet value="{!URLFOR($Resource.Xi_agency, 'css/reset.css')}" />
        <apex:stylesheet value="{!URLFOR($Resource.Xi_agency, 'css/helpers.css')}" />
        <apex:stylesheet value="{!$Resource.Activision_css}" />
        <apex:stylesheet value="{!URLFOR($Resource.Xi_agency, 'css/atviSupport.css')}" />
        <apex:stylesheet value="{!URLFOR($Resource.Xi_agency, 'css/popup-support.css')}" />
        <apex:stylesheet value="{!URLFOR($Resource.Xi_agency, 'css/colorbox.css')}" />
        <apex:includeScript value="{!URLFOR($Resource.Xi_agency, 'jQuery_UI/js/jquery-1.7.1.min.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.Xi_agency, 'js/jquery.infieldlabel.min.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.Xi_agency, 'js/jquery.jscrollpane.min.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.Xi_agency, 'js/atviSupport.js')}" />
        <style>
            /* Override Styles */
            .intersitialContainer .content {
                top: 50px;
                width: 320px;
            }
        </style>
        <apex:outputPanel layout="none" rendered="{!CONTAINS($CurrentPage.URL,'573U0000000Gqw9')}">
            <style>
                .intersitialContainer{
                    background: #318CE7 !important;
                }
            </style>
        </apex:outputPanel>

        <div id="waitingMessage" class="modal">
            <div class="intersitialContainer">
                <div class="content">
                    <div id="queuePOS" style="padding-bottom:25px;"><p id="m1"></p><p id="m2"></p><em style="display:none;">
                    <liveAgent:clientChatQueuePosition rendered="false" /></em></div>
                    <!-- CALL OF DUTY Chat waiting Backgrounds -->
                    <apex:image url="{!URLFOR($Resource.Atvi_chatsupportbg, 'chat-logos/CoD-chat-logo.png')}"
                                alt="Black Ops"
                                rendered="{!branding == 'Black_Ops'
                                            && NOT(CONTAINS($CurrentPage.URL,'573U0000000Gqw9'))}" />
                    <apex:image url="{!URLFOR($Resource.Atvi_chatsupportbg, 'chat-logos/MW3-chat-logo.png')}"
                                alt="Modern Warfare"
                                rendered="{!OR(branding == 'MW3', CONTAINS(LOWER(branding),'modern_warfare_3')) 
                                            && NOT(CONTAINS($CurrentPage.URL,'573U0000000Gqw9'))}" />
                    <apex:image url="{!URLFOR($Resource.Atvi_chatsupportbg, 'chat-logos/BO2-chat-logo.png')}"
                                alt="Black Ops II"
                                rendered="{!OR(branding == 'Black_Ops_II', CONTAINS(LOWER(branding),'black_ops_ii')) 
                                            && NOT(CONTAINS($CurrentPage.URL,'573U0000000Gqw9'))}" />    
                    <apex:image url="{!URLFOR($Resource.Atvi_chatsupportbg, 'chat-logos/Ghosts-chat-logo.png')}"
                                alt="Ghosts"
                                rendered="{!CONTAINS(LOWER(branding),'ghosts') 
                                            && NOT(CONTAINS($CurrentPage.URL,'573U0000000Gqw9'))}" />
                    <apex:image url="{!URLFOR($Resource.Atvi_chatsupportbg, 'chat-logos/AW-chat-logo.png')}"
                                alt="Advanced Warfare"
                                rendered="{!CONTAINS(LOWER(branding),'advanced_warfare') 
                                            && NOT(CONTAINS($CurrentPage.URL,'573U0000000Gqw9'))}" />
                    
                    <!-- SKYLANDERS Chat Waiting Backgrounds --> <!--height="160" width="403"-->
                    <apex:image style="width:100%;"
                                url="{!URLFOR($Resource.Atvi_chatsupportbg, 'chat-logos/SwapForce-chat-logo.png')}"
                                alt="Skylanders Swap Force"
                                rendered="{!CONTAINS(LOWER(branding), 'swap_force') && CONTAINS($CurrentPage.URL,'573U0000000Gqw9')}"/>
                    <apex:image style="width:100%;"
                                url="{!URLFOR($Resource.Atvi_chatsupportbg, 'chat-logos/TrapTeam-chat-logo.png')}"
                                alt="Skylanders Trap Team"
                                rendered="{!CONTAINS(LOWER(branding), 'trap_team') && CONTAINS($CurrentPage.URL,'573U0000000Gqw9')}"/>

                    <!-- GENERIC Chat Waiting Background -->
                    <apex:image url="{!URLFOR($Resource.Atvi_chatsupportbg, 'chat-logos/Generic-chat-logo.png')}"
                                alt="Activision"
                                rendered="{!branding == '' && NOT(CONTAINS($CurrentPage.URL,'573U0000000Gqw9'))}" />
                    <p>
                        <apex:outputPanel layout="none" rendered="{!NOT(CONTAINS($CurrentPage.URL,'573U0000000Gqw9'))}">
                           {!IF(isClanLead, 'Connecting you to a Clan Leader Specialist', $Label.Atvi_Redirecting)}
                        </apex:outputPanel>
                        <apex:outputPanel layout="none" rendered="{!CONTAINS($CurrentPage.URL,'573U0000000Gqw9')}">
                            {!$Label.Chat_Connecting_To_Skylands}
                        </apex:outputPanel>
                        <br/>
                        <br/>
                        <apex:image styleClass="loadingBar" url="{!URLFOR($Resource.Atvi_Brandings, 'Atvi_Brandings/Generic/chat-waiting_gray_sm-2.gif')}" alt="{!$Label.Chat_Connecting}"/>
                    </p>
                </div>
            </div>
        </div>

        <liveAgent:clientChatMessages />
                    
        <div class="mainChatWindow clearfix">

            <apex:outputpanel rendered="{!not(isMobile)}" layout="none">
            <div class="left">
                <h2>{!$Label.Atvi_WELCOME} <span style="text-transform: uppercase;">
                    <apex:outputText rendered="{!isLoggedIn}"> {!IF(isClanLead, $Label.clan_Commander + '  ' +  userLastName, userName)} </apex:outputText>
                    <apex:outputText rendered="{!NOT(isLoggedIn)}">{!$CurrentPage.Parameters.liveagent.prechat:FirstName}</apex:outputText>
                    </span></h2>
                <div id="newsTextContainer">
                    <br/>
                    <h3 style="padding-left:20px;">{!$Label.Atvi_ChatLeftSidebar1}</h3>
                    <br/>
                    <br/>
                    <div style="padding-left:20px;">
                        <h3>{!$Label.Atvi_ChatLeftSidebar2}</h3>
                    </div>
                    
                    <apex:outputField rendered="{!userLanguage=='en_US'}" value="{!cNews.news__c}"/>
                    <apex:outputField rendered="{!userLanguage=='en_AU'}" value="{!cNews.news__c}"/>
                    <apex:outputField rendered="{!userLanguage=='en_FI'}" value="{!cNews.news__c}"/>
                    <apex:outputField rendered="{!userLanguage=='en_GB'}" value="{!cNews.news__c}"/>
                    <apex:outputField rendered="{!userLanguage=='en_NO'}" value="{!cNews.news__c}"/>
                    <apex:outputField rendered="{!userLanguage=='de_DE'}" value="{!cNews.German_News__c}"/>
                    <apex:outputField rendered="{!userLanguage=='fr_FR'}" value="{!cNews.French_News__c}"/>
                    <apex:outputField rendered="{!userLanguage=='fr_BE'}" value="{!cNews.French_News__c}"/>
                    <apex:outputField rendered="{!userLanguage=='fr_LU'}" value="{!cNews.French_News__c}"/>
                    <apex:outputField rendered="{!userLanguage=='es_ES'}" value="{!cNews.Spanish_News__c}"/>
                    <apex:outputField rendered="{!userLanguage=='it_IT'}" value="{!cNews.Italian_News__c}"/>
                    <apex:outputField rendered="{!userLanguage=='nl_NL'}" value="{!cNews.Dutch_News__c}"/>
                    <apex:outputField rendered="{!userLanguage=='pt_BR'}" value="{!cNews.Portuguese_News__c}"/>
                    <apex:outputField rendered="{!userLanguage=='sv_SE'}" value="{!cNews.Swedish_News__c}"/>
                    
                </div>
            </div>
            </apex:outputpanel>
            <div class="{!IF(IsMobile, '','right')}">
                <apex:outputPanel layout="none" rendered="{!NOT(IsMobile)}">
                <h2 style="display: inline;">{!$Label.Atvi_LIVEHELP}</h2>
                </apex:outputPanel>
                <apex:outputPanel layout="none" rendered="{!IsMobile}">
                    <p class="livehelp">{!$Label.Atvi_LIVEHELP}</p>
                </apex:outputPanel>
                <div class="endChatButton">
                    <button class="liveAgentChatElement liveAgentEndButton" onclick="if(confirm('{!$Label.Chat_Thank_You}')){liveagent.chasitor.endChat();window.close();}">{!$Label.Chat_End_Chat} X</button>
                </div>
                
                <div class="welcome"></div>
                
                <liveAgent:clientChatLog />

                <div class="messageBox">
                    <liveagent:clientChatInput />
                    <div class="actionButtons clearfix">
                        <liveAgent:clientChatSendButton rendered="{!NOT(IsMobile)}" label="{!$Label.Chat_Send}" />
                    </div>
                    <div class="secureConnection">
                        <p class="p1">
                            {!$Label.Chat_You_are_on_a_secure_connection}
                        </p>
                    </div>
                </div>
            </div>
            
        </div>
        
        </liveAgent:clientchat>
    
    <!-- You can customize the window title with the js below -->
    <script type="text/javascript">
        var queueVar;
        var titleVar;
        var numberOfChats;
        var h = window.innerHeight;
        var w = window.innerWidth;
        queueVar = setInterval(function(){queueInfo()}, 500);

        document.title = "{!$Label.Chat_Activision_Live_Chat}";      
        
        window.liveagent.Utils = { newWindow: function(url) { window.open(url); } }
        try{
            var chatCookie = getCookie('ChatEnabled');
            if(chatCookie == undefined || chatCookie == '' || chatCookie == 'en_US'){
                
            }else{
                setCookiePath("ChatEnabled",'');
            }
        }catch(e){}
        
        $(window).blur(function(){
            numberOfChats = $('#liveAgentChatLogText').children().size();
            titleVar = setInterval(function(){titleBlink()},2000);
        });
        
        $(window).focus(function(){
            clearInterval(titleVar);
            document.title="{!$Label.Chat_Activision_Live_Chat}";
        });
        
        function queueInfo(){
            if($("#waitingMessage").css('display') == "none"){
                clearInterval(queueVar);
            }
            else{
                var queuePOS = $(".liveAgentQueuePosition").text();
                if(queuePOS == ""){
                    $("#queuePOS #m1").text("");
                }else{
                    $("#queuePOS #m1").text("Current position in the queue: " + queuePOS);
                }
            }
        }
        
        function resizeWindow(w,h){
            window.resizeTo(w,h);
        }
        
        function titleBlink(){
            var chatCompare = $('#liveAgentChatLogText').children().size();
            if(chatCompare > numberOfChats){
                if(document.title == "{!$Label.Chat_Activision_Live_Chat}"){
                    document.title= $('#liveAgentChatLogText').children().last().text();
                }else{
                    document.title="{!$Label.Chat_Activision_Live_Chat}";
                }
            }
        }
        
        function setCookiePath(c_name,value,exdays){
            var exdate=new Date();
            exdate.setDate(exdate.getDate() + exdays);
            var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
            document.cookie=c_name + "=" + c_value + ";path=/apex/";
            document.cookie=c_name + "=" + c_value + ";path=/apex";
            document.cookie=c_name + "=" + c_value + ";path=/";
        }

    </script>
</apex:page>