<apex:page action="{!currentStatus}" tabStyle="CRR_Status__tab" controller="CRR_Status">
    <apex:sectionHeader title="System status" subTitle="Activision Code repository" />
    <apex:pageMessages />
    <apex:pageBlock title="Next auto run task">
        <apex:pageBlockButtons rendered="{!canStart}" location="top">
            <apex:form >
                <apex:commandButton action="{!startJob}" value="Start {!NextTaskType} now" />
            </apex:form>
        </apex:pageBlockButtons>
        <apex:pageBlockSection >
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Next task" />
                <apex:outputText value="{!NextTaskType}" />
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem labelStyle="{!IF(NextTaskType = 'no pending jobs', 'display:none;','')}" 
                  dataStyle="{!IF(NextTaskType = 'no pending jobs', 'display:none;','')}"
                  helpText="Every 8, 23, 37 & 52 minutes after the hour">
                <apex:outputLabel value="Next task auto-start in" />
                <apex:outputPanel layout="none">
                    <div id="secondsCountdown"></div><br /><div id="taskclock"></div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>
    </apex:pageBlock>
    <apex:pageBlock title="Documents">
        <apex:pageBlockSection title="System Integration status">
            <apex:pageBlockSectionItem helpText="Every 0, 15, 30 & 45 minutes after the hour">
                <apex:outputLabel value="Next document download" />
                <apex:outputPanel layout="none">
                    <div id="documentDownload"></div><br /><div id="clock2"></div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Last verified document download" />
                <apex:outputText value="{!LastIntegrationCheckIn}" />
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>
        <apex:form >
            <apex:pageBlockSection title="Unprocessed documents in the library {{!documentsToProcess.size}}" columns="1">
                <apex:pageBlockTable value="{!DocumentsToProcess}" var="doc" rendered="{!documentsToProcess.size > 0}">
                    <apex:column headerValue="Document Title">
                        <apex:outputField value="{!doc.Title}" />
                    </apex:column>
                    <apex:column headerValue="Created">
                        <apex:outputField value="{!doc.CreatedById}" />
                    </apex:column>
                    <apex:column headerValue="Created Date">
                        <apex:outputField value="{!doc.createdDate}" />
                    </apex:column>
                    <apex:column headerValue="First party">
                        <apex:outputField value="{!doc.First_party__c}" />
                    </apex:column>
                    <apex:column headerValue="First Party Group">
                        <apex:outputField value="{!doc.First_party_group__c}" />
                    </apex:column>
                    <apex:column headerValue="FP Reference Number">
                        <apex:outputField value="{!doc.First_party_reference_number__c}" />
                    </apex:column>
                    <apex:column headerValue="Code Product Name">
                        <apex:outputField value="{!doc.Code_Product__r.Name}" />
                    </apex:column>
                    <!--
                    <apex:column headerValue="Start Processing">
                        <apex:actionStatus id="sProcStat">
                            <apex:facet name="stop">
                            </apex:facet>
                            <apex:facet name="start">
                                <img src="/img/loading24.gif" />
                            </apex:facet>
                        </apex:actionStatus>
                    </apex:column>-->
                </apex:pageBlockTable>
            </apex:pageBlockSection>
            <apex:pageBlockSection title="Processed documents in the library {{!documentsProcessed.size}}" columns="1">
                <apex:pageBlockTable value="{!DocumentsProcessed}" var="doc" rendered="{!documentsProcessed.size > 0}">
                    <apex:column headerValue="Document Title">
                        <apex:outputField value="{!doc.Title}" />
                    </apex:column>
                    <apex:column headerValue="Created by">
                        <apex:outputField value="{!doc.CreatedById}" />
                    </apex:column>
                    <apex:column headerValue="Created Date">
                        <apex:outputField value="{!doc.createdDate}" />
                    </apex:column>
                    <apex:column headerValue="First party">
                        <apex:outputField value="{!doc.First_party__c}" />
                    </apex:column>
                    <apex:column headerValue="First Party Group">
                        <apex:outputField value="{!doc.First_party_group__c}" />
                    </apex:column>
                    <apex:column headerValue="FP Reference Number">
                        <apex:outputField value="{!doc.First_party_reference_number__c}" />
                    </apex:column>
                    <apex:column headerValue="Code Product Name">
                        <apex:outputField value="{!doc.Code_Product__r.Name}" />
                    </apex:column>
                </apex:pageBlockTable>
            </apex:pageBlockSection>
        </apex:form>
    </apex:pageBlock>
    <apex:pageBlock title="Jobs run in the past 7 days">
        <apex:pageBlockSection title="Currently running jobs {{!currentlyRunningJobs.size}}" columns="1">
            <apex:pageBlockTable value="{!CurrentlyRunningJobs}" var="job" title="Jobs currently running" rendered="{!currentlyRunningJobs.size > 0}">
                <apex:column headerValue="Requested by">
                    <apex:outputText value="{!job.CreatedBy.Name}" />
                </apex:column>
                <apex:column headerValue="Job type">
                    <apex:outputText value="{!job.ApexClass.Name}" />
                </apex:column>
                <apex:column headerValue="Request time">
                    <apex:outputField value="{!job.CreatedDate}" />
                </apex:column>
                <apex:column headerValue="Status">
                    <apex:outputText value="{!job.Status}" />
                </apex:column>
                <apex:column headerValue="Completed time">
                    <apex:outputField value="{!job.CompletedDate}" />
                </apex:column>

                <apex:column headerValue="Total batches">
                    <apex:outputField value="{!job.TotalJobItems}" />
                </apex:column>

                <apex:column headerValue="Batches completed">
                    <apex:outputField value="{!job.JobItemsProcessed}" />
                </apex:column>

                <apex:column headerValue="Batches completed with Errors">
                    <apex:outputField value="{!job.NumberOfErrors}" />
                </apex:column>

            </apex:pageBlockTable>
        </apex:pageBlockSection>
        <apex:pageBlockSection title="Jobs Queued {{!QueuedJobs.size}}" columns="1">
            <apex:pageBlockTable value="{!QueuedJobs}" var="job" title="Pending jobs" rendered="{!QueuedJobs.size > 0}">
                <apex:column headerValue="Requested by">
                    <apex:outputText value="{!job.CreatedBy.Name}" />
                </apex:column>
                <apex:column headerValue="Job type">
                    <apex:outputText value="{!job.ApexClass.Name}" />
                </apex:column>
                <apex:column headerValue="Request time">
                    <apex:outputField value="{!job.CreatedDate}" />
                </apex:column>
                <apex:column headerValue="Status">
                    <apex:outputText value="{!job.Status}" />
                </apex:column>
                <apex:column headerValue="Completed time">
                    <apex:outputField value="{!job.CompletedDate}" />
                </apex:column>

                <apex:column headerValue="Total batches">
                    <apex:outputField value="{!job.TotalJobItems}" />
                </apex:column>

                <apex:column headerValue="Batches completed">
                    <apex:outputField value="{!job.JobItemsProcessed}" />
                </apex:column>

                <apex:column headerValue="Batches completed with Errors">
                    <apex:outputField value="{!job.NumberOfErrors}" />
                </apex:column>

            </apex:pageBlockTable>
        </apex:pageBlockSection>
        <apex:pageBlockSection title="Jobs being prepared {{!PreparingJobs.size}}" columns="1">
            <apex:pageBlockTable value="{!PreparingJobs}" var="job" title="Jobs about to begin" rendered="{!PreparingJobs.size > 0}">
                <apex:column headerValue="Requested by">
                    <apex:outputText value="{!job.CreatedBy.Name}" />
                </apex:column>
                <apex:column headerValue="Job type">
                    <apex:outputText value="{!job.ApexClass.Name}" />
                </apex:column>
                <apex:column headerValue="Request time">
                    <apex:outputField value="{!job.CreatedDate}" />
                </apex:column>
                <apex:column headerValue="Status">
                    <apex:outputText value="{!job.Status}" />
                </apex:column>
                <apex:column headerValue="Completed time">
                    <apex:outputField value="{!job.CompletedDate}" />
                </apex:column>

                <apex:column headerValue="Total batches">
                    <apex:outputField value="{!job.TotalJobItems}" />
                </apex:column>

                <apex:column headerValue="Batches completed">
                    <apex:outputField value="{!job.JobItemsProcessed}" />
                </apex:column>

                <apex:column headerValue="Batches completed with Errors">
                    <apex:outputField value="{!job.NumberOfErrors}" />
                </apex:column>
                
            </apex:pageBlockTable>
        </apex:pageBlockSection>
        <apex:pageBlockSection title="Completed Jobs {{!completedJobs.size}}" columns="1">
            <apex:pageBlockTable value="{!CompletedJobs}" var="job" title="Completed jobs" rendered="{!completedJobs.size > 0}">
                <apex:column headerValue="Requested by">
                    <apex:outputText value="{!job.CreatedBy.Name}" />
                </apex:column>
                <apex:column headerValue="Job type">
                    <apex:outputText value="{!job.ApexClass.Name}" />
                </apex:column>
                <apex:column headerValue="Request time">
                    <apex:outputField value="{!job.CreatedDate}" />
                </apex:column>
                <apex:column headerValue="Status">
                    <apex:outputText value="{!job.Status}" />
                </apex:column>
                <apex:column headerValue="Completed time">
                    <apex:outputField value="{!job.CompletedDate}" />
                </apex:column>
                
                <apex:column headerValue="Total batches">
                    <apex:outputField value="{!job.TotalJobItems}" />
                </apex:column>

                <apex:column headerValue="Batches completed">
                    <apex:outputField value="{!job.JobItemsProcessed}" />
                </apex:column>

                <apex:column headerValue="Batches completed with Errors">
                    <apex:outputField value="{!job.NumberOfErrors}" />
                </apex:column>
                
            </apex:pageBlockTable>
        </apex:pageBlockSection>
    </apex:pageBlock>
    <script type="text/javascript">
var documentCountdown = calcSecondsUntilNextDocumentDownload(); 
function calcSecondsUntilNextDocumentDownload(){
    var d = new Date();
    var m = d.getMinutes() + 1;
    var s = Math.abs((d.getSeconds() - 60));
    var until;
    if(m < 15){ until = 15 - m;
    }else if(m < 30){ until = 30 - m;
    }else if(m < 45){ until = 45 - m;
    }else{ until = 60 - m; }
    var result = (until * 60) + s;
    var d2=new Date();
    d2.setSeconds(d2.getSeconds() + result);
    var t=d2.toLocaleTimeString();
    document.getElementById("clock2").innerHTML = "<B>" + t + "</B>";
    return result;
}
var taskCountdown = calcSecondsUntilNextTask();
function calcSecondsUntilNextTask(){
    var d = new Date();
    var m = d.getMinutes() + 1;
    var s = Math.abs((d.getSeconds() - 60));
    var until;
    if(m < 8 || m > 52){ 
        if(m < 8){ until = 8 - m;
        }else{ until = (60 - m) + 8; }
    }else if(m < 23){ until = 23 - m;
    }else if(m < 37){ until = 37 - m;
    }else{ until = 52 - m; }
    var result = (until * 60) + s;
    var d2=new Date();
    d2.setSeconds(d2.getSeconds() + result);
    var t=d2.toLocaleTimeString();
    document.getElementById("taskclock").innerHTML = "<B>" + t + "</B>";
    return result;
}
var int = self.setInterval("clock()",1000);
function clock(){
    documentCountdown = documentCountdown - 1;
    taskCountdown = taskCountdown - 1;
    document.getElementById("documentDownload").innerHTML = "<B>"+documentCountdown+"</B> seconds";
    document.getElementById("secondsCountdown").innerHTML = "<B>"+taskCountdown+"</B> seconds";
    if(documentCountdown == 0){
        documentCountdown = calcSecondsUntilNextDocumentDownload();
    }
    if(taskCountdown == 0){
        taskCountdown = calcSecondsUntilNextTask();
        //location.reload(); //originally it reloaded the page to show the new data
    }
}
</script>
</apex:page>